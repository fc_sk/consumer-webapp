package com.freecharge.payment.dos.web;

import java.io.Serializable;
import java.util.List;

public class NetBankingOptionResponse implements Serializable{

    private static final long serialVersionUID = 1L;

    private List<NetBankingOption> priorityBanks;
    
    private List<NetBankingOption> banks;
    
    private String errorCode;
        
    private String errorDesc;

    public NetBankingOptionResponse() {
        // For de-serialization.
    }
    
    public NetBankingOptionResponse(String errorCode, String errorDesc) {
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
    }

    public List<NetBankingOption> getPriorityBanks() {
        return priorityBanks;
    }
    
    public void setPriorityBanks(List<NetBankingOption> priorityBanks) {
        this.priorityBanks = priorityBanks;
    }
    
    public List<NetBankingOption> getBanks() {
        return banks;
    }
    
    public void setBanks(List<NetBankingOption> banks) {
        this.banks = banks;
    }
    
    public String getErrorCode() {
        return errorCode;
    }
    
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    
    public String getErrorDesc() {
        return errorDesc;
    }
    
    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }
}
