package com.freecharge.payment.dos.web;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.freecharge.util.DateSerializer;

public class PaymentTxn {
    
    private Integer paymentTxnId;

    private String orderId;
    
    private Date sentToPg;
    
    private Date recievedFromPg;
    
    private Double amount;
    
    private String merchantTxnId;
    
    private String pgTransactionId;
    
    private Boolean isSuccessful;
    
    private String paymentGateway;
    
    private String underlyingPgUsed;
    
    private String paymentMode;
    
    private Integer paymentType;
    
    private String paymentOption;
    
    private String pgMessage;
    
    private Date pgTransactionTime;

    private String referenceId;

    private String merchantId;
    
    public Integer getPaymentTxnId() {
        return paymentTxnId;
    }

    public void setPaymentTxnId(Integer paymentTxnId) {
        this.paymentTxnId = paymentTxnId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonSerialize(using=DateSerializer.class)
    public Date getSentToPg() {
        return sentToPg;
    }

    public void setSentToPg(Date sentToPg) {
        this.sentToPg = sentToPg;
    }

    @JsonSerialize(using=DateSerializer.class)
    public Date getRecievedFromPg() {
        return recievedFromPg;
    }

    public void setRecievedFromPg(Date recievedFromPg) {
        this.recievedFromPg = recievedFromPg;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMerchantTxnId() {
        return merchantTxnId;
    }

    public void setMerchantTxnId(String merchantTxnId) {
        this.merchantTxnId = merchantTxnId;
    }

    public String getPgTransactionId() {
        return pgTransactionId;
    }

    public void setPgTransactionId(String pgTransactionId) {
        this.pgTransactionId = pgTransactionId;
    }

    public Boolean getIsSuccessful() {
        return isSuccessful;
    }

    public void setIsSuccessful(Boolean isSuccessful) {
        this.isSuccessful = isSuccessful;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getUnderlyingPgUsed() {
        return underlyingPgUsed;
    }

    public void setUnderlyingPgUsed(String underlyingPgUsed) {
        this.underlyingPgUsed = underlyingPgUsed;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentOption() {
        return paymentOption;
    }

    public String getPgMessage() {
        return pgMessage;
    }

    public void setPgMessage(String pgMessage) {
        this.pgMessage = pgMessage;
    }

    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }

    @JsonSerialize(using=DateSerializer.class)
    public Date getPgTransactionTime() {
        return pgTransactionTime;
    }

    public void setPgTransactionTime(Date pgTransactionTime) {
        this.pgTransactionTime = pgTransactionTime;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public String toString() {
        return "PaymentTxn{" +
                "paymentTxnId=" + paymentTxnId +
                ", orderId='" + orderId + '\'' +
                ", sentToPg=" + sentToPg +
                ", recievedFromPg=" + recievedFromPg +
                ", amount=" + amount +
                ", merchantTxnId='" + merchantTxnId + '\'' +
                ", pgTransactionId='" + pgTransactionId + '\'' +
                ", isSuccessful=" + isSuccessful +
                ", paymentGateway='" + paymentGateway + '\'' +
                ", underlyingPgUsed='" + underlyingPgUsed + '\'' +
                ", paymentMode='" + paymentMode + '\'' +
                ", paymentType=" + paymentType +
                ", paymentOption='" + paymentOption + '\'' +
                ", pgMessage='" + pgMessage + '\'' +
                ", pgTransactionTime=" + pgTransactionTime +
                ", referenceId='" + referenceId + '\'' +
                ", merchantId='" + merchantId + '\'' +
                '}';
    }
}
