package com.freecharge.payment.dos.web;

import java.util.Map;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class PaymentRequestWebDO extends BaseWebDO {

    private Map<String,String> requestMap;
    private Map<String, String> resultMap;

    public Map<String, String> getRequestMap() {
        return requestMap;
    }

    public void setRequestMap(Map<String, String> requestMap) {
        this.requestMap = requestMap;
    }

    public Map<String, String> getResultMap() {
        return resultMap;
    }

    public void setResultMap(Map<String, String> resultMap) {
        this.resultMap = resultMap;
    }
}
