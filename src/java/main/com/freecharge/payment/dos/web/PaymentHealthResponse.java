package com.freecharge.payment.dos.web;

import java.util.Map;

public class PaymentHealthResponse {
	
	private String statusCode;
	private String statusMsg;
    private String paymentHealthStatus;
    private Map<String, Object> paymentHealthResponseDetails;
	
    public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	
	public Map<String, Object> getPaymentHealthResponseDetails() {
		return paymentHealthResponseDetails;
	}
	public void setPaymentHealthResponseDetails(Map<String, Object> paymentHealthResponseDetails) {
		this.paymentHealthResponseDetails = paymentHealthResponseDetails;
	}
	public String getPaymentHealthStatus() {
		return paymentHealthStatus;
	}
	public void setPaymentHealthStatus(String paymentHealthStatus) {
		this.paymentHealthStatus = paymentHealthStatus;
	}
	
	
    
}
