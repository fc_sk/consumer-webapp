package com.freecharge.payment.dos.web;

import java.util.Map;

import com.freecharge.common.framework.basedo.BaseWebDO;

public class PaymentResponseWebDO extends BaseWebDO {

    private Map<String,String> requestMap;
    private Map<String,Object> resultMap;
    private String pg;
    private Integer paymentType;

    public Map<String, String> getRequestMap() {
        return requestMap;
    }

    public void setRequestMap(Map<String, String> requestMap) {
        this.requestMap = requestMap;
    }

    public Map<String, Object> getResultMap() {
        return resultMap;
    }

    public void setResultMap(Map<String, Object> resultMap) {
        this.resultMap = resultMap;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }
}
