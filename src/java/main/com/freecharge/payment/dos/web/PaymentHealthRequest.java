package com.freecharge.payment.dos.web;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentHealthRequest {
    private String paymentType;
    private Map<String, String> paymentHealthRequestDetails;

	public String getPaymentType() {	
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Map<String, String> getPaymentHealthRequestDetails() {
		return paymentHealthRequestDetails;
	}

	public void setPaymentHealthRequestDetails(Map<String, String> paymentHealthRequestDetails) {
		this.paymentHealthRequestDetails = paymentHealthRequestDetails;
	}
    
}
