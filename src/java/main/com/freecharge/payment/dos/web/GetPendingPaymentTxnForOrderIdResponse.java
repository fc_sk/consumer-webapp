package com.freecharge.payment.dos.web;

import java.util.List;

public class GetPendingPaymentTxnForOrderIdResponse extends PaymentDetailResponse{

    private List<PaymentTxn> pendingPaymentTxns;
    
    public List<PaymentTxn> getPendingPaymentTxns() {
        return pendingPaymentTxns;
    }
    
    public void setPendingPaymentTxns(List<PaymentTxn> pendingPaymentTxns) {
        this.pendingPaymentTxns = pendingPaymentTxns;
    }
}
