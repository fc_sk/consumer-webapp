package com.freecharge.payment.dos.web;

import java.util.List;

public class GetSuccessfulPaymentTxnForOrderIdResponse extends PaymentDetailResponse{

    private List<PaymentTxn> successfulPaymentTxns;
    
    public List<PaymentTxn> getSuccessfulPaymentTxns() {
        return successfulPaymentTxns;
    }
    
    public void setSuccessfulPaymentTxns(List<PaymentTxn> successfulPaymentTxns) {
        this.successfulPaymentTxns = successfulPaymentTxns;
    }
}
