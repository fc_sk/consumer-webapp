package com.freecharge.payment.dos.web;

import java.util.List;

public class GetFailedPaymentTxnForOrderIdResponse extends PaymentDetailResponse{

    private List<PaymentTxn> failedPaymentTxns;
    
    public List<PaymentTxn> getFailedPaymentTxns() {
        return failedPaymentTxns;
    }
    
    public void setFailedPaymentTxns(List<PaymentTxn> failedPaymentTxns) {
        this.failedPaymentTxns = failedPaymentTxns;
    }
}
