package com.freecharge.payment.dos.web;

import java.io.Serializable;

public class NetBankingOption implements Serializable{

    private static final long serialVersionUID = 1L;

    String bankCode;
    
    String bankName;
    
    public String getBankCode() {
        return bankCode;
    }
    
    public String getBankName() {
        return bankName;
    }
    
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
    
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
