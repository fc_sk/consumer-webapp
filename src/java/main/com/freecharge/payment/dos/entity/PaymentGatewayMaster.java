package com.freecharge.payment.dos.entity;

import java.io.Serializable;

public final class PaymentGatewayMaster implements Serializable {

    private int id;
    private String name;
    private String code;
    private Boolean isActive;
    private String customProperty1;
    private String customProperty2;
    private String customProperty3;


    public PaymentGatewayMaster() {

    }

    public PaymentGatewayMaster(int id, String name, String code,
                                Boolean active, String customProperty1, String customProperty2,
                                String customProperty3) {
        this.id = id;
        this.name = name;
        this.code = code;
        isActive = active;
        this.customProperty1 = customProperty1;
        this.customProperty2 = customProperty2;
        this.customProperty3 = customProperty3;
    }

   
    public int getId() {
        return id;
    }

   
    public String getName() {
        return name;
    }

   
    public String getCode() {
        return code;
    }

   
    public Boolean getActive() {
        return isActive;
    }

   
    public String getCustomProperty1() {
        return customProperty1;
    }

   
    public String getCustomProperty2() {
        return customProperty2;
    }

    
    public String getCustomProperty3() {
        return customProperty3;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public void setCustomProperty1(String customProperty1) {
        this.customProperty1 = customProperty1;
    }

    public void setCustomProperty2(String customProperty2) {
        this.customProperty2 = customProperty2;
    }

    public void setCustomProperty3(String customProperty3) {
        this.customProperty3 = customProperty3;
    }
}
