package com.freecharge.payment.dos.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.freecharge.common.framework.basedo.AbstractDO;

public class AppVersionOrder extends AbstractDO implements Serializable{
    public static final String APP_VERSION_ORDER_ID = "app_version_order_id";
    public static final String ORDER_ID             = "order_id";
    public static final String APP_VERSION          = "app_version";
    public static final String CREATED_TIME         = "created_time";

    private Integer            appVersionOrderId;
    private String             orderId;
    private Integer            appVersion;
    private Timestamp               createdTime;

    public AppVersionOrder(String orderId, Integer appVersion, Timestamp createdTime) {
        this.orderId = orderId;
        this.appVersion = appVersion;
        this.createdTime = createdTime;
    }

    public Integer getAppVersionOrderId() {
        return appVersionOrderId;
    }

    public void setAppVersionOrderId(Integer appVersionOrderId) {
        this.appVersionOrderId = appVersionOrderId;
    }

    public Integer getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(Integer appVersion) {
        this.appVersion = appVersion;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public MapSqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue(ORDER_ID, getOrderId());
        mapSqlParameterSource.addValue(APP_VERSION, getAppVersion());
        mapSqlParameterSource.addValue(CREATED_TIME, getCreatedTime());
        return mapSqlParameterSource;
    }
}
