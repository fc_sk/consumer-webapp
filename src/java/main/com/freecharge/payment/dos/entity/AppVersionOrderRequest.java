package com.freecharge.payment.dos.entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class AppVersionOrderRequest implements Serializable{
	
	private Integer            appVersionOrderId;
	private String             orderId;
    private Integer            appVersion;
    private Timestamp               createdTime;
    
    public Integer getAppVersionOrderId() {
		return appVersionOrderId;
	}
	public void setAppVersionOrderId(Integer appVersionOrderId) {
		this.appVersionOrderId = appVersionOrderId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Integer getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(Integer appVersion) {
		this.appVersion = appVersion;
	}
	public Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	

}
