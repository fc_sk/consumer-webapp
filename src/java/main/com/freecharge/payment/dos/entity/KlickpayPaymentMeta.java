package com.freecharge.payment.dos.entity;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * Created by Vivek on 02/09/16.
 */
public class KlickpayPaymentMeta implements Serializable{

    private int id;
	private String mtxnId;
    private String pgUsed;
    private String referenceId;
    private String metadata;
    private Date createdOn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMtxnId() {
        return mtxnId;
    }

    public void setMtxnId(String mtxnId) {
        this.mtxnId = mtxnId;
    }

    public String getPgUsed() {
        return pgUsed;
    }

    public void setPgUsed(String pgUsed) {
        this.pgUsed = pgUsed;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public SqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("mtxn_id", this.mtxnId);
        sqlParameterSource.addValue("pg_used", this.pgUsed);
        sqlParameterSource.addValue("reference_id", this.referenceId);
        sqlParameterSource.addValue("metadata", this.metadata);
        sqlParameterSource.addValue("created_on", this.createdOn);
        return sqlParameterSource;
    }
    
    @Override
	public String toString() {
		return "KlickpayPaymentMeta [id=" + id + ", mtxnId=" + mtxnId
				+ ", pgUsed=" + pgUsed + ", referenceId=" + referenceId
				+ ", metadata=" + metadata + ", createdOn=" + createdOn + "]";
	}
}