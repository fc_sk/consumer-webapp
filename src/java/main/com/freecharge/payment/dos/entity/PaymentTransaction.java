package com.freecharge.payment.dos.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;
import com.freecharge.common.easydb.UpdateQuery;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.services.IGatewayHandler;
import com.freecharge.payment.util.PaymentConstants;

public class PaymentTransaction implements Serializable, DBEntity {

	private static final long serialVersionUID = 1L;
    @ColumnMapping(name="payment_txn_id")
	private Integer paymentTxnId;
    @ColumnMapping(name="pg_transaction_id")
	private String transactionId;
    @ColumnMapping(name="order_id")
	private String orderId;
    @ColumnMapping(name="sent_to_pg")
	private Date setToPg;
    @ColumnMapping(name="recieved_from_pg")
	private Date recievedToPg;
    @ColumnMapping(name="amount")
	private Double amount;
    @ColumnMapping(name="mtxn_id")
	private String merchantTxnId;
    @ColumnMapping(name="is_successful")
	private Boolean isSuccessful;
    @ColumnMapping(name="payment_gateway")
	private String paymentGateway;
    @ColumnMapping(name="payment_mode")
	private String paymentMode;
    @ColumnMapping(name="pg_message")
	private String pgMessage;
    @ColumnMapping(name="fk_payment_rq_id")
	private Integer paymentRequestId;
    @ColumnMapping(name="fk_payment_rs_id")
	private Integer paymentResponseId;
    @ColumnMapping(name="payment_type")
	private Integer paymentType;
    @ColumnMapping(name="payment_option")
	private String paymentOption;
    @ColumnMapping(name="pg_transaction_time")
    private Date pgTransactionTime;

    private Set<String> whereFields = new HashSet<String>();

	public PaymentTransaction() {
		// TODO Auto-generated constructor stub
	}
	
	public PaymentTransaction(Integer id, String transactionId, String orderId,
    		Timestamp setToPg, Date recievedToPg, Double amount,
    		String merchantTxnId, Boolean successful, String paymentGateway,
    		String paymentMode, String pgMessage, Integer paymentRequestId,
    		Integer paymentResponseId, Integer paymentType, String paymentOption, Date pgTransactionTime) {
		
    		this.paymentTxnId = id;
    		this.transactionId = transactionId;
    		this.orderId = orderId;
    		this.setToPg = setToPg;
    		this.recievedToPg = recievedToPg;
    		this.amount = amount;
    		this.merchantTxnId = merchantTxnId;
    		this.isSuccessful = successful;
    		this.paymentGateway = paymentGateway;
    		this.paymentMode = paymentMode;
    		this.pgMessage = pgMessage;
    		this.paymentRequestId = paymentRequestId;
    		this.paymentResponseId = paymentResponseId;
    		this.paymentType = paymentType;
    		this.paymentOption = paymentOption;
    		this.pgTransactionTime = pgTransactionTime;
    		}

	public Boolean getIsSuccessful() {
		return isSuccessful;
	}

	public void setIsSuccessful(Boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	public Integer getPaymentTxnId() {
		return paymentTxnId;
	}

	public void setPaymentTxnId(Integer paymentTxnId) {
		this.paymentTxnId = paymentTxnId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public String getOrderId() {
		return orderId;
	}

	public Date getSetToPg() {
		return setToPg;
	}

	public Date getRecievedToPg() {
		return recievedToPg;
	}

	public Double getAmount() {
		return amount;
	}

	public String getMerchantTxnId() {
		return merchantTxnId;
	}

	public Boolean getSuccessful() {
		return isSuccessful;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public String getPgMessage() {
		return pgMessage;
	}

	public Integer getPaymentRequestId() {
		return paymentRequestId;
	}

	public Integer getPaymentResponseId() {
		return paymentResponseId;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public void setSetToPg(Date setToPg) {
		this.setToPg = setToPg;
	}

	public void setRecievedToPg(Date recievedToPg) {
		this.recievedToPg = recievedToPg;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setMerchantTxnId(String merchantTxnId) {
		this.merchantTxnId = merchantTxnId;
	}

	public void setSuccessful(Boolean successful) {
		isSuccessful = successful;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public void setPgMessage(String pgMessage) {
		this.pgMessage = pgMessage;
	}

	public void setPaymentRequestId(Integer paymentRequestId) {
		this.paymentRequestId = paymentRequestId;
	}

	public void setPaymentResponseId(Integer paymentResponseId) {
		this.paymentResponseId = paymentResponseId;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

    public Date getPgTransactionTime() {
		return pgTransactionTime;
	}

	public void setPgTransactionTime(Date pgTransactionTime) {
		this.pgTransactionTime = pgTransactionTime;
	}

    public MapSqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("amount", getAmount());
        mapSqlParameterSource.addValue("mtxn_id", getMerchantTxnId());
        mapSqlParameterSource.addValue("order_id", getOrderId());
        mapSqlParameterSource.addValue("payment_gateway", getPaymentGateway());
        mapSqlParameterSource.addValue("payment_mode", getPaymentMode());
        mapSqlParameterSource.addValue("payment_option", getPaymentOption());
        mapSqlParameterSource.addValue("fk_payment_rq_id", getPaymentRequestId());
        mapSqlParameterSource.addValue("payment_type", getPaymentType());
        mapSqlParameterSource.addValue("sent_to_pg", getSetToPg());

        mapSqlParameterSource.addValue("pg_transaction_id", getPaymentTxnId());
        mapSqlParameterSource.addValue("recieved_from_pg", getRecievedToPg());
        mapSqlParameterSource.addValue("is_successful", getSuccessful());
        mapSqlParameterSource.addValue("payment_mode", getPaymentMode());
        mapSqlParameterSource.addValue("pg_message", getPgMessage());
        mapSqlParameterSource.addValue("fk_payment_rs_id", getPaymentResponseId());
        mapSqlParameterSource.addValue("pg_transaction_time", getPgTransactionTime());

        return mapSqlParameterSource;
    }

    @Override
    public String toString() {
        return "PaymentTransaction{" +
                "paymentTxnId=" + paymentTxnId +
                ", transactionId='" + transactionId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", setToPg=" + setToPg +
                ", recievedToPg=" + recievedToPg +
                ", amount=" + amount +
                ", merchantTxnId='" + merchantTxnId + '\'' +
                ", isSuccessful=" + isSuccessful +
                ", paymentGateway='" + paymentGateway + '\'' +
                ", paymentMode='" + paymentMode + '\'' +
                ", pgMessage='" + pgMessage + '\'' +
                ", paymentRequestId=" + paymentRequestId +
                ", paymentResponseId=" + paymentResponseId +
                ", paymentType=" + paymentType +
                ", paymentOption='" + paymentOption + '\'' +
                ", pgTransactionTime='" + pgTransactionTime + '\'' +
                '}';
    }

    public static List<PaymentTransaction> filterOnPaymentGateway(Collection<PaymentTransaction> paymentTransactions, IGatewayHandler.PaymentGateways gateway) {
        String gatewayName = gateway.getName();
        List<PaymentTransaction> gatewayPaymentTransactions = new LinkedList<PaymentTransaction>();

        for (PaymentTransaction paymentTransaction : paymentTransactions) {
            if (gatewayName.equals(paymentTransaction.getPaymentGateway())) {
                gatewayPaymentTransactions.add(paymentTransaction);
            }
        }

        return gatewayPaymentTransactions;
    }

    public static Map<String, List<PaymentTransaction>> groupByPaymentOptionType(List<PaymentTransaction> paymentTransactions) {
        Map<String, List<PaymentTransaction>> map = new HashMap<String, List<PaymentTransaction>>();

        for (PaymentTransaction paymentTransaction : paymentTransactions) {
            String paymentOption = paymentTransaction.getPaymentOption();

            if (!map.containsKey(paymentOption)) {
                List<PaymentTransaction> list = new LinkedList<PaymentTransaction>();
                map.put(paymentOption, list);
            }

            map.get(paymentOption).add(paymentTransaction);
        }

        return map;
    }

    public static Collection<String> getOrderIds(List<PaymentTransaction> paymentTransactions) {
        List<String> orderIds = new LinkedList<String>();
        for (PaymentTransaction paymentTransaction : paymentTransactions) {
            orderIds.add(paymentTransaction.getOrderId());
        }

        return orderIds;
    }

    public static Collection<String> getMerchantOrderIds(List<PaymentTransaction> paymentTransactions) {
        Set<String> merchantOrderIds = new HashSet<String>();

        for (PaymentTransaction paymentTransaction : paymentTransactions) {
            merchantOrderIds.add(paymentTransaction.getMerchantTxnId());
        }

        return merchantOrderIds;
    }

    @Override
    public String getTableName() {
        return "payment_txn";
    }

    @Override
    public UpdateDBVO getUpdateDBVO() {
        UpdateQuery updateQuery = new UpdateQuery(this, this.whereFields);
        return updateQuery.getUpdateDBVO();
    }

    @Override
    public SelectDBVO getSelectDBVO() {
        SelectQuery selectQuery = new SelectQuery(this);
        return selectQuery.getSelectDBVO();
    }

    public static class PaymentTransactionSentToPGComparator implements Comparator<PaymentTransaction> {
        @Override
        public int compare(PaymentTransaction paymentTransaction0, PaymentTransaction paymentTransaction1) {
            return paymentTransaction1.getSetToPg().compareTo(paymentTransaction0.getSetToPg());
        }

        public static void main(String[] args) {
            long currentTime = System.currentTimeMillis();

            PaymentTransaction paymentTransactionBefore = new PaymentTransaction();
            paymentTransactionBefore.setSetToPg(new Date(currentTime));

            PaymentTransaction paymentTransactionAfter = new PaymentTransaction();
            paymentTransactionAfter.setSetToPg(new Date(currentTime + 1000));

            List<PaymentTransaction> paymentTransactions = FCUtil.asList(paymentTransactionAfter, paymentTransactionBefore);

            Collections.sort(paymentTransactions, new PaymentTransactionSentToPGComparator());

            System.out.println(paymentTransactions);
        }
    }

    public void addToWhereFields(String whereField) {
        this.whereFields.add(whereField);
    }
    
    public boolean isWalletPayment() {
        return paymentGateway.equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE)
                || paymentGateway.equals(PaymentConstants.ONECHECK_FC_WALLET_PG_NAME);
    }
    
    public boolean isFullFCWalletPayment() {
        return paymentType.equals(Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET))
                && PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION.equalsIgnoreCase(paymentOption);
    }
    
    public boolean isNewFCWalletPayment() {
        return paymentType.equals(Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET));
    }
    
    public boolean isFCCreditsPaymentTxn() {
        return paymentGateway.equals(PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE);
    }
    
    public boolean isPGPayment() {
        return !paymentType.equals(Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ONECHECK_WALLET))
                && !paymentType.equals(Integer.parseInt(PaymentConstants.PAYMENT_TYPE_WALLET));
    }
}
