package com.freecharge.payment.dos.entity;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.HashMap;
import java.util.Map;

public class KpayPaymentMeta {

    private int id;
    private String mtxnId;
    private String pgUsed;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMtxnId() {
        return mtxnId;
    }

    public void setMtxnId(String mtxnId) {
        this.mtxnId = mtxnId;
    }

    public String getPgUsed() {
        return pgUsed;
    }

    public void setPgUsed(String pgUsed) {
        this.pgUsed = pgUsed;
    }

    public SqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("mtxn_id", this.mtxnId);
        sqlParameterSource.addValue("pg_used", this.pgUsed);
        return sqlParameterSource;
    }
}
