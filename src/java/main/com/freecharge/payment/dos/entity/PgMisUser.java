package com.freecharge.payment.dos.entity;



public class PgMisUser {
	private Integer pgUserID;
	private String  pgUserName;
	private String  pgUserPassword;
	private Boolean pgUserActiveStatus;
    private String returnUrl;

    public PgMisUser() {
	}

	public Integer getPgUserID() {
		return pgUserID;
	}

	public void setPgUserID(Integer pgUserID) {
		this.pgUserID = pgUserID;
	}

	public String getPgUserName() {
		return pgUserName;
	}

	public void setPgUserName(String pgUserName) {
		this.pgUserName = pgUserName;
	}

	public String getPgUserPassword() {
		return pgUserPassword;
	}

	public void setPgUserPassword(String pgUserPassword) {
		this.pgUserPassword = pgUserPassword;
	}

	public Boolean getPgUserActiveStatus() {
		return pgUserActiveStatus;
	}

	public void setPgUserActiveStatus(Boolean pgUserActiveStatus) {
		this.pgUserActiveStatus = pgUserActiveStatus;
	}

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }
}
