package com.freecharge.payment.dos.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pg_relation")
public class PgRelation implements Serializable {

    private Integer id;
    private Integer circleId;
    private Integer inId;
    private Integer productId;
    private Integer affiliateId;
    private Integer bankMasterId;
    private Boolean isActive;
    private Integer primaryGateway;
    private Integer secondaryGateway;

    public PgRelation() {
    }

    public PgRelation(Integer id, Integer circleId, Integer inId, Integer productId,
                      Integer affiliateId, Integer bankMasterId, Boolean active,
                      Integer primaryGateway, Integer secondaryGateway) {
        this.id = id;
        this.circleId = circleId;
        this.inId = inId;
        this.productId = productId;
        this.affiliateId = affiliateId;
        this.bankMasterId = bankMasterId;
        isActive = active;
        this.primaryGateway = primaryGateway;
        this.secondaryGateway = secondaryGateway;
    }

    @Id
    @GeneratedValue
    @Column(name = "pg_relation_id")
    public Integer getId() {
        return id;
    }

    @Column(name = "fk_circle_master_id")
    public Integer getCircleId() {
        return circleId;
    }

    @Column(name = "fk_in_master")
    public Integer getInId() {
        return inId;
    }

    @Column(name = "fk_product_master_id")
    public Integer getProductId() {
        return productId;
    }

    @Column(name = "fk_affiliate_profile_id")
    public Integer getAffiliateId() {
        return affiliateId;
    }

    @Column(name = "fk_banks_grp_master_id")
    public Integer getBankMasterId() {
        return bankMasterId;
    }

    @Column(name = "is_active")
    public Boolean getActive() {
        return isActive;
    }

    @Column(name = "primary_gateway")
    public Integer getPrimaryGateway() {
        return primaryGateway;
    }

    @Column(name = "secondary_gateway")
    public Integer getSecondaryGateway() {
        return secondaryGateway;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public void setAffiliateId(Integer affiliateId) {
        this.affiliateId = affiliateId;
    }

    public void setBankMasterId(Integer bankMasterId) {
        this.bankMasterId = bankMasterId;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public void setPrimaryGateway(Integer primaryGateway) {
        this.primaryGateway = primaryGateway;
    }

    public void setSecondaryGateway(Integer secondaryGateway) {
        this.secondaryGateway = secondaryGateway;
    }
}
