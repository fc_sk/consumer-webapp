package com.freecharge.payment.dos.entity;

import java.io.Serializable;

public class BankMaster implements Serializable {

	private Integer id;
	private String name;
	private String codeUI;
	private String bankCode;
	private Boolean isActive;
	private Integer paymentOption;
	private String displayName;

	public BankMaster() {
		/* default constructor */
	}

	public BankMaster(Integer id, String name, String codeUI, String bankCode, Boolean active, Integer paymentOption, String displayName) {
		/* full constructor */
		this.id = id;
		this.name = name;
		this.codeUI = codeUI;
		this.bankCode = bankCode;
		isActive = active;
		this.paymentOption = paymentOption;
		this.displayName = displayName;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCodeUI() {
		return codeUI;
	}

	public String getBankCode() {
		return bankCode;
	}

	public Boolean getActive() {
		return isActive;
	}

	public Integer getPaymentOption() {
		return paymentOption;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCodeUI(String codeUI) {
		this.codeUI = codeUI;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public void setActive(Boolean active) {
		isActive = active;
	}

	public void setPaymentOption(Integer paymentOption) {
		this.paymentOption = paymentOption;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
