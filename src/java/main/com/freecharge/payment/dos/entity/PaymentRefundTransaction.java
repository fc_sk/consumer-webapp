package com.freecharge.payment.dos.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.freecharge.common.easydb.ColumnMapping;
import com.freecharge.common.easydb.CustomDataType;
import com.freecharge.common.easydb.DBEntity;
import com.freecharge.common.easydb.SelectDBVO;
import com.freecharge.common.easydb.SelectQuery;
import com.freecharge.common.easydb.UpdateDBVO;
import com.freecharge.common.easydb.UpdateQuery;
import com.freecharge.common.util.Amount;

/**
 * Created with IntelliJ IDEA. User: abhi Date: 5/10/12 Time: 4:26 PM To change
 * this template use File | Settings | File Templates.
 */
public class PaymentRefundTransaction implements DBEntity {
	@ColumnMapping(name = "payment_refund_transaction_id")
	private Integer paymentRefundTransactionId;
	@ColumnMapping(name = "pg_transaction_reference_no")
	private String pgTransactionReferenceNo;
	@ColumnMapping(name = "fk_payment_txn_id")
	private Integer paymentTransactionId;
	@ColumnMapping(name = "sent_to_pg")
	private Date sentToPG;
	@ColumnMapping(name = "received_from_pg")
	private Date receivedFromPG;
	@CustomDataType(dataType = Amount.class, getter = "getAmount")
	@ColumnMapping(name = "refunded_amount")
	private Amount refundedAmount;
	@ColumnMapping(name = "raw_pg_request")
	private String rawPGRequest;
	@ColumnMapping(name = "raw_pg_response")
	private String rawPGResponse;
	@ColumnMapping(name = "pg_response_code")
	private String pgResponseCode;
	@ColumnMapping(name = "pg_response_description")
	private String pgResponseDescription;
	@ColumnMapping(name = "status")
	@CustomDataType(dataType = Status.class, getter = "getStatusString")
	private Status status;
	@ColumnMapping(name = "pg_status")
	private String pgStatus;
	@ColumnMapping(name = "refund_to")
	private String refundTo;

	private PaymentTransaction paymentTransaction;

	private Collection<String> whereFields = new LinkedList<String>();

	private static final String TABLE_NAME = "payment_refund_transactions";

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

	@Override
	public UpdateDBVO getUpdateDBVO() {
		UpdateQuery updateQuery = new UpdateQuery(this, this.whereFields);
		return updateQuery.getUpdateDBVO();
	}

	@Override
	public SelectDBVO getSelectDBVO() {
		SelectQuery selectQuery = new SelectQuery(this);
		return selectQuery.getSelectDBVO();
	}

	public enum Status {
		REQUESTED("requested"), FAILURE("failure"), SUCCESS("success"), INITIATED(
				"initiated");

		private String statusString;

		private Status(String statusString) {
			this.statusString = statusString;
		}

		public String getStatusString() {
			return statusString;
		}

		public void setStatusString(String statusString) {
			this.statusString = statusString;
		}

		public static Status get(String value) {
			for (Status status : values()) {
				if (value.equals(status.getStatusString())) {
					return status;
				}
			}
			return null;
		}

		@Override
		public String toString() {
			return statusString;
		}
	}

	public Integer getPaymentRefundTransactionId() {
		return paymentRefundTransactionId;
	}

	public void setPaymentRefundTransactionId(Integer paymentRefundTransactionId) {
		this.paymentRefundTransactionId = paymentRefundTransactionId;
	}

	public String getPgTransactionReferenceNo() {
		return pgTransactionReferenceNo;
	}

	public void setPgTransactionReferenceNo(String pgTransactionReferenceNo) {
		this.pgTransactionReferenceNo = pgTransactionReferenceNo;
	}

	public Integer getPaymentTransactionId() {
		return paymentTransactionId;
	}

	public void setPaymentTransactionId(Integer paymentTransactionId) {
		this.paymentTransactionId = paymentTransactionId;
	}

	public Date getSentToPG() {
		return sentToPG;
	}

	public void setSentToPG(Date sentToPG) {
		this.sentToPG = sentToPG;
	}

	public Date getReceivedFromPG() {
		return receivedFromPG;
	}

	public void setReceivedFromPG(Date receivedFromPG) {
		this.receivedFromPG = receivedFromPG;
	}

	public Amount getRefundedAmount() {
		return refundedAmount;
	}

	public void setRefundedAmount(Amount refundedAmount) {
		this.refundedAmount = refundedAmount;
	}

	public String getRawPGRequest() {
		return rawPGRequest;
	}

	public void setRawPGRequest(String rawPGRequest) {
		this.rawPGRequest = rawPGRequest;
	}

	public String getRawPGResponse() {
		return rawPGResponse;
	}

	public void setRawPGResponse(String rawPGResponse) {
		this.rawPGResponse = rawPGResponse;
	}

	public String getPgResponseCode() {
		return pgResponseCode;
	}

	public void setPgResponseCode(String pgResponseCode) {
		this.pgResponseCode = pgResponseCode;
	}

	public String getPgResponseDescription() {
		return pgResponseDescription;
	}

	public void setPgResponseDescription(String pgResponseDescription) {
		this.pgResponseDescription = pgResponseDescription;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getPgStatus() {
		return pgStatus;
	}

	public void setPgStatus(String pgStatus) {
		this.pgStatus = pgStatus;
	}

	public String getRefundTo() {
		return refundTo;
	}

	public void setRefundTo(String refundTo) {
		this.refundTo = refundTo;
	}

	public PaymentTransaction getPaymentTransaction() {
		return paymentTransaction;
	}

	public void setPaymentTransaction(PaymentTransaction paymentTransaction) {
		this.paymentTransaction = paymentTransaction;
	}

	public MapSqlParameterSource getMapSqlParameterSource() {
		/*
		 * payment_refund_transaction_id int(11) NOT NULL AUTO_INCREMENT,
		 * pg_transaction_reference_no varchar(255), fk_payment_txn_id int(11)
		 * NOT NULL, sent_to_pg datetime NOT NULL, received_from_pg datetime,
		 * refunded_amount decimal(10, 2), raw_pg_request text NOT NULL,
		 * raw_pg_response text, pg_response_code varchar(255),
		 * pg_response_description varchar(255), status varchar(255) NOT NULL,
		 * pg_status varchar(255), PRIMARY KEY (payment_refund_transaction_id)
		 */
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("pg_transaction_reference_no",
				pgTransactionReferenceNo);
		mapSqlParameterSource.addValue("fk_payment_txn_id",
				paymentTransactionId);
		mapSqlParameterSource.addValue("pg_transaction_reference_no",
				pgTransactionReferenceNo);
		mapSqlParameterSource.addValue("sent_to_pg", sentToPG);
		mapSqlParameterSource.addValue("received_from_pg", receivedFromPG);
		mapSqlParameterSource.addValue("refunded_amount",
				refundedAmount.getAmount());
		mapSqlParameterSource.addValue("raw_pg_request", rawPGRequest);
		mapSqlParameterSource.addValue("raw_pg_response", rawPGResponse);
		mapSqlParameterSource.addValue("pg_response_code", pgResponseCode);
		mapSqlParameterSource.addValue("pg_response_description",
				pgResponseDescription);
		mapSqlParameterSource.addValue("status", status.getStatusString());
		mapSqlParameterSource.addValue("pg_status", pgStatus);
		mapSqlParameterSource.addValue("refund_to", refundTo);
		return mapSqlParameterSource;
	}

	public void addToWhereFields(String whereField) {
		this.whereFields.add(whereField);
	}

	public static class PaymentRefundTransactionMapper implements
			RowMapper<PaymentRefundTransaction> {

		@Override
		public PaymentRefundTransaction mapRow(ResultSet resultSet, int i)
				throws SQLException {
			PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
			paymentRefundTransaction.setPaymentRefundTransactionId(resultSet
					.getInt("payment_refund_transaction_id"));
			paymentRefundTransaction.setPgTransactionReferenceNo(resultSet
					.getString("pg_transaction_reference_no"));
			paymentRefundTransaction.setPaymentTransactionId(resultSet
					.getInt("fk_payment_txn_id"));
			paymentRefundTransaction.setSentToPG(resultSet
					.getDate("sent_to_pg"));
			paymentRefundTransaction.setReceivedFromPG(resultSet
					.getDate("received_from_pg"));
			paymentRefundTransaction.setRefundedAmount(new Amount(resultSet
					.getBigDecimal("refunded_amount")));
			paymentRefundTransaction.setRawPGRequest(resultSet
					.getString("raw_pg_request"));
			paymentRefundTransaction.setRawPGResponse(resultSet
					.getString("raw_pg_response"));
			paymentRefundTransaction.setPgResponseCode(resultSet
					.getString("pg_response_code"));
			paymentRefundTransaction.setPgResponseDescription(resultSet
					.getString("pg_response_description"));
			paymentRefundTransaction.setStatus(Status.get(resultSet
					.getString("status")));
			paymentRefundTransaction.setPgStatus(resultSet
					.getString("pg_status"));
			paymentRefundTransaction.setRefundTo(resultSet
					.getString("refund_to"));
			return paymentRefundTransaction;
		}
	}

	@Override
	public String toString() {
		return "PaymentRefundTransaction{" + "paymentRefundTransactionId="
				+ paymentRefundTransactionId + ", pgTransactionReferenceNo='"
				+ pgTransactionReferenceNo + '\'' + ", paymentTransactionId="
				+ paymentTransactionId + ", sentToPG=" + sentToPG
				+ ", receivedFromPG=" + receivedFromPG + ", refundedAmount="
				+ refundedAmount.getAmount() + ", rawPGRequest='"
				+ rawPGRequest + '\'' + ", rawPGResponse='" + rawPGResponse
				+ '\'' + ", pgResponseCode='" + pgResponseCode + '\''
				+ ", pgResponseDescription='" + pgResponseDescription + '\''
				+ ", status=" + status + ", pgStatus='" + pgStatus + '\''
				+ ", refundTo='" + refundTo + '\'' + ", paymentTransaction="
				+ paymentTransaction + ", whereFields=" + whereFields + '}';
	}
}
