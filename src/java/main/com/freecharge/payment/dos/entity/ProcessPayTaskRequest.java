package com.freecharge.payment.dos.entity;

import com.freecharge.order.checkout.request.OndeckProcessOrderRequest;
import com.snapdeal.payments.ts.registration.TaskRequest;

public class ProcessPayTaskRequest implements TaskRequest {

	private String taskId;

	private String lookupId;
	
	private OndeckProcessOrderRequest ondeckProcessOrderRequest;

	public String getLookupId() {
		return lookupId;
	}

	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}

	@Override
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public OndeckProcessOrderRequest getOndeckProcessOrderRequest() {
		return ondeckProcessOrderRequest;
	}

	public void setOndeckProcessOrderRequest(OndeckProcessOrderRequest ondeckProcessOrderRequest) {
		this.ondeckProcessOrderRequest = ondeckProcessOrderRequest;
	}

	
}
