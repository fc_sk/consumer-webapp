package com.freecharge.payment.dos.entity;

import java.math.BigDecimal;

import com.freecharge.common.util.Amount;

public class PaymentPlan {
    private String planReferenceId;
    private String mtxnId;
    private Amount pgAmount = new Amount(new BigDecimal("0"));
    private Amount walletAmount = new Amount(new BigDecimal("0"));
    
    public String getMtxnId() {
        return mtxnId;
    }

    public void setMtxnId(String mtxnId) {
        this.mtxnId = mtxnId;
    }
    
    public Amount getPgAmount() {
        return pgAmount;
    }
    
    public void setPgAmount(Amount pgAmount) {
        this.pgAmount = pgAmount;
    }
    
    public String getPlanReferenceId() {
        return planReferenceId;
    }
    
    public void setPlanReferenceId(String planReferenceId) {
        this.planReferenceId = planReferenceId;
    }
    
    public Amount getWalletAmount() {
        return walletAmount;
    }
    
    public void setWalletAmount(Amount walletAmount) {
        this.walletAmount = walletAmount;
    }
    
    public Amount getTotalAmount() {
        return new Amount(getPgAmount().getAmount().doubleValue() + getWalletAmount().getAmount().doubleValue());
    }
    
    public boolean isPartialPayment() {
        return this.getWalletAmount().compareTo(Amount.ZERO) > 0 && this.getPgAmount().compareTo(Amount.ZERO) > 0;
    }
    
    @Override
    public String toString() {
        return "[PlanReference:" + planReferenceId + "mtxnId: " + mtxnId + ", WalletAmount: " + this.getWalletAmount() + ", PGAmount: " + this.getPgAmount() + ", TotalAmount: "+this.getTotalAmount().getAmount().doubleValue() +"]" ;
    }
}
