package com.freecharge.payment.dos.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.freecharge.sns.bean.ReconMetaData;

public class PaymentResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer paymentRsId;
	private Boolean isSuccessful;
	private String pgTransactionId;
	private String orderId;
	private String mtxnId;
	private String responseData;
	private Double amount;
	private String paymentGateway;
	private String pgMessage;
	private String rrnNo;
	private String authCodeNo;
	private String avsCode;
	private String eci;
	private String cscResponseCode;
	private String responseFromPp;
	
	private Double cashbackAmount;
	private String cashbackSource;
	private String cashbackReferrence;
	
	private Timestamp pgTransactionTime;
	private Timestamp createdOn;

	private String status;
	private ReconMetaData reconMetaData;
	private String upiAccountDetails;
	
	public PaymentResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public PaymentResponse(Integer id, Boolean successful, String transactionId, String orderId, String merchantTxnId, String rawResponseFromPg, Double amount,
                           String paymentGateway, String message, String rrNumber, String authCode, String avsCode, String eci, String cscResponse,
                           String rawResponseToProduct, Timestamp pgTransactionTime,Timestamp createdOn) {
        this.paymentRsId = id;
		this.isSuccessful = successful;
		this.pgTransactionId = transactionId;
		this.orderId = orderId;
		this.mtxnId = merchantTxnId;
		this.responseData = rawResponseFromPg;
		this.amount = amount;
		this.paymentGateway = paymentGateway;
		this.pgMessage = message;
		this.rrnNo = rrNumber;
		this.authCodeNo = authCode;
		this.avsCode = avsCode;
		this.eci = eci;
		this.cscResponseCode = cscResponse;
		this.responseFromPp = rawResponseToProduct;
		this.pgTransactionTime = pgTransactionTime;
		this.createdOn = createdOn;
	}

	public Integer getPaymentRsId() {
		return paymentRsId;
	}

	public void setPaymentRsId(Integer paymentRsId) {
		this.paymentRsId = paymentRsId;
	}

	public String getUpiAccountDetails() {
        return this.upiAccountDetails;
    }

    public void setUpiAccountDetails(String upiAccountDetails) {
        this.upiAccountDetails = upiAccountDetails;
    }

    public Boolean getIsSuccessful() {
		return isSuccessful;
	}

	public void setIsSuccessful(Boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	public String getPgTransactionId() {
		return pgTransactionId;
	}

	public void setPgTransactionId(String pgTransactionId) {
		this.pgTransactionId = pgTransactionId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMtxnId() {
		return mtxnId;
	}

	public void setMtxnId(String mtxnId) {
		this.mtxnId = mtxnId;
	}

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public String getPgMessage() {
		return pgMessage;
	}

	public void setPgMessage(String pgMessage) {
		this.pgMessage = pgMessage;
	}

	public String getRrnNo() {
		return rrnNo;
	}

	public void setRrnNo(String rrnNo) {
		this.rrnNo = rrnNo;
	}

	public String getAuthCodeNo() {
		return authCodeNo;
	}

	public void setAuthCodeNo(String authCodeNo) {
		this.authCodeNo = authCodeNo;
	}

	public String getAvsCode() {
		return avsCode;
	}

	public void setAvsCode(String avsCode) {
		this.avsCode = avsCode;
	}

	public String getEci() {
		return eci;
	}

	public void setEci(String eci) {
		this.eci = eci;
	}

	public String getCscResponseCode() {
		return cscResponseCode;
	}

	public void setCscResponseCode(String cscResponseCode) {
		this.cscResponseCode = cscResponseCode;
	}

	public String getResponseFromPp() {
		return responseFromPp;
	}

	public void setResponseFromPp(String responseFromPp) {
		this.responseFromPp = responseFromPp;
	}

	public Double getCashbackAmount() {
		return cashbackAmount;
	}

	public void setCashbackAmount(Double cashbackAmount) {
		this.cashbackAmount = cashbackAmount;
	}

	public String getCashbackSource() {
		return cashbackSource;
	}

	public void setCashbackSource(String cashbackSource) {
		this.cashbackSource = cashbackSource;
	}

	public String getCashbackReferrence() {
		return cashbackReferrence;
	}

	public void setCashbackReferrence(String cashbackReferrence) {
		this.cashbackReferrence = cashbackReferrence;
	}

	public Timestamp getPgTransactionTime() {
		return pgTransactionTime;
	}

	public void setPgTransactionTime(Timestamp pgTransactionTime) {
		this.pgTransactionTime = pgTransactionTime;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentResponse that = (PaymentResponse) o;

        if (paymentRsId != null ? !paymentRsId.equals(that.paymentRsId) : that.paymentRsId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return paymentRsId != null ? paymentRsId.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "PaymentResponse{" +
                "paymentRsId=" + paymentRsId +
                ", isSuccessful=" + isSuccessful +
                ", pgTransactionId='" + pgTransactionId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", mtxnId='" + mtxnId + '\'' +
                ", responseData='" + responseData + '\'' +
                ", amount=" + amount +
                ", paymentGateway='" + paymentGateway + '\'' +
                ", pgMessage='" + pgMessage + '\'' +
                ", rrnNo='" + rrnNo + '\'' +
                ", authCodeNo='" + authCodeNo + '\'' +
                ", avsCode='" + avsCode + '\'' +
                ", eci='" + eci + '\'' +
                ", cscResponseCode='" + cscResponseCode + '\'' +
                ", responseFromPp='" + responseFromPp + '\'' +
                ", pgTransactionTime='" + pgTransactionTime + '\'' +
                ", createdOn=" + createdOn +
                '}';
    }

	public ReconMetaData getReconMetaData() {
		return reconMetaData;
	}

	public void setReconMetaData(ReconMetaData reconMetaData) {
		this.reconMetaData = reconMetaData;
	}
}
