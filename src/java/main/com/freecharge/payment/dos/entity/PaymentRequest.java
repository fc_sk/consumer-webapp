package com.freecharge.payment.dos.entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class PaymentRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer paymentRqId;
	private String orderId;
	private String requestToPp;
	private Double amount;
	private String paymentMode;
	private String paymentGateway;
	private String paymentType;
	private String mtxnId;
	private String requestToGateway;
	private String affiliateCode;
	private Timestamp createdOn;
	private String fkBbanksGrpMasterId;

	public Integer getPaymentRqId() {
		return paymentRqId;
	}

	public void setPaymentRqId(Integer paymentRqId) {
		this.paymentRqId = paymentRqId;
	}

	public String getOrderId() {
		return orderId;
	}

	public Double getAmount() {
		return amount;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public String getRequestToGateway() {
		return requestToGateway;
	}

	public String getAffiliateCode() {
		return affiliateCode;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public void setRequestToGateway(String requestToGateway) {
		this.requestToGateway = requestToGateway;
	}

	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getRequestToPp() {
		return requestToPp;
	}

	public void setRequestToPp(String requestToPp) {
		this.requestToPp = requestToPp;
	}

	public String getMtxnId() {
		return mtxnId;
	}

	public void setMtxnId(String mtxnId) {
		this.mtxnId = mtxnId;
	}

	public String getFkBbanksGrpMasterId() {
		return fkBbanksGrpMasterId;
	}

	public void setFkBbanksGrpMasterId(String fkBbanksGrpMasterId) {
		this.fkBbanksGrpMasterId = fkBbanksGrpMasterId;
	}

    @Override
    public String toString() {
        return "PaymentRequest{" +
                "paymentRqId=" + paymentRqId +
                ", orderId='" + orderId + '\'' +
                ", requestToPp='" + requestToPp + '\'' +
                ", amount=" + amount +
                ", paymentMode='" + paymentMode + '\'' +
                ", paymentGateway='" + paymentGateway + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", mtxnId='" + mtxnId + '\'' +
                ", requestToGateway='" + requestToGateway + '\'' +
                ", affiliateCode='" + affiliateCode + '\'' +
                ", createdOn=" + createdOn +
                ", fkBbanksGrpMasterId='" + fkBbanksGrpMasterId + '\'' +
                '}';
    }
}
