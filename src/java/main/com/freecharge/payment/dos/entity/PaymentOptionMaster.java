package com.freecharge.payment.dos.entity;

public class PaymentOptionMaster {

    private Integer id;
    private String paymentType;
    private String paymentOptionDisplayName;
    private String paymentOptionCode;
    private String imageUrl;
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getPaymentType() {
        return paymentType;
    }
    
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
    
    public String getPaymentOptionDisplayName() {
        return paymentOptionDisplayName;
    }
    
    public void setPaymentOptionDisplayName(String paymentOptionDisplayName) {
        this.paymentOptionDisplayName = paymentOptionDisplayName;
    }
    
    public String getPaymentOptionCode() {
        return paymentOptionCode;
    }
    
    public void setPaymentOptionCode(String paymentOptionCode) {
        this.paymentOptionCode = paymentOptionCode;
    }
    
    public String getImageUrl() {
        return imageUrl;
    }
    
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
