package com.freecharge.payment.dos.entity;

import java.util.List;

public class FeatureCheckInput {

	private List<String> featureNameList;
	private String email;
	private String merchantCode;
	
	public List<String> getFeatureNameList() {
		return featureNameList;
	}
	public void setFeatureNameList(List<String> featureName) {
		this.featureNameList = featureName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	@Override
	public String toString() {
		return "FeatureCheckInput [featureName=" + featureNameList + ", email=" + email + ", merchantCode=" + merchantCode
				+ "]";
	}
}
