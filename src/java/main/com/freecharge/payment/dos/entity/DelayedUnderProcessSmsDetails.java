package com.freecharge.payment.dos.entity;

import java.io.Serializable;

import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.comm.sms.SendSmsRequest;

public class DelayedUnderProcessSmsDetails implements Serializable{
	
	private SendSmsRequest sendSmsRequest;
	private UserDetails userDetails;
	
	public SendSmsRequest getSendSmsRequest() {
		return sendSmsRequest;
	}
	public void setSendSmsRequest(SendSmsRequest sendSmsRequest) {
		this.sendSmsRequest = sendSmsRequest;
	}
	public UserDetails getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}
	

}
