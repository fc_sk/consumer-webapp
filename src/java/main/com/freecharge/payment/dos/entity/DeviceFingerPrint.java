package com.freecharge.payment.dos.entity;

import com.freecharge.common.framework.basedo.AbstractDO;

public class DeviceFingerPrint extends AbstractDO{
    private String deviceFingerPrint;
    private String userId;
    
    public String getDeviceFingerPrint() {
        return deviceFingerPrint;
    }

    public void setDeviceFingerPrint(String deviceFingerPrint) {
        this.deviceFingerPrint = deviceFingerPrint;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    
}
