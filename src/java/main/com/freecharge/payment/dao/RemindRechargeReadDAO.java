package com.freecharge.payment.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.payment.dao.RemindRechargeDAO.RemindRechargeRowMapper;
import com.freecharge.rest.recharge.RemindRecharge;

@Component
public class RemindRechargeReadDAO {
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource jdbcReadDataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbcReadDataSource);
    }

    public List<RemindRecharge> getReminderForDay() {
        String queryString = "SELECT * FROM user_recharge_reminder WHERE DATE(reminder_date)=:reminderDate and is_active=1";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Map<String, Object> params = new HashMap<>();
        params.put("reminderDate", dateFormat.format(new Date()));

        List<RemindRecharge> reminderList = jdbcTemplate.query(queryString,
                params, new RemindRechargeRowMapper());
        return reminderList;
    }
}
