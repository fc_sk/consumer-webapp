package com.freecharge.payment.dao.mappers;

import com.freecharge.payment.dos.entity.KlickpayPaymentMeta;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Vivek on 02/09/16.
 */
public class KlickpayPaymentMetaRowMapper implements RowMapper<KlickpayPaymentMeta> {
    @Override
    public KlickpayPaymentMeta mapRow(ResultSet resultSet, int row) throws SQLException {
        KlickpayPaymentMeta klickpayPaymentMeta = new KlickpayPaymentMeta();
        klickpayPaymentMeta.setId(resultSet.getInt("id"));
        klickpayPaymentMeta.setMtxnId(resultSet.getString("mtxn_id"));
        klickpayPaymentMeta.setPgUsed(resultSet.getString("pg_used"));
        klickpayPaymentMeta.setReferenceId(resultSet.getString("reference_id"));
        klickpayPaymentMeta.setMetadata(resultSet.getString("metadata"));
        klickpayPaymentMeta.setCreatedOn(resultSet.getDate("created_on"));
        return klickpayPaymentMeta;
    }
}
