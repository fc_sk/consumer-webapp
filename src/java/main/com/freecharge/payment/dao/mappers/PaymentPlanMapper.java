package com.freecharge.payment.dao.mappers;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.common.util.Amount;
import com.freecharge.payment.dos.entity.PaymentPlan;

public class PaymentPlanMapper implements RowMapper<PaymentPlan> {

    @Override
    public PaymentPlan mapRow(ResultSet rs, int row) throws SQLException {
        PaymentPlan paymentPlan = new PaymentPlan();
        
        BigDecimal pgAmount = rs.getBigDecimal("pg_amount");
        BigDecimal walletAmount = rs.getBigDecimal("wallet_amount");
        
        paymentPlan.setPlanReferenceId(rs.getString("plan_reference"));
        paymentPlan.setPgAmount(new Amount(pgAmount));
        paymentPlan.setWalletAmount(new Amount(walletAmount));
        
        return paymentPlan;
    }
}
