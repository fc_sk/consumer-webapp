package com.freecharge.payment.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.payment.services.binrange.CardBinRange;

public class CardBinRangeMapper implements RowMapper<CardBinRange>{
    @Override
    public CardBinRange mapRow(ResultSet rs, int row) throws SQLException {
        CardBinRange issuer = new CardBinRange();

        issuer.setId(rs.getString("id"));
        issuer.setBinRangeId(rs.getString("bin_range_id"));
        issuer.setBinHead(rs.getString("bin_head"));
        issuer.setBinTail(rs.getString("bin_tail"));
        issuer.setCardType(rs.getString("card_type"));
        issuer.setCardNature(rs.getString("card_nature"));
        issuer.setCardIssuerId(rs.getString("fk_card_issuer_id"));
        issuer.setStatus(rs.getString("status"));
        issuer.setCreatedOn(rs.getString("created_on"));
        issuer.setCardCategoryId(rs.getString("fk_card_category_id"));
        return issuer;
    }
}
