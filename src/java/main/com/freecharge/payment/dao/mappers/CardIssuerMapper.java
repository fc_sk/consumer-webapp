package com.freecharge.payment.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.payment.services.binrange.CardIssuer;

public class CardIssuerMapper implements RowMapper<CardIssuer> {

    @Override
    public CardIssuer mapRow(ResultSet rs, int row) throws SQLException {
        CardIssuer issuer = new CardIssuer();

        issuer.setId(rs.getInt("id"));
        issuer.setIssuerId(rs.getString("card_issuer_id"));
        issuer.setIssuerName(rs.getString("card_issuer_name"));
        issuer.setCardNature(rs.getString("card_nature"));
        issuer.setCardType(rs.getString("card_type"));
        
        return issuer;
    }

}
