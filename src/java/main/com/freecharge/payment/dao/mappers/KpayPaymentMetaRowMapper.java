package com.freecharge.payment.dao.mappers;

import com.freecharge.payment.dos.entity.KpayPaymentMeta;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class KpayPaymentMetaRowMapper implements RowMapper<KpayPaymentMeta> {
    @Override
    public KpayPaymentMeta mapRow(ResultSet resultSet, int row) throws SQLException {
        KpayPaymentMeta kpayPaymentMeta = new KpayPaymentMeta();
        kpayPaymentMeta.setId(resultSet.getInt("id"));
        kpayPaymentMeta.setMtxnId(resultSet.getString("mtxn_id"));
        kpayPaymentMeta.setPgUsed(resultSet.getString("pg_used"));
        return kpayPaymentMeta;
    }
}
