package com.freecharge.payment.dao;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

public class PgExcelReportView  extends AbstractView {
    
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			response.reset();
			response.setContentType("text/csv");
			response.setHeader("Content-disposition","attachment; filename=Report.csv");
			ServletOutputStream out = response.getOutputStream();
			Writer writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(out)), true);
			List<PgMisDisplayRecord> csvData = (List<PgMisDisplayRecord>) model.get("excelData");
			writer.write("Txn ID");
			writer.write(",");
			writer.write("Merchant ID");
	        	writer.write(",");
			writer.write("Request received Time");
			writer.write(",");
			writer.write("Payment Mode");
			writer.write(",");
			writer.write("Amount");
			writer.write(",");
			writer.write("Order ID");
			writer.write(",");
			writer.write("Payment Gateway");
			writer.write(",");
			writer.write("Payment Type");
			writer.write(",");
			writer.write("Payment Option");
			writer.write(",");
			writer.write("Payment Gateway Message");
			writer.write(",");
			writer.write("Sent to PG");
			writer.write(",");
			writer.write("Received from PG");
			writer.write(",");
			writer.write("PG Txn ID");
			writer.write(",");
			writer.write("Txn Status");
			writer.write(",");
			writer.write("FreeFundName");
			writer.write(",");
			writer.write("FreeFundPrice(-)");
			writer.write(",");
			writer.write("FreeFundCode");
			writer.write(",");
			writer.write("CrossSellName_1");
			writer.write(",");
			writer.write("CrossSellAmount_1");
			writer.write(",");
			writer.write("CrossSellName_2");
			writer.write(",");
			writer.write("CrossSellAmount_2");
			writer.write(",");
			writer.write("CrossSellName_3");
			writer.write(",");
			writer.write("CrossSellAmount_3");
			writer.write(",");
			writer.write("CrossSellName_4");
			writer.write(",");
			writer.write("CrossSellAmount_4");
            writer.write(",");
			writer.write("Wallet ID");
            writer.write(",");
			writer.write("User ID");
            writer.write(",");
			writer.write("Fund Source");
            writer.write(",");
			writer.write("Aggregator Name");
            writer.write(",");
			writer.write("Aggregator Reference");
            writer.write(",");
			writer.write("Operator");
            writer.write(",");
			writer.write("Circle");
            writer.write(",");
			writer.write("Amount");
            writer.write(",");
			writer.write("Channel ID");
			writer.write("\n");
			if (csvData != null) {
				for (int i = 0; i < csvData.size(); i++) {
					PgMisDisplayRecord displayRecord = (PgMisDisplayRecord) csvData
							.get(i);
					writer.write(displayRecord.getTxnId().toString());
					writer.write(",");
					writer.write(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getMtxnId());
					writer.write(",");
					writer.write(displayRecord.getRequestCreatedOn().toString());
					writer.write(",");
					writer.write("pur");
					writer.write(",");
					writer.write(displayRecord.getAmount());
					writer.write(",");
					writer.write(displayRecord.getAppTxnId());
					writer.write(",");
					writer.write(displayRecord.getPaymentGateway());
					writer.write(",");
					writer.write(displayRecord.getPaymentType());
					writer.write(",");
					writer.write(displayRecord.getPaymentOption() == null ? "": displayRecord.getPaymentOption());
                    writer.write(",");
                    writer.write(displayRecord.getPaymentMessage() == null ? "Not Present": displayRecord.getPaymentMessage());
                    writer.write(",");
                    writer.write(displayRecord.getSendToPgDate() == null ? "Not Present": displayRecord.getSendToPgDate());
                    writer.write(",");
                    writer.write(displayRecord.getReceivedFromPgdate() == null ? new Date().toString() : displayRecord.getReceivedFromPgdate().toString());
					writer.write(",");
					writer.write(displayRecord.getGatewayTransactionID() == null ? "Not Present": displayRecord.getGatewayTransactionID());
					writer.write(",");
					writer.write(displayRecord.getStatus() == null ? "Not Present": displayRecord.getStatus());
                    writer.write(",");
					writer.write(displayRecord.getFreeFundName() == null ? "": displayRecord.getFreeFundName());
					writer.write(",");
					writer.write(displayRecord.getFreeFundPrice() == null ? "": displayRecord.getFreeFundPrice());
					writer.write(",");
					writer.write(displayRecord.getFreeFundCode() == null ? "": displayRecord.getFreeFundCode());
					if (displayRecord.getCrossSells() != null && displayRecord.getCrossSells().size() > 0) {
						for (int index = 0; index < 4; index++) {
							if (displayRecord.getCrossSells().size() > index) {
			                    writer.write(",");
								writer.write(displayRecord.getCrossSells().get(index).getCrossSellName() == null ? "" : displayRecord.getCrossSells().get(index).getCrossSellName());
								writer.write(",");
								writer.write(displayRecord.getCrossSells().get(index).getAmount() == null ? "" : displayRecord.getCrossSells().get(index).getAmount().toString());
							} else {
			                    writer.write(",");
								writer.write("");
								writer.write(",");
								writer.write("");
							}
						}
					} else {
						for (int index = 0; index < 4; index++) {
		                    writer.write(",");
							writer.write("");
							writer.write(",");
							writer.write("");
							
						}
					}
                    writer.write(",");
                    writer.write(displayRecord.getWalletId() == null ? "Not Present" : displayRecord.getWalletId());
                    writer.write(",");
                    writer.write(displayRecord.getUserId() == null ? "Not Present": displayRecord.getUserId());
                    writer.write(",");
                    writer.write(displayRecord.getFundSource() == null ? "Not Present": displayRecord.getFundSource());
                    writer.write(",");
                    writer.write(displayRecord.getAggregatorName() == null ? "Not Present": displayRecord.getAggregatorName());
                    writer.write(",");
                    writer.write(displayRecord.getAggregatorReference() == null ? "Not Present": displayRecord.getAggregatorReference());
                    writer.write(",");
                    writer.write(displayRecord.getOperator() == null ? "Not Present": displayRecord.getOperator());
                    writer.write(",");
                    writer.write(displayRecord.getCircle() == null ? "Not Present": displayRecord.getCircle());
                    writer.write(",");
                    writer.write(displayRecord.getAgAmount() == null ? "Not Present": displayRecord.getAgAmount());
                    writer.write(",");
                    writer.write(displayRecord.getChannelId() == null ? "Not Present": displayRecord.getChannelId());
					writer.write("\n");
				}
			}
			response.flushBuffer();
			out.flush();
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*@Override
	protected void buildExcelDocument(Map<String, Object> model,HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response) throws IOException {
		//String csvFileName = "/home/rajani/PgMisdata.csv";
		try {
			//FileWriter writer = new FileWriter(csvFileName);
			//CsvWriter csvOutput = new CsvWriter( new FileWriter(csvFileName, true), ',');
			List<PgMisDisplayRecord> csvData = (List<PgMisDisplayRecord>) model.get("excelData");
			response.setContentType("text/csv; charset=UTF-8");
			response.setHeader("Content-disposition", "attachment; filename=Report.csv");
			OutputStream outSteram = response.getOutputStream( );
			Writer writer = new OutputStreamWriter(outSteram);
			writer.write("Txn ID");
			writer.write(",");
			writer.write("Request received Time");
			writer.write(",");
			writer.write("Payment Mode");
			writer.write(",");
			writer.write("Amount");
			writer.write(",");
			writer.write("Order ID");
			writer.write(",");
			writer.write("Payment Gateway");
			writer.write(",");
			writer.write("Payment Type");
			writer.write(",");
			writer.write("Payment Option");
			writer.write(",");
			writer.write("Payment Gateway Message");
			writer.write(",");
			writer.write("Sent to PG");
			writer.write(",");
			writer.write("Received from PG");
			writer.write(",");
			writer.write("PG Txn ID");
			writer.write(",");
			writer.write("Request received Time");
			writer.write(",");
			writer.write("Txn Status");
			writer.write(",");
			writer.write("FreeFundName");
			writer.write(",");
			writer.write("FreeFundPrice(-)");
			writer.write(",");
			writer.write("FreeFundCode");
			writer.write(",");
			writer.write("CrossSellName_1");
			writer.write(",");
			writer.write("CrossSellAmount_1");
			writer.write(",");
			writer.write("CrossSellName_2");
			writer.write(",");
			writer.write("CrossSellAmount_2");
			writer.write(",");
			writer.write("CrossSellName_3");
			writer.write(",");
			writer.write("CrossSellAmount_3");
			writer.write(",");
			writer.write("CrossSellName_4");
			writer.write(",");
			writer.write("CrossSellAmount_4");
			writer.write(",");
			writer.write("Merchant ID");
			
			if (csvData != null) {
				for (int i = 0; i < csvData.size(); i++) {
					PgMisDisplayRecord displayRecord = (PgMisDisplayRecord) csvData.get(i);
					writer.write(displayRecord.getTxnId().toString());
					writer.write(",");
					writer.write(displayRecord.getRequestCreatedOn().toString());
					writer.write(",");
					writer.write("pur");
					writer.write(",");
					writer.write(displayRecord.getAmount());
					writer.write(",");
					writer.write(displayRecord.getAppTxnId());
					writer.write(",");
					writer.write(displayRecord.getPaymentGateway());
					writer.write(",");
					writer.write(displayRecord.getPaymentType());
					writer.write(",");
					writer.write(displayRecord.getPaymentOption());
					writer.write(",");
					writer.write(displayRecord.getFreeFundName() == null ? "" : displayRecord.getFreeFundName());
					writer.write(",");
					writer.write(displayRecord.getFreeFundPrice() == null ? "" : displayRecord.getFreeFundPrice());
					writer.write(",");
					writer.write(displayRecord.getFreeFundCode() == null ? "" : displayRecord.getFreeFundCode());
					writer.write(",");
					writer.write(displayRecord.getPaymentMessage() == null ? "Not Present" : displayRecord.getPaymentMessage());
					writer.write(",");
					writer.write(displayRecord.getReceivedFromPgdate() == null ? new Date().toString() : displayRecord.getReceivedFromPgdate().toString());
					writer.write(",");
					writer.write(displayRecord.getGatewayTransactionID() == null ? "Not Present" : displayRecord.getGatewayTransactionID());
					writer.write(",");
					writer.write(displayRecord.getRequestCreatedOn().toString());
					writer.write(",");
					writer.write(displayRecord.getStatus() == null ? "Not Present" : displayRecord.getStatus());
					writer.write(",");
					writer.write(displayRecord.getSendToPgDate());
					writer.write(",");
					writer.write(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getMtxnId());
					writer.write(",");
					if (displayRecord.getCrossSells() != null && displayRecord.getCrossSells().size() > 0) {
						for (int index = 0; index < 4; index++) {
							if(displayRecord.getCrossSells().size() > index) {
								writer.write(displayRecord.getCrossSells().get(index).getCrossSellName());
								writer.write(",");
								writer.write(displayRecord.getCrossSells().get(index).getAmount().toString());
							} else {
								writer.write("");
								writer.write(",");
								writer.write("");	
							}
						}
		      } else {
					for (int index = 0; index < 4; index++) {
						writer.write("");
						writer.write(",");
						writer.write("");
					}
		      }
		}
		  
		  outSteram.flush();
		  writer.flush();
		  writer.close();
		 } 
	} catch (IOException e) {
			e.printStackTrace();
	}
  }
*/
	/*@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response)
	// throws Exception {
	{
		String filename = "C:/Users/VIPIN/Desktop/PgMisdata.xls";
		try {
			List<PgMisDisplayRecord> excelData = (List<PgMisDisplayRecord>) model.get("excelData");
			HSSFSheet sheet = workbook.createSheet("Revenue Report");

			//response.setContentType("application/vnd.ms-excel");
			response.setContentType("text/csv; charset=UTF-8");
			response.setHeader("Content-disposition", "attachment; filename=Report.csv");
			OutputStream outSteram = response.getOutputStream();
				
			
			HSSFRow header = sheet.createRow(0);
			header.createCell(0).setCellValue("Txn ID");
			header.createCell(1).setCellValue("Request received Time");
			header.createCell(2).setCellValue("Payment Mode");
			header.createCell(3).setCellValue("Amount");
			header.createCell(4).setCellValue("Order ID");
			header.createCell(5).setCellValue("Payment Gateway");
			header.createCell(6).setCellValue("Payment Type");
			header.createCell(7).setCellValue("Payment Option");
			header.createCell(8).setCellValue("Payment Gateway Message");
			header.createCell(9).setCellValue("Sent to PG");
			header.createCell(10).setCellValue("Received from PG");
			header.createCell(11).setCellValue("PG Txn ID");
			header.createCell(12).setCellValue("Request received Time");
			header.createCell(13).setCellValue("Txn Status");
			header.createCell(14).setCellValue("FreeFundName");
			header.createCell(15).setCellValue("FreeFundPrice(-)");
			header.createCell(16).setCellValue("FreeFundCode");
			header.createCell(17).setCellValue("CrossSellName_1");
			header.createCell(18).setCellValue("CrossSellAmount_1");
			header.createCell(19).setCellValue("CrossSellName_2");
			header.createCell(20).setCellValue("CrossSellAmount_2");
			header.createCell(21).setCellValue("CrossSellName_3");
			header.createCell(22).setCellValue("CrossSellAmount_3");
			header.createCell(23).setCellValue("CrossSellName_4");
			header.createCell(24).setCellValue("CrossSellAmount_4");
			header.createCell(25).setCellValue("Merchant ID");
			
            header.createCell(26).setCellValue("Wallet ID");
            header.createCell(27).setCellValue("User ID");
            header.createCell(28).setCellValue("Fund Source");
            header.createCell(29).setCellValue("Aggregator Name");
            header.createCell(30).setCellValue("Aggregator Reference");
            header.createCell(31).setCellValue("Operator");
            header.createCell(32).setCellValue("Circle");
            header.createCell(33).setCellValue("Amount");
            header.createCell(34).setCellValue("Channel ID");

			int rowNum = 1;

			if (excelData != null) {
				for (int i = 0; i < excelData.size(); i++) {
					sheet.autoSizeColumn(rowNum);
					PgMisDisplayRecord displayRecord = (PgMisDisplayRecord) excelData.get(i);
					HSSFRow row = sheet.createRow(rowNum++);
					row.createCell(0).setCellValue(displayRecord.getTxnId());
					row.createCell(1).setCellValue(displayRecord.getRequestCreatedOn());
					row.createCell(2).setCellValue("pur");
					row.createCell(3).setCellValue(displayRecord.getAmount());
					row.createCell(4).setCellValue(displayRecord.getAppTxnId());
					row.createCell(5).setCellValue(displayRecord.getPaymentGateway());
					row.createCell(6).setCellValue(displayRecord.getPaymentType());
					row.createCell(7).setCellValue(displayRecord.getPaymentOption());
					row.createCell(14).setCellValue(displayRecord.getFreeFundName() == null ? "" : displayRecord.getFreeFundName());
					row.createCell(15).setCellValue(displayRecord.getFreeFundPrice() == null ? "" : displayRecord.getFreeFundPrice());
					row.createCell(16).setCellValue(displayRecord.getFreeFundCode() == null ? "" : displayRecord.getFreeFundCode());
					row.createCell(8).setCellValue(displayRecord.getPaymentMessage() == null ? "Not Present" : displayRecord.getPaymentMessage());
					row.createCell(10).setCellValue(displayRecord.getReceivedFromPgdate() == null ? new Date() : displayRecord.getReceivedFromPgdate());
					row.createCell(11).setCellValue(displayRecord.getGatewayTransactionID() == null ? "Not Present" : displayRecord.getGatewayTransactionID());
					row.createCell(12).setCellValue(displayRecord.getRequestCreatedOn());
					row.createCell(13).setCellValue(displayRecord.getStatus() == null ? "Not Present" : displayRecord.getStatus());
					row.createCell(9).setCellValue(displayRecord.getSendToPgDate());
					row.createCell(25).setCellValue(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getMtxnId());

                    row.createCell(26).setCellValue(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getWalletId());
                    row.createCell(27).setCellValue(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getUserId());
                    row.createCell(28).setCellValue(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getFundSource());
                    row.createCell(29).setCellValue(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getAggregatorName());
                    row.createCell(30).setCellValue(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getAggregatorReference());
                    row.createCell(31).setCellValue(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getOperator());
                    row.createCell(32).setCellValue(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getCircle());
                    row.createCell(33).setCellValue(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getAgAmount());
                    row.createCell(34).setCellValue(displayRecord.getMtxnId() == null ? "Not Present" : displayRecord.getChannelId());
					
					
					if (displayRecord.getCrossSells() != null && displayRecord.getCrossSells().size() > 0) {
						for (int index = 0; index < 4; index++) {
							if(displayRecord.getCrossSells().size() > index) {
								row.createCell((index*2) + 17).setCellValue(displayRecord.getCrossSells().get(index).getCrossSellName());
								row.createCell((index*2) + 18).setCellValue(displayRecord.getCrossSells().get(index).getAmount());
							}else {
								row.createCell((index*2) + 17).setCellValue("");
								row.createCell((index*2) + 18).setCellValue("");
							}
						}
					}else {
						for (int index = 0; index < 4; index++) {
							row.createCell((index*2) + 17).setCellValue("");
							row.createCell((index*2) + 18).setCellValue("");
						}
					}
				}

				workbook.write(outSteram);
				outSteram.flush();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/

}
