package com.freecharge.payment.dao;

import java.util.List;

public interface PgMisAutenticationFetchDAO {
	
	public List<PgMisDisplayRecord> getMisData(PgMISRequestDataDAO misRequestData, Integer pageCount);
	
	public Integer getMisDataCount(PgMISRequestDataDAO misRequestData);

}
