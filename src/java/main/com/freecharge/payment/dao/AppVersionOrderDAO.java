package com.freecharge.payment.dao;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.freecharge.payment.dos.entity.AppVersionOrder;
import com.google.gson.Gson;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;

import java.sql.Timestamp;

@Component
public class AppVersionOrderDAO {
	private SimpleJdbcInsert insertAppversionOrder;

	private final Logger logger = Logger.getLogger(getClass());
	private final static String TABLE_NAME = "app_version_order";
	private final static String ORDER_ID_FIELD = "orderId";
	private final static String SERIALIZATION_VERSION = "sVersion";
	private final static String CREATED_ON = "createdTime";
	private final static String API_VERSION = "apiVersion";

	@Autowired
	AmazonDynamoDBAsync dynamoClient;
	
	 @Autowired
	 public void setDataSource(DataSource dataSource) {
		 this.insertAppversionOrder = new SimpleJdbcInsert(dataSource).withTableName("app_version_order").usingGeneratedKeyColumns(AppVersionOrder.APP_VERSION_ORDER_ID, "n_last_updated", "n_created");
	 }

	@Transactional
	public void insertAppVersionOrder(AppVersionOrder appVersionOrder) throws RuntimeException {


		try {


			SqlParameterSource sqlParameterSource = appVersionOrder.getMapSqlParameterSource();
			Number primaryKey = this.insertAppversionOrder.executeAndReturnKey(sqlParameterSource);
			appVersionOrder.setAppVersionOrderId(primaryKey.intValue());
//						Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
//						item.put(ORDER_ID_FIELD, new AttributeValue(appVersionOrder.getOrderId()));
//						item.put(API_VERSION, new AttributeValue(appVersionOrder.getAppVersion().toString()));
//						item.put(CREATED_ON, new AttributeValue(String.valueOf(new Date().getTime())));
//						item.put(SERIALIZATION_VERSION, new AttributeValue("1"));
//			
//						
//						PutItemResult result = dynamoClient.putItem(new PutItemRequest()
//						.withTableName(TABLE_NAME)
//						.withItem(item));
		} catch (Exception e) {
			logger.error("Exception caught while insertin appVersion for ordeId: " + appVersionOrder.getOrderId(), e);
			throw e;
		}


	}
}
