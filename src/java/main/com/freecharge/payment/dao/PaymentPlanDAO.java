package com.freecharge.payment.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dao.mappers.PaymentPlanMapper;
import com.freecharge.payment.dos.entity.PaymentPlan;
import com.freecharge.payment.exception.PaymentPlanNotFoundException;
import com.freecharge.payment.dao.mappers.NewPaymentPlanMapper;

@Component
public class PaymentPlanDAO {
    private static Logger logger = LoggingFactory.getLogger(PaymentPlanDAO.class);

    private NamedParameterJdbcTemplate jdbcTemplate;
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    public void insertPaymentPlan(PaymentPlan toInsert) {
        logger.info("About to insert into new payment plan " + toInsert);
        String insertOrUpdateQuery = "insert into payment_plan (plan_reference, mtxn_id, pg_amount, wallet_amount, created_ts) " +
        		"values (:plan_reference, :mtxn_id, :pg_amount, :wallet_amount, current_timestamp)";
        
        Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("plan_reference", toInsert.getPlanReferenceId());
        insertParams.put("pg_amount", toInsert.getPgAmount().getAmount());
        insertParams.put("wallet_amount", toInsert.getWalletAmount().getAmount());
        insertParams.put("mtxn_id", toInsert.getMtxnId());
        try {
            this.jdbcTemplate.update(insertOrUpdateQuery, insertParams);
        } catch (Exception e) {
            logger.error("while inserting into new payment_plan" , e);
        }
    }
    
    public PaymentPlan getPaymentPlan(String planReferenceID) throws PaymentPlanNotFoundException {
        logger.info("About to fetch new payment plan for Reference : " +  planReferenceID);
        List<PaymentPlan> paymentPlanList=null;
        String retrieveQuery = "select plan_id, plan_reference, mtxn_id, pg_amount, wallet_amount from payment_plan where plan_reference = :plan_reference order by PLAN_ID desc limit 1";
        
        Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("plan_reference", planReferenceID);
        try {
            paymentPlanList = this.jdbcTemplate.query(retrieveQuery, insertParams, new NewPaymentPlanMapper());
        } catch (Exception e) {
            logger.error("Error while fetching new payment plan ", e);
        }
        if (paymentPlanList == null || paymentPlanList.isEmpty()) {
            logger.info("while fetching payment plan from old table ");
            retrieveQuery ="select plan_id, plan_reference, pg_amount, wallet_amount from payment_plan_backup where plan_reference = :plan_reference";
            try {
                logger.info("About to fetch payment plan backup for Reference : " +  planReferenceID);
                paymentPlanList = this.jdbcTemplate.query(retrieveQuery, insertParams, new PaymentPlanMapper());
            } catch (Exception exception) {
                logger.error("Error while fetching payment plan " + exception.getMessage());
                
            }
        }
        if (paymentPlanList == null || paymentPlanList.isEmpty()) {
            throw new PaymentPlanNotFoundException("No payment plan retrieved for planReferenceID: " + planReferenceID);
        }
        return paymentPlanList.get(0);
    }
    
    public PaymentPlan getPaymentPlanFromMtxnId(String mtxnId, String orderId) throws PaymentPlanNotFoundException {
        logger.info("About to fetch payment plan for mtxnId : " +  mtxnId);
        List<PaymentPlan> paymentPlanList = null;
        String retrieveQuery = "select plan_id, plan_reference, mtxn_id, pg_amount, wallet_amount from payment_plan where mtxn_id = :mtxn_id";
        Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("mtxn_id", mtxnId);
        try {
            paymentPlanList = this.jdbcTemplate.query(retrieveQuery, insertParams, new NewPaymentPlanMapper());
        } catch (Exception e) {
            logger.error("while fetching payment plan based on mtxn id "+ e.getMessage());
        }
        if(paymentPlanList==null || paymentPlanList.isEmpty()) {
            logger.info("while fetching payment plan from old table ");
            retrieveQuery = "select plan_id, plan_reference, pg_amount, wallet_amount from payment_plan_backup where plan_reference = :plan_reference";
            insertParams.remove("mtxn_id");
            insertParams.put("plan_reference", orderId);
            try {
                paymentPlanList = this.jdbcTemplate.query(retrieveQuery, insertParams, new PaymentPlanMapper());
            } catch (Exception e) {
                logger.error("while fetching payment plan based on mtxn id "+ e.getMessage());
            }
        }
        if(paymentPlanList==null || paymentPlanList.isEmpty()) {
            return null;
        }
        return paymentPlanList.get(0);
    }
    
    public boolean updatePaymentPlan(PaymentPlan paymentPlan) {
        logger.info("About to update payment plan " +  paymentPlan);

        String updateQuery = "update payment_plan set wallet_amount = :wallet_amount, pg_amount = :pg_amount where mtxn_id = :mtxn_id";
        
        Map<String, Object> insertParams = new HashMap<String, Object>();
        insertParams.put("mtxn_id", paymentPlan.getMtxnId());
        insertParams.put("wallet_amount", paymentPlan.getWalletAmount().getAmount());
        insertParams.put("pg_amount", paymentPlan.getPgAmount().getAmount());
        
        int updateCount = this.jdbcTemplate.update(updateQuery, insertParams);
        final boolean updateSuccessful = updateCount == 1;

        if (updateSuccessful) {
            logger.info("Successfully updated payment plan " +  paymentPlan);
        } else {
            logger.info("Failed to update payment plan " +  paymentPlan + ". Number of rows updated: " + updateCount);
        }

        return updateSuccessful;
    }
}