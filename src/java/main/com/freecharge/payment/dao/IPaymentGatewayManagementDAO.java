package com.freecharge.payment.dao;

import java.util.List;
import java.util.Map;

public interface IPaymentGatewayManagementDAO {

    List<Map<String,Object>> getActivePaymentGateways();

    List<Map<String,Object>> getActiveBankGroup();

    List<Map<String,Object>> getDefaultData();

    List<Map<String,Object>> getData(String whereClause);

    void updatePG(Integer pgRelationId, Integer gatewayId);
}
