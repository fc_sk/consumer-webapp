package com.freecharge.payment.dao.jdbc;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.services.PaymentFulfilmentMapDO;

public class PaymentFulfilmentMapDAO {
    private static Logger              logger = LoggingFactory.getLogger(PaymentFulfilmentMapDAO.class);
    private SimpleJdbcInsert           insertJdbcTemplate;
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.insertJdbcTemplate = new SimpleJdbcInsert(dataSource).withTableName("payment_fulfilment_map")
                .usingGeneratedKeyColumns("n_last_updated", "n_created");
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Transactional
    public int insertPaymentFulfilmentMap(PaymentFulfilmentMapDO paymentFulfilmentMapDO) {
        try {
            SqlParameterSource parameters = new BeanPropertySqlParameterSource(paymentFulfilmentMapDO);
            return this.insertJdbcTemplate.execute(parameters);
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }

    @Transactional
    public String getMtxnFromOrder(String orderId) {
        try {
            String queryString = "select mtxn_id from payment_fulfilment_map where order_id=:orderId";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("orderId", orderId);
            return this.jdbcTemplate.queryForObject(queryString, paramMap, String.class);
        } catch (RuntimeException re) {
        }
        return null;
    }

    @Transactional
    public String getOrderFromMtxn(String mtxnId) {
        try {
            String queryString = "select order_id from payment_fulfilment_map where mtxn_id=:mtxnId";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("mtxnId", mtxnId);
            return this.jdbcTemplate.queryForObject(queryString, paramMap, String.class);
        } catch (RuntimeException re) {
        }
        return null;
    }
}
