package com.freecharge.payment.dao.jdbc;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.dao.jdbc.InReadDAO;
import com.freecharge.app.domain.entity.PaymentTypeMaster;
import com.freecharge.app.domain.entity.PaymentTypeOptions;
import com.freecharge.app.domain.entity.PaymentTypeRelation;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.mappers.BankMasterRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.InRequestMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.InResponseMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.PaymentResponseRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.PaymentTransactionMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.PaymentTypeMasterRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.PaymentTypeOptionRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.PaymentTypeRelationRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.PgMasterRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.PgMisUserRowMapper;
import com.freecharge.app.domain.entity.jdbc.mappers.PgRelationRowMapper;
import com.freecharge.common.businessdo.PaymentOptionListBusinessDO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dos.entity.BankMaster;
import com.freecharge.payment.dos.entity.PaymentGatewayMaster;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction.Status;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.dos.entity.PgMisUser;
import com.freecharge.payment.dos.entity.PgRelation;

public class PgReadDAO {

	private static Logger logger = LoggingFactory.getLogger(PgReadDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate masterJdbcTemplate;
	
	@Autowired
	InReadDAO inReadDao;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

    @Autowired
    public void setMasterDataSource(DataSource dataSource) {
        this.masterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

	public BankMaster getBankMasterForRequest(String uiCode, Integer paymentOption) {

		logger.info("Finding banks per request");
		logger.info("uiCode : " + uiCode + "   paymentOption: " + paymentOption);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		try {

			String queryString = "select * from banks_grp_master where code_ui " + " = :codeUIValue and is_active = :isActiveValue and payment_option = :paymentOptionValue";

			paramMap.put("codeUIValue", uiCode);
			paramMap.put("isActiveValue", true);
			paramMap.put("paymentOptionValue", paymentOption);

			List<BankMaster> bankMasters = this.jdbcTemplate.query(queryString, paramMap, new BankMasterRowMapper());

			if (bankMasters != null && bankMasters.size() > 0)
				return bankMasters.get(0);
			else
				return null;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public List<BankMaster> getBankMaster() {

		logger.info("Finding banks per request");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		try {

			String queryString = "select * from banks_grp_master ";

			List<BankMaster> bankMasters = this.jdbcTemplate.query(queryString, paramMap, new BankMasterRowMapper());

			if (bankMasters != null && bankMasters.size() > 0)
				return bankMasters;
			else
				return null;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public PaymentGatewayMaster getPaymentGateway(Integer gatewayId) {
		logger.info("Finding PG per request");
		try {
			String queryString = "select * from payment_gateway_master  where payment_gateway_master_id = :IdValue";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("IdValue", gatewayId);

			List<PaymentGatewayMaster> paymentGatewayMasters = this.jdbcTemplate.query(queryString, paramMap, new PgMasterRowMapper());

			if (paymentGatewayMasters != null && paymentGatewayMasters.size() > 0)
				return paymentGatewayMasters.get(0);
			else
				return null;

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public List<PaymentGatewayMaster> getPaymentGatewayList() {
		logger.info("Finding PG per request");
		try {
			String queryString = "select * from payment_gateway_master ";
			Map<String, Object> paramMap = new HashMap<String, Object>();

			List<PaymentGatewayMaster> paymentGatewayMasters = this.jdbcTemplate.query(queryString, paramMap, new PgMasterRowMapper());

			if (paymentGatewayMasters != null && paymentGatewayMasters.size() > 0)
				return paymentGatewayMasters;
			else
				return null;

		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

    public Integer countPGRequestForOrderId(String orderId) {
        String queryString = "select count(*) from payment_rq where order_id " + " = :orderId and payment_mode = :paymentMode";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        paramMap.put("paymentMode", "pur");
        Integer countValue = this.jdbcTemplate.queryForInt(queryString, paramMap); // doubtful
        return countValue;
    }

	public Integer getPaymentRequestCount(String orderId) {
		logger.info("Finding old request to gateway request");
		try {
			String queryString = "select count(*) from payment_txn where order_id " + " = :orderId ";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("orderId", orderId);
			Integer count = this.masterJdbcTemplate.queryForInt(queryString, paramMap);
			return count;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public Integer getSuccessFullTxnForOrderId(String orderId) {
		logger.info("Finding old request to gateway request");
		try {
			final String queryString = "select count(*) from payment_txn where payment_mode = :mode and order_id " + " = :orderId and is_successful = :status";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("orderId", orderId);
			paramMap.put("status", 1);
            paramMap.put("mode", "pur");
            Integer count = this.masterJdbcTemplate.queryForInt(queryString, paramMap);
			return count;
			/*
			 * if (query.uniqueResult() instanceof Long) return ((Long) query.uniqueResult()).intValue(); if (query.uniqueResult() instanceof Integer) return (Integer) query.uniqueResult();
			 */
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public PaymentTransaction getSuccessfulPaymentTransactionByOrderId(String orderId) {
        final String queryString = "select * from payment_txn  where order_id = :orderId and is_successful = 1 and payment_mode = :mode";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        paramMap.put("mode", "pur");
        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(queryString, paramMap, new PaymentTransactionMapper());
        if (!paymentTransactions.isEmpty()) {
            if (paymentTransactions.size() > 1) {
                logger.info(String.format("More than 1 payment transaction found for order id %s", orderId));
            }
            return paymentTransactions.get(0);
        } else {
            return null;
        }
	}


    public PaymentTransaction getLastPaymentTransactionSentToPG(String orderId) {
        final String queryString = "select * from payment_txn  where order_id = :orderId order by payment_txn_id desc";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(queryString, paramMap, new PaymentTransactionMapper());

        if (!paymentTransactions.isEmpty()) {
            return paymentTransactions.get(0);
        }

        return null;
    }
    
    public List<PaymentTransaction> getAllPaymentTransactionSentToPG(String orderId) {
        final String queryString = "select * from payment_txn  where order_id = :orderId order by sent_to_pg desc";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(queryString, paramMap, new PaymentTransactionMapper());

        if (!paymentTransactions.isEmpty()) {
            return paymentTransactions;
        }

        return null;
    }

	public List<PaymentTransaction> getAllPaymentTxSentToPGForEmail(String email) {

		final String queryString = "select pt.* from txn_fulfilment tf "
				+ "inner join txn_home_page thp on tf.txn_home_page_id = thp.id "
				+ "inner join order_id oi on oi.lookup_id = thp.lookup_id "
				+ "inner join payment_txn pt on oi.order_id = pt.order_id "
				+ "where tf.email = :email and pt.sent_to_pg is not null "
				+ "order by pt.sent_to_pg desc";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("email", email);
		logger.info("getAllPaymentTxSentToPGForEmail :" + email);
		logger.info("jdbctemplate :" + jdbcTemplate.toString());
		List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(queryString, paramMap,
				new PaymentTransactionMapper());

		if (!paymentTransactions.isEmpty()) {
			return paymentTransactions;
		}

		return null;
	}

	public List<PaymentTransaction> getAllPaymentTxSentToPGForEmailFromDate(String email, Timestamp fromDate,
			Timestamp toDate) {

		final String queryString = "select pt.* from txn_fulfilment tf "
				+ "inner join txn_home_page thp on tf.txn_home_page_id = thp.id "
				+ "inner join order_id oi on oi.lookup_id = thp.lookup_id "
				+ "inner join payment_txn pt on oi.order_id = pt.order_id "
				+ "where tf.email = :email and tf.created_at>=:fromDate and tf.created_at<=:toDate and pt.sent_to_pg is not null "
				+ "order by pt.sent_to_pg desc";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("email", email);
		paramMap.put("fromDate", fromDate);
		paramMap.put("toDate", toDate);

		List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(queryString, paramMap,
				new PaymentTransactionMapper());

		if (!paymentTransactions.isEmpty()) {
			return paymentTransactions;
		}

		return null;
	}
	
    public List<PaymentTransaction> getAllPaymentTransactionByAcsOrder(String orderId) {
        final String queryString = "select * from payment_txn  where order_id = :orderId order by sent_to_pg asc";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(queryString, paramMap, new PaymentTransactionMapper());

        if (!paymentTransactions.isEmpty()) {
            return paymentTransactions;
        }

        return null;
    }

    
	public List<PaymentTransaction> getPaymentTxnDetails(String orderId) {
		logger.info("Finding PG per request");
		try {
			String queryString = "select * from payment_txn  where order_id  = :orderId and payment_mode = :mode";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("orderId", orderId);
			paramMap.put("mode", "pur");
			List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(queryString, paramMap, new PaymentTransactionMapper());
			if (paymentTransactions != null && paymentTransactions.size() > 0)
				return paymentTransactions;
			else
				return null;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}

	}

	public PaymentTransaction getPaymentTransactionById(Integer txnId) {
		PaymentTransaction paymentTransaction = null;
		try {
			String queryStr = "select * from payment_txn where payment_txn_id=:id";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("id", txnId);
			List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(queryStr, paramMap, new PaymentTransactionMapper());
			if (paymentTransactions != null && paymentTransactions.size() > 0)
				return paymentTransactions.get(0);
			else
				return null;
		} catch (Exception e) {
			logger.error("Error occured getting payment transaction with id :" + txnId);
		}
		return paymentTransaction;
	}

	public PgRelation getPaymentGateway(Integer productId, Integer affiliateId, Integer bankMasterId) {
		try {
			final String queryString = "select * from pg_relation  where fk_product_master_id  = :productIdValue and fk_affiliate_profile_id = :affiliateIdValue and fk_banks_grp_master_id = :bankMasterIdValue";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("productIdValue", productId);
			paramMap.put("affiliateIdValue", affiliateId);
			paramMap.put("bankMasterIdValue", bankMasterId);
			List<PgRelation> pgRelations = this.jdbcTemplate.query(queryString, paramMap, new PgRelationRowMapper());
			if (pgRelations != null && pgRelations.size() > 0)
				return pgRelations.get(0);
			else
				return null;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public List<PgRelation> getPgRelation() {
		logger.info("Finding banks per request");
		try {
			final String queryString = "select * from pg_relation ";
			Map<String, Object> paramMap = new HashMap<String, Object>();

			List<PgRelation> pgRelations = this.jdbcTemplate.query(queryString, paramMap, new PgRelationRowMapper());
			if (pgRelations != null && pgRelations.size() > 0)
				return pgRelations;
			else
				return null;
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}

	public List<PaymentTypeOptions> findAll() {
		logger.info("finding all OperatorMaster instances", null);
		try {
			String queryString = "select * from payment_type_options";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			List<PaymentTypeOptions> adminUserList = this.jdbcTemplate.query(queryString, paramMap, new PaymentTypeOptionRowMapper());
			return adminUserList;
		} catch (RuntimeException re) {
			logger.error("find all failed", re);
			throw re;
		}

	}

	public List<PaymentTypeMaster> getPaymentTypeMaster() {
		try {
			String jdbcQuery = "select * from payment_type_master ";

			Map<String, Object> paramMap = new HashMap<String, Object>();

			List<PaymentTypeMaster> paymentTypeMasters = this.jdbcTemplate.query(jdbcQuery, paramMap, new PaymentTypeMasterRowMapper());

			if (paymentTypeMasters != null && paymentTypeMasters.size() > 0)
				return paymentTypeMasters;
			else
				return null;

		} catch (Exception e) {
			return null;
		}
	}

	public List<PaymentTypeRelation> getPaymentTypeRelation() {
		try {
			String jdbcQuery = "select * from payment_type_relation ";

			Map<String, Object> paramMap = new HashMap<String, Object>();

			List<PaymentTypeRelation> paymentTypeRelations = this.jdbcTemplate.query(jdbcQuery, paramMap, new PaymentTypeRelationRowMapper());

			if (paymentTypeRelations != null && paymentTypeRelations.size() > 0)
				return paymentTypeRelations;
			else
				return null;

		} catch (Exception e) {
			return null;
		}
	}

	public boolean isMisUserValid(PgMisUser misUsers) {
		try {
			String userName = null;
			if (misUsers.getPgUserName() != null && !misUsers.getPgUserName().isEmpty()) {
				userName = misUsers.getPgUserName();

				final String queryString = "select * from pg_mis_user where pg_user_name = :userName";

				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("userName", userName);

				List<PgMisUser> adminUserList = this.jdbcTemplate.query(queryString, paramMap, new PgMisUserRowMapper());

				if (adminUserList != null && !adminUserList.isEmpty() && adminUserList.size() > 0) {
					PgMisUser userStaus = adminUserList.get(0);
					if (userStaus.getPgUserName().equalsIgnoreCase(misUsers.getPgUserName()) && userStaus.getPgUserPassword().equalsIgnoreCase(misUsers.getPgUserPassword()) && userStaus.getPgUserActiveStatus() == true) {
						return true;
					}
				}
			}
		} catch (RuntimeException re) {
			logger.error("[IN MODERATOR] finding User admin list from Database: ", re);
		}
		return true;
	}

	public List<Map<String, Object>> getPaymentOptionsList(BaseBusinessDO baseBusinessDO) {
		try {
			String queryStr = "select  options.display_name as displayName, options.display_img as displayImg, options.img_url as imgUrl, options.code as code " + "from payment_type_master master join payment_type_relation relation on master.payment_type_master_id = relation.fk_payment_type_id " + "join payment_type_options options on relation.payment_type_relation_id = options.fk_payment_type_relation_id "
					+ "where master.code=:code and options.is_active = 1 and relation.fk_product_master_id=:productMasterId";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("code", ((PaymentOptionListBusinessDO) baseBusinessDO).getPaymentType());
			paramMap.put("productMasterId", ((PaymentOptionListBusinessDO) baseBusinessDO).getProductMasterId());

			List<Map<String, Object>> resultSet = this.jdbcTemplate.queryForList(queryStr, paramMap);

			return resultSet;

		} catch (Exception e) {
			logger.error("Error while getting payment options.." + e);
			return null;
		}

	}

	public List<PaymentTypeMaster> getPaymentType(Integer productId, Integer affiliateId) {
		List<PaymentTypeMaster> pts = null;
		try {
			String jdbcQuery = "SELECT master.payment_type_master_id as paymentID, master.name as name, master.code as code, relation.payment_type_relation_id as relationPaymenttypeID" + "  FROM payment_type_master master, payment_type_relation relation " + " WHERE  relation.fk_product_master_id = :productId " + "       and relation.fk_affliate_unique_id = :affiliateId  and relation.fk_payment_type_id = master.payment_type_master_id "
					+ "       and relation.is_active = :active order by relation.order_by asc";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("productId", productId);
			paramMap.put("affiliateId", affiliateId.toString());
			paramMap.put("active", true);

			List<Map<String, Object>> lists = this.jdbcTemplate.queryForList(jdbcQuery, paramMap);
			pts = new ArrayList<PaymentTypeMaster>();
			for (Map<String, Object> map : lists) {
				PaymentTypeMaster pt = new PaymentTypeMaster(); // doubtful
				pt.setPaymentTypeMasterId(Integer.parseInt(map.get("paymentID").toString()));
				pt.setCode((String) map.get("code"));
				pt.setName((String) map.get("name"));
				pt.setRelationId((Integer) map.get("relationPaymenttypeID"));
				pts.add(pt);
			}

		} catch (Exception e) {
			logger.error("Error while getting payment type master..product id" + productId + " affiliate id : " + affiliateId, e);
		}
		return pts;
	}

	public List<PaymentTypeOptions> getPaymentType(Integer relationid) {

		String query = "select * from payment_type_options where fk_payment_type_relation_id = :relationId and is_active = :active order by order_by asc";

		logger.debug("getPaymentType method for relationid " + relationid);

		try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("relationId", relationid);
			paramMap.put("active", true);

			List<PaymentTypeOptions> paymentTypeOptions = this.jdbcTemplate.query(query, paramMap, new PaymentTypeOptionRowMapper());

			if (paymentTypeOptions != null && paymentTypeOptions.size() > 0) {
				return paymentTypeOptions;
			} else
				return null;

		} catch (Exception e) {
			return null;
		}
	}

	public List<PaymentTransaction> getPaymentTxnWithResNull() {

		logger.info("Entered into the getPaymentTxnWithResNull() of PgReadDAO class");

		try {
			String jdbcQuery = "select * from payment_txn paytxn where paytxn.recieved_from_pg is NULL ";

			Map<String, Object> paramMap = new HashMap<String, Object>();

			List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(jdbcQuery, paramMap, new PaymentTransactionMapper());
			if (paymentTransactions != null && paymentTransactions.size() > 0) {
				return paymentTransactions;
			} else
				return null;

		} catch (Exception e) {
			return null;
		}
	}
	
	public List<InResponse> isRechargeSuccess(String orderId) {

		logger.info("Entered into the isRechargeSuccess() of PgReadDAO class");

		try {
			String jdbcQuery = "select inreq.request_id from  in_request inreq where inreq.affiliate_trans_id =:orderId ORDER BY created_time DESC LIMIT 1";

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("orderId", orderId);

			Long requestID = this.jdbcTemplate.queryForLong(jdbcQuery, paramMap); // doubtful

			String query = "select * from   in_response inres where  inres.request_id =:reqId ";

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("reqId", requestID);

			List<InResponse> inResponses = this.jdbcTemplate.query(query, map, new InResponseMapper());

			if (inResponses != null && inResponses.size() > 0) {
				return inResponses;
			} else
				return null;
		} catch (Exception e) {
			return null;
		}
	}

    public List<PaymentResponse> getPaymentResponseByResponseId(Integer responseId) {
        String sql = "select * from payment_rs where payment_rs_id=:responseId";
        Map<String, Object> param = new HashMap<String, Object>(1);
        param.put("responseId", responseId);

        return this.jdbcTemplate.query(sql, param, new PaymentResponseRowMapper());
    }

	public List<PaymentTypeOptions> getPaymentOptionTypeList(BaseBusinessDO baseBusinessDO) {
		try {
			String jdbcQuery = "select * from payment_type_options where code=:code and is_active=:isActive ";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("code", ((PaymentOptionListBusinessDO) baseBusinessDO).getPaymentType()); // doubtful
			paramMap.put("isActive", true);
			List<PaymentTypeOptions> paymentTypeOptionsList = this.jdbcTemplate.query(jdbcQuery, paramMap, new PaymentTypeOptionRowMapper());
			if (paymentTypeOptionsList != null && paymentTypeOptionsList.size() > 0)
				return paymentTypeOptionsList;
			else
				return null;
		} catch (Exception exp) {
			exp.printStackTrace();
			return null;
		}
	}

    public List<PaymentTransaction> getPaymentTransactionByMerchantOrderId(String merchantOrderId) {
        String query = "select * from payment_txn where mtxn_id = :merchantOrderId order by sent_to_pg desc";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("merchantOrderId", merchantOrderId);

        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());

        if (paymentTransactions.isEmpty()) {
            logger.info("No payment transaction found for merchant order id:" + merchantOrderId);
           return null;
        }

        return paymentTransactions;
    }
    public PaymentTransaction getSuccessfulPaymentTransactionByMerchantOrderIdforGivenPG(String merchantOrderId, String paymentGateway) {
        String query = "select * from payment_txn where mtxn_id = :merchantOrderId and payment_gateway =:paymentGateway and is_successful=1 order by sent_to_pg desc";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("merchantOrderId", merchantOrderId);
        paramMap.put("paymentGateway", paymentGateway);

        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());

        if (paymentTransactions.isEmpty()) {
            logger.info("No payment transaction found for merchant order id:" + merchantOrderId);
           return null;
        }

        return paymentTransactions.get(0);
    }
    
    public List<PaymentTransaction> getSuccessfulPaymentTransactionByMerchantOrderId(String merchantOrderId) {
        String query = "select * from payment_txn where mtxn_id = :merchantOrderId and is_successful=1 order by sent_to_pg desc";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("merchantOrderId", merchantOrderId);

        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());

        if (paymentTransactions.isEmpty()) {
            logger.info("No payment transaction found for merchant order id:" + merchantOrderId);
           return null;
        }

        return paymentTransactions;
    }

    public List<PaymentTransaction> getSuccessfulPaymentTransactionThroughPGByOrderId(String orderId) {
    	List<PaymentTransaction> paymentList = null;
    	try {
    		String query = "select mtxn_id, amount, recieved_from_pg, payment_gateway from payment_txn where order_id = :orderId and payment_gateway not in (:paymentGateway) and is_successful=1 order by sent_to_pg desc";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("orderId", orderId);
            paramMap.put("paymentGateway", "fw");
            paymentList = new ArrayList<PaymentTransaction>();
            List<Map<String, Object>> lists = this.masterJdbcTemplate.queryForList(query, paramMap);
            for (Map<String, Object> map : lists) {
            	PaymentTransaction paymentTransaction = new PaymentTransaction();
            	paymentTransaction.setAmount(Double.parseDouble(map.get("amount").toString()));
            	paymentTransaction.setMerchantTxnId((String)map.get("mtxn_id"));
            	paymentTransaction.setRecievedToPg((Date)map.get("recieved_from_pg"));
            	paymentTransaction.setPaymentGateway((String)map.get("payment_gateway"));
            	paymentList.add(paymentTransaction);
            }
    	} catch (Exception ex) {
    		logger.error("Error while getting success payment_txn list " + orderId, ex);
        }
        return paymentList;
    }

    public PaymentTransaction getLatestPaymentTransactionThroughPGByOrderId(String orderId) {
        String query = "select * from payment_txn where order_id = :orderId and payment_gateway not in (:paymentGateway) order by sent_to_pg desc";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        paramMap.put("paymentGateway", "fw");
        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());
        if (paymentTransactions.isEmpty()) {
            logger.info("No payment transaction found for merchant order id:" + orderId);
           return null;
        }
        return paymentTransactions.get(0);
    }

    public PaymentTransaction getLastSuccessfulPGTransactionForUser(Integer userId) {
        String query = "select pt.* from payment_txn pt JOIN user_transaction_history uth ON (uth.order_id = pt.order_id)"
                + " where uth.fk_user_id=:userId and pt.is_successful=1 and pt.payment_gateway!='fw' and "
                + "pt.payment_gateway!='zpg' order by pt.sent_to_pg desc limit 1";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("userId", userId);
        List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(query, paramMap,
                new PaymentTransactionMapper());
        if (paymentTransactions.isEmpty()) {
            logger.info("No successful payment txn found for user id :" + userId);
            return null;
        }
        return paymentTransactions.get(0);
    }
    
    public List<PaymentTransaction> getPaymentTransactionsByOrderId(String orderId) {
        String query = "select * from payment_txn where order_id = :orderId";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("orderId", orderId);

        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());
        
        return paymentTransactions;
    }

    public LinkedHashMap<String, List<PaymentTransaction>> getPaymentTransactionsByOrderId(Iterable<String> orderIDs) {
        String queryFormat = "select * from payment_txn where order_id IN (%s) order by order_id";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (String eachOrderID: orderIDs) {
            final String token = "orderId" + i;
            sb.append((i++==0? "": ",") + ":" + token);  // prefix with comma if already started, i.e. (i > 0)
            paramMap.put(token, eachOrderID);
        }

        List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(String.format(queryFormat, sb.toString()),
                paramMap, new PaymentTransactionMapper());

        LinkedHashMap<String, List<PaymentTransaction>> m = new LinkedHashMap<>();
        for (PaymentTransaction each: paymentTransactions) {
            if (!m.containsKey(each.getOrderId())) {
                m.put(each.getOrderId(), new ArrayList<PaymentTransaction>());
            }
            m.get(each.getOrderId()).add(each);
        }
        return m;
    }

    public boolean isCancelledPayment(String orderId) {
    	if(orderId == null || orderId.trim().isEmpty()) {
    		return false;
    	}
    	
        String query = "select * from payment_txn where order_id = :orderId and (recieved_from_pg is null or is_successful = 0)";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("orderId", orderId.toString());

        List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());
        if(paymentTransactions == null || paymentTransactions.isEmpty()) {
            return false;
        }
        
        for(PaymentTransaction paymentTransaction : paymentTransactions) {
            if(paymentTransaction.getSuccessful()) {
                return false;
            }
        }
        
        return true; 
    }
    
    public boolean isExpiredPayment(String orderId) {
        
        if(orderId == null || orderId.trim().isEmpty()) {
            return false;
        }
        
        String query = "select * from payment_txn where order_id = :orderId and sent_to_pg < DATE_SUB(NOW(), INTERVAL 2 HOUR)";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("orderId", orderId.toString());

        List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());
        if(paymentTransactions == null || paymentTransactions.isEmpty()) {
            return false;
        }
        
        for(PaymentTransaction paymentTransaction : paymentTransactions) {
            if(paymentTransaction.getSuccessful()) {
                return false;
            }
        }
        
        return true;
        
    }

    public List<PaymentRefundTransaction> getPaymentRefundTransactions(Date from, Date to, boolean onlySuccesses) {
        String query = "select * from payment_refund_transactions where sent_to_pg >= :from and sent_to_pg <= :to";

        if (onlySuccesses) {
            String successFragment = String.format(" and status='%s'", PaymentRefundTransaction.Status.SUCCESS.getStatusString());
            query = query + successFragment;
        }

        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("from", from);
        paramMap.put("to", to);

        List<PaymentRefundTransaction> paymentRefundTransactions = this.jdbcTemplate.query(query, paramMap, new PaymentRefundTransaction.PaymentRefundTransactionMapper());
        return paymentRefundTransactions;
    }
    
    public List<PaymentRefundTransaction> getSuccessPaymentRefundTransactionsForPaymentTxn(Integer txnId) {
        String query = "select * from payment_refund_transactions where fk_payment_txn_id=:txnId"
                + String.format(" and status in ('%s','%s')",
                        PaymentRefundTransaction.Status.SUCCESS.getStatusString(),
                        PaymentRefundTransaction.Status.INITIATED.getStatusString());
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("txnId", txnId);
        List<PaymentRefundTransaction> paymentRefundTransactions = this.jdbcTemplate.query(query, paramMap,
                new PaymentRefundTransaction.PaymentRefundTransactionMapper());
        return paymentRefundTransactions;
    }
    
    public List<PaymentRefundTransaction> getSuccessfulRefundsForOrderId(String orderId) {
        String query = "select r.* from payment_refund_transactions r, payment_txn p where " +
        		"r.fk_payment_txn_id = p.payment_txn_id and " +
        		"(p.order_id = :orderId or p.mtxn_id = :orderId)" + 
        		String.format(" and status in ('%s','%s')", PaymentRefundTransaction.Status.SUCCESS.getStatusString(),PaymentRefundTransaction.Status.INITIATED.getStatusString());

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);

        List<PaymentRefundTransaction> paymentRefundTransactions = this.jdbcTemplate.query(query, paramMap, new PaymentRefundTransaction.PaymentRefundTransactionMapper());
        return paymentRefundTransactions;
    }
    
    public List<PaymentRefundTransaction> getSuccessfulRefundsForMtxnId(String mtxnId) {
        String query = "select r.* from payment_refund_transactions r, payment_txn p where " +
                "r.fk_payment_txn_id = p.payment_txn_id and p.mtxn_id = :mtxnId" + 
                String.format(" and status in ('%s','%s')", PaymentRefundTransaction.Status.SUCCESS.getStatusString(),PaymentRefundTransaction.Status.INITIATED.getStatusString());

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("mtxnId", mtxnId);

        List<PaymentRefundTransaction> paymentRefundTransactions = this.jdbcTemplate.query(query, paramMap, new PaymentRefundTransaction.PaymentRefundTransactionMapper());
        return paymentRefundTransactions;
    }
    
    public List<PaymentRefundTransaction> getRefundsForMtxnId(String mtxnId) {
        String query = "select r.* from payment_refund_transactions r, payment_txn p where " +
                "r.fk_payment_txn_id = p.payment_txn_id and p.mtxn_id = :mtxnId";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("mtxnId", mtxnId);

        List<PaymentRefundTransaction> paymentRefundTransactions = this.jdbcTemplate.query(query, paramMap, new PaymentRefundTransaction.PaymentRefundTransactionMapper());
        return paymentRefundTransactions;
    }

    public List<PaymentRefundTransaction> getSuccessfulcashBackRefundsForOrderId(String mtxnId, Double refundAmount) {
        String query = "select r.* from payment_refund_transactions r, payment_txn p where " +
                "r.fk_payment_txn_id = p.payment_txn_id and " +
                "p.mtxn_id = :orderId and refunded_amount = :refundedAmount " + 
                String.format(" and status='%s'", PaymentRefundTransaction.Status.SUCCESS.getStatusString());

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", mtxnId);
        paramMap.put("refundedAmount", refundAmount);

        List<PaymentRefundTransaction> paymentRefundTransactions = this.jdbcTemplate.query(query, paramMap, new PaymentRefundTransaction.PaymentRefundTransactionMapper());
        return paymentRefundTransactions;
    }


    public PaymentGatewayMaster getPaymentGatewayMasterForCode(String gatewaycode)
	{
    	try {
			String queryString = "select * from payment_gateway_master where code=:gatewaycode";
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("gatewaycode", gatewaycode);
			
			List<PaymentGatewayMaster> paymentGatewayMasters = this.jdbcTemplate.query(queryString, paramMap, new PgMasterRowMapper());

			if (paymentGatewayMasters != null && paymentGatewayMasters.size() > 0)
				return paymentGatewayMasters.get(0);
			else
				return null;
			
		} catch (RuntimeException re) {
			logger.error("find by property name failed", re);
			throw re;
		}
	}
    
    public List<PaymentTransaction> getSuccessfulPaymentTransactionsByOrderId(String orderId) {
        String query = "select * from payment_txn where order_id = :orderId and is_successful=1 order by sent_to_pg desc";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("orderId", orderId);

        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());

        return paymentTransactions;
    }
    
    public List<InRequest> getMissedRechargeStatus(String affiliateTransId) {
        Map<String, Object> paramMap = new HashMap<String, Object>();

        String query = "select * from  in_request where affiliate_trans_id= :affiliateTransId";

        paramMap.put("affiliateTransId", affiliateTransId);

        List<InRequest> inRequestList = this.jdbcTemplate.query(query, paramMap, new InRequestMapper());
        logger.info("End - Trying to get missed recharges");

        return inRequestList;
    }
    
    public List<PaymentRefundTransaction> getSuccessfulPgRefundsForOrderId(String mtxnId, String paymentGateway) {
        String query = "select r.* from payment_refund_transactions r, payment_txn p where " +
                "r.fk_payment_txn_id = p.payment_txn_id and " +
                "p.mtxn_id = :mtxnId and refund_to = :refundTo " + 
                String.format(" and status IN ('%s','%s')", PaymentRefundTransaction.Status.SUCCESS.getStatusString(),PaymentRefundTransaction.Status.INITIATED.getStatusString());

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("mtxnId", mtxnId);
        paramMap.put("refundTo", paymentGateway);

        List<PaymentRefundTransaction> paymentRefundTransactions = this.jdbcTemplate.query(query, paramMap, new PaymentRefundTransaction.PaymentRefundTransactionMapper());
        return paymentRefundTransactions;
    }
   
    public Boolean getPaymentStatus(Integer paymentTxnId) {
        String query = "select is_successful from payment_txn where payment_txn_id = :id";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("id", paymentTxnId);
        Boolean isSuccessful = this.masterJdbcTemplate.queryForObject(query, paramMap, Boolean.class);
        return isSuccessful;
    }
    
    public List<PaymentTransaction> getTransactionsForPaymentInstrument(Date startTime, Date endTime, int paymentType,
            String paymentOption) {
        String sql = "SELECT * FROM payment_txn WHERE sent_to_pg>=:startTime AND sent_to_pg<=:endTime AND payment_type=:paymenType AND payment_option=:paymentOption";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("startTime", startTime);
        paramMap.put("endTime", endTime);
        paramMap.put("paymenType", paymentType);
        paramMap.put("paymentOption", paymentOption);
        return this.jdbcTemplate.query(sql, paramMap, new PaymentTransactionMapper());
    }

    public List<PaymentTransaction> getAllPaymentTxnForOrderId(String orderId) {
        String query = "select amount, payment_txn_id, mtxn_id, order_id, payment_gateway, payment_mode, payment_option, "
                     + "fk_payment_rq_id, fk_payment_rs_id, payment_type, pg_message, recieved_from_pg, sent_to_pg, is_successful, "
                     + "pg_transaction_id, pg_transaction_time from payment_txn where order_id = :orderId";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());
        return paymentTransactions;
    }

    public List<PaymentTransaction> getPaymentTxnByMtxnId(String mtxnId) {
        String query = "select amount, payment_txn_id, mtxn_id, order_id, payment_gateway, payment_mode, payment_option, "
                     + "fk_payment_rq_id, fk_payment_rs_id, payment_type, pg_message, recieved_from_pg, sent_to_pg, is_successful, "
                     + "pg_transaction_id, pg_transaction_time from payment_txn where mtxn_id = :mtxnId";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("mtxnId", mtxnId);
        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());
        return paymentTransactions;
    }

    public List<PaymentTransaction> getSuccessfulPaymentTxnForOrderId(String orderId) {
        String query = "select amount, payment_txn_id, mtxn_id, order_id, payment_gateway, payment_mode, payment_option, "
                     + "fk_payment_rq_id, fk_payment_rs_id, payment_type, pg_message, recieved_from_pg, sent_to_pg, is_successful, "
                     + "pg_transaction_id, pg_transaction_time from payment_txn where order_id = :orderId and is_successful=1";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());
        return paymentTransactions;
    }

    public List<PaymentTransaction> getFailedPaymentTxnForOrderId(String orderId) {
        String query = "select amount, payment_txn_id, mtxn_id, order_id, payment_gateway, payment_mode, payment_option, "
                     + "fk_payment_rq_id, fk_payment_rs_id, payment_type, pg_message, recieved_from_pg, sent_to_pg, is_successful, "
                     + "pg_transaction_id, pg_transaction_time from payment_txn where order_id = :orderId and is_successful=0";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());
        return paymentTransactions;
    }

    public List<PaymentTransaction> getPendingPaymentTxnForOrderId(String orderId) {
        String query = "select amount, payment_txn_id, mtxn_id, order_id, payment_gateway, payment_mode, payment_option, "
                     + "fk_payment_rq_id, fk_payment_rs_id, payment_type, pg_message, recieved_from_pg, sent_to_pg, is_successful, "
                     + "pg_transaction_id, pg_transaction_time from payment_txn where order_id = :orderId and is_successful is null";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orderId", orderId);
        List<PaymentTransaction> paymentTransactions = this.masterJdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());
        return paymentTransactions;
    }
}
