package com.freecharge.payment.dao.jdbc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.jdbc.mappers.PaymentOptionMasterMapper;
import com.freecharge.payment.dos.entity.PaymentOptionMaster;

public class PaymentOptionConfigDAO {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    public List<PaymentOptionMaster> getNetBankingOptions(List<String> exclusionList) {
        String query = "select id, payment_type, payment_option_code, payment_option_display_name, image_url from payment_option_master where payment_type = 'NB' and payment_option_code not in (:exclusionList) order by payment_option_display_name";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("exclusionList", exclusionList);
        List<PaymentOptionMaster> paymentOptionConfigs = this.jdbcTemplate.query(query, paramMap, new PaymentOptionMasterMapper());
        return paymentOptionConfigs;
    }
    
    public List<PaymentOptionMaster> getAllNetBankingOptions() {
        String query = "select id, payment_type, payment_option_code, payment_option_display_name, image_url from payment_option_master where payment_type = 'NB' order by payment_option_display_name";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        List<PaymentOptionMaster> paymentOptionConfigs = this.jdbcTemplate.query(query, paramMap, new PaymentOptionMasterMapper());
        return paymentOptionConfigs;
    }

}
