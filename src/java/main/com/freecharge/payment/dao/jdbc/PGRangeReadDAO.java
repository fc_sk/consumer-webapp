package com.freecharge.payment.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.entity.jdbc.mappers.PaymentTransactionMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dos.entity.PaymentTransaction;

/**
 * A DAO that's customized for doing range queries against payments tables.
 * @author arun
 *
 */
public class PGRangeReadDAO {
    private static Logger logger = LoggingFactory.getLogger(PGRangeReadDAO.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<PaymentTransaction> getPaymentTxnWithResNullWithTimeLimit(Date rangeFromDate ,Date rangeToDate) {

        logger.info("Entered into the getPaymentTxnWithResNull() of PgReadDAO class");

        try {
            String jdbcQuery = "select * from payment_txn paytxn where (paytxn.recieved_from_pg is NULL or paytxn.is_successful is null or paytxn.is_successful = 0) and sent_to_pg >=:rangeFromDate and sent_to_pg<=:rangeToDate";

            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("rangeFromDate",rangeFromDate);
            paramMap.put("rangeToDate", rangeToDate);
            
            List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(jdbcQuery, paramMap, new PaymentTransactionMapper());
            if (paymentTransactions != null && paymentTransactions.size() > 0) {
                return paymentTransactions;
            } else
                return null;

        } catch (Exception e) {
            return null;
        }
    }

    public List<Integer> getPaymentTxnIdsWithResNullWithTimeLimit(Date rangeFromDate ,Date rangeToDate) {
        List<Integer> returnList = new ArrayList<>();
        
        String jdbcQuery = "select payment_txn_id from payment_txn paytxn where (paytxn.recieved_from_pg is NULL or paytxn.is_successful is null or paytxn.is_successful = 0) and sent_to_pg >=:rangeFromDate and sent_to_pg<=:rangeToDate";

        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("rangeFromDate",rangeFromDate);
        paramMap.put("rangeToDate", rangeToDate);
        
        List<Integer> paymentTransactions = this.jdbcTemplate.query(jdbcQuery, paramMap, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet rs, int row) throws SQLException {
                return rs.getInt("payment_txn_id");
            }
        });
        
        if (paymentTransactions != null) {
            returnList.addAll(paymentTransactions);
        }
        
        return returnList;
    }
    
    public List<PaymentTransaction> getAllPaymentTxnWithTimeLimit(Date rangeFromDate ,Date rangeToDate) {

        logger.info("Entered into the getAllPaymentTxnWithTimeLimit() of PgReadDAO class");

        try {
            String jdbcQuery = "select ptxn.* from payment_txn ptxn"
            		+ " inner join order_id oid on ptxn.order_id = oid.order_id and"
            		+ " oid.created_on >= :rangeFromDate and oid.created_on < :rangeToDate";
 
            Map<String, Object> paramMap = new HashMap<String, Object>();

            paramMap.put("rangeFromDate",rangeFromDate);
            paramMap.put("rangeToDate", rangeToDate);
            
            List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(jdbcQuery, paramMap, new PaymentTransactionMapper());
            if (paymentTransactions != null && paymentTransactions.size() > 0) {
                return paymentTransactions;
            } else
                return null;

        } catch (Exception e) {
            return null;
        }
    }
    
    public List<PaymentTransaction> getPendingPaymentsInLastHourWindow() {
        logger.info("Entered into the getPendingPaymentsInLastHourWindow() of PgReadDAO class");

        String jdbcQuery = "select * from payment_txn paytxn where " +
                "(paytxn.recieved_from_pg is NULL or paytxn.is_successful is null) and " +
                "sent_to_pg >= DATE_SUB(NOW(),INTERVAL 75 MINUTE)  and " +
                "sent_to_pg <= DATE_SUB(NOW(),INTERVAL 15 MINUTE)";

        Map<String, Object> paramMap = new HashMap<String, Object>();

        List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(jdbcQuery, paramMap, new PaymentTransactionMapper());
        if (paymentTransactions != null && paymentTransactions.size() > 0) {
            return paymentTransactions;
        } else {
            return null;
        }
    }
    
    // Failed payments from Android App for timerange
    public List<PaymentTransaction> getFailedPaymentsWithTimeLimit(Date rangeFromDate ,Date rangeToDate){
        logger.info("Entered into the getFailedPaymentsWithTimeLimit() of PgReadDAO class");
        String jdbcQuery = "select * from payment_txn paytxn where (paytxn.recieved_from_pg is NULL or paytxn.is_successful is null or paytxn.is_successful = 0) and sent_to_pg >=:rangeFromDate and sent_to_pg<=:rangeToDate";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("rangeFromDate", rangeFromDate);
        paramMap.put("rangeToDate", rangeToDate);

        List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(jdbcQuery, paramMap, new PaymentTransactionMapper());
        if (paymentTransactions != null && paymentTransactions.size() > 0) {
            return paymentTransactions;
        } else {
            return null;
        }
        
    }
    
    
    public Integer getPaymentTypeByLookupId(String lookupId){
        String query = "select p.payment_type, p.payment_gateway from order_id o join payment_txn p on " +
                "(o.order_id = p.order_id and o.lookup_id = :lookupId);";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("lookupId", lookupId);
        List<Map<String, Object>> results = this.jdbcTemplate.queryForList(query, paramMap);

        if (results!=null && results.size()>0){
            if (results.size()==1){ //This will be the case of payment through credits
                return (Integer) results.get(0).get("payment_type");
            }else {
                for(Map<String, Object> res : results){
                    if (!res.get("payment_gateway").equals("fw")) { //Return actual payment option
                        return (Integer) res.get("payment_type");
                    }
                }
            }
        }
        return null;
    }
}
