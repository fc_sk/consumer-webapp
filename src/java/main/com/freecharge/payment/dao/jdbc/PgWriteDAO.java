package com.freecharge.payment.dao.jdbc;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.freecharge.app.domain.entity.jdbc.mappers.PaymentTransactionMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.Amount;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentRequest;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction.Status;

public class PgWriteDAO {

	private static Logger logger = LoggingFactory.getLogger(PgWriteDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert insertPaymentTransaction;
	private SimpleJdbcInsert insertPaymentRequest;
	private SimpleJdbcInsert insertPaymentResponse;
    private SimpleJdbcInsert insertPaymentRefundTransaction;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertPaymentTransaction = new SimpleJdbcInsert(dataSource).withTableName("payment_txn").usingGeneratedKeyColumns("payment_txn_id", "n_last_updated", "n_created");
        this.insertPaymentRequest = new SimpleJdbcInsert(dataSource).withTableName("payment_rq").usingGeneratedKeyColumns("payment_rq_id", "n_last_updated", "n_created");
        this.insertPaymentResponse = new SimpleJdbcInsert(dataSource).withTableName("payment_rs").usingGeneratedKeyColumns("payment_rs_id", "n_last_updated", "n_created");
        this.insertPaymentRefundTransaction = new SimpleJdbcInsert(dataSource).withTableName("payment_refund_transactions").usingGeneratedKeyColumns("payment_refund_transaction_id", "n_last_updated", "n_created");
	}

	public PaymentTransaction insertPaymentTransaction(PaymentTransaction paymentTransaction) {
		try {
			SqlParameterSource parameters = paymentTransaction.getMapSqlParameterSource();
			Number returnParam = this.insertPaymentTransaction.executeAndReturnKey(parameters);
            paymentTransaction.setPaymentTxnId(returnParam.intValue());
			return paymentTransaction;
		} catch (RuntimeException re) {
			logger.error("insert Payment Transaction  failed", re);
			throw re;
		}
	}

	public PaymentRequest insertPaymentRequest(PaymentRequest paymentRequest) {
		try {
		    
		    if (StringUtils.isNotBlank(paymentRequest.getRequestToGateway())) {
		        logger.info("Not storing Request To Gateway");
		        
		        paymentRequest.setRequestToGateway("MOVEDTOLOG");
		    }
		    
            if (StringUtils.isNotBlank(paymentRequest.getRequestToPp())) {
            	/*
            	 * paymentRequest.getRequestToPp() will expose all the card information like cvv, cardnumber 
            	 * and other sensitive details. Hence removed paymentRequest.getRequestToPp() from the log and in future
            	 * please do not log this information in the log.[Security concern]
            	 */
                logger.info("Not storing Request To PP for payment request Id: " + paymentRequest.getPaymentRqId());
                
                paymentRequest.setRequestToPp("MOVEDTOLOG");
            }
            
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(paymentRequest);
			Number returnParam = this.insertPaymentRequest.executeAndReturnKey(parameters);
			
			paymentRequest.setPaymentRqId(returnParam.intValue());

			return paymentRequest;
		} catch (RuntimeException re) {
			logger.error("insert Payment Request  failed", re);
			throw re;
		}
	}

	public PaymentResponse insertPaymentResponse(PaymentResponse paymentResponse) {
		try {
		    if (paymentResponse.getPgTransactionTime() == null) {
		        paymentResponse.setPgTransactionTime(new Timestamp(System.currentTimeMillis()));
		    }
		    
		    if (StringUtils.isNotBlank(paymentResponse.getResponseData())) {
		        logger.info("Setting to blank PG Response: " + paymentResponse.getResponseData());
		        paymentResponse.setResponseData("MOVEDTOLOG");
		    }
		    
		    if (StringUtils.isNotBlank(paymentResponse.getResponseFromPp())) {
		        logger.debug("Setting to blank Response from PP: " + paymentResponse.getResponseFromPp());
		        paymentResponse.setResponseFromPp("MOVEDTOLOG");
		    }
		    
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(paymentResponse);
			Number returnParam = this.insertPaymentResponse.executeAndReturnKey(parameters);
			
			paymentResponse.setPaymentRsId(returnParam.intValue());
			
			return paymentResponse;
		} catch (RuntimeException re) {
			logger.error("insert Payment Response  failed", re);
			throw re;
		}
	}

    public PaymentRefundTransaction insertPaymentRefundTransaction(PaymentRefundTransaction paymentRefundTransaction) {
        SqlParameterSource sqlParameterSource = paymentRefundTransaction.getMapSqlParameterSource();
        Number primaryKey = this.insertPaymentRefundTransaction.executeAndReturnKey(sqlParameterSource);
        paymentRefundTransaction.setPaymentRefundTransactionId(primaryKey.intValue());
        return paymentRefundTransaction;
    }
    
    public void updatePRTForMtxnId(String mtxnId, Status status) {
        String query = "update payment_refund_transactions r inner join payment_txn p "
                + "on r.fk_payment_txn_id = p.payment_txn_id SET r.status=:status where p.mtxn_id = :mtxnId";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("mtxnId", mtxnId);
        paramMap.put("status", status.getStatusString());

        this.jdbcTemplate.update(query, paramMap);
    }
    
    /*
     * TODO: Remove this
     */
    public void updateRefundedAmountOnRefundTransaction(PaymentRefundTransaction paymentRefundTransaction) {
        String updateQuery = "UPDATE payment_refund_transactions SET refunded_amount=:refundAmount WHERE payment_refund_transaction_id=:refundTxnId";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("refundTxnId", paymentRefundTransaction.getPaymentRefundTransactionId());
        parameterSource.addValue("refundAmount", paymentRefundTransaction.getRefundedAmount().getAmount());
        this.jdbcTemplate.update(updateQuery, parameterSource);
    }
    
    public void updateRefundTransaction(PaymentRefundTransaction paymentRefundTransaction, Integer refundTxnId) {
        String updateQuery = "UPDATE payment_refund_transactions SET pg_transaction_reference_no=:pg_transaction_reference_no, status=:status,"
                + " sent_to_pg=:sent_to_pg, received_from_pg=:received_from_pg, refunded_amount=:refunded_amount, raw_pg_request=:raw_pg_request,"
                + " raw_pg_response=:raw_pg_response, pg_response_code=:pg_response_code, pg_response_description=:pg_response_description,"
                + " pg_status=:pg_status, refund_to=:refund_to, fk_payment_txn_id=:fk_payment_txn_id WHERE payment_refund_transaction_id=:refundTxnId";
        MapSqlParameterSource parameterSource = paymentRefundTransaction.getMapSqlParameterSource();
        parameterSource.addValue("refundTxnId", refundTxnId);
        this.jdbcTemplate.update(updateQuery, parameterSource);
    }

	public int updatePaymentTransaction(PaymentTransaction pt) {
		try {
            String updateQuery = "update payment_txn pt set pt.fk_payment_rs_id =:paymentResponseId "
                    + ", pt.pg_message=:pgMessage" + ", pt.recieved_from_pg =:recievedToPg "
                    + ", pt.is_successful =:successful" + ", pt.pg_transaction_id=:transactionId "
                    + ", pt.pg_transaction_time=:pgTransactionTime " + " where pt.payment_txn_id = :id "
                    + "and (pt.is_successful is NULL or pt.is_successful=0)";

			Map<String, Object> paramMap = new HashMap<String, Object>();

			paramMap.put("paymentResponseId", pt.getPaymentResponseId());
			paramMap.put("pgMessage", pt.getPgMessage());
			paramMap.put("recievedToPg", pt.getRecievedToPg());
			paramMap.put("successful", pt.getSuccessful());
			paramMap.put("transactionId", pt.getTransactionId());
			paramMap.put("pgTransactionTime",pt.getPgTransactionTime());
			paramMap.put("id", pt.getPaymentTxnId());
			
			int count = this.jdbcTemplate.update(updateQuery, paramMap);
			return count;
		} catch (RuntimeException re) {
			logger.error("find updating txns ", re);
			throw re;
		}
	}
	
	public int updatePaymentTransactionDetails(PaymentTransaction pt) {
		logger.info("Upddating payment transactions ");
		try {
            String updateQuery = "update payment_txn pt set pt.fk_payment_rs_id =:paymentResponseId "
                    + ", pt.pg_message=:pgMessage" + ", pt.recieved_from_pg =:recievedToPg "
                    + ", pt.is_successful =:successful" + ", pt.pg_transaction_id=:transactionId "
                    + ", pt.pg_transaction_time=:pgTransactionTime, pt.payment_type=:paymentType"
                    + ", pt.payment_option=:paymentOption, pt.payment_gateway=:pgUsed " + " where pt.payment_txn_id = :id";

			Map<String, Object> paramMap = new HashMap<String, Object>();

			paramMap.put("paymentResponseId", pt.getPaymentResponseId());
			paramMap.put("pgMessage", pt.getPgMessage());
			paramMap.put("recievedToPg", pt.getRecievedToPg());
			paramMap.put("successful", pt.getSuccessful());
			paramMap.put("transactionId", pt.getTransactionId());
			paramMap.put("pgTransactionTime",pt.getPgTransactionTime());
			paramMap.put("paymentType",pt.getPaymentType());
			paramMap.put("paymentOption",pt.getPaymentOption());
			paramMap.put("pgUsed", pt.getPaymentGateway());
			paramMap.put("id", pt.getPaymentTxnId());
			
			int count = this.jdbcTemplate.update(updateQuery, paramMap);
			return count;
		} catch (RuntimeException re) {
			logger.error("find updating txns ", re);
			throw re;
		}
	}

	public void updatePaymentTransactionAmount(PaymentTransaction pt) {
		logger.info("Upddating payment transactions ");
		try {
			String updateQuery = "update payment_txn pt set pt.amount =:amount  where pt.payment_txn_id = :id ";

			Map<String, Object> paramMap = new HashMap<String, Object>();

			paramMap.put("amount", pt.getAmount());
			paramMap.put("id", pt.getPaymentTxnId());
			
			this.jdbcTemplate.update(updateQuery, paramMap);

		} catch (RuntimeException re) {
			logger.error("find updating txns ", re);
			throw re;
		}
	}
	
    public List<PaymentTransaction> getSuccessfulPaymentTransactionsByOrderId(String orderId) {
        String query = "select * from payment_txn where order_id = :orderId and is_successful=1 order by sent_to_pg desc";
        Map<String, Object> paramMap = new HashMap<String, Object>();

        paramMap.put("orderId", orderId);

        List<PaymentTransaction> paymentTransactions = this.jdbcTemplate.query(query, paramMap, new PaymentTransactionMapper());

        return paymentTransactions;
    }
}
