package com.freecharge.payment.dao.jdbc;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import com.freecharge.common.util.FCConstants;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.services.RefundSchedulerDO;
import com.freecharge.payment.services.RefundSchedulerRowMapper;
import com.freecharge.payment.services.RefundStatus;

public class PaymentRefundDAO {
    private static Logger              logger = LoggingFactory.getLogger(PaymentRefundDAO.class);

    private NamedParameterJdbcTemplate slaveJdbcTemplate;
    private NamedParameterJdbcTemplate masterJdbcTemplate;
    private SimpleJdbcInsert           insertJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.slaveJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    @Autowired
    public void setMasterDataSource(DataSource dataSource) {
        this.masterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.insertJdbcTemplate = new SimpleJdbcInsert(dataSource).withTableName("refund_scheduler")
                .usingGeneratedKeyColumns("created_on", "last_updated", "n_last_updated", "n_created");
    }

    @Transactional
    public RefundSchedulerDO insertRefundSchedule(RefundSchedulerDO refundScheduler) {
        try {
            SqlParameterSource parameters = new RefundSchedulerBeanPropertySqlParameterSource(refundScheduler);
            this.insertJdbcTemplate.execute(parameters);
            return refundScheduler;
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
            throw re;
        }
    }

    @Transactional(readOnly=true)
    public RefundSchedulerDO getRefundScheduleForMtxn(String mtxnID) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        try {
            String queryString = "select * from refund_scheduler where mtxn_id=:mtxnId";
            paramMap.put("mtxnId", mtxnID);
            List<RefundSchedulerDO> refundSchedulerDO = this.masterJdbcTemplate.query(queryString, paramMap,
                    new RefundSchedulerRowMapper());
            if (refundSchedulerDO != null && refundSchedulerDO.size() > 0) {
                return refundSchedulerDO.get(0);
            }
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
        }
        return null;
    }

    @Transactional
    public void updateRefundScheduleForMtxn(RefundSchedulerDO refundSchedulerDO) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        try {
            String queryString = "update refund_scheduler set status=:status, expiry_time=:expiryTime, "
                    + "schedule_count=:scheduleCount where mtxn_id=:mtxnId";
            paramMap.put("mtxnId", refundSchedulerDO.getMtxnId());
            paramMap.put("status", refundSchedulerDO.getStatus().getStatus());
            paramMap.put("expiryTime", refundSchedulerDO.getExpiryTime());
            paramMap.put("scheduleCount", refundSchedulerDO.getScheduleCount());
            this.masterJdbcTemplate.update(queryString, paramMap);
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
        }
    }
    
    @Transactional
    public void updateRefundScheduleStatusForMtxn(String mtxnId, RefundStatus refundStatus) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        try {
            String queryString = "update refund_scheduler set status=:status where mtxn_id=:mtxnId";
            paramMap.put("mtxnId", mtxnId);
            paramMap.put("status", refundStatus.getStatus());
            this.masterJdbcTemplate.update(queryString, paramMap);
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
        }
    }
    
    @Transactional(readOnly = true)
    public List<RefundSchedulerDO> getScheduledRefunds(DateTime createdFrom, DateTime createdTo) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        try {
            String queryString = "select * from refund_scheduler where "
                    + " created_on >=:createdFrom and created_on<=:createdTo "
                    + " and status=1 and expiry_time <= current_timestamp";
            paramMap.put("createdFrom", new Timestamp(createdFrom.getMillis()));
            paramMap.put("createdTo", new Timestamp(createdTo.getMillis()));
            return this.slaveJdbcTemplate.query(queryString, paramMap, new RefundSchedulerRowMapper());
        } catch (RuntimeException re) {
            logger.error("find by property name failed", re);
        }
        return null;
    }
}
