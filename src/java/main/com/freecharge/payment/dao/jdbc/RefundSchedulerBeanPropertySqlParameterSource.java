package com.freecharge.payment.dao.jdbc;

import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;

import com.freecharge.payment.services.RefundStatus;

class RefundSchedulerBeanPropertySqlParameterSource extends BeanPropertySqlParameterSource {

    public RefundSchedulerBeanPropertySqlParameterSource(Object object) {
        super(object);
    }

    @Override
    public Object getValue(String paramName) {
        Object result = super.getValue(paramName);
        if (result != null && result instanceof RefundStatus) {
            return ((RefundStatus) result).getStatus();
        } else {
            return result;
        }

    }
}
