package com.freecharge.payment.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.common.framework.logging.LoggingFactory;

public class PaymentGatewayManagementDAO implements IPaymentGatewayManagementDAO{

    private static Logger logger = LoggingFactory.getLogger(PaymentGatewayManagementDAO.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<Map<String,Object>> getActivePaymentGateways(){
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        String query = "select payment_gateway_master_id id, name, code " +
                "from payment_gateway_master where is_active = 1";

        return this.jdbcTemplate.queryForList(query,parameters);
        
    }

    public List<Map<String,Object>> getActiveBankGroup(){
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        String query = "select banks_grp_master_id id, name, code_ui, payment_option, display_name " +
                "from banks_grp_master where is_active =1";

        return this.jdbcTemplate.queryForList(query,parameters);
    }

    public List<Map<String,Object>> getDefaultData(){

        HashMap<String, Object> parameters = new HashMap<String, Object>();
        String query = "select pr.pg_relation_id id, pm.product_name prname, bk.name bkname, pg.name pgname " +
                "from pg_relation pr, product_master pm, banks_grp_master bk, payment_gateway_master pg "+
                    "where pr.fk_product_master_id = pm.product_master_id and "+
                    "pr.fk_banks_grp_master_id = bk.banks_grp_master_id and "+
                    "pr.primary_gateway = pg.payment_gateway_master_id";

        return this.jdbcTemplate.queryForList(query,parameters);

        }

    public List<Map<String,Object>> getData(String whereClause){

        HashMap<String, Object> parameters = new HashMap<String, Object>();
        String subQuery = "select pr.pg_relation_id id, pm.product_name prname, bk.name bkname, pg.name pgname " +
                "from pg_relation pr, product_master pm, banks_grp_master bk, payment_gateway_master pg "+
                    "where pr.fk_product_master_id = pm.product_master_id and "+
                    "pr.fk_banks_grp_master_id = bk.banks_grp_master_id and "+
                    "pr.primary_gateway = pg.payment_gateway_master_id ";

        String query = subQuery + whereClause;



        return this.jdbcTemplate.queryForList(query,parameters);

        }

    public void updatePG(Integer pgRelationId, Integer gatewayId){

        String query = "update pg_relation set primary_gateway = "+gatewayId.intValue()+ " where pg_relation_id = "+pgRelationId.intValue();
        this.jdbcTemplate.update(query,new HashMap());

    }

}
