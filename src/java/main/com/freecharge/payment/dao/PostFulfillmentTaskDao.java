package com.freecharge.payment.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.freecharge.app.domain.entity.jdbc.mappers.PostFFTaskRowMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.post.fulfillment.state.PostFulfillmentTaskState;
import com.freecharge.web.webdo.PostFFTaskModel;

@Component
public class PostFulfillmentTaskDao {

	private static Logger logger = LoggingFactory.getLogger(PostFulfillmentTaskDao.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
		
	@Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }


public void insertSkippedTask(String rawRequest,PostFulfillmentTaskState state,String orderId,String reason,Date created) {
		final String queryString = "insert into skipped_post_recharge_tasks(order_id,state,retry_count,raw_request,reason,created) "
				+ "values (:orderId,:state,:retryCount,:rawRequest,:reason,:created)";
		MapSqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("orderId",orderId)
				.addValue("state", state.name())
				.addValue("retryCount", 0)
				.addValue("rawRequest", rawRequest)
				.addValue("reason", reason)
				.addValue("created", new Date());
	
		int updatedRows = -1;
		try {
		updatedRows = this.jdbcTemplate.update(queryString, parameters);
		} catch(Exception e) {
            logger.info("Exception while inserting SkippedTask for rawRequest : "+ rawRequest+" and orderId : "+orderId+": Exception : "+e);
		}
		if (updatedRows == 1) {
            logger.info("inserted SkippedTask for rawRequest : "+ rawRequest+" and orderId : "+orderId);
        }
		
	}

public void incrementRetryCount(PostFulfillmentTaskState state,String reason, Long id) {
	
	final String queryString = "update skipped_post_recharge_tasks set state = :state ,reason = :reason,retry_count=retry_count+1 where id = :id";
	
	MapSqlParameterSource parameters = new MapSqlParameterSource()
			.addValue("state", state.name())
			.addValue("id", id)
			.addValue("reason", reason);

	int updatedRows = -1;
	try {
	updatedRows = this.jdbcTemplate.update(queryString, parameters);
	} catch(Exception e) {
        logger.info("Exception while updating SkippedTask for  id : "+id+": Exception : "+e);
	}
	if (updatedRows == 1) {
        logger.info("updated SkippedTask for id : "+id);
    }
	
}

public void updateTaskState(PostFulfillmentTaskState state,Long id,String reason) {
	
	final String queryString = "update skipped_post_recharge_tasks set state = :state ,reason = :reason where id = :id";
	
	MapSqlParameterSource parameters = new MapSqlParameterSource()
			.addValue("state", state.name())
			.addValue("id", id)
			.addValue("reason", reason);

	int updatedRows = -1;
	try {
	updatedRows = this.jdbcTemplate.update(queryString, parameters);
	} catch(Exception e) {
        logger.info("Exception while updating SkippedTask for  id : "+id+": Exception : "+e);
	}
	if (updatedRows == 1) {
        logger.info("updated SkippedTask for id : "+id);
    }
	
}



	
@SuppressWarnings("unchecked")
public List<PostFFTaskModel> getSkippedTasksInDateRange(Date startTime , Date endTime,int retryCount) {
	List<PostFFTaskModel> tasks = null;
	try {
			final String queryString = "select * from skipped_post_recharge_tasks where "
					+ "updated >:startTime and updated <:endTime and retry_count<:retryCount and "
					+ "state in('CAMPAIGN_NOTIFICATION_MISSED','COMM_NOTIFICATION_MISSED','TSM_NOTIFICATION_MISSED','CAMPAIGN_NOTIFICATION_IN_PROGRESS',"
					+ "'COMM_NOTIFICATION_IN_PROGRESS','TSM_NOTIFICATION_IN_PROGRESS')";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("startTime", startTime);
		paramMap.put("endTime", endTime);
		paramMap.put("retryCount", retryCount);

         logger.info(queryString);
		tasks = this.jdbcTemplate.query(queryString, paramMap, new PostFFTaskRowMapper());
		if(tasks == null || tasks.size() == 0){
			logger.error("no skipped task exist for range, from : " + startTime+" , to : "+endTime);
			return null;
		}
		return tasks;
	} catch (Exception re) {
		logger.error("Exception getting SkippedTasksInDateRange, from : " + startTime+" , to : "+endTime+", reason : "+re);
	}
	return tasks;
}

@SuppressWarnings("unchecked")
public List<PostFFTaskModel> findByOrderId(String orderId) {
	List<PostFFTaskModel> tasks = null;
	try {
			final String queryString = "select * from skipped_post_recharge_tasks where order_id=:orderId";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("orderId", orderId);

		tasks = this.jdbcTemplate.query(queryString, paramMap, new PostFFTaskRowMapper());
		if(tasks == null || tasks.size() == 0){
			logger.error("no skipped task exist for orderId : " + orderId);
			return null;
		}
		return tasks;
	} catch (Exception re) {
		logger.error("Exception getting skipped task for orderId : " + orderId);
	}
	return tasks;
}


//need to review
@SuppressWarnings("unchecked")
public PostFFTaskModel findByOrderIdAndStatus(String orderId, PostFulfillmentTaskState state) {
	List<PostFFTaskModel> tasks = null;
		PostFFTaskModel task = null;
		try {
			final String queryString = "select * from skipped_post_recharge_tasks where order_id=:orderId and state= :state";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("orderId", orderId);
		paramMap.put("state", state.name());

			tasks = this.jdbcTemplate.query(queryString, paramMap, new PostFFTaskRowMapper());
			if (!CollectionUtils.isEmpty(tasks) && tasks.size() > 1) {
				throw new RuntimeException("Invalid Data Present For OrderId " + orderId + ", STATE " + state);
			} else if (!CollectionUtils.isEmpty(tasks)) {
				task = tasks.get(0);
			}
		} catch (Exception re) {
			logger.error("Exception getting skipped task for orderId : " + orderId);
		}
		return task;
}
}
