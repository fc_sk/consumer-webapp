package com.freecharge.payment.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dao.mappers.CardBinRangeMapper;
import com.freecharge.payment.dao.mappers.CardIssuerMapper;
import com.freecharge.payment.services.binrange.BinList;
import com.freecharge.payment.services.binrange.CardBinRange;
import com.freecharge.payment.services.binrange.CardIssuer;

@Component
public class CardBinRangeDAO {
    private static Logger logger = LoggingFactory.getLogger(CardBinRangeDAO.class);
    private NamedParameterJdbcTemplate jdbcTemplate;
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    public List<CardIssuer> getCardIssuers(String binHead, String binTail) {
        String retrieveQuery = "select " +
        		"ci.id, ci.card_issuer_id, ci.card_issuer_name, " +
        		"cbr.card_nature, cbr.card_type from " +
        		"card_issuer ci, card_bin_range cbr where " +
        		"(ci.card_issuer_id = cbr.fk_card_issuer_id) and " +
        		"(" +
        		"(cbr.bin_head = :binHead and cbr.bin_tail = :binTail) or " +
        		"(cbr.bin_head = :binHead and cbr.bin_tail = '*') or " +
        		"(cbr.bin_head = '*' and cbr.bin_tail = :binTail)" +
        		");";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put("binHead", binHead);
        queryParams.put("binTail", binTail);
        
        List<CardIssuer> cardIssuerList = this.jdbcTemplate.query(retrieveQuery, queryParams, new CardIssuerMapper());
        
        if (cardIssuerList == null) {
            return new ArrayList<>();
        }
        
        return cardIssuerList;
    }
    
    public List<CardBinRange> getCardBinRanges(String binHead, String binTail) {
        String retrieveQuery = "select " +
                "cbr.id, cbr.bin_range_id, cbr.bin_head, " +
                "cbr.bin_tail, cbr.card_type, cbr.card_nature, " +
                "cbr.fk_card_issuer_id, cbr.status, cbr.created_on, cbr.fk_card_category_id from " +
                "card_bin_range cbr where " +
                "(cbr.bin_head = :binHead and cbr.bin_tail = :binTail) or " +
                "(cbr.bin_head = :binHead and cbr.bin_tail = '*') or " +
                "(cbr.bin_head = '*' and cbr.bin_tail = :binTail)";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put("binHead", binHead);
        queryParams.put("binTail", binTail);
        
        List<CardBinRange> cardBinRangeList = this.jdbcTemplate.query(retrieveQuery, queryParams,
                new CardBinRangeMapper());
        
        if (cardBinRangeList == null) {
            return new ArrayList<>();
        }
        
        return cardBinRangeList;
    }
    
    public void insertNewCardBinRange(String binHead, String binTail, BinList binList, String cardIssuerId) {
        String sql = "INSERT IGNORE INTO card_bin_range(bin_range_id, bin_head, bin_tail, card_type, card_nature, fk_card_issuer_id, status, fk_card_category_id) " +
                "VALUES (:binRangeId, :binHead, :binTail, :cardType, :cardNature, :cardIssuerId, :status, :cardCategoryId)";
        
        Map<String, String> params = FCUtil.createMap(
            "binRangeId", UUID.randomUUID().toString(),
            "binHead", binHead,
            "binTail", binTail,
            "cardType", binList.getBrand(),
            "cardNature", binList.getCardType(),
            "cardIssuerId", cardIssuerId,
            "status", "Active",
            "cardCategoryId", binList.getCardCategory()
        );

        int updatedRows = this.jdbcTemplate.update(sql, params);

        if (updatedRows == 1) {
            logger.info(String.format("%s card bin range added", binList));
        }
    }
    
    public void updateCardBinRange(String id, BinList binList, String cardIssuerId) {
        String sql = "UPDATE card_bin_range SET card_type=:cardType, card_nature=:cardNature, fk_card_issuer_id=:cardIssuerId, fk_card_category_id=:cardCategoryId " +
                "WHERE id=:id";
        
        cardIssuerId = cardIssuerId == null ? "": cardIssuerId;
        Map<String, String> params = FCUtil.createMap(
            "cardType", binList.getBrand(),
            "cardNature", binList.getCardType(),
            "cardIssuerId", cardIssuerId,
            "cardCategoryId", binList.getCardCategory(),
            "id", id
        );

        int updatedRows = this.jdbcTemplate.update(sql, params);

        if (updatedRows == 1) {
            logger.info(String.format("%s card bin range Updated", binList));
        }
    }
    
    public String getCardIssuerFromId(String cardIssuerId) {
        if (cardIssuerId == null) {
            return null;
        }
        String retrieveQuery = "select card_issuer_name from card_issuer where card_issuer_id=:cardIssuerId";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put("cardIssuerId", cardIssuerId);
        
        String cardIssuerName = this.jdbcTemplate.queryForObject(retrieveQuery, queryParams, String.class);
        
        return cardIssuerName;
    }
}
