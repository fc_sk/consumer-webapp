package com.freecharge.payment.dao;

import com.freecharge.app.domain.dao.ICommonDao;
import com.freecharge.payment.dos.entity.PaymentResponse;

public interface IPaymentResponseDAO  extends ICommonDao<PaymentResponse> {
}
