package com.freecharge.payment.dao;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeAction;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughputExceededException;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.amazonaws.services.dynamodbv2.model.UpdateItemResult;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dao.mappers.KlickpayPaymentMetaRowMapper;
import com.freecharge.payment.dao.mappers.KpayPaymentMetaRowMapper;
import com.freecharge.payment.dos.entity.KlickpayPaymentMeta;
import com.freecharge.payment.dos.entity.KpayPaymentMeta;
import com.freecharge.platform.metrics.MetricsClient;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Component
public class KpayPaymentMetaDAO {
	
	private static Logger logger = LoggingFactory.getLogger(KpayPaymentMetaDAO.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert insertKpayPaymentMeta;
	
	@Autowired
    MetricsClient metricsClient;

	private final static String M_TXN_ID = "mtxnId";
	private final static String PG_USED = "pgUsed";
	private final static String REFERENCE_ID = "referenceId";
	private final static String METADATA = "metadata";
	private final static String CREATED_ON = "createdOn";
	private final static String SERIALIZATION_VERSION = "sVersion";
	private final static String TABLE_NAME = "klickpay_payment_meta";
	

	@Autowired
	AmazonDynamoDBAsync dynamoClientMum;

	private static final String ALL_COLUMNS = "id, mtxn_id, pg_used, reference_id, metadata, created_on";

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		insertKpayPaymentMeta = new SimpleJdbcInsert(dataSource).withTableName("klickpay_payment_meta").
				usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
	}

	public void insert(KlickpayPaymentMeta klickpayPaymentMeta) {
		
//		SqlParameterSource parameters = klickpayPaymentMeta.getMapSqlParameterSource();
//		Number returnParam = this.insertKpayPaymentMeta.executeAndReturnKey(parameters);

		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		item.put(M_TXN_ID, new AttributeValue(klickpayPaymentMeta.getMtxnId()));
		item.put(PG_USED, new AttributeValue(klickpayPaymentMeta.getPgUsed()));
		item.put(REFERENCE_ID, new AttributeValue(klickpayPaymentMeta.getReferenceId()));
		item.put(SERIALIZATION_VERSION,new AttributeValue("1"));
		item.put(METADATA, new AttributeValue(klickpayPaymentMeta.getMetadata()));
		item.put(CREATED_ON, new AttributeValue(String.valueOf(new Date().getTime())));

		try {
			logger.info("Attempting to insert in dynamo for mtxnId: "+klickpayPaymentMeta.getMtxnId()+" and referenceId: "+klickpayPaymentMeta.getReferenceId());
		    PutItemResult result = dynamoClientMum.putItem(new PutItemRequest()
			.withConditionExpression("attribute_not_exists(mtxnId)")
			.withTableName(TABLE_NAME)
			.withItem(item));
			logger.info("Successfully added item in dynamo for mtxnId: "+klickpayPaymentMeta.getMtxnId()+" and referenceId: "+klickpayPaymentMeta.getReferenceId());
		}catch(ProvisionedThroughputExceededException ex) {
			logger.error("ProvisionedThroughputExceededException exception caught for mTxnId: " + klickpayPaymentMeta.getMtxnId(), ex);
			metricsClient.recordEvent("KlickpayPaymentMeta", "putItem", "ProvisionedThroughputExceededException");
			throw ex;
		}catch(ConditionalCheckFailedException ex) {
			logger.error("ConditionalCheckFailedException exception caught for mTxnId: " + klickpayPaymentMeta.getMtxnId(), ex);
			metricsClient.recordEvent("KlickpayPaymentMeta", "putItem", "ConditionalCheckFailedException");
		}
		catch (RuntimeException e) {
			logger.error("Exception caught for mTxnId: " + klickpayPaymentMeta.getMtxnId(), e);
			throw e;
		}
	}

	public List<KpayPaymentMeta> getOldKpayPaymentMeta(String mtxnId) {
		String sqlQuery = "Select * from kpay_payment_meta where mtxn_id = :mtxnId";
		Map<String, Object> param = new HashMap<>();
		param.put("mtxnId", mtxnId);
		return jdbcTemplate.query(sqlQuery, param, new KpayPaymentMetaRowMapper());
	}

	public List<KlickpayPaymentMeta> getKpayPaymentMeta(String mtxnId) {

		List<String> attrList = new LinkedList<String>();
		attrList.add(M_TXN_ID);
		attrList.add(PG_USED);
		attrList.add(CREATED_ON);
		attrList.add(METADATA);
		attrList.add(SERIALIZATION_VERSION);
		attrList.add(REFERENCE_ID);


		Map<String, AttributeValue> requestKey = new HashMap<String, AttributeValue>();
		requestKey.put(M_TXN_ID, new AttributeValue(mtxnId));
		GetItemRequest getItemRequest = new GetItemRequest().withTableName(TABLE_NAME)
				.withAttributesToGet(attrList).withKey(requestKey);
		getItemRequest.setAttributesToGet(attrList);
		getItemRequest.setConsistentRead(true);
		GetItemResult result = null;
		try {
			result = dynamoClientMum.getItem(getItemRequest);
		} catch (ProvisionedThroughputExceededException ex) {
			logger.error("ProvisionedThroughputExceededException exception caught for mTxnId: " + mtxnId, ex);
			metricsClient.recordEvent("KlickpayPaymentMeta", "getItem", "ProvisionedThroughputExceededException");
			throw ex;
		} catch (RuntimeException e) {
			logger.error("Exception caught for mTxnId: " + mtxnId, e);
			throw e;
		}
		
		List<KlickpayPaymentMeta> response = new ArrayList();
		if(result != null && result.getItem() != null) {
			try {
				Map<String, AttributeValue> mappingItem = result.getItem();
				String mTxnId =  mappingItem.get(M_TXN_ID).getS();
				String pgUsed =  mappingItem.get(PG_USED).getS();
				String createdOnStr =  mappingItem.get(CREATED_ON).getS();
				Long createdOnLong = Long.valueOf(createdOnStr);
				Date createdOn = new Date(createdOnLong);
				String referenceId = mappingItem.get(REFERENCE_ID).getS();
				String metaData =  mappingItem.get(METADATA).getS();
				String apiVersion =  mappingItem.get(SERIALIZATION_VERSION).getS();
				KlickpayPaymentMeta klickPayMeta = new KlickpayPaymentMeta();
				klickPayMeta.setCreatedOn(createdOn);
				klickPayMeta.setMetadata(metaData);
				klickPayMeta.setMtxnId(mTxnId);
				klickPayMeta.setPgUsed(pgUsed);
				klickPayMeta.setReferenceId(referenceId);
				response.add(klickPayMeta);
				return response;
			} catch (Exception e) {
				logger.error("Exception caught for mtxnId: " + mtxnId,e);
				return response;
			}
		}else {


			String sqlQuery = "Select "+ALL_COLUMNS+" from klickpay_payment_meta where mtxn_id = :mtxnId";
			Map<String, Object> param = new HashMap<>();
			param.put("mtxnId", mtxnId);
			List<KlickpayPaymentMeta> resultList = jdbcTemplate.query(sqlQuery, param, new KlickpayPaymentMetaRowMapper());
			if(null == resultList || resultList.isEmpty()) {
				List<KpayPaymentMeta> oldKpayPaymentMetaList = getOldKpayPaymentMeta(mtxnId);
				resultList = new ArrayList<KlickpayPaymentMeta>();
				for(KpayPaymentMeta kpayPaymentMeta : oldKpayPaymentMetaList) {
					KlickpayPaymentMeta klickpayPaymentMeta = new KlickpayPaymentMeta();
					klickpayPaymentMeta.setId(kpayPaymentMeta.getId());
					klickpayPaymentMeta.setMtxnId(kpayPaymentMeta.getMtxnId());
					klickpayPaymentMeta.setPgUsed(kpayPaymentMeta.getPgUsed());
					klickpayPaymentMeta.setCreatedOn(null);
					klickpayPaymentMeta.setReferenceId(null);
					klickpayPaymentMeta.setMetadata(null);
					resultList.add(klickpayPaymentMeta);
				}
			}
			return resultList;
		}
	}

}
