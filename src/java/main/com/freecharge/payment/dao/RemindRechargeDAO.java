package com.freecharge.payment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.rest.recharge.RemindRecharge;
import com.freecharge.rest.recharge.RemindRecharge.ReminderAction;

@Component
public class RemindRechargeDAO {

	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public void insertRemindRecharge(RemindRecharge remindRecharge) {
		String insertQuery = "INSERT INTO user_recharge_reminder (user_id, service_no, operator_name, reminder_date, amount, inserted_on, is_active, order_id, action, pattern, plan_id) VALUES "
				+ " (:userId,:serviceNo,:operatorName,:remindOn,:amount,:insertedOn,:isActive, :orderId, :action, :pattern, :planId)";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", remindRecharge.getUserId());
		params.put("serviceNo", remindRecharge.getRechargeNo());
		params.put("operatorName", remindRecharge.getOperatorName());
		params.put("remindOn", remindRecharge.getRemindOn());
		params.put("amount", remindRecharge.getRechargeAmount());
		params.put("insertedOn", remindRecharge.getInsertedOn());
		params.put("isActive", remindRecharge.isActive());
		params.put("orderId", remindRecharge.getOrderId());
		params.put("action", remindRecharge.getReminderAction().toString());
		params.put("pattern", remindRecharge.getPattern());
		params.put("planId", remindRecharge.getPlanId());

		jdbcTemplate.update(insertQuery, params);
	}

	public List<RemindRecharge> getReminderForDay() {
		String queryString = "SELECT * FROM user_recharge_reminder WHERE DATE(reminder_date)=:reminderDate and is_active=1";
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Object> params = new HashMap<>();
		params.put("reminderDate", dateFormat.format(new Date()));

		List<RemindRecharge> reminderList = jdbcTemplate.query(queryString,
				params, new RemindRechargeRowMapper());
		return reminderList;
	}

	public void updateRemindRecharge(RemindRecharge remindRecharge) {
		String updateQuery = "UPDATE user_recharge_reminder SET user_id=:userId, service_no=:serviceNo, operator_name=:operatorName, "
				+ "amount=:amount, is_active=:isActive, order_id=:orderId, plan_id=:planId where reminder_id=:reminderId";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", remindRecharge.getUserId());
		params.put("serviceNo", remindRecharge.getRechargeNo());
		params.put("operatorName", remindRecharge.getOperatorName());
		params.put("amount", remindRecharge.getRechargeAmount());
		params.put("isActive", remindRecharge.isActive());
		params.put("orderId", remindRecharge.getOrderId());
		params.put("reminderId", remindRecharge.getReminderId());
		params.put("planId", remindRecharge.getPlanId());
		jdbcTemplate.update(updateQuery, params);
	}

	public RemindRecharge getReminderForUserNumber(Integer userId,
			String serviceNumber, Double amount) {
		String querySQL = "SELECT * FROM user_recharge_reminder WHERE user_id=:userId AND service_no=:serviceNo AND amount=:amount AND is_active=1 AND reminder_date>:remindDate";
		Map<String, Object> params = new HashMap<>();
		params.put("userId", userId);
		params.put("serviceNo", serviceNumber);
		params.put("amount", amount);
		params.put("remindDate", new Date());
		List<RemindRecharge> reminderList = jdbcTemplate.query(querySQL,
				params, new RemindRechargeRowMapper());
		if (reminderList == null || reminderList.isEmpty()) {
			return null;
		}
		return reminderList.get(0);
	}

	public static final class RemindRechargeRowMapper implements
			RowMapper<RemindRecharge> {

		@Override
		public RemindRecharge mapRow(ResultSet rs, int arg1)
				throws SQLException {
			RemindRecharge remindRecharge = new RemindRecharge();
			remindRecharge.setInsertedOn(rs.getDate("inserted_on"));
			remindRecharge.setOperatorName(rs.getString("operator_name"));
			remindRecharge.setRechargeAmount(rs.getDouble("amount"));
			remindRecharge.setRechargeNo(rs.getString("service_no"));
			remindRecharge.setReminderId(rs.getLong("reminder_id"));
			remindRecharge.setRemindOn(rs.getDate("reminder_date"));
			remindRecharge.setUserId(rs.getInt("user_id"));
			remindRecharge.setActive(rs.getBoolean("is_active"));
			remindRecharge.setOrderId(rs.getString("order_id"));
			remindRecharge.setReminderAction(ReminderAction.valueOf(rs
					.getString("action")));
			remindRecharge.setPattern(rs.getInt("pattern"));
			remindRecharge.setPlanId(rs.getInt("plan_id"));
			return remindRecharge;
		}

	}
}
