package com.freecharge.payment.dao;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.app.service.InCachedService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.google.common.collect.ImmutableMap;

/**
 * User: abhi
 * Date: 17/12/13
 * Time: 11:13 AM
 */
@Component
public class CardIssuerDao {
    private static Logger logger = LoggingFactory.getLogger(CardIssuerDao.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private InCachedService inCachedService;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * Insert a card issuer name into card_issuer table only if the name is not already present.
     * @param cardIssuerName
     */
    private void insertCardIssuer(String cardIssuerName) {
        String sql = "INSERT IGNORE INTO card_issuer(card_issuer_name, card_issuer_id) " +
                "VALUES (:cardIssuerName, :cardIssuerId)";

        Map<String, String> params = ImmutableMap.of(
            "cardIssuerName", cardIssuerName,
            "cardIssuerId", UUID.randomUUID().toString()
        );

        int updatedRows = this.jdbcTemplate.update(sql, params);

        if (updatedRows == 1) {
            logger.info(String.format("%s card issuer added", cardIssuerName));
        }
    }

    /**
     * Get the id corresponding to the cardIssuerName. If the name is not present it is inserted into db.
     * @param cardIssuerName
     * @return
     */
    public Integer getCardId(String cardIssuerName) {
        Map<String, Integer> cardIssuers = inCachedService.getAllCardIssuers();

        if (cardIssuers != null && cardIssuers.containsKey(cardIssuerName)) {
            return cardIssuers.get(cardIssuerName);
        } else {
            //Not synchronising this part as there is no chance of data corruption during this. The worse that can happen is multiple insert calls into
            //db for the same card issuer name and multiple cache sets. Duplicate inserts are taken care of in insertCardIssuer method and cache(redis for now)
            //set operations are atomic. Also, this code path will not be executed frequently as the no of unique issuing banks are limited in number. Also, it
            //is perfectly fine if this method is executed concurrently from multiple boxes, chances of data corruption or getting stale values are zilch.
            insertCardIssuer(cardIssuerName);
            final Map<String, Integer> cardIssuerMap = getCardIssuerMap();
            inCachedService.setAllCardIssuers(cardIssuerMap);
            return cardIssuerMap.get(cardIssuerName);
        }
    }

    public Map<String, Integer> getCardIssuerMap() {
        String sql = "SELECT id AS id, card_issuer_name AS cardIssuerName FROM card_issuer";
        List<Map<String, Object>> result = this.jdbcTemplate.queryForList(sql, ImmutableMap.<String, Object>of());

        ImmutableMap.Builder<String, Integer> builder = ImmutableMap.builder();

        for (Map<String, Object> map : result) {
            Integer id = ((Integer) map.get("id"));
            String issuerName = String.valueOf(map.get("cardIssuerName"));
            builder.put(issuerName, id);
        }

        return builder.build();
    }
    
    /**
     * Get the card_issuer_id corresponding to the cardIssuerName. If the name is not present it is inserted into db.
     * @param cardIssuerName
     * @return
     */
    public String getCardIssuerId(String cardIssuerName) {
        Map<String, String> cardIssuersIdMap = inCachedService.getAllCardIssuersIdMap();

        if (cardIssuersIdMap != null && cardIssuersIdMap.containsKey(cardIssuerName)) {
            return cardIssuersIdMap.get(cardIssuerName);
        } else {
            //Not synchronising this part as there is no chance of data corruption during this. The worse that can happen is multiple insert calls into
            //db for the same card issuer name and multiple cache sets. Duplicate inserts are taken care of in insertCardIssuer method and cache(redis for now)
            //set operations are atomic. Also, this code path will not be executed frequently as the no of unique issuing banks are limited in number. Also, it
            //is perfectly fine if this method is executed concurrently from multiple boxes, chances of data corruption or getting stale values are zilch.
            insertCardIssuer(cardIssuerName);
            final Map<String, String> cardIssuerMap = getCardIssuerIdMap();
            inCachedService.setAllCardIssuersIdMap(cardIssuerMap);
            return cardIssuerMap.get(cardIssuerName);
        }
    }

    public Map<String, String> getCardIssuerIdMap() {
        String sql = "SELECT card_issuer_id AS id, card_issuer_name AS cardIssuerName FROM card_issuer";
        List<Map<String, Object>> result = this.jdbcTemplate.queryForList(sql, ImmutableMap.<String, Object>of());

        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();

        for (Map<String, Object> map : result) {
            String cardIssuerId = String.valueOf(map.get("id"));
            String issuerName = String.valueOf(map.get("cardIssuerName"));
            builder.put(issuerName, cardIssuerId);
        }

        return builder.build();
    }

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        final CardIssuerDao cardIssuerDao = context.getBean(CardIssuerDao.class);

        cardIssuerDao.insertCardIssuer("foo ");
        cardIssuerDao.insertCardIssuer("foo");
        cardIssuerDao.insertCardIssuer("foo   ");


        ((ConfigurableApplicationContext) context).close();
    }
}
