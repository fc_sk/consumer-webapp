package com.freecharge.payment.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.mappers.PgMisDisplayRecordRowMapper;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.InService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.Wallet;
import com.freecharge.wallet.service.Wallet.TransactionType;
import com.freecharge.wallet.service.WalletTransaction;

public class PgMisAutenticationFetchImplDAO implements PgMisAutenticationFetchDAO {

	private static Logger logger = LoggingFactory.getLogger(PgMisAutenticationFetchImplDAO.class);
	
	@Autowired
	CartService cartService;
	
	@Autowired
	InService inService;
	
	@Autowired
	OrderIdWriteDAO orderIdDAO;
	
	@Autowired
	WalletService walletService;

	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public List<PgMisDisplayRecord> getMisData(PgMISRequestDataDAO misRequestData, Integer pageCount) {
		List<PgMisDisplayRecord> displayRecords = getMisRecords(misRequestData, pageCount);
		Collections.sort(displayRecords);
		return displayRecords;
	}

	public String prepareMisQuery(PgMISRequestDataDAO misRequestData, Integer pageCount) {
		boolean firstClause = true;
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer queryString = new StringBuffer("select inr.payment_rq_id, inr.order_id, inr.amount, inr.payment_mode, inr.payment_gateway, inr.payment_type, it.pg_transaction_id, " 
		+ " it.payment_option, it.pg_message,  it.sent_to_pg, it.recieved_from_pg, inr.created_on, it.is_successful, it.mtxn_id from payment_rq inr, " 
		+ " payment_txn it where inr.payment_rq_id = it.fk_payment_rq_id");

		if (misRequestData.getMisStartDate() != null) {
			queryString.append(firstClause ? " and " : " and ");

			Calendar startCalendar = Calendar.getInstance();
			startCalendar.setTime(misRequestData.getMisStartDate());
			String date=null;
			if (misRequestData.getStartTime()!=null) {
			     date = "" + startCalendar.get(Calendar.YEAR) + "-" + (startCalendar.get(Calendar.MONTH) + 1) + "-" + startCalendar.get(Calendar.DAY_OF_MONTH) + " " + misRequestData.getStartTime() + ":" + startCalendar.get(Calendar.MINUTE) + ":" + startCalendar.get(Calendar.SECOND);
			    queryString.append("inr.created_on >= '" + date + "'");    
			} else {
			     date = "" + startCalendar.get(Calendar.YEAR) + "-" + (startCalendar.get(Calendar.MONTH) + 1) + "-" + startCalendar.get(Calendar.DAY_OF_MONTH) + " " + startCalendar.get(Calendar.HOUR_OF_DAY) + ":" + startCalendar.get(Calendar.MINUTE) + ":" + startCalendar.get(Calendar.SECOND);
	            // date = "2012-01-16 0:0:0";
	            queryString.append("inr.created_on >= '" + date + "'");
			}
			parameters.put("startDate", date);
			firstClause = false;
		}
		if (misRequestData.getMisEndDate() != null) {
			queryString.append(" and ");
			Calendar startCalendar = Calendar.getInstance();
			startCalendar.setTime(misRequestData.getMisEndDate());
			String date =null;
			
			if (misRequestData.getEndTime()!=null) {
                date = "" + startCalendar.get(Calendar.YEAR) + "-" + (startCalendar.get(Calendar.MONTH) + 1) + "-" + startCalendar.get(Calendar.DAY_OF_MONTH) + " " + misRequestData.getEndTime() + ":" + startCalendar.get(Calendar.MINUTE) + ":" + startCalendar.get(Calendar.SECOND);
                queryString.append("inr.created_on <= '" + date + "'");    
            } else {
                date = "" + startCalendar.get(Calendar.YEAR) + "-" + (startCalendar.get(Calendar.MONTH) + 1) + "-" + startCalendar.get(Calendar.DAY_OF_MONTH) + " " + startCalendar.get(Calendar.HOUR_OF_DAY) + ":" + startCalendar.get(Calendar.MINUTE) + ":" + startCalendar.get(Calendar.SECOND);
                queryString.append("inr.created_on <= '" + date + "'");
            }
			
			parameters.put("endDate", date);
		}
		/*
		 * if (misRequestData.getMobileNo() != null && !misRequestData.getMobileNo().isEmpty() ) { queryString.append(" and "); queryString.append("inr.subscriberNumber = " + misRequestData.getMobileNo()); parameters.put("MobileNo", misRequestData.getMobileNo()); }
		 */
		if (misRequestData.getPaymentType() != null && !misRequestData.getPaymentType().isEmpty()) {
			queryString.append(" and ");
			queryString.append("inr.payment_type = '" + misRequestData.getPaymentType() + "'");
			parameters.put("operator", misRequestData.getPaymentType());
		}
		if (misRequestData.getTxnID() != null) {
			queryString.append(" and ");
			queryString.append("inr.payment_rq_id = " + misRequestData.getTxnID());
			parameters.put("txnID", misRequestData.getTxnID());
		}
		if (misRequestData.getPaymentOption() != null && !misRequestData.getPaymentOption().isEmpty()) {
			queryString.append(" and ");
			queryString.append("it.payment_option = '" + misRequestData.getPaymentOption() + "'");
			parameters.put("circle", misRequestData.getPaymentOption());
		}
		/*
		 * if (misRequestData.getGateway() != null && !misRequestData.getGateway().isEmpty()) { queryString.append(" and "); queryString.append("it.aggrName = '" + misRequestData.getGateway()+"'"); parameters.put("gateway", misRequestData.getGateway()); }
		 */
		if (misRequestData.getAppTxnID() != null && !misRequestData.getAppTxnID().isEmpty()) {
			queryString.append(" and ");
			queryString.append("inr.order_id = '" + misRequestData.getAppTxnID() + "'");
			parameters.put("appTxnID", misRequestData.getAppTxnID());
		}
		if (misRequestData.getStatus() != null && !misRequestData.getStatus().isEmpty()) {
			queryString.append(" and ");
			if (misRequestData.getStatus().equalsIgnoreCase("success")) {
				misRequestData.setStatus("1");
			} else {
				misRequestData.setStatus("0");
			}
			queryString.append("it.is_successful = '" + misRequestData.getStatus() + "'");
			parameters.put("status", misRequestData.getStatus());
		}
		if (misRequestData.getPageSize() != null) {
			queryString.append(" ORDER BY inr.payment_rq_id DESC");
		}
		if (pageCount != 500) {
			queryString.append(" limit " + (pageCount - 1) * 80 + ", 80");
		}
		parameters.put("query", queryString.toString());
		return queryString.toString();
	}

	public List<PgMisDisplayRecord> getMisRecords(final PgMISRequestDataDAO misRequestData, Integer pageCount) {
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(misRequestData.getMisStartDate());
		int year = startCalendar.get(Calendar.YEAR);
		int month = startCalendar.get(Calendar.MONTH);
		int date = startCalendar.get(Calendar.DAY_OF_MONTH);
		startCalendar.set(year, month, date, 0, 0, 0);
		misRequestData.setMisStartDate(startCalendar.getTime());

		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(misRequestData.getMisEndDate());
		year = endCalendar.get(Calendar.YEAR);
		month = endCalendar.get(Calendar.MONTH);
		date = endCalendar.get(Calendar.DAY_OF_MONTH);
		endCalendar.set(year, month, date, 23, 59, 59);
		misRequestData.setMisEndDate(endCalendar.getTime());

		String queryString = this.prepareMisQuery(misRequestData, pageCount);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<PgMisDisplayRecord> adminUserList = this.jdbcTemplate.query(queryString, paramMap, new PgMisDisplayRecordRowMapper());
		if (adminUserList != null && adminUserList.size() > 0) {
			for (PgMisDisplayRecord record : adminUserList) {

				String order_id = record.getAppTxnId();
				StringBuffer freeFundQuery = new StringBuffer("select 'freefund',fci.freefund_code,cit.price from order_id oid, cart crt, cart_items cit,freefund_coupon " + 
								"fci where oid.lookup_id = crt.lookup_id  and crt.cart_id = cit.fk_cart_id and cit.fl_product_master_id = 8 and cit.fk_item_id = fci.freefund_coupon_id");
				if (order_id != null) {
					freeFundQuery.append(" and ");
					freeFundQuery.append("oid.order_id = '" + order_id + "'");
					List<Map<String, Object>> listOfMap = this.jdbcTemplate.queryForList(freeFundQuery.toString(), paramMap);
					if (listOfMap != null && listOfMap.size() > 0) {
						Map<String, Object> map = listOfMap.get(0);
						record.setFreeFundName((String) map.get("freefund"));
						record.setFreeFundCode((String) map.get("freefund_code"));
						record.setFreeFundPrice(Double.toString((Double) (map.get("price"))));
					}
				}
				
				if (order_id != null) {
//					String crossSellQuery = "select oid.order_id as orderId, cs.campaign_name as name, ci.price as amount, ci.fk_item_id as couponStoreId, ci.quantity as quantity " +
//							"from order_id oid, cart cart, cart_items ci " +
//							"where cart.lookup_id = oid.lookup_id and cart.cart_id = ci.fk_cart_id " +
//							"and oid.order_id = '" + order_id + "' and ci.entity_id = '"+FCConstants.ENTITY_NAME_FOR_CROSSSELL+"' and ci.is_deleted is false";
//					
//					List<Map<String, Object>> crossSellList = this.jdbcTemplate.queryForList(crossSellQuery, new HashMap<String, Object>());
//					if(crossSellList != null && crossSellList.size() > 0) {
//						List<PgMisCrossSellDisplayRecord> crossSellDisplayRecords = new ArrayList<PgMisCrossSellDisplayRecord>();
//						for(Map<String, Object> map: crossSellList) {
//							PgMisCrossSellDisplayRecord crossSellDisplayRecord = new PgMisCrossSellDisplayRecord();
//							crossSellDisplayRecord.setCrossSellName((String)map.get("name"));
//							crossSellDisplayRecord.setAmount(((Double)map.get("amount")).floatValue());
//							
//							crossSellDisplayRecords.add(crossSellDisplayRecord);
//						}
//						record.setCrossSells(crossSellDisplayRecords);
//						logger.debug("cross sells in record = "+record.getCrossSells());
//					}
					
                    CartBusinessDo cartToInspect = cartService.getCartByOrderId(order_id);
                    
                    String userIdString = cartToInspect.getUserId().toString();
                    
                    Wallet userWallet = walletService.findUserWallet(cartToInspect.getUserId().longValue());
                    
                    String walletId = "";
                    
                    if (userWallet != null) {
                        walletId = Integer.toString(userWallet.getWalletStatusId());
                    }
                    
                    String mtxnId = record.getMtxnId();
                    
                    String fundSource = "";
                    
                    List<WalletTransaction> walletTransactionList = walletService.findTransaction(mtxnId, TransactionType.DEPOSIT);
                    
                    if (walletTransactionList != null && !walletTransactionList.isEmpty()) {
                        fundSource = walletTransactionList.get(0).getFundSource();
                    }
                    
                    InTransactionData inTransactionData = inService.getInTransactionData(order_id);
                    
                    String aggregatorName = "";
                    String aggregatorReference = "";
                    String operator = "";
                    String circle = "";
                    String aggregatorAmount = "";
                    String channelId = "";
                    
                    List<OrderId> orderIdList = orderIdDAO.getByOrderId(order_id);
                    
                    if (orderIdList != null && !orderIdList.isEmpty()) {
                        OrderId uniqueOrder = orderIdList.get(0);
                        
                        if (uniqueOrder.getChannelId() != null) {
                            channelId = uniqueOrder.getChannelId().toString();
                        }
                    }
                    
                    if (inTransactionData != null) {
                        InResponse inResponse = inTransactionData.getInResponse();
                        
                        if (inResponse != null) {
                            aggregatorName = inResponse.getAggrName();
                            aggregatorReference = inResponse.getAggrTransId();
                        }
                        
                        InRequest inRequest = inTransactionData.getInRequest();
                        
                        if (inRequest != null) {
                            operator = inRequest.getOperator();
                            aggregatorAmount = inRequest.getAmount().toString();
                            circle = inRequest.getCircle();
                        }
                    }
                    
                    record.setWalletId(walletId);
                    record.setUserId(userIdString);
                    record.setFundSource(fundSource);
                    record.setAggregatorName(aggregatorName);
                    record.setAggregatorReference(aggregatorReference);
                    record.setOperator(operator);
                    record.setCircle(circle);
                    record.setAgAmount(aggregatorAmount);
                    record.setChannelId(channelId);
				}
			}
		}

		return adminUserList;

	}

	public Integer getMisDataCount(PgMISRequestDataDAO misRequestData) {
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(misRequestData.getMisStartDate());
		int year = startCalendar.get(Calendar.YEAR);
		int month = startCalendar.get(Calendar.MONTH);
		int date = startCalendar.get(Calendar.DAY_OF_MONTH);
		startCalendar.set(year, month, date, 0, 0, 0);
		misRequestData.setMisStartDate(startCalendar.getTime());

		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(misRequestData.getMisEndDate());
		year = endCalendar.get(Calendar.YEAR);
		month = endCalendar.get(Calendar.MONTH);
		date = endCalendar.get(Calendar.DAY_OF_MONTH);
		endCalendar.set(year, month, date, 23, 0, 0);
		misRequestData.setMisEndDate(endCalendar.getTime());

		String queryString = this.prepareMisQueryForCount(misRequestData);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		return this.jdbcTemplate.queryForInt(queryString, paramMap);

	}

	public String prepareMisQueryForCount(PgMISRequestDataDAO misRequestData) {
		boolean firstClause = true;
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer queryString = new StringBuffer("select count(*) from payment_rq inr, payment_txn it where inr.payment_rq_id = it.fk_payment_rq_id");

		if (misRequestData.getMisStartDate() != null) {
			queryString.append(firstClause ? " and " : " and ");

			Calendar startCalendar = Calendar.getInstance();
			startCalendar.setTime(misRequestData.getMisStartDate());
			String date = "" + startCalendar.get(Calendar.YEAR) + "-" + (startCalendar.get(Calendar.MONTH) + 1) + "-" + startCalendar.get(Calendar.DAY_OF_MONTH) + " " + startCalendar.get(Calendar.HOUR_OF_DAY) + ":" + startCalendar.get(Calendar.MINUTE) + ":" + startCalendar.get(Calendar.SECOND);
			date = "2012-01-16 0:0:0";
			queryString.append("inr.created_on >= '" + date + "'");
			parameters.put("startDate", date);
			firstClause = false;
		}
		if (misRequestData.getMisEndDate() != null) {
			queryString.append(" and ");
			Calendar startCalendar = Calendar.getInstance();
			startCalendar.setTime(misRequestData.getMisEndDate());
			String date = "" + startCalendar.get(Calendar.YEAR) + "-" + (startCalendar.get(Calendar.MONTH) + 1) + "-" + startCalendar.get(Calendar.DAY_OF_MONTH) + " " + startCalendar.get(Calendar.HOUR_OF_DAY) + ":" + startCalendar.get(Calendar.MINUTE) + ":" + startCalendar.get(Calendar.SECOND);
			queryString.append("inr.created_on <= '" + date + "'");
			parameters.put("endDate", date);
		}
		/*
		 * if (misRequestData.getMobileNo() != null && !misRequestData.getMobileNo().isEmpty() ) { queryString.append(" and "); queryString.append("inr.subscriberNumber = " + misRequestData.getMobileNo()); parameters.put("MobileNo", misRequestData.getMobileNo()); }
		 */
		if (misRequestData.getPaymentType() != null && !misRequestData.getPaymentType().isEmpty()) {
			queryString.append(" and ");
			queryString.append("inr.payment_type = '" + misRequestData.getPaymentType() + "'");
			parameters.put("operator", misRequestData.getPaymentType());
		}
		if (misRequestData.getTxnID() != null) {
			queryString.append(" and ");
			queryString.append("inr.payment_rq_id = " + misRequestData.getTxnID());
			parameters.put("txnID", misRequestData.getTxnID());
		}
		if (misRequestData.getPaymentOption() != null && !misRequestData.getPaymentOption().isEmpty()) {
			queryString.append(" and ");
			queryString.append("it.payment_option = '" + misRequestData.getPaymentOption() + "'");
			parameters.put("circle", misRequestData.getPaymentOption());
		}
		/*
		 * if (misRequestData.getGateway() != null && !misRequestData.getGateway().isEmpty()) { queryString.append(" and "); queryString.append("it.aggrName = '" + misRequestData.getGateway()+"'"); parameters.put("gateway", misRequestData.getGateway()); }
		 */
		if (misRequestData.getAppTxnID() != null && !misRequestData.getAppTxnID().isEmpty()) {
			queryString.append(" and ");
			queryString.append("inr.order_id = '" + misRequestData.getAppTxnID() + "'");
			parameters.put("appTxnID", misRequestData.getAppTxnID());
		}
		if (misRequestData.getStatus() != null && !misRequestData.getStatus().isEmpty()) {
			queryString.append(" and ");
			queryString.append("it.transactionStatus = '" + misRequestData.getStatus() + "'");
			parameters.put("status", misRequestData.getStatus());
		}
		if (misRequestData.getPageSize() != null) {
			queryString.append(" ORDER BY inr.payment_rq_id DESC");
		}

		parameters.put("query", queryString.toString());
		return queryString.toString();
	}

}
