package com.freecharge.payment.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughputExceededException;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.webdo.FeeCharge;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@Component
public class ConvinenceFeeDetailsDao {

	private static Logger logger = LoggingFactory.getLogger(ConvinenceFeeDetailsDao.class);

	private final static String LOOKUP_ID = "lookupId";
	private final static String FEE_DETAILS = "feeDetails";
	private final static String TABLE_NAME = "convenience_fee_details";

	@Autowired
	AmazonDynamoDBAsync dynamoClientMum;
	ObjectMapper objectMapper = new ObjectMapper();

	public void insert(FeeCharge feeCharge, String lookupId) {

		
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
		try {
			item.put(LOOKUP_ID, new AttributeValue(lookupId));
			item.put(FEE_DETAILS, new AttributeValue(objectMapper.writeValueAsString(feeCharge)));
		} catch (Exception e) {
			logger.error("Exception caught while pushing data to DDB for lookupId: " + lookupId, e);
		}

		try {
			logger.info("Attempting to insert in dynamo for lookupId: "+lookupId);
			PutItemResult result = dynamoClientMum.putItem(new PutItemRequest()
					.withConditionExpression("attribute_not_exists(lookupId)")
					.withTableName(TABLE_NAME)
					.withItem(item));
			logger.info("Successfully added item in dynamo for lookupId: "+lookupId);
		}catch(ProvisionedThroughputExceededException ex) {
			logger.error("ProvisionedThroughputExceededException exception caught for lookupId: "+lookupId, ex);
			throw ex;
		}catch(ConditionalCheckFailedException ex) {
			logger.error("ConditionalCheckFailedException exception caught for lookupId: "+lookupId, ex);
		}
		catch (RuntimeException e) {
			logger.error("Exception caught for lookupId: "+lookupId, e);
			throw e;
		}
	}

	public FeeCharge getFeeDetails(String lookupId) {
		
		List<String> attrList = new LinkedList<String>();
		attrList.add(LOOKUP_ID);
		attrList.add(FEE_DETAILS);

		Map<String, AttributeValue> requestKey = new HashMap<String, AttributeValue>();
		requestKey.put(LOOKUP_ID, new AttributeValue(lookupId));
		GetItemRequest getItemRequest = new GetItemRequest().withTableName(TABLE_NAME)
				.withAttributesToGet(attrList).withKey(requestKey);
		getItemRequest.setAttributesToGet(attrList);
		getItemRequest.setConsistentRead(true);
		GetItemResult result = null;
		try {
			result = dynamoClientMum.getItem(getItemRequest);
		} catch (ProvisionedThroughputExceededException ex) {
			logger.error("ProvisionedThroughputExceededException exception caught for lookupId: " + lookupId, ex);
			throw ex;
		} catch (RuntimeException e) {
			logger.error("Exception caught for lookupId: " + lookupId, e);
			throw e;
		}
		FeeCharge feeCharge =null;
		if(result != null && result.getItem() != null) {
			try {
				Map<String, AttributeValue> mappingItem = result.getItem();
				String feeDetails =  mappingItem.get(FEE_DETAILS).getS();
				if(feeDetails != null) {
					feeCharge = objectMapper.readValue(feeDetails.getBytes(), FeeCharge.class);
				}
				return feeCharge;
			}catch(Exception e) {
				logger.error("Exception caught while converting data from dynamo for lookupId: " + lookupId);
			}
		}
		return feeCharge;
	}
}
