package com.freecharge.payment.dao;

import org.apache.log4j.Logger;

import com.freecharge.app.domain.dao.CommonDao;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.dos.entity.PaymentResponse;

public class PaymentResponseDAO extends CommonDao<PaymentResponse> implements IPaymentResponseDAO  {

    private final Logger logger = LoggingFactory.getLogger(getClass());


    protected Class<PaymentResponse> getDomainClass() {
        return PaymentResponse.class;
    }

}
