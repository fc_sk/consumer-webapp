package com.freecharge.payment.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PgMISRequestDataDAO {

	
	private String startDate;
	private Integer startTime;
    private String endDate;
	private String mobileNo;
	private Integer txnID;
	private String paymentType;
	private String paymentOption;
	/*private Integer circle;*/
	private String gateway;
	private Integer appName;
	private String appTxnID;
	private String status;
	private Date misStartDate;
	private Date misEndDate;
	private String endTime;
	
	
	public String getEndTime() {
        return endTime;
    }
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
    private Integer pageSize = 80;
	private int pageNo;
	
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
	
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	
	public Date getMisStartDate() {
		return misStartDate;
	}
	public void setMisStartDate(Date misStartDate) {
		this.misStartDate = misStartDate;
	}
	
	public Date getMisEndDate() {
		return misEndDate;
	}
	public void setMisEndDate(Date misEndDate) {
		this.misEndDate = misEndDate;
	}
	
	public void setStartDate(String startDate) {
		//2011/09/17
		if(startDate != null && !startDate.equals("")){
			try{
			SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		    this.misStartDate = df.parse(startDate);
		    this.startDate = startDate;
			}
			catch(ParseException pe){
				pe.printStackTrace();
			}
		}
	}
	

	public void setEndDate(String endDate) {
		if(endDate != null && !endDate.equals("")){
			try{
				SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		    this.misEndDate = df.parse(endDate);
		    this.endDate=endDate;
			}
			catch(ParseException pe){
				pe.printStackTrace();
			}
		}
	}
	
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	public Integer getTxnID() {
		return txnID;
	}
	public void setTxnID(Integer txnID) {
		this.txnID = txnID;
	}
	
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	/*public Integer getCircle() {
		return circle;
	}
	public void setCircle(Integer circle) {
		this.circle = circle;
	}*/
	
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	
	public Integer getAppName() {
		return appName;
	}
	public void setAppName(Integer appName) {
		this.appName = appName;
	}
	
	public String getAppTxnID() {
		return appTxnID;
	}
	public void setAppTxnID(String appTxnID) {
		this.appTxnID = appTxnID;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStartDate() {
		return startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	
	
	public String getPaymentOption() {
		return paymentOption;
	}
	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}
	
	public Integer getStartTime() {
        return startTime;
    }
    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

}
