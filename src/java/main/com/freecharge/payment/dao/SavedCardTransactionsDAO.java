package com.freecharge.payment.dao;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.payment.services.PaymentGatewayHandler;

@Component
public class SavedCardTransactionsDAO {
    private NamedParameterJdbcTemplate jt;

    Logger                             logger = Logger.getLogger(SavedCardTransactionsDAO.class);

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final String INSERT_TXN_SQL = "INSERT INTO"
            + " saved_card_transactions(order_id, merchant_transaction_id, ts)"
            + " VALUES (:order_id, :merchant_transaction_id, :ts)";

    public void logTransactionWithSavedCard(String orderID, String mtxnID) {
        Map<String, Object> insertParams = new HashMap<String, Object>();        
        insertParams.put("order_id", orderID);
        insertParams.put("merchant_transaction_id", mtxnID);
        insertParams.put("ts", new Timestamp(System.currentTimeMillis()));
        jt.update(INSERT_TXN_SQL, insertParams);
    }

}
