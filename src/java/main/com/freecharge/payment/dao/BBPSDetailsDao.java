package com.freecharge.payment.dao;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughputExceededException;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import com.freecharge.common.util.FCUtil;
import com.freecharge.web.webdo.BBPSTransactionDetails;
import com.freecharge.web.webdo.FeeCharge;

@Component
public class BBPSDetailsDao {

	private static Logger logger = LoggingFactory.getLogger(BBPSDetailsDao.class);

	private final static String LOOKUP_ID = "lookupId";
	private final static String BBPS_DETAILS = "bbpsDetails";
	private final static String TABLE_NAME = "bbps_details";
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

	@Autowired
	AmazonDynamoDBAsync dynamoClient;
	ObjectMapper objectMapper = new ObjectMapper();

public void insertBBPSDetails(BBPSTransactionDetails bbpsDetails) {
		final String queryString = "insert into bbps_details(customer_name,customer_account_number,lookup_id,bill_due_date,bbps_reference_number,bbps_metadata,ag_txn_id,is_bbps_active,message_details,bill_period) "
				+ "values (:customerName, :customerAccountNumber, :lookupId, :billDueDate, :bbpsReferenceNumber, :bbpsMetaData, :aggregatorTxnId, :isBbpsActive, :messageDetails , :billPeriod)";
		MapSqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("customerName", bbpsDetails.getCustomerName())
				.addValue("customerAccountNumber", bbpsDetails.getCustomerAccountNumber())
				.addValue("lookupId", bbpsDetails.getLookupId())
				.addValue("billDueDate", bbpsDetails.getBillDueDate())
				.addValue("bbpsReferenceNumber", bbpsDetails.getBbpsReferenceNumber())
				.addValue("bbpsMetaData", bbpsDetails.getBbpsMetaData())
				.addValue("aggregatorTxnId", bbpsDetails.getAggregatorTxnId())
				.addValue("isBbpsActive", bbpsDetails.getIsBbpsActive())
				.addValue("messageDetails", bbpsDetails.getMessageDetails())
				.addValue("billPeriod", bbpsDetails.getBillPeriod());
	
		int updatedRows = -1;
		try {
		updatedRows = this.jdbcTemplate.update(queryString, parameters);
		} catch(Exception e) {
			logger.error("Exception caught while inserting bbps details for lookupId: " + bbpsDetails.getLookupId(), e);
		}
		if (updatedRows == 1) {
            logger.info("bbps bill details added"+ bbpsDetails.toString());
        }
		
	}

public void updateBBPSDetails(BBPSTransactionDetails bbpsDetails) {
	
	final String queryString = "update bbps_details set bbps_reference_number = :bbpsReferenceNumber ,ag_txn_id = :aggregatorTxnId ,is_bbps_active = :isBbpsActive where lookup_id = :lookupId";
	MapSqlParameterSource parameters = new MapSqlParameterSource()
			.addValue("lookupId", bbpsDetails.getLookupId())
			.addValue("bbpsReferenceNumber", bbpsDetails.getBbpsReferenceNumber())
			.addValue("aggregatorTxnId", bbpsDetails.getAggregatorTxnId())
			.addValue("isBbpsActive", bbpsDetails.getIsBbpsActive());

	int updatedRows = -1;
	try {
	updatedRows = this.jdbcTemplate.update(queryString, parameters);
	} catch(Exception e) {
		logger.error("Exception caught while updating bbps details for lookupId: " + bbpsDetails.getLookupId());
	}
	if (updatedRows == 1) {
        logger.info("bbps bill details updated"+ bbpsDetails.toString());
    }
	
}

	public void updateBBPSReferenceNo(BBPSTransactionDetails bbpsDetails) {

		final String queryString = "update bbps_details set bbps_reference_number = :bbpsReferenceNumber where ag_txn_id = :aggregatorTxnId and is_bbps_active = :isBbpsActive";
		MapSqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("bbpsReferenceNumber", bbpsDetails.getBbpsReferenceNumber())
				.addValue("aggregatorTxnId", bbpsDetails.getAggregatorTxnId())
				.addValue("isBbpsActive", bbpsDetails.getIsBbpsActive());

		int updatedRows = -1;
		try {
			updatedRows = this.jdbcTemplate.update(queryString, parameters);
		} catch(Exception e) {
			logger.error("Exception caught while updating bbps details for Aggregator Txn Id: " + bbpsDetails.getAggregatorTxnId());
		}
		if (updatedRows == 1) {
			logger.info("bbps bill details updated"+ bbpsDetails.toString());
		}

	}
   // @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public void updateBBPSReferenceNoByLookupId(String lookupId,String bbpsRefNumber) {

		final String queryString = "update bbps_details set bbps_reference_number = :bbpsReferenceNumber where lookup_id = :lookupId";
		MapSqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("bbpsReferenceNumber", bbpsRefNumber)
				.addValue("lookupId", lookupId);

		int updatedRows = -1;
		try {
			updatedRows = this.jdbcTemplate.update(queryString, parameters);
		} catch(Exception e) {
			logger.error("Exception caught while updating bbps details for lookup Id: " + lookupId);
		}
		if (updatedRows == 1) {
			logger.info("bbps bill details updated "+ lookupId);
		}

	}

public BBPSTransactionDetails getBBPSDetailsForLookupId(String lookupId) {
	BBPSTransactionDetails result = null;
	try {
		final String queryString = "select * from bbps_details where lookup_id = :lookupId";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("lookupId", lookupId);

		List<BBPSTransactionDetails> bbpsDetailsList = this.jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(BBPSTransactionDetails.class));
		if(bbpsDetailsList == null || bbpsDetailsList.size() == 0){
			logger.error("no bbps details exist for lookupId: " + lookupId);
			return null;
		}
		if(bbpsDetailsList.size() > 1){
			logger.error("more than one bbps details object exist for lookupId: " + lookupId);
		}
		
		return bbpsDetailsList.get(0);
	} catch (RuntimeException re) {
		logger.error("Exception in get getBBPSDetails by lookupid : " + lookupId, re);
//		throw re;
	}
	return result;
}

public BBPSTransactionDetails getBBPSDetailsForLookupIdFailFast(String lookupId) {
	BBPSTransactionDetails result = null;
	try {
		final String queryString = "select * from bbps_details where lookup_id = :lookupId";
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("lookupId", lookupId);

		List<BBPSTransactionDetails> bbpsDetailsList = this.jdbcTemplate.query(queryString, paramMap, new BeanPropertyRowMapper(BBPSTransactionDetails.class));
		if(bbpsDetailsList == null || bbpsDetailsList.size() == 0){
			logger.error("no bbps details exist for lookupId: " + lookupId);
			return null;
		}
		if(bbpsDetailsList.size() > 1){
			throw new RuntimeException("Invalid BBPS Details " + lookupId);
		}
		
		return bbpsDetailsList.get(0);
	} catch (RuntimeException re) {
		logger.error("Exception in get getBBPSDetails by lookupid : " + lookupId, re);
//		throw re;
	}
	return result;
}
}
