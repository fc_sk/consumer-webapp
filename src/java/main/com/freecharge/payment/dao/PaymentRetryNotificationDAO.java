package com.freecharge.payment.dao;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class PaymentRetryNotificationDAO {

    private NamedParameterJdbcTemplate jt;

    Logger                             logger = Logger.getLogger(PaymentRetryNotificationDAO.class);

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final String INSERT_TXN_SQL = "INSERT INTO"
            + " payment_retry_notification(order_id, email_id, fire_time)"
            + " VALUES (:order_id, :email_id, :fire_time)";

    public void recordPaymentRetryNotification(String orderId, String emailId) {
//        Map<String, Object> insertParams = new HashMap<String, Object>();
//        insertParams.put("order_id", orderId);
//        insertParams.put("email_id", emailId);
//        insertParams.put("fire_time", new Timestamp(System.currentTimeMillis()));
//        jt.update(INSERT_TXN_SQL, insertParams);
    }

    public void updateNewOrderIdOnNotificationRetry(String oldOrderId, String newOrderId) {
//        String updateQuery = "UPDATE payment_retry_notification SET new_order=:newOrderId WHERE order_id=:oldOrderId";
//        MapSqlParameterSource updateParams = new MapSqlParameterSource();
//        updateParams.addValue("newOrderId", newOrderId);
//        updateParams.addValue("oldOrderId", oldOrderId);
//        this.jt.update(updateQuery, updateParams);
    }
}
