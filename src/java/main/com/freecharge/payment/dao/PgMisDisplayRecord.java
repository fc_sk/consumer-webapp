package com.freecharge.payment.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PgMisDisplayRecord implements Serializable, Comparable<PgMisDisplayRecord> {

	private static final long serialVersionUID = 1L;
	private Integer txnId;
	private String appName;
	private String mobileNo;
	private String paymentMode;
	private String amount;
	private String appTxnId;
	private String paymentGateway;
	private String paymentType;
	private String paymentOption;
	private String paymentMessage;
	private String sendToPgDate;
	private Date receivedFromPgdate;
	private Date requestCreatedOn;
	private String gatewayTransactionID;
	private String options;
	private String status;
	private String attemptOrder;
	private String freeFundName;
	private String freeFundCode;
	private String freeFundPrice;
	private String mtxnId;
	
	private String walletId;
	private String userId;
	private String fundSource;
	private String aggregatorName;
	private String aggregatorReference;
	private String circle;
	private String agAmount;
    private String operator;
    private String channelId;
    
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    
    public String getChannelId() {
        return channelId;
    }
    
    public void setAgAmount(String agAmount) {
        this.agAmount = agAmount;
    }
    
    public String getAgAmount() {
        return agAmount;
    }
    
    
    public void setCircle(String circle) {
        this.circle = circle;
    }
    
    public String getCircle() {
        return circle;
    }
    
    public void setAggregatorReference(String aggregatorReference) {
        this.aggregatorReference = aggregatorReference;
    }
    
    public String getAggregatorReference() {
        return aggregatorReference;
    }
    
    public void setAggregatorName(String aggregatorName) {
        this.aggregatorName = aggregatorName;
    }
    
    public String getAggregatorName() {
        return aggregatorName;
    }
    
    public String getFundSource() {
        return fundSource;
    }
    
    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getUserId() {
        return userId;
    }
    
    public String getWalletId() {
        return walletId;
    }
    
    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }
	
	
	private List<PgMisCrossSellDisplayRecord> crossSells;
	
	public List<PgMisCrossSellDisplayRecord> getCrossSells() {
		return crossSells;
	}

	public void setCrossSells(List<PgMisCrossSellDisplayRecord> crossSells) {
		this.crossSells = crossSells;
	}

	public String getFreeFundName() {
		return freeFundName;
	}

	public void setFreeFundName(String freeFundName) {
		this.freeFundName = freeFundName;
	}

	public String getFreeFundCode() {
		return freeFundCode;
	}

	public void setFreeFundCode(String freeFundCode) {
		this.freeFundCode = freeFundCode;
	}

	public String getFreeFundPrice() {
		return freeFundPrice;
	}

	public void setFreeFundPrice(String freeFundPrice) {
		this.freeFundPrice = freeFundPrice;
	}

	public String getGatewayTransactionID() {
		return gatewayTransactionID;
	}

	public void setGatewayTransactionID(String gatewayTransactionID) {
		this.gatewayTransactionID = gatewayTransactionID;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTxnId() {
		return txnId;
	}

	public void setTxnId(Integer txnId) {

		this.txnId = txnId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAppTxnId() {
		return appTxnId;
	}

	public void setAppTxnId(String appTxnId) {
		this.appTxnId = appTxnId;
	}

	public String getPaymentGateway() {
		return paymentGateway;
	}

	public void setPaymentGateway(String paymentGateway) {
		this.paymentGateway = paymentGateway;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public String getPaymentMessage() {
		return paymentMessage;
	}

	public void setPaymentMessage(String paymentMessage) {
		this.paymentMessage = paymentMessage;
	}

	public String getSendToPgDate() {
		return sendToPgDate;
	}

	public void setSendToPgDate(String sendToPgDate) {
		this.sendToPgDate = sendToPgDate;
	}

	public Date getReceivedFromPgdate() {
		return receivedFromPgdate;
	}

	public void setReceivedFromPgdate(Date receivedFromPgdate) {
		this.receivedFromPgdate = receivedFromPgdate;
	}

	public Date getRequestCreatedOn() {
		return requestCreatedOn;
	}

	public void setRequestCreatedOn(Date requestCreatedOn) {
		this.requestCreatedOn = requestCreatedOn;
	}

	public String getAttemptOrder() {
		return attemptOrder;
	}

	public void setAttemptOrder(String attemptOrder) {
		this.attemptOrder = attemptOrder;
	}
	
	public String getMtxnId() {
		return mtxnId;
	}

	public void setMtxnId(String mtxnId) {
		this.mtxnId = mtxnId;
	}

	public int compareTo(PgMisDisplayRecord o) {
		if (this.getRequestCreatedOn().getTime() > o.getRequestCreatedOn().getTime())
			return -1;
		else if (this.getRequestCreatedOn().getTime() < o.getRequestCreatedOn().getTime())
			return 1;
		else
			return 0;
	}

}
