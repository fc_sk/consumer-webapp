package com.freecharge.payment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.freecharge.payment.services.PaymentCard;
import com.google.common.collect.ImmutableMap;

@Component
public class CardBinDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(CardBinDao.class);
	
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private AmazonDynamoDBAsync dynamoClient;
    
    private static final String CARD_DATA_TABLE_NAME = "payment_card_data";
    
    private static final String MTXN_COLUMN_NAME = "mtxn";
    
    private static final String CARD_BIN_COLUMN_NAME = "card_bin";
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public boolean saveCardDataForPaymentTxn(String mTxnId, PaymentCard paymentCard) {
        String sql = "INSERT INTO txn_card_data(mtxnid, card_bin, card_hash) VALUES (:mtxnid, :card_bin, :cardHash)";

        Map<String, String> params = new HashMap<String, String>();
        params.put("mtxnid", mTxnId);
        params.put("card_bin", "");
        params.put("cardHash", paymentCard.getCardHash());

        int updatedRows = this.jdbcTemplate.update(sql, params);
        Map<String, AttributeValue> putItemData = new HashMap<String, AttributeValue>();
        putItemData.put(MTXN_COLUMN_NAME, new AttributeValue(mTxnId));
        putItemData.put(CARD_BIN_COLUMN_NAME, new AttributeValue(paymentCard.getCardBin()));
        dynamoClient.putItem(new PutItemRequest(CARD_DATA_TABLE_NAME, putItemData));
        if (updatedRows == 1) {
            return true;
        }
        return false;
    }

    public PaymentCard getCardRelatedDataForPaymentTxn(String mtxnId) {
        String sql = "SELECT card_hash from txn_card_data WHERE mtxnid=:mtxnId";
        Map<String, String> params = new HashMap<String, String>();
        params.put("mtxnId", mtxnId);
        List<PaymentCard> orderCardDetailsList = this.jdbcTemplate.query(sql, params, new RowMapper<PaymentCard>() {

            @Override
            public PaymentCard mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
                return new PaymentCard(resultSet.getString("card_hash"), null);
            }
        });

        if (orderCardDetailsList == null || orderCardDetailsList.isEmpty()) {
            return null;
        }
        LOGGER.info("Fetching card bin from DynamoDB for mtxnid : " + mtxnId);
        GetItemRequest getItemRequest = new GetItemRequest().withTableName(CARD_DATA_TABLE_NAME).withKey(ImmutableMap.of(MTXN_COLUMN_NAME, new AttributeValue().withS(mtxnId)));
        GetItemResult getItemResult = dynamoClient.getItem(getItemRequest);
        if(getItemResult != null && getItemResult.getItem() != null){
        	Map<String, AttributeValue> item = getItemResult.getItem();
        	String cardBin = String.valueOf(item.get(CARD_BIN_COLUMN_NAME).getS());
        	LOGGER.info("Card bin for mtxnid : " + mtxnId + " : " + cardBin);
        	orderCardDetailsList.get(0).setCardBin(cardBin);
        }
        return orderCardDetailsList.get(0);
    }
}
