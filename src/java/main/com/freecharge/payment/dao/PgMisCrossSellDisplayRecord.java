package com.freecharge.payment.dao;

import java.io.Serializable;

public class PgMisCrossSellDisplayRecord implements Serializable {

	private static final long serialVersionUID = 1L;
	private String crossSellName;
	private Float amount;
	
	public String getCrossSellName() {
		return crossSellName;
	}
	public void setCrossSellName(String crossSellName) {
		this.crossSellName = crossSellName;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return "Name = "+this.getCrossSellName() + " and amount = "+this.getAmount();
	}
	
}
