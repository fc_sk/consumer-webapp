package com.freecharge.payment.model;

public enum InstantCreditCardApiErrorCodes {

    SUCCESS("VC00","Success"),
    USER_ID_INVALID("VC01","User Id is Invalid"),
    APP_SERIAL_INVALID("VC02","Invalid Application Serial Number"),
    CARD_FETCH_ERROR("VC03","Error in feching card details form Axis"),
    HASH_VALIDATION_ERROR("VC04","Error in Hash Calculation"),
    INVALID_CARD_RESPONSE("VC05","Error in parsing the card detail response"),
    FORTNOX_CARD_SAVE_FAILED("VC06","Error in saving card to fortKnox"),
    INVALID_CHECKSUM("VC07","Request checksum is invalid");



    private String ErrorCode;
    private String ErrorMessage;

    private InstantCreditCardApiErrorCodes(String ErrorCode,
                                   String ErrorMessage) {
        this.ErrorCode = ErrorCode;
        this.ErrorMessage = ErrorMessage;
    }

    public String getErrorCode() {
        return ErrorCode;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }
}
