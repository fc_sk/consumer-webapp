package com.freecharge.payment.util;

import java.util.concurrent.TimeUnit;

import com.freecharge.common.util.FCUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.payment.services.PaymentTransactionService;

@Component
public class PaymentsCache extends RedisCacheManager {

    @Qualifier("paymentsRedisTemplate")
    @Autowired
    private RedisTemplate<String, Object> paymentsCacheTemplate;
    
    @Autowired
    protected PaymentTransactionService paymentTransactionService;

    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.paymentsCacheTemplate;
    }

    @Override
    public String get(String key) {
        return (String) super.get(key);
    }

    public void setOrderPaymentDataInCache(String key, String value, long cacheDurationInDays) {
        set(key, value, cacheDurationInDays, TimeUnit.DAYS);
    }

    public void setMerchantIdForKpayPayment(String mtxnId, String merchantId, long cacheDurationInDays) {
        set(mtxnId+"_MIdCache", merchantId, cacheDurationInDays, TimeUnit.DAYS);
    }

    public String getMerchantIdForMtxnId(String mtxnId) {
        String merchantId = get(mtxnId+"_MIdCache");
//        if(!FCUtil.isEmpty(merchantId)) {
//            delete(mtxnId);
//        }
        return merchantId;
    }

    public Long getRetryCount(String orderId) {
        String key = orderId + "_MerchantTxnId";
        Long id = this.increment(key);
        this.expireAt(key, new DateTime().plusHours(24).toDate());
        return id;
    }
    
    public void setDataInCacheMinutes(String key, String value, long cacheDurationInMinutes) {
    	set(key, value, cacheDurationInMinutes, TimeUnit.MINUTES);
    }
}
