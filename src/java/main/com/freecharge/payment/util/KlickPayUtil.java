package com.freecharge.payment.util;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.payment.services.PaymentOptionService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

@Service("klickPayUtil")
public class KlickPayUtil {
    private static Logger       logger                 = Logger.getLogger(KlickPayUtil.class);

    // headers
    private static final String HEADER_USER_AGENT   = "User-Agent";
    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final String HEADER_REQ_DATE     = "Request-Date";


    public static final int                      KLICKPAY_MERCHANT_ID      = 0;
    public static final int                      KLICKPAY_MERCHANT_KEY     = 1;
    @Autowired
    private PaymentOptionService                  paymentOptionService;
    @Autowired
    protected FCProperties fcProperties;
    @Autowired
    private AppConfigService appConfigService;
    private static HttpClient   httpClient          = null;

    public static HttpResponse getRequest(String restURL) throws ClientProtocolException, IOException {
        httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(restURL);
        HttpResponse httpResponse = httpClient.execute(request);
        return httpResponse;
    }

    public static Map<String, String> postPaymentRequest(String restURL, List<NameValuePair> params) throws Exception {
        httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(restURL);
        httpPost.setHeader(HEADER_USER_AGENT, "HTTP Client");
        httpPost.setHeader(HEADER_CONTENT_TYPE, "application/x-www-form-urlencoded");

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        httpPost.setHeader(HEADER_REQ_DATE, dateFormat.format(date));
        httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        HttpResponse httpResponse = httpClient.execute(httpPost);

        logger.info(httpResponse.getStatusLine());
        logger.info(httpResponse.getStatusLine().getStatusCode());
        
        if (httpResponse.getStatusLine().getStatusCode() != 200) {
            logger.error("Response Status code:" + httpResponse.getStatusLine().getStatusCode());
            throw new RuntimeException();
        }
        String htmlPgResponse = getHtmlResponseFromEntity(httpResponse);
        Map<String, String> klickPayResponse = getMapFromTheStringHTML(htmlPgResponse);
        return klickPayResponse;
    }
 
    public static String getHtmlResponseFromEntity(HttpResponse httpResponse) throws ClientProtocolException,
            IOException, SAXException, ParserConfigurationException, JSONException {
        HttpEntity respEntity = httpResponse.getEntity();
        String responseString = EntityUtils.toString(respEntity, "UTF-8");
        return responseString;

    }

    public static Map<String, String> getMapFromTheStringHTML(String StringEntityResp) {
        Map<String, String> map = new HashMap<String, String>();
        Scanner scanner = new Scanner(StringEntityResp);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String pattern = "(.*)(name=\")(.*)(\" value=\")(.*)(\")";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(line);
            if (m.find()) {
                map.put(m.group(3), m.group(5));
            }
        }
        scanner.close();
        return map;
    }


    public String[] getKlickPayCredentials(String productType) {
        logger.info("Fetching utility products from app config");
        final String delimiter = ",";
        String[] utilityProducts = appConfigService.getUtilityProductsConfig(fcProperties.getUtilityProductsListKey(), delimiter);
        String[] merchantCredentials = new String[2];
        if (utilityProducts != null && ArrayUtils.contains(utilityProducts, productType)) {
            merchantCredentials[KLICKPAY_MERCHANT_ID] = fcProperties
                    .getProperty(PaymentConstants.KLICKPAY_UTILITY_MERCHANT_ID);
            merchantCredentials[KLICKPAY_MERCHANT_KEY] = fcProperties
                    .getProperty(PaymentConstants.KLICKPAY_UTILITY_MERCHANT_KEY);
        } else {
            merchantCredentials[KLICKPAY_MERCHANT_ID] = fcProperties.getProperty(PaymentConstants.KLICKPAY_MERCHANT_ID);
            merchantCredentials[KLICKPAY_MERCHANT_KEY] = fcProperties
                    .getProperty(PaymentConstants.KLICKPAY_MERCHANT_KEY);
        }
        return merchantCredentials;
    }
}
