package com.freecharge.payment.util;

import com.freecharge.payment.exception.HashValidationException;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

public class EncryptionUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionUtil.class);

    public static String encrypt(String data, String keyAsHexString, String algorithm,
                                 String cipherPadding) throws HashValidationException {
        String result="";

        try {
            byte[] iv = getRandomIV();
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
            /** Generate a secret key from the hex string as key */
            SecretKeySpec skeySpec = getSecretKeySpecFromHexString(algorithm, keyAsHexString);
            /** Creating a cipher instance with the algorithm and padding */
            Cipher cipher = Cipher.getInstance(cipherPadding);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, paramSpec);
            /** generating the encrypted result */
            byte[] encrypted = cipher.doFinal(data.getBytes(StandardCharsets.UTF_8));
            // To add iv in encrypted string.
            byte[] encryptedWithIV = copyIVAndCipher(encrypted, iv);
            result = Base64.encodeBase64String(encryptedWithIV);
        }catch(Exception e){
            throw new HashValidationException(e);
        }
        return result;
    }

    public static String decrypt(String ciphertext, String keyAsHexString, String algorithm,
                                 String cipherPadding) throws HashValidationException {
        String decryptedResult="";
        try {
            SecretKeySpec skeySpec = getSecretKeySpecFromHexString(algorithm, keyAsHexString);
            byte[] encryptedIVandTextAsBytes = Base64.decodeBase64(ciphertext);
            /** First 16 bytes are always the IV */
            byte[] iv = Arrays.copyOf(encryptedIVandTextAsBytes, 16);

            byte[] ciphertextByte = Arrays.copyOfRange(encryptedIVandTextAsBytes, 16,
                    encryptedIVandTextAsBytes.length);
            // Decrypt the message
            Cipher cipher = Cipher.getInstance(cipherPadding);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(iv));
            byte[] decryptedTextBytes = cipher.doFinal(ciphertextByte);
            decryptedResult = getByteToString(decryptedTextBytes);
        }catch(Exception e){
            throw new HashValidationException(e);
        }
        return decryptedResult;
    }

    private static byte[] getRandomIV(){

        /** Generating a random IV param spec for encrypting */
        return new byte[] { (byte) 0x8E, 0x12, 0x39, (byte) 0x9C, 0x07,
                0x72, 0x6F, 0x5A, (byte) 0x8E, 0x12, 0x39, (byte) 0x9C, 0x07,
                0x72, 0x6F, 0x5A };

    }

    private  static SecretKeySpec getSecretKeySpecFromHexString(String algoCommonName,String hexString) throws Exception {
        byte [] encodedBytes = hexStrToByteArray(hexString) ;
        return new SecretKeySpec(encodedBytes, algoCommonName);
    }

    private static  byte[] hexStrToByteArray(String hex) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(hex.length() / 2);

        for (int i = 0; i < hex.length(); i += 2) {
            String output = hex.substring(i, i + 2);
            int decimal = Integer.parseInt(output, 16);
            baos.write(decimal);
        }
        return baos.toByteArray();
    }

    private static byte[] copyIVAndCipher(byte[] encryptedText, byte[] iv) throws Exception {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        os.write(iv);
        os.write(encryptedText);
        return os.toByteArray();
    }

    private static String getByteToString(byte[] byteData) throws Exception {
        return new String(byteData, "UTF-8");
    }

}
