package com.freecharge.payment.util;

public enum ActivityLogger {
	ManualRefund, PGStatus, ManualCashBack, ReversePGtoSource,RechargeReversal,RechargeStatus;
}
