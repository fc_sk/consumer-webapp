package com.freecharge.payment.util;

import java.security.MessageDigest;

public class BilldeskCardHash {

    private static final String ENCRYPT_SALT_VALUE = "EA6A429D7E7369D6";

    public static String generateHash(String cardNUmber) {
        MessageDigest md = null;
        try {
            cardNUmber = ENCRYPT_SALT_VALUE + cardNUmber;
            md = MessageDigest.getInstance("SHA-256"); // step 2
            md.update(cardNUmber.getBytes("UTF-8")); // step 3
        } catch (Exception e) {
            md = null;
            throw new IllegalStateException("Error getting hash value for card : " + cardNUmber);
        }

        StringBuffer ls_sb = new StringBuffer();
        byte raw[] = md.digest(); // step 4
        for (int i = 0; i < raw.length; i++)
            ls_sb.append(char2hex(raw[i]));
        return ls_sb.toString(); // step 6
    }

    private static String char2hex(byte x) {
        char arr[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        char c[] = { arr[(x & 0xF0) >> 4], arr[x & 0x0F] };
        return (new String(c));
    }

}
