package com.freecharge.payment.util;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.google.gson.Gson;

/**
 * 
 * @author ruchikagarg
 *
 */
public final class ConvertorUtil {

	private static Logger LOG = LoggingFactory.getLogger(ConvertorUtil.class);

	final static Gson gson = new Gson();

	// TODO : Move info logs to debug
	public static <T> T getObjFromJsonStr(final String jStr, final Class<T> clazz) {
		T obj = null;
		LOG.info("Input : " + jStr);
		try {
			obj = gson.fromJson(jStr, clazz);
		} catch (Exception e) {
			LOG.error("JSONParsingException : " + e);
		}
		return obj;
	}

	// TODO : Move info logs to debug
	public static String getStrFromObj(final Object obj) {
		String result = null;
		LOG.info("Input : " + obj);
		try {
			result = gson.toJson(obj);

		} catch (Exception ex) {
			LOG.error("JSONParsingException : " + ex);
		}
		return result;
	}

}
