package com.freecharge.payment.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.common.util.FCUtil;
import com.google.common.collect.ImmutableList;

public class PaymentConstants {

    //paramter to be submitted to payment portal
    public final static String SECURITY_KEY = "WorkingKey";
    public final static String  AMOUNT = "Amount";
    public final static String  ORDER_ID = "Order_Id" ;
    public final static String  TRANSACTION_TYPE = "TxnType" ;
    public final static String  ACTION_TYPE = "ActionID" ;
    public final static String  CURRENCY = "currency" ;
    public final static String  EXTRA_MERCHANT_PARAM = "Merchant_Param" ;
    public final static String  CHECKSUM_KEY = "Checksum" ;
    public final static String  BILLING_CUST_NAME = "billing_cust_name" ;
    public final static String  BILLING_CUST_ADDRESS = "billing_cust_address" ;
    public final static String  BILLING_CUST_COUNTRY = "billing_cust_country" ;
    public final static String  BILLING_CUST_STATE = "billing_cust_state" ;
    public final static String  BILLING_ZIP = "billing_zip" ;
    public final static String  BILLING_CUST_TEL = "billing_cust_tel" ;
    public final static String  BILLING_CUST_EMAIL = "billing_cust_email" ;
    public final static String  DELIVERY_CUST_NAME = "delivery_cust_name" ;
    public final static String  DELIVERY_CUST_ADDRESS = "delivery_cust_address" ;
    public final static String  DELIVERY_CUST_COUNTRY = "delivery_cust_country" ;
    public final static String  DELIVERY_CUST_STATE = "delivery_cust_state" ;
    public final static String  DELIVERY_CUST_TEL = "delivery_cust_tel" ;
    public final static String  DELIVERY_CUST_NOTES = "delivery_cust_notes" ;
    public final static String  BILLING_CUST_CITY = "billing_cust_city" ;
    public final static String  BILLING_ZIPCODE = "billing_zip_code" ;
    public final static String  DELIVERY_CUST_CITY = "delivery_cust_city" ;
    public final static String  DELIVERY_ZIPCODE = "delivery_zip_code" ;
    public final static String  PAYMENT_CARDOPTION = "cardOption" ;
    public final static String  PAYMENT_NETBANKING_CARDS = "netBankingCards" ;
    public final static String  NONMOTOCARDTYPE = "NonMotoCardType" ;
    public final static String  REDIRECT_URL = "Redirect_Url" ;
    
    // same as order ID
    public final static String  ORDER_NUMBER = "orderNo";
    
    public final static String PAYMENT_TYPE = "paymentType";
    public final static String PAYMENT_OPTION="paymentOption";
    
    
    public final static String PAYMENT_TYPE_CREDITCARD = "1";
    public final static String PAYMENT_TYPE_DEBITCARD = "2";
    public final static String PAYMENT_TYPE_NETBANKING = "3";
    public final static String PAYMENT_TYPE_CASHCARD = "4";
    public final static String PAYMENT_TYPE_ATMCARD = "5";
    //A new payment gateway has been configured using BillDesk for HDFC promo code that taken in only HDFC debit card
    public final static String PAYMENT_TYPE_HDFC_DEBIT_CARD = "7";
    public final static String PAYMENT_TYPE_WALLET = "6";
    public final static String PAYMENT_TYPE_ZEROPAY = "8";
    public final static String PAYMENT_TYPE_ONECHECK_WALLET = "9";
    public final static String PAYMENT_TYPE_FCUPI = "16";
    public static final String ZEROPAY_PAYMENT_OPTION = "ZPG";
    public static final String FREECHARGE_WALLET_PAYMENT_OPTION = "FW";
    public static final String ONECHECK_WALLET_PAYMENT_OPTION = "OCW";
    public static final String FC_UPI_PAYMENT_OPTION = "FCUPI";
    public static final String SOURCE_PG = "PG";
    public static final String AMEX_PAYMENT_OPTION = "AX";
    
    public final static String PAYMENT_TYPE_OPTION_MASTERCARD="1";
    public final static String PAYMENT_TYPE_OPTION_VISACARD = "2";
    public final static String PAYMENT_TYPE_OPTION_AMEX = "3";
    public final static String PAYMENT_TYPE_OPTION_DINERS = "4";

    public final static String PAYMENT_ERROR_TILE_DEFINATION = "payment/error";
    public final static String PRODUCT_PAYMENT_ERROR_TILE_DEFINATION = "productpayment/error";
    public final static String PRODUCT_PAYMENT_REQUEST="productpayment/request";
    public final static String PRODUCT_PAYMENT_OVERLOADED="productpayment/overloaded";

    public final static String PAYMENT_REQUEST_DELEGATE_NAME="paymentRequest";
    public final static String PAYMENT_RESPONSE_DELEGATE_NAME="paymentResponse";


    public final static String VISA_CC = "VI";
    public final static String MASTER_CC = "MC";

    public final static String PAYMENT_MANUAL_REFUND_DELEGATE = "refundManualDelegate";
    public final static String VISA_DC = "DVI";
    public final static String MASTER_DC = "DMC";
    public final static String MAESTRO_SBI = "DSME";
    public final static String MAESTRO_CITI = "DCME";
    public final static String RUPAY_DC = "DRU";

    public final static String PAYMENT_PORTAL_SECURE_KEY="freecharge.payment.portal.securekey";

    public final static String IS_ITTP_TXN = "is_ittp";

    //product Type String
    public final static String ABHIBUS_PRODUCT_TYPE="AB";
    public final static String GIFTCARD_PRODUCT_TYPE="GC";
    public final static String PAID_COUPONS_PRODUCT_TYPE="ABC";
    public final static String RECHARGE_PRODUCT_TYPE="V,M,D,C,F,Y,L,BP";


    //request paramters
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_AMOUNT  = "at";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_BILLINGADDRESS1  = "blad1";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_BILLINGADDRESS2  = "blad2";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_BILLINGADDRESS_COUNTRY  = "blctr";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_BILLINGADDRESS_CITY  = "blct";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_BILLINGAADDRESS_PIN  = "blpin";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_BILLINGADDRESS_STATE  = "blst";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_PAYMENT_OPTION  = "pyop";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_PAYMENT_CURRENCY  = "pycur";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_EMAIL  = "email";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_ERROR_URL  = "errurl";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_FIRST_NAME  = "frnm";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_HASHKEY  = "breaker";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_PAYMENT_DONE_STATUS  = "st";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_LAST_NAME  = "lsnm";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_MIDDLE_NAME  = "mdnm";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_MOBILE  = "mbph";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_PAYMENT_TYPE  = "pytype";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_PAYMENT_GATEWAY_OVERRIDE  = "pgover";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_SUCCESS_URL  = "scurl";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_ORDER_ID  = "orderid";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_AFFILIATE_ID  = "affid";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_PRODUCT_ID  = "pdid";
    public final static String PAYMENT_REQUEST_FROM_PRODUCT_OPERATOR_ID  = "opid";
    public final static String PAYMENT_SUCCESS_RESPONSE_STATUS  = "true";

    public final static String PAYMENT_METHOD_RETURN_STATE = "isval";
    public final static String PAYMENT_METHOD_RETURN_DESCRIPTION = "desc";
    
    public final static String DEFAULT_PAYMENT_GATEWAY_CODE_KEY = "freecharge.payment.portal.defaultgateway.code";
    public final static String DEFAULT_PAYMENT_BANK_CODE_KEY = "freecharge.payment.portal.defaultgateway.bank";

    /*CC Avenue properties constants*/
    public final static String KEY_CCAVENUE_SUCCESS_DECISION = "fcpp.ccavenue.success.decision";
    public final static String KEY_CCAVENUE_MECODE = "fcpp.ccavenue.mecode";
    public final static String KEY_CCAVENUE_WORKING_KEY = "fcpp.ccavenue.working.key";
    public final static String KEY_CCAVENUE_URL = "fcpp.ccavenue.url";
    public final static String KEY_CCAVENUE_URL_OLD = "fcpp.ccavenue.url.default";
    public final static String KEY_CCAVENUE_RETURN_URL = "fcpp.ccavenue.return.url";
    public final static String KEY_CCAVENUE_REFUND_URL = "fcpp.ccavenue.refund.url";
    public final static String KEY_CCAVENUE_CHECK_STATUS_URL = "fcpp.ccavenue.check.status.url";
    public final static String KEY_CCAVENUE_ICICINETBANKING_MECODE = "fcpp.ccavenue.icici.mecode";
    public final static String KEY_CCAVENUE_ICICINETBANKING_WORKING_KEY = "fcpp.ccavenue.icici.working.key";

    public static final String ICICNETBANKING_PAYMENT_OPTION = "ICIC";
    public static final String HDFCNETBANKING_PAYMENT_OPTION = "HDFC";
    public static final String HDFCDEBITCARD_PAYMENT_OPTION = "HDFD";
    
    /*ICICI SSL properties constants*/
    public final static String KEY_ICICI_SSL_SUCCESS_DECISION = "fcpp.icicissl.success.decision";
    public final static String KEY_ICICI_SSL_MECODE = "fcpp.icicissl.mecode";
    public final static String KEY_ICICI_SSL_URL = "fcpp.icicissl.url";
    public final static String KEY_ICICI_SSL_RETURN_URL = "fcpp.icicissl.return.url";
    public final static String KEY_ICICI_SSL_REFUND_URL = "fcpp.icicissl.refund.url";
    public final static String KEY_ICICI_SSL_CHECK_STATUS_URL = "fcpp.icicissl.check.status.url";

    public final static String PAYMENT_GATEWAY_CODE_KEY ="paymentGatewayName";
    public final static String PAYMENT_GATEWAY_CCAVENUE_CODE = "cca";
    public final static String PAYMENT_GATEWAY_BILLDESK_CODE = "bdk";
    public final static String PAYMENT_GATEWAY_FCWALLET_CODE = "fw";
    public final static String PAYMENT_GATEWAY_ZEROPG_CODE = "zpg";
    public final static String PAYMENT_GATEWAY_ICICI_SSL_CODE = "ics";
    public final static String PAYMENT_GATEWAY_FCUPI_CODE = "fcupi";
    public final static String PAYMENT_GATEWAY_PAYU_CODE = "payu";
    public final static String PAYMENT_GATEWAY_KLICKPAY_CODE = "kpay";
    public final static String PAYMENT_GATEWAY_DUMMY_FREECHARGE_CODE = "fc";
    public final static String ONE_CHECK_WALLET_CODE = "ocw";
    
    public final static String ONECHECK_FC_WALLET_PG_NAME = "fcw";

    public static final String OCW_MIGRATION = "WALLET_MIGRATION";
    public static final String OCW_MIGRATION_FAILED = "WALLET_MIGRATION_FAILED";
    public static final String OCW_MIGRATION_SUCCESS = "WALLET_MIGRATION_SUCCESS";
    public static final String ENCASH_CREDIT = "WALLET_MIGRATION_SUCCESS";

    public final static String KEY_PAYMENT_PORTAL_HOST_NAME = "payment.portal.response.hosturl";
    public final static String LOAD_MONEY_HOST = "load.money.host";

    public final static String WAITPAGE_REQUEST_VIEW ="payment/request";
    public final static String WAITPAGE_RESPONSE_VIEW ="payment/response";
    public final static String WAITPAGE_VIEW ="payment/wait";

    public final static String KEY_WAITPAGE ="waitpageKey";

    public final static String IS_METHOD_SUCCESSFUL = "success";
    public final static String DATA_MAP_FOR_WAITPAGE = "resultMap";
    public final static String PAYMENT_GATEWAY_SUBMIT_URL = "actionUrl";
    public final static String UPI_PAYMENT_FLAG = "upiPayment";

    public final static String AFFILIATE_FREECHARGE = "1";

    public final static String PAYMENT_PORTAL_RETURN_STATUS_SUCCESS = "success";
    public final static String PAYMENT_PORTAL_RETURN_STATUS_FAILURE = "failure";
    public final static String PAYMENT_PORTAL_RETURN_STATUS_ERROR = "error";
    
    public final static String PAYMENT_ALREADY_ACKNOLEDGED="paymentAlreadyAcknoledged";
    public final static String PAYMENT_PORTAL_IS_SUCCESSFUL = "suc";
    public final static String RECHARGE_RESPONSE_CODE = "RechargeResponseCode";
    public final static String PAYMENT_PORTAL_RESULT_DESCRIPTION = "desc";
    public final static String PAYMENT_PORTAL_RESULT_RESPONSE_CODE = "rescode";
    public final static String PAYMENT_PORTAL_ORDER_ID = "orderId";
    public final static String WALLET_TOPUP_ADD = "add";
    public final static String PAYMENT_PORTAL_AMOUNT = "amount";
    public final static String PAYMENT_PORTAL_TRANSACTION_ID = "transId";
    public final static String PAYMENT_PORTAL_PAYMENT_GATEWAY = "pg";
    public final static String PAYMENT_PORTAL_MODE = "mode";
    public final static String PAYMENT_PORTAL_MERCHANT_ID = "mecode";
    public final static String PAYMENT_PORTAL_SEND_RESULT_TO_URL = "redirectUrl";
    public final static String PAYMENT_PORTAL_CUSTOMER_SHIPPING_ADDRESS = "shippingaddress";
    public final static String PAYMENT_PORTAL_CUSTOMER_RECHARGE_MOBILE_NUMBER = "rcmobilenumber";
    public final static String PAYMENT_PORTAL_CUSTOMER_EMAIL_ID = "customeremailid";
    public final static String PAYMENT_PORTAL_CUSTOMER_NAME = "customername";	
    public final static String PAYMENT_PORTAL_CUSTOMER_ADDRESS = "customeraddress";	
    public final static String PAYMENT_PORTAL_CUSTOMER_AREA = "customerarea";	
    public final static String PAYMENT_PORTAL_CUSTOMER_LANDMARK = "customerlandmark";	
    public final static String PAYMENT_PORTAL_CUSTOMER_CITY = "customercity";	
    public final static String PAYMENT_PORTAL_CUSTOMER_STATE = "customerstate";	
    public final static String PAYMENT_PORTAL_CUSTOMER_PINCODE = "customerpincode";	

    public final static String PAYMENT_PORTAL_SEND_RESULT_ERROR = "error";

    public final static String KEY_PAYMENT_PORTAL_AMOUNT_OVERRIDE = "payment.portal.amount.override";

    public static final String UTF_8_ENCODING = "UTF-8";

    public static final String PARAM_BILLDESK_TXN_ADDITIONAL_INFO_1 = "txtAdditionalInfo1";
    public static final String PARAM_BILLDESK_TXN_ADDITIONAL_INFO_2 = "txtAdditionalInfo2";
    public static final String PARAM_BILLDESK_TXN_ADDITIONAL_INFO_3 = "txtAdditionalInfo3";
    public static final String PARAM_BILLDESK_TXN_ADDITIONAL_INFO_4 = "txtAdditionalInfo4";
    public static final String PARAM_BILLDESK_TXN_ADDITIONAL_INFO_5 = "txtAdditionalInfo5";
    public static final String PARAM_BILLDESK_TXN_ADDITIONAL_INFO_6 = "txtAdditionalInfo6";
    public static final String PARAM_BILLDESK_TXN_ADDITIONAL_INFO_7 = "txtAdditionalInfo7";

    public static final String BILLDESK_RESPONSE_MESSAGE_KEY = "msg";

    /*BILLDESK properties constants*/
    public final static String KEY_BILLDESK_SUCCESS_DECISION = "fcpp.billdesk.success.decision";
    public final static String KEY_BILLDESK_MECODE = "fcpp.billdesk.mecode";
    public final static String KEY_BILLDESK_WORKING_KEY = "fcpp.billdesk.working.key";
    public final static String KEY_BILLDESK_URL = "fcpp.billdesk.url";
    public final static String KEY_BILLDESK_URL_OLD = "fcpp.billdesk.url.default";
    public final static String KEY_BILLDESK_RETURN_URL = "fcpp.billdesk.return.url";
    public final static String KEY_BILLDESK_REFUND_URL = "fcpp.billdesk.refund.url";
    public final static String KEY_BILLDESK_CHECK_STATUS_URL = "fcpp.billdesk.check.status.url";
    
    
    /* PayU property constants */
    public final static String PAYU_MERCHANT_ID = "fcpp.payu.merchantid";
    public final static String PAYU_HASH_SALT = "fcpp.payu.hash.salt";
    public final static String PAYU_SUBMIT_URL = "fcpp.payu.submit.url";
    public final static String PAYU_STATUS_CHECK_URL = "fcpp.payu.statuscheck.url";
    
    /* Klickpay property constants */
    public final static String PG_USED_KEY_STRING = "pgUsed";
    public final static String REFERENCE_ID_KEY_STRING = "referenceId";
    public final static String KLICKPAY_MID_KEY_STRING = "merchantId";
    public final static String KLICKPAY_MERCHANT_ID = "fcpp.klickpay.merchantid";
    public final static String KLICKPAY_MERCHANT_KEY = "fcpp.klickpay.merchantkey";
    public final static String KLICKPAY_UTILITY_MERCHANT_ID = "fccp.klickpay.utility.merchantid";
    public final static String KLICKPAY_UTILITY_MERCHANT_KEY = "fccp.klickpay.utility.merchantkey";
    public final static String KLICKPAY_ABHIBUS_MERCHANT_ID="fcpp.klickpay.abhibus.merchantid";
    public final static String KLICKPAY_ABHIBUS_MERCHANT_KEY="fcpp.klickpay.abhibus.merchantkey";
    public final static String KLICKPAY_COUPONS_MERCHANT_ID="fcpp.klickpay.coupons.merchantid";
    public final static String KLICKPAY_COUPONS_MERCHANT_KEY="fcpp.klickpay.coupons.merchantkey";
    public final static String KLICKPAY_GIFTCARD_MERCHANT_ID="fcpp.klickpay.giftcard.merchantid";
    public final static String KLICKPAY_GIFTCARD_MERCHANT_KEY="fcpp.klickpay.giftcard.merchantkey";
    public final static String KLICKPAY_SUBMIT_URL = "fcpp.klickpay.submit.url";
    public final static String KLICKPAY_QC_STATUS_URL = "fcpp.klickpay.qc.status.url";
    public final static String KLICKPAY_CARD_STATUS_URL = "fcpp.klickpay.card.status.url";
    public final static String KLICKPAY_STATUS_CHECK_URL = "fcpp.klickpay.statuscheck.url";
    public final static String KLICKPAY_REFUND_URL = "fcpp.klickpay.refund.url";
    public final static String KLICKPAY_USER_TRANSACTION_URL = "fcpp.klickpay.user.transaction.url";
    public final static String KLICKPAY_DEBIT_ATM_STATUS_URL = "fcpp.klickpay.debit.atm.status.url";
    public final static String KLICKPAY_ITTP_STATUS_URL = "fcpp.klickpay.ittp.status.url";
    public final static String KLICKPAY_ITTP_RESEND_OTP_URL = "fcpp.klickpay.ittp.resend.otp.url";
    public final static String KLICKPAY_TRANSACTIONTYPE_STATUS_URL = "fcpp.klickpay.transactiontype.status.url";
    public final static String KLICKPAY_REFUND_STATUS_URL = "fcpp.klickpay.refund.status.url";
    public final static String KLICKPAY_DOWNTIME_URL = "fcpp.klickpay.downtime.url";
    public final static String KLICKPAY_S2S_INIT_URL = "fcpp.klickpay.s2s.init.url";
    public final static String KLICKPAY_S2S_UPI_URL = "fcpp.klickpay.s2s.upi.url";
    public final static String KLICKPAY_S2S_AUTH_URL = "fcpp.klickpay.s2s.auth.url";
    public final static String KLICKPAY_GET_CARDTYPE_FROM_BINS_URL = "fcpp.klickpay.bins.cardtype.url";
    
    /* Klickpay Feature Release Service Constants */
    public final static String KLICKPAY_FEATURE_RELEASE_URL = "fcpp.klickpay.feature.release.url";
    public final static String KLICKPAY_FEATURE_NAME = 	"featureName";
    public final static String KLICKPAY_FEATURE_USER_EMAIL = 	"email";
    public final static String KLICKPAY_FEATURE_MERCHANT_CODE = 	"merchantCode";

    public static final String ICICI_SECURE_COOKIE_KEY = "secureCookie";

    public static final String PURCHASE_MODE = "pur";

    //Used as wallet params
    public static final String MERCHANT_ORDER_ID = "merchantOrderId";
    public static final String TRANSACTION_ID = "transactionId";
    //Using double aa to distinguish from the amount constant already defined
    public static final String AAMOUNT = "amount";
    public static final String RESULT = "result";
    public static final String USER_ID = "userId";
    public static final String HASH = "hash";
    public static final String MESSAGE = "message";
    
    public static final String MERCHANT_TXN_ID = "mtxn_id";

    public static final String NA = "NA";

    //pg admin related properties
    public static final String KEY_PG_CHANGE_MAIL_RECEIVER = "payment.pg.change.mailto";
    public static final String KEY_PG_CHANGE_MAIL_SUBJECT =  "payment.pg.change.subject";
    public static final String KEY_PG_CHANGE_MAIL_FROM = "payment.pg.change.mailfrom";
    
    //refund credit Failure types
    public static final String PG_FAILURE = "PG_FAILURE";
    public static final String RECHARGE_FAILURE = "RECHARGE_FAILURE";
    public static final String MULTIPLE_PAYMENT_SUCCESS = "MULTIPLE_PAYMENT_SUCCESS";
    public static final String SCHEDULED_REFUND = "SCHEDULED_REFUND";
    public static final String PAYMENT_SUCCESS = "PAYMENT_SUCCESS";
    public static final String REFUND_AMOUNT_KEY = "refundAmount";
    public static final String HCOUPON_REFUND = "HCOUPON_REFUND";
    public static final String PG_SUCCESS_NOT_IN_TRANCE_FAILURE = "PG_SUCCESS_NO_IN";
    public static final String MANUAL_REFUND = "MANUAL_REFUND";
    public static final String CS_BANK_REFUND = "CS_BANK_REFUND";
    public static final String EXCESS_CREDIT = "EXCESS_CREDIT";
    public static final String ADMIN_REFIND = "AUTO_REFUND";
    public static final String ADMIN_CASH_BACK = "CASHBACK";
    public static final String CS_CASHBACK = "CS_CASHBACK";
    public static final String PAYMENT_SUCCESS_CREDIT_TO_WALLET = "PG_SUCCESS_WALLET_CREDIT";
    public static final String WALLET_FUND = "WALLET_FUND";
    public static final String ALL = "ALL";
    public static final String DEBIT_CARD = "Debit Card";
    public static final String CREDIT_CARD = "Credit Card";
    public static final String NET_BANKING = "Net Banking";
    public static final String PAYMENT_ADJUSTMENT = "PG_ADJUSTMENT";
    public static final String ORDER_ID_KEY = "orderId";
    public static final String BILL_ID_KEY = "billId";
    public static final String LOOKUP_ID_KEY = "lookupId";
    public static final String PAY_TXN_ID = "paymentTxnId";
    public static final String IS_SWEEP = "isSweep";
    public static final String IS_REFUND = "isRefund";
    public static final String SHOULD_SEND_SMS_KEY = "shouldSendSMS";
    public static final String REFUND_TYPE_KEY = "refundType";
    public static final String REFUND_TO_PG = "REFUND_TO_PG";
    public static final String REFUND_TO_BANK = "REFUND_TO_BANK";
    public static final String REFUND_TO_WALLET = "refundToWallet";
    public static final String FORCED_REFUND = "FORCED_REFUND";
    public static final String LOB3_FLAG_VALUE = "LOB3";
    public static final String MOB_MERCHANT_ID = "mob_merchant_id";
    
    public static final String NODAL_FLAG = "nodalFlag";
    public static final String NODAL_FLAG_ENABLE_TIME = "nodal_flag_enable_time";
    public static final String WALLET_CORP_ID_FLAG = "wallet_corp_id_flag";
    //Card Bin Data
    public static final String CARD_BIN_HEAD = "binHead";
    public static final String CARD_BIN_TAIL = "binTail";
    public static final String PAYMENT_STATUS = "payStatus";
    
    //Bill Payment related properties and params
    public static final String BILL_PAPYMENT_TYPE = "postpaid";
    public static final String BILL_PAPYMENT_TYPE_STATUS = "1";
    public static final String BILL_PAPYMENT_DEFAULT_GATEWAY = "billPaymentOxigenClient";
    public static final String BILL_PAYMENT_BILLDESK_GATEWAY = "billdeskClient";
    public static final String BILL_PAYMENT_OXIGEN_GATEWAY = "billPaymentOxigenClient";
    public static final String BILL_PAYMENT_EURONET_GATEWAY = "BillPaymentEuronetClient";
    
    public static final String BILL_PAYMENT_OXIGEN_GATEWAY_LOWER = "billpaymentoxigenclient";
    public static final String BILL_PAYMENT_EURONET_GATEWAY_LOWER = "billpaymenteuronetclient";
    
    public final static String BILL_PAPYMENT_INTERNAL_SUCCESS_RESPONSE_CODE = "1";
    public static final String BILL_ORDERID_START_STRING = "FCM";
    
    
    //New payments flow indicator
    public static final String NEW_PAYMENTS_FLOW="newPaymentsFlow";
    public static final String NEW_PAYMENTS_PAGE="isNewPaymentPage";
    
    //New Mobile web flow
    //New payments flow indicator
    public static final String IS_MOBILE_WEB="isMobileWeb";
    
    // Mobile App - New Payment Flow
    public static final String IS_MOBILE_APP="isMobileApp";
    
    // New Pay API Error codes
    public static final String ERROR_CODE_GENERAL="10000";
    public static final String ERROR_CODE_FRAUD="10001";
    public static final String ERROR_INVALID_PAY_TYPE="10002";
    public static final String ERROR_INCOMPLETE_PAY_REQUEST="10003";
    public static final String ERROR_CARD_PAY="10004";
    public static final String ERROR_CARD_DETAILS="10005";
    public static final String ERROR_DATA_NOT_FOUND_IN_LOCAL_CACHE = "10006";
    public static final String ERROR_INVALID_BENEFICIARY_VPA = "10007";

    public static final Map<String, String> paymentErrorCodeMap = FCUtil.createMap(
            ERROR_CODE_GENERAL, "Payment did not complete successfully.",
            ERROR_CODE_FRAUD, "Payment Gateway Overloaded.",
            ERROR_INVALID_PAY_TYPE, "Invalid Payment Type.",
            ERROR_INCOMPLETE_PAY_REQUEST, "Payment request is incomplete.",
            ERROR_CARD_PAY, "Please check the card details.",
            ERROR_DATA_NOT_FOUND_IN_LOCAL_CACHE, "Please restart the transaction.",
            ERROR_INVALID_BENEFICIARY_VPA, "Invalid or empty beneficiary vpa");
    
    public static final List<String>        debitCardUIPaymentOptions  = ImmutableList
            .of("DMC", "DVI", "DSME", "DCME", "DRU");
    public static final List<String>        netBankingUIPaymentOptions = ImmutableList.of("ICIC", "AXIS", "CITA", "HDFC",
            "IDBI", "KOBK", "SBIN", "PJRB", "YEBK", "BOI",
            "IOB", "UBI", "ANDB", "ALLB", "BAHK", "BOBC",
            "BOBR", "MAHB", "CANB", "CENB", "CITU",
            "CORB", "CSYB", "DENB", "DEUB", "DECB",
            "DHLB", "FEDB", "INDB", "INIB", "INVB",
            "JNKB", "KRTB", "KARB", "LXCB", "LXRB",
            "ORTB", "PJSB", "PJCB", "RATB", "SHVB",
            "SINB", "STCB", "SBJB", "SHYB", "SMYB",
            "SPTB", "STVB", "SYNB", "TMEB", "UCOB",
            "UNIB", "VIJB", "AX");
    public static final List<String>        cashCardUIPaymentOptions   = ImmutableList.of("ITZ", "DON", "OXI", "ATMY");
    public static final List<String>        atmCardUIPaymentOptions    = ImmutableList.of("ABOI", "ALVB", "AIOS", "APNB",
            "ASBI", "AUBI", "ACAN", "ACIT");

    public static final List<String> DISABLED_FREEFUND_CLASS_FOR_INVOICE = Arrays.asList(FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK, FreefundClass.PROMO_ENTITY_CASHBACK);
    
    public static final String PAYMENT_PROXY_HOST = "payment.proxy.host";
    public static final String PAYMENT_PROXY_PORT = "payment.proxy.port";
    public static final String BILLDESK_POC = "bd.poc";
    public static final String BILLDESK_POC_ENABLED = "enabled";
    public static final String BILLDESK_POC_CONDITION = "condition";
    
    // Billdesk for the debit cards.
    public static final String BILLDESK_DEBIT_POC = "bd.debit.poc";
    
    public static final String BILLDESK_DEBIT_MASTER_ENABLED = "debit.master.enabled";
    public static final String BILLDESK_DEBIT_MASTER_CONDITION = "debit.master.condition";
    
    public static final String BILLDESK_DEBIT_VISA_ENABLED = "debit.visa.enabled";
    public static final String BILLDESK_DEBIT_VISA_CONDITION = "debit.visa.condition";
    
    public static final String BILLDESK_DEBIT_MAESTRO_ENABLED = "debit.maestro.enabled";
    public static final String BILLDESK_DEBIT_MAESTRO_CONDITION = "debit.maestro.condition";
    
    public static final String CARD_HOLDER_NAME_KEY = "cardHolderName";
    public static final String CARD_NUM_KEY = "cardNumber";
    public static final String CARD_EXPMON_KEY = "cardExpMonth";
    public static final String CARD_EXPYEAR_KEY = "cardExpYear";
    public static final String CARD_CVV_KEY = "cardCvv";
    public static final String CARD_SAVE_BOOL = "saveCard";
    public static final String CARD_SAVE_NAME = "saveCardName";
    public static final String CARD_TOKEN = "cardToken";
    public static final String IS_DEBIT_CARD = "isDebitCard";
    
    public static final String MAESTRO_DEFAULT_EXPIRY_YEAR = "2049";
    public static final String MAESTRO_DEFAULT_EXPIRY_MONTH = "12";
    
    
    // Default values for the Maestro Debit cards for "BillDesk". 
    public static final String MAESTRO_BD_DEFAULT_EXPIRY_YEAR = "2099";
    public static final String MAESTRO_BD_DEFAULT_EXPIRY_MONTH = "12";
    public static final String MAESTRO_BD_DEFAULT_CVV = "111";
    
    public static final List<String> debitCardNonMaestroList = new ArrayList<String>(Arrays.asList("VISA", "MASTERCARD", "RUPAY"));
    public static final String debitCardMaestroText = "MAESTRO";
    
    public static final Map<String, String> cardStorageMasterMap = FCUtil.createMap(
            PaymentConstants.VISA_CC, "VISA",
            PaymentConstants.MASTER_CC, "MASTERCARD",
            PaymentConstants.VISA_DC, "VISA",
            PaymentConstants.MASTER_DC, "MASTERCARD",
            PaymentConstants.MAESTRO_SBI, "MAESTRO",
            PaymentConstants.MAESTRO_CITI, "MAESTRO",
            PaymentConstants.RUPAY_DC, "RUPAY");
    
    public static final Map<String, String> cardTypeMasterMap = FCUtil.createMap(
            "1", "CREDIT",
            "2", "DEBIT");
    
    public static final Map<String, String> debitCardStorageMasterMap = FCUtil.createMap(
            "VISA", PaymentConstants.VISA_DC,
            "MASTERCARD", PaymentConstants.MASTER_DC,
            "MAESTRO_SBI", PaymentConstants.MAESTRO_SBI,
            "MAESTRO", PaymentConstants.MAESTRO_CITI,
            "RUPAY", PaymentConstants.RUPAY_DC );
    
    public static final String PAYU_RESPONSE_ERROR_MSSG_FIELD = "field9";
    public static final String PAYU_RESPONSE_GATEWAY_FIELD = "PG_TYPE";
	public static final String PAYU_RISK_VIOLATION_RESPONSE_MSSG = "The card is recognized as domestic by PayU, and domestic card rule failed";
	public static final String PAYU_RESPONSE_PAYMENT_TYPE_FIELD = "mode";
	public static final String PAYU_RESPONSE_PAYMENT_OPTION_FIELD = "card_type";
	public static final String PAYU_RESPONSE_BANK_CODE_FIELD = "bankcode";
	
	public static final int CARD_NUM_MIN_LENGTH = 10;
	public static final int CARD_NUM_MAX_LENGTH = 20;
	// Constants to throw invalid input card details.
	public static final String INVALID_CARD_NUMBER = "Card number is invalid";
	public static final String INVALID_CARD_EXPIRY_MONTH = "Card expiry month is invalid";
	public static final String INVALID_CARD_EXPIRY_YEAR = "Card expiry year is invalid";
	public static final String INVALID_CARD_CVV = "Card CVV is invalid";


	public static List<PayUNBCodeMapping> nbCodeMappings = Arrays.asList(new PayUNBCodeMapping("AXIS", "AXIB"),
																	 	 new PayUNBCodeMapping("BOI", "BOIB"),
																	 	 new PayUNBCodeMapping("CENB", "CBIB"),
																	 	 new PayUNBCodeMapping("CORB", "CRPB"),
																	 	 new PayUNBCodeMapping("DECB", "DCBB"),
																	 	 new PayUNBCodeMapping("FEDB", "FEDB"),
																	 	 new PayUNBCodeMapping("ICIC", "ICIB"),
																	 	 new PayUNBCodeMapping("HDFC", "HDFB"),
																	 	 new PayUNBCodeMapping("INDB", "INDB"),
																	 	 new PayUNBCodeMapping("IOB", "INOB"),
																	 	 new PayUNBCodeMapping("INIB", "INIB"),
																	 	 new PayUNBCodeMapping("JNKB", "JAKB"),
																	 	 new PayUNBCodeMapping("KARB", "KRVB"),
																	 	 new PayUNBCodeMapping("KRTB", "KRKB"),
																	 	 new PayUNBCodeMapping("SINB", "SOIB"),
																	 	 new PayUNBCodeMapping("UBI", "UBIB"),
																	 	 new PayUNBCodeMapping("YEBK", "YESB"),
																	 	 new PayUNBCodeMapping("SBIN", "SBIB"),
																	 	 new PayUNBCodeMapping("SMYB", "SBMB"),
																	 	 new PayUNBCodeMapping("STVB", "SBTB"),
																	 	 new PayUNBCodeMapping("SHYB", "SBHB"),
																	 	 new PayUNBCodeMapping("SBJB", "SBBJB"),
																	 	 new PayUNBCodeMapping("VIJB", "VJYB"),
																	 	 new PayUNBCodeMapping("UNIB", "UNIB"),
																	 	 new PayUNBCodeMapping("MAHB", "BOMB"),
																	 	 new PayUNBCodeMapping("IDBI", "IDBB"),
																	 	 new PayUNBCodeMapping("CITU", "CUBB"),
																	 	 new PayUNBCodeMapping("CANB", "CABB"),
																	 	 new PayUNBCodeMapping("SPTB", "SBPB"),
																	 	 new PayUNBCodeMapping("CITA", "CITNB"),
																	 	 new PayUNBCodeMapping("DEUB", "DSHB"),
																	 	 new PayUNBCodeMapping("KOBK", "162B"),
																	 	 new PayUNBCodeMapping("DHLB", "DHANNB"),
																	 	 new PayUNBCodeMapping("INVB", "INGB")
																		 );
	
	public static final String TRANSACTION_STATUS_PAYMENT_INITIATED = "01";
	public static final String TRANSACTION_STATUS_PAYMENT_REDIRECTED_TO_BANK = "02";
	public static final String TRANSACTION_STATUS_PAYMENT_RESPONSE_RECEIVED = "03";
	public static final String TRANSACTION_STATUS_PAYMENT_SUCCESS = "04";
	public static final String TRANSACTION_STATUS_PAYMENT_FAILURE = "05";
	
    public static final String              TRANSACTION_STATUS_REFUND_DONE                        = "101";
    public static final String              TRANSACTION_STATUS_REFUND_INITIATED                   = "102";
    public static final String              TRANSACTION_STATUS_REFUND_FAILED                      = "103";
    public static final String              TRANSACTION_STATUS_EXCESS_REFUND                      = "104";
    public static final String              TRANSACTION_STATUS_UP_FORCE_REFUND                       = "105";
    

    public static final String              TRANSACTION_STATUS_INVALID_PAYMENT_PLAN               = "111";
    public static final String              FULFILLMENT_UNKNOWN_STATUS                            = "09";
    public static final String              FULFILLMENT_PENDING_STATUS                            = "08";


	
	
    public final static String PAYMENT_REQUEST_PARAM_PAYMENT_PURPOSE  = "paymentPurpose";
    public final static String PAYMENT_PAYMENT_PURPOSE_GIFT_VOUCHER_LOAD  = "GVLoad";

	
    public static final String              KLICKPAY                                              = "klickpay";
    public static final String              KLICKPAY_CREDIT_ENABLED                               = "credit.enabled";
    public static final String              KLICKPAY_CREDIT_PERCENTAGE                            = "credit.perc";
    public static final String              KLICKPAY_DEBIT_ENABLED                                = "debit.enabled";
    public static final String              KLICKPAY_DEBIT_PERCENTAGE                             = "debit.perc";
    public static final String              KLICKPAY_NB_ENABLED                                   = "nb.enabled";
    public static final String              KLICKPAY_NB_PERCENTAGE                                = "nb.perc";
    public static final String              KLICKPAY_CASHCARD_ENABLED                             = "ccd.enabled";
    public static final String              KLICKPAY_CASHCARD_PERCENTAGE                          = "ccd.perc";
    public static final String              KLICKPAY_PAYU                                         = "payu";
    public static final String              KLICKPAY_BDK                                          = "bdk";
    
    public static final String              CARD_VALIDATION_INVALID_CARD_NUMBER                   = "Please enter a valid card number.";
    public static final String              CARD_VALIDATION_INVALID_CARD_EXPIRY                   = "Invalid expiry date.";
    public static final String              CARD_VALIDATION_CARD_EXPIRED                          = "Card has expired. Please use a valid card.";
    public static final String              CARD_VALIDATION_INVALID_CARD_CVV                      = "Invalid CVV.";
    public static final String              CARD_VALIDATION_HOTLISTED_CARD                        = "Your bank denied the transaction due to risk. Contact your bank for assistance.";
    public static final String              CARD_VALIDATION_NON_DEOMESTIC_CARD                    = "We do not accept international cards at the moment.";
    
    public static final String              DEBIT_ATM_PRODUCTS_CACHE_KEY                          = "DEBIT_ATM_PRODUCTS_KEY";
    public static final String              FAILURE_REASON_CACHE_KEY                              = "_FAILURE_REASON";
    
    public static final String              PROCESS_PAY_TASK_TYPE                                 = "processPayTask";
    public static final long                DEFAULT_PROCESS_PAY_RETRY_LIMIT                       = 14;
    public static final int                 TASK_EXPONENTIAL_BASE                                 = 3;
    public static final long                PROCESS_PAY_TASK_FAILURE_REEXECUTION_WAIT_TIME        = 5*60*1000;
    public static final String              TASK_ALREADY_CREATED                                  = "TSM-ER-5104";
    
	public static final List<String> paymentTransactionStatusToExclude = Arrays
            .asList(PaymentConstants.TRANSACTION_STATUS_PAYMENT_INITIATED,
                    PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE,
                    PaymentConstants.TRANSACTION_STATUS_PAYMENT_SUCCESS,
                    PaymentConstants.TRANSACTION_STATUS_PAYMENT_REDIRECTED_TO_BANK,
                    PaymentConstants.TRANSACTION_STATUS_PAYMENT_RESPONSE_RECEIVED);
	
	public static final List<String> paymentTransactionStatusToExcludeForRefund = Arrays
            .asList(PaymentConstants.TRANSACTION_STATUS_PAYMENT_INITIATED,
                    PaymentConstants.TRANSACTION_STATUS_PAYMENT_FAILURE,
                    PaymentConstants.TRANSACTION_STATUS_PAYMENT_REDIRECTED_TO_BANK,
                    PaymentConstants.TRANSACTION_STATUS_PAYMENT_RESPONSE_RECEIVED);
	
    public final static String APP_VERSIONS_DISABLED_UPI = "app.versions.disabled.upi";
    public final static String UPI_DISABLED_VERSION_LIST = "disabledVersions";

	public static class PayUNBCodeMapping{

		private String fcCode;

		private String payUCode;
		
		public PayUNBCodeMapping(String fcCode, String payUCode) {
			this.fcCode = fcCode;
			this.payUCode = payUCode;
		}

		public String getFcCode() {
			return fcCode;
		}
		
		public String getPayUCode() {
			return payUCode;
		}
	}
	
    public static final List<String> VIRTUAL_CARD_BIN_LIST = new ArrayList<String>(Arrays.asList("55776310",
                                                                   "55776323", "55776335", "55776358", "55776392"));
}
