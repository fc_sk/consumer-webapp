package com.freecharge.payment.util;

import java.util.Arrays;
import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.freecharge.app.service.exception.CardValidationException;

public class CardValidationUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(CardValidationUtil.class);

    public static void validateCardDetails(String cardNumber, String cardExpiryMonth, String cardExpiryYear,
            String cardCVV) throws CardValidationException {
        if (Arrays.asList("0", "1", "2", "7", "8", "9").contains(String.valueOf(cardNumber.charAt(0)))) {
            throw new CardValidationException(PaymentConstants.CARD_VALIDATION_INVALID_CARD_NUMBER);
        }

        if (cardNumber.length() < 14 || cardNumber.length() > 19) {
            throw new CardValidationException(PaymentConstants.CARD_VALIDATION_INVALID_CARD_NUMBER);
        }

        if (cardNumber.startsWith("4") && cardNumber.length() != 16) {
            throw new CardValidationException(PaymentConstants.CARD_VALIDATION_INVALID_CARD_NUMBER);
        }

        if (!luhnCheckValidated(cardNumber)) {
            LOGGER.info("Luhn validation failed for card");
            throw new CardValidationException(PaymentConstants.CARD_VALIDATION_INVALID_CARD_NUMBER);
        }
        if (StringUtils.isEmpty(cardExpiryMonth) || StringUtils.isEmpty(cardExpiryYear)
                || StringUtils.isEmpty(cardCVV)) {
            return;
        }
        int expiryYear = Integer.parseInt(cardExpiryYear);
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        if (expiryYear < currentYear) {
            throw new CardValidationException(PaymentConstants.CARD_VALIDATION_CARD_EXPIRED);
        }
        // Expiry Year is 2049 for SBI Maestro cards
        if (expiryYear != 2049 && expiryYear >= currentYear + 30) {
            throw new CardValidationException(PaymentConstants.CARD_VALIDATION_INVALID_CARD_EXPIRY);
        }
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
        int expiryMonth = Integer.parseInt(cardExpiryMonth);
        if (expiryYear == currentYear && (expiryMonth < currentMonth + 1)) {
            throw new CardValidationException(PaymentConstants.CARD_VALIDATION_CARD_EXPIRED);
        }

        String cvvRegex = "[0-9]{3,4}";
        if (!cardCVV.matches(cvvRegex)) {
            throw new CardValidationException(PaymentConstants.CARD_VALIDATION_INVALID_CARD_CVV);
        }
    }

    private static boolean luhnCheckValidated(String cardNumber) {
        if (StringUtils.isEmpty(cardNumber)) {
            return true;
        }
        int sumOfEvenDigits = 0, sumOfOddDigits = 0;
        String reverse = new StringBuffer(cardNumber).reverse().toString();
        for (int i = 0; i < reverse.length(); i++) {
            int digit = Character.digit(reverse.charAt(i), 10);
            if (i % 2 == 0) {
                sumOfEvenDigits += digit;
            } else {
                sumOfOddDigits += 2 * digit;
                if (digit >= 5) {
                    sumOfOddDigits -= 9;
                }
            }
        }
        boolean isValid = (sumOfEvenDigits + sumOfOddDigits) % 10 == 0;
        return isValid;
    }
}
