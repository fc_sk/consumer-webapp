package com.freecharge.payment.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.concurrent.ThreadLocalRandom;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.CreditCardValidator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCPropertyPlaceholderConfigurer;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestCardDetails;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentGatewayHandler.RequestParameters;
import com.freecharge.payment.services.binrange.CardBin;
import com.freecharge.web.webdo.CardDetailsWebDO;
import com.freecharge.web.webdo.ProductPaymentWebDO;
import com.freecharge.zeropayment.ZeroPaymentController;
import com.google.common.base.CharMatcher;
import com.google.common.collect.ImmutableMap;
import com.sun.net.ssl.internal.www.protocol.https.BASE64Encoder;

public class PaymentUtil {
    
    public final static Logger logger = LoggingFactory.getLogger(PaymentUtil.class);
    
    private static final List<String> blackListedKeys = new ArrayList<String>(Arrays.asList(
            "ccNum", "ccExpMon", "ccExpYear", "ccCvv",
            "dbNum", "dbExpMon", "dbExpYear", "dbCvv",
            "CCNUM", "CCVV", "CCEXPMON", "CCEXPYR", "card_num", "card_exp_yr", "card_exp_mon", "card_cvv",
            PaymentConstants.CARD_NUM_KEY, PaymentConstants.CARD_EXPYEAR_KEY,
            PaymentConstants.CARD_EXPMON_KEY, PaymentConstants.CARD_HOLDER_NAME_KEY,
            PaymentConstants.CARD_CVV_KEY
            ));
    
    private static final List<String> excludedKeysForResponse = Arrays.asList("card_no","cardnum", "bank_ref_num", "field4", "field3", "bank_ref_no");
    
    @SuppressWarnings("rawtypes")
    public static Map<String, String> getRequestParamaterAsMap(HttpServletRequest request) {
        Map<String, String> map = new HashMap<String, String>();
        Enumeration enumeration = request.getParameterNames();

        while (enumeration.hasMoreElements()) {
            String name = (String) enumeration.nextElement();
            String value = request.getParameter(name);

            if (StringUtils.isNotBlank(value)) {
                map.put(name, value);
            }
        }
        return map;
    }

    public static List<String> requestValidation(String paymentType) {
        List<String> msgs = new ArrayList<>();
        if (paymentType == null || StringUtils.isBlank(paymentType)) {
            msgs.add("payment.error.internal.message.pt");
        }
        return msgs;
    }

    @SuppressWarnings("rawtypes")
    public static String getMapAsString(Map map) {
        String desc = "";

        if (map != null && !map.isEmpty()) {
            for (Object key : map.keySet()) {
            	if(!blackListedKeys.contains(key) && !excludedKeysForResponse.contains(key)){
            		desc += key.toString() + " ==>>  "
            				+ MapUtils.getString(map, key, null) + " :: ";
            	}
            }
        }

        return desc;
    }
    
    public static Map<String, Object> getPaymentRequestLog(Map<String, Object> requestMap){
    	Map<String, Object> returnMap = new HashMap<String, Object>();
    	
    	if(requestMap != null&& !requestMap.isEmpty()){
    		for(String key : requestMap.keySet()){
    			if(!blackListedKeys.contains(key)){
    				returnMap.put(key, requestMap.get(key));
    			}
    		}
    	}
    	return returnMap;
    }

    public static Map<String, String> validateAmount(String amountInRupees) {
        Map<String, String> resultMap = new HashMap<String, String>();

        Boolean valid = true;
        StringBuilder stringBuilder = new StringBuilder();

        if (StringUtils.isBlank(amountInRupees)) {
            valid = false;
            stringBuilder.append("Amount is not Present In Request.");
        } else {
            Integer amountToBeSent = 0;
            Boolean conversionError = false;

            try {
                amountToBeSent = PaymentUtil.amountInPaise(new Double(amountInRupees));
            } catch (Exception e) {
                logger.error("Amount conversion is not correct. ", e);
                valid = false;
                conversionError = true;
                stringBuilder.append("The amount sent " + amountInRupees + " is not a proper format amount.");
            }

            if (!conversionError) {
                if (amountToBeSent <= 0) {
                    valid = false;
                    stringBuilder.append("Amount sent : " + amountInRupees + " , should be greater than 0.");
                }
            }
        }

        resultMap.put(PaymentConstants.PAYMENT_METHOD_RETURN_STATE, valid.toString());
        resultMap.put(PaymentConstants.PAYMENT_METHOD_RETURN_DESCRIPTION, stringBuilder.toString());

        return resultMap;
    }

    public static Integer amountInPaise(Double amountInRupees) {

        Integer amountInPaise;

        if (amountInRupees >= 0d) {
            amountInRupees = getDoubleTillSpecificPlaces(amountInRupees, 2);
            amountInPaise = new Long(Math.round(amountInRupees * 100.0)).intValue();
        } else {
            throw new IllegalArgumentException("Input amount is not in proper format : " + amountInRupees);
        }
        return amountInPaise;
    }

    public static Double getDoubleTillSpecificPlaces(Double doubleValue, int decimalPlaces) {

        if (doubleValue >= 0d) {
            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            decimalFormat.setMinimumIntegerDigits(1);
            decimalFormat.setMaximumFractionDigits(decimalPlaces);
            doubleValue = new Double(decimalFormat.format(doubleValue));
        } else {
            throw new IllegalArgumentException("The input is not in --> "
                    + doubleValue + " format.");
        }

        return doubleValue;
    }

    public static String getHashKey(String inputString) {
        String outputString = null;

        try {
            MessageDigest md5Digest = MessageDigest.getInstance("MD5");

            md5Digest.reset();

            md5Digest.update(inputString.getBytes());

            byte[] encrypted = md5Digest.digest();

            // print out the digest in base64

            BASE64Encoder encoder = new BASE64Encoder();

            String base64 = encoder.encode(encrypted);

            outputString = StringUtils.left(base64, base64.length() - 2);

        } catch (Exception ex) {
            logger.error(ex);
        }

        return outputString;
    }


    public static String createSubmitData(Map<String, String> fieldMap, String encoding) {
        StringBuilder stringBuilder = new StringBuilder();

        String ampersand = "";

        if (fieldMap == null || fieldMap.isEmpty()) {
            return null;
        }

        for (String key : fieldMap.keySet()) {
            String value = fieldMap.get(key);

            try {
                if ((value != null) && (value.length() > 0)) {
                    stringBuilder.append(ampersand);
                    stringBuilder.append(URLEncoder.encode(key, encoding));
                    stringBuilder.append('=');
                    stringBuilder.append(URLEncoder.encode(value, encoding));
                }
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
            ampersand = "&";
        }

        return stringBuilder.toString();
    }


    @SuppressWarnings("resource")
    public static String submitPost(String url, String data,
                                    boolean useProxy, String proxyHost, int proxyPort) throws IOException {

        InputStream is;
        OutputStream os;
        int port = 443;
        String fileName = "";
        boolean useSSL = false;

        // determine if SSL encryption is being used
        if (url.substring(0, 8).equalsIgnoreCase("HTTPS://")) {
            useSSL = true;
            // remove 'HTTPS://' from host URL
            url = url.substring(8);
            // get the filename from the last section of vpc_URL
            fileName = url.substring(url.indexOf("/"));
            // get the IP address of the VPC machine
            url = url.substring(0, url.indexOf('/'));
        }

        // use the next block of code if using a proxy server
        if (useProxy) {
            Socket s = new Socket(proxyHost, proxyPort);
            os = s.getOutputStream();
            is = s.getInputStream();
            // use next block of code if using SSL encryption
            if (useSSL) {
                String msg = "CONNECT " + url + ":" + port + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n\r\n";
                os.write(msg.getBytes());
                byte[] buf = new byte[4096];
                int len = is.read(buf);
                String res = new String(buf, 0, len);

                // check if a successful HTTP connection
                if (res.indexOf("200") < 0) {
                    throw new IOException("Proxy would now allow connection - " + res);
                }

                SSLSocket ssl = (SSLSocket) s_sslSocketFactory.createSocket(s, url, port, true);
                ssl.startHandshake();
                os = ssl.getOutputStream();
                is = ssl.getInputStream();
                // use the next block of code if NOT using SSL encryption
            } else {
                fileName = url;
            }
            // use the next block of code if NOT using a proxy server
        } else {
            // use next block of code if using SSL encryption
            if (useSSL) {
                Socket s = s_sslSocketFactory.createSocket(url, port);
                os = s.getOutputStream();
                is = s.getInputStream();
                // use next block of code if NOT using SSL encryption
            } else {
                Socket s = new Socket(url, port);
                os = s.getOutputStream();
                is = s.getInputStream();
            }
        }

        String req = "POST " + fileName + " HTTP/1.0\r\n"
                + "User-Agent: HTTP Client\r\n"
                + "Content-Type: application/x-www-form-urlencoded\r\n"
                + "Content-Length: " + data.length() + "\r\n\r\n"
                + data;


        os.write(req.getBytes());
        String res = new String(readAll(is));

        // check if a successful connection
        if (res.indexOf("200") < 0) {
            throw new IOException("Connection Refused - " + res);
        }

        if (res.indexOf("404 Not Found") > 0) {
            throw new IOException("File Not Found Error - " + res);
        }

        int resIndex = res.indexOf("\r\n\r\n");
        String body = res.substring(resIndex + 4, res.length());
        return body;
    }


    public static X509TrustManager s_x509TrustManager = null;
    public static SSLSocketFactory s_sslSocketFactory = null;

    static {
        s_x509TrustManager = new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[]{};
            }

            @Override
            public void checkClientTrusted(X509Certificate[] ax509certificate,
                                           String s) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] ax509certificate,
                                           String s) throws CertificateException {
            }
        };

        java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        try {
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{s_x509TrustManager}, null);
            s_sslSocketFactory = context.getSocketFactory();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

    }

    private static byte[] readAll(InputStream is) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];

        while (true) {
            int len = is.read(buf);
            if (len < 0) {
                break;
            }
            baos.write(buf, 0, len);
        }
        return baos.toByteArray();
    }

    public static Map<String, String> createMapFromResponse(String queryString,
                                                            String encoding) {

        Map<String, String> map = new HashMap<String, String>();
        StringTokenizer st = new StringTokenizer(queryString, "&");

        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            int i = token.indexOf('=');
            if (i > 0) {
                try {
                    String key = token.substring(0, i);
                    String value = URLDecoder.decode(token.substring(i + 1,
                            token.length()), encoding);
                    map.put(key, value);
                } catch (Exception ex) {
                }
            }
        }

        return map;
    }

    public static String getPaymentGatewayOrderId(PaymentRequestBusinessDO paymentRequestBusinessDO, int pgRequestCount) {
        //pgRequestCount is the no of payment requests made for this order id
        String pgOrderId = paymentRequestBusinessDO.getOrderId();

        if (pgRequestCount > 0) {
            pgOrderId = pgOrderId + pgRequestCount;
        }

        return pgOrderId;
    }
    
    public static ProductPaymentBusinessDO convertWebDOToBusinessDO(ProductPaymentWebDO baseWebDO) {
        ProductPaymentBusinessDO productPaymentBusinessDO = new ProductPaymentBusinessDO();
        productPaymentBusinessDO.setAffiliateid(baseWebDO.getAffiliateid());
        productPaymentBusinessDO.setUpiInfo(baseWebDO.getUpiInfo());
        productPaymentBusinessDO.setCartId(baseWebDO.getCartId());
        productPaymentBusinessDO.setLookupID(baseWebDO.getLookupID());
        productPaymentBusinessDO.setPaymentoption(baseWebDO.getPaymentoption());
        productPaymentBusinessDO.setPaymenttype(baseWebDO.getPaymenttype());
        productPaymentBusinessDO.setProductId(baseWebDO.getProductId());
        productPaymentBusinessDO.setCouponCode(baseWebDO.getCouponCode());
        productPaymentBusinessDO.setOfferType(baseWebDO.getOfferType());
        productPaymentBusinessDO.setBinOfferId(baseWebDO.getBinOfferId());
        productPaymentBusinessDO.setProductType(baseWebDO.getProductType());
        productPaymentBusinessDO.setCcNumber(baseWebDO.getCcNum());
        productPaymentBusinessDO.setDcNumber(baseWebDO.getDbNum());
        productPaymentBusinessDO.setUsePartialWalletPayment(baseWebDO.isPartialWalletPaymentsSet());
        productPaymentBusinessDO.setUnCheckedStoreCard(Boolean.valueOf(baseWebDO.getUnCheckedStoreCard()));
        productPaymentBusinessDO.setDbCardToken(baseWebDO.getDbCardToken());
        productPaymentBusinessDO.setCardDetailsDO(convertWebDOToCardDetailsWebDO(baseWebDO));
        productPaymentBusinessDO.setPaymentRequestCardDetails(convertWebDOToPaymentRequestCardDetailsDO(baseWebDO));
        productPaymentBusinessDO.setQCPayment(baseWebDO.isQCPayment());
        String transactionTypeStatus=baseWebDO.getTransactionTypeStatus();
        Map<String, String> transactionTypeStatusMap = convertJsonToMap(transactionTypeStatus);
        logger.info("Final map is :"+transactionTypeStatusMap);
        productPaymentBusinessDO.setTransactionTypeStatus(transactionTypeStatusMap);
        return productPaymentBusinessDO;
    }

    public static PaymentRequestCardDetails convertWebDOToPaymentRequestCardDetailsDO(ProductPaymentWebDO baseWebDO) {
        PaymentRequestCardDetails paymentRequestCardDetails = new PaymentRequestCardDetails();
        paymentRequestCardDetails.setCardCvv(baseWebDO.getCardCvv());
        paymentRequestCardDetails.setCardExpMonth(baseWebDO.getCardExpMonth());
        paymentRequestCardDetails.setCardExpYear(baseWebDO.getCardExpYear());
        paymentRequestCardDetails.setCardHolderName(baseWebDO.getCardHolderName());
        paymentRequestCardDetails.setCardNumber(baseWebDO.getCardNumber());
        paymentRequestCardDetails.setCardToken(baseWebDO.getCardToken());
        paymentRequestCardDetails.setCardType(baseWebDO.getCardType());
        paymentRequestCardDetails.setDebitCard(baseWebDO.isDebitCard());
        paymentRequestCardDetails.setSaveCard(baseWebDO.isSaveCard());
        paymentRequestCardDetails.setSaveCardName(baseWebDO.getSaveCardName());
        return paymentRequestCardDetails;
    }
    
    public static CardDetailsWebDO convertWebDOToCardDetailsWebDO(ProductPaymentWebDO baseWebDo) {
        CardDetailsWebDO cardDetailsWebDO = new CardDetailsWebDO();
        cardDetailsWebDO.setCcCardSaveName(baseWebDo.getCcCardSaveName());
        cardDetailsWebDO.setCcCardSaveBool(baseWebDo.getCcCardSaveBool());
        cardDetailsWebDO.setCcCardToken(baseWebDo.getCcCardToken());
        cardDetailsWebDO.setCcCvv(baseWebDo.getCcCvv());
        cardDetailsWebDO.setCcExpMon(baseWebDo.getCcExpMon());
        cardDetailsWebDO.setCcExpYear(baseWebDo.getCcExpYear());
        cardDetailsWebDO.setCcHolderName(baseWebDo.getCcHolderName());
        cardDetailsWebDO.setCcNum(baseWebDo.getCcNum());
        
        cardDetailsWebDO.setDbCardSaveName(baseWebDo.getDbCardSaveName());
        cardDetailsWebDO.setDbCardSaveBool(baseWebDo.getDbCardSaveBool());
        cardDetailsWebDO.setDbCardToken(baseWebDo.getDbCardToken());
        cardDetailsWebDO.setDbCvv(baseWebDo.getDbCvv());
        cardDetailsWebDO.setDbExpMon(baseWebDo.getDbExpMon());
        cardDetailsWebDO.setDbExpYear(baseWebDo.getDbExpYear());
        cardDetailsWebDO.setDbHolderName(baseWebDo.getDbHolderName());
        cardDetailsWebDO.setDbNum(baseWebDo.getDbNum());
        cardDetailsWebDO.setPyop(baseWebDo.getPyop());
        return cardDetailsWebDO;
    }

    private static final String CALL_STATUS = "callStatus";
    private static final String FAILURE = "failure";
    private static final String SUCCESS = "success";
    private static final String ERROR_CODE = "errorCode";
    
    public static Map<Object, Object> errorResponse(ErrorCode errorCode) {
        return ImmutableMap.builder()
        		.put(CALL_STATUS, FAILURE)
        		.put(ERROR_CODE, errorCode.getErrorNumberString()).build();
    }
    
    public static Map<Object, Object> successResponse(Map<String, Object> successResult) {
    	return ImmutableMap.builder()
    		    .put(CALL_STATUS, SUCCESS)
    		    .putAll(successResult)
    		    .build();
        
    }

	public static String serializedResponse(Map<String, ?> payResponseMap) {
		String serializedString = "";

		if (payResponseMap.entrySet().size() > 75) {
			logger.error("Something wrong here, got more than 75 key-value pairs in pay response. Not serializing!. ResponseMap: "
					+ payResponseMap);
			return serializedString;
		}

		for (Entry<String, ?> payEntry : payResponseMap.entrySet()) {
			if(!blackListedKeys.contains(payEntry.getKey()) && !excludedKeysForResponse.contains(payEntry.getKey())){
				serializedString = serializedString + "|" + payEntry.getKey() + ":"
						+ payEntry.getValue();
			}
		}

		return serializedString;
	}
	
    public static boolean isNewPaymentsFlow() {
        FreechargeSession session = FCSessionUtil.getCurrentSession();
        String isNewPaymentsFlow = (String) session.getSessionData().get(PaymentConstants.NEW_PAYMENTS_FLOW);
        if (isNewPaymentsFlow == null) {
            return false;
        }
        return BooleanUtils.toBoolean(isNewPaymentsFlow);
    }
    
    public static boolean isWindowsApp(String channel) {
        if (!FCUtil.isEmpty(channel) && FCConstants.channelMasterMap.containsValue(channel)
                && (FCConstants.channelMasterMap.get(FCConstants.CHANNEL_ID_WINDOWS_APP).equalsIgnoreCase(channel)
                        || FCConstants.channelMasterMap.get(FCConstants.CHANNEL_ID_UWP_APP).equalsIgnoreCase(channel) ) ) {
            return true;
        }
        return false;
    }

    public static boolean isValidPaymentTypeOptionMap(Integer payType, String paymentoption) {
        String paymentType = payType.toString();
        if (paymentType.equals(PaymentConstants.PAYMENT_TYPE_DEBITCARD)) {
            return PaymentConstants.debitCardUIPaymentOptions.contains(paymentoption);
        } else if (paymentType.equals(PaymentConstants.PAYMENT_TYPE_NETBANKING)) {
            return PaymentConstants.netBankingUIPaymentOptions.contains(paymentoption);
        } else if (paymentType.equals(PaymentConstants.PAYMENT_TYPE_CASHCARD)) {
            return PaymentConstants.cashCardUIPaymentOptions.contains(paymentoption);
        } else if (paymentType.equals(PaymentConstants.PAYMENT_TYPE_ATMCARD)) {
            return PaymentConstants.atmCardUIPaymentOptions.contains(paymentoption);
        }
        return true;
    }
    
    public static boolean isDebitCard(String paymentType) {
        return StringUtils.equals("2", paymentType);
    }

    public static boolean isCreditCard(String paymentType) {
        return StringUtils.equals("1", paymentType);
    }
    
    public static boolean isCardPayment(String paymentType) {
        return isCreditCard(paymentType) || isDebitCard(paymentType);
    }

    public static PaymentRequestCardDetails convertRequestMapToPaymentRequestCardDetails(Map<String, String> requestMap) {
        PaymentRequestCardDetails paymentRequestCardDetails = new PaymentRequestCardDetails();
        paymentRequestCardDetails.setCardCvv(requestMap.get(PaymentConstants.CARD_CVV_KEY));
        paymentRequestCardDetails.setCardExpMonth(requestMap.get(PaymentConstants.CARD_EXPMON_KEY));
        paymentRequestCardDetails.setCardExpYear(requestMap.get(PaymentConstants.CARD_EXPYEAR_KEY));
        paymentRequestCardDetails.setCardHolderName(requestMap.get(PaymentConstants.CARD_HOLDER_NAME_KEY));
        paymentRequestCardDetails.setCardNumber(requestMap.get(PaymentConstants.CARD_NUM_KEY));
        paymentRequestCardDetails.setCardToken(requestMap.get(PaymentConstants.CARD_TOKEN));
        paymentRequestCardDetails.setCardType(requestMap.get(RequestParameters.CARD_TYPE));
        paymentRequestCardDetails.setDebitCard(Boolean.valueOf(requestMap.get(PaymentConstants.IS_DEBIT_CARD)));
        paymentRequestCardDetails.setSaveCard(Boolean.valueOf(requestMap.get(PaymentConstants.CARD_SAVE_BOOL)));
        paymentRequestCardDetails.setSaveCardName(requestMap.get(PaymentConstants.CARD_SAVE_NAME));
        return paymentRequestCardDetails;
    }

    public static PaymentRequestCardDetails convertCardDetailsToPaymentRequestCardDetails(CardDetailsWebDO cardWebDO) {
        PaymentRequestCardDetails paymentRequestCardDetails = new PaymentRequestCardDetails();
        if (!FCUtil.isEmpty(cardWebDO.getCcNum())) {
            paymentRequestCardDetails.setDebitCard(false);
            paymentRequestCardDetails.setCardCvv(cardWebDO.getCcCvv());
            paymentRequestCardDetails.setCardExpMonth(cardWebDO.getCcExpMon());
            paymentRequestCardDetails.setCardExpYear(cardWebDO.getCcExpYear());
            paymentRequestCardDetails.setCardHolderName(cardWebDO.getCcHolderName());
            paymentRequestCardDetails.setCardNumber(cardWebDO.getCcNum());
            paymentRequestCardDetails.setCardToken(cardWebDO.getCcCardToken());
            paymentRequestCardDetails.setSaveCard(Boolean.valueOf(cardWebDO.getCcCardSaveBool()));
            paymentRequestCardDetails.setSaveCardName(cardWebDO.getCcCardSaveName());
        } else if (!FCUtil.isEmpty(cardWebDO.getDbNum())) {
            paymentRequestCardDetails.setDebitCard(true);
            paymentRequestCardDetails.setCardCvv(cardWebDO.getDbCvv());
            paymentRequestCardDetails.setCardExpMonth(cardWebDO.getDbExpMon());
            paymentRequestCardDetails.setCardExpYear(cardWebDO.getDbExpYear());
            paymentRequestCardDetails.setCardHolderName(cardWebDO.getDbHolderName());
            paymentRequestCardDetails.setCardNumber(cardWebDO.getDbNum());
            paymentRequestCardDetails.setCardToken(cardWebDO.getDbCardToken());
            paymentRequestCardDetails.setSaveCard(Boolean.valueOf(cardWebDO.getDbCardSaveBool()));
            paymentRequestCardDetails.setSaveCardName(cardWebDO.getDbCardSaveName());
        }
        return paymentRequestCardDetails;
    }
    
    public static CardBin getCardBinDataFromUIResponse(String paymentGatewayCode, Map<String, String> payResponseMap) {
        CardBin cardBin = new CardBin();
        if (PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE.equals(paymentGatewayCode)) {
            cardBin.setCardBin(payResponseMap.get(RequestParameters.PAYU_CARD_NUMBER));
            cardBin.setNameOnCard(payResponseMap.get(RequestParameters.NAME_ON_CARD));
            cardBin.setCardNature(payResponseMap.get(RequestParameters.CARD_NATURE));
            cardBin.setIssuingBank(payResponseMap.get(RequestParameters.CARD_ISSUER_BANK));
            cardBin.setCardType(payResponseMap.get(RequestParameters.CARD_TYPE));
        }
        return cardBin;
    }
    
    public static CardBin getCardBinDataFromS2SResponse(String paymentGatewayCode, Map<String, String> payResponseMap) {
        CardBin cardBin = new CardBin();
        if (PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE.equals(paymentGatewayCode)) {
            cardBin.setCardBin(payResponseMap.get(RequestParameters.PAYU_S2S_CARD_NUMBER));
            cardBin.setNameOnCard("");
            cardBin.setCardNature(payResponseMap.get(RequestParameters.CARD_NATURE));
            cardBin.setIssuingBank("");
            cardBin.setCardType("");
        }
        return cardBin;
    }
    
    /**
     * Given a (debit/credit) card number determine its
     * type (VISA/MASTER/etc.,).
     * To maintain backward compatibility and just to be sane
     * defaults to VISA.
     * @param cardNumber
     * @return
     */
    public static String getCreditCardType(String cardNumber) {
        if(CreditCardValidator.VISA_VALIDATOR.isValid(cardNumber)) {
            return PaymentConstants.VISA_CC;
        } else if(CreditCardValidator.MASTERCARD_VALIDATOR.isValid(cardNumber)) {
            return PaymentConstants.MASTER_CC;
        } else {
            return PaymentConstants.VISA_CC;
        }
    }

    public static Boolean isMaestroCard(String paymentOption) {
    	if (PaymentConstants.MAESTRO_CITI.equals(paymentOption) ||
    			PaymentConstants.MAESTRO_SBI.equals(paymentOption)) {
    		return true;
    	}
    	return false;
    }
    
    public static Boolean isSBIMaestroCard(String paymentOption) {
    	if (PaymentConstants.MAESTRO_SBI.equals(paymentOption)) {
    		return true;
    	}
    	return false;
    }
    
    
    public static String getCardTypeFromCard(String cardNumber, boolean isDebitCard, String paymentOption) {
        String cardType = null;
        if (isDebitCard) {
            if (paymentOption.length() < 2) {
                return null;
            }
            if (paymentOption.toCharArray()[1] == 'V') {
                cardType = "V";
            } else {
                cardType = "M";
            }
        } else if (cardNumber != null && !cardNumber.isEmpty()) {
            String ccCardType = PaymentUtil.getCreditCardType(cardNumber);
            if (ccCardType.equals(PaymentConstants.VISA_CC)) {
                cardType = "V";
            } else {
                cardType = "M";
            }
        }
        return cardType;
    }
    
    public static boolean isStoredCardPayment(ProductPaymentWebDO productPaymentWebDO) {
        if (!FCUtil.isEmpty(productPaymentWebDO.getCcCardToken())
                || !FCUtil.isEmpty(productPaymentWebDO.getDbCardToken())
                || !FCUtil.isEmpty(productPaymentWebDO.getCardToken())) {
            return true;
        }
        return false;
    }
        /*
    
    /*
     * Masking the card number keeping only the card bin head(first 6 digits) and tail(last 4 digits) portions
     */
    public static String getCardNumberInBinFormat(String cardNumber) {
        StringBuffer stringBuffer = new StringBuffer(cardNumber);
        for (int i = 6; i < (cardNumber.length() - 4); i++) {
            stringBuffer.setCharAt(i, 'X');
        }
        return stringBuffer.toString();
    }
    
    /*
     * Method for formatting expiry month to 2 digits. Billdesk has the
     * requirement for the expiry month to always be as 2
     * digits("01","02"..."12")
     */
    public static String getExpMonthInReqFormat(String expMonth) {
        if (expMonth.length() == 1){
            expMonth = "0" + expMonth;
        }
        return expMonth;
    }
    
    public static Map<String, String> getResponseDataWithoutCardData(Map<String, String> responseData){
    	Map<String,String> resultMap = new HashMap<String, String>();
    	for(String key : responseData.keySet()){
    		if(!excludedKeysForResponse.contains(key)){
    				resultMap.put(key, responseData.get(key));
    		}
    	}
    	return resultMap;
    }
    
    public static boolean isWelFormedName(String name) {
        if (name == null) {
            return false;
        }
        return !(CharMatcher.anyOf("[\\<\\>'\"=%\\\\]{1,}").matchesAnyOf(name));
    }

    public static String getPaymentGatewayNameFromUrl(String pgUrl) {
        if(StringUtils.isEmpty(pgUrl)){
            return null;
        }
        String redirectingToPG = null;
        if (pgUrl.equals(FCPropertyPlaceholderConfigurer.getStringValueStatic(PaymentConstants.PAYU_SUBMIT_URL))) {
            redirectingToPG = "payu";
        } else if (pgUrl.contains("ccavenue")) {
            redirectingToPG = "cca";
        } else if(pgUrl.equals(ZeroPaymentController.ZERO_PAY_ACTION)){
            redirectingToPG = "zpg";
        } else if (pgUrl.contains("bdk") || pgUrl.contains("billdesk")) {
            redirectingToPG = "bdk";
        }
        return redirectingToPG;
    }
    
    public static Amount getNewFCWalletAmount(List<PaymentTransaction> paymentTransactions) {
        for (PaymentTransaction paymentTransaction : paymentTransactions) {
            if (paymentTransaction.isNewFCWalletPayment()) {
                return new Amount(paymentTransaction.getAmount());
            }
        }
        return Amount.ZERO;
    }
    
    public static Amount getPGAmount(List<PaymentTransaction> paymentTransactions) {
        for (PaymentTransaction paymentTransaction : paymentTransactions) {
            if (paymentTransaction.isPGPayment()) {
                return new Amount(paymentTransaction.getAmount());
            }
        }
        return Amount.ZERO;
    }

    public static String getRequestParamaterAsMapAsString(HttpServletRequest request) {
        Map<String, String> map = new HashMap<String, String>();
        Enumeration enumeration = request.getParameterNames();

        while (enumeration.hasMoreElements()) {
            String name = (String) enumeration.nextElement();
            String value = request.getParameter(name);
            map.put(name, value);
        }
        return map.toString();
    }

    public static Map<String,String> convertJsonToMap(String stringToParse){

        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> result = new HashMap<>();
        try {
            if(!StringUtils.isEmpty(stringToParse))
            result = mapper.readValue(stringToParse, Map.class);
        } catch (Exception e) {
            logger.error("Exception parsing transaction Status response", e);
        }
        return result;
    }
}