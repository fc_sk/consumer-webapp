package com.freecharge.payment.web.controller;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.addcash.service.PromocodeAddCashService;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.service.exception.CardValidationException;
import com.freecharge.cardstorage.CardStorageKey;
import com.freecharge.cardstorage.FortKnoxCardStorageService;
import com.freecharge.cardstorage.FortknoxCommandBuilder;
import com.freecharge.cardstorage.ICardStorageService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.exception.FCPaymentGatewayException;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.timeout.ExternalClientCallService;
import com.freecharge.common.util.DelegateHelper;
import com.freecharge.common.util.FCCardUtil;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.KlickPayConstants;
import com.freecharge.condition.Result;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.fortknox.Environment;
import com.freecharge.fortknox.FortKnoxClient;
import com.freecharge.fortknox.core.Card;
import com.freecharge.freefund.services.UtmTrackerService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.mobile.service.MobileCheckoutService;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.web.PaymentResponseWebDO;
import com.freecharge.payment.exception.PaymentTransactionNotFoundException;
import com.freecharge.payment.services.CardDataHandler;
import com.freecharge.payment.services.CardGatewayHandler;
import com.freecharge.payment.services.CardStatusHandler;
import com.freecharge.payment.services.IGatewayHandler;
import com.freecharge.payment.services.KlickPayGatewayHandler;
import com.freecharge.payment.services.KlickPayGatewayHandler.ResponseConstants;
import com.freecharge.payment.services.PaymentPortalService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.services.binrange.CardBin;
import com.freecharge.payment.util.CardValidationUtil;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.onecheck.model.DeviceInformation;
import com.freecharge.rest.onecheck.model.FCWalletErrorCode;
import com.freecharge.rest.onecheck.model.InitPaymentRequestDO;
import com.freecharge.rest.onecheck.model.PaymentDetails;
import com.freecharge.rest.onecheck.model.PaymentType;
import com.freecharge.rest.onecheck.model.WalletCreditInitReqDo;
import com.freecharge.rest.onecheck.model.WalletCreditInitResponseDo;
import com.freecharge.rest.onecheck.service.FCWalletService;
import com.freecharge.rest.onecheck.service.OneCheckFCWalletService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.web.interceptor.annotation.NoSession;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.LoginWebDO;
import com.freecharge.web.webdo.UserDO;
import com.google.common.base.Strings;
import com.snapdeal.ims.dto.UserDetailsDTO;
import com.snapdeal.payments.aggregator.api.IPaymentAggregatorService;
import com.snapdeal.payments.aggregator.client.impl.PaymentsAggregatorServiceClient;
import com.snapdeal.payments.aggregator.request.AddCashRequest;
import com.snapdeal.payments.aggregator.request.PGResponseHandlerRequest;
import com.snapdeal.payments.aggregator.response.AddCashResponse;
import com.snapdeal.payments.aggregator.response.PGResponseHandlerResponse;
import com.snapdeal.payments.aggregator.utils.ClientDetails;
import com.snapdeal.payments.sdmoney.service.model.Balance;
import com.snapdeal.payments.sdmoney.service.model.GetUserAccountDetailsResponse;
import com.snapdeal.payments.sdmoney.service.model.type.BalanceType;
import com.snapdeal.payments.sdmoney.service.model.type.KycStatus;

@Controller
@RequestMapping("/payment/*")
public class PaymentPortalController {

    private final Logger               logger                         = LoggingFactory.getLogger(getClass());

    @Autowired
    private FCWalletService            oneCheckWalletService;

    @Autowired
    private OneCheckFCWalletService    walletService;

    @Autowired
    private AppConfigService           appConfig;

    @Autowired
    private PaymentSuccessAction       paymentAction;

    @Autowired
    private FCProperties               fcProperties;

    @Autowired
    private PaymentTransactionService  paymentTransactionService;

    @Autowired
    private MetricsClient              metricsClient;

    @Autowired
    private OrderIdReadDAO             orderIdReadDAO;

    @Autowired
    private MobileCheckoutService      mobileCheckoutService;

    @Autowired
    private KestrelWrapper             kestrelWrapper;

    @Autowired
    private OneCheckWalletService      walletServiceOneCheck;

    @Autowired
    private PromocodeAddCashService    promoCodeAddCashService;

    @Autowired
    @Qualifier("klickPayGatewayHandler")
    private CardDataHandler            klickPayGatewayHandler;

    @Autowired
    @Qualifier("klickPayGatewayHandler")
    private CardGatewayHandler         klickPayCardGatewayHandler;

    
    @Autowired
    @Qualifier("billDeskGatewayHandler")
    private IGatewayHandler            bdGatewayHandler;

    @Autowired
    @Qualifier("card-and-meta")
    ICardStorageService                cardStorageService;

    @Autowired
    @Qualifier("fk-card-storage")
    private FortKnoxCardStorageService fortKnoxCardStorageService;
    
    @Autowired
    private PaymentSuccessAction      paymentSuccessAction;
    
    @Autowired
    @Qualifier("klickPayGatewayHandler")
    private IGatewayHandler kpGatewayHandler;
    
    private IPaymentAggregatorService aggregatorClient;
    
    @Autowired
    private ExternalClientCallService externalClientCallService;
    
    @Autowired
    @Qualifier("klickPayGatewayHandler")
    CardStatusHandler                     cardStatusHandler;

    @Autowired
    private PaymentPortalService paymentPortalService;
    
    @Autowired
    private UtmTrackerService utmTrackerService;
    
    @Autowired
    private FCCardUtil fcCardUtil;

    public static final String         HANDLE_WALLET_RESPONSE_ACTION  = "/payment/handlewalletresponse.htm";

    public static final String         HANDLE_PAYMENT_REQUEST_ACTION  = "/payment/handlerequest.htm";

    public static final String         HANDLE_ZEROPAY_RESPONSE_ACTION = "/payment/handlezeropayresponse.htm";

    public static final String         ONECHECK_MERCHANT_ID           = "3";
    
    public static final String         LOGIN_FLAG                     = "isloggedin";

    public static final String         WALLET_S2S_FLAG                = "s2sEnabled";

    public void populatePGOpenNewWindow(Model model) {
        final boolean pgOpenNewWindow = fcProperties.getBooleanProperty(FCConstants.PG_OPEN_NEW_WINDOW);
        model.addAttribute(FCConstants.MODEL_PG_OPEN_NEW_WINDOW, pgOpenNewWindow);
    }

    /* This is for handling generic response */
    @RequestMapping(value = "handleresponse")
    public String handleResponse(Model model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return null;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "handlefreechargedummyresponse")
    public String handleFreeChargeDummyPaymentResponse(Model model, HttpServletRequest request,
            HttpServletResponse response, @RequestParam Map<String, String> params) throws Exception {
        populatePGOpenNewWindow(model);

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String returnTileDef = PaymentConstants.WAITPAGE_RESPONSE_VIEW;
        logger.info("Response recieved mock gateway for session id " + fs.getUuid());
        String pg = PaymentConstants.PAYMENT_GATEWAY_DUMMY_FREECHARGE_CODE;
        Map<String, String> requestMap = PaymentUtil.getRequestParamaterAsMap(request);
        String delegateName = PaymentConstants.PAYMENT_RESPONSE_DELEGATE_NAME;
        PaymentResponseWebDO paymentResponseWebDO = new PaymentResponseWebDO();
        paymentResponseWebDO.setRequestMap(requestMap);
        paymentResponseWebDO.setPg(pg);

        paymentResponseWebDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_CREDITCARD));

        try {
            DelegateHelper.processRequest(request, response, delegateName, model, paymentResponseWebDO);

            Map<String, Object> dataMap = (Map<String, Object>) paymentResponseWebDO.getResultMap()
                    .get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);

            /*
             * This is *different* from payment success. didMethodCallSucceed
             * just indicates whether the call to delegate succeed without any
             * exceptions.
             */
            boolean didDelegateMethodSucceed = Boolean
                    .valueOf((String) dataMap.get(PaymentConstants.IS_METHOD_SUCCESSFUL));

            if (didDelegateMethodSucceed) {

                String orderId = (String) dataMap.get(PaymentConstants.PAYMENT_PORTAL_ORDER_ID);
                Boolean didPaymentSucceed = Boolean
                        .valueOf((String) dataMap.get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL));
                returnTileDef = (String) dataMap.get(PaymentConstants.KEY_WAITPAGE);

                /*
                 * Indicates if the customer paid for topping up his Balance
                 * (a.k.a. Wallet) as opposed to recharge.
                 */
                Boolean isWalletTopUp = (Boolean) dataMap.get(PaymentConstants.WALLET_TOPUP_ADD);

                model.addAttribute(PaymentConstants.DATA_MAP_FOR_WAITPAGE,
                        paymentResponseWebDO.getResultMap().get(PaymentConstants.DATA_MAP_FOR_WAITPAGE));
                model.addAttribute(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL,
                        paymentResponseWebDO.getResultMap().get(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL));
                model.addAttribute(PaymentConstants.ORDER_NUMBER, orderId);

                if (!isWalletTopUp) {
                    if (didPaymentSucceed) {
                        String mtxnId = (String) dataMap.get(PaymentConstants.MERCHANT_TXN_ID);
                        paymentAction.onPaymentSuccess(orderId, mtxnId);
                    }
                } else {
                    model.addAttribute(PaymentConstants.ORDER_NUMBER, "null");
                }

            } else {
                returnTileDef = PaymentConstants.PAYMENT_ERROR_TILE_DEFINATION;
            }
        } catch (FCRuntimeException fcExp) {
            logger.error("Error in Freecharge Serving Request. Message : ", fcExp);
            throw new Exception(fcExp);
        }
        return returnTileDef;
    }

    public String handlePaymentResponse(Model model, HttpServletRequest request, HttpServletResponse response,
            String paymentGateway) throws Exception {
        populatePGOpenNewWindow(model);

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String returnTileDef = PaymentConstants.WAITPAGE_RESPONSE_VIEW;
        logger.info("Response recieved for " + paymentGateway + " gateway for session id " + fs.getUuid());
        Map<String, String> requestMap = PaymentUtil.getRequestParamaterAsMap(request);
        logger.info("debugmsg: requestMap: "+requestMap);
        String delegateName = PaymentConstants.PAYMENT_RESPONSE_DELEGATE_NAME;
        PaymentResponseWebDO paymentResponseWebDO = new PaymentResponseWebDO();
        paymentResponseWebDO.setRequestMap(requestMap);
        paymentResponseWebDO.setPg(paymentGateway);
        paymentResponseWebDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_CREDITCARD));
        boolean isITTPTxn = Boolean.parseBoolean((String)requestMap.get(PaymentConstants.IS_ITTP_TXN));
        logger.info("debugmsg: isITTPTxn: "+isITTPTxn);
        try {
            DelegateHelper.processRequest(request, response, delegateName, model, paymentResponseWebDO);

            /*
             * Data map from the delegate
             */
            Map<String, Object> paymentDelegateResponseMap = paymentResponseWebDO.getResultMap();

            /*
             * This data map is populated by respective payment gateways.
             */
            @SuppressWarnings("unchecked")
            Map<String, Object> paymentGatewayDataMap = (Map<String, Object>) paymentDelegateResponseMap
                    .get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);

            /*
             * This is *different* from payment success. didMethodCallSucceed
             * just indicates whether the call to delegate succeed without any
             * exceptions.
             */
            boolean didDelegateMethodSucceed = (Boolean) paymentDelegateResponseMap
                    .get(PaymentConstants.IS_METHOD_SUCCESSFUL);

            if (didDelegateMethodSucceed) {

                String orderId = (String) paymentGatewayDataMap.get(PaymentConstants.PAYMENT_PORTAL_ORDER_ID);
                Boolean didPaymentSucceed = Boolean
                        .valueOf((String) paymentGatewayDataMap.get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL));
                Boolean isPaymentAlreadyAcknolodged = Boolean
                        .valueOf((String) paymentGatewayDataMap.get(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED));
                returnTileDef = (String) paymentDelegateResponseMap.get(PaymentConstants.KEY_WAITPAGE);

                /*
                 * Indicates if the customer paid for topping up his Balance
                 * (a.k.a. Wallet) as opposed to recharge.
                 */
                Boolean isWalletTopUp = (Boolean) paymentGatewayDataMap.get(PaymentConstants.WALLET_TOPUP_ADD);

                model.addAttribute(PaymentConstants.DATA_MAP_FOR_WAITPAGE,
                        paymentDelegateResponseMap.get(PaymentConstants.DATA_MAP_FOR_WAITPAGE));
                model.addAttribute(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL,
                        paymentDelegateResponseMap.get(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL));
                model.addAttribute(PaymentConstants.ORDER_NUMBER, orderId);
                model.addAttribute(PaymentConstants.NEW_PAYMENTS_FLOW,
                        (String) fs.getSessionData().get(PaymentConstants.NEW_PAYMENTS_FLOW));
                model.addAttribute(PaymentConstants.NEW_PAYMENTS_PAGE,
                        (String) fs.getSessionData().get(PaymentConstants.NEW_PAYMENTS_PAGE));

                logger.debug("didPaymentSucceed : [" + String.valueOf(didPaymentSucceed)
                        + "]. isPaymentAlreadyAcknolodged : [" + String.valueOf(isPaymentAlreadyAcknolodged) + "]");

                if (!isPaymentAlreadyAcknolodged && didPaymentSucceed) {
                    if (paymentGateway.equals(PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE)) {
                        metricsClient.recordEvent("Payment", PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE,
                                "UIEnqueue");
                    } else if (paymentGateway.equals(PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE)) {
                        metricsClient.recordEvent("Payment", PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE, "UIEnqueue");
                    } else if (paymentGateway.equals(PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE)) {
                        metricsClient.recordEvent("Payment", PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE,
                                "UIEnqueue");
                    }
                }

                if (paymentGateway.equals(PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE)) {
                    CardBin cardBin = PaymentUtil
                            .getCardBinDataFromUIResponse(PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE, requestMap);
                    // Korath : Enqueue to consumers for card bin range table
                    // updation
                    kestrelWrapper.enqueueForCardBinRangeInsert(cardBin, didPaymentSucceed);
                }
                if (!isWalletTopUp) {
                    if (didPaymentSucceed && !isPaymentAlreadyAcknolodged) {
                        String mtxnId = (String) paymentGatewayDataMap.get(PaymentConstants.MERCHANT_TXN_ID);
                        metricsClient.recordEvent("Payment", "UI", "SuccessEnqueue");
                        paymentAction.onPaymentSuccess(orderId, mtxnId);
                    }
                } else {
                    model.addAttribute(PaymentConstants.ORDER_NUMBER, "null");
                }
                String channel = paymentTransactionService.getChannelFromOrderId(orderId);
                
                logger.info("isMobileAppPayment : " + MobileUtil.isMobileAppPayment() + " and " + fs.getSessionData().get(PaymentConstants.IS_MOBILE_APP) +  ", isWindowsApp : " + PaymentUtil.isWindowsApp(channel));
                
                if (MobileUtil.isNewMobilePayment() && !isITTPTxn) {
                    logger.info("Order " + orderId + " is a isNewMobilePayment");
                    Boolean isNode = (Boolean) fs.getSessionData().get(FCConstants.IS_NODE);
                    if (isNode == null || !isNode) {
                        String mobileWebRedirectUrl = "/m/new/#!payments?lid="
                                + orderIdReadDAO.getLookupIdForOrderId(orderId) + "&paycomplete=true&payStatus="
                                + didPaymentSucceed;
                        logger.info("The mobile redirect url for order id " + orderId + " is " + mobileWebRedirectUrl);
                        response.sendRedirect(mobileWebRedirectUrl);
                        // We need not render a jsp page here
                        returnTileDef = null;
                    } else {
                        String mobileWebRedirectUrl = "/mobile/fulfilment?lid="
                                + orderIdReadDAO.getLookupIdForOrderId(orderId) + "&paycomplete=true&payStatus="
                                + didPaymentSucceed;
                        if (fcProperties.isDevMode()) {
                            mobileWebRedirectUrl = "http://localhost:3000/mobile/fulfilment?lid="
                                    + orderIdReadDAO.getLookupIdForOrderId(orderId) + "&paycomplete=true&payStatus="
                                    + didPaymentSucceed;
                        }
                        returnTileDef = "redirect:" + mobileWebRedirectUrl;
                    }
                } else if ((PaymentUtil.isWindowsApp(channel) || MobileUtil.isMobileAppPayment()) && !isITTPTxn) {
                    Boolean isAddCashPayment = mobileCheckoutService.isAddCashPaymentResponse(orderId);
                    String appRedirectUrl = MobileUtil.getMobileAppRedirectUrl(didPaymentSucceed, orderId,
                            isAddCashPayment);
                    logger.info("The app redirect url for order id " + orderId + " is " + appRedirectUrl);
                    response.sendRedirect(appRedirectUrl);
                    // We need not render a jsp page here
                    returnTileDef = null;
                }
            } else {
                returnTileDef = PaymentConstants.PAYMENT_ERROR_TILE_DEFINATION;
            }
        } catch (FCRuntimeException fcExp) {
            logger.error("Error in Freecharge Serving Request. Message : ", fcExp);
            throw new Exception(fcExp);
        }
        if(isITTPTxn){
            logger.debug("debugmsg: setting jsonview: "+returnTileDef);
            logger.info("debugmsg: setting model: "+model.asMap());
            returnTileDef = "jsonView";
        }
        return returnTileDef;
    }

    @RequestMapping(value = "handleccaresposne", method = RequestMethod.POST)
    @Csrf(exclude = true)
    public String handleCCAvenueResponse(Model model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        long startTime = System.currentTimeMillis();
        logPGResponseMetric("cca");
        populatePGOpenNewWindow(model);
        String payResponse = handlePaymentResponse(model, request, response,
                PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE);
        metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.PAYMENT_GATEWAY_CCAVENUE_CODE,
                System.currentTimeMillis() - startTime);
        return payResponse;
    }

    @RequestMapping(value = "handlbdkeresposne", method = RequestMethod.POST)
    @Csrf(exclude = true)
    public String handleBilldeskResponse(Model model, HttpServletRequest request, HttpServletResponse response,
            @RequestParam Map<String, String> params) throws Exception {
        long startTime = System.currentTimeMillis();
        logPGResponseMetric("bdk");
        populatePGOpenNewWindow(model);
        String payResponse = handlePaymentResponse(model, request, response,
                PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE);
        metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE,
                System.currentTimeMillis() - startTime);
        return payResponse;
    }

    @RequestMapping(value = "handleicsresponse", method = RequestMethod.POST)
    @Csrf(exclude = true)
    public String handleICICISSLResponse(Model model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        long startTime = System.currentTimeMillis();
        logPGResponseMetric("ics");
        populatePGOpenNewWindow(model);
        String payResponse = handlePaymentResponse(model, request, response,
                PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE);
        metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.PAYMENT_GATEWAY_ICICI_SSL_CODE,
                System.currentTimeMillis() - startTime);
        return payResponse;
    }

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "handlewalletresponse")
    @Csrf
    public String handleWalletResponse(Model model, HttpServletRequest request, HttpServletResponse response,
            @RequestParam Map<String, String> params) throws Exception {
        long startTime = System.currentTimeMillis();
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map map = fs.getSessionData();
        Boolean isloggedin = false;
        String returnTileDef = PaymentConstants.WAITPAGE_RESPONSE_VIEW;
        if (map != null) {
            if (map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
                isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
            }
        }
        if (isloggedin) {
            logger.info("Response recieved for cc avenue gateway for session id " + fs.getUuid());
            String delegateName = PaymentConstants.PAYMENT_RESPONSE_DELEGATE_NAME;
            PaymentResponseWebDO paymentResponseWebDO = new PaymentResponseWebDO();
            paymentResponseWebDO.setRequestMap(params);
            paymentResponseWebDO.setPg(IGatewayHandler.PaymentGateways.FREECHARGE_WALLET.getName());
            paymentResponseWebDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_WALLET));

            try {
                DelegateHelper.processRequest(request, response, delegateName, model, paymentResponseWebDO);
                if ("true".equalsIgnoreCase(
                        paymentResponseWebDO.getResultMap().get(PaymentConstants.IS_METHOD_SUCCESSFUL).toString())) {
                    model.addAttribute(PaymentConstants.DATA_MAP_FOR_WAITPAGE,
                            paymentResponseWebDO.getResultMap().get(PaymentConstants.DATA_MAP_FOR_WAITPAGE));
                    model.addAttribute(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, paymentResponseWebDO
                            .getResultMap().get(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL));
                    returnTileDef = (String) paymentResponseWebDO.getResultMap().get(PaymentConstants.KEY_WAITPAGE);

                    @SuppressWarnings("unchecked")
                    Map<String, Object> dataMap = (Map<String, Object>) paymentResponseWebDO.getResultMap()
                            .get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);
                    String orderNo = (String) dataMap.get(PaymentConstants.PAYMENT_PORTAL_ORDER_ID);
                    String mtxnId = (String) dataMap.get(PaymentConstants.MERCHANT_TXN_ID);
                    String orderID = paymentAction.onPaymentSuccess(orderNo, mtxnId);
                    model.addAttribute("orderNo", orderID);
                } else {
                    returnTileDef = PaymentConstants.PAYMENT_ERROR_TILE_DEFINATION;
                }
            } catch (FCRuntimeException fcExp) {
                throw new Exception(fcExp);
            }
        } else {
            returnTileDef = PaymentConstants.PAYMENT_ERROR_TILE_DEFINATION;
            model.addAttribute("loginWebDO", new LoginWebDO());
            model.addAttribute("userDO", new UserDO());
        }
        metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE,
                System.currentTimeMillis() - startTime);
        return returnTileDef;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "handlezeropayresponse")
    public String handleZeroPayResponse(Model model, HttpServletRequest request, HttpServletResponse response,
            @RequestParam Map<String, String> params) throws Exception {
        long startTime = System.currentTimeMillis();
        populatePGOpenNewWindow(model);
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        Boolean isloggedin = false;
        String returnTileDef = PaymentConstants.WAITPAGE_RESPONSE_VIEW;
        if (map != null) {
            if (map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
                isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
            }
        }
        if (isloggedin) {
            logger.info("Response recieved for zeropay gateway for session id " + fs.getUuid());
            String delegateName = PaymentConstants.PAYMENT_RESPONSE_DELEGATE_NAME;
            PaymentResponseWebDO paymentResponseWebDO = new PaymentResponseWebDO();
            paymentResponseWebDO.setRequestMap(params);
            paymentResponseWebDO.setPg(IGatewayHandler.PaymentGateways.FREECHARGE_ZEROPAY.getName());
            paymentResponseWebDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_ZEROPAY));

            try {
                DelegateHelper.processRequest(request, response, delegateName, model, paymentResponseWebDO);
                if ("true".equalsIgnoreCase(
                        paymentResponseWebDO.getResultMap().get(PaymentConstants.IS_METHOD_SUCCESSFUL).toString())) {
                    if (null != model) {
                        model.addAttribute(PaymentConstants.DATA_MAP_FOR_WAITPAGE,
                                paymentResponseWebDO.getResultMap().get(PaymentConstants.DATA_MAP_FOR_WAITPAGE));
                        model.addAttribute(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL, paymentResponseWebDO
                                .getResultMap().get(PaymentConstants.PAYMENT_PORTAL_SEND_RESULT_TO_URL));
                    }
                    returnTileDef = (String) paymentResponseWebDO.getResultMap().get(PaymentConstants.KEY_WAITPAGE);
                    Map<String, Object> dataMap = (Map<String, Object>) paymentResponseWebDO.getResultMap()
                            .get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);
                    String orderId = (String) dataMap.get(PaymentConstants.PAYMENT_PORTAL_ORDER_ID);
                    if ((Boolean) dataMap.get(PaymentConstants.WALLET_TOPUP_ADD) == false) {
                        String mtxnId = (String) dataMap.get(PaymentConstants.MERCHANT_TXN_ID);
                        String orderID = paymentAction.onPaymentSuccess(orderId, mtxnId);
                        if (null != model)
                            model.addAttribute("orderNo", orderID);
                    } else {
                        if (null != model)
                            model.addAttribute("orderNo", "null");
                    }
                } else {
                    returnTileDef = PaymentConstants.PAYMENT_ERROR_TILE_DEFINATION;
                }
            } catch (FCRuntimeException fcExp) {
                throw new Exception(fcExp);
            }
        } else {
            returnTileDef = PaymentConstants.PAYMENT_ERROR_TILE_DEFINATION;
            if (null != model) {
                model.addAttribute("loginWebDO", new LoginWebDO());
                model.addAttribute("userDO", new UserDO());
            }
        }
        metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.ZEROPAY_PAYMENT_OPTION,
                System.currentTimeMillis() - startTime);
        return returnTileDef;
    }

    @RequestMapping(value = "handleklickpayresponse", method = RequestMethod.POST)
    @Csrf(exclude = true)
    public String handleKlickpayResponse(Model model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        long startTime = System.currentTimeMillis();
        logPGResponseMetric(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        logger.info("debugmsg: POST request, params: "+PaymentUtil.getRequestParamaterAsMapAsString(request));
        String httpMethod = request.getMethod();
        logger.info("PG Response handling starts now");
        if (HttpMethod.GET.toString().equals(httpMethod)) {
            logger.info("Taking care of spurious GET call to handlePayUResponse.");
            return FCUtil.getRedirectAction("/");
        }

        String payResponse = handlePaymentResponse(model, request, response,
                PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE,
                System.currentTimeMillis() - startTime);
        logger.info("debugmsg: returning view: "+payResponse);
        return payResponse;
    }
    
    @RequestMapping(value = "handleklickpayresponse", method = RequestMethod.GET)
    @Csrf(exclude = true)
    public String handleKlickpayGETResponse(Model model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        long startTime = System.currentTimeMillis();
        logPGResponseMetric(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        logger.info("debugmsg: GET request, params: "+request.getParameterMap());
        String payResponse = handlePaymentResponse(model, request, response,
                PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE,
                System.currentTimeMillis() - startTime);
        return payResponse;
    }
    
    @Csrf(exclude=true)
    @RequestMapping(value = "handlePGResponse", method = RequestMethod.POST)
    public String handlePGResponse(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception{
        logger.info("*******Entering handlePGResponse*****");
        long startTime = System.currentTimeMillis();
        logPGResponseMetric(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        Map<String, String> requestMap = PaymentUtil.getRequestParamaterAsMap(request);
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String referenceId = (String) fs.getSessionData().get(KlickPayGatewayHandler.KP_REFERENCE_ID_KEY);
        requestMap.put(ResponseConstants.PARAM_REFERENCE_ID, referenceId);
        final String submitData = ow.writeValueAsString(requestMap);
        metricsClient.recordEvent("Payment", "KP.S2S.Auth", "Request");
        logger.info("Performing KP S2S Auth call for " + referenceId + "Auth Request: "+submitData);
        Map<String, Object> authResponse = klickPayGatewayHandler.makeWSCall(submitData,
                fcProperties.getProperty(PaymentConstants.KLICKPAY_S2S_AUTH_URL), 1000, 120000, true);
        logger.info("KP Auth response : " + authResponse);
        if (!authResponse.containsKey(ResponseConstants.PARAM_STATUS)
                || !authResponse.get(ResponseConstants.PARAM_STATUS).equals("E000")) {
            metricsClient.recordEvent("Payment", "KP.S2S.Auth", "Fail");
            throw new FCPaymentGatewayException("Error in KP authorization API");
        } else if (!authResponse.isEmpty()) {
            metricsClient.recordEvent("Payment", "KP.S2S.Auth", "Success");
        }
        Map<String, String> payResponseMap = new HashMap<>();
        for (String key : authResponse.keySet()) {
            Object value = authResponse.get(key);
            if (value != null) {
                payResponseMap.put(key, value.toString());
            }
        }
        PGResponseHandlerRequest aggregatorRequest = createRequestForUpdateTransation(payResponseMap);
        logger.info("Aggregator request for handlepgresponse: "+ow.writeValueAsString(aggregatorRequest));
        PGResponseHandlerResponse aggregatorResponse = aggregatorClient.updateTransactionStatus(aggregatorRequest);
        logger.info("Aggregator response for handlepgresponse: "+aggregatorResponse);
        metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE,
                System.currentTimeMillis() - startTime);
        return "redirect:"+aggregatorResponse.getMerchantRedirectUrl();
    }


    @Csrf(exclude=true)
    @RequestMapping(value = "handlePGResponse", method = RequestMethod.GET)
    public String handlePGGETResponse(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception{
        logger.info("*******Entering handlePGResponse***** GET call");
        long startTime = System.currentTimeMillis();
        logPGResponseMetric(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
        Map<String, String> requestMap = PaymentUtil.getRequestParamaterAsMap(request);
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        String referenceId = (String) fs.getSessionData().get(KlickPayGatewayHandler.KP_REFERENCE_ID_KEY);
        requestMap.put(ResponseConstants.PARAM_REFERENCE_ID, referenceId);
        final String submitData = ow.writeValueAsString(requestMap);
        metricsClient.recordEvent("Payment", "KP.S2S.Auth", "Request");
        logger.info("Performing KP S2S Auth call for " + referenceId + "Auth Request: "+submitData);
        Map<String, Object> authResponse = klickPayGatewayHandler.makeWSCall(submitData,
                fcProperties.getProperty(PaymentConstants.KLICKPAY_S2S_AUTH_URL), 1000, 120000, true);
        logger.info("KP Auth response : " + authResponse);
        if (!authResponse.containsKey(ResponseConstants.PARAM_STATUS)
                || !authResponse.get(ResponseConstants.PARAM_STATUS).equals("E000")) {
            metricsClient.recordEvent("Payment", "KP.S2S.Auth", "Fail");
            throw new FCPaymentGatewayException("Error in KP authorization API");
        } else if (!authResponse.isEmpty()) {
            metricsClient.recordEvent("Payment", "KP.S2S.Auth", "Success");
        }
        Map<String, String> payResponseMap = new HashMap<>();
        for (String key : authResponse.keySet()) {
            Object value = authResponse.get(key);
            if (value != null) {
                payResponseMap.put(key, value.toString());
            }
        }
        PGResponseHandlerRequest aggregatorRequest = createRequestForUpdateTransation(payResponseMap);
        logger.info("Aggregator request for handlepgresponse: "+ow.writeValueAsString(aggregatorRequest));
        PGResponseHandlerResponse aggregatorResponse = aggregatorClient.updateTransactionStatus(aggregatorRequest);
        logger.info("Aggregator response for handlepgresponse: "+aggregatorResponse);
        metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE,
                System.currentTimeMillis() - startTime);
        return "redirect:"+aggregatorResponse.getMerchantRedirectUrl();
    }



    @RequestMapping(value = "handlepayuresposne")
    @Csrf(exclude = true)
    public String handlePayUResponse(Model model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        long startTime = System.currentTimeMillis();
        logPGResponseMetric("payu");
        String httpMethod = request.getMethod();
        /*
         * Gracefully handling GET. Redirect to home page.
         */
        if (HttpMethod.GET.toString().equals(httpMethod)) {
            logger.info("Taking care of spurious GET call to handlePayUResponse.");
            return FCUtil.getRedirectAction("/");
        }

        String payResponse = handlePaymentResponse(model, request, response,
                PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE);
        metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.PAYMENT_GATEWAY_PAYU_CODE,
                System.currentTimeMillis() - startTime);
        return payResponse;
    }

    @NoLogin
    @RequestMapping(value = "s2s/bd")
    @Csrf(exclude = true)
    public void handleBDS2SResponse(HttpServletRequest request, HttpServletResponse response,
            @RequestParam Map<String, String> requestMapping) throws Exception {
        try {
            long startTime = System.currentTimeMillis();
            if (requestMapping == null) {
                logger.info("Got NULL post data in handleBDS2SResponse doing nothing");
                return;
            }

            String bdS2SMessage = requestMapping.get(PaymentConstants.BILLDESK_RESPONSE_MESSAGE_KEY);

            logger.info("S2S Message body from BillDesk : " + bdS2SMessage);
            if (StringUtils.isEmpty(bdS2SMessage)) {
                logger.info("BillDesk S2S message is empty, so NOT updating payments.");
                return;
            }

            if (!appConfig.isBillDeskS2SEnabled()) {
                logger.info("BillDesk S2S is not enabled, so NOT updating payments.");
                return;
            }

            logger.info("BD S2S is running in payment process mode so going ahead with payments update");

            Map<String, String> responseForProcessing = new HashMap<>();
            responseForProcessing.put(PaymentConstants.BILLDESK_RESPONSE_MESSAGE_KEY, bdS2SMessage.trim());

            PaymentResponseBusinessDO paymentResponseDO = new PaymentResponseBusinessDO();
            paymentResponseDO.setPg(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
            paymentResponseDO.setRequestMap(responseForProcessing);
            //paymentResponseDO.setPaymentType(new Integer(PaymentConstants.PAYMENT_TYPE_CREDITCARD));
            Map<String, Object> paymentGatewayDataMap = null;
            paymentGatewayDataMap = (Map<String, Object>) kpGatewayHandler.handleS2SPaymentResponse(paymentResponseDO);

            @SuppressWarnings({ "unchecked" })
            Map<String, String> resultMap = (Map<String, String>) paymentGatewayDataMap
                    .get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);

            Boolean didPaymentSucceed = Boolean
                    .valueOf((String) resultMap.get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL));
            Boolean isPaymentAlreadyAcknolodged = Boolean
                    .valueOf((String) resultMap.get(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED));
            String orderId = (String) resultMap.get(PaymentConstants.PAYMENT_PORTAL_ORDER_ID);
            if (didPaymentSucceed && !isPaymentAlreadyAcknolodged) {
                logger.info("[Billdesk S2S Response] Payment success and going ahead with recharge flows ");
                metricsClient.recordEvent("Payment", PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE, "S2SEnqueue");
                String mtxnId = (String) resultMap.get(PaymentConstants.MERCHANT_TXN_ID);
                paymentAction.onPaymentSuccess(orderId, mtxnId);
            }
            metricsClient.recordLatency("Payment",
                    "New.Response." + PaymentConstants.PAYMENT_GATEWAY_BILLDESK_CODE + "_s2s",
                    System.currentTimeMillis() - startTime);
        } catch (Exception e) {
            logger.error("Exception while handling s2s response from billdesk.", e);
        }
    }

    @NoLogin
    @Csrf(exclude = true)
    @NoSession
    @RequestMapping(value = "/fcwallet/v1/addmoney/init", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, String> initWalletCredit(@RequestBody Map<String, String> paramMap, HttpServletRequest request, HttpServletResponse response) {
		List<String> payUExcludedNBList = Arrays.asList("BAHK", "BOBC", "BOBR", "CITA", "HDFC", "ICIC", "DHLB", "LXRB",
				"LXCB", "ORTB", "SHVB", "STCB", "SYNB", "TMEB", "UCOB");
		long startTime = System.currentTimeMillis();
		String originEndPoint = request.getHeader("Origin");
		GetUserAccountDetailsResponse getUserAccountDetailsResponse = null;
		UserDetailsDTO oneCheckUser = null;
		BigDecimal depositeLimit = BigDecimal.ZERO;
		BigDecimal voucherDepositeLimit = BigDecimal.ZERO;
		BigDecimal totalBalance = BigDecimal.ZERO;
		String kycStatus = null;
		Map<String, String> responseMap = new HashMap<>();
		String promoId = paramMap.get("promoId");
		String sessionId = FCSessionUtil.getAppCookie(request);
		try {
			oneCheckUser = getOneCheckUser(request);
			if (oneCheckUser != null) {
				getUserAccountDetailsResponse = walletServiceOneCheck.getUserAccountDetails(oneCheckUser.getUserId());
				logger.info("user EmailId:"+oneCheckUser.getEmailId());
				if (getUserAccountDetailsResponse != null) {
               depositeLimit = getUserAccountDetailsResponse.getBalanceDetails().get(BalanceType.CREDITABLE_BALANCE);
               voucherDepositeLimit = getUserAccountDetailsResponse.getBalanceDetails().get(BalanceType.CREDITABLE_VOUCHER_BALANCE);
					totalBalance = getUserAccountDetailsResponse.getBalanceDetails().get(BalanceType.TOTAL_BALANCE);
					if (getUserAccountDetailsResponse.getKycStatus() != null
							&& !getUserAccountDetailsResponse.getKycStatus().equals(KycStatus.NO_KYC)) {
						kycStatus = "true";
					} else {
						kycStatus = "false";
					}
				}
			}
			paramMap.put("userId", String.valueOf(oneCheckUser.getFcUserId()));
			
			WalletCreditInitReqDo creditInitReqDo = new WalletCreditInitReqDo();

			createCreditInitRequest(paramMap, creditInitReqDo);

			Map<String, String> validationErrorMap = validateWalletCreditRequest(creditInitReqDo, depositeLimit,
					oneCheckUser.getUserId(),voucherDepositeLimit);
			if (validationErrorMap != null) {
				metricsClient.recordEvent("PaymentPortalController", "initWalletCredit", "Failed");
				metricsClient.recordLatency("PaymentPortalController", "initWalletCredit",
						System.currentTimeMillis() - startTime);
				return validationErrorMap;
			}

			DeviceInformation deviceInformation = paymentPortalService.initDeviceInformation(request);
			deviceInformation.setClientId(paramMap.get("clientId"));
			PaymentDetails paymentDetails = paymentPortalService.initPaymentDetails(paramMap, request);

			InitPaymentRequestDO initRequest = new InitPaymentRequestDO();
			initRequest.setReqChannel(creditInitReqDo.getReqChannel());
			initRequest.setSource(creditInitReqDo.getSource());
			initRequest.setTxnAmount(creditInitReqDo.getTxnAmount());
			initRequest.setUserId(paramMap.get("userId"));
			initRequest.setTimeStamp(startTime);
			initRequest.setSessionId(sessionId);
			initRequest.setCustomerName(oneCheckUser.getFirstName());
			initRequest.setPaymentDetails(paymentDetails);
			initRequest.setDeviceInformation(deviceInformation);
			initRequest.setVisitId(creditInitReqDo.getVisitId());
			initRequest.setIsGVLoadMoneyRequest(creditInitReqDo.isGvLoadMoneyRequest());

			if (!Strings.isNullOrEmpty(promoId)) {
				initRequest.setPromoCode(true);
			} else {
				initRequest.setPromoCode(false);
			}

			if (StringUtils.isNotBlank(originEndPoint)) {
				initRequest.setRedirectUrl(originEndPoint + "/app/walletsuccess");
				initRequest.setFailureUrl(originEndPoint + "/app/walletfailure");
			} else {
				initRequest.setRedirectUrl(fcProperties.getAppRootUrl() + "/app/walletsuccess");
				initRequest.setFailureUrl(fcProperties.getAppRootUrl() + "/app/walletfailure");
			}

			WalletCreditInitResponseDo initResponse = walletService.initWalletCreditTransaction(initRequest, oneCheckWalletService.getHeaders(request));

			logger.info("TransactionId:" + initResponse.gettransactionId() + "\nTxnAmount:" + initResponse.getTxnAmount());
			logger.info("MerchantId:" + initResponse.getMerchantId() + "\nStatuscode:" + initResponse.getStatusCode());

			if (initResponse.getStatusCode() != "E00") {
				String errorCode = initResponse.getStatusCode();
				if (errorCode.equalsIgnoreCase(FCWalletErrorCode.E36.toString())) {
					responseMap.putAll(FCWalletErrorCode.E36.toErrorMap());
				} else if (errorCode.equalsIgnoreCase(FCWalletErrorCode.E35.toString())) {
					responseMap.putAll(FCWalletErrorCode.E35.toErrorMap());
				} else {
					responseMap.put(errorCode, "Error-payment is not success!");
				}
			} else {
				responseMap.putAll(FCWalletErrorCode.E00.toErrorMap());
				responseMap.put("merchant_id", initResponse.getMerchantId());
				responseMap.put("merchant_txn_id", initResponse.getTxnId());
				responseMap.put("txn_amount", initResponse.getTxnAmount().toString());
				responseMap.put("callback_url", initResponse.getRedirectUrl());
				responseMap.put("checksum", initResponse.getChecksum());
				responseMap.put("req_channel", "WEB");
				responseMap.put("pg_url", initResponse.getPgUrl() + "/rest/payment/processPay");
				responseMap.put("user_id", initResponse.getUserId());
				responseMap.put("pg", "payu");
				responseMap.put("walletAmount", totalBalance.toString());
				responseMap.put("orderId", initResponse.getTxnId());
				responseMap.put("fcUserId", String.valueOf(oneCheckUser.getFcUserId()));
				responseMap.put("paymentType", creditInitReqDo.getPaymentType());
				responseMap.put("customer_email", oneCheckUser.getEmailId());
				responseMap.put("customer_contact", oneCheckUser.getMobileNumber());

				if (!Strings.isNullOrEmpty(promoId)) {
					promoCodeAddCashService.updateLookupIdForOrderId(promoId, initResponse.getTxnId());
				}

				PaymentType pt = PaymentType.fromCode(creditInitReqDo.getPaymentType());

				String metaData = "disable_pay_options=EMI,CCD";

				switch (pt) {
				case Card:
					responseMap.put("payment_type_code", "CC");
					responseMap.put("card_type", "VI");

					if (StringUtils.isBlank(creditInitReqDo.getCardNumber())) {
						return FCWalletErrorCode.E14.toErrorMap();
					}

					responseMap.put("card_num", creditInitReqDo.getCardNumber());
					responseMap.put("card_cvv", creditInitReqDo.getCardCvv());
					responseMap.put("card_exp_mon", creditInitReqDo.getCardExpiryMonth());
					responseMap.put("card_exp_yr", creditInitReqDo.getCardExpiryYear());
					responseMap.put("save_card", "TRUE");

					metaData += "|is_onecheck_user=true";
					break;

				case SavedCard:
					responseMap.put("payment_type_code", "SC");

					if (StringUtils.isBlank(creditInitReqDo.getSavedCardToken())) {
						return FCWalletErrorCode.E15.toErrorMap();
					}

					responseMap.put("card_token", creditInitReqDo.getSavedCardToken());
					responseMap.put("card_cvv", creditInitReqDo.getCardCvv());
					responseMap.put("card_type", "VI");

					metaData += "|is_onecheck_user=true";

					break;

				case NetBanking:
					responseMap.put("payment_type_code", "NB");

					if (StringUtils.isBlank(creditInitReqDo.getPaymentOption())) {
						return FCWalletErrorCode.E16.toErrorMap();
					}

					if (payUExcludedNBList.contains(creditInitReqDo.getPaymentOption())) {
						// some fessed up special case.
						responseMap.put("pg", "cca");
					}
					responseMap.put("card_type", creditInitReqDo.getPaymentOption());
					break;
				default:
					return FCWalletErrorCode.E29.toErrorMap();
				}
				metaData += "|kyc_user=" + kycStatus;
				responseMap.put("metadata", metaData);
				logger.info("Metadata:"+metaData);
				metricsClient.recordEvent("PaymentPortalController", "initWalletCredit", "Success");
				metricsClient.recordLatency("PaymentPortalController", "initWalletCredit",
						System.currentTimeMillis() - startTime);
			}
		} catch (Exception e) {
			logger.error("Exception during addmoney to wallet", e);
			responseMap.putAll(FCWalletErrorCode.E38.toErrorMap());
		}
		return responseMap;
	}
    
    
    //This API enables S2S flow for load money. The keys required for the switch are being fetched from AppConfig. 
    @Csrf(exclude = true)
    @RequestMapping(value = "/fcwallet/v2/addmoney/init", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object> initWalletCreditS2S(@RequestBody Map<String, String> paramMap, HttpServletRequest request, HttpServletResponse response) {
        JSONObject walletS2SObject = appConfig.getCustomProp(FCConstants.WALLET_S2S_KEY);
        Object walletS2SEnabled = walletS2SObject.get(FCConstants.WALLET_S2S_ENABLED);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        logger.info("walletS2Skey: "+String.valueOf(walletS2SEnabled));
        
        JSONObject androidVersionObject = appConfig.getCustomProp(FCConstants.S2S_ANDROID_VERSION_KEY);
        Object androidVersion = androidVersionObject.get(FCConstants.S2S_ANDROID_VERSION);
                
        //TODO: Create two methods for V1 and V2 and call accordingly
        boolean appVersionAllowed = true;
        String channelId = request.getParameter("fcChannel");
        String fcVersion = request.getParameter("fcversion");
        String fcAppType = request.getParameter("fcAppType");
        String utmDetails = paramMap.get("utmDetails");
        if (StringUtils.isNotEmpty(fcAppType) && "android".equalsIgnoreCase(fcAppType)
                && StringUtils.isNotEmpty(fcVersion)
                && Integer.parseInt(fcVersion) <= Integer.parseInt(String.valueOf(androidVersion))) {
            appVersionAllowed = false;
            logger.info("Version of order: " + fcVersion);
        }

        if (Boolean.parseBoolean(String.valueOf(walletS2SEnabled)) && paymentPortalService.isS2SLoadMoneyAllowed() && appVersionAllowed) {
            logger.info("Going through S2S flow for load money. ");
            try {
                FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
                @SuppressWarnings("rawtypes")
                Map map = fs.getSessionData();
                Boolean isloggedin = false;
                if (map != null) {
                    if ((Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN) != null) {
                        isloggedin = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);
                    }
                }
                //TODO: Need to move this static list 
                List<String> payUExcludedNBList = Arrays.asList("BAHK", "BOBC", "BOBR", "CITA", "HDFC", "ICIC", "DHLB",
                        "LXRB", "LXCB", "ORTB", "SHVB", "STCB", "SYNB", "TMEB", "UCOB");
                long startTime = System.currentTimeMillis();
                String originEndPoint = request.getHeader("Origin");
                GetUserAccountDetailsResponse getUserAccountDetailsResponse = null;
                UserDetailsDTO oneCheckUser = null;
                BigDecimal depositeLimit = BigDecimal.ZERO;
                BigDecimal voucherDepositeLimit = BigDecimal.ZERO;
                String promoId = paramMap.get("promoId");
                ObjectMapper mapper = new ObjectMapper();
                
                oneCheckUser = getOneCheckUser(request);
                if (oneCheckUser != null) {
                    getUserAccountDetailsResponse = walletServiceOneCheck.getUserAccountDetails(oneCheckUser.getUserId());
                    logger.info("user EmailId:" + oneCheckUser.getEmailId());
                    if (getUserAccountDetailsResponse != null) {
                       depositeLimit = getUserAccountDetailsResponse.getBalanceDetails().get(BalanceType.CREDITABLE_BALANCE);
                       voucherDepositeLimit = getUserAccountDetailsResponse.getBalanceDetails().get(BalanceType.CREDITABLE_VOUCHER_BALANCE);
                    }
                }
                paramMap.put("userId", String.valueOf(oneCheckUser.getFcUserId()));
                WalletCreditInitReqDo creditInitReqDo = new WalletCreditInitReqDo();

                createCreditInitRequest(paramMap, creditInitReqDo);

                Map<String, String> validationErrorMap = validateWalletCreditRequest(creditInitReqDo, depositeLimit,
                        oneCheckUser.getUserId(),voucherDepositeLimit);
                if (validationErrorMap != null) {
                    metricsClient.recordEvent("PaymentPortalController", "initWalletCredit", "Failed");
                    metricsClient.recordLatency("PaymentPortalController", "initWalletCredit",
                            System.currentTimeMillis() - startTime);
                    resultMap.putAll(validationErrorMap);
                    return resultMap;
                }
                
                PaymentDetails paymentDetails = paymentPortalService.initPaymentDetails(paramMap, request);

               AddCashRequest addCashRequest = paymentPortalService.createAddCashRequestForAggregator(creditInitReqDo,
                        oneCheckUser, promoId, originEndPoint, paymentDetails);
                logger.info("AddCash request: " + mapper.writeValueAsString(addCashRequest));
                AddCashResponse addCashResponse = aggregatorClient.addCash(addCashRequest);
                //TODO//  for utm
                logger.info("AddCash response: " + mapper.writeValueAsString(addCashResponse));
                utmTrackerService.storeUtmDetailsInDynamo(utmDetails, channelId, addCashResponse.getTxnId());
                
                logger.info("PromoId got == " + promoId + ", for orderId = "+ addCashResponse.getTxnId() + ", and for user = "+ oneCheckUser.getEmailId());
                if (!Strings.isNullOrEmpty(promoId)) {
                    promoCodeAddCashService.updateLookupIdForOrderId(promoId, addCashResponse.getTxnId());
                }

                Map<String, Object> paymentRequestMap = paymentPortalService
                        .createPaymentRequestForKlickpay(creditInitReqDo, addCashResponse, payUExcludedNBList);

                ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                final String submitData = ow.writeValueAsString(paymentRequestMap);
                metricsClient.recordEvent("Payment", "KP.S2S.Init", "Request");
                Map<String, Object> wsResponse = klickPayGatewayHandler.makeWSCall(submitData,
                        fcProperties.getProperty(PaymentConstants.KLICKPAY_S2S_INIT_URL), 10000, 90000, true);
//                logger.info("Kpay Init Response:" + mapper.writeValueAsString(wsResponse));

                String status = (String) wsResponse.get(ResponseConstants.PARAM_STATUS);
                String kpReferenceId = (String) wsResponse.get(ResponseConstants.PARAM_REFERENCE_ID);
                fs.getSessionData().put(KlickPayGatewayHandler.KP_REFERENCE_ID_KEY, kpReferenceId);
                fs.getSessionData().put(KlickPayGatewayHandler.KP_S2S_KEY, Boolean.toString(true));

                //Setting up the result map
                createFinalResultMap(resultMap, isloggedin, creditInitReqDo, wsResponse, status);
//                logger.info("Result being returned to Frontend: " + mapper.writeValueAsString(resultMap));
            } catch (Exception e) {
                logger.error("Exception during addmoney to wallet", e);
                resultMap.putAll(FCWalletErrorCode.E38.toErrorMap());
                return resultMap;
            }
            return resultMap;
        }
        List<String> payUExcludedNBList = Arrays.asList("BAHK", "BOBC", "BOBR", "CITA", "HDFC", "ICIC", "DHLB", "LXRB",
                "LXCB", "ORTB", "SHVB", "STCB", "SYNB", "TMEB", "UCOB");
        long startTime = System.currentTimeMillis();
        String originEndPoint = request.getHeader("Origin");
        GetUserAccountDetailsResponse getUserAccountDetailsResponse = null;
        UserDetailsDTO oneCheckUser = null;
        BigDecimal depositeLimit = BigDecimal.ZERO;
        BigDecimal voucherDepositeLimit = BigDecimal.ZERO;
        BigDecimal totalBalance = BigDecimal.ZERO;
        String kycStatus = null;
        Map<String, String> responseMap = new HashMap<>();
        String promoId = paramMap.get("promoId");
        String sessionId = FCSessionUtil.getAppCookie(request);
        try {
            oneCheckUser = getOneCheckUser(request);
            if (oneCheckUser != null) {
                getUserAccountDetailsResponse = walletServiceOneCheck.getUserAccountDetails(oneCheckUser.getUserId());
                logger.info("user EmailId:"+oneCheckUser.getEmailId());
                if (getUserAccountDetailsResponse != null) {
                    depositeLimit = getUserAccountDetailsResponse.getBalanceDetails().get(BalanceType.CREDITABLE_BALANCE);
                    voucherDepositeLimit = getUserAccountDetailsResponse.getBalanceDetails().get(BalanceType.CREDITABLE_VOUCHER_BALANCE);
                    totalBalance = getUserAccountDetailsResponse.getBalanceDetails().get(BalanceType.TOTAL_BALANCE);
                    if (getUserAccountDetailsResponse.getKycStatus() != null
                            && !getUserAccountDetailsResponse.getKycStatus().equals(KycStatus.NO_KYC)) {
                        kycStatus = "true";
                    } else {
                        kycStatus = "false";
                    }
                }
            }
            paramMap.put("userId", String.valueOf(oneCheckUser.getFcUserId()));
            WalletCreditInitReqDo creditInitReqDo = new WalletCreditInitReqDo();

            createCreditInitRequest(paramMap, creditInitReqDo);

            Map<String, String> validationErrorMap = validateWalletCreditRequest(creditInitReqDo, depositeLimit,
                    oneCheckUser.getUserId(),voucherDepositeLimit);
            if (validationErrorMap != null) {
                metricsClient.recordEvent("PaymentPortalController", "initWalletCredit", "Failed");
                metricsClient.recordLatency("PaymentPortalController", "initWalletCredit",
                        System.currentTimeMillis() - startTime);
                resultMap.putAll(validationErrorMap);
                return resultMap;
            }

            DeviceInformation deviceInformation = paymentPortalService.initDeviceInformation(request);
            deviceInformation.setClientId(paramMap.get("clientId"));
            PaymentDetails paymentDetails = paymentPortalService.initPaymentDetails(paramMap, request);

            InitPaymentRequestDO initRequest = new InitPaymentRequestDO();
            initRequest.setReqChannel(creditInitReqDo.getReqChannel());
            initRequest.setSource(creditInitReqDo.getSource());
            initRequest.setTxnAmount(creditInitReqDo.getTxnAmount());
            initRequest.setUserId(paramMap.get("userId"));
            initRequest.setTimeStamp(startTime);
            initRequest.setSessionId(sessionId);
            initRequest.setCustomerName(oneCheckUser.getFirstName());
            initRequest.setPaymentDetails(paymentDetails);
            initRequest.setDeviceInformation(deviceInformation);
            initRequest.setVisitId(creditInitReqDo.getVisitId());
            initRequest.setIsGVLoadMoneyRequest(creditInitReqDo.isGvLoadMoneyRequest());

            if (!Strings.isNullOrEmpty(promoId)) {
                initRequest.setPromoCode(true);
            } else {
                initRequest.setPromoCode(false);
            }

            if (StringUtils.isNotBlank(originEndPoint)) {
                initRequest.setRedirectUrl(originEndPoint + "/app/walletsuccess");
                initRequest.setFailureUrl(originEndPoint + "/app/walletfailure");
            } else {
                initRequest.setRedirectUrl(fcProperties.getAppRootUrl() + "/app/walletsuccess");
                initRequest.setFailureUrl(fcProperties.getAppRootUrl() + "/app/walletfailure");
            }

            WalletCreditInitResponseDo initResponse = walletService.initWalletCreditTransaction(initRequest, oneCheckWalletService.getHeaders(request));
            //TODO// utm 
            utmTrackerService.storeUtmDetailsInDynamo(utmDetails, channelId, initResponse.getTxnId());
            
            logger.info("TransactionId:" + initResponse.gettransactionId() + "\nTxnAmount:" + initResponse.getTxnAmount());
            logger.info("MerchantId:" + initResponse.getMerchantId() + "\nStatuscode:" + initResponse.getStatusCode());

            if (initResponse.getStatusCode() != "E00") {
                String errorCode = initResponse.getStatusCode();
                if (errorCode.equalsIgnoreCase(FCWalletErrorCode.E36.toString())) {
                    responseMap.putAll(FCWalletErrorCode.E36.toErrorMap());
                } else if (errorCode.equalsIgnoreCase(FCWalletErrorCode.E35.toString())) {
                    responseMap.putAll(FCWalletErrorCode.E35.toErrorMap());
                } else {
                    responseMap.put(errorCode, "Error-payment is not success!");
                }
            } else {
                responseMap.putAll(FCWalletErrorCode.E00.toErrorMap());
                responseMap.put("merchant_id", initResponse.getMerchantId());
                responseMap.put("merchant_txn_id", initResponse.getTxnId());
                responseMap.put("txn_amount", initResponse.getTxnAmount().toString());
                responseMap.put("callback_url", initResponse.getRedirectUrl());
                responseMap.put("checksum", initResponse.getChecksum());
                responseMap.put("req_channel", "WEB");
                responseMap.put("pg_url", initResponse.getPgUrl() + "/rest/payment/processPay");
                responseMap.put("user_id", initResponse.getUserId());
                responseMap.put("pg", "payu");
                responseMap.put("walletAmount", totalBalance.toString());
                responseMap.put("orderId", initResponse.getTxnId());
                responseMap.put("fcUserId", String.valueOf(oneCheckUser.getFcUserId()));
                responseMap.put("paymentType", creditInitReqDo.getPaymentType());
                responseMap.put("customer_email", oneCheckUser.getEmailId());
                responseMap.put("customer_contact", oneCheckUser.getMobileNumber());
                responseMap.put(WALLET_S2S_FLAG, "false");

                logger.info("PromoId got = " + promoId + ", for orderId = "+ initResponse.getTxnId() + ", and for user = "+ oneCheckUser.getEmailId());
                if (!Strings.isNullOrEmpty(promoId)) {
                    promoCodeAddCashService.updateLookupIdForOrderId(promoId, initResponse.getTxnId());
                }

                PaymentType pt = PaymentType.fromCode(creditInitReqDo.getPaymentType());

                String metaData = "disable_pay_options=EMI,CCD";

                switch (pt) {
                case Card:
                    responseMap.put("payment_type_code", "CC");
                    responseMap.put("card_type", "VI");

                    if (StringUtils.isBlank(creditInitReqDo.getCardNumber())) {
                        resultMap.putAll(FCWalletErrorCode.E14.toErrorMap());
                        return resultMap;
                    }

                    responseMap.put("card_num", creditInitReqDo.getCardNumber());
                    responseMap.put("card_cvv", creditInitReqDo.getCardCvv());
                    responseMap.put("card_exp_mon", creditInitReqDo.getCardExpiryMonth());
                    responseMap.put("card_exp_yr", creditInitReqDo.getCardExpiryYear());
                    responseMap.put("save_card", "TRUE");

                    metaData += "|is_onecheck_user=true";
                    break;

                case SavedCard:
                    responseMap.put("payment_type_code", "SC");

                    if (StringUtils.isBlank(creditInitReqDo.getSavedCardToken())) {
                        resultMap.putAll(FCWalletErrorCode.E15.toErrorMap());
                        return resultMap;
                    }

                    responseMap.put("card_token", creditInitReqDo.getSavedCardToken());
                    responseMap.put("card_cvv", creditInitReqDo.getCardCvv());
                    responseMap.put("card_type", "VI");

                    metaData += "|is_onecheck_user=true";

                    break;

                case NetBanking:
                    responseMap.put("payment_type_code", "NB");

                    if (StringUtils.isBlank(creditInitReqDo.getPaymentOption())) {
                        resultMap.putAll(FCWalletErrorCode.E16.toErrorMap());
                        return resultMap;
                    }

                    if (payUExcludedNBList.contains(creditInitReqDo.getPaymentOption())) {
                        // some fessed up special case.
                        responseMap.put("pg", "cca");
                    }
                    responseMap.put("card_type", creditInitReqDo.getPaymentOption());
                    break;
                default:
                    resultMap.putAll(FCWalletErrorCode.E29.toErrorMap());
                    return resultMap;
                }
                metaData += "|kyc_user=" + kycStatus;
                responseMap.put("metadata", metaData);
                logger.info("Metadata:"+metaData);
                metricsClient.recordEvent("PaymentPortalController", "initWalletCredit", "Success");
                metricsClient.recordLatency("PaymentPortalController", "initWalletCredit",
                        System.currentTimeMillis() - startTime);
            }
        } catch (Exception e) {
            logger.error("Exception during addmoney to wallet", e);
            responseMap.putAll(FCWalletErrorCode.E38.toErrorMap());
        }
        resultMap.putAll(responseMap);
        return resultMap;
    }

    private void createFinalResultMap(Map<String, Object> resultMap, Boolean isloggedin,
            WalletCreditInitReqDo creditInitReqDo, Map<String, Object> wsResponse, String status) {
        resultMap.put(LOGIN_FLAG, String.valueOf(isloggedin));
        resultMap.put(WALLET_S2S_FLAG, "true");
        if (!StringUtils.isEmpty(status) && status.equals(KlickPayConstants.SUCCESS_CODE)
                && wsResponse.containsKey(KlickPayConstants.PG_URL)) {
            metricsClient.recordEvent("Payment", "KP.S2S.Init", "Success");
            Map<String, String> resMap = getResponseDataMap(
                    (Map<String, String>) wsResponse.get(KlickPayConstants.REQUEST_DATA),
                    getChannelId(creditInitReqDo.getReqChannel()));
            resultMap.put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, resMap);
            resultMap.putAll(resMap);

            resultMap.put(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, wsResponse.get(KlickPayConstants.PG_URL));
            resultMap.put(KlickPayConstants.PG_URL, wsResponse.get(KlickPayConstants.PG_URL));
            resultMap.put(PaymentConstants.UPI_PAYMENT_FLAG, "false");
        } else {
            metricsClient.recordEvent("Payment", "KP.S2S.Init", "Fail");
            resultMap.put(PaymentConstants.IS_METHOD_SUCCESSFUL, "false");
        }
    }

    private void createCreditInitRequest(Map<String, String> paramMap, WalletCreditInitReqDo creditInitReqDo) {
        if (FCUtil.isValidTxnAmount(paramMap.get("txn_amount"))) {
            creditInitReqDo.setTxnAmount(new Double(paramMap.get("txn_amount")));
        } else {
            creditInitReqDo.setTxnAmount(new Double("0"));
        }
        creditInitReqDo.setReqChannel(paramMap.get("req_channel"));
        creditInitReqDo.setSource(paramMap.get("Source"));
        creditInitReqDo.setPaymentType(paramMap.get("payment_type"));
        creditInitReqDo.setPaymentOption(paramMap.get("payment_option"));
        creditInitReqDo.setCardNumber(paramMap.get("card_num"));
        creditInitReqDo.setNameOnCard(paramMap.get("name_on_card"));
        creditInitReqDo.setCardExpiryMonth(paramMap.get("card_exp_month"));
        creditInitReqDo.setCardExpiryYear(paramMap.get("card_exp_year"));
        creditInitReqDo.setCardCvv(paramMap.get("card_cvv"));
        creditInitReqDo.setSavedCardToken(paramMap.get("saved_card_token"));
        creditInitReqDo.setCardReference(paramMap.get("card_reference"));
        creditInitReqDo.setVisitId(paramMap.get("visitId"));
        
        boolean isGVLoadMoneyRequest = PaymentConstants.PAYMENT_PAYMENT_PURPOSE_GIFT_VOUCHER_LOAD.equals(paramMap.get(PaymentConstants.PAYMENT_REQUEST_PARAM_PAYMENT_PURPOSE));
        creditInitReqDo.setIsGVLoadMoneyRequest(isGVLoadMoneyRequest);
    }

	@NoLogin
	@Csrf(exclude = true)
	@NoSession
	@RequestMapping(value = "/fcwallet/v1/validate/promocode", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Map<String, Object> validatePromocode(@RequestBody Map<String, String> paramMap,
			HttpServletRequest request, HttpServletResponse response) {

        String promoId = paramMap.get("promoId");
        String txnAmount = paramMap.get("txn_amount");
        String cardToken = paramMap.get("saved_card_token");
        String paymentTyp = paramMap.get("payment_type");
        String cardNumber = paramMap.get("card_num");
        String channel = paramMap.get("req_channel");
        
		UserDetailsDTO oneCheckUser = getOneCheckUser(request);
		Balance fcWalletAccountBalance = walletServiceOneCheck.getFCWalletAccountBalance(oneCheckUser.getUserId());
		paramMap.put("userId", String.valueOf(oneCheckUser.getFcUserId()));

		Map<String, Object> responseMap = new HashMap<String, Object>();
		try {
			Double pgTxnAmount = Double.parseDouble(txnAmount);
			int fcUserId = oneCheckUser.getFcUserId();
			int paymentType = Integer.parseInt(paymentTyp);
			responseMap.put("pgAmount", pgTxnAmount);
			responseMap.put("walletAmount", fcWalletAccountBalance.getTotalBalance());
			responseMap.put("cardToken", cardToken);
			responseMap.put("cardNumber", cardNumber);

			Map<String, Object> resultMap = paymentPortalService.getCardInformation(paramMap, request);
			responseMap.put("card_is_in", resultMap.get("card_is_in"));
			responseMap.put("fingerPrint", resultMap.get("fingerPrint"));

			if (!Strings.isNullOrEmpty(cardNumber)) {
				paymentType = 1;
			}
			
			responseMap.put("paymentType", paymentType);
			logger.info("paymentType:" + paymentType+"\tcardReference:"+paramMap.get("card_reference"));
			Result result = promoCodeAddCashService.validatePromocode(promoId, fcUserId, responseMap);
			logger.debug("Result from promoCodeAddcashService:" + result.isSuccess());
			if (!result.isSuccess()) {
				responseMap.put("errorCode", result.getErrorCode());
				responseMap.put("errorMessage", result.getDetailedFailureMessage());
			} else {
				responseMap.put("status", "success");
				responseMap.put("successMessage", "promocode validated successfully");
			}
		} catch (Exception e) {
			logger.error("Exception:", e);
			responseMap.put("error", "exception in API");
			responseMap.put("errorMessage", e.getMessage());
		}
		logger.info("promoId:" + promoId + "\n" + "txnAmount:" + txnAmount + "\n" + "cardToken:" + cardToken + "\n"
                + "paymentType" + paymentTyp + "\n" + "cardNumber" + cardNumber + "\n" + "userId:"
                + paramMap.get("userId") + "\n" + "card_is_in:" + responseMap.get("card_is_in") + "\n" + "fingerPrint"
                + responseMap.get("fingerPrint")+"Channel:"+channel);
		return responseMap;
	}

    private Map<String, String> validateWalletCreditRequest(WalletCreditInitReqDo creditInitReqDo,
            BigDecimal depositeLimit, String userId, BigDecimal voucherDepositeLimit) {
		BigDecimal txnAmount = new BigDecimal(creditInitReqDo.getTxnAmount());
      
      boolean isGVLoadMoneyRequest = creditInitReqDo.isGvLoadMoneyRequest();
      
      if(isGVLoadMoneyRequest){
         int voucherDepositLimitComparisonResult = txnAmount.compareTo(voucherDepositeLimit);
         if(voucherDepositLimitComparisonResult > 0){
            return FCWalletErrorCode.E37.toErrorMap();
         }
      }else{
         int generalDepositeLimitComparisonResult = txnAmount.compareTo(depositeLimit);
         if(generalDepositeLimitComparisonResult > 0){
            return FCWalletErrorCode.E37.toErrorMap();
         }
      }
      
    	if (FCUtil.isVirtualCardBin(creditInitReqDo.getCardNumber())) {
    	 logger.warn("Blocking user trying to add cash using virtual card.");
    	 return FCWalletErrorCode.E33.toErrorMap();
    	 }

        if (FCUtil.isVirtualCardBin(creditInitReqDo.getCardNumber())) {
            logger.warn("Blocking user trying to add cash using virtual card.");
            return FCWalletErrorCode.E33.toErrorMap();
        }
        
        JSONObject ppiSwitches = appConfig.getCustomProp(FCConstants.PPI_SWITCHES);
        Boolean isCreditCardsEnabledForGVLoad = Boolean.valueOf((String)ppiSwitches.get(FCConstants.PPI_GVLOAD_IS_CREDIT_CARD_ALLOWED));

        PaymentType pt = PaymentType.fromCode(creditInitReqDo.getPaymentType());
        switch (pt) {
            case Card:
                if (StringUtils.isBlank(creditInitReqDo.getCardNumber())) {
                    return FCWalletErrorCode.E14.toErrorMap();
                }
                String cardNumber = creditInitReqDo.getCardNumber();
                String cardExpYear = creditInitReqDo.getCardExpiryYear();
                String cardExpMonth = creditInitReqDo.getCardExpiryMonth();
                String cardCVV = creditInitReqDo.getCardCvv();
                // Handle empty values for SBI Maestro Cards
                if (StringUtils.isEmpty(cardExpYear) || cardExpYear.equals("20")) {
                    cardExpYear = "2049";
                    creditInitReqDo.setCardExpiryYear(cardExpYear);
                }
                if (StringUtils.isEmpty(cardExpMonth) || cardExpMonth.equals("-1")) {
                    cardExpMonth = "12";
                    creditInitReqDo.setCardExpiryMonth(cardExpMonth);
                }
                if (StringUtils.isEmpty(cardCVV) || cardCVV.equals("-1")) {
                    cardCVV = "111";
                    creditInitReqDo.setCardCvv(cardCVV);
                }
                try {
                    CardValidationUtil.validateCardDetails(cardNumber, cardExpMonth, cardExpYear, cardCVV);
                    validateCardStatus(cardNumber);
                } catch (CardValidationException e) {
                    Map<String, String> errorMap = FCWalletErrorCode.E34.toErrorMap();
                    errorMap.put("errorMessage", e.getCustomMsg());
                    return errorMap;
                }
                
                // if GVLoadMoneyRequest then check for Credit card
                if(isGVLoadMoneyRequest && !isCreditCardsEnabledForGVLoad){
                   String cardBin = cardNumber.substring(0,6);
                   HashMap<String, Object> klickpayCardDetails = klickPayCardGatewayHandler.getIssuingBankStatus(cardBin);
                   Map<String, String> result = fcCardUtil.checkForNonCreditCards(klickpayCardDetails);
                   if(result!=null){
                      return result;
                   }
                } 
                
                break;

            case SavedCard:
                if (StringUtils.isBlank(creditInitReqDo.getSavedCardToken())) {
                    return FCWalletErrorCode.E15.toErrorMap();
                }
                if (StringUtils.isEmpty(creditInitReqDo.getCardCvv()) || creditInitReqDo.getCardCvv().equals("-1")) {
                    cardCVV = "111";
                    creditInitReqDo.setCardCvv(cardCVV);
                }
                String cardToken = creditInitReqDo.getSavedCardToken();
                if (!StringUtils.isEmpty(cardToken)) {
                    logger.info("Getting card details for stored card payment");
                    Map<String, String> cardDetails = getCardDetails(userId, cardToken);
                    if (cardDetails != null && cardDetails.containsKey(CardStorageKey.CARD_NUMBER)
                            && !StringUtils.isEmpty(cardDetails.get(CardStorageKey.CARD_NUMBER))) {
                        try {
                            logger.info("Performing card data validation");
                            CardValidationUtil.validateCardDetails(cardDetails.get(CardStorageKey.CARD_NUMBER),
                                    cardDetails.get(CardStorageKey.CARD_EXPIRY_MONTH),
                                    cardDetails.get(CardStorageKey.CARD_EXPIRY_YEAR), creditInitReqDo.getCardCvv());
                            validateCardStatus(cardDetails.get(CardStorageKey.CARD_NUMBER));
                        } catch (CardValidationException e) {
                            Map<String, String> errorMap = FCWalletErrorCode.E34.toErrorMap();
                            errorMap.put("errorMessage", e.getCustomMsg());
                            return errorMap;
                        } catch (Exception e) {
                            logger.error("Exception validating card details for stored card", e);
                        }
                        
                        // if GVLoadMoneyRequest then check for Credit card
                        if(isGVLoadMoneyRequest && !isCreditCardsEnabledForGVLoad){
                           cardNumber = cardDetails.get(CardStorageKey.CARD_NUMBER);
                           Map<String, String> result = fcCardUtil.checkForNonCreditCards(cardDetails);
                           if(result!=null){
                              return result;
                           }
                        }      
                    } else {
                        String cvv = creditInitReqDo.getCardCvv();
                        String cvvRegex = "[0-9]{3,4}";
                        if (!cvv.matches(cvvRegex)) {
                            Map<String, String> errorMap = FCWalletErrorCode.E34.toErrorMap();
                            errorMap.put("errorMessage", PaymentConstants.CARD_VALIDATION_INVALID_CARD_CVV);
                            return errorMap;
                        }
                    }
                }
                break;
                
            case NetBanking:
                if (StringUtils.isBlank(creditInitReqDo.getPaymentOption())) {
                    return FCWalletErrorCode.E16.toErrorMap();
                }
                break;
            default:
                return FCWalletErrorCode.E29.toErrorMap();
        }
        return null;
    }


    private void validateCardStatus(String cardNumber) throws CardValidationException {
        Map<String, Object> cardStatus = cardStatusHandler.isCardValid(cardNumber);
        if (cardStatus != null && cardStatus.containsKey("status") && ((String) cardStatus.get("status")).equals("0")) {
            String mssg = (String) cardStatus.get("mssg");
            if (StringUtils.isEmpty(mssg)) {
                return;
            }
            if (mssg.equals("Non-domestic card")) {
                throw new CardValidationException(PaymentConstants.CARD_VALIDATION_NON_DEOMESTIC_CARD);
            } else if (mssg.equals("Hotlisted card")) {
                throw new CardValidationException(PaymentConstants.CARD_VALIDATION_HOTLISTED_CARD);
            }
        }
    }
    
    /**
     * Method logs the PG from which the response is received
     * 
     * @param responseMsg
     */
    private void logPGResponseMetric(String pg) {
        metricsClient.recordEvent("Payment", "PG.Response", pg);
    }
    
    @NoLogin
    @RequestMapping(value = "s2s/kp", method = RequestMethod.POST)
    @Csrf(exclude = true)
    public void handleKPS2SResponse(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.info("***** Entering handleKPS2SResponse *****");
    	try {
            long startTime = System.currentTimeMillis();
            Map<String, String> requestMap = PaymentUtil.getRequestParamaterAsMap(request);
            logger.info("S2S Message body from KlickPay : " + PaymentUtil.getResponseDataWithoutCardData(requestMap));
            if (null == requestMap) {
                logger.info("KlickPay S2S message is empty, so NOT updating payments.");
                return;
            }
            if (!appConfig.isKlickPayS2SEnabled()) {
                logger.info("KlickPay S2S is not enabled, so NOT updating payments.");
                return;
            }

            logger.info("KlickPay S2S is running in payment process mode so going ahead with payments update");
            PaymentResponseBusinessDO paymentResponseDO = new PaymentResponseBusinessDO();
            paymentResponseDO.setPg(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
            paymentResponseDO.setRequestMap(requestMap);
            Map<String, Object> paymentGatewayDataMap = null;
            paymentGatewayDataMap = (Map<String, Object>) kpGatewayHandler.handleS2SPaymentResponse(paymentResponseDO);

            @SuppressWarnings({ "unchecked" })
            Map<String, String> resultMap = (Map<String, String>) paymentGatewayDataMap.get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);

            Boolean didPaymentSucceed = Boolean.valueOf((String) resultMap.get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL));
            Boolean isPaymentAlreadyAcknolodged = Boolean.valueOf((String) resultMap.get(PaymentConstants.PAYMENT_ALREADY_ACKNOLEDGED));
            String orderId = (String) resultMap.get(PaymentConstants.PAYMENT_PORTAL_ORDER_ID);
            if (didPaymentSucceed && !isPaymentAlreadyAcknolodged) {
                metricsClient.recordEvent("Payment", PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE, "S2SEnqueue");
                String mtxnId = (String) resultMap.get(PaymentConstants.MERCHANT_TXN_ID);
                paymentSuccessAction.onPaymentSuccess(orderId, mtxnId);
            }

            if (!isPaymentAlreadyAcknolodged) {
                CardBin cardBin = PaymentUtil.getCardBinDataFromS2SResponse(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE, requestMap);
                kestrelWrapper.enqueueForCardBinRangeInsert(cardBin, didPaymentSucceed);
            }
            metricsClient.recordLatency("Payment", "New.Response." + PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE + "_s2s", System.currentTimeMillis() - startTime);
        } catch (PaymentTransactionNotFoundException e) {
            logger.info("No payment found; aborting S2S processing");
            metricsClient.recordEvent("Payment", PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE, "S2S_PaymentTransactionNotFound");
        } catch (Exception e) {
            metricsClient.recordEvent("Payment", PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE, "S2S_Exception");
            logger.error("Exception while handling callbck from KlickPay", e);
        }
    }
    
    private Map<String, String> getCardDetails(String userId, String cardToken) {
        FortKnoxClient fortKnoxClient = initForKnoxClient(userId);
        logger.info("Requesting card with cardToken=" + cardToken + " userID=" + userId);
        Card card = externalClientCallService
                .executeWithTimeOut(FortknoxCommandBuilder.getGetCardCommand(fortKnoxClient, cardToken, userId));
        Map<String, String> result = new LinkedHashMap<String, String>();
        if (card == null) {
            logger.error("Some error while getting the card details for card token " + cardToken);
            return result;
        }
        logger.info("Card details : " + card.toString());
        result.put(CardStorageKey.CARD_NUMBER, card.getCardNumber());
        result.put(CardStorageKey.CARD_TYPE, card.getCardType());
        result.put(CardStorageKey.CARD_EXPIRY_MONTH, card.getCardExpiryMonth());
        result.put(CardStorageKey.CARD_EXPIRY_YEAR, card.getCardExpiryYear());
        return result;
    }

    private FortKnoxClient initForKnoxClient(String oneCheckUserId) {
        FortKnoxClient client = null;
        Environment fkEnv = Environment.QA;
        if (fcProperties.isProdMode() || fcProperties.isPreProdMode()) {
            fkEnv = Environment.PROD;
        }
        client = new FortKnoxClient(fcProperties.getOneCheckFortknoxAccessKey(),
                fcProperties.getOneCheckFortknoxSecretKey(), fcProperties.getFortknoxConnectTimeout(),
                fcProperties.getFortknoxReadTimeout(), fkEnv);

        return client;
    }
    
    private PGResponseHandlerRequest createRequestForUpdateTransation(Map<String, String> authResponse){
        PGResponseHandlerRequest aggregatorRequest = new PGResponseHandlerRequest();
        aggregatorRequest.setAmount(Double.parseDouble(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_AMOUNT)));
        aggregatorRequest.setCard_bin(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_CARD_BIN));
        aggregatorRequest.setCard_sub_type(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_CARD_SUB_TYPE));
        aggregatorRequest.setCard_type(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_CARD_TYPE));
        aggregatorRequest.setChecksum(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_CHECKSUM));
        aggregatorRequest.setMerchant_txn_id(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_MERCHANT_TXN_ID));
        aggregatorRequest.setMetadata(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_METADATA));
        aggregatorRequest.setPayment_status(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_PAYMENT_STATUS));
        aggregatorRequest.setPg_mssg(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_PG_MSSG));
        aggregatorRequest.setPg_underlier(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_PG_UNDERLIER));
        aggregatorRequest.setPg_used(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_PG_USED));
        aggregatorRequest.setReference_id(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_REFERENCE_ID));
        aggregatorRequest.setStatus(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_STATUS));
        aggregatorRequest.setTransaction_time(authResponse.get(KlickPayGatewayHandler.ResponseConstants.PARAM_TRANSACTION_TIME));
        return aggregatorRequest;
    }
    
    private int getChannelId(String channelName) {
        return StringUtils.isEmpty(channelName) ? -1
                : FCConstants.channelNameMasterMapReverse.get(channelName);

    }
    @SuppressWarnings("deprecation")
    public Map<String, String> getResponseDataMap(Map<String, String> responseMap, int channel) {
        Map<String, String> dataMap = new HashMap<String, String>();
        for (String token : responseMap.keySet()) {
            if (!token.isEmpty()) {
                String value = responseMap.get(token);
                if (token.equals("PaReq") && (channel != FCConstants.CHANNEL_ID_WEB
                        && channel != FCConstants.CHANNEL_ID_MOBILE && channel != FCConstants.CHANNEL_ID_TIZEN_PHONE)) {
                    value = URLEncoder.encode(value);
                }
                if (!token.equals("paramcount")) {
                    dataMap.put(token, value);
                }
            }
        }
        return dataMap;
    }
    
    public UserDetailsDTO getOneCheckUser(HttpServletRequest request){
        String imsToken = FCSessionUtil.getAppCookie(request);
        if (imsToken==null) {
            imsToken = request.getHeader("token");
        }
        return walletServiceOneCheck.getOneCheckUserByToken(imsToken);
    }
    
    @PostConstruct
    public void init() throws Exception{
        ClientDetails.init(fcProperties.getAggregatorIp(), fcProperties.getAggregatorPort(), fcProperties.getAggregatorTimeout(),fcProperties.getAggregatorClientId(), fcProperties.getAggregatorClientKey(),true);
        aggregatorClient = new PaymentsAggregatorServiceClient();
    }
}
