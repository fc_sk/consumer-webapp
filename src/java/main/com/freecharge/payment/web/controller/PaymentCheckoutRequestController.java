package com.freecharge.payment.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.app.service.CheckoutService;
import com.freecharge.app.service.exception.FraudDetectedException;
import com.freecharge.app.service.exception.InvalidPaymentRequestException;
import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCPropertyPlaceholderConfigurer;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCInfraUtils;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.web.webdo.ProductPaymentWebDO;

@Controller
@RequestMapping("/checkout/pay/*")
public class PaymentCheckoutRequestController {
    public static final String METRICS_SERVICE = "Payment";
    public static final String METRIC_PAYMENT = "Attempt";
    public static final String METRIC_REQUEST = "Request";
    public static final String METRIC_REQUEST_START = "start";
    public static final String METRIC_REQUEST_END = "end";

    @Autowired
    private FCPropertyPlaceholderConfigurer fcPropertyPlaceholderConfigurer;
    
    @Autowired
    private CheckoutService checkoutService;
    
    @Autowired
    private MetricsClient metricsClient;
    
    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    @RequestMapping(value = "dopayment", method = RequestMethod.POST)
    public String doPayment(ProductPaymentWebDO productPaymentWebDO, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        long startTime = System.currentTimeMillis();
        metricsClient.recordEvent(METRICS_SERVICE, METRIC_PAYMENT + "." +productPaymentWebDO.getPaymenttype(), "attempt");
        logPaymentRequestStartMetric();
        model.addAttribute("isloggedin", true);
        String hostPrefix = fcPropertyPlaceholderConfigurer.getStringValueStatic("hostprefix");
        hostPrefix = hostPrefix.trim();
        model.addAttribute("hostPrefix", hostPrefix);
         
        if (productPaymentWebDO.getLookupID() == null || productPaymentWebDO.getPaymenttype() == null){
            logger.error("No lid recieved or no payment type recieed. Lid: "+productPaymentWebDO.getLookupID());
            model.addAttribute("code","1");
            logPaymentRequestCompleteMetric();
            return "productpayment/error";
        }
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        fs.getSessionData().put(PaymentConstants.NEW_PAYMENTS_PAGE, request.getParameter(PaymentConstants.NEW_PAYMENTS_PAGE));
    	ProductPaymentBusinessDO productPaymentBusinessDO = PaymentUtil.convertWebDOToBusinessDO(productPaymentWebDO);
    	productPaymentBusinessDO.setAppVersion(null);
    	if(!bindingResult.hasErrors()){
    		try {
    			String remoteIp = FCInfraUtils.getSecureCookie(request);
    	        String iciciSecureCookie = FCInfraUtils.getSecureCookie(request);
    			Map<String, String> requestMap = checkoutService.doPayment(productPaymentWebDO.getParams(), productPaymentBusinessDO, productPaymentWebDO.getLookupID(), remoteIp, iciciSecureCookie, FCConstants.CHANNEL_ID_WEB);
    			if ("true".equalsIgnoreCase(requestMap.get(PaymentConstants.IS_METHOD_SUCCESSFUL))) {
                    model.addAttribute(PaymentConstants.DATA_MAP_FOR_WAITPAGE, requestMap.get(PaymentConstants.DATA_MAP_FOR_WAITPAGE));
                    model.addAttribute(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL, requestMap.get(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL));
                    metricsClient.recordEvent(METRICS_SERVICE, METRIC_PAYMENT, "success");
                    metricsClient.recordLatency("Payment", "Old.Request." + productPaymentBusinessDO.getPaymenttype(),
                            System.currentTimeMillis() - startTime);
                    logPaymentRequestCompleteMetric();
                    return requestMap.get(PaymentConstants.KEY_WAITPAGE);
                } else {
                    metricsClient.recordEvent("Payment", "Attempt", "DelegateFailure");
                    logPaymentRequestCompleteMetric();
                	return PaymentConstants.PAYMENT_ERROR_TILE_DEFINATION;
                }
    		} catch (FraudDetectedException e) {
    		    metricsClient.recordEvent("Payment", "Attempt." + productPaymentBusinessDO.getPaymenttype(), "Fraud");
    		    logger.warn("Fraud detected in Freecharge Serving Request. Message : " + e.getCustomMsg(), e);
    		    logPaymentRequestCompleteMetric();
				return "productpayment/overloaded";
			} catch (InvalidPaymentRequestException e) {
                metricsClient.recordEvent("Payment", "Attempt." + productPaymentBusinessDO.getPaymenttype(), "InvalidRequest");
				model.addAttribute("msgs", e.getErrorMessages());
				logPaymentRequestCompleteMetric();
	            return PaymentConstants.PAYMENT_ERROR_TILE_DEFINATION;
			} catch (FCRuntimeException fcExp) {
			    metricsClient.recordEvent("Payment", "Attempt." + productPaymentBusinessDO.getPaymenttype(), "RunTimeException");
	            logger.error("Error in Freecharge Serving Request. Message : ", fcExp);
	            logPaymentRequestCompleteMetric();
	            throw new Exception(fcExp);
	        }	
    	} else {
    		logPaymentRequestCompleteMetric();
            return PaymentConstants.PAYMENT_ERROR_TILE_DEFINATION;
    	}
    }

	private void logPaymentRequestStartMetric() {
		metricsClient.recordEvent(METRICS_SERVICE, METRIC_REQUEST + "." + FCConstants.CHANNEL_ID_WEB, METRIC_REQUEST_START);
	}

	private void logPaymentRequestCompleteMetric() {
		metricsClient.recordEvent(METRICS_SERVICE, METRIC_REQUEST + "." + FCConstants.CHANNEL_ID_WEB, METRIC_REQUEST_END);
	}
}