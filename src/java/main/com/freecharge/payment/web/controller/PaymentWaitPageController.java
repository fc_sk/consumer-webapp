package com.freecharge.payment.web.controller;

import java.io.BufferedReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.framework.properties.FCPropertyPlaceholderConfigurer;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.payment.util.PaymentConstants;

@Controller
@RequestMapping("/payment/*")
public class PaymentWaitPageController {

	@RequestMapping(value = "pay", method = RequestMethod.POST)
	@Csrf(exclude = true)
	public String redirectToBank(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
			
			if(jb.toString().trim().isEmpty()){
				jb.append(request.getParameter("postData"));
			}
		} catch (Exception e) {

		}
        // Dirty hack to handle ICICI NB redirection
        if (jb.toString().contains("shopping.icicibank.com") && jb.toString().endsWith("bay_mc_login")) {
            jb.append("&BAY_BANKID=ICI\"}");
        }
		JSONObject jsonObject = JSONObject.fromObject(jb.toString());
		model.addAttribute("isloggedin", true);
		String hostPrefix = FCPropertyPlaceholderConfigurer
				.getStringValueStatic("hostprefix");
		hostPrefix = hostPrefix.trim();
		model.addAttribute("hostPrefix", hostPrefix);

		model.addAttribute(PaymentConstants.DATA_MAP_FOR_WAITPAGE,
				jsonObject.get(PaymentConstants.DATA_MAP_FOR_WAITPAGE));
		model.addAttribute(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL,
				jsonObject.get(PaymentConstants.PAYMENT_GATEWAY_SUBMIT_URL));
		return PaymentConstants.WAITPAGE_REQUEST_VIEW;
	}

	@RequestMapping(value = "pay", method = RequestMethod.GET)
	@Csrf(exclude = true)
	public String waitIntermediatePayPage(Model model,
			HttpServletRequest request, HttpServletResponse response) {
		return PaymentConstants.WAITPAGE_VIEW;
	}

}
