package com.freecharge.payment.web.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

//import com.freecharge.common.framework.session.FreechargeContext;
//import com.freecharge.common.framework.session.FreechargeContextDirectory;
//import com.freecharge.admin.controller.FreechargeSession;
import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.app.service.PaymentService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.payment.dos.entity.PaymentResponse;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.CardBinService;
import com.freecharge.payment.services.ITTPHandler;
import com.freecharge.payment.services.PaymentCard;
import com.freecharge.payment.services.PaymentStatusDetails;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.ActivityLogger;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.sns.bean.ReconMetaData;
import com.freecharge.web.util.ViewConstants;
import com.freecharge.web.webdo.ProductPaymentWebDO;

@Controller
public class PaymentRequestController {
    public static final String        METRICS_SERVICE      = "Payment";
    public static final String        METRIC_PAYMENT       = "Attempt";
    public static final String        ERROR_CODE           = "errorCode";
    public static final String        ERROR_MSG            = "errorMessage";
    public static final String        METRIC_ATTEMPT       = "attempt";
    public static final String        METRIC_SUCCESS       = "success";
    public static final String        METRIC_FAILURE       = "failure";
    public static final String        METRIC_REQUEST       = "Request";
    public static final String        METRIC_REQUEST_START = "start";
    public static final String        METRIC_REQUEST_END   = "end";

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private PaymentService            paymentService;

    @Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO;

    @Autowired
    CardBinService                    cardBinService;
    
    @Autowired
	private AppConfigService appConfig;
    
    @Autowired
    @Qualifier("klickPayGatewayHandler")
    private ITTPHandler ittpHandler;

    private final Logger              logger               = LoggingFactory.getLogger(getClass());

    @RequestMapping(value = "/rest/payment/{lookupId}/status/", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Map<String, String> getPaymentStatus(HttpServletRequest request, HttpServletResponse response,
            @PathVariable String lookupId) {
        if (StringUtils.isBlank(lookupId)) {
            return ErrorCode.EMPTY_REQUEST_DATA.restResponse();
        }
        String curUserEmail = null;
    	/*FreechargeSession freechargeSession = FreechargeContextDirectory.get().getFreechargeSession();
   	 	Map<String, Object> map = freechargeSession.getSessionData();
   	 	if (map != null) {
   	 		curUserEmail =	(String) map.get(FCConstants.ADMIN_CUR_USER_EMAIL); 
   	 	}*/
        logger.info(ActivityLogger.PGStatus + "initiated by ");

        try {
            PaymentStatusDetails paymentStatus = paymentTransactionService.isPaymentSuccessful(lookupId);

            final Map<String, String> responseMap = new HashMap<>();
            responseMap.put("callStatus", "success");
            responseMap.put("paymentStatus", paymentStatus.getPaymentStatus().toString());

            if (StringUtils.isNotBlank(paymentStatus.getOrderId())) {
                responseMap.put("orderId", paymentStatus.getOrderId());
            }

            return responseMap;

        } catch (Exception e) {
            logger.error("Exception while retrieving payment status for lookupID: " + String.valueOf(lookupId), e);
            return ErrorCode.INTERNAL_SERVER_ERROR.restResponse();
        }
    }

    @RequestMapping(value = "/rest/payment/{paymentType}", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object> doCompletePayment(ProductPaymentWebDO productPaymentWebDO,
            BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response,
            @PathVariable String paymentType) {
        logger.info("PaymentType : "+paymentType);
        return paymentService.doCompletePayment(productPaymentWebDO, bindingResult, request, response, paymentType);
    }
    
    @RequestMapping(value = "/protected/rest/payment/bin/{orderId}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Map<String, String> getCardBinForOrderId(
            @PathVariable("orderId") String orderId) {
        logger.info("Fetching card bin. For orderId " + orderId);
        String successMtxnId = null;
        Map<String, String> returnMap = new HashMap<String, String>();
        try {
            List<PaymentTransaction> paymentTransactions = paymentTransactionService.getPaymentTransactionsByOrderId(orderId);
            for (PaymentTransaction paymentTransaction : paymentTransactions) {
                if (paymentTransaction.getIsSuccessful()) {
                    successMtxnId = paymentTransaction.getMerchantTxnId();
                    break;
                }
            }
        } catch (Exception e) {
            logger.error("Error occured in getting successMtxnId for orderId " + orderId, e);
            return returnMap;
        }
        logger.info("For orderId " + orderId + " successful mtxnId is " + successMtxnId);
        if (StringUtils.isEmpty(successMtxnId)) {
            return returnMap;
        }
        PaymentCard paymentCard = cardBinService.getPaymentCardForOrderId(successMtxnId);
        if (paymentCard != null) {
            String cardHash = paymentCard.getCardHash();
            String cardBin = paymentCard.getCardBin();
            if (StringUtils.isNotEmpty(cardBin)) {
                returnMap.put(InTxnDataConstants.CARD_BIN, cardBin);
            }
            if (StringUtils.isNotEmpty(cardHash)) {
                returnMap.put(InTxnDataConstants.CARD_HASH, cardHash);
            }
        }
        return returnMap;
    }
    
    @RequestMapping(value = "/rest/payment/resendotp", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object> resendOTP(@RequestBody Map<String, Object> requestParamMap,
            HttpServletRequest request, HttpServletResponse response) {
        return ittpHandler.resendOTP(requestParamMap);
    }
    
    @RequestMapping(value = "/rest/payment/notification", method = RequestMethod.POST)
    @NoLogin
    @Csrf(exclude= true)
    public void paymentNotification(@RequestParam("orderids") MultipartFile file, Model model, HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        int count = 0;
        String messages = "";
        boolean status = false;
        try {
            BufferedReader br = null;
            List<String> successOrderIdList = new ArrayList<String>();
            List<String> failedOrderIdList = new ArrayList<String>();
            try {
                br = new BufferedReader(new InputStreamReader(
                        file.getInputStream()));
                String line = "";
                while ((line = br.readLine()) != null) {
                    try {
                        line = StringUtils.trim(line);
                        if (line.isEmpty())
                            continue;
                        String[] array = line.split("[,]");
                        if (array.length > 3) {
                            String orderId = array[0];
                            logger.info("order id for payment notification " + orderId);
                            String pg = array[1];
                            String categorySubtype = array[2];
                            String notificationType = array[3];
                            logger.info("pg for payment notification " + pg + " categoty subtype " + categorySubtype + " notificationType " + notificationType);
                            if(!StringUtils.isEmpty(orderId)) {
                                List<PaymentTransaction> listOfPaymentTxn = paymentTransactionService.getPaymentTransactionByMerchantOrderId(orderId);
                                if(listOfPaymentTxn!=null && listOfPaymentTxn.size()>0) {
                                    for (PaymentTransaction paymentTransaction : listOfPaymentTxn) {
                                        if(!StringUtils.isEmpty(notificationType) &&  notificationType.equalsIgnoreCase(paymentTransaction.getPaymentGateway())) {
                                        logger.info("payment transaction for order id " + orderId + paymentTransaction.toString());
                                            PaymentResponse paymentResponse = new PaymentResponse();
                                            paymentResponse.setPgMessage("true");
                                            paymentResponse
                                                .setPaymentGateway(PaymentConstants.PAYMENT_GATEWAY_KLICKPAY_CODE);
                                            paymentResponse.setEci(pg);
                                            paymentResponse.setReconMetaData(new ReconMetaData(paymentTransaction
                                                .getMerchantTxnId(), paymentTransaction.getTransactionId(), "NA", categorySubtype,
                                                paymentTransaction.getTransactionId()));
                                            status =
                                                paymentService.sendNotification(paymentTransaction, paymentResponse,
                                                    paymentTransaction.getTransactionId());
                                            logger.info("status for payment notification for order id " + orderId + " status " + status);
                                        }
                                    }
                                }
                            } else {
                                logger.info("order id not correct " + orderId);
                            }
                            if (status) {
                                count++;
                                successOrderIdList.add(orderId);
                            } else {
                                failedOrderIdList.add(orderId);
                            }
                            }
                        } catch (Exception e) {
                            logger.error("Exception ", e);
                    }
                }
            } catch (Exception e) {
                messages += "Something went wrong. Try again later " + ""
                        + e.getMessage();
                throw new Exception("Try again later: " + e.toString());
            } finally {
                if (br != null)
                    br.close();
            }
            messages += "notification done successfully for " + count + " order ids.";
        } catch (Exception e) {
            logger.info("Exception " + messages, e);
            if (!messages.isEmpty())
                messages += "<br/>Try again later " + "" + e.getMessage();
        }

    }
}
