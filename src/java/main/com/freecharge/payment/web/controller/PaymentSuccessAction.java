package com.freecharge.payment.web.controller;

import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.autorefund.PaymentRefund;
import com.freecharge.payment.util.PaymentConstants;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.TxnHomePage;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.OrderService;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.app.service.TxnFulFilmentService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCSessionUtil;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.hcoupon.service.HCouponFulfilmentService;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.wallet.exception.DuplicateRequestException;
import com.freecharge.web.util.WebConstants;

@Component
public class PaymentSuccessAction {
    @Autowired
    private AppConfigService appConfig;

    @Autowired
    private TxnHomePageDAO txnHomePageDAO;
    
    @Autowired
    private TxnFulFilmentService txnFulFilmentService;

    @Autowired
    private RechargeInitiateService rechargeInitiateService;

    @Autowired
    BillPaymentService billPaymentService;
    
    @Autowired
    private KestrelWrapper kestrelWrapper;
    
    @Autowired
    private OrderService orderService;
    
    @Autowired
    private CartService cartService;
    
    @Autowired
    private HCouponFulfilmentService hCouponFulfilmentService;
    
    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private PaymentRefund paymentRefund;

    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    public String onPaymentSuccess(String orderId, String mtxnId) throws DuplicateRequestException {
        String storedMtxnId = paymentTransactionService.getPaymentFulfilmentMtxnFromOrder(orderId);
        if(!FCUtil.isEmpty(storedMtxnId) && !storedMtxnId.equals(mtxnId)) {
            Double refundAmount = paymentRefund.calculateRefundAmountForMtxnId(mtxnId);
            kestrelWrapper.decideAndEnqueueRefund(mtxnId, refundAmount , PaymentConstants.MULTIPLE_PAYMENT_SUCCESS);
            return null;
        }
        insertPaymentFulfilmentOrder(orderId, mtxnId);
        String productType = "";
        boolean nodalFlag = false;
        ProductName productName = null;
        TxnHomePage thp = txnHomePageDAO.findTxnHomePageByOrderID(orderId);
        /*
         * Handle for HCoupons
         */
        if (thp == null ) {
            OrderId orderIdObj = orderService.getByOrderId(orderId);
            productName = ProductName.fromProductId(orderIdObj.getFkProductMasterId());
            productType = productName.getProductType();
        } else {
            productType = thp.getProductType();
        }
        
        productName = ProductName.fromPrimaryProductType(productType);
        if(productName.isUtilityPaymentProduct()) {
    		JSONObject jsonObject = appConfig.getCustomProp(PaymentConstants.NODAL_FLAG_ENABLE_TIME);
    		if(jsonObject != null) {
    			String time = (String) jsonObject.get("time");
    			logger.info("Time Received from AppConfig:" + time);
    			Date date;
				try {
					date = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss").parse(time);
					if((System.currentTimeMillis()) > date.getTime()) {
        				nodalFlag = true;
        			}
				} catch (ParseException e) {
					throw new RuntimeException("Unable to parse Nodal Date" + time);
				}
    		} else throw new RuntimeException("Unable to get property" + PaymentConstants.NODAL_FLAG_ENABLE_TIME);
    	}
        switch (productType) {
        case "V":
        case "C":
        case "D":
        case "F":
        case "X":
            recharge(orderId);
            break;

        case "M":
            initiatePostPaidPayment(orderId);
            break;
            
        case "B":
        case "L":
        case "E":
        case "I":
        case "G":
        case "Z":
        case "Y":
        case "W":
            initiateBillPayFulfillment(orderId, nodalFlag);
            break;
            
        case "T":
            /*
             *  do nothing. this is to handle a quirky case when this thing
             *  gets called from payment callback handlers.
             */
            
            break;
        case "H":
            /*
             * Debit Wallet and just enqueue it to a prod_HCouponAWS for fulfilment
             */

            initiateHyperCopounFulfillment(orderId);
            break;

        default:
            throw new IllegalStateException("Unsupported product type code '" + productType + "'");
        }
        if(FCSessionUtil.getCurrentSession()!=null && FCSessionUtil.getCurrentSession().getSessionData()!=null && thp != null){
    		FCSessionUtil.getCurrentSession().getSessionData().put(WebConstants.LAST_ORDER_AMOUNT,thp.getAmount());
    	}
        return orderId;
    }

    private void insertPaymentFulfilmentOrder(String orderId, String mtxnId) throws DuplicateRequestException {
        paymentTransactionService.insertPaymentFulfilmentMap(orderId, mtxnId);
    }

    private void initiateHyperCopounFulfillment(String orderId) {
    	hCouponFulfilmentService.enqueueForfulfilment(orderId);
    }

    private void recharge(String orderID) {
        rechargeInitiateService.doRechargeWithAsyncConfig(orderID);
    }

    private void initiatePostPaidPayment(String orderID) {
        billPaymentService.initiatePostpaidFulfillment(orderID);
    }
    
    private void initiateBillPayFulfillment(String orderId, boolean nodalFlag) {
        billPaymentService.initiateBillpayFulfillment(orderId, nodalFlag);
    }
}
