package com.freecharge.payment.web.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/testpayment/*")
public class TestPaymentPortalController {


 @RequestMapping(value = "handlerequest")
 public String handleRequest(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
    {

        Map<String,String> requestMap = getRequestParamaterAsMap(request);
        model.addAttribute("actionUrl","/app/recharge.htm");
        model.addAttribute("resultMap",requestMap);
        return "payment/request";
    }


    @RequestMapping(value = "handleresponse")
    public String handleResponse(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception
       {
           String orderId = "FCMW1205180000001";
           String pg = "CCA";
           String transactionId = "1234567890";
           String isSuccessful = "1";
           String doRecharge = "1";
           String amount = "2345";

           String successUrl = request.getParameter("scurl");

           Map<String,String> responseMap = new HashMap<String, String>();
           responseMap.put("orderid",orderId);
           responseMap.put("pg",pg);
           responseMap.put("txnid",transactionId);
           responseMap.put("issuc",isSuccessful);
           responseMap.put("doaction",doRecharge);
           responseMap.put("amount",amount);

           model.addAttribute("actionUrl",successUrl);
           model.addAttribute("resultMap",responseMap);

           return "payment/response"; 
                   




       }

    public static Map<String, String> getRequestParamaterAsMap(HttpServletRequest request) {
        Map<String, String> map = new HashMap<String, String>();
        Enumeration enumeration = request.getParameterNames();

        while (enumeration.hasMoreElements()) {
            String name = (String) enumeration.nextElement();
            String value = request.getParameter(name);

            if (StringUtils.isNotBlank(value)) {
                map.put(name, value);
            }
        }
        return map;
    }

}
