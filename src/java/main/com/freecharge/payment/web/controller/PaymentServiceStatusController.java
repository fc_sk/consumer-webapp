package com.freecharge.payment.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.framework.annotations.NoLogin;

@Controller
@RequestMapping("/rest/payment/*")
public class PaymentServiceStatusController {

	@NoLogin
	@RequestMapping(value = "test/serviceup", method = RequestMethod.GET)
	public void paymentServiceUp(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		
		response.setStatus(HttpServletResponse.SC_OK);
		
	}
}
