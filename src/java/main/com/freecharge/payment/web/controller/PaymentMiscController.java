package com.freecharge.payment.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.util.CustomHttpClientBuilder;
import com.freecharge.api.error.ErrorCode;
import com.freecharge.cardstorage.CardStorageKey;
import com.freecharge.cardstorage.JuspayCardStorageService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.KlickPayGatewayHandler;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;
import com.freecharge.web.interceptor.annotation.NoSession;
import com.freecharge.web.interceptor.annotation.NoSessionWrite;
import com.google.gson.Gson;

@Controller
@RequestMapping("/rest/payment/*")
public class PaymentMiscController {
    private static final int[] UNSUPPORTED_BINS = new int[] { 0, 1, 2, 7, 8, 9 };

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    UserServiceProxy userServiceProxy;

    @Autowired
    private PaymentTransactionService paymentTransactionService;
    @Autowired
    private JuspayCardStorageService  juspayCardStorageService;
    @Autowired
    private FCProperties              fcProperties;

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "user/lastsuccess", produces = "application/json")
    public @ResponseBody Map<Object, Object> lastSuccessPayment() {
        Map<String, Object> respMap = new HashMap<>();
        try {
            Integer userId = userServiceProxy.getLoggedInUser().getUserId();
            if (userId == null) {
                logger.info("Could not fetch a proper user id");
                return PaymentUtil.errorResponse(ErrorCode.EMPTY_REQUEST_DATA);
            }
            PaymentTransaction paymentTransaction = paymentTransactionService
                    .getLastSuccessfulTransactionForUser(userId);
            String paymentOption = null;
            String paymentType = null;
            if (paymentTransaction != null) {
                paymentOption = paymentTransaction.getPaymentOption() == null ? ""
                        : paymentTransaction.getPaymentOption();
                paymentType = paymentTransaction.getPaymentType().toString();
                respMap.put(PaymentConstants.PAYMENT_TYPE, paymentType);
                respMap.put(PaymentConstants.PAYMENT_OPTION, paymentOption);
            }
            /*
             * Special case for debit and credit card to check is there are any
             * stored cards associated with it.
             */
            if (paymentTransaction != null && (paymentType.equals(PaymentConstants.PAYMENT_TYPE_CREDITCARD)
                    || paymentType.equals(PaymentConstants.PAYMENT_TYPE_DEBITCARD))) {
                Map<String, Object> jusPayCards = juspayCardStorageService.list(userId);
                List<Map<String, String>> storedCardList = new ArrayList<>();
                storedCardList = (List<Map<String, String>>) jusPayCards.get("cards");
                List<Map<String, String>> allStoredCards = new ArrayList<>();
                Map<String, String> storedCardData = new HashMap<>();
                if (!FCUtil.isEmpty(storedCardList)) {
                    String cardBrand = null;
                    String cardType = null;
                    for (Map<String, String> storedCard : storedCardList) {
                        cardBrand = storedCard.get(CardStorageKey.CARD_BRAND);
                        cardType = storedCard.get(CardStorageKey.CARD_TYPE);
                        if (PaymentConstants.cardStorageMasterMap.get(paymentOption).equals(cardBrand)
                                && PaymentConstants.cardTypeMasterMap.get(paymentType).equals(cardType)) {
                            storedCardData.put(CardStorageKey.CARD_NUMBER, storedCard.get(CardStorageKey.CARD_NUMBER));
                            storedCardData.put(CardStorageKey.CARD_BRAND, cardBrand);
                            storedCardData.put(CardStorageKey.CARD_TYPE, cardType);
                            storedCardData.put(CardStorageKey.CARD_EXPIRY_MONTH,
                                    storedCard.get(CardStorageKey.CARD_EXPIRY_MONTH));
                            storedCardData.put(CardStorageKey.CARD_EXPIRY_YEAR,
                                    storedCard.get(CardStorageKey.CARD_EXPIRY_YEAR));
                            allStoredCards.add(storedCardData);
                        }
                    }
                }
                respMap.put("storedCards", allStoredCards);
            }
            logger.info("LastSuccessPayment for user id =" + userId + " is = " + respMap);
            if (null == respMap || respMap.isEmpty()) {
                return PaymentUtil.errorResponse(ErrorCode.EMPTY_RESPONSE_DATA);
            }
            return PaymentUtil.successResponse(respMap);
        } catch (Exception e) {
            logger.error("Failed to get the last successful payment.", e);
            return PaymentUtil.errorResponse(ErrorCode.EMPTY_RESPONSE_DATA);
        }
    }

    @RequestMapping(value = "bins", produces = "application/json", method = RequestMethod.GET)
    @NoLogin
    @NoSession
    @NoSessionWrite
    public @ResponseBody String getBins() {
        String response = "";
        try {
            HttpClient client = new HttpClient();
            GetMethod method = new GetMethod(fcProperties.getBinsUrl());
            client.executeMethod(method);
            response = method.getResponseBodyAsString();
        } catch (Exception e) {
            logger.error("Error while getting bins list.", e);
        }
        return response;
    }

    @RequestMapping(value = "v2/bins", produces = "application/json", method = RequestMethod.GET)
    @NoSessionWrite
    public @ResponseBody Map<String, Object> getBinsV2() {
        logger.info("getBinsV2 begins");
        Map<String, Object> response = new HashMap<String, Object>();
        try {
            CloseableHttpClient httpClient = CustomHttpClientBuilder.getCustomHttpClient(fcProperties.getBinsUrl(), 500, 2000);
            HttpGet httpGet = new HttpGet(fcProperties.getBinsUrl());
            CloseableHttpResponse httpResponse=httpClient.execute(httpGet);
            logger.info("Status Code : "+ httpResponse.getStatusLine().getStatusCode());
            response.put("allBins", new Gson().fromJson(EntityUtils.toString(httpResponse.getEntity()), List.class));
            response.put("blockedBinPrefixes", UNSUPPORTED_BINS);
        } catch (Exception e) {
            logger.error("Error while getting bins list.", e);
        }
        logger.info("getBinsV2 ends");
        return response;
    }

}
