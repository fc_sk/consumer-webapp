package com.freecharge.payment.web.delegate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.TxnHomePageDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.PaymentService;
import com.freecharge.app.service.VoucherService;
import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDelegate;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.web.PaymentResponseWebDO;
import com.freecharge.payment.services.IPaymentType;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.services.PaymentTypeFactory;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;

public class PaymentResponseDelegate implements WebDelegate {

    private PaymentTypeFactory paymentTypeFactory;
    private FCProperties properties;

    private static Logger logger = LoggingFactory.getLogger(PaymentResponseDelegate.class);

    @Autowired
    public void setPaymentTypeFactory(PaymentTypeFactory paymentTypeFactory) {
        this.paymentTypeFactory = paymentTypeFactory;
    }

    @Autowired
    public void setProperties(FCProperties properties) {
        this.properties = properties;
    }

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private HomeBusinessDao homeBusinessDao;

    @Autowired
    private FreefundService freeFundService;

    @Autowired
    private CartService cartService;
    
    @Autowired
    private UserTransactionHistoryService transactionHistoryService;
    
    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDAO;
    
    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private PaymentTransactionService paymnetTransactionService;

    @Autowired
    private TxnHomePageDAO txnHomePageDAO;

    public void delegate(WebContext webContext) throws Exception {
        PaymentResponseBusinessDO paymentResponseBusinessDO = (PaymentResponseBusinessDO) webContext.getWebDOMapper().convertWebDOToBusinessDO(webContext.getBaseWebDO(),webContext);

        IPaymentType paymentType = paymentTypeFactory.getPaymentType(paymentResponseBusinessDO.getPaymentType());
        paymentType.handleResponse(paymentResponseBusinessDO);
        paymentService.getUserDetailsByUsingOrderId(paymentResponseBusinessDO);
        BaseWebDO baseWebDO = webContext.getWebDOMapper().convertBusinessDOtoWebDO(paymentResponseBusinessDO,webContext);
        PaymentResponseWebDO paymentResponseWebDO = (PaymentResponseWebDO)baseWebDO;
        //If payment failed :Reset coupon inventory start here
        Map<String, Object> dataMap = (Map<String, Object>) paymentResponseWebDO.getResultMap().get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);
        String orderNo = (String) dataMap.get(PaymentConstants.PAYMENT_PORTAL_ORDER_ID);
        Integer userId = orderIdSlaveDAO.getUserIdFromOrderId(orderNo);
        Boolean ispaymentsucces = Boolean.valueOf((String)dataMap.get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL));
        CartBusinessDo cartToInspect = cartService.getCartByOrderId(orderNo);
        
        ProductName primaryProduct = cartService.getPrimaryProduct(cartToInspect);
        
        if (ProductName.WalletCash == primaryProduct) {
            Float walletCashAmount = null;
            for (CartItemsBusinessDO cartItems: cartToInspect.getCartItems()) {
                if (cartItems.getProductMasterId() == FCConstants.PRODUCT_ID_WALLET_CASH) {
                    walletCashAmount = cartItems.getPrice().floatValue();
                    break;
                }
            }
            
            Map<String, Object> userDetail = new HashMap<>();
            if (walletCashAmount != null) {
                userDetail.put("amount", walletCashAmount);
            }
            userDetail.put("userId", userId);
            userDetail.put("productType", primaryProduct.getProductType());
            // Dummy Data 
            userDetail.put("mobileno", "0000000000");
            userDetail.put("operatorName", "");
            userDetail.put("circleName", "");
            String txnStatus = ispaymentsucces ? "00":"-1";
            transactionHistoryService.createOrUpdateRechargeHistory(orderNo, userDetail,
                    txnStatus);
            dataMap.put(PaymentConstants.WALLET_TOPUP_ADD, true);
        } else {
            dataMap.put(PaymentConstants.WALLET_TOPUP_ADD, false);
        }
        
        dataMap.put(PaymentConstants.MERCHANT_TXN_ID, paymentResponseBusinessDO.getmTxnId());
        
        if (ispaymentsucces) {
            BigDecimal rechargeAmount = txnHomePageDAO.findAmountByOrderID(orderNo);
            if (rechargeAmount != null && !Float.isNaN(rechargeAmount.floatValue()) && !Float.isInfinite(rechargeAmount.floatValue())) {
                dataMap.put(PaymentConstants.AMOUNT, rechargeAmount.floatValue());
            } else {
                logger.info("amount object is null for orderNo: [" + orderNo + "]. Not setting recharge amount");
            }
            paymentResponseWebDO.getResultMap().put(PaymentConstants.DATA_MAP_FOR_WAITPAGE, dataMap);
        }
        webContext.setBaseWebDO(baseWebDO);
        logger.debug("Delegate ends....");
    }

    private String overrideAmount(String amount) {
        String overrideAmount = properties.getProperty(PaymentConstants.KEY_PAYMENT_PORTAL_AMOUNT_OVERRIDE);

        if (StringUtils.isNotBlank(overrideAmount) && !StringUtils.equalsIgnoreCase(overrideAmount, "-1")) {
            amount = overrideAmount;
        }
        return amount;
    }

    private String identifyGateway() {
        return null;
    }
}
