package com.freecharge.payment.web.mapper;

import com.freecharge.common.framework.context.WebContext;
import com.freecharge.common.framework.context.WebDOMapper;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.web.PaymentResponseWebDO;

public class PaymentResponseMapper  implements WebDOMapper<PaymentResponseWebDO, PaymentResponseBusinessDO> {

    public PaymentResponseWebDO convertBusinessDOtoWebDO(PaymentResponseBusinessDO baseBusinessDO, WebContext webContext) {

        ((PaymentResponseWebDO)webContext.getBaseWebDO()).setResultMap(baseBusinessDO.getResultMap());


        return (PaymentResponseWebDO)webContext.getBaseWebDO();  //To change body of implemented methods use File | Settings | File Templates.
    }

    public PaymentResponseBusinessDO convertWebDOToBusinessDO(PaymentResponseWebDO baseWebDO, WebContext webContext) {
        PaymentResponseBusinessDO paymentResponseBusinessDO = new PaymentResponseBusinessDO();
        paymentResponseBusinessDO.setRequestMap(baseWebDO.getRequestMap());
        paymentResponseBusinessDO.setPg(baseWebDO.getPg());
        paymentResponseBusinessDO.setPaymentType(baseWebDO.getPaymentType());
        return paymentResponseBusinessDO;
    }

    public void mergeWebDos(PaymentResponseWebDO oldBaseWebDO, PaymentResponseWebDO newBaseWebDO) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public PaymentResponseWebDO createWebDO(PaymentResponseWebDO baseWebDO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
