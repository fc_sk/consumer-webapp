package com.freecharge.affiliate.order;

import com.mongodb.BasicDBObject;

public class AffiliateOrder extends BasicDBObject{
	/**
	 * Serial version UID for this serializable class.
	 */
	private static final long serialVersionUID = 5631562953581623312L;
	
	private String orderId;
	
	private String affiliateId;
	private String affiliateOrderId;
	
	private BasicDBObject orderData; 
	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
		put("orderId", orderId);
	}
	
	public BasicDBObject getOrderData() {
		return orderData;
	}
	public void setOrderData(BasicDBObject orderData) {
		this.orderData = orderData;
		put("orderData", orderData);
	}
	public String getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(String affiliateId) {
		this.affiliateId = affiliateId;
		put("affiliateId", affiliateId);
	}
	public String getAffiliateOrderId() {
		return affiliateOrderId;
	}
	public void setAffiliateOrderId(String affiliateOrderId) {
		this.affiliateOrderId = affiliateOrderId;
		put("affiliateOrderId", affiliateOrderId);
	}
}
