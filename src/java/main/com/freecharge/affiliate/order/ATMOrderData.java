package com.freecharge.affiliate.order;

import com.mongodb.BasicDBObject;

public class ATMOrderData extends BasicDBObject{
	/**
	 * Serial version UID for this serializable class.
	 */
	private static final long serialVersionUID = -6671407255063932959L;
	private String prnNo;
	private int merchantId;
	private String atmIdentifier;
	public String getPrnNo() {
		return prnNo;
	}
	public void setPrnNo(String prnNo) {
		put("prnNo", prnNo);
		this.prnNo = prnNo;
	}
	public int getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(int merchantId) {
		put("merchantId", merchantId);
		this.merchantId = merchantId;
	}
	public String getAtmIdentifier() {
		return atmIdentifier;
	}
	public void setAtmIdentifier(String atmIdentifier) {
		put("atmIdentifier", atmIdentifier);
		this.atmIdentifier = atmIdentifier;
	}
	
}
