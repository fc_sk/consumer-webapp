package com.freecharge.zeropayment;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import com.freecharge.common.encryption.mdfive.MD5;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.util.PaymentUtil;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 29/12/12
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ZeroPaymentUtil {
	private static Logger zeropayLogger = LoggingFactory.getLogger(LoggingFactory.getZeropayLoggerName(ZeroPaymentUtil.class.getName()));
	
    @Autowired
    private ZeroPaymentService zeroPaymentService;

    public void addZeropaySwitches(Model model) {
        model.addAttribute("isZeropayPayEnabled", zeroPaymentService.isZeropayPayEnabled());
        model.addAttribute("isZeropayDataDisplayEnabled", zeroPaymentService.isZeropayDataDisplayEnabled());
    }

    public static String createZeropayResponseHash(Map<String, String> params) {
        String[] keys = {
                PaymentConstants.MERCHANT_ORDER_ID,
                PaymentConstants.AAMOUNT,
                PaymentConstants.RESULT,
                PaymentConstants.MESSAGE
        };

        String data = "";
        for (String key : keys) {
            if (params.containsKey(key)) {
                data = data + params.get(key);
            }
        }

        return MD5.hash(data);
    }

    public static boolean doesZeroPaymentResponseHashMatch(Map<String, String> params) {
        String hash = params.get(PaymentConstants.HASH);
        return createZeropayResponseHash(params).equals(hash);
    }

    public static String createHash(String merchantOrderId, int userId) {
        String data = merchantOrderId + userId;
        return new String(MD5.hash(data));
    }

    public static boolean doesHashMatch(String merchantOrderId, int userId, String hash) {
        return createHash(merchantOrderId, userId).equals(hash);
    }

    public static Map<String, String> validateAmount(String amountInRupees) {
        Map<String, String> resultMap = new HashMap<String, String>();

        Boolean valid = true;
        StringBuilder stringBuilder = new StringBuilder();

        if (StringUtils.isBlank(amountInRupees)) {
            valid = false;
            stringBuilder.append("Amount is not Present In Request.");
        } else {
            Integer amountToBeSent = 0;
            Boolean conversionError = false;

            try {
                amountToBeSent = PaymentUtil.amountInPaise(new Double(amountInRupees));
            } catch (Exception e) {
            	zeropayLogger.error("Amount conversion is not correct. ", e);
                valid = false;
                conversionError = true;
                stringBuilder.append("The amount sent " + amountInRupees + " is not a proper format amount.");
            }

            if (!conversionError) {
                if (!(amountToBeSent == 0)) {
                    valid = false;
                    stringBuilder.append("Amount sent : " + amountInRupees + " , should be  0.");
                }
            }
        }

        resultMap.put(PaymentConstants.PAYMENT_METHOD_RETURN_STATE, valid.toString());
        resultMap.put(PaymentConstants.PAYMENT_METHOD_RETURN_DESCRIPTION, stringBuilder.toString());

        return resultMap;
    }
    
    public static class Test {
        @org.junit.Test
        public void testZeropayResponseHashMatches() {
            Map<String, String> params = new HashMap<String, String>();
            params.put(PaymentConstants.MERCHANT_ORDER_ID, "fckck88");
            params.put(PaymentConstants.AAMOUNT, String.valueOf(10));
            params.put(PaymentConstants.RESULT, "success");
            params.put(PaymentConstants.MESSAGE, "this is a message");
            params.put(PaymentConstants.HASH, createZeropayResponseHash(params));

            org.junit.Assert.assertEquals(doesZeroPaymentResponseHashMatch(params), true);
        }

        @org.junit.Test
        public void testZeropayResponseHashMisMatch() {
            Map<String, String> params = new HashMap<String, String>();
            params.put(PaymentConstants.MERCHANT_ORDER_ID, "fckck88");
            params.put(PaymentConstants.AAMOUNT, String.valueOf(10));
            params.put(PaymentConstants.RESULT, "success");
            params.put(PaymentConstants.MESSAGE, "this is a message");
            params.put(PaymentConstants.HASH, "some other hash");

            org.junit.Assert.assertEquals(doesZeroPaymentResponseHashMatch(params), false);
        }
    }
}
