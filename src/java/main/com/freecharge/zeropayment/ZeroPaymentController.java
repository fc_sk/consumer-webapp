package com.freecharge.zeropayment;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.service.CartService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.common.util.FCConstants;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.mobile.service.util.CommonUtil;
import com.freecharge.mobile.web.util.ConvenienceFeeUtil;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.common.businessdo.CartBusinessDo;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.GSTService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.payment.dos.business.PaymentResponseBusinessDO;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.IGatewayHandler;
import com.freecharge.payment.services.PaymentPlanService;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.services.PaymentType;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.payment.web.controller.PaymentCheckoutRequestController;
import com.freecharge.payment.web.controller.PaymentPortalController;
import com.freecharge.payment.web.controller.PaymentSuccessAction;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.sns.PaymentSNSService;
import com.freecharge.sns.bean.GSTFee;
import com.freecharge.sns.bean.PaymentAlertBean;
import com.freecharge.sns.bean.PaymentAlertBean.PaymentStatus;
import com.freecharge.sns.bean.PaymentPlan;
import com.freecharge.web.util.WebConstants;
import com.google.common.base.Joiner;

@Controller
@RequestMapping("/app/*")
public class ZeroPaymentController {

    @Autowired
    private ZeroPaymentService zeroPaymentService;

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private MetricsClient metricsClient;
    
    @Autowired
    UserServiceProxy userServiceProxy;
    
    @Autowired
    @Qualifier("zeroPaymentGatewayHandler")
    private IGatewayHandler zeroPaymentGatewayHandler;
    
    @Autowired
    private PaymentSuccessAction paymentSuccessAction;

    @Autowired
    private CampaignServiceClient campaignServiceClient;
    
    @Autowired
    private PaymentPlanService paymentPlanService;

    @Autowired
    private CartService cartService;

    @Autowired
    private OrderIdReadDAO orderIdReadDAO;

    @Autowired
    private PaymentSNSService paymentSNSService;
    
    @Autowired
    private ConvenienceFeeUtil convenienceFeeUtil;
    
    @Autowired
    private GSTService gstService;
    
    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;
    
    private Logger logger = LoggingFactory.getLogger(getClass());

    private Logger zeropayLogger = LoggingFactory.getLogger(LoggingFactory.getZeropayLoggerName(ZeroPaymentController.class.getName()));

    public static final String ZERO_PAY_ACTION = "/app/zeropay.htm";

    @RequestMapping(value = "zeropay")
    public String processPaymentRequest(@RequestParam Map<String, String> requestParams, Model model) throws UnsupportedEncodingException {
        logger.debug("Starting processing of zeropay");
        metricsClient.recordEvent(PaymentCheckoutRequestController.METRICS_SERVICE, PaymentCheckoutRequestController.METRIC_PAYMENT, "zAttempt");

        boolean success = false;

        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map<String, Object> map = fs.getSessionData();
        Object userIdObject =  map.get(WebConstants.SESSION_USER_USERID);
        
        if (userIdObject == null && null != model) {
            model.addAttribute("status", "error");
            model.addAttribute("error", "User not logged in");
            return FCUtil.getRedirectAction("/");
        }

        Map<String, String> responseParams = new HashMap<String, String>();

        String merchantOrderId = "";
     
        try {
            if (requestParams.containsKey(PaymentConstants.HASH)) {
                merchantOrderId = requestParams.get(PaymentConstants.MERCHANT_ORDER_ID);
                int userId = Integer.parseInt(requestParams.get(PaymentConstants.USER_ID));
                String hash = requestParams.get(PaymentConstants.HASH);
            
                if (ZeroPaymentUtil.doesHashMatch(merchantOrderId, userId, hash)) {
                    if (userId != (Integer)userIdObject) {
                        responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                        responseParams.put(PaymentConstants.MESSAGE, "user id passed is not the same as the logged in user");
                    } else {
                        long transactionId = zeroPaymentService.pay(merchantOrderId, ((Integer) userIdObject));
                        logger.info("zeropay_txn ID " + transactionId + "merchantOrderId" + merchantOrderId);
                        zeropayLogger.info("zeropay_txn ID " + transactionId + "merchantOrderId" + merchantOrderId);
                        responseParams.put(PaymentConstants.TRANSACTION_ID, String.valueOf(transactionId));
                        //todo - fix this, there must be a better way to do this
                        double transactionAmount = getTransactionAmount(merchantOrderId);
                        responseParams.put(PaymentConstants.AAMOUNT, String.valueOf(transactionAmount));
                        responseParams.put(PaymentConstants.RESULT, String.valueOf(true));
                        fs.getSessionData().put(PaymentConstants.RESULT, String.valueOf(true));
                        fs.getSessionData().put(PaymentConstants.ORDER_ID, merchantOrderId);
                        success = true;
                    }
                } else {
                    responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                    responseParams.put(PaymentConstants.MESSAGE, "hash mismatch");
                }
            } else {
                responseParams.put(PaymentConstants.RESULT, String.valueOf(false));
                responseParams.put(PaymentConstants.MESSAGE, "hash key not found");
            }
        } catch (Exception e) {
            logger.error(e);
            zeropayLogger.error(e);
            return "home/error";
        }

        responseParams.put(PaymentConstants.MERCHANT_ORDER_ID, merchantOrderId);
        String hash = ZeroPaymentUtil.createZeropayResponseHash(responseParams);

        responseParams.put(PaymentConstants.HASH, hash);

        String params = Joiner.on("&").withKeyValueSeparator("=").join(responseParams);

        final String url = PaymentPortalController.HANDLE_ZEROPAY_RESPONSE_ACTION + "?" + params;

		if (success) {
			metricsClient.recordEvent(PaymentCheckoutRequestController.METRICS_SERVICE,
					PaymentCheckoutRequestController.METRIC_PAYMENT, "zSuccess");
        } else {
            metricsClient.recordEvent(PaymentCheckoutRequestController.METRICS_SERVICE, PaymentCheckoutRequestController.METRIC_PAYMENT, "zFailure");
        }
        if (requestParams.containsKey(PaymentConstants.HASH)) {
            merchantOrderId = requestParams.get(PaymentConstants.MERCHANT_ORDER_ID);
        List<PaymentTransaction> paymentTransactions = paymentTransactionService
                .getPaymentTransactionByMerchantOrderId(merchantOrderId);
        if (paymentTransactions != null) {
            try {
                notifyPaymentStatus(paymentTransactions.get(0), "zpg", success);
            } catch (Exception exception) {
                logger.error("Exception sending status notification for ZPG for order : " + merchantOrderId, exception);
            }

        }
        }
        logger.debug("Done processing of zeropay");
        return FCUtil.getRedirectAction(url);
    }
  

	public Map<String, Object> processPaymentAndRecharge(String orderId, String mtxnId, String lookupID) {
        logger.info("ZPG process payment and recharge for orderId " + orderId + ", mtxnId " + mtxnId + ", lookupID "
                + lookupID);
		metricsClient.recordEvent(
				PaymentCheckoutRequestController.METRICS_SERVICE,
				PaymentCheckoutRequestController.METRIC_PAYMENT, "zAttempt");
		boolean result = false;
		Map<String, Object> responseMap = new HashMap<>();
		Map<String, String> requestParams = new HashMap<>();
		try {

			Integer userId = userServiceProxy.getLoggedInUser().getUserId();
			long transactionId = zeroPaymentService
					.pay(mtxnId, userId);
			double transactionAmount = getTransactionAmount(mtxnId);

			result = true;
			requestParams.put(PaymentConstants.TRANSACTION_ID,
					String.valueOf(transactionId));
			requestParams.put(PaymentConstants.MERCHANT_ORDER_ID,
			        mtxnId);
			requestParams.put(PaymentConstants.AAMOUNT,
					String.valueOf(transactionAmount));
			requestParams.put(PaymentConstants.RESULT, String.valueOf(true));
			requestParams.put(PaymentConstants.MESSAGE, "Zero Payment Success");
			PaymentResponseBusinessDO paymentResponseBusinessDO = new PaymentResponseBusinessDO();

			paymentResponseBusinessDO.setRequestMap(requestParams);
			responseMap = zeroPaymentGatewayHandler
					.paymentResponse(paymentResponseBusinessDO);
			Map<String, Object> resultMap = (Map<String, Object>) responseMap.get(PaymentConstants.DATA_MAP_FOR_WAITPAGE);
            Boolean isPaySuccess = false;
            if (resultMap != null) {
                isPaySuccess = Boolean.valueOf((String) resultMap
                        .get(PaymentConstants.PAYMENT_PORTAL_IS_SUCCESSFUL));
            }
            if (isPaySuccess) {

                // save in-txn-data
                HashMap<String, Object> paymentInfo = new HashMap<String, Object>();
                paymentInfo.put(InTxnDataConstants.PAYMENT_TYPE, Integer.parseInt(PaymentConstants.PAYMENT_TYPE_ZEROPAY));
                paymentInfo.put(InTxnDataConstants.PG_AMOUNT, 0d); // PG amount 0 in case of zero payment
                paymentInfo.put(InTxnDataConstants.WALLET_AMOUNT, 0d); // wallet amount 0 in case of zero payment
                paymentInfo.put(InTxnDataConstants.PAYMENT_SUCCESS_ON, new Date());
                try {
                    logger.info("Populating zero payment data to intxndata " + paymentInfo + " for lookupid " + lookupID);
                    campaignServiceClient.saveInTxnData1(lookupID, FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, paymentInfo);
                }catch (Exception e){
                    logger.error("Error in saving intxndata in zpgcontroller ", e);
                }

                paymentSuccessAction.onPaymentSuccess(orderId, mtxnId);
            }
            List<PaymentTransaction> paymentTransactions = paymentTransactionService
                    .getPaymentTransactionByMerchantOrderId(mtxnId);
            if (paymentTransactions != null) {
                try {
                    notifyPaymentStatus(paymentTransactions.get(0), "zpg", isPaySuccess);
                } catch (Exception exception) {
                    logger.error("Exception sending status notification for ZPG for mtxnId : " + mtxnId, exception);
                }

            }
			metricsClient
					.recordEvent(
                            PaymentCheckoutRequestController.METRICS_SERVICE,
                            PaymentCheckoutRequestController.METRIC_PAYMENT,
                            "zSuccess");
		} catch (Exception e) {
			metricsClient
					.recordEvent(
							PaymentCheckoutRequestController.METRICS_SERVICE,
							PaymentCheckoutRequestController.METRIC_PAYMENT,
							"zFailure");
			result = false;

		}
		responseMap.put(PaymentConstants.RESULT, String.valueOf(result));
		responseMap.put(PaymentConstants.ORDER_ID, orderId);
		return responseMap;
	}

    private double getTransactionAmount(String merchantOrderId) {
        List<PaymentTransaction> paymentTransactions = paymentTransactionService.getPaymentTransactionByMerchantOrderId(merchantOrderId);
        return paymentTransactions.get(0).getAmount();
    }
    
    private void notifyPaymentStatus(PaymentTransaction paymentTransaction, String pg, boolean isPaymentSuccess) {
        try {
            logger.info("Wallet paymentSNSService publish triggered for orderId : " + paymentTransaction.getOrderId());
            PaymentAlertBean alertBean = new PaymentAlertBean();
            alertBean.setMtxnId(paymentTransaction.getMerchantTxnId());
            alertBean.setAmount(paymentTransaction.getAmount());
            alertBean.setChannel(FCUtil.getChannelFromOrderId(paymentTransaction.getOrderId()));
            alertBean.setMessageType(isPaymentSuccess? PaymentStatus.PAYMENT_SUCCESS.getStatus()
                    : PaymentStatus.PAYMENT_FAILURE.getStatus());
            alertBean.setOrderId(paymentTransaction.getOrderId());

            com.freecharge.payment.dos.entity.PaymentPlan fcPaymentPlan = paymentPlanService.getPaymentPlan(paymentTransaction.getOrderId());
            PaymentPlan paymentPlan = new PaymentPlan();
            paymentPlan.setPg(fcPaymentPlan.getPgAmount().getAmount().doubleValue());
            paymentPlan.setCredits(fcPaymentPlan.getWalletAmount().getAmount().doubleValue());
            alertBean.setPaymentPlan(paymentPlan);
            String paymentTypeName = PaymentType.PaymentTypeName.FC_BALANCE.toString();
            alertBean.setPaymentType(paymentTypeName);

            CartBusinessDo cartBusinessDO = cartService.getCartByOrderId(paymentTransaction.getOrderId());
            
            convenienceFeeUtil.populateFeeChargeInPaymentBean(cartBusinessDO, alertBean);
            
            ProductName primaryProduct = cartService.getPrimaryProduct(cartBusinessDO);
            alertBean.setProduct(primaryProduct.name());
            alertBean.setUserId(orderIdReadDAO.getUserIdFromOrderId(paymentTransaction.getOrderId()).longValue());
            alertBean.setPaymentGateway(pg);
            UserTransactionHistory userTransactionHistory = userTransactionHistoryService.findUserTransactionHistoryByOrderId(paymentTransaction.getOrderId());
            String operator = userTransactionHistory.getServiceProvider();
            //Set GST
            gstService.populateGST(alertBean, primaryProduct,operator);
            paymentSNSService.publish(paymentTransaction.getOrderId(), "{\"message\" : " + PaymentAlertBean.getJsonString(alertBean) + "}");
            logger.info("SNS alert published is: "+PaymentAlertBean.getJsonString(alertBean));
        } catch (Exception e) {
            logger.error(" Some error while pushing the payment notification for order id" + paymentTransaction.getOrderId());
        }
    }
    
}
