package com.freecharge.zeropayment;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;

@Component
public class ZeroPaymentService {
    private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private FCProperties fcProperties;

    @Autowired
    private PaymentTransactionService paymentTransactionService;

	// ----- methods ----- //
	public boolean isUnderMaintenance() {
	    return fcProperties.isZeropayUnderMaintenance();
	}

    public boolean isZeropayPayEnabled() {
        return fcProperties.isZeropayPayEnabled();
    }

    public boolean isZeropayDataDisplayEnabled() {
        return fcProperties.isZeropayDataDisplayEnabled();
    }

	@Transactional
	public long pay(String merchantOrderId, Integer userId) {
        List<PaymentTransaction> paymentTransactions = paymentTransactionService.getPaymentTransactionByMerchantOrderId(merchantOrderId);

        if (paymentTransactions.isEmpty()) {
            throw new RuntimeException("No payment transaction found for merchant order id:" + merchantOrderId);
        }

        if (paymentTransactions.size() > 1) {
            logger.error("More than one transaction found for merchant order id:" + merchantOrderId);
        }

        PaymentTransaction paymentTransaction = paymentTransactions.get(0);

        return 0;
	}

    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(ZeroPaymentService.class.getSimpleName());
    }
    
}
