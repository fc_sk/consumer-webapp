package com.freecharge.invoice;

import java.math.BigDecimal;

public class TaxInvoiceInfo {
	private BigDecimal billAmount;
	private BigDecimal couponCharges;
	private BigDecimal handlingCharges;
	private BigDecimal taxableValue;
	private BigDecimal sgst;
	private BigDecimal cgst;
	private BigDecimal totalAmount;
	
	public BigDecimal getBillAmount() {
		return billAmount;
	}
	public void setBillAmount(BigDecimal billAmount) {
		this.billAmount = billAmount;
	}
	public BigDecimal getCouponCharges() {
		return couponCharges;
	}
	public void setCouponCharges(BigDecimal couponCharges) {
		this.couponCharges = couponCharges;
	}
	public BigDecimal getHandlingCharges() {
		return handlingCharges;
	}
	public void setHandlingCharges(BigDecimal handlingCharges) {
		this.handlingCharges = handlingCharges;
	}
	public BigDecimal getTaxableValue() {
		return taxableValue;
	}
	public void setTaxableValue(BigDecimal taxableValue) {
		this.taxableValue = taxableValue;
	}
	public BigDecimal getSgst() {
		return sgst;
	}
	public void setSgst(BigDecimal sgst) {
		this.sgst = sgst;
	}
	public BigDecimal getCgst() {
		return cgst;
	}
	public void setCgst(BigDecimal cgst) {
		this.cgst = cgst;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
}
