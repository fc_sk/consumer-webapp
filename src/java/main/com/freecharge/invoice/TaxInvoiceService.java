package com.freecharge.invoice;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.wallet.OneCheckWalletService;
import com.snapdeal.fcpt.txnhistoryview.client.impl.TxnHistoryViewClientImpl;
import com.snapdeal.fcpt.txnhistoryview.commons.request.GetInvoiceDetailsRequest;
import com.snapdeal.fcpt.txnhistoryview.commons.request.GetTxnDetailsRequest;
import com.snapdeal.fcpt.txnhistoryview.commons.response.GetInvoiceDetailsResponse;
import com.snapdeal.fcpt.txnhistoryview.commons.response.GetTxnDetailsResponse;
import com.snapdeal.fcpt.txnhistoryview.commons.vo.InvoiceInfo;
import com.snapdeal.fcpt.txnhistoryview.exceptions.HttpTransportException;
import com.snapdeal.fcpt.txnhistoryview.exceptions.ServiceException;
import com.snapdeal.ims.dto.UserDetailsDTO;
import com.snapdeal.ims.response.GetUserResponse;

@Service
public class TaxInvoiceService {

	Logger logger = Logger.getLogger(this.getClass());
	
	private TxnHistoryViewClientImpl txnHistoryViewClientImpl;

	@Autowired
	private OneCheckWalletService oneCheckWalletService;
	
	@Autowired
    private MetricsClient metricsClient;
	
	private static final DateFormat invoiceDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@PostConstruct
	public void init() {
		txnHistoryViewClientImpl = new TxnHistoryViewClientImpl();
	}
	public TaxInvoiceDetails getTaxInvoiceDetails(String txnId, String txnType, String imsToken, String userAgent,
			String machineIdentifier) throws Exception {
		GetUserResponse userByToken = oneCheckWalletService.getUserDetailsByToken(imsToken, userAgent,
				machineIdentifier);
		long startMillis = System.currentTimeMillis();
		GetInvoiceDetailsRequest getInvoiceDetailsRequest = new GetInvoiceDetailsRequest();
		getInvoiceDetailsRequest.setTxnId(txnId);
		getInvoiceDetailsRequest.setTxnType(txnType);
		UserDetailsDTO userDetails=userByToken.getUserDetails();
		getInvoiceDetailsRequest.setUserId(userDetails.getUserId());
		GetTxnDetailsRequest getTxnDetailsRequest = new GetTxnDetailsRequest();
		getTxnDetailsRequest.setTxnId(txnId);
		getTxnDetailsRequest.setTxnType(txnType);
		getTxnDetailsRequest.setUserId(userDetails.getUserId());
		
		try {
			GetInvoiceDetailsResponse getInvoiceDetailsResponse = txnHistoryViewClientImpl
					.getInvoiceDetails(getInvoiceDetailsRequest);
			metricSuccess(startMillis, "getTaxInvoiceDetails");
			logger.info("getInvoiceDetailsResponse:" + getInvoiceDetailsResponse);
			
			
			if(getInvoiceDetailsResponse!=null) {
				TaxInvoiceDetails taxInvoiceDetails = new TaxInvoiceDetails();
				taxInvoiceDetails.setEmailId(userDetails.getEmailId());
				taxInvoiceDetails.setMobileNo(userDetails.getMobileNumber());
				taxInvoiceDetails.setUserName(userDetails.getFirstName());
				taxInvoiceDetails.setOrderId(txnId);
				taxInvoiceDetails.setInvoiceDate(invoiceDateFormat.format(getInvoiceDetailsResponse.getTransactionDate()));
				taxInvoiceDetails.setInvoiceId(getInvoiceDetailsResponse.getInvoiceId());
				if (getInvoiceDetailsResponse.getInvoiceDetails() != null) {
					InvoiceInfo invoiceInfo = getInvoiceDetailsResponse.getInvoiceDetails();
					TaxInvoiceInfo taxInvoiceInfo = new TaxInvoiceInfo();
					taxInvoiceInfo.setBillAmount(invoiceInfo.getBillAmount());
					taxInvoiceInfo.setCgst(invoiceInfo.getCgst());
					taxInvoiceInfo.setCouponCharges(invoiceInfo.getCouponCharges());
					taxInvoiceInfo.setHandlingCharges(invoiceInfo.getHandlingCharges());
					taxInvoiceInfo.setSgst(invoiceInfo.getSgst());
					taxInvoiceInfo.setTaxableValue(invoiceInfo.getTaxableValue());
					taxInvoiceInfo.setTotalAmount(invoiceInfo.getTxnAmount());
					taxInvoiceDetails.setInvoiceInfo(taxInvoiceInfo);
				}
				try {
					GetTxnDetailsResponse getTxnDetailsResponse = txnHistoryViewClientImpl.getTxnByDetails(getTxnDetailsRequest);
					taxInvoiceDetails.setServiceNumber(getTxnDetailsResponse.getUserTxnDetails().getFulfilmentInfo().getServiceNumber());
					taxInvoiceDetails.setCategory(getTxnDetailsResponse.getUserTxnDetails().getFulfilmentInfo().getCategory());
				} catch (Exception e) {
					logger.error("Error to fetch invoice service number for txnId: "+txnId);
				}
				return taxInvoiceDetails;
			}
			throw new Exception("No Data Found for this request");
		} catch (HttpTransportException e) {
			logger.error("HttpTransportException while getting getTaxInvoiceDetails to user:",e);
	        recordTypeOfException(startMillis, "getTaxInvoiceDetails", e);
	        throw e;
		} catch (ServiceException e) {
			logger.error("ServiceException while getting getTaxInvoiceDetails to user:",e);
	        recordTypeOfException(startMillis, "getTaxInvoiceDetails", e);
	        throw e;
		} catch (Exception e) {
			logger.error("Exception while getting getTaxInvoiceDetails to user:",e);
	        recordTypeOfException(startMillis, "getTaxInvoiceDetails", e);
	        throw e;
		}

	}

	private void recordTypeOfException(long startMillis, String methodName, Exception exception) {
		Throwable throwableException = ExceptionUtils.getRootCause(exception);
		if (throwableException != null) {
			metricFailure(startMillis, methodName, throwableException.getClass().getSimpleName());
		} else {
			metricFailure(startMillis, methodName, exception.getClass().getSimpleName());
		}
	}

	private void metricFailure(final long startMillis, final String methodName, final String eventName) {
		this.recordMetric(startMillis, methodName, eventName);
	}

	private void metricSuccess(final long startMillis, final String methodName) {
		this.recordMetric(startMillis, methodName, "Success");
	}

	private void recordMetric(final long startMillis, final String methodName, final String eventName) {
		String serviceName = "OnecheckWallet";
		String finalMethodName = methodName + ".Latency";
		long latency = System.currentTimeMillis() - startMillis;

		metricsClient.recordLatency(serviceName, finalMethodName, latency);
		metricsClient.recordEvent(serviceName, methodName, eventName);
	}
	
	public GetTxnDetailsResponse getTxnDetails(String userId, String txnId, String txnType) throws HttpTransportException, ServiceException {
		GetTxnDetailsRequest getTxnDetailsRequest = new GetTxnDetailsRequest();
		getTxnDetailsRequest.setTxnId(txnId);
		getTxnDetailsRequest.setTxnType(txnType);
		getTxnDetailsRequest.setUserId(userId);	
		return txnHistoryViewClientImpl.getTxnByDetails(getTxnDetailsRequest);
	}
}
