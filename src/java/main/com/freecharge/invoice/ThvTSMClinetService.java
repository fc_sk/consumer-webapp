package com.freecharge.invoice;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.snapdeal.fcpt.txnhistoryview.client.impl.TxnHistoryViewClientImpl;
import com.snapdeal.fcpt.txnhistoryview.commons.enums.EnvironmentEnum;
import com.snapdeal.fcpt.txnhistoryview.util.ClientDetails;

@Service
public class ThvTSMClinetService {

	Logger logger = Logger.getLogger(ThvTSMClinetService.class);

	@Value("${thv.client.env}")
	private String thvClientEnv;
	
	@Value("${thv.client.id}")
	private String thvClientId;

	@Value("${thv.client.name}")
	private String thvClientName;

	@Value("${thv.client.key}")
	private String thvClientKey;

	@Value("${thv.client.timeout}")
	private int thvClientTimeOut;

	@Value("${thv.client.port}")
	private String port;

	@Value("${thv.client.ip}")
	private String ip;
	
	@PostConstruct
	public void init() {
		try {
			EnvironmentEnum env = EnvironmentEnum.QA;
			if(thvClientEnv.equalsIgnoreCase("prod")) {
				env=EnvironmentEnum.PRODUCTION;
			} 
			ClientDetails.init(ip, port, thvClientKey, thvClientId, thvClientTimeOut,env);
			logger.info("THV client credentials ip " + ip + " port " + port);
		} catch (Exception e) {
			logger.error("Exception raised while initializing tsm thv client", e);
		}
	}
	
}
