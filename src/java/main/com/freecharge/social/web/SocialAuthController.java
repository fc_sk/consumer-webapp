package com.freecharge.social.web;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.NotAuthorizedException;
import org.springframework.social.SocialException;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.admin.component.GoogleClient;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.RegistrationService;
import com.freecharge.common.businessdo.RegisterBusinessDO;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.util.CSRFTokenManager;
import com.freecharge.common.framework.util.SessionConstants;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.common.util.FCUtil;
import com.freecharge.csrf.annotations.Csrf;
import com.freecharge.freefund.fraud.FreefundFraudConstant;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.rest.model.LoginRequest;
import com.freecharge.rest.model.ResponseStatus;
import com.freecharge.rest.user.LoginSessionResourceController;
import com.freecharge.rest.user.LoginSessionResourceHelper;
import com.freecharge.rest.user.LoginSessionResourceHelper.RememberMeHandler;
import com.freecharge.social.config.FacebookManager;
import com.freecharge.social.config.GoogleManager;
import com.freecharge.social.db.SocialUser;
import com.freecharge.social.db.SocialUserData;
import com.freecharge.tracker.Tracker;
import com.freecharge.util.TrackerEvent;
import com.google.common.base.Strings;

/**
 * This controller is responsible for handling login/signup auth stuff for all
 * providers. Request comes to [provider]/login which starts the oauth2 flow. We
 * redirect to the authorizationUrl of the provider where user gives permission
 * for the scope. After this he's redirected back to our redirect url(This is
 * added at the time we create an app on facebook).Now this callback receives a
 * code if user allowed else error. If user allowed - we exchange it for access
 * token and use it to fetch users data. If user denied - we get error in our
 * callback we show appropriate message to user.
 * <p/>
 * https://www.facebook.com/dialog/oauth?client_id={app-id}&redirect_uri={
 * redirect-uri}&scope={scope} - Authorization
 * <p/>
 * denied response -
 * REDIRECT_URI?error_reason=user_denied&error=access_denied&error_description
 * =The+user+denied+your+request.
 * <p/>
 * success response - We get the {code}
 * <p/>
 * Get access token - GET https://graph.facebook.com/oauth/access_token?
 * client_id={app-id} &redirect_uri={redirect-uri} &client_secret={app-secret}
 * &code={code-parameter}
 */
@Controller
@RequestMapping("/social/auth/*")
public class SocialAuthController {
    private static final String                  VIEW   = "social/auth";

    private Logger                         logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private RegistrationService            registrationService;

    @Autowired
    private UserServiceProxy                    userServiceProxy;

    @Autowired
    private LoginSessionResourceHelper     loginHelper;

    @Autowired
    private LoginSessionResourceController loginController;

    @Autowired
    private FacebookManager                facebookManager;

    @Autowired
    private GoogleManager                  googleManager;

    @Autowired
    private GoogleClient                   googleClient;

    @Autowired
    private MetricsClient                  metricsClient;

    @Autowired
    private Tracker                        tracker;

    @Autowired
    private FCProperties                   fcProperties;

    @Autowired
    private CSRFTokenManager csrfTokenManager;

    @NoLogin
    @Csrf(exclude = false)
    @RequestMapping(value = "fb/login", method = RequestMethod.GET)
    public String fbLogin(@RequestParam final Map<String, String> params, final HttpServletRequest request,
            final HttpServletResponse response, final Model model) {
        // Get any post callback action available
        this.metricsClient.recordEvent("Social", "fblogin", "click");
        return String.format("redirect:%s",
                this.facebookManager.getAuthorizeUrl(params.get(CSRFTokenManager.CSRF_REQUEST_IDENTIFIER)));
    }

    // Name of the succeeding method
    public static final String FB_CALLBACK_CONTROLLER_METHOD_NAME = "fbCallback";

    /**
     * Facebook signin -- Returns HTML response, for consumption by website.
     */
    @NoLogin
    @Csrf(exclude = false)
    @RequestMapping(value = "fb/callback", method = { RequestMethod.GET, RequestMethod.POST })
    // Do not change this method name, it is used in CSRF interceptor
    public String fbCallback(@RequestParam final Map<String, String> params, final HttpServletRequest request,
            final HttpServletResponse response, final Model model) throws IOException {
        try {
            final AttributesHolder ah = new AttributesHolder();
            final SocialResult result = socialCallback(request, params, SocialProvider.FACEBOOK, ah, createFacebookConnector());
            model.addAllAttributes(ah.getAttrs());
            return result.getView();
        } catch (SocialException e) {
            logger.error("Unable to login via Facebook (Web), params: " + params.toString(), e);
            throw new FreechargeSocialException(e, SocialProvider.FACEBOOK);
        } catch (RuntimeException e) {
            metricsClient.recordEvent("Social", "fblogin", "failure_exception");
            logger.error("Unable to login via Facebook (Web), params: " + params.toString(), e);
            throw e;
        }
    }

    /**
     * Google signin -- Returns HTML response, for consumption by website.
     */
    @NoLogin
    @Csrf(exclude = false)
    @RequestMapping(value = "google/callback", method = { RequestMethod.GET, RequestMethod.POST })
    public String googleCallback(@RequestParam final Map<String, String> params, final HttpServletRequest request,
            final HttpServletResponse response, final Model model) throws IOException {
        try {
            final AttributesHolder ah = new AttributesHolder();
            final SocialResult result = socialCallback(request, params, SocialProvider.GOOGLE, ah, createGoogleConnector());
            model.addAllAttributes(ah.getAttrs());
            return result.getView();
        } catch (SocialException e) {
            logger.error("Unable to login via Google (Web), params: " + params.toString(), e);
            throw new FreechargeSocialException(e, SocialProvider.GOOGLE);
        } catch (RuntimeException e) {
            metricsClient.recordEvent("Social", "googlelogin", "failure_exception");
            logger.error("Unable to login via Google (Web), params: " + params.toString(), e);
            throw e;
        }
    }

    private static String socialErrorCode(final SocialException e) {
        if (e instanceof NotAuthorizedException) {
            return "not-authorized";
        } else {
            return "generic";
        }
    }

    /**
     * Facebook signin -- Returns JSON response, for consumption by mobile app.
     */
    @NoLogin
    @RequestMapping(value = "fb/signin", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public SocialResult fbCallbackJson(@RequestParam final Map<String, String> params, final HttpServletRequest request,
            final HttpServletResponse response, final Model model) throws IOException {
        try {
            // We do the CSRF check here manually because IOS requests from the
            // app do not pass CSRF token. Check JIRA ticket FC-422 for more on
            // this.
            if (isAndroidRequest(params)) {
                String requestToken = Strings.nullToEmpty(csrfTokenManager.getCSRFToken(request));
                String sessionToken = CSRFTokenManager.getTokenFromSession();

                if ("".equals(requestToken) || !requestToken.equals(sessionToken)) {
                    response.setStatus(HttpStatus.SC_FORBIDDEN);
                    return null;
                }
            }

            final AttributesHolder ah = new AttributesHolder();
            final SocialResult result = socialCallback(request, params, SocialProvider.FACEBOOK, ah, createFacebookConnector());
            model.addAllAttributes(ah.getAttrs());
            if (!ah.getAttrs().isEmpty()) {
                result.errorMessage = ah.getAttrs();
            }

            captureRegistrationEvent(result, params.get("fcChannel"), request.getParameter("imei"),
                    request.getParameter(FreefundFraudConstant.FINGER_PRINT));

            return result;
        } catch (SocialException e) {
            logger.error("Unable to login via Facebook (REST), params: " + params.toString(), e);
            return new SocialResult(SocialProvider.FACEBOOK).withErrorCode(socialErrorCode(e), e);
        } catch (RuntimeException e) {
            logger.error("Unable to login via Facebook (REST), params: " + params.toString(), e);
            return new SocialResult(SocialProvider.FACEBOOK).withException(e);
        }
    }

    /**
     * Google signin -- Returns JSON response, for consumption by mobile app.
     */
    @NoLogin
    @RequestMapping(value = "google/signin", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public SocialResult googleCallbackJson(@RequestParam final Map<String, String> params,
            final HttpServletRequest request, final HttpServletResponse response, final Model model)
        throws IOException {
        try {
            // We do the CSRF check here manually because IOS requests from the
            // app do not pass CSRF token. Check JIRA ticket FC-422 for more on
            // this.
            if (isAndroidRequest(params)) {
                String requestToken = Strings.nullToEmpty(csrfTokenManager.getCSRFToken(request));
                String sessionToken = CSRFTokenManager.getTokenFromSession();

                if ("".equals(requestToken) || !requestToken.equals(sessionToken)) {
                    response.setStatus(HttpStatus.SC_FORBIDDEN);
                    return null;
                }
            }

            final AttributesHolder ah = new AttributesHolder();
            final SocialResult result = socialCallback(request, params, SocialProvider.GOOGLE, ah, createGoogleConnector());
            model.addAllAttributes(ah.getAttrs());
            if (!ah.getAttrs().isEmpty()) {
                result.errorMessage = ah.getAttrs();
            }

            captureRegistrationEvent(result, params.get("fcChannel"), request.getParameter("imei"),
                    request.getParameter(FreefundFraudConstant.FINGER_PRINT));

            return result;
        } catch (SocialException e) {
            logger.error("Unable to login via Google (REST), params: " + params.toString(), e);
            return new SocialResult(SocialProvider.GOOGLE).withErrorCode(socialErrorCode(e), e);
        } catch (RuntimeException e) {
            logger.error("Unable to login via Google (REST), params: " + params.toString(), e);
            return new SocialResult(SocialProvider.GOOGLE).withException(e);
        }
    }

    private void captureRegistrationEvent(final SocialResult result, final String fcChannel, final String imei,
            final String fingerPrint) {

        if (!result.isSignedUp() || FCUtil.isEmpty(result.getSignedInEmail())) {
            return;
        }

        Map<String, Object> userInfo = new HashMap<String, Object>();
        userInfo.put(FCConstants.GROWTH_EVENT_USER_EMAIL, result.getSignedInEmail());
        userInfo.put(FreefundFraudConstant.TXN_CHANNEL, fcChannel);
        userInfo.put(FCConstants.IMEI_NUMBER, imei);
        userInfo.put(FreefundFraudConstant.FINGER_PRINT, fingerPrint);
    }

    private boolean isAndroidRequest(final Map<String, String> httpParams) {
        return "android".equals(httpParams.get("fcAppType"));
    }

    public class SocialInfo extends AbstractDO {
        private final String name;
        private final String email;
        private final String gender;

        public SocialInfo(final String name, final String email, final String gender) {
            this.name = name;
            this.email = email;
            this.gender = gender;
        }
    }

    public static class CodeOrAccessToken {
        private final String code;
        private final String accessToken;

        public CodeOrAccessToken(final String code, final String accessToken) {
            this.code = code;
            this.accessToken = accessToken;
        }

        public boolean isBlank() {
            return FCStringUtils.isBlank(code) && FCStringUtils.isBlank(accessToken);
        }

        /**
         * Never returns null.
         *
         * @param params
         * @return
         */
        public static CodeOrAccessToken from(final Map<String, String> params) {
            if (params.containsKey("code")) {
                return new CodeOrAccessToken(params.get("code"), null);
            } else if (params.containsKey("access_token")) {
                return new CodeOrAccessToken(null, params.get("access_token"));
            }
            return new CodeOrAccessToken(null, null);
        }
    }

    public interface SocialConnector {
        void trackSignupAttempt();

        void trackSignupSuccess();

        void trackSigninAttempt();

        void trackSigninSuccess();

        void trackSigninFailure();

        boolean isError(Map<String, String> params);

        void onError(AttributesHolder model);

        CodeOrAccessToken getCodeOrAccessToken(Map<String, String> params);

        void onCodeError(AttributesHolder model);

        SocialUser createConnection(CodeOrAccessToken codeOrAccessToken);

        SocialInfo fetchSocialInfo();

        void linkProfile(Users existingUser);

        void updateConnection();
    }

    public SocialConnector createFacebookConnector() {
        final MetricsClient mClient = metricsClient;
        final FacebookManager fbManager = facebookManager;
        return new SocialConnector() {
            private Connection<Facebook> fbConnection       = null;
            private Connection<Facebook> existingConnection = null;

            @Override
            public void trackSignupAttempt() {
                tracker.trackEvent(TrackerEvent.SIGNUP_FACEBOOK_ATTEMPT.createTrackingEvent());
            }

            @Override
            public void trackSignupSuccess() {
                mClient.recordEvent("Social", "fblogin", "success_signup");
                tracker.trackEvent(TrackerEvent.SIGNUP_FACEBOOK_SUCCESS.createTrackingEvent());
            }

            @Override
            public void trackSigninAttempt() {
                tracker.trackEvent(TrackerEvent.SIGNIN_FACEBOOK_ATTEMPT.createTrackingEvent());
            }

            @Override
            public void trackSigninSuccess() {
                mClient.recordEvent("Social", "fblogin", "login_success");
                tracker.trackEvent(TrackerEvent.SIGNIN_FACEBOOK_SUCCESS.createTrackingEvent());
            }

            @Override
            public void trackSigninFailure() {
                mClient.recordEvent("Social", "fblogin", "login_failure");
            }

            @Override
            public boolean isError(final Map<String, String> params) {
                return params.containsKey("error");
            }

            @Override
            public void onError(final AttributesHolder model) {
                model.addAttribute("message", "We need your permission to link your Facebook account.");
                mClient.recordEvent("Social", "fblogin", "denied");
            }

            @Override
            public CodeOrAccessToken getCodeOrAccessToken(final Map<String, String> params) {
                return CodeOrAccessToken.from(params);
            }

            @Override
            public void onCodeError(final AttributesHolder model) {
                model.addAttribute("message", "No code received from Facebook.");
                mClient.recordEvent("Social", "fblogin", "error");
            }

            @Override
            public SocialUser createConnection(final CodeOrAccessToken codeOrAccessToken) {
                fbConnection = codeOrAccessToken.code == null ? fbManager
                        .createConnectionFromAccessToken(codeOrAccessToken.accessToken) : fbManager
                        .createConnection(codeOrAccessToken.code);
                existingConnection = fbManager.getExistingConnection(fbConnection.getKey().getProviderUserId());
                return fbManager.getExistingFacebookUser(fbConnection.getKey().getProviderUserId());
            }

            @Override
            public SocialInfo fetchSocialInfo() {
                UserProfile p = fbConnection.fetchUserProfile();
                FacebookProfile f = fbConnection.getApi().userOperations().getUserProfile();
                return new SocialInfo(p.getName(), p.getEmail(), f.getGender());
            }

            @Override
            public void linkProfile(final Users existingUser) {
                facebookManager.linkFacebookAccount(fbConnection, existingUser);
                mClient.recordEvent("Social", "fblogin", "success_linkprofile");
            }

            @Override
            public void updateConnection() {
                if (existingConnection != null) {
                    // update access token (given by Facebook) into the DB
                    ConnectionKey key = existingConnection.getKey();
                    if (key != null) {
                        fbManager.updateFacebookConnection(fbConnection, key.getProviderUserId());
                    }
                }
            }
        };
    }

    public SocialConnector createGoogleConnector() {
        return new SocialConnector() {
            private Map<String, Object> tokenMap       = null;
            private SocialUserData      socialUserData = null;

            @Override
            public void trackSignupAttempt() {
                tracker.trackEvent(TrackerEvent.SIGNUP_GOOGLE_ATTEMPT.createTrackingEvent());
            }

            @Override
            public void trackSignupSuccess() {
                metricsClient.recordEvent("Social", "googlelogin", "success_signup");
                tracker.trackEvent(TrackerEvent.SIGNUP_GOOGLE_SUCCESS.createTrackingEvent());
            }

            @Override
            public void trackSigninAttempt() {
                tracker.trackEvent(TrackerEvent.SIGNIN_GOOGLE_ATTEMPT.createTrackingEvent());
            }

            @Override
            public void trackSigninSuccess() {
                metricsClient.recordEvent("Social", "googlelogin", "login_success");
                tracker.trackEvent(TrackerEvent.SIGNIN_GOOGLE_SUCCESS.createTrackingEvent());
            }

            @Override
            public void trackSigninFailure() {
                metricsClient.recordEvent("Social", "googlelogin", "login_failure");
            }

            @Override
            public boolean isError(final Map<String, String> params) {
                return params.containsKey("error");
            }

            @Override
            public void onError(final AttributesHolder model) {
                model.addAttribute("message", "We need your permission to link your Google account.");
                metricsClient.recordEvent("Social", "googlelogin", "denied");
            }

            @Override
            public CodeOrAccessToken getCodeOrAccessToken(final Map<String, String> params) {
                return CodeOrAccessToken.from(params);
            }

            @Override
            public void onCodeError(final AttributesHolder model) {
                model.addAttribute("message", "No code received from Google.");
                metricsClient.recordEvent("Social", "googlelogin", "error");
            }

            @Override
            public SocialUser createConnection(final CodeOrAccessToken codeOrAccessToken) {
                String redirectUri = fcProperties.getProperty(FCProperties.GOOGLE_LOGIN_REDIRECT_URI);
                this.tokenMap = (codeOrAccessToken.code == null ? FCUtil.createMap("access_token",
                        (Object) codeOrAccessToken.accessToken, "expires_in", 0L) : googleClient.getAccessTokenMap(
                                codeOrAccessToken.code, redirectUri));
                if (this.tokenMap == null) {
                    throw new IllegalStateException(String.format(
                            "Received Google-OAuth token-map is NULL for accessToken=%s, code=%s, redirect-URI=%s",
                            codeOrAccessToken.accessToken, codeOrAccessToken.code, redirectUri));
                }
                socialUserData = googleManager.getUserData(tokenMap);
                return googleManager.getExistingGoogleUser(socialUserData.providerUserID);
            }

            @Override
            public SocialInfo fetchSocialInfo() {
                return new SocialInfo(socialUserData.displayName, socialUserData.email, socialUserData.gender);
            }

            @Override
            public void linkProfile(final Users existingUser) {
                googleManager.linkGoogleAccount(socialUserData, existingUser, fetchSocialInfo().email);
                metricsClient.recordEvent("Social", "googlelogin", "success_linkprofile");
            }

            @Override
            public void updateConnection() {
                if (tokenMap != null) {
                    // update access token (given by Facebook) into the DB
                    googleManager.updateConnection(socialUserData, socialUserData.providerUserID);
                }
            }
        };
    }

    public class AttributesHolder {
        private final Map<String, String> attrs = new LinkedHashMap<>();

        public void addAttribute(final String key, final String value) {
            attrs.put(key, value);
        }

        public Map<String, String> getAttrs() {
            return Collections.unmodifiableMap(attrs);
        }

        public Model populateModel(final Model model) {
            return model.addAllAttributes(attrs);
        }
    }

    public enum SocialProvider {
        GOOGLE, FACEBOOK
    }

    public class SocialResult extends AbstractDO {
        private final String        provider;               // 'google',
                                                            // 'facebook' etc
        private boolean             userDenied      = false;
        private boolean             signedUp        = false;
        private boolean             linked          = false;
        private boolean             signedIn        = false;
        private String              signedInEmail   = null;
        private String              signedInName    = null;
        private long                signedInCredits = 0;

        private Map<String, String> errorMessage    = null;
        private String              view            = null;

        public SocialResult(final SocialProvider provider) {
            this.provider = provider.name();
        }

        public boolean isUserDenied() {
            return userDenied;
        }

        public void setUserDenied(final boolean userDenied) {
            this.userDenied = userDenied;
        }

        public boolean isSignedUp() {
            return signedUp;
        }

        public void setSignedUp(final boolean signedUp) {
            this.signedUp = signedUp;
        }

        public boolean isLinked() {
            return linked;
        }

        public void setLinked(final boolean linked) {
            this.linked = linked;
        }

        public boolean isSignedIn() {
            return signedIn;
        }

        public void setSignedIn(final boolean signedIn) {
            this.signedIn = signedIn;
        }

        public Map<String, String> getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(final Map<String, String> errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getView() {
            return view;
        }

        public void setView(final String view) {
            this.view = view;
        }

        public String getProvider() {
            return provider;
        }

        public SocialResult withView(final String view) {
            setView(view);
            return this;
        }

        public String getSignedInEmail() {
            return signedInEmail;
        }

        public void setSignedInEmail(final String signedInEmail) {
            this.signedInEmail = signedInEmail;
        }

        public String getSignedInName() {
            return signedInName;
        }

        public void setSignedInName(final String signedInName) {
            this.signedInName = signedInName;
        }

        public long getSignedInCredits() {
            return signedInCredits;
        }

        public void setSignedInCredits(final long signedInCredits) {
            this.signedInCredits = signedInCredits;
        }

        public SocialResult withErrorCode(final String errorCode, final SocialException e) {
            setErrorMessage(FCUtil.createMap("status", "exception", "exception", e.getClass().getSimpleName(),
                    "errorcode", errorCode, "message", e.getMessage(), "fcexmsg",
                    SpringSocialErrorTranslator.getUserFacingErrorMessage(e)));
            return this;
        }

        public SocialResult withException(final RuntimeException e) {
            setErrorMessage(FCUtil.createMap("status", "error", "message", "Something went wrong. Please try again later"));
            return this;
        }

        public String toString() {
            return "SocialResult [ userDenied : " + userDenied + ", signedUp : " + signedUp + ", linked : " + linked
                    + ", signedIn : " + signedIn + ", signedInEmail : " + signedInEmail + ", signedInName : "
                    + signedInName + ", signedInCredits : " + signedInCredits + ", errorMessage :" + errorMessage
                    + ", view :" + view + " ]";
        }
    }

    public SocialResult socialCallback(final HttpServletRequest request, final Map<String, String> params,
            final SocialProvider provider, final AttributesHolder model, final SocialConnector conn)
                    throws IOException {
        final SocialResult result = new SocialResult(provider);
        conn.trackSigninAttempt();
        if (conn.isError(params)) {
            // Send appropriate response as user denied permission
            model.addAttribute("status", "error");
            conn.onError(model);
            return result.withView(VIEW);
        }
        CodeOrAccessToken codeOrAccessToken = conn.getCodeOrAccessToken(params);
        if (codeOrAccessToken.isBlank()) {
            model.addAttribute("status", "error");
            conn.onCodeError(model);
            return result.withView(VIEW);
        }
        // Authorization Code present - get access token
        SocialUser socialUser = conn.createConnection(codeOrAccessToken);

        // Connection formed. Check if connection already exists
        Users curUser = this.userServiceProxy.getLoggedInUser();
        model.addAttribute("callback", "processReward");
        SessionConstants.LoginSource loginSource = null;

        if (provider.equals(SocialProvider.FACEBOOK)) {
            loginSource = SessionConstants.LoginSource.FACEBOOK;
        } else if (provider.equals(SocialProvider.GOOGLE)) {
            loginSource = SessionConstants.LoginSource.GOOGLE;
        }
        if (curUser == null) {
            /*
             * We know: There is no currently logged in user. We want to: 1.
             * Login the social user if the user already exists in the DB. 2. If
             * social user not in DB, then automatically sign him up and do as
             * in #1
             */
            // check whether the email (configured on the social media website)
            // exists in our User database
            SocialInfo socialProfile = conn.fetchSocialInfo();
            String socialEmail = socialProfile.email;
            if (socialUser == null) {
                conn.trackSignupAttempt();
                if (FCUtil.isEmpty(socialEmail)) {
                    model.addAttribute("status", "error");
                    model.addAttribute("message", "Email address not found from social profile. Unable to register.");
                    model.addAttribute("fcexmsg", "Your email address is not verified by " + provider.toString()
                            + ". Please finish your email verification.");
                    return result.withView(VIEW);
                }

                Users existingUser = userServiceProxy.getUserByEmailId(socialEmail);
                if (existingUser == null) {
                    // does not exist, sign him up
                    final int passwordLength = 10;
                    RegisterBusinessDO rbd = new RegisterBusinessDO();
                    rbd.setEmail(socialEmail);
                    rbd.setFirstName(socialProfile.name);
                    if (!FCUtil.isEmpty(socialProfile.gender)) {
                        rbd.setMorf(("" + socialProfile.gender.charAt(0)).toUpperCase());
                        rbd.setTitle("female".equalsIgnoreCase(socialProfile.gender) ? "Ms" : "Mr");
                    }
                    rbd.setMobileNo(FCConstants.DUMMY_MOBILE_NO);
                    rbd.setPassword(RandomStringUtils.randomAlphanumeric(passwordLength));
                    rbd.setLoginSource(loginSource);
                    final String affiliate = params.get("affiliate");
                    int affiliateID = FCConstants.AFFILIATE_ID_WEB;
                    switch (affiliate == null ? "" : affiliate.toLowerCase()) {
                        case "mobileweb":
                            affiliateID = FCConstants.AFFILIATE_ID_MOBILE;
                            break;
                        case "mobileapp":
                            // mobileapp is used for android app
                            affiliateID = FCConstants.AFFILIATE_ID_ANDROID_APP;
                            break;
                        case "mobileiosapp":
                            affiliateID = FCConstants.AFFILIATE_ID_IOS_APP;
                            break;
                        case "mobilewindowsapp":
                            affiliateID = FCConstants.AFFILIATE_ID_WINDOWS_APP;
                            break;
                        default:
                            affiliateID = FCConstants.AFFILIATE_ID_WEB;
                            break;
                    }
                    rbd.setFkAffiliateProfileId(affiliateID);
                    rbd.setImei(request.getParameter(FCConstants.IMEI));
                    rbd.setUniqueDeviceId(request.getParameter(FCConstants.DEVICE_UNIQUE_ID));
                    registrationService.registerUser(new ResponseStatus(), rbd);
                    conn.trackSignupSuccess();
                    result.setSignedUp(true);
                    existingUser = userServiceProxy.getUserByEmailId(socialEmail);

                    if (existingUser == null) {
                        throw new IllegalStateException(
                                String.format("User not found in database after successful signup via "
                                        + "%s login: email=%s, name=%s %s, affiliateID=%s, params=%s",
                                        provider.toString(), socialEmail, rbd.getTitle(), rbd.getFirstName(),
                                        affiliate, params));
                    }
                }
                // link existing user with social FB profile
                conn.linkProfile(existingUser);
                result.setLinked(true);
            }
            // login the user
            LoginRequest loginRequest = new LoginRequest();
            loginRequest.setEmail(socialEmail);
            loginRequest.setRememberme("0");

            ResponseStatus responseStatus = loginHelper.passwordlessLogin(request, loginRequest, RememberMeHandler.NO_REMEMBER,
                    loginSource);
            model.addAttribute("status", "" + responseStatus.getStatus());
            model.addAttribute("message", "" + responseStatus.getResult());
            if (FCConstants.SUCCESS.equals(responseStatus.getResult())) {
                conn.trackSigninSuccess();
                conn.updateConnection();
                result.setSignedIn(true);
                LoginSessionResourceController.WhoAmI wai = loginController.whoami();
                result.setSignedInEmail(wai.getEmail());
                result.setSignedInName(wai.getName());
                result.setSignedInCredits(wai.getCredits());
            } else {
                conn.trackSigninFailure();
            }
        } else {
            return result.withView("redirect:/");
        }
        model.addAttribute("status", "success");
        return result.withView(VIEW);
    }

}
