package com.freecharge.social.web;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.ApiException;
import org.springframework.social.InsufficientPermissionException;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.UserService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.social.config.FacebookManager;
import com.freecharge.social.db.IConnectionRepository;
import com.freecharge.social.db.ReferralCampaign;
import com.freecharge.social.db.SocialUser;

/**
 * This controller handles simple shares on Providers - without ardeals.
 * This prepares a predefined message to be shared on Facebook and called FacebookManager's share method.
 */
@Controller
@RequestMapping("/social/share/*")
public class SocialShareController {

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    protected FCProperties fcProperties;

    @Autowired
    private FacebookManager facebookManager;

    @Autowired
    IConnectionRepository connectionRepository;

    @Autowired
    UserServiceProxy userServiceProxy;

    @Autowired
    ReferralCampaignService referralCampaignService;
    /**
     * Shares Recharge Successful on Facebook.
     * @param response
     * @param model
     * @return
     */
    @RequestMapping(value = "fb/rs", method = RequestMethod.POST)
    public String rechargeSuccessShare(HttpServletResponse response, Model model) {
        Users curUser = this.userServiceProxy.getLoggedInUser();
        if (curUser == null) {
            logger.error("Could not find user");
            model.addAttribute("status", "failure");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "jsonView";
        }
        Connection<Facebook> fbCon = (Connection<Facebook>) this.connectionRepository.
                getSocialConnectionForUser(curUser.getUserId().longValue(), "facebook");
        if (fbCon == null) {
            model.addAttribute("status", "UV");
            return "jsonView";
        }
        try {
            this.facebookManager.shareRechargeSuccess(fbCon, curUser);
            logger.info("fb_share_rs: " + curUser.getUserId());
            model.addAttribute("status", "success");
            return "jsonView";
        } catch (InsufficientPermissionException ip) {
            model.addAttribute("status", "UV");
            return "jsonView";
        } catch (ApiException ae) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            model.addAttribute("status", "error");
            return "jsonView";
        }
    }

    @RequestMapping(value = "fb/saveUserAndRecipients" , method = { RequestMethod.POST })
    public String saveUserAndRecipients(HttpServletRequest request, HttpServletResponse response, Model model) {
        String provider = null;
        String referralCampaignId = null;
        String senderChannelId = null;
        String recipients = null;
        String senderProviderId = null;

        try {
            logger.info("Save fb notification :" + request.getParameter("requestObject") + " "
                    + request.getParameter("recipients") + " " + request.getParameter("provider_id")
                    + " " + request.getParameter("referral_campaign_id"));

            Users curUser = this.userServiceProxy.getLoggedInUser();
            if (curUser == null) {
                logger.error("Could not find user");
                model.addAttribute("status", "failure");
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return "jsonView";
            }
            provider = request.getParameter("provider");
            referralCampaignId = request.getParameter("referral_campaign_id");
            senderChannelId = request.getParameter("sender_channel_id");
            recipients = request.getParameter("recipients");

            SocialUser socialUser = this.connectionRepository.getSocialUser(curUser.getUserId() + "", provider);

            if (socialUser == null) {
                senderProviderId = getSenderProviderId(request.getParameter("requestObject"));
            } else {
                senderProviderId = socialUser.getProvider_id();
            }

            String[] recipientsArray = recipients.split(",");
            for (String recipientProviderId : recipientsArray) {
                ReferralCampaign referralCampaignObj = referralCampaignService.getRecipientReferralObject(
                        senderProviderId, recipientProviderId);

                if (referralCampaignObj == null) {
                    referralCampaignService.insert("" + curUser.getUserId(), provider, senderProviderId,
                            recipientProviderId, senderChannelId, referralCampaignId);
                } else {
                    logger.info("Notification already sent to user with " + provider + " id: " + recipientProviderId
                            + " from id: " + referralCampaignObj.getSenderProviderId());
                }
            }
        } catch (Exception e){
            logger.error("Error saving fb referral notifications sent by : " + senderProviderId + " to : "
                    + recipients, e);
        }
        return "jsonView";
    }

    private String getSenderProviderId(String requestObject) {
        try {
        RestTemplate restTemplate = new RestTemplate();
        String appId =  fcProperties.getProperty(FCConstants.APP_ID);
        String appSecret = fcProperties.getProperty(FCConstants.APP_SECRET);
        String result = restTemplate.getForObject("https://graph.facebook.com/v2.2/oauth/access_token?"
                + "grant_type=client_credentials&client_id=" + appId
                + "&client_secret=" + appSecret ,  String.class);
        String appAccessToken = result.replaceAll("access_token=", "");

        FacebookTemplate facebookTemplate = new FacebookTemplate(appAccessToken);

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();

        Map map = facebookTemplate.fetchObject(requestObject, Map.class, parameters);
        if (!map.isEmpty()) {
            logger.debug("REQUEST OBJECT: " + map.keySet() + " " + map.values());
            Map from = (Map)map.get("from");
            logger.debug("FROM MAP: " + from);
            if (from != null && !from.isEmpty()) {
                return  (String) from.get("id");
            } else {
                logger.info("returning facebook Object didnt contain 'from' object " + requestObject);
            }
        } else {
            logger.info("No resulting map found " + requestObject);
        }
        } catch (Exception e){
            logger.error("Exception in getting sender provider id using facebook request object: " + requestObject);
        }
        return null;
    }

    @RequestMapping(value = "fb/getExcludeIds" , method = { RequestMethod.GET })
    public @ResponseBody Map getExcludeIds(@RequestParam Map<String, String> mapping, HttpServletResponse response) {
        int userId = 0;
        Map responseMap = new HashMap();
        try {
            Users curUser = this.userServiceProxy.getLoggedInUser();
            if (curUser == null) {
                logger.error("Could not find user");
                return null;
            }
            userId = curUser.getUserId();
            List<ReferralCampaign> referralCampaignList = referralCampaignService.getRecipientReferralObject(
                    userId + "");
            List<String> recipientIds = new LinkedList();
            for (ReferralCampaign referralCampaign : referralCampaignList) {
                recipientIds.add(referralCampaign.getRecipientProviderId());
            }
            responseMap.put("recipients", recipientIds.toArray());
            return responseMap;
        } catch (Exception e){
            logger.error("Error retrieving exclude ids for " + userId, e);
            responseMap.put("recipients", null);
        }
        return responseMap;
    }

}
