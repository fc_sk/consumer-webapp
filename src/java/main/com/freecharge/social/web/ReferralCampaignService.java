package com.freecharge.social.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.social.db.ReferralCampaign;
import com.freecharge.social.db.ReferralCampaignDao;

@Service("referralCampaignService")
public class ReferralCampaignService {

    @Autowired
    ReferralCampaignDao referralCampaignDao;

    public long getSenderInvitesCountByReferralCampaignId(Long senderUserId, String referralCampaignId) {
        return referralCampaignDao.getSenderInvitesCountByReferralCampaignId(senderUserId, referralCampaignId);
    }

    public ReferralCampaign getRecipientReferralObject(String senderProviderId, String recipientProviderId){
        return referralCampaignDao.getRecipientReferralObject(senderProviderId, recipientProviderId);
    }


    public int insert(String userId, String provider, String senderProviderId, String recipientProviderId,
                      String senderChannelId, String referralCampaignId) {
        return referralCampaignDao.insert(userId, provider, senderProviderId, recipientProviderId, senderChannelId,
                referralCampaignId);
    }

    public List<ReferralCampaign> getRecipientReferralObject(String userId) {
        return referralCampaignDao.getRecipientReferralObject(userId);
    }

    
    public  List<ReferralCampaign> getSenderInvitesByReferralCampaignId(Long senderUserId, String referralCampaignId) {
        return referralCampaignDao.getSenderInvitesByReferralCampaignId(senderUserId, referralCampaignId);
    }

}
