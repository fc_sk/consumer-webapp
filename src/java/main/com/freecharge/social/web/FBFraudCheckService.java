package com.freecharge.social.web;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Service;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.condition.Result;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.social.config.FacebookManager;

@Service("fbFraudCheckService")
public class FBFraudCheckService {

    public static final String METRIC_FB_CHECK = "fbCheck";
    public static final String METRIC_FB_CHECK_FAILED = "fbCheckFailed";
    public static final String FB_LAST_POST_DAY_COUNT = "lastPostDaysCount";
    public static final String FB_POSTS = "posts";
    public static final String FB_ACCESS_TOKEN = "accessToken";
    public static final String FB_MIN_POSTS_COUNT = "fbMinPostCount";
    public static final String FB_LAST_POST_DAY_DIFFERENCE = "fbLastPostDayDiff";

    @Autowired
    private FacebookManager facebookManager;

    @Autowired
    private AppConfigService appConfig;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private FCProperties fcProperties;

    /*
     * This method checks whether Logged in and Social User are same
     */
    public Result isValidSocialUser(Users user, Connection<Facebook> fbConnection){
        Result result = new Result();
        result.setSuccess(false);

        try {
            UserProfile socialProfile = fbConnection.fetchUserProfile();
            if(!socialProfile.getEmail().equals(user.getEmail())) {
                result.setFailureMessage("Social user : " + socialProfile.getEmail() + " does not match logged in user : " + user.getEmail());
                return result;
            }

            result.setSuccess(true);
        }catch (Exception e){
            logger.error(ErrorCode.FB_FRAUD_ERROR + ": FB Profile retrieving failed for user : " + user.getEmail(), e);
            result.setFailureMessage("Exception while retrieving FB Profile for user : " + user.getEmail() + ". Error : " + e.getMessage());
        }
        return result;
    }

    /*
     * This method checks whether User satisfies Min Posts Count and Last Post Days Count condition
     */
    public Result fbPostsCountValidation(Integer userId, Map<String, Object> fbUserMap, Integer minPostCount, Integer lastPostDayDiff){
        Result result = new Result();
        result.setSuccess(false);

        JSONObject postsObj = (JSONObject) fbUserMap.get(FB_POSTS);
        if (postsObj == null){
            result.setFailureMessage("Posts Details returns null for User : " + userId);
            return result;
        }

        JSONArray postList = (JSONArray) postsObj.get("data");
        if (postList==null){
            result.setFailureMessage("Posts List returns null for User : " + userId);
            return result;
        }

        logger.debug("For user : " + userId + " fbPostCount : " + postList.size() + ", minPostCount : " + minPostCount);

        if ((minPostCount > 0) && (postList.size() >= minPostCount)) {
            logger.info("Minimum posts condition is satisfied by user: " + userId + " with fbPostCount ("
                    + postList.size() + ") < minPostCount(" + minPostCount);
            result.setSuccess(true);
            return result;
        }

         //If post list do not satisfy minimum post count check last post creation time
        if (lastPostDayDiff > 0){
            JSONObject lastPost  = (JSONObject) postList.get(postList.size()-1);
            String lastPostCreationTime  = (String) lastPost.get("created_time");
            logger.debug("Last Post creation time : " + lastPostCreationTime);

            if (lastPostCreationTime == null){
                result.setFailureMessage("fbPostCount (" + postList.size() + ") < minPostCount("
                        + minPostCount + ") and lastPostCreationTime is null for user : " + userId);
                return result;
            }

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date createdOn = sdf.parse(lastPostCreationTime.split("T")[0]);
                Calendar allowedDay = Calendar.getInstance();
                allowedDay.add(Calendar.DATE, -lastPostDayDiff);
                logger.debug("FB_LAST_POST_DAY_DIFFERENCE: " + lastPostDayDiff + " i.e. allowed day: " + allowedDay.getTime().toString()
                        + ", and Last Post Created Time : " + createdOn.toString() + " for user:" + userId);

                if (createdOn.after(allowedDay.getTime())){
                    //if creation time of post falls after our allowed days difference then show error.
                    //This means last post is fairly recent and account is newly created.
                    result.setFailureMessage("fbPostCount (" + postList.size() + ") < minPostCount(" + minPostCount + ") and "
                            + "lastPostCreationTime : " + createdOn.toString() + " is not old as allowed time : "
                            + allowedDay.getTime().toString() + " for user : " + userId);
                    return result;
                }

            } catch (Exception e) {
                logger.error("Unable to parse post creation date: " + lastPostCreationTime + " for user : " + userId, e);
            }
        }

        logger.info("Minimum posts and last post creation day condition is satisfied by user: " + userId
                + " with fbPostCount (" + postList.size() + ") < minPostCount(" + minPostCount + ") and with last post "
                + "allowed diff : " + lastPostDayDiff);
        result.setSuccess(true);
        return result;
    }

    /*
     * This method checks whether User satisfies FB Fraud Check condition
     */
    public Result fbFraudCheck(Users curUser, Connection<Facebook> fbConnection, Integer minPostCount, Integer lastPostDayDiff){
        String failureMessage = "";
        String detailedFailureMessage = fcProperties.getProperty(FCProperties.MSG_OFFER_MAX_LIMIT_REACHED);

        try {

            if (curUser==null){
                failureMessage = "User is null. Fraud check can not be continued";
                return returnFailureResult(failureMessage, detailedFailureMessage, ErrorCode.FB_FRAUD_ERROR);
            }

            if (fbConnection == null){
                failureMessage = "Fb Connection is null for User : " + curUser.getUserId();
                return returnFailureResult(failureMessage, detailedFailureMessage, ErrorCode.FB_FRAUD_ERROR);
            }

            Result userValidationResult = new Result();
            userValidationResult = isValidSocialUser(curUser, fbConnection);
            if(!userValidationResult.isSuccess()) {
                failureMessage = userValidationResult.getFailureMessage();
                return returnFailureResult(failureMessage, detailedFailureMessage, ErrorCode.FB_FRAUD_ERROR);
            }

            logger.debug("Fetching users profilefriendsposts: " + curUser.getUserId());

            Map<String, Object> fbUserMap = this.facebookManager.getProfileFriendsPosts(fbConnection);
            if (fbUserMap == null){
                failureMessage = "ProfileFriendsPosts Details returns null for User : " + curUser.getUserId();
                return returnFailureResult(failureMessage, detailedFailureMessage, ErrorCode.FB_FRAUD_ERROR);
            }

            Result fbPostsCountValidationResult = new Result();
            fbPostsCountValidationResult = fbPostsCountValidation(curUser.getUserId(), fbUserMap, minPostCount, lastPostDayDiff);
            if(!fbPostsCountValidationResult.isSuccess()) {
                failureMessage = fbPostsCountValidationResult.getFailureMessage();
                return returnFailureResult(failureMessage, detailedFailureMessage, ErrorCode.FB_FRAUD_ERROR);
            }

            logger.debug("User: " + curUser.getUserId() + " have passed Facebook Fraud Check with Minimum post count: "
                    + minPostCount + ", Last Post Day Diff: " + lastPostDayDiff);
            Result result = new Result();
            result.setSuccess(true);
            return result;

        }catch (Exception e){
            logger.error(ErrorCode.UNKNOWN_ERROR + ": Something went wrong", e);
            failureMessage = "Something went wrong. Exception : " + e.getMessage();
            return returnFailureResult(failureMessage, detailedFailureMessage, ErrorCode.UNKNOWN_ERROR);
        }
    }

    private Result returnFailureResult(String failureMsg, String detailedFailureMsg, ErrorCode errorCode){
        Result result = new Result();
        result.setSuccess(false);
        result.setErrorCode(errorCode.getErrorNumberString());
        result.setFailureMessage(errorCode + ": " + failureMsg);
        result.setDetailedFailureMessage(detailedFailureMsg);
        return result;
    }

    private static final Logger logger = LoggingFactory.getLogger(FBFraudCheckService.class);
}
