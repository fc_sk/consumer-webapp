package com.freecharge.social.web;

import org.springframework.social.SocialException;

import com.freecharge.social.web.SocialAuthController.SocialProvider;

public class FreechargeSocialException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public final SocialProvider provider;

    public FreechargeSocialException(SocialException e, SocialProvider provider) {
        super(e);
        if (e == null) {
            throw new IllegalArgumentException("Argument SocialException is NULL");
        }
        if (provider == null) {
            throw new IllegalArgumentException("Argument SocialProvider is NULL");
        }
        this.provider = provider;
    }

    @Override
    public SocialException getCause() {
        return (SocialException) super.getCause();
    }

    public SocialProvider getProvider() {
        return provider;
    }

}
