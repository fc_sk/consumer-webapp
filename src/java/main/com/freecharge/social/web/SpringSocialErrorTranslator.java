package com.freecharge.social.web;

import org.springframework.social.SocialException;

public class SpringSocialErrorTranslator {

    public static final String
    AUTHZ_EXPIRED      = "The authorization has expired.",
    UNEXPECTED_ERROR   = "An unexpected error has occurred. Please retry your request later.",
    LOGIN_TO_FACEBOOK  = "Error validating access token: You cannot access the app till you log in to www.facebook.com and follow the instructions given.",
    SESSION_CHANGED    = "Error validating access token: Session does not match current stored session. This may be because the user changed the password since the time the session was created or Facebook has changed the session for security reasons.",
    UNCONFIRMED_USER   = "Error validating access token: Sessions for the user  are not allowed because the user is not a confirmed user.",
    NULL_USER_DATA     = "Received NULL userData",
    PASSWORD_CHANGED   = "The authorization has been revoked. Reason: Error validating access token: The session has been invalidated because the user has changed the password.",
    DIDNT_AUTHZ_PREFIX = "The authorization has been revoked. Reason: Error validating access token: The user has not authorized application ", // suffix: 267804853344286.
    USER_LOGGED_OUT    = "The authorization has been revoked. Reason: Error validating access token: The session is invalid because the user logged out.";

    public static final String NO_INFERRED_MESSAGE = null;

    public static String getUserFacingErrorMessage(SocialException e) {
        final String msg = e.getMessage() == null? "": e.getMessage();
        if (e instanceof org.springframework.social.ExpiredAuthorizationException && AUTHZ_EXPIRED.equals(msg)) {
            return "Facebook authorization expired. Please login into Facebook and try again.";
        } else if (e instanceof org.springframework.social.InternalServerErrorException && UNEXPECTED_ERROR.equals(msg)) {
            return NO_INFERRED_MESSAGE;
        } else if (e instanceof org.springframework.social.InvalidAuthorizationException) {
            if (LOGIN_TO_FACEBOOK.equals(msg)) {
                return "You need to first login to www.facebook.com or Facebook app and follow the instructions given.";
            } else if (SESSION_CHANGED.equals(msg)) {
                return "Either you changed password before login was successful or Facebook changed session for security reasons. Please try logging in again";
            } else if (UNCONFIRMED_USER.equals(msg)) {
                return "Facebook signup process is incomplete. Please confirm Facebook registration first.";
            }
        } else if (e instanceof org.springframework.social.NotAuthorizedException && NULL_USER_DATA.equals(msg)) {
            return NO_INFERRED_MESSAGE;
        } else if (e instanceof org.springframework.social.RevokedAuthorizationException) {
            if (PASSWORD_CHANGED.equals(msg)) {
                return "You changed Facebook password before FreeCharge login was successful. Please try logging in again";
            } else if (msg.startsWith(DIDNT_AUTHZ_PREFIX)) {
                return "You did not authorize FreeCharge to connect to your Facebook account. Authorize and try logging in again.";
            } else if (USER_LOGGED_OUT.equals(msg)) {
                return "You logged out of Facebook. Login on Facebook and try again.";
            }
        }
        return NO_INFERRED_MESSAGE;
    }

}
