package com.freecharge.social.db;

import org.springframework.social.connect.Connection;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 7/15/13
 * Time: 11:56 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IConnectionRepository {
    public Connection<?> getSocialConnectionForUser(Long userId, String provider);
    public Connection<?> getSocialConnection(String providerId, String provider);
    public SocialUser getSocialUser(String providerId, String provider);
    public <Provider> void saveSocialConnection(SocialUserData data, Long userId, String socialEmail);
    public void updateSocialConnection(String providerId, String provider, String accessToken, Long expiry);
    public SocialUser getSocialUser(Long userId, String provider);
    public void verifySocialUser(String providerId, String provider);
    public SocialUserData getSocialUserData(long userId, String provider);
}
