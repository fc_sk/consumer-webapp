package com.freecharge.social.db;


import java.sql.Date;

import com.freecharge.common.framework.basedo.AbstractDO;

public class ReferralCampaign extends AbstractDO {

    private int senderUserId;
    private String provider;
    private String senderProviderId;
    private String recipientProviderId;
    private String referralCampaignId;
    private int senderChannelId;
    private Date notificationSentTime;

    public int getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(int senderUserId) {
        this.senderUserId = senderUserId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getSenderProviderId() {
        return senderProviderId;
    }

    public void setSenderProviderId(String senderProviderId) {
        this.senderProviderId = senderProviderId;
    }

    public String getRecipientProviderId() {
        return recipientProviderId;
    }

    public void setRecipientProviderId(String recipientProviderId) {
        this.recipientProviderId = recipientProviderId;
    }

    public String getReferralCampaignId() {
        return referralCampaignId;
    }

    public void setReferralCampaignId(String referralCampaignId) {
        this.referralCampaignId = referralCampaignId;
    }

    public int getSenderChannelId() {
        return senderChannelId;
    }

    public void setSenderChannelId(int senderChannelId) {
        this.senderChannelId = senderChannelId;
    }

    public Date getNotificationSentTime() {
        return notificationSentTime;
    }

    public void setNotificationSentTime(Date notificationSentTime) {
        this.notificationSentTime = notificationSentTime;
    }
}
