package com.freecharge.social.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

@Repository("socialConnectionRepository")
public class SocialConnectionRepository implements IConnectionRepository {
    private Logger logger = LoggingFactory.getLogger(getClass());
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;

    @Autowired
    public void setDataSource(DataSource dataSource){
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Connection<?> getSocialConnectionForUser(Long userId, String provider) {
        logger.debug("Getting social connection for user: "+userId+" and provider: "+provider);
        String sql = "select * from social_user where user_id = :userId and provider = :provider;";
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("provider", provider);
        List<Connection<?>> connections = this.jdbcTemplate.query(sql, params,
                new SocialConnectionRowMapper(this.connectionFactoryLocator));
        if(connections.size()>0){
            return connections.get(0);
        }
        return null;
    }

    @Override
    public Connection<?> getSocialConnection(String providerId, String provider) {
        logger.debug("Getting social connection for providerUserId: "+providerId+" and provider: "+provider);
        String sql = "select * from social_user where provider = :provider and provider_id = :providerId;";
        Map<String, Object> params = new HashMap<>();
        params.put("providerId", providerId);
        params.put("provider", provider);
        List<Connection<?>> connections = this.jdbcTemplate.query(sql, params,
                new SocialConnectionRowMapper(this.connectionFactoryLocator));
        logger.debug("Found: providerUserId: "+providerId+" provider: "+provider+" Count: "+connections.size());
        return connections.size()>0?connections.get(0):null;
    }

    @Override
    public SocialUser getSocialUser(String providerId, String provider) {
        logger.debug("Getting social connection for providerUserId: "+providerId+" and provider: "+provider);
        String sql = "select * from social_user where provider = :provider and provider_id = :providerId;";
        Map<String, Object> params = new HashMap<>();
        params.put("providerId", providerId);
        params.put("provider", provider);
        List<SocialUser> socialUsers = this.jdbcTemplate.query(sql, params,
                new SocialUserRowMapper(this.connectionFactoryLocator));
        logger.debug("Found: providerUserId: "+providerId+" provider: "+provider+" Count: "+socialUsers.size());
        return socialUsers.size()>0?socialUsers.get(0):null;
    }

    @Override
    public SocialUser getSocialUser(Long userId, String provider) {
        logger.debug("Getting social user for userId: "+userId+" and provider: "+provider);
        String sql = "select * from social_user where user_id = :userId and provider = :provider;";
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userId);
        params.put("provider", provider);
        List<SocialUser> socialUserList = this.jdbcTemplate.query(sql, params,
                new SocialUserRowMapper(this.connectionFactoryLocator));
        logger.debug("Found for user: " + userId + " count: " + socialUserList.size());
        return socialUserList.size()>0?socialUserList.get(0):null;
    }

    public static final String SOCIAL_USER_UPSERT = "INSERT INTO social_user"
            + " (provider, email, provider_id, access_token, secret, display_name, image_url, expiry, user_id, created_on) VALUES"
            + " (:provider, :email, :provider_id, :access_token, :secret, :display_name, :image_url, :expiry, :user_id, :created_on)"
            + " ON DUPLICATE KEY UPDATE"
            + " provider = :provider, email = :email, provider_id = :provider_id, access_token = :access_token,"
            + " secret = :secret, display_name = :display_name, image_url = :image_url, expiry = :expiry,"
            + " user_id = :user_id";

    @Override
    public <Provider> void saveSocialConnection(SocialUserData data, Long userId, String socialEmail) {
        Map<String, Object> params = new HashMap<>();
        params.put("provider", data.providerID);
        params.put("email", socialEmail);
        params.put("provider_id", data.providerUserID);
        params.put("access_token", data.accessToken);
        params.put("secret", data.secret);
        params.put("display_name", data.displayName);
        params.put("image_url", data.imageURL);
        params.put("expiry", data.expireTime);
        params.put("user_id", userId);
        params.put("created_on", new Timestamp(System.currentTimeMillis()));
        this.jdbcTemplate.update(SOCIAL_USER_UPSERT, params);
    }

    public SocialUserData getSocialUserData(long userId, String provider) {
        String sql = "select * from social_user where user_id = :userId and provider = :provider";

        Map<String, ?> params = ImmutableMap.of(
                "userId", userId,
                "provider", provider
        );

        List<SocialUserData> socialUserDataList = this.jdbcTemplate.query(
                sql, params,
                new RowMapper<SocialUserData>() {
                    @Override
                    public SocialUserData mapRow(ResultSet rs, int rowNum) throws SQLException {
                        SocialUserData socialUserData = new SocialUserData();
                        socialUserData.providerID = rs.getString("provider_id");
                        socialUserData.providerUserID = rs.getString("user_id");
                        socialUserData.accessToken = rs.getString("access_token");
                        socialUserData.secret = rs.getString("secret");
                        socialUserData.displayName = rs.getString("display_name");
                        socialUserData.email = rs.getString("email");
                        socialUserData.imageURL = rs.getString("image_url");
                        socialUserData.expireTime = rs.getLong("expiry");
                        return socialUserData;
                    }
                }
        );

        if (socialUserDataList.isEmpty()) {
            return null;
        }

        return socialUserDataList.get(0);
    }


    @Override
    public void updateSocialConnection(String providerId, String provider, String accessToken, Long expiry) {
        String sql = "update social_user set expiry = :expiry, access_token = :accessToken where provider = " +
                ":provider and provider_id = :providerId;";
        Map<String, Object> params = new HashMap<>();
        params.put("expiry", expiry);
        params.put("accessToken", accessToken);
        params.put("provider", provider);
        params.put("providerId", providerId);
        this.jdbcTemplate.update(sql, params);
    }

    @Override
    public void verifySocialUser(String providerId, String provider) {
        String sql = "update social_user set verified = 1 where provider = :provider and provider_id = :providerId;";
        Map<String, Object> params = new HashMap<>();
        params.put("provider", provider);
        params.put("providerId", providerId);
        this.jdbcTemplate.update(sql, params);
    }

    private static final class SocialConnectionRowMapper implements RowMapper<Connection<?>>{
        private ConnectionFactoryLocator connectionFactoryLocator1;

        private SocialConnectionRowMapper(ConnectionFactoryLocator connectionFactoryLocator1) {
            this.connectionFactoryLocator1 = connectionFactoryLocator1;
        }

        @Override
        public Connection<?> mapRow(ResultSet rs, int rowNum) throws SQLException {
            long expiry = rs.getLong("expiry");
            String provider = Strings.nullToEmpty(rs.getString("provider")).toLowerCase();

            //FIXME: Hacky fix for this issue - https://app.asana.com/0/11106413911990/10977081477696
            if ("facebook".equals(provider) && expiry == 0) {
                //Random value, plucked from thin air :)
                expiry = System.currentTimeMillis() + TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS);
            }

            return this.connectionFactoryLocator1.getConnectionFactory(rs.getString("provider")).
                    createConnection(new ConnectionData(rs.getString("provider"), rs.getString("provider_id"),
                            rs.getString("display_name"), null, rs.getString("image_url"),
                            rs.getString("access_token"), rs.getString("secret"), null, expiry));
        }
    }

    private static final class SocialUserRowMapper implements RowMapper<SocialUser> {
        private ConnectionFactoryLocator connectionFactoryLocator1;
        private SocialUserRowMapper(ConnectionFactoryLocator connectionFactoryLocator1) {
            this.connectionFactoryLocator1 = connectionFactoryLocator1;
        }

        @Override
        public SocialUser mapRow(ResultSet rs, int rowNum) throws SQLException {
            Connection<?> con = "facebook".equalsIgnoreCase(rs.getString("provider"))? // only for facebook
                    this.connectionFactoryLocator1.getConnectionFactory(rs.getString("provider")).
                    createConnection(new ConnectionData(rs.getString("provider"), rs.getString("provider_id"),
                            rs.getString("display_name"), null, rs.getString("image_url"), rs.getString("access_token"),
                            rs.getString("secret"), null, rs.getLong("expiry"))): null;
            SocialUser user = new SocialUser();
            user.setUserId(rs.getLong("user_id"));
            user.setProvider(rs.getString("provider"));
            user.setVerified(rs.getBoolean("verified"));
            user.setCreatedOn(rs.getDate("created_on"));
            user.setUpdatedOn(rs.getDate("updated_on"));
            user.setConnection(con);
            return user;
        }
    }
}
