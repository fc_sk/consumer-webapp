package com.freecharge.social.db;

import java.util.Map;

import org.springframework.social.connect.ConnectionData;

import com.freecharge.common.framework.basedo.AbstractDO;

public class SocialUserData extends AbstractDO {

    public String providerID = null;
    public String providerUserID = null;
    public String accessToken = null;
    public String secret = null;
    public String displayName = null;
    public String email = null;
    public String gender = null;
    public String imageURL = null;
    public long expireTime = 0;

    public static SocialUserData createFrom(ConnectionData data) {
        SocialUserData inst = new SocialUserData();
        inst.providerID = data.getProviderId();
        inst.providerUserID = data.getProviderUserId();
        inst.accessToken = data.getAccessToken();
        inst.secret = data.getSecret();
        inst.displayName = data.getDisplayName();
        inst.imageURL = data.getImageUrl();
        inst.expireTime = data.getExpireTime() == null? 0: data.getExpireTime();
        return inst;
    }

    public static SocialUserData createFromGoogle(Map<String, Object> tokenMap, Map<String, Object> userData,
            Map<String, Object> plusData) {
        SocialUserData inst = new SocialUserData();
        inst.providerID = "google";
        inst.accessToken = (String)tokenMap.get("access_token");
        inst.expireTime = (Long)tokenMap.get("expires_in");
        inst.providerUserID = userData == null? null: (String) userData.get("id");
        // Converting email in user object to lowercase as google acts stupid sometimes.
        inst.displayName = userData == null? null: (String) userData.get("displayName");
        if (userData != null && userData.get("email") != null) {
            inst.email = ((String)userData.get("email")).toLowerCase();
        }
        inst.gender = (plusData == null? null: (String) plusData.get("gender"));
        return inst;
    }

}
