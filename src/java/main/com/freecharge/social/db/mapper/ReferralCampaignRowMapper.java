package com.freecharge.social.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.social.db.ReferralCampaign;

public class ReferralCampaignRowMapper implements RowMapper<ReferralCampaign> {


    @Override
    public ReferralCampaign mapRow(ResultSet resultSet, int i) throws SQLException {
        ReferralCampaign referralCampaign = new ReferralCampaign();
        referralCampaign.setSenderUserId(Integer.parseInt(resultSet.getString("sender_user_id")));
        referralCampaign.setSenderProviderId(resultSet.getString("sender_provider_id"));
        referralCampaign.setRecipientProviderId(resultSet.getString("recipient_provider_id"));
        referralCampaign.setProvider(resultSet.getString("provider"));
        referralCampaign.setReferralCampaignId(resultSet.getString("referral_campaign_id"));
        referralCampaign.setSenderChannelId(Integer.parseInt(resultSet.getString("sender_channel_id")));
        referralCampaign.setNotificationSentTime(resultSet.getDate("notification_sent_time"));
        return referralCampaign;
    }
}
