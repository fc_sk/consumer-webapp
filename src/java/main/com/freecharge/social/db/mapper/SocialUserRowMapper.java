package com.freecharge.social.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.freecharge.social.db.SocialUser;

public class SocialUserRowMapper implements RowMapper<SocialUser> {

    public SocialUser mapRow(ResultSet resultSet, int row) throws SQLException {
        SocialUser socialUser = new SocialUser();
        socialUser.setProvider(resultSet.getString("provider"));
        socialUser.setProvider_id(resultSet.getString("provider_id"));
        socialUser.setUserId(Long.parseLong(resultSet.getString("user_id")));
        return socialUser;
    }
}
