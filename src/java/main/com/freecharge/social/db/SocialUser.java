package com.freecharge.social.db;

import java.util.Date;

import org.springframework.social.connect.Connection;

import com.freecharge.common.framework.basedo.AbstractDO;

public class SocialUser extends AbstractDO{
    private Long userId;
    private String provider;
    private String provider_id;
    private Connection<?> connection;
    private Boolean verified;
    private Date createdOn;
    private Date updatedOn;

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Connection<?> getConnection() {
        return connection;
    }

    public void setConnection(Connection<?> connection) {
        this.connection = connection;
    }

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }
}
