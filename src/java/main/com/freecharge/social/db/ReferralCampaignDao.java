package com.freecharge.social.db;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.social.db.mapper.ReferralCampaignRowMapper;

@Service("ReferralCampaignDao")
public class ReferralCampaignDao {

	private static Logger logger = LoggingFactory.getLogger(ReferralCampaignDao.class);
	
    public static final String REFERRAL_CAMPAIGN_GET_FROM_RECIPIENT_QUERY = "SELECT * FROM referral_campaign "
            + "where recipient_provider_id=:recipientProviderId and "
            + "sender_provider_id=:senderProviderId";

    public static final String REFERRAL_CAMPAIGN_GET_FROM_USER_QUERY = "SELECT * FROM referral_campaign "
            + "where sender_user_id=:userId";

    public static final String REFERRAL_CAMPAIGN_INSERT_QUERY = "insert into referral_campaign (sender_user_id, "
            + "provider, sender_provider_id, recipient_provider_id, sender_channel_id, referral_campaign_id, "
            + "notification_sent_time) values(:senderUserId,:provider,:senderProviderId, "
            + ":recipientProviderId, :senderChannelId, :referralCampaignId, now())";

    public static final String REFERRAL_CAMPAIGN_GET_COUNT = "select count(*) from referral_campaign where "
            + "DATE(notification_sent_time)=CURDATE()";

    private NamedParameterJdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert insertReferralCampaign;

    private DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource);
        this.insertReferralCampaign = new SimpleJdbcInsert(dataSource).withTableName("referral_campaign").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
    }

    public int insert(String senderUserId, String provider, String providerId,
                          String recipientProviderId, String senderChannelId,
                          String referralCampaignId) {
        Map<String, Object> paramMap = new HashMap();
        logger.debug("Sender User ID::" + senderUserId);
        paramMap.put("senderUserId", senderUserId);
        paramMap.put("provider", provider);
        paramMap.put("senderProviderId", providerId);
        paramMap.put("recipientProviderId", recipientProviderId);
        paramMap.put("senderChannelId", senderChannelId);
        paramMap.put("referralCampaignId" , referralCampaignId);
        try {
            Number id = this.jdbcTemplate.update(REFERRAL_CAMPAIGN_INSERT_QUERY, paramMap);
            return id.intValue();
        } catch (Exception e){
            logger.error("create Referral campaign failed for sender user id:" +
                    senderUserId + "and recipeint id: " + recipientProviderId
                    , e);
            throw e;
        }
    }

    public ReferralCampaign getRecipientReferralObject(String senderProviderId, String recipient) {
        Map<String, Object> paramMap = new HashMap();
        paramMap.put("recipientProviderId", recipient);
        paramMap.put("senderProviderId", senderProviderId);
        List<ReferralCampaign> referralCampaignList = this.jdbcTemplate.query(
                REFERRAL_CAMPAIGN_GET_FROM_RECIPIENT_QUERY,paramMap, new ReferralCampaignRowMapper());
        return  FCUtil.getFirstElement(referralCampaignList);
    }

    public List<ReferralCampaign> getRecipientReferralObject(String userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("userId", userId);
        List<ReferralCampaign> referralCampaignList = this.jdbcTemplate.query(REFERRAL_CAMPAIGN_GET_FROM_USER_QUERY,
                paramMap,
                new ReferralCampaignRowMapper());
        return referralCampaignList;
    }

    public long totalNumberOfNotificationsSentToday() {
        SqlParameterSource paramMap = null;
        return this.jdbcTemplate.queryForLong(REFERRAL_CAMPAIGN_GET_COUNT, paramMap);
    }
	
    public long getSenderInvitesCountByReferralCampaignId(Long senderUserId, String referralCampaignId){        
        long referralCampaignCount = 0;
        try { 
            String queryString = "SELECT COUNT(sender_user_id) FROM referral_campaign where sender_user_id = :sender_user_id and referral_campaign_id = :referral_campaign_id";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("sender_user_id", senderUserId);
            paramMap.put("referral_campaign_id", referralCampaignId);
            referralCampaignCount = this.jdbcTemplate.queryForLong(queryString, paramMap);          
        } catch (Exception e){
            logger.error("Retrieval of ReferralCampaign failed for senderUserId : " + senderUserId, e);
        }        
        return referralCampaignCount;        
    }

    public List<ReferralCampaign> getSenderInvitesByReferralCampaignId(Long senderUserId, String referralCampaignId){        
        List<ReferralCampaign> referralCampaignList = null;
        try { 
            String queryString = "SELECT * FROM referral_campaign where sender_user_id = :sender_user_id and "
                    + "referral_campaign_id = :referral_campaign_id";
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("sender_user_id", senderUserId);
            paramMap.put("referral_campaign_id", referralCampaignId);
            referralCampaignList = this.jdbcTemplate.query(queryString, paramMap, new ReferralCampaignRowMapper());          
        } catch (Exception e){
            logger.error("Retrieval of ReferralCampaign failed for senderUserId : " + senderUserId, e);
        }
        return referralCampaignList;
    }

}
