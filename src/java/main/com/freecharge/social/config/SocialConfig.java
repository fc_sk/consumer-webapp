package com.freecharge.social.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;

import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;

@Configuration
public class SocialConfig {
    @Autowired
    private FCProperties fcProperties;

    @Bean
    @Scope(value = "singleton", proxyMode = ScopedProxyMode.INTERFACES)
    public ConnectionFactoryLocator connectionFactoryLocator(){
        ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
        registry.addConnectionFactory(new FacebookConnectionFactory(fcProperties.getProperty(FCConstants.FB_CLIENT_ID),
                fcProperties.getProperty(FCConstants.FB_CLIENT_SECRET)));
        return registry;
    }
}
