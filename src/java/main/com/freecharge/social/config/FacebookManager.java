package com.freecharge.social.config;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.social.db.IConnectionRepository;
import com.freecharge.social.db.SocialUser;
import com.freecharge.social.db.SocialUserData;
import com.freecharge.web.service.CommonHttpService;
import org.apache.commons.lang.NotImplementedException;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.social.ApiException;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.facebook.api.*;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 7/18/13
 * Time: 7:44 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class FacebookManager {
    public static final String BIRTHDAY = "birthday";
    public static final String GENDER = "gender";
    private Logger logger = LoggingFactory.getLogger(getClass());
    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;

    @Autowired
    IConnectionRepository connectionRepository;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private CommonHttpService commonHttpService;

    /**
     * This method returns the authorization for facebook oauth2. It uses connectionfactorylocator to get facebook
     * factory. It basically sets scope, clientId and clientSecret to the url and returns it.
     * @return authorization url
     */
    public String getAuthorizeUrl(String state){
        FacebookConnectionFactory facebookConnectionFactory = (FacebookConnectionFactory)
                this.connectionFactoryLocator.getConnectionFactory(Facebook.class);
        OAuth2Operations oAuth2Operations = facebookConnectionFactory.getOAuthOperations();
        OAuth2Parameters parameters = new OAuth2Parameters();
        parameters.setScope(fcProperties.getProperty(FCConstants.FB_SCOPE));
        if(FCStringUtils.isNotBlank(state)){
            parameters.setState(state);
        }
        parameters.setRedirectUri(fcProperties.getAppRootUrl()+fcProperties.getProperty(FCConstants.FB_REDIRECT_URI));
        return oAuth2Operations.buildAuthenticateUrl(GrantType.AUTHORIZATION_CODE, parameters);
    }

    /**
     * This method represent the second step of oauth2 flow where we get accessToken in exchange of the code we
     * received from facebook. It makes an http get request to facebook graph api access token endpoint to get access
     * token for the current user. AccessGrant object is created and is used to connect users facebook account. The new
     * Connection object formed is returned which is used to manage users data on facebook.
     * @param code
     * @return
     */
    public Connection<Facebook> createConnection(String code) {
        FacebookConnectionFactory facebookConnectionFactory = (FacebookConnectionFactory) this.
                connectionFactoryLocator.getConnectionFactory(Facebook.class);
        OAuth2Operations oAuth2Operations = facebookConnectionFactory.getOAuthOperations();
        final String url = fcProperties.getAppRootUrl() + fcProperties.getProperty("fb.redirect.url");
        try {
            AccessGrant grant = oAuth2Operations.exchangeForAccess(code, url, null);
            return facebookConnectionFactory.createConnection(grant);
        } catch (HttpClientErrorException e) {
            throw new IllegalStateException("Error creating Facebook connection redirect_url=" + url +
                    ", response status: " + e.getStatusText() +
                    ", response body: " + e.getResponseBodyAsString(), e);
        }
    }

    public Connection<Facebook> createConnectionFromAccessToken(String accessToken) {
        try {
            FacebookConnectionFactory facebookConnectionFactory = (FacebookConnectionFactory) this.
                    connectionFactoryLocator.getConnectionFactory(Facebook.class);
            return facebookConnectionFactory.createConnection(new AccessGrant(accessToken));
        }
        catch (Exception e) {
            logger.error("Getting FB Connection From AccessToken failed", e);
            return null;
        }
    }

    public Connection<Facebook> getExistingConnection(String providerUserId){
        return (Connection<Facebook>) this.connectionRepository.getSocialConnection(providerUserId, "facebook");
    }

    public SocialUser getExistingFacebookUser(String providerUserId){
        return this.connectionRepository.getSocialUser(providerUserId, "facebook");
    }

    public SocialUser getExistingFbUserWithLastUpdatedGtThan(Long userId, Date date){
        SocialUser socialUser = this.connectionRepository.getSocialUser(userId, "facebook");
        if (socialUser!=null && socialUser.getUpdatedOn().after(date)){
            return socialUser;
        }
        return null;
    }

    public void updateFacebookConnection(Connection<Facebook> fbConnection, String existingId){
        ConnectionData fbData = fbConnection.createData();
        this.connectionRepository.updateSocialConnection(existingId, "facebook", fbData.getAccessToken(),
                fbData.getExpireTime());
    }

    public void linkFacebookAccount(Connection<Facebook> connection, Users user){
        UserProfile socialProfile = connection.fetchUserProfile();
        this.connectionRepository.saveSocialConnection(SocialUserData.createFrom(connection.createData()),
                user.getUserId().longValue(), socialProfile.getEmail());
        //fetch extra data about user. Process in a separate job.
        try {
            this.metricsClient.recordEvent("Social", "fb", "link_account");
        } catch (Exception e) {
            logger.error("Exception while trying to enqueue facebook linked user id - " + user.getUserId(), e);
        }
    }

    /**
     * This method is called for any type of share. It prepares the feed share (MultiValueMap) and using
     * FacebookTemplate posts on user's wall. This method should be called for final sharing. For each type of post
     * data map should have required params otherwise ApiException is thrown.
     * If post type is not implemented it throws NotImplementedException.
     *
     *
     *
     * @param fbCon Connection<Facebook>
     * @param data Map<String. String>
     * @param type Post.PostType
     * @return String - PostId on Facebook
     * @throws ApiException
     * @throws NotImplementedException
     */
    public String share(Connection<Facebook> fbCon, Map<String, Object> data, Post.PostType type) throws
            ApiException, NotImplementedException {
        switch (type) {
            case LINK:
                MultiValueMap<String, Object> dataMap = new LinkedMultiValueMap<>();
                dataMap.set("link", data.get("postLink"));
                dataMap.set("message", data.get("postMessage"));
                dataMap.set("name", data.get("postName"));
                dataMap.set("caption", data.get("postCaption"));
                dataMap.set("description", data.get("postDescription"));
                dataMap.set("picture", data.get("postPicture"));
                return fbCon.getApi().publish("me", "feed", dataMap);
            default:
                throw new NotImplementedException("Operation type: "+type+" is not implemented");
        }
    }

    public void shareRechargeSuccess(Connection<Facebook> fbCon, Users user) throws ApiException, NotImplementedException{
        Map<String, Object> data = new HashMap<>();
        data.put("postMessage", "This is too good to be true! I just recharged my phone and got cool coupons. " +
                "You can do it too. ");
        data.put("postLink", "https://www.freecharge.in/?utm_source=facebook&utm_medium=banner&utm_campaign=fb-success-share");
        data.put("postCaption", "FreeCharge");
        data.put("postName", "The smartest way to recharge.");
        data.put("postDescription", "Get more value for your money with FreeCharge - instant recharge and rewards!");
        data.put("postPicture", fcProperties.getImagePrefix1()+"/images/logo/Free_love.png");
        this.share(fbCon, data, Post.PostType.LINK);
        this.metricsClient.recordEvent("Social", "fb", "recharge_successful");
        logger.info("fb_share_rs: "+user.getUserId());
    }

    public Map<String, Object> getProfileFriendsPosts(Connection<Facebook> fbCon){
        logger.info("Fetching fb_data for fraud_check for fb_user: "+fbCon.getKey().getProviderUserId());
        String url = "https://graph.facebook.com/v2.2/me";
        Map<String, String> fields = new HashMap<>();
        fields.put("fields", FRAUD_CHECK_FIELDS);
        fields.put("access_token", fbCon.createData().getAccessToken());
        try{
            Long startTime = System.currentTimeMillis();
            JSONObject result = null;
            CommonHttpService.HttpResponse response = this.commonHttpService.fireGetRequestWithTimeOut(url, fields, 5000);
            if (response.getStatusCode().equals(200)){
                JSONParser parser = new JSONParser();
                result = (JSONObject) parser.parse(response.getResponseBody());
            }
            Long endTime = System.currentTimeMillis();
            this.metricsClient.recordLatency("Social", "fbFraud", endTime-startTime);
            logger.info("Returning fb_fraud_check_data for fb_user: "+fbCon.getKey().getProviderUserId());
            return result;
        }catch (Exception e){
            logger.error("Error occurred while making http call to facebook:", e);
            return null;
        }
    }

    /**
     * We can get only up to 100 friends in one http call. Hence a loop to fetch all friends. We do not know the count
     * of friends so if 100 friends are fetched assuming more than 100 friends we make another call else exit the loop.
     *
     * @param facebook
     * @return
     */
    public List<Map<String, Object>> getFriendsMap(Facebook facebook){
    	int offset = 0;
        int limit = 100;
    	if (facebook == null){
    		logger.error("Received null facebook object");
    		return null;
    	}
    	FriendOperations friendOperations = facebook.friendOperations();
    	if (friendOperations == null){
    		return null;
    	}
        logger.debug("Pulling Friends from Facebook");
        List<FacebookProfile> allFriends = new ArrayList<>();
        List<FacebookProfile> friends = null;
        do {
        	friends = friendOperations.getFriendProfiles(new PagingParameters(limit, offset, null, null));
        	if (friends == null){
        		logger.error("Received null facebook.friendOperations.friends object");
        	}else{
        		allFriends.addAll(friends);
        	}
            offset+=limit;
        }while (friends.size()>99);
        List<Map<String, Object>> friendList = new ArrayList<>();
        for (FacebookProfile fp: allFriends){
        	if (fp != null){
        		 Map<String, Object> tm = new HashMap<>();
                 tm.put("id", fp.getId());
                 tm.put("name", fp.getName());
                 tm.put("birthday", fp.getBirthday());
                 tm.put("gender", fp.getGender());
                 tm.put("location", fp.getLocation()!=null?locationAsMap(fp.getLocation()):null);
                 friendList.add(tm);
        	}
        }
        return friendList;
    }

    public Map<String, Object> getFacebookProfile(Facebook facebook){
    	if (facebook == null){
    		logger.error("Received null facebook object");
    		return null;
    	}
    	UserOperations userOperations = facebook.userOperations();
    	FacebookProfile fbProfile = userOperations.getUserProfile();
    	if (userOperations == null || fbProfile == null){
    		logger.error("Received null facebook profile object");
    		return null;
    	}
        Map<String, Object> profile = new HashMap<>();
        addToMapIfValueNotNull(BIRTHDAY, fbProfile.getBirthday(), profile);
        addToMapIfValueNotNull("bio", fbProfile.getBio(), profile);
        addToMapIfValueNotNull("name", fbProfile.getName(), profile);
        addToMapIfValueNotNull("email", fbProfile.getEmail(), profile);
        addToMapIfValueNotNull(GENDER, fbProfile.getGender(), profile);
        addToMapIfValueNotNull("hometown", referenceAsMap(fbProfile.getHometown()), profile);
        addToMapIfValueNotNull("location", referenceAsMap(fbProfile.getLocation()), profile);
        //addToMapIfValueNotNull("username", fbProfile.getUsername(), profile);
        addToMapIfValueNotNull("education", getEducationHistory(fbProfile.getEducation()), profile);
        return profile;
    }

    private void addToMapIfValueNotNull(String name, Object value, Map<String, Object> profile){
    	if (profile != null && value != null){
    		profile.put(name, value);
    	}
    }
    
    private List<Map<String, Object>> getEducationHistory(List<EducationEntry> educationEntries){
    	if (educationEntries == null){
    		return null;
    	}
        List<Map<String, Object>> educationMap = new ArrayList<>();
        for (EducationEntry ee : educationEntries){
            Map<String, Object> edu = new HashMap<>();
            addToMapIfValueNotNull("school", referenceAsMap(ee.getSchool()), edu);
            addToMapIfValueNotNull("type", ee.getType(), edu);
            addToMapIfValueNotNull("year", referenceAsMap(ee.getYear()), edu);
            addToMapIfValueNotNull("concentration", referenceListAsMapList(ee.getConcentration()), edu);
            educationMap.add(edu);
        }
        return educationMap;
    }

    private List<Map<String, Object>> referenceListAsMapList(List<Reference> referenceList){
    	if (referenceList == null){
    		return null;
    	}
        List<Map<String, Object>> mapList = new ArrayList<>();
        for(Reference r : referenceList){
            mapList.add(referenceAsMap(r));
        }
        return mapList;
    }

    private Map<String, Object> referenceAsMap(Reference reference){
    	if (reference == null){
    		return null;
    	}
        Map<String, Object> map = new HashMap<>();
        map.put("id", reference.getId());
        map.put("name", reference.getName());
        return map;
    }

    /**
     * Spring Social provides a Reference object indicating location. So converting it into a Map
     *
     * @param location
     * @return
     */
    private Map<String, Object> locationAsMap(Reference location){
        Map<String, Object> loc = new HashMap<>();
        loc.put("id", location.getId());
        loc.put("name", location.getName());
        return loc;
    }

    /**
     * Fetching User's interests represented by this Facebook Connection.
     *
     * @param facebook
     * @return
     */
    public List<Map<String, Object>> getInterests(Facebook facebook){
    	if (facebook == null){
    		logger.error("Received null facebook object.");
    	}
        logger.debug("Pulling User's interests.");
        LikeOperations likeOperations = facebook.likeOperations();
        if (likeOperations == null ){
        	return null;
        }
        List<Page> interests = likeOperations.getInterests();
        if (interests == null){
        	return null;
        }
        List<Map<String, Object>> userInterests = new ArrayList<>();
        for(Page pg : interests){
            userInterests.add(getFbPageMap(pg));
        }
        return userInterests;
    }

    /**
     * Fetching user's liked pages.
     *
     * @param facebook
     * @return
     */
    public List<Map<String, Object>> getLikedPages(Facebook facebook){
        logger.debug("Pulling Likes.");
        if (facebook == null){
    		logger.error("Received null facebook object.");
    	}
        LikeOperations likeOperations = facebook.likeOperations();
        if (likeOperations == null){
        	return null;
        }
        List<Page> likedPages = likeOperations.getPagesLiked();
        if (likedPages == null){
        	return null;
        }
        List<Map<String, Object>> userLikes = new ArrayList<>();
        for(Page pg: likedPages){
        	if (pg != null){
        		userLikes.add(getFbPageMap(pg));
        	}
        }
        return userLikes;
    }

    private Map<String, Object> getFbPageMap(Page page){
        Map<String, Object> map = new HashMap<>();
        map.put("id", page.getId());
        map.put("name", page.getName());
        return map;
    }

    public static String FRAUD_CHECK_FIELDS = "id,name,gender,posts";

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        IConnectionRepository connectionRepository = context.getBean(IConnectionRepository.class);
        Connection<Facebook> fbCon = (Connection<Facebook>) connectionRepository.getSocialConnectionForUser(481l, "facebook");
        FacebookManager facebookManager = context.getBean(FacebookManager.class);
        Map<String, Object> map = facebookManager.getFacebookProfile(fbCon.getApi());
        System.out.println(map);
        ((ConfigurableApplicationContext) context).close();
    }
}
