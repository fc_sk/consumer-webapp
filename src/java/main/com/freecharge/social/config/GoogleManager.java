package com.freecharge.social.config;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.NotAuthorizedException;
import org.springframework.stereotype.Service;

import com.freecharge.admin.component.GoogleClient;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.social.db.IConnectionRepository;
import com.freecharge.social.db.SocialUser;
import com.freecharge.social.db.SocialUserData;

@Service
public class GoogleManager {

    private final Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private IConnectionRepository connectionRepository;

    @Autowired
    private GoogleClient googleClient;

    @Autowired
    private MetricsClient metricsClient;

    public SocialUser getExistingGoogleUser(String providerUserId){
        return this.connectionRepository.getSocialUser(providerUserId, "google");
    }

    public SocialUserData getUserData(Map<String, Object> tokenMap) {
        String accessToken = (String)tokenMap.get("access_token");
        Map<String, Object> userData = this.googleClient.getUserProfile(accessToken);
        Map<String, Object> plusData = this.googleClient.getGooglePlusProfile(accessToken);
        if (userData == null) {
            // FIXME: shantanu
            // This is a hack to emulate what SpringSocial would have done. We catch this exception in controller and
            // make some meaning out of it.
            throw new NotAuthorizedException("google", "Received NULL userData");
        }
        if (!FCUtil.isEmpty(plusData)) {
            final String DISPLAY_NAME = "displayName";
            if (plusData.containsKey(DISPLAY_NAME) && (!FCUtil.isEmpty((String) plusData.get(DISPLAY_NAME)))) {
                userData.put(DISPLAY_NAME, plusData.get(DISPLAY_NAME));
            } else if (plusData.containsKey("name")) {
                @SuppressWarnings("unchecked")
                Map<String, String> nameTokens = (Map<String, String>) plusData.get("name");
                final String givenName = FCUtil.trim(nameTokens.get("givenName"));
                final String familyName = FCUtil.trim(nameTokens.get("familyName"));
                userData.put(DISPLAY_NAME, (FCUtil.isEmpty(givenName) && FCUtil.isEmpty(familyName))?
                        userData.get("email"): (givenName + " " + familyName));
            }
        }
        return SocialUserData.createFromGoogle(tokenMap, userData, plusData);
    }

    public void linkGoogleAccount(SocialUserData userData, Users user, String email) {
        final Integer userId = user.getUserId();
        this.connectionRepository.saveSocialConnection(userData, userId.longValue(), email);
        // Recording metrics for linking Google account
        logger.info("Enqueue ing user id in google web queue - " + userId);

        try {
            this.metricsClient.recordEvent("Social", "google", "link_account");
        } catch (Exception e) {
            //Extra cautious catch all block to prevent the stalling of the critical code path in case of some exception.
            logger.error("Exception while trying to enqueue google linked user id - " + userId, e);
        }

    }

    public void updateConnection(SocialUserData data, String existingId) {
        this.connectionRepository.updateSocialConnection(existingId, "google", data.accessToken,
                data.expireTime);
    }

}
