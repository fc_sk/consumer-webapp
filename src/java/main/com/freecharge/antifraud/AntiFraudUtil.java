package com.freecharge.antifraud;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freecharge.app.domain.dao.UsersDAO;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.google.common.collect.ImmutableSet;

@Service
public class AntiFraudUtil {
	
	@Autowired
	private AntiFraudDAO antiFraudDao;
	
	@Autowired
	private UserServiceProxy userServiceProxy;
	
    private static final long ONE_HOUR_MILLIS = 60 * 60 * 1000L;
    private static final long ONE_DAY_MILLIS  = 24 * ONE_HOUR_MILLIS;

	private static Logger logger = LoggingFactory.getLogger(AntiFraudUtil.class);
	
	public static final Set<String> SUCCESS_OR_UNDERPROCESS_RECHARGES = ImmutableSet.of("0", "00", "08");
	
	/**
	 * Return the time when the given user was registered.
	 * @return
	 */
	public long getUserRegistrationTimestamp(int userId) {
		com.freecharge.app.domain.entity.jdbc.Users u = userServiceProxy.getUserByUserId(userId);
		if (u == null) {
			logger.warn("Users object retrieved from database is null for userId " + userId);
			return -1;
		}
		Timestamp ts = u.getDateAdded();
		if (ts != null) {
			return ts.getTime();
		}
		logger.warn("The `dateAdded` column for user ID " + userId + " is null!");
		return -1;
	}
	
	    /**
     * Return total amount of successful mobile recharge for given user and time
     * range. V = Mobile, D = DTH, C = Data Card, M = Post paid
     * 
     * @return
     */
    public Amount cumulativeProductRechargeAmount(List<UserTransactionHistory> userTransactionHistories,
            Timestamp beginTimestamp, String product) {
        Double cumulativeMobileRecharge = 0.0;

        for (UserTransactionHistory userTransactionHistory : userTransactionHistories) {
            Timestamp createdTimestamp = userTransactionHistory.getCreatedOn();
            String productType = userTransactionHistory.getProductType();
            String txnStatus = userTransactionHistory.getTransactionStatus();
            if (createdTimestamp.after(beginTimestamp) && product.equals(productType)
                    && SUCCESS_OR_UNDERPROCESS_RECHARGES.contains(txnStatus)) {
                cumulativeMobileRecharge += userTransactionHistory.getTransactionAmount();
            }
        }
        return new Amount(cumulativeMobileRecharge);
    }
	
    public Long cumulativeAddCashVelocity(Timestamp beginTimestamp, List<UserTransactionHistory> userTransactionHistories) {
        long count = 0;
        for (UserTransactionHistory userTransactionHistory : userTransactionHistories) {
            Timestamp createdTimestamp = userTransactionHistory.getCreatedOn();
            String txnStatus = userTransactionHistory.getTransactionStatus();
            String productType = userTransactionHistory.getProductType();
            if (createdTimestamp.after(beginTimestamp) && SUCCESS_OR_UNDERPROCESS_RECHARGES.contains(txnStatus) && FCConstants.WALLET_FUND_PRODUCT_TYPE.equals(productType)) {
                count += 1;
            }
        }
        return count;
    }
    
    public Long cumulativeCreditsPay(int userId, long beginTimestamp, long endTimestamp) {
        return antiFraudDao.cumulativeCreditsPaymentCount(userId, new Timestamp(beginTimestamp), new Timestamp(endTimestamp));
    }
    
	    /**
     * Return the count of successful recharges for given user and time range,
     * regardless of the product/type of recharge.
     * 
     * @return
     */
    public int rechargeCount(List<UserTransactionHistory> userTransactionHistories, Timestamp beginTimestamp) {
        int count = 0;

        for (UserTransactionHistory userTransactionHistory : userTransactionHistories) {
            Timestamp createdTimestamp = userTransactionHistory.getCreatedOn();
            String txnStatus = userTransactionHistory.getTransactionStatus();
            if (createdTimestamp.after(beginTimestamp) && SUCCESS_OR_UNDERPROCESS_RECHARGES.contains(txnStatus)) {
                count += 1;
            }
        }
        return count;
    }

	@Transactional
	public int newRegisteredUsers(long beginTimestamp) {
        return antiFraudDao.newRegisteredUsers(new Timestamp(beginTimestamp));
    }
	
	@Transactional
    public int successfulTransactInLast24Hours(long beginTimestamp) {
        return antiFraudDao.successfulTransactInLast24Hours(new Timestamp(beginTimestamp));
    }
	@Transactional
    public int avgSuccessTxnTicketSize(long beginTimestamp) {
        return antiFraudDao.avgSuccessTxnTicketSize(new Timestamp(beginTimestamp));
    }
	@Transactional
    public int avgSuccessMobTxnTicketSize(long beginTimestamp) {
        return antiFraudDao.avgSuccessTxnTicketSize(new Timestamp(beginTimestamp));
    }
	@Transactional
    public int avgSuccessDthTxnTicketSize(long beginTimestamp) {
        return antiFraudDao.avgSuccessTxnTicketSize(new Timestamp(beginTimestamp));
    }
    
    public static final long beginningOfLastDays(int n) {
        return System.currentTimeMillis() - (n * ONE_DAY_MILLIS);
    }
    
    public static final long beginningOfLastHours(int n) {
        return System.currentTimeMillis() - (n * ONE_HOUR_MILLIS);
    }
}
