package com.freecharge.antifraud;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.antifraud.FraudDetection.FraudType;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;

public class FraudDetectionSchedular {
    private static Logger logger  = LoggingFactory.getLogger(FraudDetectionSchedular.class);

    @Autowired
    AntiFraudUtil afu;

    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
    private AppConfigService appConfig;

    @Autowired
    EmailService emailService;

    @Autowired
    private FraudDetection fd;

    private static final long ONE_HOUR_MILLIS = 60 * 60 * 1000L;
    private static final long THIRTY_MINUTES_MILLIS = 60 * 1000L;
    private static final long ONE_DAY_MILLIS = 24 * ONE_HOUR_MILLIS;

    public long beginningOfLastDays(int n) {
        return System.currentTimeMillis() - (n * ONE_HOUR_MILLIS);
    }

    public long beginningOfLast30Minutes(int n) {
        return System.currentTimeMillis() - (n * THIRTY_MINUTES_MILLIS);
    }

    public void fraudRegisteredTxn() {
        boolean isEnabled = fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUD_REGISTERED_TO_TXN_ENABLED);  
                
        String benchmarkParcentage = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD_REGISTERED_TO_TXN_BENCHMARK_PERCENT);
        String periodHours = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD_REGISTERED_TO_TXN_PERIOD_HOURS);
        String emailSub = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD_REGISTERED_TO_TXN_EMAIL_SUB);
        try {
            if (isEnabled) {
                Integer newRegisteredUsers = afu.newRegisteredUsers(beginningOfLastDays(Integer.parseInt(periodHours)));
                Integer successfulTransactInLast24Hour = afu.successfulTransactInLast24Hours(beginningOfLastDays(Integer.parseInt(periodHours)));
                Integer successRatio = (successfulTransactInLast24Hour*100 / newRegisteredUsers);
                if (successRatio > Integer.parseInt(benchmarkParcentage)) {
                    emailSub = emailSub + "[" + newRegisteredUsers + "Regns" 
                + ", " + successfulTransactInLast24Hour + " Txn, " 
                + (successfulTransactInLast24Hour* 100 / newRegisteredUsers)  + "% ]";
                    sendEmail(FraudType.FRAUD_REGISTERED_TO_TXN, emailSub);
                }
            }
        } catch (Exception e) {
            logger.info("Exception thrown");
        }
    }

    public void fraudTicketSize() {
        boolean isEnabled = fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUD_TICKET_SIZE_TXN_ENABLED);
        String benchmarkAverage = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD_TICKET_SIZE_TXN_BENCHMARK_AVERAGE);
        String periodMinites = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD_TICKET_SIZE_TXN_PERIOD_MINUTES);
        String emailSub = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD_TICKET_SIZE_TXN_EMAIL_SUB);
        try {
            if (isEnabled) {
                Integer avgTicketSize = afu.avgSuccessTxnTicketSize(beginningOfLast30Minutes(Integer.parseInt(periodMinites)));
                if (avgTicketSize > Integer.parseInt(benchmarkAverage)) {
                    Integer avgMobTicketSize = afu.avgSuccessMobTxnTicketSize(beginningOfLast30Minutes(Integer.parseInt(periodMinites)));
                    Integer avgDthTicketSize = afu.avgSuccessDthTxnTicketSize(beginningOfLast30Minutes(Integer.parseInt(periodMinites)));
                    emailSub = emailSub + "[ " + avgTicketSize + ", M" + avgMobTicketSize + ", D" + avgDthTicketSize + "] ";
                    sendEmail(FraudType.FRAUD_TICKET_SIZE_LIMIT, emailSub);
                }
            }
        } catch (Exception e) {
        }
    }

    public void sendEmail(FraudType scheme, String emailSub) {
        EmailBusinessDO ebdo = new EmailBusinessDO();
        ebdo.setFrom(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_EMAIL_NOTIFICATION_FROM));
        ebdo.setReplyTo(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_EMAIL_NOTIFICATION_FROM));
        ebdo.setTo(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_EMAIL_NOTIFICATION_TO));
        ebdo.setSubject(emailSub);
        emailService.sendNonVelocityEmail(ebdo);
    }
}
