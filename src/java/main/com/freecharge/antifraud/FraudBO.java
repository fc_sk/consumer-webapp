package com.freecharge.antifraud;

import com.freecharge.antifraud.FraudDetection.FraudType;

public class FraudBO {
    private FraudType fraudType;
    private String fraudCondn;
    private boolean isFraud;
    
    public FraudBO(){}
    
    public FraudBO(FraudType fraudType, String fraudCondn, boolean isFraud){
        this.fraudType = fraudType;
        this.fraudCondn = fraudCondn;
        this.isFraud = isFraud;
    }
    
    public FraudType getFraudType() {
        return fraudType;
    }
    public void setFraudType(FraudType fraudType) {
        this.fraudType = fraudType;
    }
    public String getFraudCondn() {
        return fraudCondn;
    }
    public void setFraudCondn(String fraudCondn) {
        this.fraudCondn = fraudCondn;
    }
    public boolean isFraud() {
        return isFraud;
    }
    public void setFraud(boolean isFraud) {
        this.isFraud = isFraud;
    }
}
