package com.freecharge.antifraud;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class AntiFraudDAO {
	public static final String MOBILE = "V", DTH = "D", DATA_CARD = "C", POST_PAID="M", ADD_CASH="T";
	
	private static final String sqlRechargeAmountByLookupID =
	        "SELECT thp.product_type, thp.amount FROM txn_home_page thp " +
	        " where thp.lookup_id = :lookup_id";
	
	private static final String sqlNewRegisteredUsersCount = 
	        "select count(user_id) from users where date_added >= :begin_ts";
	
	private static final String sqlNewTransactUserIn24hours = 
            "select count(user_id) from users user "
	        + "  JOIN txn_fulfilment txn ON user.email = txn.email " 
            + "  where user.date_added >= :begin_ts and txn.transaction_status = 1 limit 1";
	
	private static final String sqlSuccessTxnTicketSize = 
            "select sum(irq.amount)/count(1) from users user "
            + "  JOIN txn_fulfilment txn ON user.email = txn.email "
            + "  JOIN in_request irq ON txn.order_id = irq.affiliate_trans_id"
            + "  where user.date_added >= :begin_ts and txn.transaction_status = 1";
	
    private static final String sqlCreditsPaymentCount = "select count(distinct pt.order_id) as txn_count from "
                                                            + "payment_txn as pt JOIN wallet_txn as wt "
                                                            + "ON wt.ORDER_ID = pt.order_id "
                                                            + "JOIN payment_rq as prq ON prq.order_id=pt.order_id "
                                                            + "JOIN in_request as irq ON pt.order_id=irq.affiliate_trans_id "
                                                            + "JOIN in_response as irs ON irs.request_id=irq.request_id "
                                                            + "where prq.payment_type=6 AND wt.FK_USER_ID=:user_id " 
                                                            + "AND pt.payment_gateway='fw' "
                                                            + "AND (irs.in_resp_code = '00' or irs.in_resp_code='0' or irs.in_resp_code='08') "
                                                            + "AND pt.is_successful=1 "
                                                            + "AND wt.transaction_date >= :begin_ts "
                                                            + "AND wt.transaction_date < :end_ts ";

	private NamedParameterJdbcTemplate jt;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		jt = new NamedParameterJdbcTemplate(dataSource);
	}
	
    public List<Map<String,Object>> findRechargeAmountbyLookUpID(String lookupID) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("lookup_id", lookupID);
        return jt.queryForList(sqlRechargeAmountByLookupID, paramMap);
    }
    
    public Long cumulativeCreditsPaymentCount(int userId, Timestamp begin, Timestamp end) {
        Map<String, Object> paramSource = new HashMap<String, Object>();
        paramSource.put("begin_ts", begin);
        paramSource.put("end_ts", end);
        paramSource.put("user_id", userId);
        List<Map<String, Object>> result = jt.queryForList(sqlCreditsPaymentCount, paramSource);
        Map<String, Object> map = result.size() > 0 ? result.get(0) : new HashMap<String, Object>();
        Object txnCount = map.get("txn_count");
        return (Long) (txnCount==null? 0: txnCount);
    }
    
	   public int newRegisteredUsers(Timestamp begin) {
	        Map<String, Object> paramSource = new HashMap<String, Object>();
	        paramSource.put("begin_ts", begin);
	        int result = jt.queryForInt(sqlNewRegisteredUsersCount, paramSource);
	        return result;
	    }
	    
	    public int successfulTransactInLast24Hours(Timestamp begin) {
            Map<String, Object> paramSource = new HashMap<String, Object>();
            paramSource.put("begin_ts", begin);
            int result = jt.queryForInt(sqlNewTransactUserIn24hours, paramSource);
            return result;
        }
	    
	    public int avgSuccessTxnTicketSize(Timestamp begin) {
            Map<String, Object> paramSource = new HashMap<String, Object>();
            paramSource.put("begin_ts", begin);
            int result = jt.queryForInt(sqlSuccessTxnTicketSize, paramSource);
            return result;
        }
}
