package com.freecharge.antifraud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.common.cache.CacheManager;

@Component
public class FraudLimitCacheManager extends CacheManager {

    private static String FRAUD_LIMIT_KEY_PREFIX = "fraudLimits";

    @Autowired
    private FraudLimitDAO fraudLimitDAO;

    @SuppressWarnings("unchecked")
    public List<FraudLimit> getTransactionAmountLimits(String productType) {
        List<FraudLimit> fraudLimits = (List<FraudLimit>) get(productType + "_" + FRAUD_LIMIT_KEY_PREFIX);
        if (fraudLimits == null || fraudLimits.size() == 0) {
            int productMasterId = ProductMaster.ProductName.fromCode(productType).getProductId();
            fraudLimits = fraudLimitDAO.getActiveFraudLimits(productMasterId);
            set(productType + "_" + FRAUD_LIMIT_KEY_PREFIX, fraudLimits);
        }
        return fraudLimits == null ? new ArrayList<FraudLimit>() : fraudLimits;
    }

}
