package com.freecharge.antifraud;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;

@Repository
public class FraudLimitDAO {

    private NamedParameterJdbcTemplate jt;

    private static final String fraudProductAmtLimitSQL = "SELECT pfl.fraud_limit_id as id,fcactage.number_of_days as fcAcctAgeInDays,fcactage.number_of_hours as fcAcctAgeInHours,fcactage.number_of_minutes as fcAcctAgeInMinutes,fcactage.description as fcActAgeDescription,"
            + "rwdm.number_of_days as rollingWindowDays,rwdm.number_of_hours as rollingWindowHours,rwdm.number_of_minutes as rollingWindowMinutes,rwdm.description as rollingWindowDescription,"
            + "max_amount as maxAmount,max_txn_count as maxTxnCount FROM product_fraud_limit pfl left join duration_master rwdm on rwdm.duration_id = pfl.fk_rolling_window_id "
            + "left join  duration_master fcactage on fcactage.duration_id = pfl.fk_fc_acct_age_id where fk_product_master_id = :product_master_id";

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<FraudLimit> getActiveFraudLimits(final int productMasterId) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("product_master_id", productMasterId);
        return jt.query(fraudProductAmtLimitSQL, paramMap, new RowMapper<FraudLimit>() {
            @Override
            public FraudLimit mapRow(ResultSet rs, int rowNumber) throws SQLException {
                FraudLimit fraudLimit = new FraudLimit();
                fraudLimit.setId(rs.getInt("id"));
                fraudLimit.setProductType(ProductName.fromProductId(productMasterId).getLabel());
                fraudLimit.setMaxAmount(rs.getDouble("maxAmount"));
                fraudLimit.setMaxTxnCount(rs.getInt("maxTxnCount"));
                fraudLimit.setRollingWindowDays(rs.getInt("rollingWindowDays"));
                fraudLimit.setRollingWindowHours(rs.getInt("rollingWindowHours"));
                fraudLimit.setRollingWindowMinutes(rs.getInt("rollingWindowMinutes"));
                fraudLimit.setFcAcctAgeInDays(rs.getInt("fcAcctAgeInDays"));
                fraudLimit.setFcAcctAgeInHours(rs.getInt("fcAcctAgeInHours"));
                fraudLimit.setFcAcctAgeInMinutes(rs.getInt("fcAcctAgeInMinutes"));
                return fraudLimit;
            }
        });
    }

}
