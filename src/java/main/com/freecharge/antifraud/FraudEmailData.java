package com.freecharge.antifraud;

import java.util.List;

import com.freecharge.app.domain.entity.UserTransactionHistory;

public class FraudEmailData {
    private int userID;
    private FraudBO fraudBO;
    private String productType;
    private String rechargeAmount;
    private String totalAmount; 
    private List<String> duplicateUserIds = null;
    private String deviceId;
    private List<UserTransactionHistory> userTransactionHistories;
    private String lookupId;
    private int channelId;
    private String orderId;
    
    public FraudEmailData(int userID, String productType, String rechargeAmount, String totalAmount, String deviceId, List<UserTransactionHistory> userTransactionHistories, String lookupId, int channelId, String orderId) {
        super();
        this.userID = userID;
        this.productType = productType;
        this.rechargeAmount = rechargeAmount;
        this.totalAmount = totalAmount;
        this.deviceId = deviceId;
        this.userTransactionHistories = userTransactionHistories;
        this.lookupId = lookupId;
        this.channelId = channelId;
        this.setOrderId(orderId);
    }

    public int getUserID() {
        return userID;
    }
    public void setUserID(int userID) {
        this.userID = userID;
    }
    public FraudBO getFraudBO() {
        return fraudBO;
    }
    public void setFraudBO(FraudBO fraudBO) {
        this.fraudBO = fraudBO;
    }
    public String getProductType() {
        return productType;
    }
    public void setProductType(String productType) {
        this.productType = productType;
    }
    public String getRechargeAmount() {
        return rechargeAmount;
    }
    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }
    public String getTotalAmount() {
        return totalAmount;
    }
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
    public List<String> getDuplicateUserIds() {
        return duplicateUserIds;
    }
    public void setDuplicateUserIds(List<String> duplicateUserIds) {
        this.duplicateUserIds = duplicateUserIds;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<UserTransactionHistory> getUserTransactionHistories() {
        return userTransactionHistories;
    }

    public void setUserTransactionHistories(List<UserTransactionHistory> userTransactionHistories) {
        this.userTransactionHistories = userTransactionHistories;
    }

    public String getLookupId() {
        return lookupId;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
