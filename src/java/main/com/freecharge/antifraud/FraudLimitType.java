package com.freecharge.antifraud;

public enum FraudLimitType {
    
    PROD_RLNG_WNDW_AMT_LIMIT,
    PROD_USR_PRF_AGE_TXN_CNT_LIMIT

}
