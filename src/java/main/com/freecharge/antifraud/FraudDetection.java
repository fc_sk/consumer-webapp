package com.freecharge.antifraud;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.Users;
import com.freecharge.app.service.TxnHomePageService;
import com.freecharge.app.service.exception.FraudDetectedException;
import com.freecharge.common.businessdo.ProductPaymentBusinessDO;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.beans.BillPayValidator;
import com.freecharge.infrastructure.billpay.types.BillWithSubscriberNumber;
import com.freecharge.mobile.web.util.MobileUtil;
import com.freecharge.mongo.repos.PaymentHistoryRepository;
import com.freecharge.payment.dos.entity.DeviceFingerPrint;
import com.freecharge.payment.services.DeviceFingerPrintManager;
import com.freecharge.payment.services.FraudWhitelistService;
import com.freecharge.payment.util.BilldeskCardHash;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.wallet.WalletWrapper;

@Service
public class FraudDetection {
    private Logger                        logger = LoggingFactory.getLogger(getClass());
    @Autowired
    AntiFraudUtil                         afu;

    @Autowired
    private FCProperties                  fcProperties;

    @Autowired
    private EmailService                  emailService;

    @Autowired
    private MetricsClient                 metricsClient;

    @Autowired
    private WalletWrapper                 walletWrapper;

    @Autowired
    private AntiFraudDAO                  afDAO;

    @Autowired
    private FraudWhitelistService         fraudWhitelistService;

    @Autowired
    private UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    private TxnHomePageService            txnHomePageService;

    @Autowired
    private OrderIdReadDAO                orderIdReadDAO;

    @Autowired
    private DeviceFingerPrintManager      deviceFingerPrintManager;

    @Autowired
    private FraudLimitCacheManager        fraudLimitCacheManager;

    @Autowired
    private PaymentHistoryRepository      paymentHistoryRepository;
    
    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDao;
    
    @Autowired
    @Qualifier("billPayServiceProxy")
    private IBillPayService billPayServiceRemote;
    
    @Autowired
    private UserServiceProxy userServiceProxy;
    
    
    public void checkFraud(int userId, String lookupId, String totalAmount, String paymentType, String deviceId,
            int channelId, String orderId) throws FraudDetectedException {
        List<Map<String, Object>> rechargeDetails = afDAO.findRechargeAmountbyLookUpID(lookupId);
        logger.debug("size of list of recharge deatils in Checkout service " + rechargeDetails.size());
        if (rechargeDetails.size() > 0 && !fraudWhitelistService.isWhitelisted(userId)) {
            Map<String, Object> listOfRechargeDetails = rechargeDetails.get(0);
            String rechargeAmount = "" + listOfRechargeDetails.get("amount");
            String productType = "" + listOfRechargeDetails.get("product_type");
            if (FCUtil.isUtilityPaymentType(productType)) {
                doFraudCheckForAdhocUtilityPayment(userId, lookupId, totalAmount, deviceId, channelId, orderId,
                        rechargeAmount, productType);
                logger.info("Ignore fraud checks for Utility bill payments for lookupId : " + lookupId);
                return;
            }
            long reg = afu.getUserRegistrationTimestamp(userId);

            /*
             * 1. Check if the user has registered within 90 days. 2. If YES,
             * then fetch the user_transaction_history data for this user. 3.
             * Pass this data to different methods to process different fraud
             * rules.
             */
            String fraudApplyMaxBenchmarkDays =
                    fcProperties.getAntiFraudProperty(FCConstants.ANTI_FRAUD_APPLY_BENCHMARK_DAYS);
            long fraudApplyMaxBenchmarkDayMillis =
                    AntiFraudUtil.beginningOfLastDays(Integer.parseInt(fraudApplyMaxBenchmarkDays));
            if (reg > fraudApplyMaxBenchmarkDayMillis) {
                List<UserTransactionHistory> userTransactionHistories =
                        userTransactionHistoryService.findAllUserTransactionHistoryByUserId(userId);

                FraudEmailData fraudEmailData = new FraudEmailData(userId, productType, rechargeAmount, totalAmount,
                        deviceId, userTransactionHistories, lookupId, channelId, orderId);

                FraudBO fraudBO = checkFraudFresh(reg, userTransactionHistories);
                if (!productType.equals(FCConstants.WALLET_FUND_PRODUCT_TYPE) && fraudBO.isFraud()) {
                    fraudEmailData.setFraudBO(fraudBO);
                    sendEmail(fraudEmailData);
                    throw new FraudDetectedException("User ID : " + userId + " " + fraudBO.getFraudType());
                }

                fraudBO = checkFraudNew3000(reg, userTransactionHistories);
                if (!productType.equals(FCConstants.WALLET_FUND_PRODUCT_TYPE) && fraudBO.isFraud()) {
                    fraudEmailData.setFraudBO(fraudBO);
                    sendEmail(fraudEmailData);
                    throw new FraudDetectedException("User ID : " + userId + " " + fraudBO.getFraudType());
                }

                fraudBO = checkFraud5000(reg, userTransactionHistories);
                if (!productType.equals(FCConstants.WALLET_FUND_PRODUCT_TYPE) && fraudBO.isFraud()) {
                    fraudEmailData.setFraudBO(fraudBO);
                    sendEmail(fraudEmailData);
                    throw new FraudDetectedException("User ID : " + userId + " " + fraudBO.getFraudType());
                }

                // TODO:
                fraudBO = checkFraudCreditsPay(userId, reg);
                if (!productType.equals(FCConstants.WALLET_FUND_PRODUCT_TYPE)
                        && paymentType.equals(PaymentConstants.PAYMENT_TYPE_WALLET) && fraudBO.isFraud()) {
                    fraudEmailData.setFraudBO(fraudBO);
                    sendEmail(fraudEmailData);
                    throw new FraudDetectedException("User ID : " + userId + " " + fraudBO.getFraudType());
                }

                fraudBO = checkFraudAddCashAmount(totalAmount, reg);
                if (productType.equals(FCConstants.WALLET_FUND_PRODUCT_TYPE) && fraudBO.isFraud()) {
                    fraudEmailData.setFraudBO(fraudBO);
                    sendEmail(fraudEmailData);
                    throw new FraudDetectedException("User ID : " + userId + " " + fraudBO.getFraudType());
                }

                fraudBO = checkFraudAddCashVelocity(totalAmount, reg, userTransactionHistories);
                if (productType.equals(FCConstants.WALLET_FUND_PRODUCT_TYPE) && fraudBO.isFraud()) {
                    fraudEmailData.setFraudBO(fraudBO);
                    sendEmail(fraudEmailData);
                    throw new FraudDetectedException("User ID : " + userId + " " + fraudBO.getFraudType());
                }
            }

            // For an Add cash Transaction create a dynamo entry with key as
            // device id and data as user id.
            boolean isFraudImeiAddCashEnabled =
                    Boolean.parseBoolean(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_ADDCASH_IMEI_ENABLED));
            boolean isFraudDeviceFingerPrintAddCashEnabled = Boolean.parseBoolean(
                    fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_ADDCASH_DEVICE_FINGERPRINT_ENABLED));
            if (ProductMaster.ProductName.fromPrimaryProductType(productType) == ProductName.WalletCash
                    && (isFraudImeiAddCashEnabled || isFraudDeviceFingerPrintAddCashEnabled)) {
                DeviceFingerPrint deviceFingerPrint = getDeviceFingerPrint(deviceId, userId);
                List<String> userIdList = deviceFingerPrintManager.getUserIdsForDeviceFingerPrint(deviceFingerPrint);
                FraudBO fraudBO = handleAddCashDeviceFraudCheck(deviceFingerPrint, channelId, userIdList,
                        isFraudImeiAddCashEnabled, isFraudDeviceFingerPrintAddCashEnabled);
                if (fraudBO.isFraud()) {
                    List<UserTransactionHistory> userTransactionHistories =
                            userTransactionHistoryService.findAllUserTransactionHistoryByUserId(userId);
                    FraudEmailData fraudEmailData = new FraudEmailData(userId, productType, rechargeAmount, totalAmount,
                            deviceId, userTransactionHistories, lookupId, channelId, orderId);
                    fraudEmailData.setFraudBO(fraudBO);
                    fraudEmailData.setDuplicateUserIds(userIdList);
                    sendEmail(fraudEmailData);
                    throw new FraudDetectedException(
                            "User ID : " + userId + " " + fraudBO.getFraudType() + " " + deviceId);
                }
            }

            FraudBO fraudBO = checkFraudAmount(productType, rechargeAmount);
            if (fraudBO.isFraud()) {
                List<UserTransactionHistory> userTransactionHistories =
                        userTransactionHistoryService.findAllUserTransactionHistoryByUserId(userId);
                FraudEmailData fraudEmailData = new FraudEmailData(userId, productType, rechargeAmount, totalAmount,
                        deviceId, userTransactionHistories, lookupId, channelId, orderId);
                fraudEmailData.setFraudBO(fraudBO);
                sendEmail(fraudEmailData);
                throw new FraudDetectedException("User ID : " + userId + " " + fraudBO.getFraudType());
            }
        }
    }

    private void doFraudCheckForAdhocUtilityPayment(int userId, String lookupId, String totalAmount, String deviceId,
            int channelId, String orderId, String rechargeAmount, String productType) throws FraudDetectedException {
        try {
            String billId = orderIdSlaveDao.getBillIdForOrderId(orderId, productType);
            BillWithSubscriberNumber bill = billPayServiceRemote.getBillDetails(billId);
            BillPayValidator validator = billPayServiceRemote.getBillPayValidatorForOperator(bill.getOperatorId(),
                    bill.getCircleId());
            if (validator.getBillerType() == BillPaymentService.ADHOC_BILLER_TYPE
                    && new BigDecimal(totalAmount).compareTo(new BigDecimal("10000")) >= 0) {
                List<UserTransactionHistory> userTransactionHistories = userTransactionHistoryService
                        .findAllUserTransactionHistoryByUserId(userId);

                FraudEmailData fraudEmailData = new FraudEmailData(userId, productType, rechargeAmount, totalAmount,
                        deviceId, userTransactionHistories, lookupId, channelId, orderId);
                sendEmail(fraudEmailData);
                throw new FraudDetectedException("User ID : " + userId + " " + FraudType.FRAUD_ADHOC_UTILITY_LIMIT);
            }
        } catch (Exception ex) {
            logger.error("Error doing fraud check for adhoc utility bill payment.", ex);
        }
    }

    private FraudBO handleAddCashDeviceFraudCheck(DeviceFingerPrint deviceFingerPrint, int channelId,
            List<String> userIdList, boolean isFraudImeiAddCashEnabled,
            boolean isFraudDeviceFingerPrintAddCashEnabled) {
        FraudBO fraudBO = new FraudBO();
        Integer threshold = MobileUtil.isAppChannel(channelId)
                ? fcProperties.getAntifraudIntProperty(FCConstants.ANTIFRAUD_ADDCASH_IMEI_MAX_USERS)
                : fcProperties.getAntifraudIntProperty(FCConstants.ANTIFRAUD_ADDCASH_DEVICE_FINGERPRINT_MAX_USERS);
        if (userIdList.size() < threshold) {
            deviceFingerPrintManager.saveUsersForFingerPrint(deviceFingerPrint, userIdList);
        }
        if (!FCUtil.isEmpty(userIdList) && userIdList.size() == threshold
                && !userIdList.contains(deviceFingerPrint.getUserId())) {
            if (MobileUtil.isAppChannel(channelId) && isFraudImeiAddCashEnabled) {
                fraudBO.setFraudType(FraudType.FRAUD_DEVICE_APP);
                fraudBO.setFraudCondn("userIdList size for deviceFingerPrint(userId:" + deviceFingerPrint.getUserId()
                        + ", deviceId: " + deviceFingerPrint.getDeviceFingerPrint() + "): " + userIdList.size()
                        + " > imeiMaxUsers: " + threshold);
                fraudBO.setFraud(true);
            } else if (!MobileUtil.isAppChannel(channelId) && isFraudDeviceFingerPrintAddCashEnabled) {
                fraudBO.setFraudType(FraudType.FRAUD_DEVICE_LAPTOP);
                fraudBO.setFraudCondn("userIdList size for deviceFingerPrint(userId:" + deviceFingerPrint.getUserId()
                        + ", deviceId: " + deviceFingerPrint.getDeviceFingerPrint() + "): " + userIdList.size()
                        + " > deviceFingerPrintMaxUsers: " + threshold);
                fraudBO.setFraud(true);
            }
        }
        return fraudBO;
    }

    private DeviceFingerPrint getDeviceFingerPrint(String deviceId, int userId) {
        DeviceFingerPrint deviceFingerPrint = new DeviceFingerPrint();
        String userIdStr = String.valueOf(userId);
        deviceFingerPrint.setDeviceFingerPrint(deviceId);
        deviceFingerPrint.setUserId(userIdStr);
        return deviceFingerPrint;
    }

    private FraudBO checkFraudAmount(final String productType, String strAmount) {
        FraudBO fraudBO = new FraudBO();
        double amt = Double.parseDouble(strAmount);
        if (amt <= 0) {
            return fraudBO;
        }
        char prod = productType.trim().toUpperCase().charAt(0);
        double mobileMax = Double.parseDouble(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_AMOUNT_MOBILE));
        double dthMax = Double.parseDouble(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_AMOUNT_DTH));
        double datacardMax =
                Double.parseDouble(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_AMOUNT_DATACARD));
        switch (prod) {
            case 'V':
                if (amt > mobileMax) {
                    fraudBO.setFraudType(FraudType.FRAUD_AMOUNT);
                    fraudBO.setFraud(true);
                    fraudBO.setFraudCondn("amt:" + amt + " > mobileMax:" + mobileMax);
                    return fraudBO;
                }
                break;
            case 'D':
                if (amt > dthMax) {
                    fraudBO.setFraudType(FraudType.FRAUD_AMOUNT);
                    fraudBO.setFraud(true);
                    fraudBO.setFraudCondn("amt:" + amt + " > dthMax:" + dthMax);
                    return fraudBO;
                }
                break;
            case 'C':
                if (amt > datacardMax) {
                    fraudBO.setFraudType(FraudType.FRAUD_AMOUNT);
                    fraudBO.setFraud(true);
                    fraudBO.setFraudCondn("amt:" + amt + " > datacardMax:" + datacardMax);
                    return fraudBO;
                }
                break;
            default:
                if (amt > Math.max(mobileMax, Math.max(dthMax, datacardMax))) {
                    fraudBO.setFraudType(FraudType.FRAUD_AMOUNT);
                    fraudBO.setFraud(true);
                    fraudBO.setFraudCondn("default block reached - amt:" + amt + " > mobileMax:" + mobileMax
                            + "|dthMax:" + dthMax + "|datacardMax:" + datacardMax);
                    return fraudBO;
                }
        }
        return fraudBO;
    }

    private FraudBO checkFraudAddCashAmount(String strAmount) {
        FraudBO fraudBO = new FraudBO();
        double amt = Double.parseDouble(strAmount);
        if (amt <= 0) {
            return fraudBO;
        }
        double addCashMax =
                Double.parseDouble(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_ADDCASH_AMOUNT_MAX_AMT));
        if (amt > addCashMax) {
            fraudBO.setFraud(true);
            fraudBO.setFraudType(FraudType.FRAUD_ADD_CASH_AMOUNT);
            fraudBO.setFraudCondn("amt:" + amt + " > addCashMax:" + addCashMax);
        }
        return fraudBO;
    }

    private FraudBO isFraudGenericCheck(long benchmarkBeginMillis, long periodBeginMillis, long reg,
            List<UserTransactionHistory> userTransactionHistories, String mobileMinAmt, String dthMinAmt,
            String postPaidMinAmt) {
        FraudBO fraudBO = new FraudBO();
        // is registered in benchmark?
        if (!(reg >= benchmarkBeginMillis)) {
            return fraudBO;
        }

        BigDecimal mobAmt = afu.cumulativeProductRechargeAmount(userTransactionHistories,
                new Timestamp(periodBeginMillis), FCConstants.PRODUCT_TYPE_MOBILE).getAmount();
        // is mobile recharge amount in limit?
        if (mobAmt.compareTo(new BigDecimal(mobileMinAmt)) >= 0) {
            fraudBO.setFraud(true);
            fraudBO.setFraudCondn("cumulativeMobAmt:" + mobAmt + " > mobileMinAmt:" + mobileMinAmt);
            return fraudBO;
        }

        BigDecimal dthAmt = afu.cumulativeProductRechargeAmount(userTransactionHistories,
                new Timestamp(periodBeginMillis), FCConstants.PRODUCT_TYPE_DTH).getAmount();
        // is DTH recharge amount in limit?
        if (dthAmt.compareTo(new BigDecimal(dthMinAmt)) >= 0) {
            fraudBO.setFraud(true);
            fraudBO.setFraudCondn("cumulativeDTHAmt:" + dthAmt + " > DTHMinAmt:" + dthMinAmt);
            return fraudBO;
        }
        BigDecimal postPaidAmt = afu.cumulativeProductRechargeAmount(userTransactionHistories,
                new Timestamp(periodBeginMillis), FCConstants.PRODUCT_TYPE_POSTPAID).getAmount();

        if (postPaidAmt.compareTo(new BigDecimal(postPaidMinAmt)) >= 0) {
            fraudBO.setFraud(true);
            fraudBO.setFraudCondn("cumulativePostPaidAmt:" + postPaidAmt + " > postPaidMinAmt:" + postPaidMinAmt);
            return fraudBO;
        }

        return fraudBO;
    }

    private FraudBO checkFraudFresh(long reg, List<UserTransactionHistory> userTransactionHistories) {
        FraudBO fraudBO = new FraudBO();
        Boolean isEnabled =
                Boolean.parseBoolean(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_ENABLED));
        String benchmarkDays = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_BENCHMARK_DAYS);
        String periodHours = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_PERIOD_HOURS);
        String mobileMinAmt = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_MOBILE_MIN_AMT);
        String dthMinAmt = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_DTH_MIN_AMT);
        String postPaidMinAmt = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_POSTPAID_MIN_AMT);
        String rechargeCount = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_RECHARGE_COUNT);

        if (isEnabled) {
            fraudBO = isFraudGenericCheck(AntiFraudUtil.beginningOfLastDays(Integer.parseInt(benchmarkDays)),
                    AntiFraudUtil.beginningOfLastHours(Integer.parseInt(periodHours)), reg, userTransactionHistories,
                    mobileMinAmt, dthMinAmt, postPaidMinAmt);
            fraudBO.setFraudType(FraudType.FRAUD_FRESH);
            if (!fraudBO.isFraud()) {
                int userRechargeCount = afu.rechargeCount(userTransactionHistories,
                        new Timestamp(AntiFraudUtil.beginningOfLastHours(Integer.parseInt(periodHours))));
                if (userRechargeCount >= Integer.parseInt(rechargeCount)) {
                    fraudBO.setFraud(true);
                    fraudBO.setFraudCondn(
                            "userRechargeCount:" + userRechargeCount + " > rechargeCount:" + rechargeCount);
                }
            }
        }
        return fraudBO;
    }

    private FraudBO checkFraudNew3000(long reg, List<UserTransactionHistory> userTransactionHistories) {
        FraudBO fraudBO = new FraudBO();
        Boolean isEnabled =
                Boolean.parseBoolean(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD3000_ENABLED));

        String benchmarkDays = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD3000_BENCHMARK_DAYS);
        String periodHours = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD3000_PERIOD_HOURS);
        String mobileMinAmt = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD3000_MOBILE_MIN_AMT);
        String dthMinAmt = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD3000_DTH_MIN_AMT);
        String postPaidMinAmt = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD3000_POSTPAID_MIN_AMT);

        if (isEnabled) {
            fraudBO = isFraudGenericCheck(AntiFraudUtil.beginningOfLastDays(Integer.parseInt(benchmarkDays)),
                    AntiFraudUtil.beginningOfLastHours(Integer.parseInt(periodHours)), reg, userTransactionHistories,
                    mobileMinAmt, dthMinAmt, postPaidMinAmt);
            fraudBO.setFraudType(FraudType.FRAUD_NEW_3000);
        }
        return fraudBO;
    }

    private FraudBO checkFraud5000(long reg, List<UserTransactionHistory> userTransactionHistories) {
        FraudBO fraudBO = new FraudBO();
        Boolean isEnabled =
                Boolean.parseBoolean(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD5000_ENABLED));

        String benchmarkDays = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD5000_BENCHMARK_DAYS);
        String periodHours = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD5000_PERIOD_HOURS);
        String mobileMinAmt = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD5000_MOBILE_MIN_AMT);
        String dthMinAmt = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD5000_DTH_MIN_AMT);
        String postPaidMinAmt = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD5000_POSTPAID_MIN_AMT);

        if (isEnabled) {
            fraudBO = isFraudGenericCheck(AntiFraudUtil.beginningOfLastDays(Integer.parseInt(benchmarkDays)),
                    AntiFraudUtil.beginningOfLastHours(Integer.parseInt(periodHours)), reg, userTransactionHistories,
                    mobileMinAmt, dthMinAmt, postPaidMinAmt);
            fraudBO.setFraudType(FraudType.FRAUD_NEW_5000);
        }
        return fraudBO;
    }

    private FraudBO checkFraudNewCreditsPay(int userId, long benchmarkBeginMillis, long periodBeginMillis, long reg) {
        FraudBO fraudBO = new FraudBO();
        // is registered in benchmark?
        if (!(reg >= benchmarkBeginMillis)) {
            return fraudBO;
        }
        long periodEndMillis = System.currentTimeMillis();
        Long txn_count = afu.cumulativeCreditsPay(userId, periodBeginMillis, periodEndMillis);
        String maxCount = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_CREDITSPAY_MAX_COUNT);
        Amount currentWalletBalance = walletWrapper.getTotalBalance(userId);
        BigDecimal maxWalletBalance = BigDecimal.valueOf(
                Long.parseLong(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_CREDITSPAY_WALLET_LIMIT)));

        if (txn_count > Integer.parseInt(maxCount)
                && currentWalletBalance.getAmount().compareTo(maxWalletBalance) == 1) {
            fraudBO.setFraud(true);
            fraudBO.setFraudType(FraudType.FRAUD_CREDITS_PAY_COUNT);
            fraudBO.setFraudCondn("txn_count:" + txn_count + " > maxCount:" + maxCount + " && currentWalletBalance:"
                    + currentWalletBalance + " > maxWalletBalance:" + maxWalletBalance);
            return fraudBO;
        }
        return fraudBO;
    }

    private FraudBO checkFraudCreditsPay(int userId, long reg) {
        FraudBO fraudBO = new FraudBO();
        Boolean isEnabled =
                Boolean.parseBoolean(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_CREDITSPAY_ENABLED));

        String benchmarkDays = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_CREDITSPAY_BENCHMARK_DAYS);
        String periodHours = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_CREDITSPAY_PERIOD_HOURS);

        if (isEnabled) {
            fraudBO =
                    checkFraudNewCreditsPay(userId, AntiFraudUtil.beginningOfLastDays(Integer.parseInt(benchmarkDays)),
                            AntiFraudUtil.beginningOfLastHours(Integer.parseInt(periodHours)), reg);
        }
        return fraudBO;
    }

    private FraudBO checkFraudAddCashVelocity(long benchmarkBeginMillis, long periodBeginMillis, String amount,
            long reg, List<UserTransactionHistory> userTransactionHistories) {
        FraudBO fraudBO = new FraudBO();
        // is registered in benchmark?
        if (!(reg >= benchmarkBeginMillis)) {
            return fraudBO;
        }
        double maxAmount =
                Double.parseDouble(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_ADDCASH_VELOCITY_MAX_AMT));
        double amt = Double.parseDouble(amount);
        Long txn_count = afu.cumulativeAddCashVelocity(new Timestamp(periodBeginMillis), userTransactionHistories);
        String maxCount = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_ADDCASH_VELOCITY_COUNT);
        if (amt > maxAmount && txn_count > Integer.parseInt(maxCount)) {
            fraudBO.setFraud(true);
            fraudBO.setFraudType(FraudType.FRAUD_ADD_CASH_VELOCITY);
            fraudBO.setFraudCondn("amt:" + amt + " > maxAmount:" + maxAmount + " && txn_count:" + txn_count
                    + " > maxCount:" + maxCount);
            return fraudBO;
        }
        return fraudBO;
    }

    private FraudBO checkFraudAddCashVelocity(String amount, long reg,
            List<UserTransactionHistory> userTransactionHistories) {
        FraudBO fraudBO = new FraudBO();
        Boolean isEnabled =
                Boolean.parseBoolean(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_ADDCASH_VELOCITY_ENABLED));

        String benchmarkDays = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_ADDCASH_VELOCITY_BENCHMARK_DAYS);
        String periodHours = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_ADDCASH_VELOCITY_PERIOD_HOURS);

        if (isEnabled) {
            fraudBO = checkFraudAddCashVelocity(AntiFraudUtil.beginningOfLastDays(Integer.parseInt(benchmarkDays)),
                    AntiFraudUtil.beginningOfLastHours(Integer.parseInt(periodHours)), amount, reg,
                    userTransactionHistories);
        }
        return fraudBO;
    }

    private boolean isNewUser(int userId, String lastDays) {
        long regMillis = afu.getUserRegistrationTimestamp(userId);
        long nowMillis = System.currentTimeMillis();
        long benchmarkBeginMillis = AntiFraudUtil.beginningOfLastDays(Integer.parseInt(lastDays));
        if (regMillis >= benchmarkBeginMillis && regMillis <= nowMillis) {
            return true;
        }
        return false;
    }

    private FraudBO checkFraudAddCashAmount(long benchmarkBeginMillis, String amount, long reg) {
        FraudBO fraudBO = new FraudBO();
        // is registered in benchmark?
        if (!(reg >= benchmarkBeginMillis)) {
            return fraudBO;
        }
        fraudBO = checkFraudAddCashAmount(amount);
        return fraudBO;
    }

    private FraudBO checkFraudAddCashAmount(String amount, long reg) {
        FraudBO fraudBO = new FraudBO();
        Boolean isEnabled =
                Boolean.parseBoolean(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_ADDCASH_VELOCITY_ENABLED));
        String benchmarkDays = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_ADDCASH_VELOCITY_BENCHMARK_DAYS);
        if (isEnabled) {
            fraudBO = checkFraudAddCashAmount(AntiFraudUtil.beginningOfLastDays(Integer.parseInt(benchmarkDays)),
                    amount, reg);
            return fraudBO;
        } else {
            return fraudBO;
        }
    }

    public void checkFraudNewUser(long userId, BigDecimal creditAmt, String rechargeAmount, String totalAmount,
            String lookupId, String fundSource) {
        String benchmarkDaysForPGRefund = fcProperties.getProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_BENCHMARK_DAYS);
        BigDecimal benchmarkAmountForNewUser =
                new BigDecimal(fcProperties.getProperty(FCConstants.ANTIFRAUD_ADDCASH_AMOUNT_MAX_AMT));
        boolean isFraudRefundToPGEnabled =
                fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_REFUND_TO_PG_ENABLED);
        if (isFraudRefundToPGEnabled && isNewUser((int) userId, benchmarkDaysForPGRefund)
                && creditAmt.compareTo(benchmarkAmountForNewUser) > 0) {
            List<UserTransactionHistory> userTransactionHistories =
                    userTransactionHistoryService.findAllUserTransactionHistoryByUserId((int) userId);
            FraudEmailData fraudEmailData = new FraudEmailData((int) userId, "Unknown", rechargeAmount, totalAmount,
                    null, userTransactionHistories, lookupId, -1, null);
            FraudBO fraudBO = new FraudBO(FraudType.FRAUD_REFUND_TO_PG, "refundToPGAmt:" + creditAmt + " > "
                    + "benchmarkAmountForNewUser/addCashMaxAmt:" + benchmarkAmountForNewUser, true);
            fraudEmailData.setFraudBO(fraudBO);
            sendEmail(fraudEmailData);
            throw new IllegalStateException("New User - Wallet Credit will happen only if refund amount < "
                    + benchmarkAmountForNewUser + " for fund source " + fundSource);
        }
    }

    public static enum FraudType {
        FRAUD_AMOUNT, FRAUD_FRESH, FRAUD_NEW_3000, FRAUD_NEW_5000, FRAUD_REGISTERED_TO_TXN, FRAUD_TICKET_SIZE_LIMIT, 
        FRAUD_CREDITS_PAY_COUNT, FRAUD_ADD_CASH_VELOCITY, FRAUD_ADD_CASH_AMOUNT, FRAUD_REFUND_TO_PG, FRAUD_DEVICE_APP, 
        FRAUD_DEVICE_LAPTOP, FRAUD_PRODUCT_LIMIT,FRAUD_ADHOC_UTILITY_LIMIT
    }

    public static class FraudsterData {
        private String     mobileNo;
        private String     emailID;
        private Date       registrationDate;
        private int        periodThresholdInHours;
        private int        txnCountInPeriod;
        private BigDecimal cumulativeMobileRechargeAmountInPeriod;
        private BigDecimal cumulativeDTHRechargeAmountInPeriod;
        private BigDecimal cummulativePostPaidAmount;

        public BigDecimal getCummulativePostPaidAmount() {
            return cummulativePostPaidAmount;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public String getEmailID() {
            return emailID;
        }

        public Date getRegistrationDate() {
            return registrationDate;
        }

        public int getPeriodThresholdInHours() {
            return periodThresholdInHours;
        }

        public int getTxnCountInPeriod() {
            return txnCountInPeriod;
        }

        public BigDecimal getCumulativeMobileRechargeAmountInPeriod() {
            return cumulativeMobileRechargeAmountInPeriod;
        }

        public BigDecimal getCumulativeDTHRechargeAmountInPeriod() {
            return cumulativeDTHRechargeAmountInPeriod;
        }

        public void setCummulativePostPaidAmount(BigDecimal cummulativePostPaidAmount) {
            this.cummulativePostPaidAmount = cummulativePostPaidAmount;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public void setRegistrationDate(Date registrationDate) {
            this.registrationDate = registrationDate;
        }

        public void setPeriodThresholdInHours(int periodThresholdInHours) {
            this.periodThresholdInHours = periodThresholdInHours;
        }

        public void setTxnCountInPeriod(int txnCountInPeriod) {
            this.txnCountInPeriod = txnCountInPeriod;
        }

        public void setCumulativeMobileRechargeAmountInPeriod(BigDecimal cumulativeMobileRechargeAmountInPeriod) {
            this.cumulativeMobileRechargeAmountInPeriod = cumulativeMobileRechargeAmountInPeriod;
        }

        public void setCumulativeDTHRechargeAmountInPeriod(BigDecimal cumulativeDTHRechargeAmountInPeriod) {
            this.cumulativeDTHRechargeAmountInPeriod = cumulativeDTHRechargeAmountInPeriod;
        }
    }

    private FraudsterData getFraudsterData(int userID, FraudType fraudType,
            List<UserTransactionHistory> userTransactionHistories) {
        FraudsterData fd = new FraudsterData();
        com.freecharge.app.domain.entity.jdbc.Users u = userServiceProxy.getUserByUserId(userID);
        fd.mobileNo = u.getMobileNo(); // set mobile no
        fd.emailID = u.getEmail(); // set email
        fd.registrationDate = u.getDateAdded(); // set registration date
        String periodHours = "";
        switch (fraudType) {
            case FRAUD_FRESH:
                periodHours = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_PERIOD_HOURS);
                break;
            case FRAUD_NEW_3000:
                periodHours = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD3000_PERIOD_HOURS);
                break;
            case FRAUD_NEW_5000:
                periodHours = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_FRAUD5000_PERIOD_HOURS);
                break;
            case FRAUD_ADD_CASH_VELOCITY:
                periodHours = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_ADDCASH_VELOCITY_PERIOD_HOURS);
                break;
            case FRAUD_CREDITS_PAY_COUNT:
                periodHours = fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_CREDITSPAY_PERIOD_HOURS);
                break;
            default:
                periodHours = "24";
        }
        int nPeriodHours = Integer.parseInt(periodHours);
        long periodBeginMillis = AntiFraudUtil.beginningOfLastHours(nPeriodHours);
        BigDecimal mobAmt = afu.cumulativeProductRechargeAmount(userTransactionHistories,
                new Timestamp(periodBeginMillis), FCConstants.PRODUCT_TYPE_MOBILE).getAmount();
        BigDecimal dthAmt = afu.cumulativeProductRechargeAmount(userTransactionHistories,
                new Timestamp(periodBeginMillis), FCConstants.PRODUCT_TYPE_DTH).getAmount();
        BigDecimal postPaidAmt = afu.cumulativeProductRechargeAmount(userTransactionHistories,
                new Timestamp(periodBeginMillis), FCConstants.PRODUCT_TYPE_POSTPAID).getAmount();
        int rechargeCount = afu.rechargeCount(userTransactionHistories, new Timestamp(periodBeginMillis));
        fd.setPeriodThresholdInHours(nPeriodHours); // period (hours)
        fd.setTxnCountInPeriod(rechargeCount); // recharge count in period
        fd.setCumulativeMobileRechargeAmountInPeriod(mobAmt); // mobile recharge
                                                              // amount in
                                                              // period
        fd.setCumulativeDTHRechargeAmountInPeriod(dthAmt); // DTH recharge
                                                           // amount in period
        fd.setCummulativePostPaidAmount(postPaidAmt);
        return fd;
    }

    private void sendEmail(FraudEmailData fraudEmail) {
        FraudBO fraudBO = fraudEmail.getFraudBO();
        metricsClient.recordEvent("Payment", "Fraud." + fraudEmail.getProductType(), fraudBO.getFraudType().name());
        String serviceNumber = null;
        if (fraudEmail.getOrderId() != null) {
            String lookupId = orderIdReadDAO.getLookupIdForOrderId(fraudEmail.getOrderId());
            TxnHomePageBusinessDO homePage = txnHomePageService.getTransactionDetails(lookupId);
            serviceNumber = homePage.getServiceNumber();
        }
        EmailBusinessDO ebdo = new EmailBusinessDO();
        ebdo.setFrom(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_EMAIL_NOTIFICATION_FROM));
        ebdo.setReplyTo(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_EMAIL_NOTIFICATION_FROM));
        ebdo.setTo(fcProperties.getAntiFraudProperty(FCConstants.ANTIFRAUD_EMAIL_NOTIFICATION_TO));
        FraudsterData fd = getFraudsterData(fraudEmail.getUserID(), fraudBO.getFraudType(),
                fraudEmail.getUserTransactionHistories());
        ebdo.setSubject("Detected potential fraud attempt by userId=" + fraudEmail.getUserID() + " under "
                + fraudBO.getFraudType());
        String subject = String.format("%s [%d txns|M %s|D %s|%s]", fraudBO.getFraudType(), fd.txnCountInPeriod,
                fd.cumulativeMobileRechargeAmountInPeriod.toString(), fd.cumulativeDTHRechargeAmountInPeriod.toString(),
                fd.emailID);
        ebdo.setSubject(subject);
        int nPeriod = fd.periodThresholdInHours;
        String br = "<br>\n";
        String emailContent = "\nUser-ID: " + fraudEmail.getUserID() + br + "\nScheme: " + fraudBO.getFraudType() + br
                + "\nEmail ID: " + fd.emailID + br + "\nRegistered mobile no: " + fd.mobileNo + br + "\nRegistered on: "
                + new SimpleDateFormat("dd-MM-yyyy").format(fd.registrationDate) + br + "\nChannel Id: "
                + fraudEmail.getChannelId() + br + "\nSubscriber Identification Number: " + serviceNumber + br
                + "\nRecharge attempted: Rs " + fraudEmail.getRechargeAmount() + " for productType: "
                + ProductName.fromCode(fraudEmail.getProductType()) + ", total: Rs " + fraudEmail.getTotalAmount() + br
                + "\nFraud Condition violated: " + fraudBO.getFraudCondn() + br + "\nTransaction count in " + nPeriod
                + " hours: " + fd.txnCountInPeriod + br + "\nCumulative Mobile recharge amount in " + nPeriod
                + " hours: Rs " + fd.cumulativeMobileRechargeAmountInPeriod + br
                + "\nCumulative DTH recharge amount in " + nPeriod + " hours: Rs "
                + fd.cumulativeDTHRechargeAmountInPeriod + br + "\nCumulative Postpaid recharge amount in " + nPeriod
                + " hours: Rs " + fd.cummulativePostPaidAmount;
        if (ProductName.fromPrimaryProductType(fraudEmail.getProductType()) == ProductName.WalletCash
                && !FCUtil.isEmpty(fraudEmail.getDuplicateUserIds())) {
            emailContent = emailContent + br + "\n Duplicate registered email,mobile : " + br;
            for (String userId : fraudEmail.getDuplicateUserIds()) {
                com.freecharge.app.domain.entity.jdbc.Users u = userServiceProxy.getUserByUserId(Integer.parseInt(userId));
                emailContent += u.getEmail() + "," + u.getMobileNo() + br;
            }
            if (fraudEmail.getDeviceId() != null) {
                emailContent += "Device Id : " + fraudEmail.getDeviceId() + br;
            }
        }
        if (fraudEmail.getLookupId() != null) {
            emailContent += "\nLookupId / MtxnId : " + fraudEmail.getLookupId() + br;
        }
        if (fraudEmail.getOrderId() != null) {
            emailContent += "\nOrderId : " + fraudEmail.getOrderId() + br;
        }
        ebdo.setContent(emailContent.replace("\n", ""));
        logger.info("\n" + subject + emailContent.replace(br, "") + "\n");
        if (fcProperties.isProdMode()) {
            emailService.sendNonVelocityEmail(ebdo);
        }
    }

    public void checkProductLevelFraudLimits(com.freecharge.app.domain.entity.jdbc.Users user,
            ProductPaymentBusinessDO productPaymentBusinessDO) throws FraudDetectedException {
        String productType = getProductType(productPaymentBusinessDO);
        if (StringUtils.isEmpty(productType)) {
            metricsClient.recordEvent("Fraud", "ProductLimit", "Invalid ProductType");
            return;
        }
        double amount = productPaymentBusinessDO.getAmount();
        if (productPaymentBusinessDO.isCreditOrDebitCardTransaction()) {
            String cardHash = BilldeskCardHash
                    .generateHash(productPaymentBusinessDO.getPaymentRequestCardDetails().getCardNumber());
            if (!this.isWithinProductRollingDaysLimit(productType, amount, cardHash, user)) {
                throw new FraudDetectedException("User ID : " + user.getUserId() + " " + FraudType.FRAUD_PRODUCT_LIMIT
                        + "for productType: " + productType + " OrderId:" + productPaymentBusinessDO.getOrderId());
            }
        }
    }

    private String getProductType(ProductPaymentBusinessDO productPaymentBusinessDO) {
        String productType = null;
        try {
            productType = ProductMaster.ProductName
                    .fromProductId(Integer.parseInt(productPaymentBusinessDO.getProductId())).getProductType();
        } catch (Exception ex) {
            logger.error("Unknown product id :" + productPaymentBusinessDO.getProductId());
        }
        return productType;
    }

    private boolean isWithinProductRollingDaysLimit(String productType, double newAmount, String cardHash,
            com.freecharge.app.domain.entity.jdbc.Users user) {
        List<FraudLimit> fraudLimits = fraudLimitCacheManager.getTransactionAmountLimits(productType);
        double totalRollingAmount = 0;
        long txnCount = 0;
        boolean willReachfraudLimit = false;
        for (FraudLimit limit : fraudLimits) {
            if (limit.isApplicableForUser(user)) {
                if (limit.isMaxTxnCountApplicable()) {
                    txnCount = paymentHistoryRepository.getTotalTxCount(
                            ProductMaster.ProductName.fromPrimaryProductType(productType).getProductId(),
                            user.getUserId(), limit.getRollingWindowDurationInSeconds());
                    if (txnCount + 1 > limit.getMaxTxnCount()) {
                        willReachfraudLimit = true;
                        metricsClient.recordEvent("Fraud", "ProductLimitRule", String.valueOf(limit.getId()));
                        metricsClient.recordEvent("Fraud", "ProductLimitRule" + String.valueOf(limit.getId()),
                                String.valueOf(newAmount));
                        logger.info("User " + user.getUserId() + " reached fraud limit rule " + limit.getId()
                                + " for transaction count.Current rolling transaction count :" + txnCount);
                        break;
                    }
                }
                if (limit.isMaxAmountApplicable()) {
                    totalRollingAmount = paymentHistoryRepository.getTotalTxAmount(cardHash,
                            ProductMaster.ProductName.fromPrimaryProductType(productType).getProductId(),
                            limit.getRollingWindowDurationInSeconds());
                    if (totalRollingAmount + newAmount > limit.getMaxAmount()) {
                        willReachfraudLimit = true;
                        metricsClient.recordEvent("Fraud", "ProductLimitRule", String.valueOf(limit.getId()));
                        metricsClient.recordEvent("Fraud", "ProductLimitRule" + String.valueOf(limit.getId()),
                                String.valueOf(newAmount));
                        logger.info("User " + user.getUserId() + " reached fraud limit rule " + limit.getId()
                                + " for rolling amount.Current rolling amount :" + totalRollingAmount);
                        break;
                    }
                }
            }
        }
        return !willReachfraudLimit;
    }
}
