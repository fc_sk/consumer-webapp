package com.freecharge.antifraud;

import java.io.Serializable;

import org.joda.time.DateTime;
import org.joda.time.Seconds;

import com.freecharge.app.domain.entity.jdbc.Users;

public class FraudLimit implements Serializable{

    private static final long serialVersionUID = 1L;
    private int    id;
    private String productType;
    private int    rollingWindowDays;
    private int    rollingWindowHours;
    private int    rollingWindowMinutes;
    private int    fcAcctAgeInDays;
    private int    fcAcctAgeInHours;
    private int    fcAcctAgeInMinutes;
    private int    maxTxnCount;
    private double maxAmount;
    private char   activeIn;
    
    public FraudLimit(){
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getRollingWindowDays() {
        return rollingWindowDays;
    }

    public void setRollingWindowDays(int rollingWindowDays) {
        this.rollingWindowDays = rollingWindowDays;
    }

    public int getRollingWindowHours() {
        return rollingWindowHours;
    }

    public void setRollingWindowHours(int rollingWindowHours) {
        this.rollingWindowHours = rollingWindowHours;
    }

    public int getRollingWindowMinutes() {
        return rollingWindowMinutes;
    }

    public void setRollingWindowMinutes(int rollingWindowMinutes) {
        this.rollingWindowMinutes = rollingWindowMinutes;
    }

    public int getRollingWindowDurationInSeconds() {
        return rollingWindowDays * 24 * 60 * 60 + rollingWindowHours * 60 * 60 + rollingWindowMinutes * 60;
    }

    public int getFcAcctAgeInDays() {
        return fcAcctAgeInDays;
    }

    public void setFcAcctAgeInDays(Integer fcAcctAgeInDays) {
        this.fcAcctAgeInDays = fcAcctAgeInDays;
    }

    public int getFcAcctAgeInHours() {
        return fcAcctAgeInHours;
    }

    public void setFcAcctAgeInHours(Integer fcAcctAgeInHours) {
        this.fcAcctAgeInHours = fcAcctAgeInHours;
    }

    public int getFcAcctAgeInMinutes() {
        return fcAcctAgeInMinutes;
    }

    public void setFcAcctAgeInMinutes(Integer fcAcctAgeInMinutes) {
        this.fcAcctAgeInMinutes = fcAcctAgeInMinutes;
    }

    public long getFcAcctAgeInSeconds() {
        long totalTime = fcAcctAgeInDays * 24 * 60 * 60 + fcAcctAgeInHours * 60 * 60 + fcAcctAgeInMinutes * 60;
        return totalTime == 0 ? Long.MAX_VALUE : totalTime;
    }

    public Integer getMaxTxnCount() {
        return maxTxnCount == 0 ? Integer.MAX_VALUE - 10 : maxTxnCount;
    }

    public void setMaxTxnCount(Integer maxTxnCount) {
        this.maxTxnCount = maxTxnCount;
    }

    public double getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(double maxAmount) {
        this.maxAmount = maxAmount;
    }

    public char getActiveIn() {
        return activeIn;
    }

    public void setActiveIn(char activeIn) {
        this.activeIn = activeIn;
    }

    public boolean isMaxTxnCountApplicable() {
        return maxTxnCount > 0;
    }

    public boolean isMaxAmountApplicable() {
        return maxAmount > 0;
    }

    public boolean isApplicableForUser(Users user) {
        return this.getFcAcctAgeInSeconds() == Long.MAX_VALUE
                || Seconds.secondsBetween(new DateTime(user.getDateAdded().getTime()),new DateTime())
                        .getSeconds() <= this.getFcAcctAgeInSeconds();
    }

}
