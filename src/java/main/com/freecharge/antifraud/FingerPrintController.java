package com.freecharge.antifraud;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.mongo.repos.DeviceFingerPrintRepository;
import com.freecharge.web.util.WebConstants;

/**
 * User: abhi
 * Date: 20/2/14
 * Time: 4:14 PM
 */
@Controller
@RequestMapping("/app/fnprnt")
//Path is intentionally obfuscated
public class FingerPrintController {
    private Logger logger = LoggingFactory.getLogger(FingerPrintController.class);

    @Autowired
    private DeviceFingerPrintRepository deviceFingerPrintRepository;

    @Autowired
    private AppConfigService appConfigService;

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.POST)
    public String defaultHandler(@RequestParam Map<String, String> params) {
        
        if (appConfigService.isFingerPrintingEnabled()) {
            FreechargeSession freechargeSession = FreechargeContextDirectory.get().getFreechargeSession();
            Integer userId = (Integer) freechargeSession.getSessionData().get(WebConstants.SESSION_USER_USERID);
            if (userId != null) {
                try {
                    String fingerPrint = params.get("hash");
                    deviceFingerPrintRepository.update(fingerPrint, userId);
                    freechargeSession.getSessionData().put(WebConstants.SESSION_FINGER_PRINT, fingerPrint);
                } catch (RuntimeException e) {
                    logger.error("Exception while persisting fingerprint for user - " + userId, e);
                }
            } else {
                logger.error("Got a fingerprint request but found no user in session");
            }
        }
        //Response does not matter
        return "";
    }
}
