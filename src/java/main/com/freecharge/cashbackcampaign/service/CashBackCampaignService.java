package com.freecharge.cashbackcampaign.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.cashbackcampaign.dao.CashBackCampaignDao;
import com.freecharge.cashbackcampaign.entity.CashBackCampaign;
import com.freecharge.common.framework.properties.FCProperties;

@Component
public class CashBackCampaignService {
    @Autowired
    private CashBackCampaignDao cashBackCampaignDao;

    @Autowired
    private FCProperties fcProperties;

    public long insertCashBackCampaign(CashBackCampaign cashBackCampaign) {
        return cashBackCampaignDao.insertCashBackCampaign(cashBackCampaign);
    }

    public CashBackCampaign.State getState(int userId) {
        return cashBackCampaignDao.getState(userId);
    }
    
    public boolean updateToPending(int userId) {
        return cashBackCampaignDao.updateToPending(userId);
    }
    
    public boolean updateToApplied(int userId) {
        return cashBackCampaignDao.updateToApplied(userId);
    }
    
    public boolean deletedOptedOrPending(Integer userId) {
        return cashBackCampaignDao.deleteOptedOrPending(userId);
    }
    
    public boolean deletedOpted(Integer userId) {
        return cashBackCampaignDao.deleteOpted(userId);
    }

    public boolean hasReachedLimit() {
        return cashBackCampaignDao.noOfParticipants() > fcProperties.getCashBackOfferLimit();
    }
}
