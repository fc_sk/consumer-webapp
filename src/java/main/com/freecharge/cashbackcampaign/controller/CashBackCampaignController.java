package com.freecharge.cashbackcampaign.controller;

import java.sql.Timestamp;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.freecharge.cashbackcampaign.entity.CashBackCampaign;
import com.freecharge.cashbackcampaign.service.CashBackCampaignService;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.util.LanguageUtility;
import com.freecharge.web.util.WebConstants;

@Controller
@RequestMapping("/cashbackcampaign/*")
public class CashBackCampaignController {
    public static final String APPLY_ACTION = "/cashbackcampaign/apply.htm";
    public static final String REMOVE_ACTION = "/cashbackcampaign/remove.htm";

    @Autowired
    private CashBackCampaignService cashBackCampaignService;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private LanguageUtility languageUtility;

    @Autowired
    private AppConfigService appConfigService;

    @RequestMapping(value = "/apply", produces = "application/json")
    public String apply(@RequestParam Map<String, String> params, Model model, HttpServletRequest request) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        Map map = fs.getSessionData();
        boolean isLoggedIn = (Boolean) map.get(WebConstants.SESSION_USER_IS_LOGIN);

        final String VALID = "valid";
        final String MESSAGE = "message";

        if (isLoggedIn && appConfigService.isCashBackCampaignEnabled() && !cashBackCampaignService.hasReachedLimit()
                && fcProperties.getCashBackOfferCode().equalsIgnoreCase(params.get("code"))) {
            Integer userId = (Integer)map.get(WebConstants.SESSION_USER_USERID);

            CashBackCampaign.State state = cashBackCampaignService.getState(userId);
            if (state != CashBackCampaign.State.APPLIED) {
                CashBackCampaign cashBackCampaign = new CashBackCampaign();
                cashBackCampaign.setState(CashBackCampaign.State.OPTED);
                cashBackCampaign.setUserId(userId);
                cashBackCampaign.setCreatedTime(new Timestamp(System.currentTimeMillis()));
                cashBackCampaignService.insertCashBackCampaign(cashBackCampaign);
                model.addAttribute(VALID, true);
                model.addAttribute(MESSAGE, languageUtility.getLocaleMessage(request, "cash.back.campaign.successful.code.application", null));
            } else  {
                model.addAttribute(VALID, false);
                model.addAttribute(MESSAGE, languageUtility.getLocaleMessage(request, "cash.back.campaign.code.already.applied", null));
            }
        } else {
            model.addAttribute(VALID, false);
            model.addAttribute(MESSAGE, "Uh-oh! Incorrect code.");
        }

        return "jsonView";
    }
    
	@RequestMapping(value = "/remove", produces = "application/json")
	public String remove(@RequestParam Map<String, String> params, Model model,
			HttpServletRequest request) {
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		Map map = fs.getSessionData();
	
		final String VALID = "valid";
		final String MESSAGE = "message";

		Integer userId = (Integer) map.get(WebConstants.SESSION_USER_USERID);

		CashBackCampaign.State state = cashBackCampaignService.getState(userId);
		if (state == CashBackCampaign.State.OPTED) {
			cashBackCampaignService.deletedOpted(userId);
			model.addAttribute(VALID, true);
		}
		else {
			model.addAttribute(VALID, false);
		}

		return "jsonView";
	}
}
