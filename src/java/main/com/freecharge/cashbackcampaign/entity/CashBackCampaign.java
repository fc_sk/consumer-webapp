package com.freecharge.cashbackcampaign.entity;

import java.sql.Timestamp;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 * A bean representing the cash_back_campaign table.
 */
public class CashBackCampaign {
    private int id;
    private int userId;
    private State state;
    private Timestamp createdTime;

    public enum State {
        OPTED("opted"), PENDING("pending"), APPLIED("applied");

        private String state;

        private State(String state) {
            this.state = state;
        }

        public String getValue() {
            return this.state;
        }

        public static State of(String value) {
            for (State state : State.values()) {
                if (state.getValue().equals(value)) {
                    return state;
                }
            }

            return null;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public MapSqlParameterSource getMapSqlParameterSource() {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("fk_user_id", this.getUserId());
        mapSqlParameterSource.addValue("state", this.getState().getValue());
        mapSqlParameterSource.addValue("created_at", this.getCreatedTime());
        return mapSqlParameterSource;
    }
}
