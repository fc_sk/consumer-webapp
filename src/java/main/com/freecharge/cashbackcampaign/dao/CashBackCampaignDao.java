package com.freecharge.cashbackcampaign.dao;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import com.freecharge.cashbackcampaign.entity.CashBackCampaign;
import com.freecharge.common.util.FCUtil;

@Component
public class CashBackCampaignDao {
    private SimpleJdbcInsert insertCashBackDao;
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.insertCashBackDao = new SimpleJdbcInsert(dataSource).withTableName("cash_back_campaign").usingGeneratedKeyColumns("id", "n_last_updated", "n_created");
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public long insertCashBackCampaign(CashBackCampaign cashBackCampaign) {
        SqlParameterSource sqlParameterSource = cashBackCampaign.getMapSqlParameterSource();
        Number rowsInserted = insertCashBackDao.executeAndReturnKey(sqlParameterSource);
        return rowsInserted.longValue();
    }

    public CashBackCampaign.State getState(int userId) {
        String sql = "select state from cash_back_campaign where fk_user_id = :userId";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("userId", userId);

        List<String> states = jdbcTemplate.queryForList(sql, paramMap, String.class);

        if (states.isEmpty()) {
            return null;
        }

        return CashBackCampaign.State.of(states.get(0));
    }
    
    public boolean updateToPending(int userId) {
        String sql = "update cash_back_campaign set state='pending' where state='opted' and fk_user_id=:userId";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("userId", userId);

        int noOfRows = jdbcTemplate.update(sql, paramMap);

        return noOfRows != 0;
    }
    
    public boolean updateToApplied(int userId) {
        String sql = "update cash_back_campaign set state='applied' where (state='opted' or state='pending') and fk_user_id=:userId";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("userId", userId);

        int noOfRows = jdbcTemplate.update(sql, paramMap);

        return noOfRows != 0;
    }
    

	public boolean deleteOptedOrPending(int userId) {
		String sql = "delete from cash_back_campaign where (state='opted' or state='pending') and fk_user_id=:userId";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("userId", userId);

        int noOfRows = jdbcTemplate.update(sql, paramMap);

        return noOfRows != 0;
	}
	
	public boolean deleteOpted(int userId) {
		String sql = "delete from cash_back_campaign where (state='opted') and fk_user_id=:userId";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("userId", userId);

        int noOfRows = jdbcTemplate.update(sql, paramMap);

        return noOfRows != 0;
	}

    public long noOfParticipants() {
        String sql = "select count(*) from cash_back_campaign";
        List<Long> opted = jdbcTemplate.queryForList(sql, new HashMap<String, Object>(), Long.class);

        if (opted.isEmpty()) {
            return 0;
        }

        return opted.get(0);
    }

    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(CashBackCampaignDao.class.getSimpleName());
    }

    public static void main(String[] args) {
        CashBackCampaign cashBackCampaign = new CashBackCampaign();
        cashBackCampaign.setCreatedTime(new Timestamp(System.currentTimeMillis()));
        cashBackCampaign.setState(CashBackCampaign.State.OPTED);
        cashBackCampaign.setUserId(1);

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        CashBackCampaignDao cashBackCampaignDao = ((CashBackCampaignDao) context.getBean(CashBackCampaignDao.getBeanName()));

        System.out.println("No of applied codes:" + cashBackCampaignDao.noOfParticipants());

        context.close();

        System.exit(0);
    }

}
