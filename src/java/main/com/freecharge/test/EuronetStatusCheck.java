package com.freecharge.test;

import java.io.IOException;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.app.domain.dao.jdbc.InReadDAO;
import com.freecharge.app.domain.dao.jdbc.OperatorCircleReadDAO;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InRequest;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.recharge.businessDo.AggregatorResponseDo;
import com.freecharge.recharge.businessDo.RechargeDo;
import com.freecharge.recharge.services.EuronetService;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 20/9/12
 * Time: 1:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class EuronetStatusCheck {
    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");

        InReadDAO inReadDAO = (InReadDAO)ctx.getBean("inReadDAO");

        OperatorCircleReadDAO operatorCircleReadDAO = (OperatorCircleReadDAO)ctx.getBean("operatorCircleReadDAO");

        OperatorCircleService operatorCircleService = (OperatorCircleService)ctx.getBean("operatorCircleService");

        long requestId = Long.parseLong(args[0]);

        RechargeDo rechargeDo = new RechargeDo();
        rechargeDo.setInRequestId(requestId);

        InResponse inResponse = inReadDAO.getInResponseByRequestId(requestId);
        InRequest inRequest = inReadDAO.getInRequest(requestId);

        EuronetService euronetService = ((EuronetService) ctx.getBean("euronet"));

        OperatorMaster operatorMaster = operatorCircleReadDAO.getOperator(inRequest.getOperator());
        CircleMaster circleMaster = operatorCircleReadDAO.getCircle(inRequest.getCircle());

        List<InAggrOprCircleMap> inAggrOprCircleMapsList = operatorCircleService.getInAggrOprCircleMap(operatorMaster.getOperatorMasterId(), circleMaster.getCircleMasterId());

        AggregatorResponseDo aggregatorResponseDo = euronetService.getTransactionStatus(rechargeDo, inResponse);
        System.out.println("Transaction status is:" + aggregatorResponseDo);

        ctx.close();
    }
}
