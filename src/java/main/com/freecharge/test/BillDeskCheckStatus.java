package com.freecharge.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.payment.dos.business.PaymentQueryBusinessDO;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.BillDeskGatewayHandler;
import com.freecharge.payment.services.PaymentTransactionService;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 15/9/12
 * Time: 12:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class BillDeskCheckStatus {
    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");

        PaymentTransactionService paymentTransactionService = ((PaymentTransactionService) ctx.getBean("paymentTransactionService"));
        int paymentTransactionId = Integer.parseInt(args[0]);
        PaymentTransaction paymentTransaction = paymentTransactionService.getPaymentTransactionById(paymentTransactionId);

        PaymentQueryBusinessDO paymentQueryBusinessDO = PaymentQueryBusinessDO.create(paymentTransaction);

        BillDeskGatewayHandler billDeskGatewayHandler = (BillDeskGatewayHandler)ctx.getBean("billDeskGatewayHandler");

        billDeskGatewayHandler.paymentStatus(paymentQueryBusinessDO, false);

        System.out.println(paymentQueryBusinessDO);
    }
}
