package com.freecharge.common.easydb;

import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 6/10/12
 * Time: 9:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class SelectDBVO {
    private String selectQuery;
    private RowMapper rowMapper;
    private Map<String, Object> paramMap;

    public String getSelectQuery() {
        return selectQuery;
    }

    public void setSelectQuery(String selectQuery) {
        this.selectQuery = selectQuery;
    }

    public RowMapper getRowMapper() {
        return rowMapper;
    }

    public void setRowMapper(RowMapper rowMapper) {
        this.rowMapper = rowMapper;
    }

    public Map<String, Object> getParamMap() {
        return paramMap;
    }

    public void setParamMap(Map<String, Object> paramMap) {
        this.paramMap = paramMap;
    }

}
