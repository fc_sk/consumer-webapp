package com.freecharge.common.easydb;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 6/10/12
 * Time: 7:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class SelectQuery {
    private static Logger logger = LoggingFactory.getLogger(SelectQuery.class);

    private static  Map<Class, String> methodReturnTypeNameMapping = new HashMap<Class, String>();
    private Map<String, Object> paramMap = new HashMap<String, Object>();

    private DBEntity dbEntity;
    private SelectDBVO selectDBVO;

    private RowMapper rowMapper;
    private String query;

    private static final String QUERY_SNIPPET_FORMAT = "%s=:%s";
    private static final String SELECT_QUERY_FORMAT = "select * from %s where %s";

    public SelectQuery(DBEntity dbEntity) {
        this.dbEntity = dbEntity;
        this.init();
    }

    static {
        methodReturnTypeNameMapping.put(String.class, "getString");
        methodReturnTypeNameMapping.put(Integer.class, "getInt");
        methodReturnTypeNameMapping.put(Long.class, "getLong");
        methodReturnTypeNameMapping.put(BigDecimal.class, "getBigDecimal");
        methodReturnTypeNameMapping.put(Date.class, "getDate");
        methodReturnTypeNameMapping.put(Float.class, "getFloat");
        methodReturnTypeNameMapping.put(Boolean.class, "getBoolean");
        methodReturnTypeNameMapping.put(Timestamp.class, "getTimestamp");
        methodReturnTypeNameMapping.put(java.sql.Date.class, "getDate");
        methodReturnTypeNameMapping.put(Double.class, "getDouble");
    }

    private void init() {
        this.populateQueryAndParamMap();
        this.rowMapper = this.getRowMapper();

        SelectDBVO selectDBVO = new SelectDBVO();
        selectDBVO.setRowMapper(rowMapper);
        selectDBVO.setSelectQuery(query);
        selectDBVO.setParamMap(paramMap);

        this.selectDBVO = selectDBVO;
    }

    private void populateQueryAndParamMap() {
        List<String> wheres = new LinkedList<String>();

        Field[] fields = dbEntity.getClass().getDeclaredFields();

        for (Field field : fields) {
            try {
                String name = field.getName();
                if (field.isAnnotationPresent(ColumnMapping.class)) {
                    ColumnMapping columnMapping = field.getAnnotation(ColumnMapping.class);
                    String dbColumnName = columnMapping.name();
                    Object value = PropertyUtils.getProperty(this.dbEntity, name);

                    if (value != null) {
                        if (field.isAnnotationPresent(CustomDataType.class)) {
                            CustomDataType customDataType = field.getAnnotation(CustomDataType.class);
                            String getter = customDataType.getter();
                            Class type = customDataType.dataType();
                            Method method = type.getMethod(getter);
                            value = method.invoke(value);
                        }

                        wheres.add(String.format(QUERY_SNIPPET_FORMAT, dbColumnName, dbColumnName));
                        paramMap.put(dbColumnName, value);
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        String stringWheres = "";
        if (!wheres.isEmpty()) {
            stringWheres = StringUtils.join(wheres, " and ");
        } else {
            throw new RuntimeException("Cannot do a select all, please provide a where condition");
        }

        this.query = String.format(SELECT_QUERY_FORMAT, this.dbEntity.getTableName(), stringWheres);
    }

    private RowMapper getRowMapper() {
        RowMapper rowMapper = new RowMapper() {

            @Override
            public Object mapRow(ResultSet resultSet, int i) throws SQLException {
                Object object = null;
                try {
                    Class cls = dbEntity.getClass();
                    object = cls.newInstance();

                    Field[] fields = cls.getDeclaredFields();

                    for (Field field : fields) {
                        if (field.isAnnotationPresent(ColumnMapping.class)) {
                            ColumnMapping columnMapping = field.getAnnotation(ColumnMapping.class);
                            String dbColumnName = columnMapping.name();
                            Class fieldTypeClass = field.getType();

                            if (field.isAnnotationPresent(CustomDataType.class)) {
                                CustomDataType customDataType = field.getAnnotation(CustomDataType.class);

                                if (customDataType.dataType().isEnum()) {
                                    fieldTypeClass = String.class;
                                } else {
                                    String customDataTypeGetterMethodName = customDataType.getter();
                                    fieldTypeClass = customDataType.dataType().getMethod(customDataTypeGetterMethodName).getReturnType();
                                }
                            }

                            String resultSetMethodName = methodReturnTypeNameMapping.get(fieldTypeClass);

                            if (resultSetMethodName == null) {
                                throw new RuntimeException(fieldTypeClass.getName() + " data type is not yet mapped to a method name in ResultSet. Please do this mapping.");
                            }

                            Object value = resultSet.getClass().getMethod(resultSetMethodName, String.class).invoke(resultSet, dbColumnName);

                            if (field.isAnnotationPresent(CustomDataType.class)) {
                                CustomDataType customDataType = field.getAnnotation(CustomDataType.class);
                                Class type = customDataType.dataType();

                                if (type.isEnum()) {
                                    //todo - fix this
                                    value = type.getDeclaredMethod("get", String.class).invoke(null, value);
                                } else {
                                    value = type.getConstructor(value.getClass()).newInstance(value);
                                }
                            } else {

                            }

                            PropertyUtils.setProperty(object, field.getName(), value);
                        }
                    }

                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                return object;
            }
        };

        return rowMapper;
    }

    public SelectDBVO getSelectDBVO() {
        return selectDBVO;
    }

    public void setSelectDBVO(SelectDBVO selectDBVO) {
        this.selectDBVO = selectDBVO;
    }
}
