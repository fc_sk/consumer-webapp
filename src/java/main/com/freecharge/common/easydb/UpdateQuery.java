package com.freecharge.common.easydb;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 5/10/12
 * Time: 10:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class UpdateQuery {
    private DBEntity entity;
    private static final String QUERY_SNIPPET_FORMAT = "%s=:%s";
    private static final String UPDATE_QUERY_FORMAT = "update %s set %s where %s";
    private UpdateDBVO updateDBVO;

    public UpdateQuery(DBEntity entity, Collection<String> whereFields) {
        this.entity = entity;
        this.whereFields = whereFields;
        this.init();
    }

    private Collection<String> whereFields;
    private String query = "";

    private Map<String, Object> paramMap = new HashMap<String, Object>();

    private void init() {
        List<String> values = new LinkedList<String>();
        List<String> wheres = new LinkedList<String>();

        Field[] fields = this.entity.getClass().getDeclaredFields();

        for (Field field : fields) {
            if (isWhereField(field.getName())) {
               createQuerySnippet(field, wheres);
            } else {
               createQuerySnippet(field, values);
            }
        }

        if (!this.whereFields.isEmpty()) {
            throw new RuntimeException("Mismatch in where fields. Field " + String.format("{%s}", StringUtils.join(this.whereFields, ", ") + " not matching any bean fields."));
        }

        String stringValues = StringUtils.join(values, ", ");
        String stringWheres = StringUtils.join(wheres, " and ");

        this.query = String.format(UPDATE_QUERY_FORMAT, entity.getTableName(), stringValues, stringWheres);

        this.updateDBVO = new UpdateDBVO(query, this.paramMap);
    }

    private void createQuerySnippet(Field field, List<String> values) {
        try {
            String name = field.getName();
            if (field.isAnnotationPresent(ColumnMapping.class)) {
                ColumnMapping columnMapping = field.getAnnotation(ColumnMapping.class);
                String dbColumnName = columnMapping.name();
                Object value = PropertyUtils.getProperty(this.entity, name);

                if (value != null) {
                    if (field.isAnnotationPresent(CustomDataType.class)) {
                        CustomDataType customDataType = field.getAnnotation(CustomDataType.class);
                        String getter = customDataType.getter();
                        Class type = customDataType.dataType();
                        Method method = type.getMethod(getter);
                        value = method.invoke(value);
                    }

                    values.add(String.format(QUERY_SNIPPET_FORMAT, dbColumnName, dbColumnName));
                    paramMap.put(dbColumnName, value);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public boolean isWhereField(String fieldName) {
        if (whereFields.contains(fieldName)) {
            whereFields.remove(fieldName);
            return true;
        }
        return false;
    }

    public String getQuery() {
        return query;
    }

    public Map<String, Object> getParamMap() {
        return paramMap;
    }

    public Collection<String> getWhereFields() {
        return whereFields;
    }

    public UpdateDBVO getUpdateDBVO() {
        return this.updateDBVO;
    }

    public static void main(String[] args) {
        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
        paymentRefundTransaction.setPaymentRefundTransactionId(1);
        paymentRefundTransaction.setRefundedAmount(new Amount(new BigDecimal(10)));
        paymentRefundTransaction.setPaymentTransactionId(1);
        paymentRefundTransaction.setPgResponseCode("500");
        paymentRefundTransaction.setPgStatus("status");
        paymentRefundTransaction.setPgTransactionReferenceNo("abcdef");
        paymentRefundTransaction.setRawPGRequest("raw request sent to pg");
        paymentRefundTransaction.setRawPGResponse("raw response form pg");
        paymentRefundTransaction.setSentToPG(new Date());
        paymentRefundTransaction.setReceivedFromPG(new Date());
        paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.SUCCESS);

        UpdateQuery updateQuery = new UpdateQuery(paymentRefundTransaction, FCUtil.asList("paymentRefundTransactionId"));

        Map<String, Object> params = updateQuery.getParamMap();

        for (Map.Entry<String, Object> map : params.entrySet()) {
            System.out.println(map.getValue().getClass());
        }

/*        System.out.println("Param map:" + updateQuery.getParamMap());
        System.out.println("Query:" + updateQuery.getQuery());*/
    }
}
