package com.freecharge.common.easydb;


/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 5/10/12
 * Time: 10:48 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DBEntity {
    public String getTableName();
    public UpdateDBVO getUpdateDBVO();
    public SelectDBVO getSelectDBVO();
}
