package com.freecharge.common.easydb;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.common.util.FCUtil;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 27/10/12
 * Time: 11:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class EasyDBReadDAO<T extends DBEntity> {
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<T> get(T dbEntity) {
        SelectDBVO selectDBVO = dbEntity.getSelectDBVO();
        List<T> dbEntities =  this.jdbcTemplate.query(selectDBVO.getSelectQuery(), selectDBVO.getParamMap(), selectDBVO.getRowMapper());
        return dbEntities;
    }

    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(EasyDBReadDAO.class.getSimpleName());
    }
}
