package com.freecharge.common.easydb;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 6/10/12
 * Time: 1:11 AM
 * To change this template use File | Settings | File Templates.
 */
public class UpdateDBVO {
    private String query;
    private Map<String, Object> params;

    public UpdateDBVO(String query, Map<String, Object> params) {
        this.query = query;
        this.params = params;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
}
