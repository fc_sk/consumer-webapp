package com.freecharge.common.easydb;

import java.math.BigDecimal;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.common.util.Amount;
import com.freecharge.common.util.FCUtil;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 5/10/12
 * Time: 11:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class EasyDBWriteDAO<T extends DBEntity> {
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public int update(DBEntity dbEntity) {
        UpdateDBVO updateDBVO = dbEntity.getUpdateDBVO();
        return this.jdbcTemplate.update(updateDBVO.getQuery(), updateDBVO.getParams());
    }

    public List<T> get(T dbEntity) {
        SelectDBVO selectDBVO = dbEntity.getSelectDBVO();
        List<T> dbEntities =  this.jdbcTemplate.query(selectDBVO.getSelectQuery(), selectDBVO.getParamMap(), selectDBVO.getRowMapper());
        return dbEntities;
    }

    public static String getBeanName() {
        return FCUtil.getLowerCamelCase(EasyDBWriteDAO.class.getSimpleName());
    }

    public static void main(String[] args) {
        PaymentRefundTransaction paymentRefundTransaction = new PaymentRefundTransaction();
        //paymentRefundTransaction.setPaymentRefundTransactionId(1);
        //paymentRefundTransaction.setPgResponseCode("400");
        paymentRefundTransaction.setRefundedAmount(new Amount(new BigDecimal(12)));
/*
        paymentRefundTransaction.setPaymentRefundTransactionId(1);
        paymentRefundTransaction.setRefundedAmount(new Amount(new BigDecimal(10)));
        paymentRefundTransaction.setPaymentTransactionId(1);
        paymentRefundTransaction.setPgResponseCode("500");
        paymentRefundTransaction.setPgStatus("status");
        paymentRefundTransaction.setPgTransactionReferenceNo("abcdefsldjflkdslfksk");
        paymentRefundTransaction.setRawPGRequest("raw request sent to pg");
        paymentRefundTransaction.setRawPGResponse("raw response form pg");
        paymentRefundTransaction.setSentToPG(new Date());
        paymentRefundTransaction.setReceivedFromPG(new Date());
        paymentRefundTransaction.setStatus(PaymentRefundTransaction.Status.SUCCESS);

        paymentRefundTransaction.addToWhereFields("paymentRefundTransactionId");
*/

        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContextDev.xml", "web-delegates.xml");
        EasyDBWriteDAO easyDBWriteDAO = ((EasyDBWriteDAO) ctx.getBean("easyDBWriteDAO"));

        //easyDBWriteDAO.update(paymentRefundTransaction);
        System.out.println(easyDBWriteDAO.get(paymentRefundTransaction));
    }
}
