package com.freecharge.common.comm.rps;

import java.io.Serializable;
import java.util.Map;

public class ThirdPartyMappingsResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<Long, String> operatorMappingMap;
	private Map<Long, String> circleMappingMap;
	private Map<String, String> categoryMappingMap;

	public Map<Long, String> getOperatorMappingMap() {
		return operatorMappingMap;
	}

	public void setOperatorMappingMap(Map<Long, String> operatorMappingMap) {
		this.operatorMappingMap = operatorMappingMap;
	}

	public Map<Long, String> getCircleMappingMap() {
		return circleMappingMap;
	}

	public void setCircleMappingMap(Map<Long, String> circleMappingMap) {
		this.circleMappingMap = circleMappingMap;
	}

	public Map<String, String> getCategoryMappingMap() {
		return categoryMappingMap;
	}

	public void setCategoryMappingMap(Map<String, String> categoryMappingMap) {
		this.categoryMappingMap = categoryMappingMap;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ThirdPartyMappingsResponse [operatorMappingMap=").append(operatorMappingMap)
				.append(", circleMappingMap=").append(circleMappingMap).append(", categoryMappingMap=")
				.append(categoryMappingMap).append("]");
		return builder.toString();
	}

}