package com.freecharge.common.comm.rps;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.google.gson.Gson;

@Service
public class RPSService {

	private Logger logger = LoggingFactory.getLogger(RPSService.class);

	@Autowired
	@Qualifier("rpsPostTxnClient")
	private HttpClient rpsPostTxnClient;

	@Value("${rps.post.txn.api.url}")
	private String rpsPlanMappingURL;

	public ThirdPartyMappingsResponse getPlansMapping() {
		logger.info("rpsPostTxnClient : " + rpsPostTxnClient + ", rpsPlanMappingURL : " + rpsPlanMappingURL);
		GetMethod method = new GetMethod(rpsPlanMappingURL);
		// method.setParameter(BillPayBilldeskUtil.MESSAGE_PARAM_NAME, rawRequest);
		try {
			Integer statusCode = rpsPostTxnClient.executeMethod(method);
			logger.info(statusCode);
            String rawResponse = method.getResponseBodyAsString();
			logger.info(rawResponse);
			ThirdPartyMappingsResponse responseDTO = new Gson().fromJson(rawResponse,
					ThirdPartyMappingsResponse.class);
			logger.info(responseDTO);
           return responseDTO;
		} catch (Exception ex) {
			logger.error("Exception : ",ex);
			return null;
		}
	}
}
