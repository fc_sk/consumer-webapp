package com.freecharge.common.comm.fulfillment;

public class MissedCampaignDTO implements PostFulfillmentTaskDTO {
	
	private static final long serialVersionUID = 1L;


	private String orderId;
	private UserDetails userDetailsObj;
	private TransactionStatus transactionStatus;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public UserDetails getUserDetailsObj() {
		return userDetailsObj;
	}
	public void setUserDetailsObj(UserDetails userDetailsObj) {
		this.userDetailsObj = userDetailsObj;
	}
	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MissedCampaignDTO [orderId=").append(orderId).append(", userDetailsObj=").append(userDetailsObj)
				.append(", transactionStatus=").append(transactionStatus).append("]");
		return builder.toString();
	}

}
