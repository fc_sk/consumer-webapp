package com.freecharge.common.comm.fulfillment;


import com.freecharge.app.domain.entity.TxnFulfilment;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;

public class MissedTSMDTO implements PostFulfillmentTaskDTO{


	private static final long serialVersionUID = 1L;
	private String orderId;
	private Boolean shouldSmsSend;
	private Boolean isRetrigger;
	private boolean isAdminReversal;
	private UserDetails userDetailsObj;
	private TxnHomePageBusinessDO homePage;
	private TransactionStatus transactionStatus;
	private TxnFulfilment txnFulfilment;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Boolean getShouldSmsSend() {
		return shouldSmsSend;
	}

	public void setShouldSmsSend(Boolean shouldSmsSend) {
		this.shouldSmsSend = shouldSmsSend;
	}

	public Boolean getIsRetrigger() {
		return isRetrigger;
	}

	public void setIsRetrigger(Boolean isRetrigger) {
		this.isRetrigger = isRetrigger;
	}

	public UserDetails getUserDetailsObj() {
		return userDetailsObj;
	}

	public void setUserDetailsObj(UserDetails userDetailsObj) {
		this.userDetailsObj = userDetailsObj;
	}

	public TxnFulfilment getTxnFulfilment() {
		return txnFulfilment;
	}

	public void setTxnFulfilment(TxnFulfilment txnFulfilment) {
		this.txnFulfilment = txnFulfilment;
	}

	public TxnHomePageBusinessDO getHomePage() {
		return homePage;
	}

	public void setHomePage(TxnHomePageBusinessDO homePage) {
		this.homePage = homePage;
	}

	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public boolean isAdminReversal() {
		return isAdminReversal;
	}

	public void setAdminReversal(boolean isAdminReversal) {
		this.isAdminReversal = isAdminReversal;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MissedTSMDTO [orderId=").append(orderId).append(", shouldSmsSend=").append(shouldSmsSend)
				.append(", isRetrigger=").append(isRetrigger).append(", isAdminReversal=").append(isAdminReversal)
				.append(", userDetailsObj=").append(userDetailsObj).append(", homePage=").append(homePage)
				.append(", transactionStatus=").append(transactionStatus).append(", txnFulfilment=")
				.append(txnFulfilment).append("]");
		return builder.toString();
	}

}
