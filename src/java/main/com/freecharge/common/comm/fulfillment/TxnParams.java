package com.freecharge.common.comm.fulfillment;

import java.io.Serializable;

public class TxnParams implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String uniqueId;
    private String trigger;
    private int merchantId;

   

	public String getUniqueId() {
		return uniqueId;
	}



	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}



	public String getTrigger() {
		return trigger;
	}



	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}



	public int getMerchantId() {
		return merchantId;
	}



	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TxnParams [uniqueId=").append(uniqueId).append(", trigger=").append(trigger)
				.append(", merchantId=").append(merchantId).append("]");
		return builder.toString();
	}
    
    
}
