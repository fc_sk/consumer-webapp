package com.freecharge.common.comm.fulfillment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.affiliate.order.AffiliateOrder;
import com.freecharge.api.coupon.common.CouponConstants;
import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.exception.CouponExpireException;
import com.freecharge.api.coupon.service.exception.CouponOutOfStockException;
import com.freecharge.api.coupon.service.web.model.BlockCouponRequest;
import com.freecharge.api.coupon.service.web.model.BlockedCoupon;
import com.freecharge.api.coupon.service.web.model.BlockedCouponCode;
import com.freecharge.api.coupon.service.web.model.CouponOptinType;
import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.app.service.CouponService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.rest.coupon.model.AffiliateCouponDataLite;

@Service
public class AffiliateCouponFulfilmentService{

	private Logger logger = LoggingFactory.getLogger(getClass());
	
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private FCProperties fcProperties;
	
	private Map<Integer, List<Integer>> merchantCouponMap = new HashMap<>();
	
	@Autowired
	private AppConfigService appConfigService;
	
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	
	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
	
	@PostConstruct
	public void init(){
		updateMerchantMap();
		/*try{
        	scheduler.scheduleAtFixedRate(new  java.lang.Runnable() {
    			@Override
    			public void run() {
    				try{
    					updateMerchantMap();
    				}catch(Exception e){
    					logger.error("An exception occured during in-memory merchant map refresh", e);
    				}
    				
    			}
    		}, 0, 60*15, TimeUnit.SECONDS);
    	} catch(Exception e){
    		logger.warn("Thread failed to initliaze InMemoryAppConfigService in @PostConstruct.", e);
    	}*/
	}

	public void updateMerchantMap() {
		List<Integer> couponIds = appConfigService.getCouponIdsForATM();
		synchronized (merchantCouponMap) {
			if (couponIds != null){
				for (Integer couponId : couponIds){
					updateCouponInMap(couponId);
				} // end for
			} // end if
		} // end synchronized block
	}

	private void updateCouponInMap(Integer couponId) {
		if (couponId != null){
			CouponStore coupon = couponServiceProxy.getCouponStore(couponId);
			if (coupon != null){
				Integer merchantId = coupon.getMerchantId();
				if (merchantId != null){
					List<Integer> couponList = merchantCouponMap.get(merchantId);
					if (couponList == null){
						couponList = new ArrayList<>();
					} 
					couponList.add(couponId);	
					merchantCouponMap.put(merchantId, couponList);
				} // end if
			} else{
				logger.warn("Could not find active coupon for coupon id: " + couponId);
			} // end else
		}
	}
	
	public AffiliateCouponDataLite doMerchantCouponFulfilment(AffiliateOrder order) throws FulfilmentFailedException{
		String merchantIdStr = getMerchantIdForFulfilment(order);
		if (!FCUtil.isInteger(merchantIdStr)){
			logger.error("Invalid coupon merchant id found for order id: " + order.getOrderId());
		}
		Integer merchantId = Integer.parseInt(merchantIdStr);
		AffiliateCouponDataLite atmCouponData = null;
		BlockedCoupon blockedCoupon = doMerchantBlockCoupon(order.getOrderId(), merchantId);
		logger.debug("Blocked codes: " + blockedCoupon);
		if (blockedCoupon != null && blockedCoupon.getBlockedCodes() != null && blockedCoupon.getBlockedCodes().size() > 0
			&& !StringUtils.isEmpty(blockedCoupon.getBlockedCodes().get(0).getCode())){
			List<BlockedCouponCode> codes = blockedCoupon.getBlockedCodes();
			CouponStore coupon = couponService.getCouponStore(blockedCoupon.getCouponStoreId());
			if (coupon == null){
				logger.error("No coupon found with coupon id: " + blockedCoupon.getCouponStoreId());
				return null;
			}
			
			SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy");
			
			atmCouponData = new AffiliateCouponDataLite();
			atmCouponData.setCouponCode(codes.get(0).getCode());
			atmCouponData.setShortDesc(coupon.getCouponShortDesc());
			atmCouponData.setTnCUrl("https://www.freecharge.in/tnc?id=" + codes.get(0).getBlockedCouponHistoryId());
			atmCouponData.setValidity(dateFormatter.format(coupon.getValidityDate()));
		}
		logger.debug("Returning AffiliateCouponData " + atmCouponData);
		return atmCouponData;
	}
	
	public List<BlockedCoupon> doCouponsFulfilment(AffiliateOrder order)
			throws FulfilmentFailedException {
		Map<Integer, Integer> couponIdsToCountMap = getCouponIdsForFulfilment(order);
		if (couponIdsToCountMap == null) {
			logger.error("Got null couponIdsToCountMap from request for orderId: "
					+ order.getOrderId());
		}
		List<BlockedCoupon> blockedCoupons = doBlockCoupons(order.getOrderId(),
				couponIdsToCountMap);
		logger.debug("Returning blockedCoupons " + blockedCoupons);
		return blockedCoupons;
	}

	private Map<Integer, Integer> getCouponIdsForFulfilment(AffiliateOrder order) {
		Map<Integer, Integer> couponIdsToCountMap = new HashMap<>();
		if (order.getOrderData() != null){
			String couponIdsStr = String.valueOf(order.getOrderData().get(CouponConstants.KEY_COUPON_IDS));
			if (couponIdsStr == null){
				return null;
			}
			if (FCUtil.isCommaSeparatedIntegers(couponIdsStr)){
				List<Integer> couponIds = FCUtil.getIntegerList(couponIdsStr);
				if (couponIds == null){
					logger.error("Null couponIds while blocking for orderId:" + order.getOrderId());
					return null;
				}
				for (Integer couponId : couponIds){
					if (!couponIdsToCountMap.containsKey(couponId)){
						couponIdsToCountMap.put(couponId, 1);
					} else {
						Integer count = couponIdsToCountMap.get(couponId);
						++count;
						couponIdsToCountMap.put(couponId, count);
					}
				} // end for loop
			} // end if
		}
		return couponIdsToCountMap;
	}

	private BlockedCoupon doMerchantBlockCoupon(String orderId, int merchantId){
		BlockedCoupon blockedCoupon =  null;
		logger.debug(merchantCouponMap);
		List<Integer> couponIds = merchantCouponMap.get(merchantId);
		logger.debug("Blocking one of : " + couponIds);
		if (couponIds == null){
			return null;
		}
		for (Integer couponId : couponIds){
			if (couponId == null){
				continue;
			}
			BlockCouponRequest blockCouponRequest = new BlockCouponRequest();
			blockCouponRequest.setCouponOptinType(CouponOptinType.AFFILIATE);
			blockCouponRequest.setCouponStoreId(couponId);
			blockCouponRequest.setOrderId(orderId);
			blockCouponRequest.setQuantity(1);
			try{
				blockedCoupon = blockCoupon(blockCouponRequest);
				// exit for loop
				break;
			} catch(CouponOutOfStockException e){
				logger.warn("Coupon with id: " + couponId + " has gone out of stock.");
			} catch (CouponExpireException e) {
				logger.warn("Coupon with id: " + couponId + " has expired.");
			}
		}
		return blockedCoupon;		
	}
	
	private List<BlockedCoupon> doBlockCoupons(String orderId, Map<Integer, Integer> couponIdsToCountMap){
		List<BlockedCoupon> blockedCoupons =  new ArrayList<>();
		logger.debug(merchantCouponMap);
		logger.debug("Blocking one of : " + couponIdsToCountMap);
		if (couponIdsToCountMap == null){
			return null;
		}
		
		Iterator<Integer> couponIdIterator = couponIdsToCountMap.keySet().iterator();
		while (couponIdIterator.hasNext()){
			Integer couponId = couponIdIterator.next();
			if (couponId == null){
				continue;
			}
			BlockCouponRequest blockCouponRequest = new BlockCouponRequest();
			blockCouponRequest.setCouponOptinType(CouponOptinType.AFFILIATE);
			blockCouponRequest.setCouponStoreId(couponId);
			blockCouponRequest.setOrderId(orderId);
			blockCouponRequest.setQuantity(couponIdsToCountMap.get(couponId));
			try{
				BlockedCoupon blockedCoupon = blockCoupon(blockCouponRequest);
				blockedCoupons.add(blockedCoupon);
			} catch(CouponOutOfStockException e){
				logger.warn("Coupon with id: " + couponId + " has gone out of stock.");
			} catch (CouponExpireException e) {
				logger.warn("Coupon with id: " + couponId + " has expired.");
			}
		}
		return blockedCoupons;		
	}

	private String getMerchantIdForFulfilment(AffiliateOrder order) {
		String merchantId = null;
		if (order.getOrderData() != null){
			merchantId = String.valueOf(order.getOrderData().get(FCConstants.KEY_COUPON_MERCHANT_ID));
		}
		return merchantId;
	}


	private BlockedCoupon blockCoupon(BlockCouponRequest blockCouponRequest) 
			throws CouponOutOfStockException, CouponExpireException{
		BlockedCoupon blockedCoupon = null;
		couponServiceProxy.blockCouponForOrder(blockCouponRequest,null);
		List<BlockedCoupon> blockedCoupons = couponServiceProxy.getBlockedCouponListForOrder(blockCouponRequest.getOrderId());
		if (blockedCoupons != null && blockedCoupons.size() > 0){
			blockedCoupon = blockedCoupons.get(0);
		}
		return blockedCoupon;
	}
}
