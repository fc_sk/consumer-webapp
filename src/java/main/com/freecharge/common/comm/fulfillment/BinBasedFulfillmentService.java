package com.freecharge.common.comm.fulfillment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.web.model.BlockCouponRequest;
import com.freecharge.api.coupon.service.web.model.BlockedCoupon;
import com.freecharge.api.coupon.service.web.model.BlockedCouponCode;
import com.freecharge.api.coupon.service.web.model.BlockedCouponResponse;
import com.freecharge.api.coupon.service.web.model.CouponOptinType;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.app.domain.entity.jdbc.BinOffer;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.BinOfferService;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.eventprocessing.event.RechargeSuccessEvent;
import com.freecharge.growthevent.service.UserServiceProxy;

@Service("binBasedFulfilmentService")
public class BinBasedFulfillmentService implements ApplicationListener<RechargeSuccessEvent>{

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private EmailService emailService;

    @Autowired
    private OrderIdSlaveDAO orderIdSlaveDAO ;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
    
    @Autowired
    private BinOfferService binOfferService;

    @Override
    public void onApplicationEvent(RechargeSuccessEvent event) {

        String lookupId = event.getLookupId();
        String orderId = event.getOrderId();
        BinOffer binOffer =  null;
        String datamap = "";
        Map<String, String> propertyMap = new HashMap<String, String>();
        logger.info("BinBasedFulfilmentService - RechargeSuccessEvent fired with LookupID : " + lookupId + " and OrderID : " + orderId);
        List<CartItemsBusinessDO> cartItemsBusinessDOs = null;

        try{
            cartItemsBusinessDOs = orderIdSlaveDAO.getCartItemsByOrderId(orderId);      
        }
        catch(Exception e) {
            logger.error("Retrieving Cart Items failed for OrderID : " + orderId, e);
            return;
        }

        if(cartItemsBusinessDOs!= null) {
            for(CartItemsBusinessDO cartItemsBusinessDO : cartItemsBusinessDOs){
                String cartItemsEntityId = cartItemsBusinessDO.getEntityId();
                int productMasterId = cartItemsBusinessDO.getProductMasterId();
                Boolean isBinOfferAvailed = false;
                final int BINOFFER_PRODUCT_MASTER_ID = 9;

                if(cartItemsEntityId != null) {
                    isBinOfferAvailed = cartItemsEntityId.equals(FCConstants.FFREEFUND_PRODUCT_TYPE) && 
                            (productMasterId == BINOFFER_PRODUCT_MASTER_ID);
                }

                if(isBinOfferAvailed){
                    binOffer =  binOfferService.getBinOffer(cartItemsBusinessDO.getItemId());
                    break;
                }
            }
        }

        if(binOffer != null && BinOffer.OfferType.valueOf(binOffer.getOfferType()).equals(BinOffer.OfferType.REWARD)) {
            /*
             * Do coupon offer fulfillment here
             */
            try{
                datamap = binOffer.getDatamap();
                if(datamap == null) { 
                    logger.error("Reward mail sending failed for OrderId : " + orderId + ", binOfferId :" + binOffer.getId() 
                            + " as datamap is null");
                    return;                
                }      
                propertyMap = BinOffer.getStringMap(datamap, "&"); 

                if(propertyMap == null) {
                    logger.error("Reward mail sending failed for OrderId : " + orderId + ", binOfferId :" + binOffer.getId() 
                            + " as datamap provided in wrong format");
                    return;                        
                }

                if(!propertyMap.containsKey("couponStoreId") || !propertyMap.containsKey("couponQuantity") || !propertyMap.containsKey("mailerTemplate")){
                    logger.error("Reward mail sending failed for OrderId : " + orderId + ", binOfferId :" + binOffer.getId() 
                            + " as datamap missing few required properties");
                    return;                        
                }
            }
            catch (Exception e) {
                logger.error("Reward mail sending failed for OrderId : " + orderId + ", binOfferId :" + binOffer.getId() 
                        + " as datamap provided in wrong format", e);
                return;
            }


            int couponStoreId = 0;
            int couponQuantity = 0;

            BlockedCouponResponse blockedCouponResponse = null;
            BlockedCoupon blockedCoupon = null;
            List<BlockedCouponCode> couponCodes = null;
            try{
            	Integer userId = orderIdSlaveDAO.getUserIdFromOrderId(orderId);
            	BlockCouponRequest blockCouponRequest = new BlockCouponRequest();
            	blockCouponRequest.setCouponStoreId(couponStoreId);
            	blockCouponRequest.setOrderId(orderId);
            	blockCouponRequest.setQuantity(couponQuantity);
            	blockCouponRequest.setCouponOptinType(CouponOptinType.OFFER);
            	blockedCouponResponse = couponServiceProxy.blockNewCouponForOrder(blockCouponRequest,userId);
                if(blockedCouponResponse != null && blockedCouponResponse.getBlockedCoupon(couponStoreId) != null  
                		&& blockedCouponResponse.getBlockedCoupon(couponStoreId).getBlockedCodes() != null) {
                	blockedCoupon = blockedCouponResponse.getBlockedCoupon(couponStoreId);
                    if(blockedCoupon.getBlockedCodes().size() != couponQuantity) {
                        logger.error("Blocked Coupon size is not equal to the requested couponQuantity : " + couponQuantity 
                                + " for binOfferId:" + binOffer.getId() +  ", couponStoreId : " + couponStoreId + ", OrderId : " + orderId);
                        return;
                    }
                    couponCodes = blockedCoupon.getBlockedCodes(); 
                }
                else {                            
                    logger.warn("Failed to send Reward coupon email for OrderId : " + orderId + " and binOfferId :" 
                            + binOffer.getId() + " as no coupon was blocked");
                    return;
                }

            } catch(Exception e) {
                logger.error("Coupon blocking failed for OrderId : " + orderId + ", binOfferId:" + binOffer.getId() + ", couponStoreId : " 
                        + couponStoreId + " and couponQuantity :" + couponQuantity, e);
                return;
            }

            try{ 
            	List<String> codes = new ArrayList<>();
            	for (BlockedCouponCode couponCode : couponCodes){
            		codes.add(couponCode.getCode());
            	}
                Integer userId = orderIdSlaveDAO.getUserIdFromOrderId(orderId);
                Users user = userServiceProxy.getUserByUserId(userId);
                UserProfile userProfile = userServiceProxy.getUserProfile(userId);                
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat sdfValidFrom = new SimpleDateFormat("dd MMM");
                String validFromStr = sdfValidFrom.format(binOffer.getValidFrom().getTime());
                String validUptoStr = sdf.format(binOffer.getValidUpto().getTime()); 

                String mailTemplate = propertyMap.get("mailerTemplate").trim().toLowerCase();
                String subject = "";

                if(propertyMap.containsKey("mailerSubject")){
                    subject = propertyMap.get("mailerSubject").trim();
                }

                if(mailTemplate.isEmpty()){
                    logger.error("Failed to send Reward coupon email for OrderId : " + orderId + " and binOfferId :" 
                            + binOffer.getId() + " as no mailer template was set");
                    return;
                }

                if(subject.isEmpty()){
                    subject = "Here's your FreeCharge Reward!"; 
                }

                emailService.sendBinBasedCouponMail(userProfile.getFirstName(), user.getEmail(), codes, 
                        validFromStr, validUptoStr, subject, mailTemplate);
                logger.info("Reward mail sent for OrderId:" + orderId + ", binOfferId:" + binOffer.getId()+ ", couponStoreId : " 
                        + couponStoreId + " and couponQuantity :" + couponQuantity);

            }
            catch(Exception e) {
                logger.error("Reward coupon email sending failed for OrderId : " + orderId + ", binOfferId:" + binOffer.getId(), e);
                return;
            }

        }
        else {
        	if (binOffer != null){
        		logger.warn("Reward email not sent as binoffer is null or NOT of REWARD TYPE for OrderId: " 
                        + orderId + ", binOfferId:" + binOffer.getId());	
        	}
            return;
        }
    }
}
