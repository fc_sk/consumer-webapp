package com.freecharge.common.comm.fulfillment;

public class FulfilmentFailedException extends Exception {
	/**
	 * Serial version uid for this serializable class.
	 */
	private static final long serialVersionUID = 4256232376541461180L;

	public FulfilmentFailedException(String message, Exception cause) {
		super(message, cause);
	}
}
