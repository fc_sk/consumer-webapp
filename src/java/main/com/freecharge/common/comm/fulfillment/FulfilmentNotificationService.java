package com.freecharge.common.comm.fulfillment;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.freecharge.admin.service.RefundService;
import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.TxnFulfilment;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.InErrorcodeMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.app.domain.entity.jdbc.InTransactionData;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.CouponService;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.OrderService;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.app.service.RechargeRetryConfigReadThroughCache;
import com.freecharge.app.service.SuccessfulRechargeService;
import com.freecharge.app.service.TxnFulFilmentService;
import com.freecharge.app.service.UserService;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.sms.SMSService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.eventprocessing.event.FreechargeEventPublisher;
import com.freecharge.eventprocessing.event.FulfilmentEvent;
import com.freecharge.freebill.domain.BillPaymentStatus;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.google.GoogleCreditsDO;
import com.freecharge.google.GoogleCreditsService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.infrastructure.billpay.beans.BillTransactionLedger;
import com.freecharge.infrastructure.billpay.beans.ViewUserBillsResponse;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.mailer.campaign.MailerTypeEnum;
import com.freecharge.mobile.web.util.RechargeUtil;
import com.freecharge.notification.email.SendEmailRequest;
import com.freecharge.notification.service.common.INotificationToolApiService;
import com.freecharge.payment.dao.BBPSDetailsDao;
import com.freecharge.payment.dao.ConvinenceFeeDetailsDao;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.productdata.services.RechargePlanService;
import com.freecharge.recharge.MNPListener;
import com.freecharge.recharge.businessDo.SuccessfulRechargeDetails;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.recharge.util.RechargeConstants.StatusCheckType;
import com.freecharge.rest.merchandising.MerchandisingBanner;
import com.freecharge.rest.merchandising.MerchandisingService;
import com.freecharge.rest.rechargeplans.RechargePlanBusinessDO;
import com.freecharge.rest.user.bean.FailureRechargeInfo;
import com.freecharge.rest.user.bean.SuccessfulRechargeInfo;
import com.freecharge.sns.RechargeSNSService;
import com.freecharge.sns.UserDataPointSNSService;
import com.freecharge.sns.bean.RechargeAlertBean;
import com.freecharge.sns.bean.ReconMetaData;
import com.freecharge.tracker.TrackerEventVo;
import com.freecharge.tracker.api.TrackerApiService;
import com.freecharge.useragent.UserAgentController;
import com.freecharge.uuip.UserDataPointType;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.web.webdo.BBPSTransactionDetails;
import com.freecharge.web.webdo.FeeCharge;
import com.freecharge.web.webdo.FeeShare;
import com.freecharge.web.webdo.ServiceChargeEntity;
import com.snapdeal.payments.sdmoney.service.model.Balance;
import com.freecharge.app.domain.dao.jdbc.ReconMappingDao;
import com.freecharge.app.domain.entity.jdbc.ReconMapping;


/**
 * This Service class is used to perform all the fulfilment related
 * communications to the user using the notification service client
 * 
 * @author fcaa17922
 *
 */
@Service
public class FulfilmentNotificationService {

	private static final String CUSTOM_FAILURE_EMAIL_TEMPLATE_NAME_BSNL = "CustomEmailFailureTemplateBsnl";
	private static final String CUSTOM_FAILURE_EMAIL_TEMPLATE_NAME_DISH_TV = "ustomEmailFailureTemplateDishTv";
	private static final String TSM_FORCE_UPDATE_TXN_TYPE = "TransactionUpdate";


	private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private UserServiceProxy userServiceProxy;

	@Autowired
	private FreefundService freefundService;

	@Autowired
	private GoogleCreditsService googleCreditsService;

	@Autowired
	private PaymentTransactionService paymentTransactionService;

	@Autowired
	@Qualifier("billPayServiceProxy")
	private IBillPayService billPayServiceRemote;

	@Autowired
	private SMSService smsService;

	@Autowired
	private OperatorCircleService operatorCircleService;

	@Autowired
	private InService inService;

	@Autowired
	private SuccessfulRechargeService successfulRechargeService;

	@Autowired
	private RechargeNotificationService rechargeNotificationService;

	@Autowired
	private FCProperties fcProperties;

	@Autowired
	private CouponService couponService;

	@Autowired
	private PricingService pricingService;

	@Autowired
	private WalletWrapper walletWrapper;

	@Autowired
	private FreechargeEventPublisher freechargeEventPublisher;

	@Autowired
	private UserDataPointSNSService userDataPointSNSService;

	@Autowired
	private UserService userService;

	@Autowired
	private CartService cartService;

	@Autowired
	private RechargeInitiateService rechargeInitiateService;

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private KestrelWrapper kestrelWrapper;

	@Autowired
	@Qualifier("notificationApiServiceClient")
	private INotificationToolApiService notificationService;

	@Autowired
	private RechargePlanService rechargePlanService;

	@Autowired
	private TrackerApiService trackerApiService;

	@Autowired
	private UserAgentController userAgentController;

	@Autowired
	private OneCheckWalletService oneCheckWalletService;

	@Autowired
	private ConvinenceFeeDetailsDao convinenceFeeDetailsDao;

	@Autowired
	private OrderService orderService;
	
	@Autowired
	private ReconMappingDao reconMappingDao;

	@Autowired
	private BBPSDetailsDao bbpsDetailsDao;

	@Autowired
	private MNPListener mnpListener;

	@Autowired
	private BillPaymentService billPaymentService;

	@Autowired
	private RechargeSNSService rechargeSNSService;

	@Autowired
	private TxnFulFilmentService txnFulFilmentService;

	@Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;

	@Autowired
	private RefundService refundService;

	@Autowired
	private MerchandisingService merchandisingService;

	@Value("${bbps.success.transaction.email.id}")
	private String bbpsSuccessTxnEmailId;

	@Value("${utility.email.id.with.convenience.fee}")
	private String emailIdWithConvenienceFee;

	@Autowired
	protected MetricsClient metricsClient;

	@Autowired
	private RechargeRetryConfigReadThroughCache rechargeCache;
	
	public void triggerCommunication(final String orderId, final Boolean shouldSmsSend,

			final Boolean isRetrigger, UserDetails userDetailsObj, TxnHomePageBusinessDO homePage,
			TransactionStatus transactionStatus, TxnFulfilment txnFulfilment) {
		String productType = userDetailsObj.getProductType();
		List<Integer> mailerTypes = MailerTypeEnum.getIdsForProductTypes(productType);

		logger.debug("mailer types found for this product : " + productType + " is " + mailerTypes.size());

		Map<String, String> variableValues = createVariableMap(userDetailsObj, transactionStatus);

		if (ProductMaster.ProductName.MobilePostpaid == ProductMaster.ProductName
				.fromCode(userDetailsObj.getProductType())) {
			variableValues.put("isMobilePostpaid", "true");
		} else if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
			String aggregatorTxnid = getAggregatorTxnIdForBdk(userDetailsObj.getBillId());
			logger.info("product type:" + userDetailsObj.getProductType() + "\tAggregator txn id:" + aggregatorTxnid);
			if (aggregatorTxnid != null) {
				variableValues.put("aggregatorTxnid", aggregatorTxnid);
			}
			if (FCUtil.isMetroPaymentType(userDetailsObj.getProductType())) {
				variableValues.put("isMetroPay", "true");
			}
			variableValues.put("isBillPay", "true");
		}

		// condition added for google credits
		// if a trigger via admin panel contact euronet again for the codes since we are
		// not storing it
		if (FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS.equalsIgnoreCase(productType)) {
			if (isRetrigger) {
				logger.info("Admin Panel Retriggered Fulfilment for : " + orderId);
				boolean isSuccess = googleCreditsService.triggerGoogleCodeResend(orderId);
				if (!isSuccess) {
					logger.info("An error occurred in retrigger for google codes from admin panel for order id : "
							+ orderId);
					return;
				}
			}
			GoogleCreditsDO googleCredits = googleCreditsService.getGoogleCredits(orderId);
			if (googleCredits != null) {
				logger.info("The Google Credits is not null for order id : " + orderId);
				variableValues.put("googleSerial", googleCredits.getGoogleSerial());
				variableValues.put("googleCode", googleCredits.getGoogleCode());
			}
		}

		logger.info("product type user details object:" + userDetailsObj.getProductType());
		logger.info("product type from product master:"
				+ ProductMaster.ProductName.fromCode(userDetailsObj.getProductType()));

		List<PaymentTransaction> paymentTxnList = paymentTransactionService
				.getSuccessfulPaymentTransactionThroughPGByOrderId(userDetailsObj.getOrderId());
		boolean isWalletPayment = false;
		for (PaymentTransaction paymentTxn : paymentTxnList) {
			if (paymentTxn.getPaymentOption() != null && !paymentTxn.getPaymentOption()
					.equalsIgnoreCase(PaymentConstants.ONECHECK_WALLET_PAYMENT_OPTION)) {
				variableValues.put("paymentType", paymentTxn.getPaymentOption());
			} else {
				variableValues.put("walletPayment", "OCW");
			}
			if (paymentTxn.getPaymentGateway() != null
					&& (PaymentConstants.ONE_CHECK_WALLET_CODE.equalsIgnoreCase(paymentTxn.getPaymentGateway())
							|| PaymentConstants.ONECHECK_FC_WALLET_PG_NAME
									.equalsIgnoreCase(paymentTxn.getPaymentGateway())
							|| PaymentConstants.PAYMENT_GATEWAY_FCWALLET_CODE
									.equalsIgnoreCase(paymentTxn.getPaymentGateway()))) {
				isWalletPayment = true;
			}
		}
		if (isWalletPayment) {
			String fcWalletId = oneCheckWalletService
					.getOneCheckIdIfOneCheckWalletEnabled(userDetailsObj.getUserEmail());
			Balance balance = null;
			if (fcWalletId != null) {
				balance = oneCheckWalletService.getFCWalletAccountBalance(fcWalletId);
				if (balance != null) {
					variableValues.put("walletBalance", String.valueOf(balance.getTotalBalance()));
				}
			}
		}

		String operatorTxnid = getOperatorTxnId(orderId);
		if (operatorTxnid != null) {
			variableValues.put("operatorTxnId", operatorTxnid);
		}

		// SMS sending process
		String aggregatorName = null;
		if (shouldSmsSend) {
			if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
				List<BillTransactionLedger> billTxnLedger = billPayServiceRemote
						.getBillTransactionLedgers(userDetailsObj.getBillId());
				logger.info("#######   billTxnLedger   ####### : " + billTxnLedger);
				aggregatorName = billTxnLedger.get(0).getAggregator();
			}
			smsService.sendTransactionSMS(userDetailsObj, transactionStatus, variableValues, aggregatorName);
		}

		logger.info("Email sending process start with mobile no : " + userDetailsObj.getUserMobileNo()
				+ " on emailId : " + userDetailsObj.getUserEmail());
		/* Email Sending process (start) */
		sendFulfilmentEmail(orderId, userDetailsObj, homePage, transactionStatus, txnFulfilment, variableValues,
				aggregatorName);

		try {
			// Track recharge plan info for every fulfilled order(success/failure)
			if (FCUtil.isRechargeProductType(userDetailsObj.getProductType())) {
				float amt = homePage.getAmount();
				int operatorId = Integer.parseInt(homePage.getOperatorName());
				int circleId = Integer.parseInt(homePage.getCircleName());
				int productId = Integer.parseInt(homePage.getProductId());

				List<RechargePlanBusinessDO> list = rechargePlanService.getRechargePlan(circleId, operatorId, productId,
						amt);
				if (FCUtil.isEmpty(list)) {
					logger.info("No recharge plan found for [Operator, Circle, Product, Amount] = [" + operatorId + ", "
							+ circleId + ", " + productId + ", " + amt + "]");
				} else {
					RechargePlanBusinessDO rechargePlan = list.get(0);
					TrackerEventVo ev = new TrackerEventVo();
					ev.setEventName(FCConstants.RECHARGE_PLAN_EVENT_NAME);
					ev.setVal0(orderId);
					ev.setVal1(Integer.toString(rechargePlan.getRechargePlanId()));
					ev.setVal2(rechargePlan.getName());
					ev.setVal3(rechargePlan.getDenominationType());
					ev.setVal4(rechargePlan.getAmount().toPlainString());
					logger.info("Mapping data for recharge plan: " + ev.asJsonString());
					trackerApiService.trackEvent(ev, FCConstants.DUMMY_VISIT_ID);
				}
			}
		} catch (RuntimeException ex) {
			logger.error("Exception while tracking recharge plan info ", ex);
		}
	}

	public void notifyToTranslator(final String orderId, final Boolean shouldSmsSend,
			final Boolean isRetrigger, UserDetails userDetailsObj, TxnHomePageBusinessDO homePage,
			TransactionStatus transactionStatus, TxnFulfilment txnFulfilment,final Boolean isTrigerredFromAdminPanel) throws Exception {
		Long userId = null;
		String rechargeStatus = null;
		String aggregatorName = "";

		try {
			long startTimeuserServiceProxy = System.currentTimeMillis();
			Users user = userServiceProxy.getUserByEmailId(userDetailsObj.getUserEmail());
			long endTimeuserServiceProxy = System.currentTimeMillis();
			metricsClient.recordLatency("Fulfillment.userServiceProxy.", "Latency",
					endTimeuserServiceProxy - startTimeuserServiceProxy);
			userId = Long.parseLong(user.getUserId() + "");
		} catch (Exception ex) {
			logger.error("Exception " + ex);
		}

		try {
			if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
				List<BillTransactionLedger> billTxnLedger = billPayServiceRemote
						.getBillTransactionLedgers(userDetailsObj.getBillId());
				logger.info("#######   billTxnLedger   ####### : " + billTxnLedger);
				aggregatorName = billTxnLedger.get(0).getAggregator();
			}
		} catch (Exception ex) {
			logger.error("Exception " + ex);
		}

		if (inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus())) {
			rechargeStatus = RechargeAlertBean.RechargeStatus.RECHARGE_SUCCESS.getStatus();
		} else {
			if (!transactionStatus.getTransactionStatus().equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE)) {
				rechargeStatus = RechargeAlertBean.RechargeStatus.RECHARGE_FAILURE.getStatus();
			} else {
				rechargeStatus = RechargeAlertBean.RechargeStatus.RECHARGE_UNDER_PROCESS.getStatus();
			}
		}
		long startTimeNotifyRecharge = System.currentTimeMillis();
		notifyRecharge(orderId, userId, rechargeStatus, userDetailsObj, homePage, transactionStatus, aggregatorName,isTrigerredFromAdminPanel);
		long endTimeNotifyRecharge = System.currentTimeMillis();
		metricsClient.recordLatency("Fulfillment.NotifyRecharge.", "Latency",
				endTimeNotifyRecharge - startTimeNotifyRecharge);
	}


	public String getProfileNumber(String lookupID) {
		String mobileNumber = null;
		try {
			if (lookupID != null && !lookupID.isEmpty()) {
				Cart cartObj = cartService.getCartBylookUp(lookupID);
				if (cartObj != null) {
					Users userObj = userServiceProxy.getUserByUserId(cartObj.getFkUserId());
					if (userObj != null && userObj.getMobileNo() != null && !userObj.getMobileNo().isEmpty()) {
						mobileNumber = userObj.getMobileNo();
					}
				}
			}
		} catch (Exception exception) {
			logger.error("Error while fetching mobile number from cartObj :", exception);
		}
		return mobileNumber;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void sendFulfilmentEmail(final String orderId, UserDetails userDetailsObj, TxnHomePageBusinessDO homePage,
			TransactionStatus transactionStatus, TxnFulfilment txnFulfilment, Map<String, String> variableValues,
			String aggregatorName) {
		String from = null;
		String replyTo = null;
		String subject = null;
		String bcc = null;
		logger.info(
				"AnantQA transaction status: " + transactionStatus.getTransactionStatus() + " for orderId: " + orderId);

		List<Map<String, Object>> couponList = null;
		String rechargedProductType = ProductMaster.ProductName.fromCode(userDetailsObj.getProductType()).getLabel();
		long startTimeuserServiceProxy = System.currentTimeMillis();
		Users user = userServiceProxy.getUserByEmailId(userDetailsObj.getUserEmail());
		long endTimeuserServiceProxy = System.currentTimeMillis();
		metricsClient.recordLatency("Fulfillment.userServiceProxy.", "Latency",
				endTimeuserServiceProxy - startTimeuserServiceProxy);
		Long userId = Long.parseLong(user.getUserId() + "");
		Integer operatorMasterId = null;
		EmailBusinessDO rechargeReceiptBusinessDO = new EmailBusinessDO();
		if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
			try {
				long startTimeBillTransactionLedgers = System.currentTimeMillis();
				BillPayOperatorMaster bopm = billPayServiceRemote
						.getBillPayOperatorFromName(userDetailsObj.getOperatorName());
				long endTimeBillTransactionLedgers = System.currentTimeMillis();
				metricsClient.recordLatency("Fulfillment.BillTransactionLedgers.", "Latency",
						endTimeBillTransactionLedgers - startTimeBillTransactionLedgers);
				if (bopm != null) {
					operatorMasterId = bopm.getOperatorMasterId();
					variableValues.put("operatorIcon", bopm.getImgUrl());
				} else {
					variableValues.put("operatorIcon", "");
				}
			} catch (Exception e) {
				variableValues.put("operatorIcon", "");
			}
		} else {
			OperatorMaster operatorMaster = operatorCircleService.getOperator(userDetailsObj.getOperatorName());
			if (operatorMaster != null) {
				operatorMasterId = operatorMaster.getOperatorMasterId();
				variableValues.put("operatorIcon", operatorMaster.getImgUrl());
			} else {
				variableValues.put("operatorIcon", "");
			}
		}
		logger.info(
				"Captured operator:" + userDetailsObj.getOperatorName() + " ,circle:" + userDetailsObj.getCircleName());
		String rechargeStatus = null;
		Map<String, Object> emailSubjectDataMap = new HashMap<String, Object>();
		Map<String, Object> emailContentDataMap = new HashMap();
		BBPSTransactionDetails bbpsTxnDetails = bbpsDetailsDao.getBBPSDetailsForLookupId(userDetailsObj.getLookupId());
		if (inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus())) {

			if (bbpsTxnDetails != null) { // Since we have bbps details for this order, this is bbps txn
				String customerName = "NA";
				if (bbpsTxnDetails.getCustomerName() != null) {
					customerName = bbpsTxnDetails.getCustomerName();
				}
				emailContentDataMap.put("customerName", customerName);
				emailContentDataMap.put("customerMobile", getProfileNumber(userDetailsObj.getLookupId()));
				String billDueDate = "NA";
				if (bbpsTxnDetails.getBillDueDate() != null) {
					billDueDate = bbpsTxnDetails.getBillDueDate();
				}
				emailContentDataMap.put("billDueDate", billDueDate);
				String bbpsReferenceNumber = "NA";
				if (bbpsTxnDetails.getBbpsReferenceNumber() != null) {
					bbpsReferenceNumber = bbpsTxnDetails.getBbpsReferenceNumber();
				}
				emailContentDataMap.put("bbpsReferenceNumber", bbpsReferenceNumber);
				OrderId orderIdDetails = orderService.getByOrderId(orderId);
				DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String dateFormatted = formatter.format(orderIdDetails.getCreatedOn());
				emailContentDataMap.put("transactionDate", dateFormatted);
			}

			SuccessfulRechargeDetails sucDetails = new SuccessfulRechargeDetails();
			sucDetails.setEmailId(userDetailsObj.getUserEmail());
			sucDetails.setServiceNumber(userDetailsObj.getUserMobileNo());
			sucDetails.setAmount(userDetailsObj.getAmount());
			sucDetails.setProductType(userDetailsObj.getProductType());
			sucDetails.setRechargePlanType(transactionStatus.getPlanType());
			try {
				if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
					sucDetails.setOperatorId(billPayServiceRemote
							.getBillPayOperatorFromName(userDetailsObj.getOperatorName()).getOperatorMasterId());
					sucDetails.setCircleId(-1);
				} else {
					sucDetails.setOperatorId(
							operatorCircleService.getOperator(userDetailsObj.getOperatorName()).getOperatorMasterId());
					logger.info("operatorid set is " + sucDetails.getOperatorId());
					if (!FCUtil.isEmpty(userDetailsObj.getCircleName())) {
						sucDetails.setCircleId(
								operatorCircleService.getCircle(userDetailsObj.getCircleName()).getCircleMasterId());
					}
				}
				logger.info("sucDetails set operator:" + sucDetails.getOperatorId() + " ,circle:"
						+ sucDetails.getCircleId());

			} catch (Exception e) {
				logger.error(" Error while converting the operator/circle id  " + orderId, e);
			}
			if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
				logger.info("About to insert Transaction for BillId " + userDetailsObj.getBillId());
				successfulRechargeService.insertNewBillPayTransaction(sucDetails, userDetailsObj.getBillId(), orderId);
			} else {
				successfulRechargeService.insertNewTransaction(sucDetails);
			}

			// Sending PUSH Notification
			if (rechargeNotificationService.isEligibleForNotification(userDetailsObj.getProductType())) {
				long startTimeNotifyOnSuccess = System.currentTimeMillis();
				rechargeNotificationService.notifyOnSuccess(orderId, userDetailsObj, variableValues);
				long endTimeNotifyOnSuccess = System.currentTimeMillis();
				metricsClient.recordLatency("Fulfillment.NotifyOnSuccess.", "Latency",
						endTimeNotifyOnSuccess - startTimeNotifyOnSuccess);

			}

			from = fcProperties.getProperty(FCConstants.RECHARGE_SUCCESS_FROM_EMAILID);
			subject = fcProperties.getProperty(FCConstants.RECHARGE_SUCCESS_SUBJECT) + " "
					+ userDetailsObj.getOrderId();
			replyTo = fcProperties.getProperty(FCConstants.RECHARGE_SUCCESS_REPLYTO);

			SuccessfulRechargeInfo successRechargeInfo = SuccessfulRechargeInfo
					.createSuccessfullRechargeInfo(transactionStatus, userDetailsObj);
			// voucherService.blockCoupons(orderId);
			try {
				long startTimeCouponFulfillment = System.currentTimeMillis();
				couponService.doCouponFulfillment(orderId, variableValues);
				long endTimeCouponFulfillment = System.currentTimeMillis();
				metricsClient.recordLatency("Fulfillment.CouponFulfillment.", "Latency",
						endTimeCouponFulfillment - startTimeCouponFulfillment);
			} catch (Exception e) {
				logger.error("Unknow error couponFulfillment for orderId: " + orderId, e);
			}

			txnFulfilment.setTransactionStatus(1); // update successfull
													// transaction status
			txnFulfilment = updateFulfilment(txnFulfilment);

			variableValues.put("productName", rechargedProductType);
			variableValues.put("feedbackMailId", fcProperties.getCustomerSupportEmailAddress());

			logger.info("Got couponList: " + couponList);

			logger.info("Got empty coupons list for order id: " + orderId);
			setSuccessfulTansWithoutCoupTemplate(rechargeReceiptBusinessDO, userDetailsObj);

			int couponHandlingCharge = pricingService.getHandlingCharge(userDetailsObj.getProductType());
			variableValues.put("couponHandlingCharge", String.valueOf(couponHandlingCharge));
			String walletAmount = walletWrapper.getTotalBalance(userId.intValue()).toString();
			if (walletAmount.indexOf(".") != -1) {
				walletAmount = walletAmount.substring(0, walletAmount.indexOf("."));
			}
			variableValues.put("walletAmount", walletAmount);
			freechargeEventPublisher.publishRechargeSuccessEvent(successRechargeInfo, userDetailsObj.getLookupId());

			rechargeStatus = RechargeAlertBean.RechargeStatus.RECHARGE_SUCCESS.getStatus();

			// Publishing user data points for Mystique (Unique User
			// Identification Service)
			try {
				userDataPointSNSService.publishUserDataPoint(userId.intValue(), userDetailsObj.getUserMobileNo(),
						UserDataPointType.RECHARGE_NO, new Date());
			} catch (Exception ex) {
				logger.info("Exception : ", ex);
			}

		} else {
			from = fcProperties.getProperty(FCConstants.RECHARGE_FAILURE_FROM_EMAILID);
			subject = fcProperties.getProperty(FCConstants.RECHARGE_FAILURE_SUBJECT) + " " + orderId.trim();
			replyTo = fcProperties.getProperty(FCConstants.RECHARGE_FAILURE_REPLYTO);
			if (!transactionStatus.getTransactionStatus().equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE)) {
				txnFulfilment.setTransactionStatus(0);
				txnFulfilment = updateFulfilment(txnFulfilment);
				logger.info("[FULFILLMENT] <<<<<<<<<<<<      Failed Transaction Status updated      >>>>>>>>>>>>>");
				freechargeEventPublisher.publishRechargeFailureEvent(orderId, userDetailsObj.getUserMobileNo());
				checkAndInitiateRefund(userDetailsObj, transactionStatus);
				FailureRechargeInfo failureRechargeInfo = FailureRechargeInfo
						.createFailureRechargeInfo(transactionStatus, userDetailsObj);
				userService.setLatestFailureRecharge(userDetailsObj.getUserEmail(), failureRechargeInfo);
				final Double refundAmount = pricingService.getRefundAmountForOrderId(orderId);
				variableValues.put("totalRefundAmt", refundAmount.toString());
				variableValues.put("productName", rechargedProductType);
				variableValues.put("feedbackMailId", fcProperties.getCustomerSupportEmailAddress());

				addFailureReason(transactionStatus, variableValues, userDetailsObj);
				if (pricingService.hasPaidCoupons(userDetailsObj.getOrderId())) {
					variableValues.put("hasPaidCoupons", String.valueOf(true));
					variableValues.put("paidCouponAmount", String.valueOf(pricingService.getCouponCharges(orderId)));
				}
				if (cartService.hasCoupons(orderId)) {
					// todo: read it from config.
					int couponHandlingCharge = pricingService.getHandlingCharge(userDetailsObj.getProductType());
					variableValues.put("couponHandlingCharge", String.valueOf(couponHandlingCharge));
					setTemplateUnsuccessfulTransactionCoup(rechargeReceiptBusinessDO, userDetailsObj, operatorMasterId);
				} else {
					setTemplateUnsucessfulTransaction(rechargeReceiptBusinessDO, userDetailsObj, operatorMasterId);
				}
				String refundTo = rechargeInitiateService.getRefundTo(userDetailsObj.getOrderId());
				if (refundTo.equalsIgnoreCase(PaymentConstants.ONE_CHECK_WALLET_CODE)) {
					variableValues.put("refundedTo", "FreeCharge wallet");
				} else if (refundTo.equals(PaymentConstants.SOURCE_PG)) {
					variableValues.put("refundedTo", "original payment instrument");
				} else {
					variableValues.put("refundedTo", "FreeCharge Credits");
				}
				logger.info("refundedTo:" + variableValues.get("refundedTo"));
				rechargeStatus = RechargeAlertBean.RechargeStatus.RECHARGE_FAILURE.getStatus();
			} else {
				rechargeStatus = RechargeAlertBean.RechargeStatus.RECHARGE_UNDER_PROCESS.getStatus();
				if (appConfigService.isAggressiveStatusCheckEnabled()) {
					/*
					 * If it's pending then do one status check immediately just in case it
					 * succeeds.
					 */
					kestrelWrapper.enqueueForStatusCheck(orderId, StatusCheckType.Aggressive);
				}
			}
		}

		String basePath = fcProperties.getCCSBasePathSurveyUrl();
		String urlKey = fcProperties.getSurveyUrlKey();
		String[] urlList = FCUtil.getCCSValues(urlKey, basePath, fcProperties.getCCSHost());
		if (urlList != null) {
			variableValues.put("improveUrl", urlList[0]);
			variableValues.put("serveBetterUrl", urlList[1]);
			logger.info("UrlList:" + urlList + "\nimproveUrl:" + urlList[0] + "\nserveBetterurl" + urlList[1]);
		}
		rechargeReceiptBusinessDO.setVariableValues(variableValues);
		rechargeReceiptBusinessDO.setFrom(from);
		rechargeReceiptBusinessDO.setTo(userDetailsObj.getUserEmail());
		rechargeReceiptBusinessDO.setSubject(subject);
		rechargeReceiptBusinessDO.setReplyTo(replyTo);
		rechargeReceiptBusinessDO.setBcc(bcc);
		SendEmailRequest emailRequest = new SendEmailRequest();

		List<String> tags = new ArrayList<String>();

		emailContentDataMap.put("stringMap", rechargeReceiptBusinessDO.getVariableValues());
		emailContentDataMap.put("objectMap", rechargeReceiptBusinessDO.getCouponsList());

		if (rechargeReceiptBusinessDO.getCouponsList() != null) {
			emailContentDataMap.put("allCouponsCount", rechargeReceiptBusinessDO.getCouponsList().size());
		}
		try {
			if ("templates/mail/rechargeSuccessMail.vm".equals(rechargeReceiptBusinessDO.getTemplateName())) {
				emailRequest.setEmailId(fcProperties.getIntProperty(FCProperties.SUCCESS_MAIL_TRANSACTION));
			} else if ("templates/mail/newRechargeFailureMail.vm".equals(rechargeReceiptBusinessDO.getTemplateName())) {
				emailRequest.setEmailId(fcProperties.getIntProperty(FCProperties.FAILURE_MAIL_TRANSACTION));
			} else if ("templates/mail/successfulTans.vm".equals(rechargeReceiptBusinessDO.getTemplateName())) {
				emailRequest.setEmailId(fcProperties.getIntProperty(FCProperties.SUCCESS_COUPON_MAIL_TRANSACTION));
			} else if ("templates/mail/billPaymentSuccessfulTans.vm"
					.equals(rechargeReceiptBusinessDO.getTemplateName())) {
				emailRequest.setEmailId(
						fcProperties.getIntProperty(FCProperties.BILL_PAYMENT_SUCCESS_COUPON_MAIL_TRANSACTION));
			} else if ("templates/mail/unsuccessfulTransactionCoup.vm"
					.equals(rechargeReceiptBusinessDO.getTemplateName())) {
				emailRequest.setEmailId(
						fcProperties.getIntProperty(FCProperties.RECHARGE_FAILURE_WITH_COUPON_MAIL_TRANSACTION));
			} else if ("templates/mail/billPaymentUnsuccessfulTransactionCoup.vm"
					.equals((rechargeReceiptBusinessDO.getTemplateName()))) {
				emailRequest.setEmailId(
						fcProperties.getIntProperty(FCProperties.BILL_PAYMENT_FAILURE_WITH_COUPON_MAIL_TRANSACTION));
			} else if ("templates/mail/gCredRechargeSuccessMail".equals(rechargeReceiptBusinessDO.getTemplateName())) {
				emailRequest.setEmailId(
						fcProperties.getIntProperty(FCProperties.GOOGLE_CREDITS_RECHARGE_SUCCESS_MAIL_TRANSACTION));
			} else if (CUSTOM_FAILURE_EMAIL_TEMPLATE_NAME_BSNL.equals((rechargeReceiptBusinessDO.getTemplateName()))) {
				emailRequest.setEmailId(fcProperties.getIntProperty(FCProperties.CUSTOM_FAILURE_EMAIL_TEMPLATE_BSNL));
			} else if (CUSTOM_FAILURE_EMAIL_TEMPLATE_NAME_DISH_TV.equals(rechargeReceiptBusinessDO.getTemplateName())) {
				emailRequest
						.setEmailId(fcProperties.getIntProperty(FCProperties.CUSTOM_FAILURE_EMAIL_TEMPLATE_DISH_TV));
			}
			logger.info("Template Name:" + rechargeReceiptBusinessDO.getTemplateName());
			emailRequest.setContentTemplateDataMap(emailContentDataMap);
			tags.add(orderId);
			emailSubjectDataMap.put("orderno", orderId);
			if (ProductMaster.ProductName.MobilePostpaid == ProductMaster.ProductName
					.fromCode(userDetailsObj.getProductType())
					|| FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())
					|| FCUtil.isMetroPaymentType(userDetailsObj.getProductType())) {
				emailSubjectDataMap.put("productType", "bill payment");
				emailSubjectDataMap.put("operatorName", userDetailsObj.getOperatorName());
			} else {
				emailSubjectDataMap.put("productType", "Recharge");
				emailSubjectDataMap.put("operatorName", "");
			}
			logger.info("product type:" + userDetailsObj.getProductType());
			emailSubjectDataMap.put("amount", variableValues.get("rechargeamount"));
			emailSubjectDataMap.put("serviceNumber", transactionStatus.getSubscriberIdentificationNumber());

			// Using registered emailId for sending email
			emailRequest.setSubjectTemplateDataMap(emailSubjectDataMap);
			emailRequest.setToEmail(rechargeReceiptBusinessDO.getTo());
			emailRequest.setTags(tags);
			logger.info("emailRequest template id:" + emailRequest.getEmailId());
			logger.info("emailRequest to email:" + emailRequest.getToEmail());
			setBannerImageData(emailRequest, userDetailsObj.getProductType());

			FeeCharge feeCharge = convinenceFeeDetailsDao.getFeeDetails(userDetailsObj.getLookupId());
			if (appConfigService.isConvenienceFeeEnabled()
					&& "templates/mail/billPaymentSuccessfulTans.vm".equals(rechargeReceiptBusinessDO.getTemplateName())
					&& inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus())
					&& feeCharge != null) {
				emailSubjectDataMap.put("billamount", variableValues.get("rechargeamount"));

				Map<ServiceChargeEntity, FeeShare> feeShare = feeCharge.getServiceChargeShare();
				emailSubjectDataMap.put("servicetax", feeShare.get(ServiceChargeEntity.ServiceTax).getAmount());
				emailSubjectDataMap.put("swachhcess", feeShare.get(ServiceChargeEntity.SwachhBharatCess).getAmount());
				emailSubjectDataMap.put("krishicess", feeShare.get(ServiceChargeEntity.KrishiKalyanCess).getAmount());
				Double rechargeAmt = Double.valueOf(userDetailsObj.getAmount().trim());
				BigDecimal rechargeAmtBD = new BigDecimal(rechargeAmt);
				emailSubjectDataMap.put("totalAmount", feeCharge.getFeeAmount().add(rechargeAmtBD));
				emailSubjectDataMap.put("conveniencefee", feeCharge.getFeeAmount());
				emailRequest.setEmailId(Integer.valueOf(emailIdWithConvenienceFee));

			}
			if (bbpsTxnDetails != null && inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus())) {
				emailRequest.setEmailId(Integer.valueOf(bbpsSuccessTxnEmailId));
			}
			if (userDetailsObj.getChannelId() != FCConstants.CHANNEL_ID_AUTO_PAY) {
				Boolean isMailSent = notificationService.sendEmail(emailRequest);
				logger.info("is MailSent:" + isMailSent);
			}
		} catch (Exception e) {
			logger.error("Caught exception while sending email for order : " + orderId, e);
		}
		Date currentDate = new Date();
		txnFulfilment.setEmailSent(new Timestamp(currentDate.getTime()));
		txnFulfilment.setSmsSent(new Timestamp(currentDate.getTime()));
		txnFulfilment.setOrderId(orderId);

		txnFulfilment = updateFulfilment(txnFulfilment);

		logger.info("[FULFILLMENT] <<<<<<<<<<<<<<<      Fulfillment Process Completed       >>>>>>>>>>>>>>>", null);
		triggerFulfilmentEvent(transactionStatus, userDetailsObj, txnFulfilment, couponList);
		/*long startTimeNotifyRecharge = System.currentTimeMillis();
		notifyRecharge(orderId, userId, rechargeStatus, userDetailsObj, homePage, transactionStatus, aggregatorName);
		long endTimeNotifyRecharge = System.currentTimeMillis();
		metricsClient.recordLatency("Fulfillment.NotifyRecharge.", "Latency",
				endTimeNotifyRecharge - startTimeNotifyRecharge);*/

	}

	/**
	 * Private method to create a map with all the variable values required to
	 * perform the email and sms sending task
	 * 
	 * @param userDetails
	 * @param transactionStatus
	 * @return Map<String, String> - the variable value map
	 */
	private Map<String, String> createVariableMap(final UserDetails userDetails,
			final TransactionStatus transactionStatus) {
		String freeFundAmount = "0";

		if (freefundService.containsFreeFundItem(userDetails.getLookupId())) {
			freeFundAmount = freefundService.getFreeFundItemPrice(userDetails.getLookupId()) + "";
		}
		String imgPrefix = fcProperties.getProperty("imgprefix1");
		imgPrefix = imgPrefix.trim();

		String hostPrefix = fcProperties.getProperty("hostprefix");
		hostPrefix = hostPrefix.trim();

		String voucherPrefix = fcProperties.getProperty("voucherprefix");
		voucherPrefix = voucherPrefix.trim();

		Map<String, String> variableValues = new HashMap<String, String>();
		String userAgentRecordUrl = userAgentController.getRecordUrl(userDetails.getUserEmail());
		SimpleDateFormat fromDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat toDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String createdDate = null;
		try {
			Date date = fromDateFormat.parse(userDetails.getCreatedAt());
			createdDate = toDateFormat.format(date);
		} catch (ParseException e) {
			logger.error("cannot parse the date:" + userDetails.getCreatedAt(), e);
		}
		boolean isOrderIdContainsFreeFund = freefundService.doesOrderContainFreeFund(userDetails.getOrderId());
		if (isOrderIdContainsFreeFund) {
			CartItemsBusinessDO cartItemsBusinessDO = freefundService.getFreeFundItem(userDetails.getLookupId());
			if (cartItemsBusinessDO != null && cartItemsBusinessDO.getItemId() != null) {
				FreefundCoupon freefundCoupon = freefundService
						.getFreefundCoupon((long) cartItemsBusinessDO.getItemId());
				if (freefundCoupon != null) {
					variableValues.put("promocode", freefundCoupon.getFreefundCode());
					if (FreefundClass.PROMO_ENTITY_CASHBACK.equals(freefundCoupon.getPromoEntity())
							|| FreefundClass.PROMO_ENTITY_GENERIC_CASHBACK.equals(freefundCoupon.getPromoEntity())) {
						variableValues.put("isCashbackPromocode", String.valueOf(true));
					} else {
						variableValues.put("isCashbackPromocode", String.valueOf(false));
					}
				}
			}
		}
		String rechargeAmount = String.valueOf(Math.round(Double.valueOf(userDetails.getAmount().trim())));
		String firstName = userDetails.getUserFirstName();
		String customerName = null;
		if (FCUtil.isEmpty(firstName) || firstName.contains("@")) {
			customerName = "FreeCharger";
		} else {
			customerName = firstName;
		}
		variableValues.put("freefundamount", freeFundAmount);
		variableValues.put("isFreefundApplied", String.valueOf(isOrderIdContainsFreeFund));
		variableValues.put("orderno", userDetails.getOrderId().trim());
		variableValues.put("createdAt", createdDate);
		variableValues.put("rechargeamount", rechargeAmount);
		variableValues.put("mobileno", transactionStatus.getSubscriberIdentificationNumber());
		variableValues.put("operator", userDetails.getOperatorName());
		variableValues.put("productType", userDetails.getProductType());
		variableValues.put("firstname", customerName);
		variableValues.put("userEmail", userDetails.getUserEmail());
		variableValues.put("refundTime", String.valueOf(FCConstants.REFUND_RESPONSE_TIME_HOURS));
		variableValues.put("responseTime", String.valueOf(FCConstants.RECHARGE_PROCESS_TIME));
		variableValues.put("showCampaignBanner", String.valueOf(shouldShowBannerInCampaignMail()));
		variableValues.put("showCampaignCouponBanner", String.valueOf(shouldShowBannerInCouponMail()));
		variableValues.put("totalPayedAmt",
				pricingService.getPayableAmountByOrderId(userDetails.getOrderId()).toString());
		boolean hasPaidCoupons = pricingService.hasPaidCoupons(userDetails.getOrderId());
		variableValues.put("hasPaidCoupons", String.valueOf(hasPaidCoupons));
		Double couponAmount = pricingService.getCouponPayableAmountByOrderId(userDetails.getOrderId());
		Double couponWorthAmount = pricingService.getCouponWorthAmountByOrderId(userDetails.getOrderId());
		Double totalEarnedAmount = couponWorthAmount + Double.parseDouble(userDetails.getAmount().trim());
		variableValues.put("isCouponPricingEnabled", String.valueOf(fcProperties.isCouponPricingEnabled()));
		variableValues.put("totalCouponAmount", String.valueOf(couponAmount));
		variableValues.put("couponWorthAmount", String.valueOf(couponWorthAmount));
		variableValues.put("totalEarnedAmount", String.valueOf(totalEarnedAmount));
		variableValues.put("circleName", userDetails.getCircleName());
		variableValues.put("timestamp", String.valueOf(System.currentTimeMillis()));
		variableValues.put("hostPrefix", hostPrefix);
		variableValues.put("imgPrefix", imgPrefix);
		variableValues.put("voucherPrefix", voucherPrefix);
		variableValues.put("userAgentRecordUrl", userAgentRecordUrl);
		variableValues.put("userAgentTrack", String.valueOf(fcProperties.isUserAgentTrackingEnabled()));
		return variableValues;
	}

	/**
	 * Used to publish a fulfilment event using event publisher
	 * 
	 * @param transactionStatus
	 * @param userDetails
	 * @param txnFulfilment
	 * @param couponList
	 */
	private void triggerFulfilmentEvent(final TransactionStatus transactionStatus, final UserDetails userDetails,
			final TxnFulfilment txnFulfilment, final List<Map<String, Object>> couponList) {
		FulfilmentEvent fulfilmentEvent = new FulfilmentEvent(this);
		fulfilmentEvent.setOrderId(userDetails.getOrderId());
		fulfilmentEvent.setTxnFulfilmentCreatedTime(txnFulfilment.getCreatedAt());

		if (inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus())) {
			fulfilmentEvent.setRechargeNumber(userDetails.getUserMobileNo());
			fulfilmentEvent.setRechargeAmount(Double.valueOf(userDetails.getAmount()).intValue());
			fulfilmentEvent.setProductType(userDetails.getProductType());
			fulfilmentEvent.setSuccessfulRecharge(true);
			fulfilmentEvent.seteCoupons(couponList);
		} else if (!transactionStatus.getTransactionStatus()
				.equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE)) {
			fulfilmentEvent.setSuccessfulRecharge(false);
			fulfilmentEvent.seteCoupons(couponList);
		}
		freechargeEventPublisher.publish(fulfilmentEvent);
	}

	/**
	 * Used to updateReconMetaData
	 * 
	 * @param orderId
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	public boolean updateReconMetaData(String orderId)
			throws JsonGenerationException, JsonMappingException, IOException {

		String lookupId = null;
		BBPSTransactionDetails bbpsDetails = null;
		String serviceName = null;
		String refNo = null;
		RechargeAlertBean rechargeAlertBean = new RechargeAlertBean();
		ReconMetaData reconMetaData = new ReconMetaData();
		rechargeAlertBean.setReconMetaData(reconMetaData);
		rechargeAlertBean.setMessageType(TSM_FORCE_UPDATE_TXN_TYPE);
		rechargeAlertBean.setOrderId(orderId);
		TxnHomePageBusinessDO homePage = orderService.getHomePageForOrder(orderId);
		lookupId = orderService.getLookupIdForOrderId(orderId);
		bbpsDetails = bbpsDetailsDao.getBBPSDetailsForLookupId(lookupId);
		logger.info("bbpsDetails : " + bbpsDetails);
		reconMetaData.setCategoryType(homePage.getProductType());
		if (bbpsDetails != null && bbpsDetails.getIsBbpsActive()) {
			serviceName = RechargeConstants.BBPS_SERVICE_NAME;
			refNo = bbpsDetails.getBbpsReferenceNumber();
		}
		List<BillTransactionLedger> billDetails = billPayServiceRemote
				.getBillTransactionLedgers(homePage.getServiceNumber());
		if (billDetails != null) {
			BillTransactionLedger latestTxn = billDetails.get(0);

			// used to set parameters for the new reporting flow
			reconMetaData.setCategorySubtype(RechargeUtil.getAggregatorName(latestTxn.getAggregator()));
			reconMetaData.setReconId(latestTxn.getBillTransactionId());
			reconMetaData.setReferenceId(latestTxn.getAggrBillTransactionId());
			reconMetaData.setServiceName(serviceName);
			reconMetaData.setServiceReferenceNumber(refNo);
		} else {
			logger.info("NotEligible to update : " + orderId);
			return false;
		}
		rechargeAlertBean.setPlanType(FCConstants.RECHARGE_TYPE_TOPUP);
		reconMetaData.setCategoryId(FCConstants.RECHARGE_TYPE_TOPUP);
		rechargeSNSService.publish("{\"message\" : " + RechargeAlertBean.getJsonString(rechargeAlertBean) + "}");
		logger.info("Successfully published rechargeAlert bean for orderId: " + orderId);
		return true;

	}

	private void notifyRecharge(final String orderId, final Long userId, final String rechargeStatus,
			final UserDetails userDetailsObj, final TxnHomePageBusinessDO homePage,
			final TransactionStatus transactionStatus, String aggregatorName,final Boolean isTrigerredFromAdminPanel) throws Exception {
		logger.info("rechargeSNSService publish triggered for orderId : " + orderId);
		String serviceName=null;
		String refNo=null;
/*        boolean isRechargeReversal = isTrigerredFromAdminPanel;
*/
		try {
			if (RechargeAlertBean.RechargeStatus.RECHARGE_UNDER_PROCESS.getStatus().equals(rechargeStatus)) {
				return;
			}
			/*if (inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus())) {
				isRechargeReversal = false;
			}*/
			String emailId = userDetailsObj.getUserEmail();
			String oneCheckId = null;
			String lookupId = null;
			BBPSTransactionDetails bbpsDetails = null;
			try {
				oneCheckId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(emailId);
			} catch (Exception e) {
				logger.error("Caught exception in fetching oneCheckId for orderId: " + orderId, e);
			}
			
			try {
				lookupId = orderService.getLookupIdForOrderId(orderId);
				bbpsDetails = bbpsDetailsDao.getBBPSDetailsForLookupId(lookupId);
				logger.info("bbpsDetails : "+bbpsDetails);
			} catch (Exception e) {
				logger.error("Caught exception in fetching bbpsDetails for lookupId: " + lookupId, e);
			}
			
			if(bbpsDetails!=null && bbpsDetails.getIsBbpsActive()) {
				serviceName = RechargeConstants.BBPS_SERVICE_NAME;
				refNo = bbpsDetails.getBbpsReferenceNumber();
			}
			RechargeAlertBean rechargeAlertBean = new RechargeAlertBean();
			ReconMetaData reconMetaData = new ReconMetaData();
			rechargeAlertBean.setOrderId(orderId);
			rechargeAlertBean.setOneCheckId(oneCheckId);
			rechargeAlertBean.setChannel(orderService.getChannelName(userDetailsObj.getChannelId()));
			rechargeAlertBean.setServiceNumber(userDetailsObj.getUserMobileNo());
			rechargeAlertBean.setServiceProvider(userDetailsObj.getOperatorName());
			rechargeAlertBean.setServiceRegion(userDetailsObj.getCircleName());
			rechargeAlertBean.setAmount(Double.valueOf(userDetailsObj.getAmount()));
			rechargeAlertBean.setBbpsDetails(bbpsDetails);

			reconMetaData.setCategoryType(userDetailsObj.getProductType());
			boolean isMnp = mnpListener.isMNPNumber(userDetailsObj.getUserMobileNo(), homePage.getOperatorName(),
					homePage.getProductId());
			rechargeAlertBean.setIsMNP(isMnp ? "yes" : "no");
			// get cache value for lob3
			if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
				boolean lob3 = rechargeCache.getLOBKey(orderId);
				if (lob3) {
					rechargeAlertBean.setLob("ondeck_utility");
					// get the merchant id for aggregator, i.e billdesk
					String merchantId = fcProperties.getProperty(FCConstants.BILLDESK_MERCHANT_ID);
					rechargeAlertBean.setMobMerchantId(merchantId);
				}
			}

			if (FCConstants.PRODUCT_IDS_FOR_IN_REQUEST.contains(userDetailsObj.getProductType())) {
				InTransactionData inTransactionData = inService.getInTransactionData(orderId);
				rechargeAlertBean
						.setAg(RechargeUtil.getAggregatorName(inTransactionData.getInResponse().getAggrName()));
				rechargeAlertBean.setAgRequestId(
						fcProperties.getInTransIdPrefix() + inTransactionData.getInResponse().getRetryNumber() + "_"
								+ inTransactionData.getInResponse().getRequestId());
				rechargeAlertBean.setPlanType(inService.getInRequest(orderId).getRechargePlan());
				rechargeAlertBean.setOperatorTxnId(inTransactionData.getInResponse().getOprTransId());
				// used to set parameters for the new reporting flow
				reconMetaData.setCategorySubtype(
						RechargeUtil.getAggregatorName(inTransactionData.getInResponse().getAggrName()));
				reconMetaData.setCategoryId(inService.getInRequest(orderId).getRechargePlan());
				reconMetaData.setReconId(
						fcProperties.getInTransIdPrefix() + inTransactionData.getInResponse().getRetryNumber() + "_"
								+ inTransactionData.getInResponse().getRequestId());
				reconMetaData.setReferenceId(inTransactionData.getInResponse().getAggrTransId());

			} else if (userDetailsObj.getProductType().equals(FCConstants.PRODUCT_TYPE_POSTPAID)) {
				BillPaymentStatus billPaymentStatus = billPaymentService.findBillPaymentStatusByOrderId(orderId);
				rechargeAlertBean.setAg(RechargeUtil.getAggregatorName(billPaymentStatus.getBillPaymentGateway()));
				rechargeAlertBean.setAgRequestId(
						fcProperties.getInTransIdPrefix() + billPaymentStatus.getRetryNumber() + "_" + orderId);
				rechargeAlertBean.setPlanType(FCConstants.RECHARGE_TYPE_TOPUP);
				rechargeAlertBean.setOperatorTxnId(billPaymentStatus.getGatewayReferenceNumber());

				// used to set parameters for the new reporting flow
				reconMetaData
						.setCategorySubtype(RechargeUtil.getAggregatorName(billPaymentStatus.getBillPaymentGateway()));
				reconMetaData.setCategoryId(FCConstants.RECHARGE_TYPE_TOPUP);
				if (billPaymentStatus.getBillPaymentGateway()
						.equalsIgnoreCase(PaymentConstants.BILL_PAYMENT_BILLDESK_GATEWAY)
						&& !StringUtils.isEmpty(billPaymentStatus.getGatewayReferenceNumber())) {
					String reconIdForBDK = billPaymentStatus.getGatewayReferenceNumber()
							.replace(RechargeConstants.BDK_SOURCE_STRING, "");
					reconMetaData.setReconId(reconIdForBDK);

				} else {
					reconMetaData.setReconId(
							fcProperties.getInTransIdPrefix() + billPaymentStatus.getRetryNumber() + "_" + orderId);

				}
				reconMetaData.setReferenceId(billPaymentStatus.getGatewayReferenceNumber());

			} else if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
				List<BillTransactionLedger> billDetails = billPayServiceRemote
						.getBillTransactionLedgers(homePage.getServiceNumber());
				if (billDetails != null) {
					BillTransactionLedger latestTxn = billDetails.get(0);
					rechargeAlertBean.setAg(RechargeUtil.getAggregatorName(latestTxn.getAggregator()));
					rechargeAlertBean.setAgRequestId(latestTxn.getBillTransactionId());
					try {
						ViewUserBillsResponse viewUserBillsResponse = billPayServiceRemote.getUserBiller(
								homePage.getServiceNumber(), userId.intValue(), userDetailsObj.getUserMobileNo(),
								RechargeUtil.getAggregatorName(latestTxn.getAggregator()));
						logger.info(String.format("Fetched fcBillerId for userId %d billId %s : %s", userId,
								homePage.getServiceNumber(), viewUserBillsResponse.getFcBillerId()));
						rechargeAlertBean.setFcBillerId(viewUserBillsResponse.getFcBillerId());
					} catch (Exception e) {
						logger.error(String.format(
								"Unable to fetch fcBillerId for userId %d billId %s becasue of exception : ", userId,
								homePage.getServiceNumber()), e);
					}
					rechargeAlertBean.setOperatorTxnId(latestTxn.getAggrBillTransactionId());

					// used to set parameters for the new reporting flow
					reconMetaData.setCategorySubtype(RechargeUtil.getAggregatorName(latestTxn.getAggregator()));
					reconMetaData.setReconId(latestTxn.getBillTransactionId());
					reconMetaData.setReferenceId(latestTxn.getAggrBillTransactionId());
					reconMetaData.setServiceName(serviceName);
					reconMetaData.setServiceReferenceNumber(refNo);
				}
				rechargeAlertBean.setPlanType(FCConstants.RECHARGE_TYPE_TOPUP);
				reconMetaData.setCategoryId(FCConstants.RECHARGE_TYPE_TOPUP);
			}
			String rechargedProductType = ProductMaster.ProductName.fromCode(userDetailsObj.getProductType())
					.getLabel();
			rechargeAlertBean.setProduct(rechargedProductType);
			rechargeAlertBean.setStatusCode(transactionStatus.getTransactionStatus());
			rechargeAlertBean.setUserId(userId);
			rechargeAlertBean.setEmailId(emailId);
			rechargeAlertBean.setMessageType(rechargeStatus);

			// setting the parameter for recon meta data as it is required by new Reporting
			// flow
			rechargeAlertBean.setReconMetaData(reconMetaData);
			rechargeAlertBean.setRechargeReversal(isTrigerredFromAdminPanel);
			
			//Passing reconId mapping with order to new Mapping table
			insertReconMapping(orderId, reconMetaData);
			logger.info("AnantQA about to publish recharge alert bean for orderId: " + orderId + " with data: "
					+ rechargeAlertBean.toString());

			rechargeSNSService.publish("{\"message\" : " + RechargeAlertBean.getJsonString(rechargeAlertBean) + "}");
			logger.info("Successfully published rechargeAlert bean for orderId: " + orderId);
		} catch (Exception e) {
			logger.error("rechargeSNSService publish failure for orderId : " + orderId, e);
			throw e;
		}
	}
	
	private void insertReconMapping(String orderId, ReconMetaData reconMetaData) {
		try {
			ReconMapping reconMapping = new ReconMapping();
			reconMapping.setOrderId(orderId);
			reconMapping.setReconId(reconMetaData.getReferenceId());
			reconMappingDao.insertReconMapping(reconMapping);
		} catch (Exception e) {
			logger.error("Exception caught while saving recon meta data for orderId: " + orderId, e);
		}
	}

	private boolean shouldShowBannerInCampaignMail() {
		return FCUtil.doesTimeLieInBetween(fcProperties.getMailCampaignStartDate(),
				fcProperties.getMailCampaignEndDate(), new Date());
	}

	private boolean shouldShowBannerInCouponMail() {
		return FCUtil.doesTimeLieInBetween(fcProperties.getCouponCampaignStartDate(),
				fcProperties.getCouponCampaignEndDate(), new Date());
	}

	/**
	 * Used to get the aggregator transaction id for bill desk using the bill id
	 * 
	 * @param billId
	 * @return String - the bill desk aggr transaction id
	 */
	private String getAggregatorTxnIdForBdk(final String billId) {
		String aggregatorTxnId = null;
		List<BillTransactionLedger> billtxnledger = billPayServiceRemote.getBillTransactionLedgers(billId);
		if (billtxnledger != null) {
			for (BillTransactionLedger btl : billtxnledger) {
				if (btl.getAggregator().equalsIgnoreCase("bdk")) {
					aggregatorTxnId = btl.getAggrBillTransactionId();
					logger.info("Aggregator name:" + btl.getAggregator());
					logger.info("Aggregator txnId:" + btl.getAggrBillTransactionId());
				}
			}
		}
		return aggregatorTxnId;
	}

	/**
	 * Used to get the operator transaction id for an order id
	 * 
	 * @param orderId
	 * @return String - the operator transaction id
	 */
	private String getOperatorTxnId(String orderId) {
		InResponse inResponse = inService.getInResponseByOrderID(orderId);
		if (inResponse != null && inResponse.getOprTransId() != null) {
			logger.info("operator txn id:" + inResponse.getOprTransId());
			return inResponse.getOprTransId();
		}
		return null;
	}

	/**
	 * Used to update the txn fulfilment table
	 * 
	 * @param txnFulfilment
	 * @return txnFulfilment - the updated object
	 */
	private TxnFulfilment updateFulfilment(final TxnFulfilment txnFulfilment) {
		logger.info("[FULFILLMENT] Fulfillment Process : Updating TxnFulfilment table ", null);
		return txnFulFilmentService.updateTxnFulfilment(txnFulfilment);
	}

	/**
	 * Used to set the template name for successful transaction with coupon template
	 * 
	 * @param rechargeReceiptBusinessDO
	 * @param userDetailsObj
	 */
	private void setSuccessfulTansWithoutCoupTemplate(final EmailBusinessDO rechargeReceiptBusinessDO,
			final UserDetails userDetailsObj) {
		if (FCUtil.isGoogleCreditRechargeType(userDetailsObj.getProductType())) {
			rechargeReceiptBusinessDO.setTemplateName("templates/mail/gCredRechargeSuccessMail");
		} else {
			rechargeReceiptBusinessDO.setTemplateName("templates/mail/rechargeSuccessMail.vm");
		}
	}

	/**
	 * Used to initiate the refund of a particular transaction
	 * 
	 * @param userBean
	 * @param transactionStatus
	 */
	private void checkAndInitiateRefund(UserDetails userBean, TransactionStatus transactionStatus) {
		if (FCUtil.isRechargeProductType(userBean.getProductType())) {
			refundService.initiateRechargeRefund(userBean.getOrderId(), transactionStatus);
		}

		if (FCUtil.isBillPostpaidType(userBean.getProductType())) {
			if (transactionStatus.getTransactionStatus().equals("55")) {
				refundService.initiateBillPayRefund(userBean.getOrderId(), transactionStatus);
			}
		}

		if (FCUtil.isUtilityPaymentType(userBean.getProductType())) {
			refundService.initiateBillPayRefund(userBean.getOrderId(), transactionStatus);
		}
	}

	/**
	 * Used to add the failure reason to variable value map for the purpose of email
	 * and sms template
	 * 
	 * @param transactionStatus
	 * @param variableValues
	 * @param userDetails
	 */
	private void addFailureReason(TransactionStatus transactionStatus, Map<String, String> variableValues,
			UserDetails userDetails) {
		variableValues.put("failureReason", "");

		if (FCUtil.isRechargeProductType(userDetails.getProductType())) {
			InResponse inResponse = inService.getInResponseByOrderID(userDetails.getOrderId());
			List<InErrorcodeMap> inErrorcodeMap = inService.getErrorCodeMap(inResponse.getAggrRespCode(),
					transactionStatus.getServiceProvider());
			if (!FCUtil.isEmpty(inErrorcodeMap)) {
				InErrorcodeMap rechargeError = inErrorcodeMap.get(0);

				if (rechargeError.getIsDisplayEnabled() != null && rechargeError.getIsDisplayEnabled()) {
					variableValues.put("failureReason", " because of " + rechargeError.getInErrorMsg());
				}
			}
		}
	}

	/**
	 * Used to set the template name for unsuccessful transactions with coupons
	 * 
	 * @param rechargeReceiptBusinessDO
	 * @param userDetailsObj
	 * @param operatorMasterId
	 */
	private void setTemplateUnsuccessfulTransactionCoup(final EmailBusinessDO rechargeReceiptBusinessDO,
			final UserDetails userDetailsObj, Integer operatorMasterId) {
		String customTemplateName = getCustomTemplateName(userDetailsObj, operatorMasterId);
		logger.info("customTemplateName from method: " + customTemplateName);
		if (FCUtil.isNotEmpty(customTemplateName)) {
			rechargeReceiptBusinessDO.setTemplateName(customTemplateName);
		} else {
			if (FCUtil.isRechargeProductType(userDetailsObj.getProductType())) {
				rechargeReceiptBusinessDO.setTemplateName("templates/mail/unsuccessfulTransactionCoup.vm");
			}
			if (FCUtil.isBillPostpaidType(userDetailsObj.getProductType())) {
				rechargeReceiptBusinessDO.setTemplateName("templates/mail/billPaymentUnsuccessfulTransactionCoup.vm");
			}
			if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
				rechargeReceiptBusinessDO.setTemplateName("templates/mail/billPaymentUnsuccessfulTransactionCoup.vm");
			}
		}
	}

	/**
	 * Used to set the template name for unsuccessful transactions
	 * 
	 * @param rechargeReceiptBusinessDO
	 * @param userDetailsObj
	 * @param operatorMasterId
	 */
	private void setTemplateUnsucessfulTransaction(EmailBusinessDO rechargeReceiptBusinessDO,
			UserDetails userDetailsObj, Integer operatorMasterId) {
		String customTemplateName = getCustomTemplateName(userDetailsObj, operatorMasterId);
		if (FCUtil.isNotEmpty(customTemplateName)) {
			rechargeReceiptBusinessDO.setTemplateName(customTemplateName);
		} else {
			if (FCUtil.isRechargeProductType(userDetailsObj.getProductType())) {
				rechargeReceiptBusinessDO.setTemplateName("templates/mail/newRechargeFailureMail.vm");
			}
			if (FCUtil.isBillPostpaidType(userDetailsObj.getProductType())) {
				rechargeReceiptBusinessDO.setTemplateName("templates/mail/newRechargeFailureMail.vm");
			}
			if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
				rechargeReceiptBusinessDO.setTemplateName("templates/mail/newRechargeFailureMail.vm");
			}
		}
	}

	/**
	 * Used to get custom email template name
	 * 
	 * @param userDetailsObj
	 * @param operatorMasterId
	 */
	private String getCustomTemplateName(UserDetails userDetails, Integer operatorMasterId) {
		Boolean customOperator = false;
		if (operatorMasterId == 3 || operatorMasterId == 4) {
			customOperator = true;
		}
		if (operatorMasterId != null && customOperator) {
			InResponse inResponse = inService.getInResponseByOrderID(userDetails.getOrderId());
			if (appConfigService.isCustomFailureAggrRespCode(operatorMasterId, inResponse.getAggrRespCode())) {
				String customTemplateName = null;
				switch (operatorMasterId) {
				case 3:
					customTemplateName = CUSTOM_FAILURE_EMAIL_TEMPLATE_NAME_BSNL;
					break;
				case 4:
					customTemplateName = CUSTOM_FAILURE_EMAIL_TEMPLATE_NAME_DISH_TV;
					break;
				}
				return customTemplateName;
			} else {
			}
		}
		return null;
	}

	/**
	 * Used to set the banner image data for being used in the template for sending
	 * email
	 * 
	 * @param emailRequest
	 * @param productType
	 */
	private void setBannerImageData(SendEmailRequest emailRequest, String productType) {
		logger.info("Enter into setBannerImageData for productType: " + productType);
		Map<String, Object> emailContentDataMap = emailRequest.getContentTemplateDataMap();
		if (emailContentDataMap == null) {
			emailContentDataMap = new HashMap<String, Object>();
		}
		emailContentDataMap.put("emailBannerEnabled", appConfigService.isEmailBannerEnabled());
		MerchandisingBanner banner = null;
		if (productType != null) {
			banner = merchandisingService.getMerchandisingEmailBanner("productType_" + productType.toLowerCase());
		}
		if (banner == null) {
			banner = merchandisingService.getMerchandisingEmailBanner("rechargesuccess");
		}
		logger.info("Merchandising data got:" + banner);
		if (banner != null) {
			emailContentDataMap.put("bannerImageUrl", banner.getImageUrl());
			emailContentDataMap.put("bannerCtaActionUrl", banner.getCtaActionUrl());
		} else {
			emailContentDataMap.put("emailBannerEnabled", false);
		}
		emailRequest.setContentTemplateDataMap(emailContentDataMap);
	}
}
