package com.freecharge.common.comm.fulfillment;

import java.io.Serializable;
import java.util.Map;

public class UserDetails implements Serializable{

    String userEmail;
    String userMobileNo;
    String userFirstName;
    String userRechargeAmount;
    String lookupId;
    String operatorName;
    String productType;
    Integer channelId;
    String createdAt;
    String amount;
    String txnfulfilmentid;
    String orderId;
    String circleName;
    String billId;
    int operatorMasterId;
    int circleMasterId;
    private String operatorUIName;
    private String circleUIName;

    public int getOperatorMasterId() {
		return operatorMasterId;
	}

	public void setOperatorMasterId(int operatorMasterId) {
		this.operatorMasterId = operatorMasterId;
	}

	public int getCircleMasterId() {
		return circleMasterId;
	}

	public void setCircleMasterId(int circleMasterId) {
		this.circleMasterId = circleMasterId;
	}

	public String getCircleName() {
		return circleName;
	}
    
    public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTxnfulfilmentid() {
        return txnfulfilmentid;
    }

    public void setTxnfulfilmentid(String txnfulfilmentid) {
        this.txnfulfilmentid = txnfulfilmentid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserMobileNo() {
        return userMobileNo;
    }

    public void setUserMobileNo(String userMobileNo) {
        this.userMobileNo = userMobileNo;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserRechargeAmount() {
        return userRechargeAmount;
    }

    public void setUserRechargeAmount(String userRechargeAmount) {
        this.userRechargeAmount = userRechargeAmount;
    }

    public String getLookupId() {
        return lookupId;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }
    
    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
    
    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }
    

    public static UserDetails getUserDetails(Map<String, Object> userDetail, String orderId) {

        UserDetails userDetailsObj = new UserDetails();
        userDetailsObj.setUserEmail((String) userDetail.get("email"));
        userDetailsObj.setUserMobileNo((String) userDetail.get("mobileno"));
        userDetailsObj.setUserFirstName((String) userDetail.get("firstname"));
        userDetailsObj.setAmount(userDetail.get("amount").toString());
        userDetailsObj.setLookupId((String) userDetail.get("lookupId"));
        userDetailsObj.setOperatorName((String) userDetail.get("operatorName"));
        userDetailsObj.setProductType((String) userDetail.get("productType"));
        userDetailsObj.setChannelId(userDetail.get("channelId") != null ? (Integer) userDetail.get("channelId") : 1);
        userDetailsObj.setCreatedAt(userDetail.get("createdAt").toString());
        userDetailsObj.setTxnfulfilmentid(userDetail.get("txnfulfilmentid").toString());
        userDetailsObj.setCircleName(userDetail.get("circleName") != null ? (String) userDetail.get("circleName") : "");
        userDetailsObj.setBillId(userDetail.get("billId") != null ? (String) userDetail.get("billId") : "");
        userDetailsObj.setOrderId(orderId);
        userDetailsObj.setCircleMasterId(userDetail.get("circleMasterId") != null ? (Integer) userDetail.get("circleMasterId") : -1);
        userDetailsObj.setOperatorMasterId(userDetail.get("operatorMasterId") != null ? (Integer) userDetail.get("operatorMasterId") : -1);
        userDetailsObj.setOperatorUIName(userDetail.get("operatorUIName") != null ? (String) userDetail.get("operatorUIName") : "");
        userDetailsObj.setCircleUIName(userDetail.get("circleUIName") != null ? (String) userDetail.get("circleUIName") : "");
        return userDetailsObj;
    }

	public String getOperatorUIName() {
		return operatorUIName;
	}

	public void setOperatorUIName(String operatorUIName) {
		this.operatorUIName = operatorUIName;
	}

	public String getCircleUIName() {
		return circleUIName;
	}

	public void setCircleUIName(String circleUIName) {
		this.circleUIName = circleUIName;
	}

}
