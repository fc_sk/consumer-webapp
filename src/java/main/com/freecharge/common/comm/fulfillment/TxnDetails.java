package com.freecharge.common.comm.fulfillment;

import java.io.Serializable;
import java.util.Date;

public class TxnDetails implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean isFirstTxn;
    private Integer userId;
    private String userEmail;
    private String profileNumber;
    private String operatorName;
    private String circleName;
    private int    operatorMasterId;
    private int    circleMasterId;
    private String orderId;
    private Date   transactionSuccessOn;
    private String migrationStatus;
    private String oneCheckWalletId;
    private Double transactionAmount;
    private boolean promoApplied;
    
	public boolean isPromoApplied() {
		return promoApplied;
	}
	public void setPromoApplied(boolean promoApplied) {
		this.promoApplied = promoApplied;
	}
    
	public boolean isFirstTxn() {
		return isFirstTxn;
	}
	public void setFirstTxn(boolean isFirstTxn) {
		this.isFirstTxn = isFirstTxn;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getProfileNumber() {
		return profileNumber;
	}
	public void setProfileNumber(String profileNumber) {
		this.profileNumber = profileNumber;
	}
	public String getOperatorName() {
		return operatorName;
	}
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	public String getCircleName() {
		return circleName;
	}
	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}
	public int getOperatorMasterId() {
		return operatorMasterId;
	}
	public void setOperatorMasterId(int operatorMasterId) {
		this.operatorMasterId = operatorMasterId;
	}
	public int getCircleMasterId() {
		return circleMasterId;
	}
	public void setCircleMasterId(int circleMasterId) {
		this.circleMasterId = circleMasterId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Date getTransactionSuccessOn() {
		return transactionSuccessOn;
	}
	public void setTransactionSuccessOn(Date transactionSuccessOn) {
		this.transactionSuccessOn = transactionSuccessOn;
	}
	public String getMigrationStatus() {
		return migrationStatus;
	}
	public void setMigrationStatus(String migrationStatus) {
		this.migrationStatus = migrationStatus;
	}
	public String getOneCheckWalletId() {
		return oneCheckWalletId;
	}
	public void setOneCheckWalletId(String oneCheckWalletId) {
		this.oneCheckWalletId = oneCheckWalletId;
	}
	public Double getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TxnDetails [isFirstTxn=").append(isFirstTxn).append(", userId=").append(userId)
				.append(", userEmail=").append(userEmail).append(", profileNumber=").append(profileNumber)
				.append(", operatorName=").append(operatorName).append(", circleName=").append(circleName)
				.append(", operatorMasterId=").append(operatorMasterId).append(", circleMasterId=")
				.append(circleMasterId).append(", orderId=").append(orderId).append(", transactionSuccessOn=")
				.append(transactionSuccessOn).append(", migrationStatus=").append(migrationStatus)
				.append(", oneCheckWalletId=").append(oneCheckWalletId).append(", transactionAmount=")
				.append(transactionAmount).append(", promoApplied=").append(promoApplied).append("]");
		return builder.toString();
	}

}
