package com.freecharge.common.comm.fulfillment;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.HomeService;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.OrderService;
import com.freecharge.app.service.RechargeRetryConfigReadThroughCache;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.recharge.MNPListener;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.sns.RechargeSNSService;
import com.freecharge.sns.bean.RechargeAlertBean;
import com.freecharge.sns.bean.ReconMetaData;
import com.freecharge.wallet.OneCheckWalletService;

/**
 * This Service class is used to send such transactions to TSM which were killed
 * before hitting AG and not have any entry anywhere other then UTH
 * 
 * @author sandeep kumar
 *
 */
@Service
public class PreAGHitNotificationService {

	private static Logger logger = LoggingFactory.getLogger(BillPaymentService.class);

	@Autowired
	private RechargeRetryConfigReadThroughCache rechargeCache;

	@Autowired
	private MNPListener mnpListener;

	@Autowired
	private RechargeSNSService rechargeSNSService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private OneCheckWalletService oneCheckWalletService;

	@Autowired
	private FCProperties fcProperties;

	@Autowired
	private HomeService homeService;

	@Autowired
	@Qualifier("billPayServiceProxy")
	private IBillPayService billPayServiceRemote;

	@Autowired
	private UserTransactionHistoryService transactionHistoryService;

	@Autowired
	private OperatorCircleService operatorCircleService;

	@Autowired
	private InService inService;
	
	@Autowired
	private UserServiceProxy userServiceProxy;

	public void notifyPreAGHITTransactions(final String orderId) throws Exception {
		logger.info("notifyPreAGHITTransactions publish triggered for orderId : " + orderId);

		// ################### PREPARE DATA ################################### //
		Map<String, Object> userDetail = homeService.getUserRechargeDetailsFromOrderId(orderId);
		UserTransactionHistory userTransactionHistory = transactionHistoryService
				.findUserTransactionHistoryByOrderId(orderId);
		String rechargeStatus = RechargeAlertBean.RechargeStatus.RECHARGE_UNDER_PROCESS.getStatus();
		String txnStatus = userTransactionHistory.getTransactionStatus();
		Long userId = null;
		
		if (inService.isSuccessfulStatusCode(txnStatus)) {
			rechargeStatus = RechargeAlertBean.RechargeStatus.RECHARGE_SUCCESS.getStatus();
		} else if (!txnStatus.equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE)) {
			rechargeStatus = RechargeAlertBean.RechargeStatus.RECHARGE_FAILURE.getStatus();
		}
		if (userTransactionHistory != null && userTransactionHistory.getServiceProvider() != null) {
			userDetail.put("operatorName", userTransactionHistory.getServiceProvider());
			userDetail.put("circleName", userTransactionHistory.getServiceRegion());
			userDetail.put("productType", userTransactionHistory.getProductType());
			try {
				Integer operatorMasterId = null;
				Integer circleMasterId = -1;
				String operatorUIName = null;
				String circleUIName = null;

				if (FCUtil.isUtilityPaymentType(userTransactionHistory.getProductType())) {
					BillPayOperatorMaster operator = billPayServiceRemote
							.getBillPayOperatorFromName(userTransactionHistory.getServiceProvider());

					operatorMasterId = operator.getOperatorMasterId();
					operatorUIName = operator.getName();
				} else {
					OperatorMaster operator = operatorCircleService
							.getOperator(userTransactionHistory.getServiceProvider());
					CircleMaster circle = operatorCircleService.getCircle(userTransactionHistory.getServiceRegion());
					operatorMasterId = operator.getOperatorMasterId();
					operatorUIName = operator.getName();
					if (circle != null) {
						circleMasterId = circle.getCircleMasterId();
						circleUIName = circle.getName();
					}
				}

				userDetail.put("operatorMasterId", operatorMasterId);
				userDetail.put("circleMasterId", circleMasterId);
				userDetail.put("operatorUIName", operatorUIName);
				userDetail.put("circleUIName", circleUIName);
				logger.info("userDetails set operator:" + operatorMasterId + ", circle: " + circleMasterId);
			} catch (Exception e) {
				logger.error(" Error while converting the operator/circle id  " + orderId, e);
			}
		}
		UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);
		TxnHomePageBusinessDO homePage = orderService.getHomePageForOrder(orderId);

		TransactionStatus transactionStatus = new TransactionStatus();
		transactionStatus.setTransactionStatus(null);
		transactionStatus.setServiceProvider(null);
		transactionStatus.setPlanType(null);
		transactionStatus.setSubscriberIdentificationNumber(userDetailsObj.getUserMobileNo());
		transactionStatus.setOperator(userDetailsObj.getOperatorName());
		transactionStatus.setServiceRegion(userDetailsObj.getCircleName());
		transactionStatus.setProductType(userDetailsObj.getProductType());

		try {
			if (RechargeAlertBean.RechargeStatus.RECHARGE_UNDER_PROCESS.getStatus().equals(rechargeStatus)) {
				logger.info("rechargeStatus : "+rechargeStatus+", not proceeding");
				return;
			}


			
			String emailId = userDetailsObj.getUserEmail();
			String oneCheckId = null;
			String lookupId = null;
			try {
				oneCheckId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(emailId);
			} catch (Exception e) {
				logger.error("Caught exception in fetching oneCheckId for orderId: " + orderId, e);
			}

			try {
				lookupId = orderService.getLookupIdForOrderId(orderId);

			} catch (Exception e) {
				logger.error("Caught exception in fetching bbpsDetails for lookupId: " + lookupId, e);
			}

			try {
				Users user = userServiceProxy.getUserByEmailId(userDetailsObj.getUserEmail());
				userId = Long.parseLong(user.getUserId() + "");
			} catch (Exception ex) {
				logger.error("Exception " + ex);
			}

			RechargeAlertBean rechargeAlertBean = new RechargeAlertBean();
			ReconMetaData reconMetaData = new ReconMetaData();
			rechargeAlertBean.setOrderId(orderId);
			rechargeAlertBean.setOneCheckId(oneCheckId);
			rechargeAlertBean.setChannel(orderService.getChannelName(userDetailsObj.getChannelId()));
			rechargeAlertBean.setServiceNumber(userDetailsObj.getUserMobileNo());
			rechargeAlertBean.setServiceProvider(userDetailsObj.getOperatorName());
			rechargeAlertBean.setServiceRegion(userDetailsObj.getCircleName());
			rechargeAlertBean.setAmount(Double.valueOf(userDetailsObj.getAmount()));

			reconMetaData.setCategoryType(userDetailsObj.getProductType());
			boolean isMnp = mnpListener.isMNPNumber(userDetailsObj.getUserMobileNo(), homePage.getOperatorName(),
					homePage.getProductId());
			rechargeAlertBean.setIsMNP(isMnp ? "yes" : "no");
			// get cache value for lob3
			if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
				boolean lob3 = rechargeCache.getLOBKey(orderId);
				if (lob3) {
					rechargeAlertBean.setLob("ondeck_utility");
					// get the merchant id for aggregator, i.e billdesk
					String merchantId = fcProperties.getProperty(FCConstants.BILLDESK_MERCHANT_ID);
					rechargeAlertBean.setMobMerchantId(merchantId);
				}
			}

			String rechargedProductType = ProductMaster.ProductName.fromCode(userDetailsObj.getProductType())
					.getLabel();
			rechargeAlertBean.setProduct(rechargedProductType);
			rechargeAlertBean.setStatusCode(transactionStatus.getTransactionStatus());
			rechargeAlertBean.setUserId(userId);
			rechargeAlertBean.setEmailId(emailId);
			rechargeAlertBean.setMessageType(rechargeStatus);
			rechargeAlertBean.setReconMetaData(reconMetaData);
			logger.info("AnantQA about to publish recharge alert bean for orderId: " + orderId + " with data: "
					+ rechargeAlertBean.toString());

			rechargeSNSService.publish("{\"message\" : " + RechargeAlertBean.getJsonString(rechargeAlertBean) + "}");
			logger.info("Successfully published rechargeAlert bean for orderId: " + orderId);
		} catch (Exception e) {
			logger.error("rechargeSNSService publish failure for orderId : " + orderId, e);
			throw e;
		}
	}

}
