package com.freecharge.common.comm.fulfillment;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;

public class FulfillmentServiceThread extends Thread{
 
	private static Logger logger = LoggingFactory.getLogger(FulfillmentServiceThread.class);
	private boolean shouldSendSMS;
	private String orderId;
    private FulfillmentService fulfillmentService;
	 
	public FulfillmentServiceThread(String orderId, FulfillmentService fulfillmentService, boolean shouldSendSMS) {
		this.fulfillmentService = fulfillmentService;
		this.shouldSendSMS = shouldSendSMS;
		this.orderId = orderId;
		setName("Fulfillment-" + System.currentTimeMillis());
	}
	
	public void run() {
		try{
			fulfillmentService.doFulfillment(orderId, shouldSendSMS, false,false);
		}catch (Exception e) {
			logger.error("Exception occur in FulfillmentServiceThread,Cause :"+e.getCause()+"and Message :"+e.getMessage()+" "+e.toString());
		}
	}
	
}
