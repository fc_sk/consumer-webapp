package com.freecharge.common.comm.fulfillment;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.freecharge.common.comm.sms.SMSBusinessDO;
import com.freecharge.common.comm.sms.SMSService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.eventprocessing.event.RechargeSuccessEvent;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mongo.repos.LookupIdRepository;
import com.freecharge.platform.metrics.MetricsClient;
import com.mongodb.DBObject;

@Service("giftSmsService")
public class GiftSmsService implements ApplicationListener<RechargeSuccessEvent> {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private LookupIdRepository lookupIdRepository;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private SMSService smsService;
    
    @Autowired
    private UserServiceProxy userServiceProxy;

    @Override
    public void onApplicationEvent(RechargeSuccessEvent event) {
        String number = event.getNumber();
        if (number == null) {
            logger.info("No number found in RechargeSuccessEvent - Not sending Gift SMS");
            return;
        }

        String lookupId = event.getLookupId();
        if (lookupId == null) {
            logger.info("No lookupId found in RechargeSuccessEvent - Not sending Gift SMS");
            return;
        }

        // Get message from mongo collection - lookupId
        DBObject data = lookupIdRepository.getDataByLookupId(lookupId);
        if (data == null) {
            logger.debug("No document found for lookupId=" + lookupId + " - Not sending Gift SMS");
            return;
        }
        Object msgObj = data.get(LookupIdRepository.GIFTSMS_NAME);
        if (msgObj == null) {
            logger.info("No message found in document for lookupId=" + lookupId +
                        " - Not sending SMS");
            return;
        }
        String message = (String) msgObj;

        SMSBusinessDO smsBusinessDO = new SMSBusinessDO();
        smsBusinessDO.setReceiverNumber(number);
        smsBusinessDO.setText(message);
        smsBusinessDO.setRegisteredUserEmail(userServiceProxy.getEmailIdByLookupId(lookupId));

        // sending it as a promotional sms through sinfini
        try {
            if (smsService.sendSMSText(smsBusinessDO, "sinfiniSmsService")) {
                metricsClient.recordEvent("SMS", "Gift", "success");
            } else {
                logger.warn("Failed sending SMS to " + number);
                metricsClient.recordEvent("SMS", "Gift", "failure");
            }
        } catch (Exception logNForget) {
            logger.warn("Error sending SMS to " + number + ": " + logNForget);
            metricsClient.recordEvent("SMS", "Gift", "failure");
        }
    }
}
