package com.freecharge.common.comm.fulfillment;

import java.util.concurrent.Callable;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;

import com.freecharge.common.timeout.ExternalClientCallCommand;
import com.freecharge.common.timeout.TimeoutKey;

public class RechargeNotificationCommandBuilder {

	public static ExternalClientCallCommand<Integer> getNotificationServiceResponseCommand(
            final HttpClient httpClient, final HttpMethod method, final String serviceName, final String mtricName) {
        return new ExternalClientCallCommand<Integer>(serviceName, mtricName,
                new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        return httpClient.executeMethod(method);
                    }
                }, TimeoutKey.NOTIFICATION_SERVICE_TIME_OUT);
    }


}
