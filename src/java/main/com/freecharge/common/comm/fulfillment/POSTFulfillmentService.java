package com.freecharge.common.comm.fulfillment;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.web.model.BlockedCoupon;
import com.freecharge.api.coupon.service.web.model.BlockedCouponCode;
import com.freecharge.app.domain.dao.TxnFulfilmentDAO;
import com.freecharge.app.domain.dao.TxnFulfilmentReadDAO;
import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.app.domain.entity.TxnFulfilment;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.TxnFulFilmentService;
import com.freecharge.app.service.UserService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.centaur.service.common.RequestType;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.cache.CommonCacheService;
import com.freecharge.common.comm.email.EmailBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.comm.google.FetchGoogleUTMService;
import com.freecharge.common.comm.google.FetchUTMKey;
import com.freecharge.common.comm.google.GooglePostTxnService;
import com.freecharge.common.comm.rps.RPSService;
import com.freecharge.common.comm.rps.ThirdPartyMappingsResponse;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.eventprocessing.event.FreechargeEventPublisher;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.fraud.FreefundFraudConstant;
import com.freecharge.freefund.services.FreefundFulfillment;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.beans.BillTransaction;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.notification.service.common.INotificationToolApiService;
import com.freecharge.order.service.OrderDataService;
import com.freecharge.payment.dao.PostFulfillmentTaskDao;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.payment.util.ConvertorUtil;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.post.fulfillment.state.PostFulfillmentConstants;
import com.freecharge.post.fulfillment.state.PostFulfillmentTaskState;
import com.freecharge.postTxn.google.dto.GooglePostTxnDTO;
import com.freecharge.postTxn.google.dto.GooglePostTxnStatus;
import com.freecharge.postTxn.google.dto.GoogleSurface;
import com.freecharge.postTxn.google.dto.MobileRechargeDetailDTO;
import com.freecharge.postTxn.google.dto.PlanParamDTO;
import com.freecharge.recharge.cron.FulfillmentIdempotencyCache;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.sns.AmazonSNSService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.web.webdo.PostFFTaskModel;

import com.freecharge.common.businessdo.CartItemsBusinessDO;

/**
 * Service class for post Order fulfillment functionality
 */
@Service
public class POSTFulfillmentService {

   private Logger logger = LoggingFactory.getLogger(getClass());

   private static final String PLAN_CACHE = "plansCache";
   private static final String PLAN_CURRENCY = "INR";
   private static final String GOOGLE_RECHARGE_UTM_SOURCE = "google_search_recharge";
   
   @Autowired
   private TxnFulfilmentDAO txnFulfilmentDAO;

   @Autowired
   private TxnFulfilmentReadDAO txnFulfilmentReadDAO;

   @Autowired
   private EmailService emailService;

  

   @Autowired
   private TxnFulFilmentService txnFulFilmentService;

   @Autowired
   private UserService userService;

   @Autowired
   private FreechargeEventPublisher freechargeEventPublisher;

   @Autowired
   private InService inService;

   @Autowired
   private OrderDataService orderDataService;

   @Autowired
   private UserTransactionHistoryService transactionHistoryService;

   @Autowired
   private BillPaymentService billPaymentService;

  
   @Autowired
   @Qualifier("billPayServiceProxy")
   private IBillPayService billPayServiceRemote;

   @Autowired
   private FreefundFulfillment freefundFulfillment;

   @Autowired
   PaymentTransactionService paymentTransactionService;

   @Autowired
   private AmazonSNSService campaignSNSService;

   @Autowired
   WalletWrapper walletWrapper;

   @Autowired
   private CampaignServiceClient campaignServiceClient;

   @Autowired
   private OneCheckWalletService oneCheckWalletService;

   

   @Autowired
   @Qualifier("couponServiceProxy")
   private ICouponService couponServiceProxy;

   @Value("${centaur.random.coupon.experiment.id}")
   private int experimentId;
   @Value("${centaur.random.coupon.experiment.order.count}")
   private int orderCount;
   @Value("${centaur.random.coupon.experiment.couponed.order.count}")
   private int couponOrderCount;
   @Value("${centaur.random.coupon.experiment.coupon.count}")
   private int totalCouponCount;

   @Autowired
   @Qualifier("notificationApiServiceClient")
   private INotificationToolApiService notificationService;

   @Autowired
   private FulfilmentNotificationService fulfilmentNotificationService;

   @Autowired
   protected MetricsClient metricsClient;

  

   @Autowired
   private CommonCacheService commonCacheService;

   @Autowired
   private RPSService rpsService;

   @Autowired
   private FetchGoogleUTMService utmService;

   @Autowired
   private GooglePostTxnService googlePostTxnService;

   @Autowired
   private FulfillmentIdempotencyCache fulfillmentIdempotencyCache;
   
   @Autowired
   private AmazonSNSService campaignPromoSNSService;

   @Autowired
   private FCProperties   fcProperties;
   
   @Autowired
   private PostFulfillmentTaskDao postFFDao;
   
   @Value("${skipped.ff.retry.count}")
   private int skippedFFRetryCount;
   
   @Autowired
   private FreefundService   freefundService;


   public void doPostFulfillmentTasks(String orderId, Boolean shouldSmsSend, Boolean isRetrigger,
			UserDetails userDetailsObj, TxnHomePageBusinessDO homePage, TransactionStatus transactionStatus,
			UserTransactionHistory userTransactionHistory,final Boolean isTrigerredFromAdminPanel) {
	   logger.info("executing post fulfillment tasks");
	// capture recharge success event
       logger.info("Capturing Recharge Success Event for orderId " + orderId);
       captureRechargeSuccessEvent(orderId, transactionStatus.getTransactionStatus(), userDetailsObj);

       logger.info("Capturing in transaction data for orderId " + orderId);
       logger.info("userDetailsObj: " + userDetailsObj);
       publishToCampaign(orderId,userDetailsObj,transactionStatus);

       freechargeEventPublisher.publishFulfillmentEvent(transactionStatus, userDetailsObj);
       feedToIdempotencyCache(orderId,transactionStatus);
       logger.info("Logging for metrics");

       TxnFulfilment txnFulfilment = getTxnFulfillmentId(userDetailsObj);

       initiateCommunication(orderId, shouldSmsSend, isRetrigger, userDetailsObj, homePage, transactionStatus, txnFulfilment);
       
       notifyToTranslator(orderId, shouldSmsSend, isRetrigger,
               userDetailsObj, homePage, transactionStatus, txnFulfilment,isTrigerredFromAdminPanel);
      
       postToGoogle(orderId, transactionStatus, homePage, userTransactionHistory);
	   logger.info("post fulfillment tasks executed");


	   
   }
   
   
	private boolean notifyToTranslator(String orderId, Boolean shouldSmsSend, Boolean isRetrigger,
			UserDetails userDetailsObj, TxnHomePageBusinessDO homePage, TransactionStatus transactionStatus,
			TxnFulfilment txnFulfilment,final Boolean isTrigerredFromAdminPanel) {
		try {
			if (txnFulfilment != null) {

				fulfilmentNotificationService.notifyToTranslator(orderId, shouldSmsSend, isRetrigger,
						userDetailsObj, homePage, transactionStatus, txnFulfilment,isTrigerredFromAdminPanel);
				return true;

			}
		} catch (Exception ex) {
			logger.error("Exception notifying translator : " + ex);
			MissedTSMDTO dto = getMissedTSMDTO(orderId, shouldSmsSend, isRetrigger, userDetailsObj, homePage,
					transactionStatus, txnFulfilment);
			dto.setAdminReversal(isTrigerredFromAdminPanel);
			markAsSkipped(PostFulfillmentTaskState.TSM_NOTIFICATION_MISSED, dto, orderId, ex.getMessage());
			logger.info("Exception : ", ex);
		}
		return false;
	}
	
	private boolean initiateCommunication(String orderId, Boolean shouldSmsSend, Boolean isRetrigger,
			UserDetails userDetailsObj, TxnHomePageBusinessDO homePage, TransactionStatus transactionStatus,
			TxnFulfilment txnFulfilment) {
		try {
			if (txnFulfilment != null) {

				fulfilmentNotificationService.triggerCommunication(orderId, shouldSmsSend, isRetrigger,
						userDetailsObj, homePage, transactionStatus, txnFulfilment);
				return true;

			}
		} catch (Exception ex) {
			logger.error("Exception notifying communicator : " + ex);
			MissedTSMDTO dto = getMissedTSMDTO(orderId, shouldSmsSend, isRetrigger, userDetailsObj, homePage,
					transactionStatus, txnFulfilment);
			markAsSkipped(PostFulfillmentTaskState.COMM_NOTIFICATION_MISSED, dto, orderId, ex.getMessage());
			logger.info("Exception : ", ex);
		}
		return false;
	}

 	private TxnFulfilment getTxnFulfillmentId(UserDetails userDetailsObj) {
 		TxnFulfilment txnFulfilment = null;
 		try {
             if (!StringUtils.isEmpty(userDetailsObj.getTxnfulfilmentid())) {
                txnFulfilment = txnFulFilmentService.read(Integer.parseInt(userDetailsObj.getTxnfulfilmentid()));
             }
          } catch (Exception ex) {
             logger.error("Exception finding txnFulfilment : " + ex);

          }
 		return txnFulfilment;
 }


 	private void feedToIdempotencyCache(String orderId, TransactionStatus transactionStatus) {
 		try {
 			if (inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus()) || inService.isPending(transactionStatus.getTransactionStatus())) {

 				logger.info("Coming to enqueue order id in idempotentency cache " + orderId);
 				fulfillmentIdempotencyCache.markEnqueue(orderId);
 				logger.info("Successfully enqueued order id in idempotentency cache " + orderId);
 			}
 		} catch (Exception e) {
 			logger.info("Exception in adding order id to idempotency cache");
 		}
 	}




 public boolean publishToCampaign(String orderId,UserDetails userDetailsObj,TransactionStatus  transactionStatus) {
	 try {

	 	  
	        //
	        logger.info("Capturing in transaction data for orderId " + orderId);

	        Random rand = new Random();
	        int randInt = rand.nextInt(100);
	        int flowPercentage = fcProperties.getFlowPercentage();
	        logger.info("#######  flowPercentage  #######"+flowPercentage+"  , randInt ### : "+randInt);
	        if(randInt >= flowPercentage) {
	            storeInTxnData(orderId, userDetailsObj);
	            if (inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus())) {
	                logger.info("[CampaignService] Processing eligible campaigns for orderId " + orderId);
	                publishCampaignCheckMessage(userDetailsObj.getLookupId());
	            }

	            freefundFulfillment.handleFreefundFulfillment(orderId, userDetailsObj.getLookupId(),
	                    transactionStatus.getTransactionStatus());

			} else {
				if (inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus())) {
					logger.info("userDetailsObj : "+userDetailsObj);
					CampaignTxnDTO txnDataMap = getInTxnDataMap(orderId, userDetailsObj);
					String serialTxnDataMap = serializeTxnDataMap(txnDataMap);
					logger.info("Publishing to campaign SNS #####  " + serialTxnDataMap);
					publishPromoCampaignMessage(serialTxnDataMap);
				}
	        }      

	    return true;
	 }catch(Exception ex) {
		 logger.error("Exception while notifying campaign : "+ex);
		 MissedCampaignDTO dto=getMissedCampaignDTO(orderId,userDetailsObj,transactionStatus);
		 markAsSkipped(PostFulfillmentTaskState.CAMPAIGN_NOTIFICATION_MISSED,dto,orderId, ex.getMessage());
		 return false;
	 }
 }
    
	private boolean deduceIfPromoApplied(String lookupId) {
		CartItemsBusinessDO freefundCartItem = freefundService.getFreeFundItem(lookupId);
		if (freefundCartItem != null) {
			Long freefundCouponId = freefundCartItem.getItemId().longValue();
			FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon(freefundCouponId);
			if (freefundCoupon != null) {
				return true;
			}
		}
		return false;
	}


	private void markAsSkipped(PostFulfillmentTaskState state, PostFulfillmentTaskDTO dto, String orderId, String errorMsg) {		
		
		PostFFTaskModel postFFTaskModel = postFFDao.findByOrderIdAndStatus(orderId,state);
		String rawRequest = null;
		try {
		    rawRequest=ConvertorUtil.getStrFromObj(dto);
		} catch(Exception e) {
			logger.error("JsonProcessingException : ",e);
			return;
		}
		switch (state) {
		case CAMPAIGN_NOTIFICATION_MISSED:
		case CAMPAIGN_NOTIFICATION_IN_PROGRESS: 
			updateSkippedRecord(state, orderId, errorMsg, postFFTaskModel, rawRequest);
		break;
		case COMM_NOTIFICATION_MISSED:
		case COMM_NOTIFICATION_IN_PROGRESS: {
			updateSkippedRecord(state, orderId, errorMsg, postFFTaskModel, rawRequest);
		}
		break;
		case TSM_NOTIFICATION_MISSED:
		case TSM_NOTIFICATION_IN_PROGRESS: 
			updateSkippedRecord(state, orderId, errorMsg, postFFTaskModel, rawRequest);
		break;
		case TSM_NOTIFICATION_FAILED:
		case TSM_NOTIFICATION_PROCESSED: 
		case CAMPAIGN_NOTIFICATION_FAILED:
		case CAMPAIGN_NOTIFICATION_PROCESSED:
		case COMM_NOTIFICATION_FAILED:
		case COMM_NOTIFICATION_PROCESSED: {
			logger.info("IllegalStateToRetry : "+state);
			return;
		}
		default: {
			logger.info("invalid state : " + state);
		}
		}
	}


    // open for extension
	protected void updateSkippedRecord(PostFulfillmentTaskState state, String orderId, String errorMsg,
			 PostFFTaskModel postFFTaskModel, String rawRequest) {
		if(postFFTaskModel==null) {
		     postFFDao.insertSkippedTask(rawRequest, state, orderId, state.name(), new Date());
	     } else{
			postFFDao.incrementRetryCount(state, String.format(postFFTaskModel.getState().getMsg(),errorMsg),
					postFFTaskModel.getId());
		}
	}


	private MissedCampaignDTO getMissedCampaignDTO(String orderId, UserDetails userDetailsObj,
		TransactionStatus transactionStatus) {
    	MissedCampaignDTO dto=new MissedCampaignDTO();
    	dto.setOrderId(orderId);
    	dto.setTransactionStatus(transactionStatus);
    	dto.setUserDetailsObj(userDetailsObj);
	return dto;
}


	   private MissedTSMDTO getMissedTSMDTO(String orderId, Boolean shouldSmsSend, Boolean isRetrigger,
		 		UserDetails userDetailsObj, TxnHomePageBusinessDO homePage, TransactionStatus transactionStatus,
		 		TxnFulfilment txnFulfilment) {
			MissedTSMDTO missedTSMDTO = new MissedTSMDTO();
			missedTSMDTO.setHomePage(homePage);
			missedTSMDTO.setIsRetrigger(isRetrigger);
			missedTSMDTO.setShouldSmsSend(shouldSmsSend);
			missedTSMDTO.setTransactionStatus(transactionStatus);
			missedTSMDTO.setUserDetailsObj(userDetailsObj);
			missedTSMDTO.setOrderId(orderId);
			missedTSMDTO.setTxnFulfilment(txnFulfilment);
			return missedTSMDTO;
		}	
	
	


	public void publishPromoCampaignMessage(String serializedObject) {
 	        campaignPromoSNSService.publish(serializedObject);
 	        logger.info("[CampaignService] async call made to campaign with details: "+serializedObject);
 	   
 	}
    
    private String serializeTxnDataMap(CampaignTxnDTO campaignTxnDTO) throws JsonProcessingException {
    	logger.info("CampaignTxnDTO ### : "+ campaignTxnDTO);
 	    ObjectMapper mapper = new ObjectMapper();
 	    String result = mapper.writeValueAsString(campaignTxnDTO);
    	logger.info("result ### : "+ result);

 	    return result;
 	}

 	private CampaignTxnDTO getInTxnDataMap(String orderId, UserDetails userDetailsObj) {
 	    CampaignTxnDTO campaignTxnDTO = new CampaignTxnDTO();
 	    campaignTxnDTO.setTxnDetails(getTxnDetails(orderId, userDetailsObj));
 	    campaignTxnDTO.setTxnParams(getTxnParams(userDetailsObj));
 	    return campaignTxnDTO;
 	}
 	private Users getUserObjFromEmail(String userEmail) {
 		   try {
 		      long startTimeUserService = System.currentTimeMillis();
 		      Users user = userService.getUser(userEmail);
 		      long endTimeUserService = System.currentTimeMillis();
 		      metricsClient.recordLatency("Fulfillment.userService.getUser.", "Latency", endTimeUserService - startTimeUserService);
 		      return user;
 		   } catch (Exception e) {
 		      logger.error("Exception in getting user details for emailId " + userEmail + " so skipping save intxndata");
 		   }
 		   return null;
 		}


 		private boolean isFirstTxn(Integer userId) {
 		       //new User condition
 		       List<String> orders = transactionHistoryService.getRechargeSuccessDistinctOrderIdsByUserId(userId, 2);
 		       if (null == orders || orders.size() < 2) { // because current transaction will always be there since this is after recharge success
 		          return true;
 		       } else {
 		          return false;
 		       }
 		   }
 	private TxnDetails getTxnDetails(String orderId, UserDetails userDetailsObj) {

 	      TxnDetails txnDetails = new TxnDetails();
 	     txnDetails.setPromoApplied(deduceIfPromoApplied(userDetailsObj.getLookupId()));

 	   Users user = getUserObjFromEmail(userDetailsObj.getUserEmail());
 	      UserTransactionHistory userTransactionHistory = transactionHistoryService
 	              .findUserTransactionHistoryByOrderId(orderId);

 	   if(user==null) {
 	      return null;
 	   }
 	   
 	boolean isFirstTxn = isFirstTxn(user.getUserId());

 	   txnDetails.setFirstTxn(isFirstTxn);
 	   txnDetails.setUserId(user.getUserId());
 	   txnDetails.setUserEmail(userDetailsObj.getUserEmail());
 	   txnDetails.setProfileNumber(user.getMobileNo());
 	   txnDetails.setOperatorName(userDetailsObj.getOperatorUIName());
 	   txnDetails.setCircleName(userDetailsObj.getCircleUIName());
 	   txnDetails.setOperatorMasterId(userDetailsObj.getOperatorMasterId());
 	   txnDetails.setCircleMasterId(userDetailsObj.getCircleMasterId());
 	   txnDetails.setOrderId(orderId);
 	   txnDetails.setTransactionSuccessOn(new Date());
 	   txnDetails.setTransactionAmount(userTransactionHistory.getTransactionAmount());

 	   MigrationStatus migrationStatus = getMigrationStatus(userDetailsObj.getUserEmail());

 	   String oneCheckWalletId = getOneCheckWalletIdFromEmail(userDetailsObj.getUserEmail());

 	   if (null != migrationStatus) {
 	      txnDetails.setMigrationStatus(migrationStatus.getMigrationStatus());
 	   }
 	   if (null != oneCheckWalletId) {
 	      txnDetails.setOneCheckWalletId(oneCheckWalletId);
 	   }
 	   return txnDetails;
 	  }

 	  private TxnParams getTxnParams(UserDetails userDetailsObj) {
 	      TxnParams txnParams = new TxnParams();
 	      txnParams.setMerchantId(FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID);
 	      txnParams.setUniqueId(userDetailsObj.getLookupId());
 	      txnParams.setTrigger("RECHARGE_SUCCESS");
 	      return txnParams;
 	  }
 	  private MigrationStatus getMigrationStatus(String userEmail) {
 	       MigrationStatus migrationStatus = oneCheckWalletService.getFcWalletMigrationStatus(userEmail, null);
 	       logger.info("Migration STatus for Email "+userEmail + " is"+ migrationStatus);
 	       return migrationStatus;
 	}


 	private String getOneCheckWalletIdFromEmail(String userEmail) {
 	       String oneCheckWalletId = null;
 	       try {
 	          logger.info("Fetching oneCheckWalletId  for user " + userEmail);
 	          oneCheckWalletId = oneCheckWalletService.getOneCheckIdIfOneCheckWalletEnabled(userEmail);
 	       } catch (Exception e) {
 	          logger.error("error fetching oneCheckWalletId for email " + userEmail, e);
 	       }
 	       logger.info("For emailId " + userEmail + ". OneCheckWalletId is "
 	             + oneCheckWalletId );
 	       return oneCheckWalletId;

 	   }

    private void postToGoogle(String orderId, TransactionStatus transactionStatus, TxnHomePageBusinessDO homePage,
          UserTransactionHistory userTransactionHistory) {
       try {
          Map<String, String> result = utmService.fetchOrderAttributes(FetchUTMKey.ORDER_ID, orderId);
          logger.info("result ### : " + result);

          if (!CollectionUtils.isEmpty(result)) {
             logger.info("result ### : " + result);

             String refId = result.get(RechargeConstants.UTM_REF_KEY);
             String utmSource = result.get(RechargeConstants.UTM_SOURCE_TYPE_KEY);
             String channelId = result.get(RechargeConstants.UTM_SOURCE_CHANNEL_ID);
             if (inService.isPending(transactionStatus.getTransactionStatus()) || StringUtils.isEmpty(refId) || StringUtils.isEmpty(utmSource)
                   || !utmSource.equalsIgnoreCase(GOOGLE_RECHARGE_UTM_SOURCE)) {
                logger.info("Not eligible to post to google");
                return;
             }
             ThirdPartyMappingsResponse responseDTO = getRPSPlansMapping();
             logger.info("responseDTO : " + responseDTO);
             GooglePostTxnDTO gptDTO = createGooglePostTxnDTO(responseDTO, orderId, homePage, transactionStatus,
                   userTransactionHistory,channelId);
             
				if (inService.isFailure(transactionStatus.getTransactionStatus())) {
					gptDTO.getMobileRechargeDetails()
							.setErrorCode(RechargeConstants.GOOGLE_RECHARGES_FAILURE_ERROR_CODE);
				}
				
             logger.info("gptDTO : " + gptDTO);
             googlePostTxnService.post(orderId, gptDTO, refId);
          }
       } catch (Exception ex) {
          logger.info("Exception Posting Recharge to google : " + ex);
       }

    }

 

	private GooglePostTxnDTO createGooglePostTxnDTO(ThirdPartyMappingsResponse responseDTO, String orderId,
          TxnHomePageBusinessDO homePage, TransactionStatus transactionStatus,
          UserTransactionHistory userTransactionHistory,String channelId) {
       if (responseDTO == null || CollectionUtils.isEmpty(responseDTO.getCategoryMappingMap())
             || CollectionUtils.isEmpty(responseDTO.getCircleMappingMap())
             || CollectionUtils.isEmpty(responseDTO.getOperatorMappingMap())) {
          throw new RuntimeException("Invalid Data From RPS");
       }
       GooglePostTxnDTO gptDTO = new GooglePostTxnDTO();
       if (inService.isSuccessfulStatusCode(transactionStatus.getTransactionStatus())) {
          gptDTO.setStatus(GooglePostTxnStatus.SUCCESSFUL.name());
       } else if (inService.isFailure(transactionStatus.getTransactionStatus())) {
          gptDTO.setStatus(GooglePostTxnStatus.FAILED.name());
       } else {
          gptDTO.setStatus(GooglePostTxnStatus.PENDING.name());
       }

       MobileRechargeDetailDTO md = new MobileRechargeDetailDTO();
       String operatorIdStr = homePage.getOperatorName();
       String circleIdStr = homePage.getCircleName();
       Long operatorId = null;
       Long circleId = null;
       if (!StringUtils.isEmpty(operatorIdStr)) {
          try {
             operatorId = Long.parseLong(operatorIdStr);
          } catch (Exception ex) {
             logger.error("INVALID ORERATOR-ID");
          }
       }

       if (!StringUtils.isEmpty(circleIdStr)) {
          try {
             circleId = Long.parseLong(circleIdStr);
          } catch (Exception ex) {
             logger.error("INVALID CIRCLE-ID");

          }
       }
       String surface = GoogleSurface.getSurfaceByChannel(channelId);
       gptDTO.setSurface(surface);

       logger.info("Google Surface => " + surface + " using channel id from dynamo channel id => " + channelId);
       
       PlanParamDTO pd = new PlanParamDTO();
       pd.setAmount(userTransactionHistory.getTransactionAmount().toString());
       pd.setCurrency(PLAN_CURRENCY);
       pd.setOperatorId(responseDTO.getOperatorMappingMap().get(operatorId));
       pd.setPhone(userTransactionHistory.getSubscriberIdentificationNumber());
       pd.setPlanId(inService.getInRequest(orderId).getRechargePlan());
       pd.setPrice(userTransactionHistory.getTransactionAmount().toString());
       pd.setSubregionId(responseDTO.getCircleMappingMap().get(circleId));
       md.setPlanParams(pd);
       gptDTO.setMobileRechargeDetails(md);
       gptDTO.setCreateTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(new Date()));
       return gptDTO;
    }

    private ThirdPartyMappingsResponse getRPSPlansMapping() {
       ThirdPartyMappingsResponse responseDTO = (ThirdPartyMappingsResponse) commonCacheService.get(PLAN_CACHE);
       if (responseDTO == null) {
          logger.info("getting from service");
          responseDTO = rpsService.getPlansMapping();
          logger.info("RPS responseDTO : " + responseDTO);
          commonCacheService.set(PLAN_CACHE, responseDTO);

       }
       return responseDTO;
    }

    // capture recharge success event
    private void captureRechargeSuccessEvent(String orderId, String status, UserDetails userDetailsObj) {
      try {
     	 Map<String, Object> userInfo = new HashMap<String, Object>();
          if (inService.isSuccessfulStatusCode(status)) {

             String fcChannel = String.valueOf(userDetailsObj.getChannelId());

             userInfo.put(FreefundFraudConstant.TXN_CHANNEL, fcChannel);
             userInfo.put(FCConstants.GROWTH_EVENT_USER_EMAIL, userDetailsObj.getUserEmail());
             userInfo.put(FreefundFraudConstant.PRODUCT_TYPE, userDetailsObj.getProductType());
             userInfo.put(FreefundFraudConstant.ORDERID, orderId);
             userInfo.put(FCConstants.RECHARGE_AMOUNT, Float.parseFloat(userDetailsObj.getAmount()));
             userInfo.put(FreefundFraudConstant.SERVICE_NUMBER, userDetailsObj.getUserMobileNo());
             userInfo.put(FCConstants.OPERATOR, userDetailsObj.getOperatorName());
          }
      }catch(Exception ex) {
     	 logger.error("Exception capturing RechargeSuccessEvent , although this method is doing nothing: ",ex);
      }
    }

    public void storeInTxnData(String orderId, UserDetails userDetailsObj) {
       HashMap<String, Object> transactionInfo = new HashMap<String, Object>();
       Users user = null;
       try {
          long startTimeUserService = System.currentTimeMillis();
          user = userService.getUser(userDetailsObj.getUserEmail());
          long endTimeUserService = System.currentTimeMillis();
          metricsClient.recordLatency("Fulfillment.userService.getUser.", "Latency",
                endTimeUserService - startTimeUserService);
       } catch (Exception e) {
          logger.error("Exception in getting user details for emailId " + userDetailsObj.getUserEmail()
                + " so skipping save intxndata");
       }
       if (null != user) {
          Integer userId = user.getUserId();
          // new User condition
          List<String> orders = transactionHistoryService.getRechargeSuccessDistinctOrderIdsByUserId(userId, 2);

          if (null == orders || orders.size() < 2) { // because current transaction will always be there since this is
                                           // after recharge success
             transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, true);
          } else {
             transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, false);
          }
          transactionInfo.put(InTxnDataConstants.USER_ID, userId);
          transactionInfo.put(InTxnDataConstants.USER_EMAIL, userDetailsObj.getUserEmail());
          transactionInfo.put(InTxnDataConstants.PROFILE_NUMER, user.getMobileNo());
          transactionInfo.put(InTxnDataConstants.OPERATOR_NAME, userDetailsObj.getOperatorUIName());
          transactionInfo.put(InTxnDataConstants.CIRCLE_NAME, userDetailsObj.getCircleUIName());
          transactionInfo.put(InTxnDataConstants.OPERATOR_MASTER_ID, userDetailsObj.getOperatorMasterId());
          transactionInfo.put(InTxnDataConstants.CIRCLE_MASTER_ID, userDetailsObj.getCircleMasterId());
          MigrationStatus migrationStatus = oneCheckWalletService
                .getFcWalletMigrationStatus(userDetailsObj.getUserEmail(), null);
          String oneCheckWalletId = null;
          try {
             logger.info("Fetching oneCheckWalletId  for user " + userId);
             oneCheckWalletId = oneCheckWalletService
                   .getOneCheckIdIfOneCheckWalletEnabled(userDetailsObj.getUserEmail());
          } catch (Exception e) {
             logger.error("error fetching oneCheckWalletId for email " + userDetailsObj.getUserEmail(), e);
          }
          logger.info("For emailId " + userDetailsObj.getUserEmail() + ". OneCheckWalletId is " + oneCheckWalletId
                + " and migrationStatus is " + migrationStatus);
          if (null != migrationStatus) {
             transactionInfo.put(InTxnDataConstants.MIGRATION_STATUS, migrationStatus.getMigrationStatus());
          }
          if (null != oneCheckWalletId) {
             transactionInfo.put(InTxnDataConstants.ONE_CHECK_WALLET_ID, oneCheckWalletId);
          }
          transactionInfo.put(InTxnDataConstants.ORDER_ID, orderId);
          transactionInfo.put(InTxnDataConstants.TRANSACTION_SUCCESS_ON, new Date());
          try {
             long startTimeCampaignInTxnData = System.currentTimeMillis();
             campaignServiceClient.saveInTxnData1(userDetailsObj.getLookupId(),
                   FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, transactionInfo);
             long endTimeCampaignInTxnData = System.currentTimeMillis();
             metricsClient.recordLatency("Fulfillment.saveInTxnData1.", "Latency",
                   endTimeCampaignInTxnData - startTimeCampaignInTxnData);
          } catch (Exception e) {
             logger.error("Error saving intxndata in fulfillment: " + orderId, e);
          }
       }
    }

      public static String getBeanName() {
       return FCUtil.getLowerCamelCase(FulfillmentService.class.getSimpleName());
    }

    public int markTransactionFailure(final String orderId) {
       return txnFulfilmentDAO.markTransactionFailed(orderId);
    }

    public List<String> getTxnHomePageIdByEmail(final String email) {
       return txnFulfilmentReadDAO.getTxnHomePageIdByEmail(email);
    }

    public TransactionStatus getTransactionStatus(final UserDetails userDetailsObj, final String orderId) {
       TransactionStatus transactionStatus = null;
       TransactionStatus retryTransactionStatus = null;
       if (FCUtil.isRechargeProductType(userDetailsObj.getProductType())) {
          transactionStatus = inService.getRechargeStatus(orderId);
          if (transactionStatus != null) {
             if (inService.isFailure(transactionStatus.getTransactionStatus())) {
                retryTransactionStatus = billPaymentService.getBillPaymentStatus(userDetailsObj, orderId);
                if (retryTransactionStatus != null) {
                   return retryTransactionStatus;
                }
             }
          }
       }

       if (FCUtil.isBillPostpaidType(userDetailsObj.getProductType())) {
          transactionStatus = billPaymentService.getBillPaymentStatus(userDetailsObj, orderId);
          if (transactionStatus != null) {
             if (inService.isFailure(transactionStatus.getTransactionStatus())) {
                retryTransactionStatus = inService.getRechargeStatus(orderId);
                if (retryTransactionStatus != null) {
                   return retryTransactionStatus;
                }
             }
          }
       }
       return transactionStatus;
    }

    public TransactionStatus getTransactionStatusForBillPay(final String billId) {
       TransactionStatus transactionStatus = new TransactionStatus();
       BillTransaction billTransaction = billPayServiceRemote.getBillTransaction(billId);
       transactionStatus.setTransactionStatus(billTransaction.getTransactionStatus());
       transactionStatus.setServiceProvider(null);
       transactionStatus.setPlanType(null);
       return transactionStatus;
    }

    public List<String> reTriggerTxnAndCouponEmail(List<String> orderIdList) {

       List<String> reTriggeredOrderIds = new ArrayList<String>();
       logger.debug("ReTriggerTxnAndCouponEmail: OrderIds :" + orderIdList);

       for (String orderId : orderIdList) {
          boolean couponEmailStatus = emailService.retriggerECouponMailer(orderId);
          if (couponEmailStatus) {
             reTriggeredOrderIds.add(orderId);
          } else {
             logger.debug("ReTriggerTxnAndCouponEmail: Unable to trigger E-Coupon Mailer for: " + orderId);
          }

       }

       return reTriggeredOrderIds;
    }

    // TODO : Leverage the FreechargeEventpublisher to publish metrics to SQS.
    public void storeMetric(String orderId) {
       List<BlockedCoupon> blockedCouponList = couponServiceProxy.getBlockedCouponListForOrder(orderId);
       if (blockedCouponList == null)
          logger.error("blockedCouponList is null");
       else if (blockedCouponList.size() == 0)
          logger.error("blockedCouponList has size zero");
       Double ordersWithCoupon = (double) 0;
       Double couponCount = (double) 0;
       Map<Integer, Double> metricValues = new HashMap<Integer, Double>();
       if (blockedCouponList != null && blockedCouponList.size() > 0) {
          ordersWithCoupon++;
          for (BlockedCoupon blockedCoupon : blockedCouponList) {
             List<BlockedCouponCode> blockedCodes = blockedCoupon.getBlockedCodes();
             couponCount = couponCount + blockedCodes.size();
          }
          logger.info("CouponOrderCount for orderId: " + orderId + "is " + couponOrderCount);
          logger.info("totalCouponCount for orderId: " + orderId + "is " + totalCouponCount);
          metricValues.put(couponOrderCount, ordersWithCoupon);
          metricValues.put(totalCouponCount, couponCount);
       }
       metricValues.put(orderCount, (double) 1);
       String ipAddress = orderDataService.getIpFromOrderDataByOrderId(orderId);
       logger.info("RequestType: " + RequestType.VISIT + "experimentId : " + experimentId
             + "FCSessionUtil.getIpAddress(): " + ipAddress);
    }

    @SuppressWarnings("unused")
    private void setTemplateSuccessfulTans(final EmailBusinessDO rechargeReceiptBusinessDO,
          final UserDetails userDetailsObj) {
       if (FCUtil.isRechargeProductType(userDetailsObj.getProductType())) {
          rechargeReceiptBusinessDO.setTemplateName("templates/mail/successfulTans.vm");
       }
       if (FCUtil.isBillPostpaidType(userDetailsObj.getProductType())) {
          rechargeReceiptBusinessDO.setTemplateName("templates/mail/billPaymentSuccessfulTans.vm");
       }
       if (FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
          rechargeReceiptBusinessDO.setTemplateName("templates/mail/billPaymentSuccessfulTans.vm");
       }
    }

    public List<String> getTxnHomePageIdByEmailFromDate(final String email, final Timestamp fromDateTime,
          final Timestamp toDateTime) {
       return txnFulfilmentReadDAO.getTxnHomePageIdByEmailFromDate(email, fromDateTime, toDateTime);
    }

    public void publishCampaignCheckMessage(String lookupId) {
       String message = "{\"lookupId\": \"" + lookupId + "\", \"merchantId\": \""
             + FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID + "\", \"trigger\":\"RECHARGE_SUCCESS\"}";
       try {
          campaignSNSService.publish(message);
          logger.info("[CampaignService] message published to campaign sns: " + message);
       } catch (Exception e) {
          logger.error("[CampaignService] error publishing campaign service message", e);
       }

    }
    
    public void proccessSkippedRecordListForFF(String orderID) {
    	// we can multiple orderId for same id
    	 List<PostFFTaskModel>  postFFTaskModelList= postFFDao.findByOrderId(orderID);
		 if(org.apache.commons.collections.CollectionUtils.isEmpty(postFFTaskModelList)) {
			 logger.info("No record found for Skipped activity");
			 return;
			 
		 }
		 logger.info("Processing records for skippedRecords: size:"+ postFFTaskModelList.size());
		 for(PostFFTaskModel postFFTaskModel: postFFTaskModelList) {
		       proccessSkippedRecordForFF(postFFTaskModel);
		 }
    }


	public void proccessSkippedRecordForFF(final PostFFTaskModel postFFTaskModel) {	
		if (postFFTaskModel.getRetryCount() >= skippedFFRetryCount) {
			logger.info("RetryCount Exhaust for orderId: "+postFFTaskModel.getOrderId());
             // maintain prev state so that we can know the last try which was made after retry exhaust
			postFFDao.updateTaskState(postFFTaskModel.getState(),
					postFFTaskModel.getId(),  
					PostFulfillmentConstants.FAILED_DOER_RETRY_EXHAUST);
			return;
		}
		
		PostFulfillmentTaskState postFulfillmentTaskState = postFFTaskModel.getState();
		logger.info("Capturing in transaction data for orderId:" + postFFTaskModel.getOrderId() + " and id "+postFFTaskModel.getId());
	   
		switch(postFulfillmentTaskState) {
		case CAMPAIGN_NOTIFICATION_MISSED:
		case CAMPAIGN_NOTIFICATION_IN_PROGRESS: 
		    processSkippedCampaignRecords(postFFTaskModel);
			break;
		case COMM_NOTIFICATION_MISSED:
		case COMM_NOTIFICATION_IN_PROGRESS: 
			processedSkippedCommunications(postFFTaskModel);
			break;
		case TSM_NOTIFICATION_MISSED:
		case TSM_NOTIFICATION_IN_PROGRESS: 
			processedSkippedTSMRecord(postFFTaskModel);
			break;
		case TSM_NOTIFICATION_FAILED:
		case TSM_NOTIFICATION_PROCESSED: 
		case CAMPAIGN_NOTIFICATION_FAILED:
		case CAMPAIGN_NOTIFICATION_PROCESSED:
		case COMM_NOTIFICATION_FAILED:
		case COMM_NOTIFICATION_PROCESSED: {
			logger.warn("IllegalStateToRetry..Already Processed at terminated state:"+postFulfillmentTaskState+ 
					" orderId:"+ postFFTaskModel.getOrderId());
			return;
		}
		default: {
			logger.warn("invalid state: " + postFulfillmentTaskState + "for orderId:" + postFFTaskModel.getOrderId()+ 
					" and id: " + postFFTaskModel.getId());
		}
		}
		
	}


	protected void processSkippedCampaignRecords(final PostFFTaskModel postFFTaskModel) {
		MissedCampaignDTO missedCampaignDTO= ConvertorUtil.getObjFromJsonStr(postFFTaskModel.getRawRequest(), MissedCampaignDTO.class);
			if (missedCampaignDTO == null) {
			    	logger.info("Issue in conversion to missedCampaignDTO :" + missedCampaignDTO+ 
			    			" for an orderId" + postFFTaskModel.getOrderId());
			    	postFFDao.updateTaskState(PostFulfillmentTaskState.CAMPAIGN_NOTIFICATION_FAILED,
							postFFTaskModel.getId(),  
							PostFulfillmentConstants.FAILED_DOER_INVALID_ENTRY);
			    	return;
			}
		boolean response = publishToCampaign(missedCampaignDTO.getOrderId(),
				   missedCampaignDTO.getUserDetailsObj(),missedCampaignDTO.getTransactionStatus());
		
		if(response)
			postFFDao.updateTaskState(PostFulfillmentTaskState.CAMPAIGN_NOTIFICATION_PROCESSED,
					postFFTaskModel.getId(),  
					String.format(PostFulfillmentTaskState.CAMPAIGN_NOTIFICATION_PROCESSED.getMsg(),
							PostFulfillmentConstants.SUCCESS_DOER));
		logger.debug("Successfully done call with Skipped Campaign");
	}


	protected void processedSkippedTSMRecord(final PostFFTaskModel postFFTaskModel) {
		 MissedTSMDTO missedTSMDTO= ConvertorUtil.getObjFromJsonStr(postFFTaskModel.getRawRequest(), MissedTSMDTO.class);
		if (missedTSMDTO == null) {
		    	logger.info("Issue in conversion to missedTSMDTO:" + missedTSMDTO+ 
		    			" for an orderId" + postFFTaskModel.getOrderId());
		    	postFFDao.updateTaskState(PostFulfillmentTaskState.TSM_NOTIFICATION_FAILED,
						postFFTaskModel.getId(),  
						PostFulfillmentConstants.FAILED_DOER_INVALID_ENTRY);
		    	return;
		}
	    boolean response = notifyToTranslator(missedTSMDTO.getOrderId(), missedTSMDTO.getShouldSmsSend(), missedTSMDTO.getIsRetrigger(), missedTSMDTO.getUserDetailsObj(), missedTSMDTO.getHomePage(), missedTSMDTO.getTransactionStatus(), missedTSMDTO.getTxnFulfilment(),missedTSMDTO.isAdminReversal());
		if(response)
			postFFDao.updateTaskState(PostFulfillmentTaskState.TSM_NOTIFICATION_PROCESSED,
					postFFTaskModel.getId(),
					String.format(PostFulfillmentTaskState.TSM_NOTIFICATION_PROCESSED.getMsg(),
							PostFulfillmentConstants.SUCCESS_DOER));
		logger.debug("Successfully done call with TSM");
	}
	
	protected void processedSkippedCommunications(final PostFFTaskModel postFFTaskModel) {
		 MissedTSMDTO missedTSMDTO= ConvertorUtil.getObjFromJsonStr(postFFTaskModel.getRawRequest(), MissedTSMDTO.class);
		if (missedTSMDTO == null) {
		    	logger.info("Issue in conversion to missedTSMDTO:" + missedTSMDTO+ 
		    			" for an orderId" + postFFTaskModel.getOrderId());
		    	postFFDao.updateTaskState(PostFulfillmentTaskState.COMM_NOTIFICATION_FAILED,
						postFFTaskModel.getId(),  
						PostFulfillmentConstants.FAILED_DOER_INVALID_ENTRY);
		    	return;
		}
	    boolean response = initiateCommunication(missedTSMDTO.getOrderId(), missedTSMDTO.getShouldSmsSend(), missedTSMDTO.getIsRetrigger(), missedTSMDTO.getUserDetailsObj(), missedTSMDTO.getHomePage(), missedTSMDTO.getTransactionStatus(), missedTSMDTO.getTxnFulfilment());
		if(response)
			postFFDao.updateTaskState(PostFulfillmentTaskState.COMM_NOTIFICATION_PROCESSED,
					postFFTaskModel.getId(),
					String.format(PostFulfillmentTaskState.COMM_NOTIFICATION_PROCESSED.getMsg(),
							PostFulfillmentConstants.SUCCESS_DOER));
		logger.debug("Successfully done call with Communicator");
	}
 }
   
 