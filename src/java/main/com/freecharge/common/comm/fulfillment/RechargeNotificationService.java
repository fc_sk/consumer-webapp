package com.freecharge.common.comm.fulfillment;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.freecharge.app.domain.dao.jdbc.OrderIdSlaveDAO;
import com.freecharge.common.comm.sms.SMSService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.ExternalClientCallService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.recharge.exception.RechargeEnqueFailedException;
import com.freecharge.wallet.OnecheckCommandBuilder;
import com.google.gson.Gson;

import flexjson.JSONSerializer;

@Service
public class RechargeNotificationService {

	@Autowired
	private SMSService smsService;

	@Autowired
	private FCProperties fcProperties;

	@Autowired
	private OrderIdSlaveDAO orderIdSlaveDAO;
	
	@Autowired
	private UserServiceProxy userServiceProxy;

	@Autowired
	@Qualifier("notificationConnectionManager")
	private MultiThreadedHttpConnectionManager notificationConnectionManager;
	
	@Autowired
    private ExternalClientCallService externalClientCallService;
	
	@Autowired
    @Qualifier("notificationSQSClient")
    AmazonSQSAsync sqsClient;

	private static final String ON_SUCCESS_DEEPLINK_ANDROID = "recharge:action=view&page=home";
	private static final String ON_SUCCESS_DEEPLINK_IOS = "freechargeapp://freecharge?action=view&page=home";

	private static final String TRANSACTION_ID_TEXT = "Operator Transaction Id:";

	private Logger logger = LoggingFactory.getLogger(getClass());
	
	private JSONSerializer jsonSerializer = new JSONSerializer();

	public void notifyOnSuccess(String orderId, UserDetails userDetailsObj, Map<String, String> variablesMap) {
		try {
			char channel = orderId.charAt(3);
			String emailId= userServiceProxy.getEmailIdFromOrderId(orderId);
			switch (channel) {
			case 'A':
				sendAndroidNotification(orderId, userDetailsObj, variablesMap, emailId);
				break;

			case 'I':
				sendIOSNotification(orderId, userDetailsObj, variablesMap, emailId);
				break;

			default:
				logger.debug("Notification not supported for channel: " + channel);
			}
		} catch (Exception e) {
			logger.error("Exception caught while sending sound of success ",e);
		}

	}

	private void sendIOSNotification(String orderId, UserDetails userDetailsObj, Map<String, String> variablesMap,
			String emailId) {
		String messageUrl = ON_SUCCESS_DEEPLINK_IOS;

		String pushUrl = fcProperties.getProperty(FCProperties.IOS_RETRY_PUSH_ENDPOINT);
		String ussd = smsService.getOperatorUssdCode(userDetailsObj.getOperatorName());
		String transactionId = variablesMap.get("operatorTxnId");
		Map<String, Object> payLoadMap = new HashMap<String, Object>();
		Map<String, String> titleMap = new HashMap<String, String>();
		titleMap.put("operator", userDetailsObj.getOperatorName());
		titleMap.put("amount", userDetailsObj.getAmount());
		Map<String, String> msgMap = new HashMap<String, String>();
		msgMap.put("transactionId", transactionId);
		msgMap.put("ussd", ussd);
		payLoadMap.put("Title", titleMap);
		payLoadMap.put("message", msgMap);
		payLoadMap.put("deeplink", messageUrl);

		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("templateId", fcProperties.getProperty(FCProperties.SOUND_OF_SUCCESS_IOSTEMPLATEID));
		reqMap.put("userEmail", emailId);
		reqMap.put("payLoadMap", payLoadMap);

		postHttpMessage(pushUrl, reqMap, orderId, emailId);

	}

	private void sendAndroidNotification(String orderId, UserDetails userDetailsObj, Map<String, String> variablesMap,
			String emailId) {
		String ussd = smsService.getOperatorUssdCode(userDetailsObj.getOperatorName());
		String transactionId = variablesMap.get("operatorTxnId");
		Integer templateId = getTemplateIdForAndroid(userDetailsObj.getProductType());
		if(templateId==null){
			logger.info("No Mathching template found");
			return;
		}
		Map<String, String> titleMap = new HashMap<String, String>();
		titleMap.put("operator", userDetailsObj.getOperatorName());
		titleMap.put("amount", userDetailsObj.getAmount());
		Map<String, String> descriptionMap = new HashMap<String, String>();
		descriptionMap.put("transactionId", transactionId);
		descriptionMap.put("operatorrefstring", getFormattedText(transactionId));
		descriptionMap.put("ussd", ussd);
		//descriptionMap.put("messageUrl", messageUrl);

		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("templateId",templateId);
		reqMap.put("titleMap", titleMap);
		reqMap.put("descriptionMap", descriptionMap);
		reqMap.put("emailId", emailId);
		
		postToSQS(reqMap);

	/*	String pushUrl = fcProperties.getProperty(FCProperties.ANDROID_RETRY_PUSH_ENDPOINT);
		postHttpMessage(pushUrl, reqMap, orderId, emailId);*/

	}

	private String getFormattedText(String transactionId) {
		if(FCUtil.isEmpty(transactionId)){
			return "";
		}
		return TRANSACTION_ID_TEXT + transactionId;
	}

	private Integer getTemplateIdForAndroid(String productType) {
		if(FCUtil.isPrePaidRecharge(productType)){
			return Integer.valueOf(fcProperties.getProperty(FCProperties.SOUND_OF_SUCCESS_ANDROIDTEMPLATEID));
		}else if(FCUtil.isPrePaidDatacard(productType)||FCUtil.isDTH(productType)){
			return Integer.valueOf(fcProperties.getProperty(FCProperties.SOUND_OF_SUCCESS_ANDROIDTEMPLATEID_TWO));
		}else if(FCUtil.isUtilityPaymentType(productType)||FCUtil.isBillPostpaidType(productType)){
			return Integer.valueOf(fcProperties.getProperty(FCProperties.SOUND_OF_SUCCESS_ANDROIDTEMPLATEID_THREE));
		}
		return null;
	}

	private void postToSQS(Map<String, Object> reqMap) {
		String payLoad = jsonSerializer.serialize(reqMap);

		String queueName = fcProperties.getAndroidNotificationQueue();
		this.logger.debug("About to enqueue payLoad: [" + payLoad + "] into SQS queue: [" + queueName + "]");

		SendMessageRequest sendRequest = new SendMessageRequest();
		sendRequest.setQueueUrl(getSQSUrl(fcProperties.getAndroidNotificationQueue()));
		sendRequest.setMessageBody(payLoad);

		try {
			this.sqsClient.sendMessage(sendRequest);
			this.logger.info("Successfully enqueued payLoad: [" + payLoad + "] into SQS queue: [" + queueName + "]. ");
		} catch (Exception e) {

			this.logger.error(String.format("SQS Queue Error:Error while queuing %s in %s", payLoad, queueName), e);
		}

	}

	private String getSQSUrl(String queueName) {
		String sqsEndpoint = fcProperties.getNotificationEndPoint()+"/"+ queueName;
		return sqsEndpoint;
	}

	@SuppressWarnings("deprecation")
	private void postHttpMessage(String postUrl, Map<String, Object> reqMap, String orderId, String emailId) {
		HttpClient client = new HttpClient();
		client.setHttpConnectionManager(notificationConnectionManager);
		PostMethod postMethod = new PostMethod(postUrl);
		postMethod.setRequestHeader("Content-Type", "application/json");
		Gson gson = new Gson();
		String postData = gson.toJson(reqMap);
		logger.info("postData for SOS is " + postData);
		postMethod.setRequestEntity(new StringRequestEntity(postData));
		Integer httpStatusCode = null;
		try {
			httpStatusCode = externalClientCallService.executeWithTimeOut(RechargeNotificationCommandBuilder
					.getNotificationServiceResponseCommand(client, postMethod, "Fulfillment", "SuccessNotification"));
			postMethod.getResponseBodyAsStream();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		if (httpStatusCode != null && HttpStatus.SC_OK == httpStatusCode) {
			logger.info("Sound Of Success sent for orderId:" + orderId);
		} else {
			logger.error("Sound Of Success failed to send push notification. Status : " + httpStatusCode);
		}
	}

	public boolean isEligibleForNotification(String productType) {
		String basePath = fcProperties.getCCSBasePathFulfillment();
		String productsKey = fcProperties.getFulfillmentSucceesNotificationProductKey();

		List<String> eligibleProducts;
		try {
			String[] products = FCUtil.getCCSValues(productsKey, basePath, fcProperties.getCCSHost());
			eligibleProducts = Arrays.asList(products);
		} catch (Exception e) {
			logger.error("Exception caught while getting value from CCS for productType : " + productType, e);
			return false;
		}
		if (eligibleProducts != null && eligibleProducts.contains(productType)) {
			logger.info("Product Type "+productType +" is not eligible for SOS");
			return true;
		}
		return false;
	}

	
}
