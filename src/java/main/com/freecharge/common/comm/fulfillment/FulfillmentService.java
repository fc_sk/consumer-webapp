package com.freecharge.common.comm.fulfillment;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.web.model.BlockedCoupon;
import com.freecharge.api.coupon.service.web.model.BlockedCouponCode;
import com.freecharge.app.domain.dao.TxnFulfilmentDAO;
import com.freecharge.app.domain.dao.TxnFulfilmentReadDAO;
import com.freecharge.app.domain.dao.jdbc.OrderIdReadDAO;
import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.app.domain.entity.UserTransactionHistory;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.HomeService;
import com.freecharge.app.service.InService;
import com.freecharge.app.service.OperatorCircleService;
import com.freecharge.app.service.OrderService;
import com.freecharge.app.service.UserService;
import com.freecharge.campaign.client.CampaignServiceClient;
import com.freecharge.centaur.service.common.RequestType;
import com.freecharge.common.businessdo.TxnHomePageBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freebill.service.BillPaymentService;
import com.freecharge.infrastructure.billpay.api.IBillPayService;
import com.freecharge.infrastructure.billpay.app.jdbc.BillPayOperatorMaster;
import com.freecharge.infrastructure.billpay.beans.BBPSBillTransactionDetails;
import com.freecharge.infrastructure.billpay.beans.BillTransaction;
import com.freecharge.intxndata.common.InTxnDataConstants;
import com.freecharge.notification.service.common.INotificationToolApiService;
import com.freecharge.order.service.OrderDataService;
import com.freecharge.payment.dao.BBPSDetailsDao;
import com.freecharge.payment.services.PaymentTransactionService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.services.IRechargeRetryService;
import com.freecharge.recharge.services.UserTransactionHistoryService;
import com.freecharge.rest.recharge.LogRequestService;
import com.freecharge.sns.AmazonSNSService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletWrapper;
import com.freecharge.web.webdo.BBPSTransactionDetails;

/**
 * Service class for Order fulfilment functionality
 */
@Service
public class FulfillmentService {

   private Logger logger = LoggingFactory.getLogger(getClass());

   @Autowired
   private TxnFulfilmentDAO txnFulfilmentDAO;

   @Autowired
   private TxnFulfilmentReadDAO txnFulfilmentReadDAO;

   @Autowired
   private EmailService emailService;

   @Autowired
   private HomeService homeService;

   @Autowired
   private UserService userService;

   @Autowired
   private InService inService;

   @Autowired
   private OrderDataService orderDataService;

   @Autowired
   private UserTransactionHistoryService transactionHistoryService;

   @Autowired
   private BillPaymentService billPaymentService;

   @Autowired
   private OrderService orderService;

   @Autowired
   private OperatorCircleService operatorCircleService;

   @Autowired
   private IRechargeRetryService rechargeRetryService;

   @Autowired
   @Qualifier("billPayServiceProxy")
   private IBillPayService billPayServiceRemote;

   @Autowired
   PaymentTransactionService paymentTransactionService;

   @Autowired
   private AmazonSNSService campaignSNSService;

   @Autowired
   WalletWrapper walletWrapper;

   @Autowired
   private CampaignServiceClient campaignServiceClient;

   @Autowired
   private OneCheckWalletService oneCheckWalletService;

   @Autowired
   private BBPSDetailsDao bbpsDetailsDao;

   @Autowired
   @Qualifier("couponServiceProxy")
   private ICouponService couponServiceProxy;

   @Value("${centaur.random.coupon.experiment.id}")
   private int experimentId;
   @Value("${centaur.random.coupon.experiment.order.count}")
   private int orderCount;
   @Value("${centaur.random.coupon.experiment.couponed.order.count}")
   private int couponOrderCount;
   @Value("${centaur.random.coupon.experiment.coupon.count}")
   private int totalCouponCount;

   @Autowired
   @Qualifier("notificationApiServiceClient")
   private INotificationToolApiService notificationService;

   @Autowired
   protected MetricsClient metricsClient;

   @Autowired
   private LogRequestService logRequestService;
   
   @Autowired
   private POSTFulfillmentService postFulfillmentService;

   @Autowired
   private FulfilmentNotificationService fulfilmentNotificationService;
   
   @Autowired
	private OrderIdReadDAO orderIdDao;
   

   
   /**
    * Used to start the fulfilment task for a given orderId
    * 
    * @param orderId
    * @param shouldSmsSend
    * @param isRetrigger
    */
   public void doFulfillment(final String orderId, final Boolean shouldSmsSend, final Boolean isRetrigger, final Boolean isTrigerredFromAdminPanel) {
      logger.info("fulfillment process start for order Id" + orderId);

      /*
       * Handling of retry piece here.
       */

      try {
         Map<String, Object> userDetail = homeService.getUserRechargeDetailsFromOrderId(orderId);

         boolean scheduledForRetry = rechargeRetryService.checkAndScheduleForRetry(orderId, userDetail);

         if (scheduledForRetry) {
            logger.info("Skipping fulfillment due to retry scheduling : [" + orderId + "]");
            logRequestService.cacheScheduleInfo(orderId);
            return;
         }

         // plan level retry check
         boolean isPlanLevelRetry = rechargeRetryService.checkForPlanLevelRetry(orderId, userDetail);
         if (isPlanLevelRetry) {
            logger.info("Skipping fulfilment due to plan level retry for order id : [" + orderId + "]");
            return;
         }

         boolean mgageRechargeOperatorRetry = rechargeRetryService.checkForHLRServiceRetry(orderId, userDetail);
         boolean rechargeOperatorRetry = false;
         if (!mgageRechargeOperatorRetry) {
            rechargeOperatorRetry = rechargeRetryService.checkForOperatorRetry(orderId, userDetail);
         }

         if (mgageRechargeOperatorRetry || rechargeOperatorRetry) {
            logger.info("Skipping fulfillment due to operator retry : [" + orderId + "]");
            return;
         }

         /*
          * Get the latest successful operator/circle from userTransactionHistory in case
          * if it is successful retry
          */

         long startTimeUserTxnHistory = System.currentTimeMillis();
         UserTransactionHistory userTransactionHistory = transactionHistoryService
               .findUserTransactionHistoryByOrderId(orderId);
         long endTimeUserTxnHistory = System.currentTimeMillis();
         metricsClient.recordLatency("Fulfillment.UserTxnHistory.", "Latency",
               endTimeUserTxnHistory - startTimeUserTxnHistory);
         if (userTransactionHistory != null && userTransactionHistory.getServiceProvider() != null) {
            userDetail.put("operatorName", userTransactionHistory.getServiceProvider());
            userDetail.put("circleName", userTransactionHistory.getServiceRegion());
            userDetail.put("productType", userTransactionHistory.getProductType());
            try {
               Integer operatorMasterId = null;
               Integer circleMasterId = -1;
               String operatorUIName = null;
               String circleUIName = null;

               if (FCUtil.isUtilityPaymentType(userTransactionHistory.getProductType())) {
                  long startTimeBillPayOperatorMaster = System.currentTimeMillis();
                  BillPayOperatorMaster operator = billPayServiceRemote
                        .getBillPayOperatorFromName(userTransactionHistory.getServiceProvider());
                  long endTimeBillPayOperatorMaster = System.currentTimeMillis();
                  metricsClient.recordLatency("Fulfillment.BillPayOperator.", "Latency",
                        endTimeBillPayOperatorMaster - startTimeBillPayOperatorMaster);
                  operatorMasterId = operator.getOperatorMasterId();
                  operatorUIName = operator.getName();
               } else {
                  OperatorMaster operator = operatorCircleService
                        .getOperator(userTransactionHistory.getServiceProvider());
                  CircleMaster circle = operatorCircleService
                        .getCircle(userTransactionHistory.getServiceRegion());
                  operatorMasterId = operator.getOperatorMasterId();
                  operatorUIName = operator.getName();
                  if (circle != null) {
                     circleMasterId = circle.getCircleMasterId();
                     circleUIName = circle.getName();
                  }
               }

               userDetail.put("operatorMasterId", operatorMasterId);
               userDetail.put("circleMasterId", circleMasterId);
               userDetail.put("operatorUIName", operatorUIName);
               userDetail.put("circleUIName", circleUIName);
               logger.info("userDetails set operator:" + operatorMasterId + ", circle: " + circleMasterId);
            } catch (Exception e) {
               logger.error(" Error while converting the operator/circle id  " + orderId, e);
            }
         }
         UserDetails userDetailsObj = UserDetails.getUserDetails(userDetail, orderId);
         TxnHomePageBusinessDO homePage = orderService.getHomePageForOrder(orderId);

         TransactionStatus transactionStatus = getTransactionStatus(userDetailsObj, orderId);
         logger.info("transactionStatus : " + transactionStatus);
         if (transactionStatus == null && FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {
            BBPSBillTransactionDetails result = billPayServiceRemote
                  .getBillTransactionDetails(userDetailsObj.getBillId());
            logger.info("BBPSBillTransactionDetails : " + result);
            if (result.getIsBbpsEnabled()) {
               logger.info("orderId: " + orderId + " is bbps transaction");
               BBPSTransactionDetails bbpsTxnDetails = bbpsDetailsDao
                     .getBBPSDetailsForLookupId(homePage.getLookupId());
               if (bbpsTxnDetails.getBbpsReferenceNumber() != null
                     && bbpsTxnDetails.getBbpsReferenceNumber() != "") {
                  // Do nothing as details have been updated by recharge consumer from the sync
                  // response from billpay of payment API

               } else {
                  logger.info("inserting bbps reference number for orderId: " + orderId);
                  bbpsTxnDetails.setBbpsReferenceNumber(result.getBbpsReferenceNumber());
                  logger.info("Ag txn ID: " + result);
                  bbpsTxnDetails.setAggregatorTxnId(result.getAggregatorTxnId());
                  bbpsTxnDetails.setIsBbpsActive(true);
                  bbpsDetailsDao.updateBBPSDetails(bbpsTxnDetails);
               }
            }

            transactionStatus = new TransactionStatus();
            transactionStatus.setTransactionStatus(result.getTransactionStatus());
            transactionStatus.setServiceProvider(null);
            transactionStatus.setPlanType(null);
            transactionStatus.setSubscriberIdentificationNumber(userDetailsObj.getUserMobileNo());
            transactionStatus.setOperator(userDetailsObj.getOperatorName());
            transactionStatus.setServiceRegion(userDetailsObj.getCircleName());
            transactionStatus.setProductType(userDetailsObj.getProductType());
         }

         logger.debug("obtained transaction details from order id. OrderID :  " + orderId);

         transactionHistoryService.createOrUpdateRechargeHistory(orderId, userDetail,
               transactionStatus.getTransactionStatus());

         logger.info("Populated transaction history for the OrderID :  " + orderId);
         postFulfillmentService.doPostFulfillmentTasks(orderId, shouldSmsSend, isRetrigger, userDetailsObj, homePage, transactionStatus, userTransactionHistory,isTrigerredFromAdminPanel);
         
      } catch (Exception e) {
         logger.error("Caught exception in fulfilling orderId " + orderId + " ", e);

      }
   }
   
 
   public void storeInTxnData(String orderId, UserDetails userDetailsObj) {
      HashMap<String, Object> transactionInfo = new HashMap<String, Object>();
      Users user = null;
      try {
         long startTimeUserService = System.currentTimeMillis();
         user = userService.getUser(userDetailsObj.getUserEmail());
         long endTimeUserService = System.currentTimeMillis();
         metricsClient.recordLatency("Fulfillment.userService.getUser.", "Latency",
               endTimeUserService - startTimeUserService);
      } catch (Exception e) {
         logger.error("Exception in getting user details for emailId " + userDetailsObj.getUserEmail()
               + " so skipping save intxndata");
      }
      if (null != user) {
         Integer userId = user.getUserId();
         // new User condition
         List<String> orders = transactionHistoryService.getRechargeSuccessDistinctOrderIdsByUserId(userId, 2);

         if (null == orders || orders.size() < 2) { // because current transaction will always be there since this is
                                          // after recharge success
            transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, true);
         } else {
            transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, false);
         }
         transactionInfo.put(InTxnDataConstants.USER_ID, userId);
         transactionInfo.put(InTxnDataConstants.USER_EMAIL, userDetailsObj.getUserEmail());
         transactionInfo.put(InTxnDataConstants.PROFILE_NUMER, user.getMobileNo());
         transactionInfo.put(InTxnDataConstants.OPERATOR_NAME, userDetailsObj.getOperatorUIName());
         transactionInfo.put(InTxnDataConstants.CIRCLE_NAME, userDetailsObj.getCircleUIName());
         transactionInfo.put(InTxnDataConstants.OPERATOR_MASTER_ID, userDetailsObj.getOperatorMasterId());
         transactionInfo.put(InTxnDataConstants.CIRCLE_MASTER_ID, userDetailsObj.getCircleMasterId());
         MigrationStatus migrationStatus = oneCheckWalletService
               .getFcWalletMigrationStatus(userDetailsObj.getUserEmail(), null);
         String oneCheckWalletId = null;
         try {
            logger.info("Fetching oneCheckWalletId  for user " + userId);
            oneCheckWalletId = oneCheckWalletService
                  .getOneCheckIdIfOneCheckWalletEnabled(userDetailsObj.getUserEmail());
         } catch (Exception e) {
            logger.error("error fetching oneCheckWalletId for email " + userDetailsObj.getUserEmail(), e);
         }
         logger.info("For emailId " + userDetailsObj.getUserEmail() + ". OneCheckWalletId is " + oneCheckWalletId
               + " and migrationStatus is " + migrationStatus);
         if (null != migrationStatus) {
            transactionInfo.put(InTxnDataConstants.MIGRATION_STATUS, migrationStatus.getMigrationStatus());
         }
         if (null != oneCheckWalletId) {
            transactionInfo.put(InTxnDataConstants.ONE_CHECK_WALLET_ID, oneCheckWalletId);
         }
         transactionInfo.put(InTxnDataConstants.ORDER_ID, orderId);
         transactionInfo.put(InTxnDataConstants.TRANSACTION_SUCCESS_ON, new Date());
         try {
            long startTimeCampaignInTxnData = System.currentTimeMillis();
            campaignServiceClient.saveInTxnData1(userDetailsObj.getLookupId(),
                  FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, transactionInfo);
            long endTimeCampaignInTxnData = System.currentTimeMillis();
            metricsClient.recordLatency("Fulfillment.saveInTxnData1.", "Latency",
                  endTimeCampaignInTxnData - startTimeCampaignInTxnData);
         } catch (Exception e) {
            logger.error("Error saving intxndata in fulfillment: " + orderId, e);
         }
      }
   }

   public void storeInTxnData(String orderId, UserDetails userDetailsObj, Map<String, Object> inTxnPayload) {
      HashMap<String, Object> transactionInfo = new HashMap<String, Object>();
      Users user = null;
      try {
         user = userService.getUser(userDetailsObj.getUserEmail());
         // metricsClient.recordLatency("Fulfillment.userService.getUser.", "Latency",
         // endTimeUserService - startTimeUserService);
      } catch (Exception e) {
         logger.error("Exception in getting user details for emailId " + userDetailsObj.getUserEmail()
               + " so skipping save intxndata");
      }
      if (null != user) {
         Integer userId = user.getUserId();
         // new User condition
         List<String> orders = transactionHistoryService.getRechargeSuccessDistinctOrderIdsByUserId(userId, 2);

         if (null == orders || orders.size() < 2) { // because current transaction will always be there since this is
                                          // after recharge success
            transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, true);
         } else {
            transactionInfo.put(InTxnDataConstants.IS_FIRST_TXN, false);
         }
         transactionInfo.put(InTxnDataConstants.USER_ID, userId);
         transactionInfo.put(InTxnDataConstants.USER_EMAIL, userDetailsObj.getUserEmail());
         transactionInfo.put(InTxnDataConstants.PROFILE_NUMER, user.getMobileNo());
         transactionInfo.put(InTxnDataConstants.OPERATOR_NAME, userDetailsObj.getOperatorUIName());
         transactionInfo.put(InTxnDataConstants.CIRCLE_NAME, userDetailsObj.getCircleUIName());
         transactionInfo.put(InTxnDataConstants.OPERATOR_MASTER_ID, userDetailsObj.getOperatorMasterId());
         transactionInfo.put(InTxnDataConstants.CIRCLE_MASTER_ID, userDetailsObj.getCircleMasterId());
     //    MigrationStatus migrationStatus = null;
         String oneCheckWalletId = null;
         try {
            // logger.info("Fetching migration and oneCheckWalletId status for user " +
            // userId);
            oneCheckWalletId = oneCheckWalletService
                  .getOneCheckIdIfOneCheckWalletEnabled(userDetailsObj.getUserEmail());
         } catch (Exception e) {
            // logger.error("error fetching wallet migration status for email " +
            // userDetailsObj.getUserEmail(), e);
         }
         /*
          * logger.info("For emailId " + userDetailsObj.getUserEmail() +
          * ". OneCheckWalletId is " + oneCheckWalletId + " and migrationStatus is " +
          * migrationStatus);
          */
        /* if (null != migrationStatus) {
            transactionInfo.put(InTxnDataConstants.MIGRATION_STATUS, migrationStatus.getMigrationStatus());
         }*/
         if (null != oneCheckWalletId) {
            transactionInfo.put(InTxnDataConstants.ONE_CHECK_WALLET_ID, oneCheckWalletId);
         }
         transactionInfo.put(InTxnDataConstants.ORDER_ID, orderId);
         transactionInfo.put(InTxnDataConstants.TRANSACTION_SUCCESS_ON, new Date());
         mergeMapsFrom(inTxnPayload, transactionInfo);
         try {
            campaignServiceClient.saveInTxnData1(userDetailsObj.getLookupId(),
                  FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID, transactionInfo);
            // metricsClient.recordLatency("Fulfillment.saveInTxnData1.", "Latency",
            // endTimeCampaignInTxnData - startTimeCampaignInTxnData);
         } catch (Exception e) {
            // logger.error("Error saving intxndata in fulfillment: " + orderId, e);
         }
      }
   }

   private void mergeMapsFrom(Map<String, Object> source, Map<String, Object> destination) {
      if (source != null) {
         Set<Map.Entry<String, Object>> entrySet = source.entrySet();
         for (Map.Entry<String, Object> entry : entrySet) {
            String key = entry.getKey();
            Object value = entry.getValue();
            destination.put(key, value);
         }
         /*
          * try { logger.info("Merged map : " + new
          * ObjectMapper().writeValueAsString(destination)); } catch
          * (JsonProcessingException jpe) {
          * logger.error("Unable to log data because of exception : ", jpe); }
          */
      }
   }

   public static String getBeanName() {
      return FCUtil.getLowerCamelCase(FulfillmentService.class.getSimpleName());
   }

   public int markTransactionFailure(final String orderId) {
      return txnFulfilmentDAO.markTransactionFailed(orderId);
   }

   public List<String> getTxnHomePageIdByEmail(final String email) {
      return txnFulfilmentReadDAO.getTxnHomePageIdByEmail(email);
   }

   public TransactionStatus getTransactionStatus(final UserDetails userDetailsObj, final String orderId) {
      TransactionStatus transactionStatus = null;
      TransactionStatus retryTransactionStatus = null;
      if (FCUtil.isRechargeProductType(userDetailsObj.getProductType())) {
         transactionStatus = inService.getRechargeStatus(orderId);
         if (transactionStatus != null) {
            if (inService.isFailure(transactionStatus.getTransactionStatus())) {
               retryTransactionStatus = billPaymentService.getBillPaymentStatus(userDetailsObj, orderId);
               if (retryTransactionStatus != null) {
                  return retryTransactionStatus;
               }
            }
         }
      }

      if (FCUtil.isBillPostpaidType(userDetailsObj.getProductType())) {
         transactionStatus = billPaymentService.getBillPaymentStatus(userDetailsObj, orderId);
         if (transactionStatus != null) {
            if (inService.isFailure(transactionStatus.getTransactionStatus())) {
               retryTransactionStatus = inService.getRechargeStatus(orderId);
               if (retryTransactionStatus != null) {
                  return retryTransactionStatus;
               }
            }
         }
      }
      return transactionStatus;
   }

   public TransactionStatus getTransactionStatusForBillPay(final String billId) {
      TransactionStatus transactionStatus = new TransactionStatus();
      BillTransaction billTransaction = billPayServiceRemote.getBillTransaction(billId);
      transactionStatus.setTransactionStatus(billTransaction.getTransactionStatus());
      transactionStatus.setServiceProvider(null);
      transactionStatus.setPlanType(null);
      return transactionStatus;
   }

   public List<String> reTriggerTxnAndCouponEmail(List<String> orderIdList) {

      List<String> reTriggeredOrderIds = new ArrayList<String>();
      logger.debug("ReTriggerTxnAndCouponEmail: OrderIds :" + orderIdList);

      for (String orderId : orderIdList) {
         boolean couponEmailStatus = emailService.retriggerECouponMailer(orderId);
         if (couponEmailStatus) {
            reTriggeredOrderIds.add(orderId);
         } else {
            logger.debug("ReTriggerTxnAndCouponEmail: Unable to trigger E-Coupon Mailer for: " + orderId);
         }

      }

      return reTriggeredOrderIds;
   }

   // TODO : Leverage the FreechargeEventpublisher to publish metrics to SQS.
   public void storeMetric(String orderId) {
      List<BlockedCoupon> blockedCouponList = couponServiceProxy.getBlockedCouponListForOrder(orderId);
      if (blockedCouponList == null)
         logger.error("blockedCouponList is null");
      else if (blockedCouponList.size() == 0)
         logger.error("blockedCouponList has size zero");
      Double ordersWithCoupon = (double) 0;
      Double couponCount = (double) 0;
      Map<Integer, Double> metricValues = new HashMap<Integer, Double>();
      if (blockedCouponList != null && blockedCouponList.size() > 0) {
         ordersWithCoupon++;
         for (BlockedCoupon blockedCoupon : blockedCouponList) {
            List<BlockedCouponCode> blockedCodes = blockedCoupon.getBlockedCodes();
            couponCount = couponCount + blockedCodes.size();
         }
         logger.info("CouponOrderCount for orderId: " + orderId + "is " + couponOrderCount);
         logger.info("totalCouponCount for orderId: " + orderId + "is " + totalCouponCount);
         metricValues.put(couponOrderCount, ordersWithCoupon);
         metricValues.put(totalCouponCount, couponCount);
      }
      metricValues.put(orderCount, (double) 1);
      String ipAddress = orderDataService.getIpFromOrderDataByOrderId(orderId);
      logger.info("RequestType: " + RequestType.VISIT + "experimentId : " + experimentId
            + "FCSessionUtil.getIpAddress(): " + ipAddress);
   }

 
   public List<String> getTxnHomePageIdByEmailFromDate(final String email, final Timestamp fromDateTime,
         final Timestamp toDateTime) {
      return txnFulfilmentReadDAO.getTxnHomePageIdByEmailFromDate(email, fromDateTime, toDateTime);
   }

   public void publishCampaignCheckMessage(String lookupId) {
      String message = "{\"lookupId\": \"" + lookupId + "\", \"merchantId\": \""
            + FCConstants.FREECHARGE_CAMPAIGN_MERCHANT_ID + "\", \"trigger\":\"RECHARGE_SUCCESS\"}";
      try {
         campaignSNSService.publish(message);
         logger.info("[CampaignService] message published to campaign sns: " + message);
      } catch (Exception e) {
         logger.error("[CampaignService] error publishing campaign service message", e);
      }

   }

	@Transactional
	public void UpdateReconMetaForUtilities(String lookupId, String reconId, boolean forceUpdate) throws Exception {
		BBPSTransactionDetails details = bbpsDetailsDao.getBBPSDetailsForLookupIdFailFast(lookupId);
		logger.info(" #####    BBPSTransactionDetails    ##### : "+details);
		if (details == null) {
			throw new RuntimeException("Not Eligible to update for : " + lookupId);

		}
		if(!forceUpdate) {
			if (reconId.equalsIgnoreCase(details.getBbpsReferenceNumber())) {
				throw new RuntimeException("reconId already exists for : " + lookupId);
			}	
		}
		List<String> orderIds = orderIdDao.getOrderIdListForLookUpId(lookupId);
		if (CollectionUtils.isEmpty(orderIds) || orderIds.size() > 1) {
			throw new RuntimeException("Invalid OrderID Detected for : " + lookupId);
		}
		String orderId = orderIds.get(0);
		bbpsDetailsDao.updateBBPSReferenceNoByLookupId(lookupId, reconId);
		fulfilmentNotificationService.updateReconMetaData(orderId);

	}
}