package com.freecharge.common.comm.fulfillment.test;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.comm.fulfillment.GiftSmsService;
import com.freecharge.eventprocessing.event.RechargeSuccessEvent;

public class GiftSmsServiceUTest {

    ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
    GiftSmsService gss = (GiftSmsService) ctx.getBean("giftSmsService");
    
    /**
     * Test what happens when we get a RechargeSuccessEvent
     */
    @Test
    public void testOnApplicationEvent() throws Exception {
	RechargeSuccessEvent event = new RechargeSuccessEvent("utest-source");
	event.setLookupId("abcd-1234");
	event.setNumber("7760577499"); // This is the OnCall phone number

	gss.onApplicationEvent(event);
    }
}
