package com.freecharge.common.comm.fulfillment;

import java.io.Serializable;

public class CampaignTxnDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TxnDetails txnDetails;
    private TxnParams txnParams;
    
    
	public TxnDetails getTxnDetails() {
		return txnDetails;
	}
	public void setTxnDetails(TxnDetails txnDetails) {
		this.txnDetails = txnDetails;
	}
	public TxnParams getTxnParams() {
		return txnParams;
	}
	public void setTxnParams(TxnParams txnParams) {
		this.txnParams = txnParams;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CampaignTxnDTO [txnDetails=").append(txnDetails).append(", txnParams=").append(txnParams)
				.append("]");
		return builder.toString();
	}

}
