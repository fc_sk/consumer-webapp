package com.freecharge.common.comm.fulfillment;

public class TransactionStatus {

    String transactionStatus;
    String subscriberIdentificationNumber;
    String serviceProvider;
    String productType;
    String serviceRegion;
    String operator;
    String planType;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getServiceRegion() {
        return serviceRegion;
    }

    public void setServiceRegion(String serviceRegion) {
        this.serviceRegion = serviceRegion;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getSubscriberIdentificationNumber() {
        return subscriberIdentificationNumber;
    }

    public void setSubscriberIdentificationNumber(String subscriberIdentificationNumber) {
        this.subscriberIdentificationNumber = subscriberIdentificationNumber;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }
    
    public String getPlanType() {
           return planType;
    }

    public void setPlanType(String planType) {
           this.planType = planType;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransactionStatus [transactionStatus=").append(transactionStatus)
				.append(", subscriberIdentificationNumber=").append(subscriberIdentificationNumber)
				.append(", serviceProvider=").append(serviceProvider).append(", productType=").append(productType)
				.append(", serviceRegion=").append(serviceRegion).append(", operator=").append(operator)
				.append(", planType=").append(planType).append("]");
		return builder.toString();
	}

  }
