package com.freecharge.common.comm.fulfillment;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.CartService;
import com.freecharge.common.businessdo.CartItemsBusinessDO;
import com.freecharge.common.comm.email.EmailService;
import com.freecharge.common.comm.notification.GCMNotificationService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.freefund.services.FreefundService;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.mongo.repos.PromocodeFailMsgRepository;
import com.freecharge.rest.onecheck.model.WalletResponseDo;
import com.freecharge.wallet.CampaignCashRewardService;
import com.freecharge.wallet.OneCheckWalletService;
import com.freecharge.wallet.WalletService;
import com.freecharge.wallet.service.Wallet;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service("cashbackService")
public class CashbackService {

    @Autowired
    private FreefundService freefundService;

    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private CartService cartService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private WalletService walletService;
    
    @Autowired
    private PromocodeFailMsgRepository promocodeFailMsgRepository;
    
    @Autowired
    private GCMNotificationService gcmNotificationService;
    
    @Autowired
    private FCProperties fcproperties;

    @Autowired
    private AppConfigService appConfigService;
    
    private Logger logger = LoggingFactory.getLogger(getClass());

    public static final String PROMOCODE_ORDERID = "order_id";
    public static final String PROMOCODE = "promocode";
    public static final String PROMOCODE_FAILED_MSG = "failedMsg";

    @Autowired
    private CampaignCashRewardService campaignCashRewardService;

    /** This method is responsible for Promocode Cashback and sending success email
     *
     * @param cartItemsBusinessDO gives Cart-Freefunds details for the Cashback eligible transactions
     * @param orderId of the successful transaction
     * @param userId of the successful transaction
     * @return boolean status, true if
     *      1] cashback and mail sending is successful. Also send GCM notification on cashback success.
     *      2] wallet cash back fails and we sent failure cashback mail. Else returns false. 
     */
    public boolean doPromocodeCashback(final CartItemsBusinessDO  cartItemsBusinessDO, final String orderId, 
            final Integer userId, final String fcChannel, Float additionalCashbackAmount) {
        if (orderId == null || cartItemsBusinessDO == null || userId == null) {
            logger.warn("Promocode cashback not initiated as OrderID/ CartItemsBusinessDO/ userId is null");
            return false;
        }
        
        FreefundCoupon freefundCoupon = freefundService.getFreefundCoupon((long) cartItemsBusinessDO.getItemId()); 
        
        if (freefundCoupon == null) {
            logger.warn("Promocode cashback not initiated for OrderID: " + orderId + " as freefundCoupon is null.");
            return false;
        }     
        
        FreefundClass freefundClass = freefundService.getFreefundClass(freefundCoupon.getFreefundCode());
        
        if (freefundClass == null) {
            logger.warn("Promocode cashback not initiated for OrderID: " + orderId + " as freefundClass is null.");
            return false;
        }
  
        Double discountedValue = cartItemsBusinessDO.getPrice() + new Double(additionalCashbackAmount);
        
        if(discountedValue <= 0) {
            logger.warn("Promocode cashback not initiated for OrderID: " + orderId + " as Cashback value is " + discountedValue);                  

            return false;
        }
        
        String metaData = freefundClass.getPromoEntity();
        if (freefundClass.getDescription() != null && !freefundClass.getDescription().isEmpty()) {
            metaData = freefundClass.getDescription();
        }
        
        String callerReference = getCallerReference(orderId, freefundClass.getName(), freefundCoupon.getId());    
        WalletResponseDo response = null;
        String reasons = ErrorCode.WALLET_CREDIT_FAIL + " : Wallet amount exceeds limit";        
        String detailedReasons = "You have reached the Maximum Credit Limit of Rs. " 
                + fcproperties.getProperty(FCConstants.WALLET_CREDIT_BALANCE_LIMIT) + " that can be added to "
                + "FreeCharge Credits.";
        // TODO: Reason should be changed as per the wallet response
        
        //Cashback processing
        try {
            logger.info("Promocode cashback initiated for OrderID: " + orderId + " and freefundCouponId: " 
                    + freefundCoupon.getId());
            response = campaignCashRewardService.voucherCredit((long) userId, discountedValue, orderId, callerReference,
                    Wallet.FundSource.CASHBACK.name(), freefundCoupon.getId(), metaData,
                    OneCheckWalletService.OneCheckTxnType.CREDIT_PROMOCODE,
                    fcproperties.getProperty(FCConstants.PROMOTION_CORP_ID));

        } catch (Exception e) {
            logger.error("Promocode wallet cashback failed for OrderID : " + orderId, e);
            response.setStatusCode(FCConstants.FAILURE);
            response.setErrorMessage("Promocode wallet cashback failed");
        }

        if (!response.getStatusCode().equalsIgnoreCase(FCConstants.SUCCESS_STATUS)) {

            logger.info(ErrorCode.WALLET_CREDIT_FAIL + " : Promocode wallet cashback failed for OrderID : " + orderId
                    + ", freefundCouponId : " + freefundCoupon.getId() + ", discountedValue : " + discountedValue);

            JSONObject versionObject = appConfigService.getCustomProp(FCConstants.PROMO_FAILED_EMAIL_KEY);
            if (FCUtil.isNotEmpty(versionObject)) {
                String shouldSendFailureMail = String.valueOf(versionObject.get(FCConstants.SEND_MAIL));
                if (FCUtil.isNotEmpty(shouldSendFailureMail) && "true".equalsIgnoreCase(shouldSendFailureMail)) {
                    //'Cashback Failed' email sending
                    return notifyPromocodeCashbackFailure(orderId, userId, freefundClass.getName(),
                            freefundCoupon.getFreefundCode(), reasons, detailedReasons);
                } else {
                    return false;
                }
            }
        }

        generateCashbackNotification(orderId, userId, fcChannel, freefundClass.getName(), discountedValue);
        //'Cashback Success' email sending
        String mailTemplate = "promocodeCashback";
        String subject = "Your cash back of Rs." + discountedValue.intValue() + " has been added to your account "
                + "for order " + orderId;    
        return generateCashbackMailer(FCConstants.SUCCESS, orderId, userId, freefundClass.getName(),
                freefundClass.getDescription(), freefundCoupon.getFreefundCode(),
                discountedValue, reasons, mailTemplate, subject, additionalCashbackAmount);

    }


    /** This method is responsible for notifying Promocode Cashback Failure via email
     * @param orderId of the successful transaction
     * @param userId of the successful transaction
     * @param offerName states the name of the offer i.e. freefund class name
     * @param freefundCode states freefund code used 
     * @param failureReasons states reasons for the non-eligibility for cashback
     * @return boolean status, true if no-cashback mail sending is successful otherwise false
     */   
    public boolean notifyPromocodeCashbackFailure(String orderId, Integer userId, String offerName, String freefundCode,
                        String failureReasons, String detailedFailureReasons){
        String mailTemplate = "promocodeNoCashback";
        String subject = "Regarding your cashback for order " + orderId;     
        Double cashbackValue = 0.0;
        String offerDescription = "";

        // PromocodeFailMsg mongodb - entry for cashback failure
        addPromocodeCashbackFailureReason(orderId, freefundCode, failureReasons);

        return generateCashbackMailer(FCConstants.FAIL, orderId, userId, offerName, offerDescription, freefundCode, cashbackValue,
                detailedFailureReasons, mailTemplate, subject, null);
    }

    public boolean addPromocodeCashbackFailureReason(String orderId, String freefundCode, String failureReasons) {
        try {
            Map<String, Object> promocodeFailMsgInfoMap = new HashMap<String, Object>();
            promocodeFailMsgInfoMap.put(PROMOCODE_ORDERID, orderId);
            promocodeFailMsgInfoMap.put(PROMOCODE, freefundCode);
            promocodeFailMsgInfoMap.put(PROMOCODE_FAILED_MSG, failureReasons);
            promocodeFailMsgRepository.addPromocodeFailMsg(promocodeFailMsgInfoMap);

        } catch (Exception e) {
            logger.error("PromocodeFailMsg mongodb entry failed for " + orderId, e);
            return false;
        }
        return true;
    }
          
    public boolean generateCashbackMailer(String status, String orderId, Integer userId, String offerName,
                                           String offerDescription, String freefundCode, Double cashbackValue,
                                           String reasons, String mailTemplate, String subject,
                                           Float additionalCashbackAmount){

        UserProfile userProfile = userServiceProxy.getUserProfile(userId);   
        Users user = userServiceProxy.getUserByUserId(userId); 

        try {
            emailService.sendPromocodeCashbackMail(userProfile.getFirstName(), user.getEmail(), subject, mailTemplate, 
                            offerName, offerDescription, freefundCode, cashbackValue, reasons, orderId, additionalCashbackAmount);
            logger.info("Promocode Cashback " + status + " mail sent for OrderID : " + orderId + "," +
            		    " Reasons: " + reasons);

        } catch (Exception e) {
            logger.error("Promocode Cashback " + status + " mail sending failed for OrderID : " + orderId 
                    + ", Reasons: " + reasons, e);
            return false;
        } 

        return true;
    }

    private void generateCashbackNotification(final String orderId, final Integer userId, final String fcChannel, 
            final String offerName, final Double cashbackValue) {

        if (FCUtil.isEmpty(fcChannel) || Integer.parseInt(fcChannel) != FCConstants.CHANNEL_ID_ANDROID_APP) {
            logger.info("Cashback Success Notification not sent for userId : " + userId + ", orderId : " + orderId 
                    + ", fcChannel : " + fcChannel + ", offerName : " + offerName + ", cashbackValue : " 
                    + cashbackValue);
            return;
        }

        if (!appConfigService.isChildPropertyEnabled(AppConfigService.GCM_NOTIFICATION_CONFIG, 
                AppConfigService.GCM_PROMOCODE_CASHBACK_NOTIFICATION_ENABLED)) {
            logger.info("Promocode Cashback Notification [Disabled] not sent for orderId : " + orderId + ", userId : " 
                    + userId + ", fcChannel : " + fcChannel + ", offerName : " + offerName + ", cashbackValue : " 
                    + cashbackValue);
            return;
        }

        Users user = userServiceProxy.getUserByUserId(userId); 
        String shopNo = "21";
        String locationId = "3";
        String messageTitle = "Rs." + cashbackValue.intValue() + " Cashback added";
        String messageDescription = "for " + offerName;
        String messageUrl = "launch:com.freecharge.android:AddCashback";
        String messageAction = "launch";

        gcmNotificationService.sendGCMNotification(user.getEmail(), shopNo, locationId, messageTitle, messageUrl, 
                messageDescription, "", messageAction, null, null, null, null, Wallet.FundSource.CASHBACK.name(), 
                fcChannel);
        logger.info("Cashback Success Notification initiated for email : " + user.getEmail() + ", orderId : " + orderId 
                + ", fcChannel : " + fcChannel + ", offerName : " + offerName + ", cashbackValue : " + cashbackValue);
    }

    public String getCallerReference(final String orderId, final String offerName, final Long couponId) {
        return "PROMOCODE_CASHBACK_" + orderId + "_" + offerName + "_" + couponId;
    }

}
