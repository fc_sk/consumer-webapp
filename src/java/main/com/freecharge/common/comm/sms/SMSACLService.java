package com.freecharge.common.comm.sms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service("smsACLService")
public class SMSACLService {

    private static Logger logger = LoggingFactory.getLogger("did.sms."+SMSACLService.class.getName());

    public static String getBeanName() {
        return "smsACLService";
    }

    @Autowired
    @Qualifier("aclProps")
    private ACLProps aclProps;

    public boolean sendSMS(String mobileNo, String message, String promocode) {
        boolean isSMSSent = false;

        if (!aclProps.isEnabled()) {
            return false;
        }

        try {
            logger.debug("[SMS Gateway] ACL SMS API : " + aclProps.getUserid() + " , password : XXXXXXXX and smsurl : "
                    + aclProps.getSmsurl());

            logger.debug("message : " + message + " , mobileNo : " + mobileNo);
            /*
             * http://push1.maccesssmspush.com/servlet/com.aclwireless.pushconnectivity.listeners.TextListener
             * ?userId=asalt&pass=asalt&appid=asalt&subappid=asalt&contenttype=1&to=91XXXXXXXXXX&from=FCHRGE& text=Thx
             * for voting! Use promocode (456dfy456i) n get Rs10 off on mobile/dth/datacard recharge on
             * www.freecharge.com once per week, valid on all operators TnC apply. &selfid=true&alert=1&dlrreq=true
             */
            String data = "";
            data += "&userId=" + aclProps.getUserid();
            data += "&pass=" + URLEncoder.encode(aclProps.getPass(), "UTF-8");
            data += "&appid=" + aclProps.getAppid();
            data += "&subappid=" + aclProps.getSubappid();
            data += "&contenttype=1";
            data += "&to=" + URLEncoder.encode(mobileNo, "UTF-8");
            data += "&from=" + aclProps.getFrom();
            data += "&text=" + URLEncoder.encode(message, "UTF-8");
            data += "&selfid=true";
            data += "&alert=1";
            data += "&dlrreq=true";

            logger.debug("[SMS Gateway] Data with url for sms gateway ACL : " + aclProps.getSmsurl() + "?" + data, null);

            URL url = new URL(aclProps.getSmsurl() + "?" + data);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("GET");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.connect();
            int responseCode = conn.getResponseCode();
            if (responseCode == 200) {
                BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                StringBuffer buffer = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    buffer.append(line).append("\n");
                }
                logger.debug("[SMS Gateway] Output from sms(" + mobileNo + ") gateway ACL : " + buffer.toString(), null);
                rd.close();
                conn.disconnect();

                String responseContent = buffer.toString();
                logger.info("sent sms to " + mobileNo + " for ( "+promocode+" ) , response : " + responseContent);
                if (!responseContent.startsWith("-")) {
                    isSMSSent = true;
                }
            } else {
                logger.error("[SMS Gateway] Got responseCode : " + responseCode + " while sending sms to " + mobileNo,
                        null);
            }
        } catch (Exception e) {
            logger.error("[SMS Gateway] Error in sending sms for ACL (sendSMS) " + mobileNo, e);
        }

        return isSMSSent;
    }

    public boolean sendMultiSMS(Map<String, String> mutliSMSmap, List<String> promocodes) throws IOException {
       
        if (!aclProps.isEnabled()) {
            return false;
        }
        
        String xmlRequest = getXmlRequest(mutliSMSmap);
        HttpClient httpclient = new HttpClient();
        PostMethod post = new PostMethod(aclProps.getMutlismsurl());
        post.setRequestEntity(new StringRequestEntity(xmlRequest, "text/xml", "ISO-8859-1"));
        httpclient.executeMethod(post);
        InputStream in = post.getResponseBodyAsStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        StringBuffer buffer = new StringBuffer();
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
            buffer.append("\n");
        }
        reader.close();
        logger.info("sent sms to " + mutliSMSmap.keySet() + " for ( "+promocodes+" ) , response : " + buffer.toString());
        Document responseDoc = convertStringToDocument(buffer.toString());
        int status = getStatusCode(responseDoc);
        return (status == 1) ? true : false;
    }

    private int getStatusCode(Document doc) {

        int status = 0;
        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("push");

        for (int i = 0; i < nList.getLength(); i++) {

            Node nNode = nList.item(i);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;
                try {
                    status = Integer.parseInt(eElement.getElementsByTagName("status").item(0).getTextContent());
                } catch (Exception e) {

                }
            }
        }
        return status;
    }

    private String getXmlRequest(Map<String, String> mutliSMSmap) {

        StringBuffer sbuf = new StringBuffer();

        sbuf.append("<?xml version=\"1.0\"?>");

        sbuf.append("<push>");

        sbuf.append("<appid>" + aclProps.getAppid() + "</appid>");

        sbuf.append("<subappid>" + aclProps.getSubappid() + "</subappid>");

        sbuf.append("<userid>" + aclProps.getUserid() + "</userid>");

        sbuf.append("<pass>" + aclProps.getPass() + "</pass>");

        sbuf.append("<acct-type>1</acct-type>");

        sbuf.append("<msgid>" + UUID.randomUUID().toString() + "</msgid>");

        sbuf.append("<content-type>1</content-type>");

        sbuf.append("<priority>true</priority>");

        sbuf.append("<masking>true</masking>");

        sbuf.append("<from>" + aclProps.getFrom() + "</from>");

        sbuf.append("<multisms>");

        int i = 0;
        for (String mobile : mutliSMSmap.keySet()) {
            sbuf.append("<detail msisdn=\"" + mobile + "\" id=\"" + (i++) + "\" msg=\"" + mutliSMSmap.get(mobile)
                    + "\" />");
        }

        sbuf.append("</multisms>");

        sbuf.append("<dlr>true</dlr>");

        sbuf.append("<alert>1</alert>");

        sbuf.append("</push>");

        return sbuf.toString();
    }

    private Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
