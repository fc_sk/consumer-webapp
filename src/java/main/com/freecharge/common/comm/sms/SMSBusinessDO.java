package com.freecharge.common.comm.sms;

import java.util.Map;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: Rananjay
 * Date: Jun 1, 2012
 * Time: 12:38:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class SMSBusinessDO extends BaseBusinessDO {

    private String templateName;
    private Map<String, String> variableValues;
    private String text;
    private String receiverNumber;
    private String registeredUserEmail;
    

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Map<String, String> getVariableValues() {
        return variableValues;
    }

    public void setVariableValues(Map<String, String> variableValues) {
        this.variableValues = variableValues;
    }
    
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getReceiverNumber() {
        return receiverNumber;
    }

    public void setReceiverNumber(String receiverNumber) {
        this.receiverNumber = receiverNumber;
    }

    public String getRegisteredUserEmail() {
        return registeredUserEmail;
    }

    public void setRegisteredUserEmail(String registeredUserEmail) {
        this.registeredUserEmail = registeredUserEmail;
    }
}
