package com.freecharge.common.comm.sms;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;

@Service("smsTemplateService")
public class SMSTemplateService {

    private Logger logger = LoggingFactory.getLogger(getClass());
    //[prepaid|postpaid|dth|datacard][web|mweb|app][rechargeSelf|rechargeOther][couponYes|couponNo]
    public static Map<String, String> templatesMap = new HashMap<String, String>(){{
        
        //Success SMS templates
        put("prepaid-web-self-yes", "Recharge of Rs. #amount# for #mobile# is successful. Dial #ussd# to check updated balance. #operatorTxnText# #operatorTxnId# For offers,visit http://frch.in/OF");
        put("prepaid-web-self-no", "Recharge of Rs. #amount# for #mobile# is successful. Dial #ussd# to check updated balance. #operatorTxnText# #operatorTxnId# For offers,visit http://frch.in/OF");
        put("prepaid-app-self-yes", "Recharge of Rs. #amount# for #mobile# is successful. Dial #ussd# to check updated balance. #operatorTxnText# #operatorTxnId# For offers,visit http://frch.in/OF");
        put("prepaid-app-self-no", "Recharge of Rs. #amount# for #mobile# is successful. Dial #ussd# to check updated balance. #operatorTxnText# #operatorTxnId# For offers,visit http://frch.in/OF");
        put("prepaid-web-other-yes", "Recharge of Rs. #amount# for #mobile# is successful. Dial #ussd# to check updated balance. #operatorTxnText# #operatorTxnId# For offers,visit http://frch.in/OF");
        put("prepaid-web-other-no", "Recharge of Rs. #amount# for #mobile# is successful. Dial #ussd# to check updated balance. #operatorTxnText# #operatorTxnId# For offers,visit http://frch.in/OF");
        put("prepaid-app-other-yes", "Recharge of Rs. #amount# for #mobile# is successful. Dial #ussd# to check updated balance. #operatorTxnText# #operatorTxnId# For offers,visit http://frch.in/OF");
        put("prepaid-app-other-no",  "Recharge of Rs. #amount# for #mobile# is successful. Dial #ussd# to check updated balance. #operatorTxnText# #operatorTxnId# For offers,visit http://frch.in/OF");
       
        put("dth-web-other-yes", "Your DTH recharge of Rs. #amount# is successful. Email care@freecharge.in for queries. For exciting offers,visit http://frch.in/OF");
        put("dth-web-other-no", "Your DTH recharge of Rs. #amount# is successful. Email care@freecharge.in for queries. For exciting offers,visit http://frch.in/OF");
        put("dth-app-other-yes", "Your DTH recharge of Rs. #amount# is successful. Email care@freecharge.in for queries. For exciting offers,visit http://frch.in/OF");
        put("dth-app-other-no",  "Your DTH recharge of Rs. #amount# is successful. Email care@freecharge.in for queries. For exciting offers,visit http://frch.in/OF");
        
        put("datacard-web-other-yes", "Recharge of Rs. #amount# for #mobile# is successful, coupons emailed to you. Recharge Mobile instantly using FreeCharge App, Download Now: http://frch.in/ToApp");
        put("datacard-web-other-no", "Recharge of Rs. #amount# for #mobile# is successful. Recharge Mobile instantly using FreeCharge App, Download Now: http://frch.in/ToApp");
        put("datacard-app-other-yes", "Recharge of Rs. #amount# for #mobile# is successful, coupons emailed to you. Recharge Mobile instantly using FreeCharge App, Download Now: http://frch.in/ToApp");
        put("datacard-app-other-no",  "Recharge of Rs. #amount# for #mobile# is successful. Recharge Mobile instantly using FreeCharge App, Download Now: http://frch.in/ToApp");

        put("postpaid-web-self-yes", "Bill Payment of Rs. #amount# for #mobile# is successful. Email care@freecharge.in for queries.For exciting offers,visit http://frch.in/OF");
        put("postpaid-web-self-no", "Bill Payment of Rs. #amount# for #mobile# is successful. Email care@freecharge.in for queries.For exciting offers,visit http://frch.in/OF");
        put("postpaid-app-self-yes", "Your Bill Payment of Rs. #amount# for #mobile# is successful. Email care@freecharge.in for queries. For exciting offers,visit http://frch.in/OF");
        put("postpaid-app-self-no", "Your Bill Payment of Rs. #amount# for #mobile# is successful. Email care@freecharge.in for queries. For exciting offers,visit http://frch.in/OF");
        put("postpaid-web-other-yes", "Bill Payment of Rs. #amount# for #mobile# is successful. Email care@freecharge.in for queries.For exciting offers,visit http://frch.in/OF");
        put("postpaid-web-other-no", "Bill Payment of Rs. #amount# for #mobile# is successful. Email care@freecharge.in for queries.For exciting offers,visit http://frch.in/OF");
        put("postpaid-app-other-yes", "Bill Payment of Rs. #amount# for #mobile# is successful. Email care@freecharge.in for queries.For exciting offers,visit http://frch.in/OF");
        put("postpaid-app-other-no",  "Bill Payment of Rs. #amount# for #mobile# is successful. Email care@freecharge.in for queries.For exciting offers,visit http://frch.in/OF");
        
        put("billpay-success", "Payment for Rs. #amount# for #billercode# (#mobile#) done successfully. Biller usually takes 3 business days to process it. #aggregatorTxt# #aggregatorTxnId#");
        put("metro-success", "Recharge of Rs.#amount# for #billercode# done successfuly. Visit a TVM at any station to validate your recharge. Queries? Write to care@freecharge.com");
        
        //Pending SMS templates
        put("prepaid-pending",  "Hi #firstName#, confirmation of Rs. #amount# recharge for #mobile# is pending from #operatorName#. Usually it takes few minutes. If not successful, we will refund within 3 hrs to Freecharge wallet");
        put("prepaid-other-pending",  "Recharge of Rs.#amount# for #mobile# (Order ID - #orderno#) is in process. You will receive further update within 2 hrs.");
        put("billpay-pending",  "Dear Customer, Your transaction on FreeCharge for order id #orderno# is in process. You will receive an update via email or sms within 2 hrs.");
        
        put("postpaid-pending",  "Dear Customer, Your transaction on FreeCharge for order id #orderno# is in process. You will receive an update via email or sms within 2 hrs.");
        
        //Failure SMS templates
        put("prepaid-failure",  "Recharge of Rs.#amount# for #mobile# (Order ID #orderno#) has failed. Your amount will be refunded to your #refundTo# within #refundTime# hours.");
        put("postpaid-failure",  "Dear Customer, Your transaction on FreeCharge for order id #orderno# was unsuccessful. Your amount will be refunded to your #refundTo# within #refundTime# hours. For queries, email us: care@freecharge.com");
        put("billpay-failure",  "Dear Customer, Your transaction on FreeCharge for order id #orderno# was unsuccessful. Your amount will be refunded to your #refundTo# within #refundTime# hours. For queries, email us: care@freecharge.com");
    }};
    
    public String getSuccessSMSText(String productType, Boolean isAppOrder, Boolean isSelfRecharge, Boolean hasCoupons, Map<String, String> templateVariables) {
        StringBuffer key = new StringBuffer("");
        if(productType != null) {
            if (productType.equals("V")){
                key.append("prepaid");
            } else if (productType.equals("D")){
                key.append("dth");
            } else if (productType.equals("C") || productType.equals("F")){
                key.append("datacard");
            } else if (productType.equals("M")){
                key.append("postpaid");
            } else if (ProductName.fromPrimaryProductType(productType) == ProductName.Metro) {
                key.append("metro-success");
            } else if (FCUtil.isUtilityPaymentType(productType)) {
                key.append("billpay-success");
            }
        }
        if (!FCUtil.isUtilityPaymentType(productType)) {
            if(isAppOrder) {
                key.append("-app");
            } else {
                key.append("-web");  
            }
            if(isSelfRecharge) {
                key.append("-self");  
            } else {
                key.append("-other");   
            }
            if(hasCoupons) {
                key.append("-yes");  
            } else {
                key.append("-no");   
            }
        }
        String template = SMSTemplateService.templatesMap.get(key.toString());
        
        if(template != null) {
            return generateCompleteSMS(template, templateVariables);
        } else {
            return null;
        }
    }
    
    public String getPendingSMSText(String productType, Map<String, String> templateVariables) {
        StringBuffer key = new StringBuffer("");
		if (productType != null) {
			if (productType.equals("V")) {
				key.append("prepaid");
			} else if (productType.equals("D") || productType.equals("C") || productType.equals("F")) {
				key.append("prepaid-other");
			} else if (FCUtil.isUtilityPaymentType(productType)) {
				key.append("billpay");
			} else if (FCUtil.isBillPostpaidType(productType)) {
				key.append("postpaid");
			}

		}
        key.append("-pending");
        String template = SMSTemplateService.templatesMap.get(key.toString());
        
        if(template != null) {
            return generateCompleteSMS(template, templateVariables);
        } else {
            return null;
        }
    }

    public String getFailureSMSText(String productType, Map<String, String> templateVariables) {
        StringBuffer key = new StringBuffer("");
        if(productType != null) {
            if (productType.equals("V") || productType.equals("D") || productType.equals("C") || productType.equals("F")){
                key.append("prepaid");
            } else if (productType.equals("M")){
                key.append("postpaid");
            } else if (FCUtil.isUtilityPaymentType(productType)) {
                key.append("billpay");
            }
        }
        key.append("-failure");
        String template = SMSTemplateService.templatesMap.get(key.toString());
        
        if(template != null) {
            return generateCompleteSMS(template, templateVariables);
        } else {
            return null;
        }
    }

    private String generateCompleteSMS(String template, Map<String, String> templateVariables) {
        for(String key : templateVariables.keySet()) {
            template = template.replace("#"+key+"#", templateVariables.get(key));
        }
        return template;
    }
    
    public static void main(String[] args) {
        /*ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        SMSTemplateService smsTemplateService = ((SMSTemplateService) ctx.getBean("smsTemplateService"));
        String text = smsTemplateService.getSuccessSMSText("M", true, false, true, "100", "9986648913");
        System.out.println(text);*/
    }

}
