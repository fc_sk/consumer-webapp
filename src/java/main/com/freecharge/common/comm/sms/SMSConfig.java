package com.freecharge.common.comm.sms;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;

@Configuration
public class SMSConfig {

    private Logger       logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private FCProperties fcProperties;

    @Bean
    public ACLProps aclProps() {

        ACLProps aclProps = new ACLProps();
        aclProps.setEnabled(fcProperties.isSmsAclEnabled());
        aclProps.setUserid(fcProperties.getSmsAclUserid());
        aclProps.setPass(fcProperties.getSmsAclPass());
        aclProps.setSmsurl(fcProperties.getSmsAclSmsUrl());
        aclProps.setMutlismsurl(fcProperties.getSmsAclMultiSmsUrl());
        aclProps.setAppid(fcProperties.getSmsAclAppid());
        aclProps.setSubappid(fcProperties.getSmsAclSubAppid());
        aclProps.setFrom(fcProperties.getSmsAclFrom());
        return aclProps;
    }

}
