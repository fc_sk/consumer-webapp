package com.freecharge.common.comm.sms;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;

/**
 * Created by IntelliJ IDEA.
 * User: Rananjay
 * Date: Jun 14, 2012
 * Time: 10:09:57 AM
 * To change this template use File | Settings | File Templates.
 */
public class SMSValueFirstService implements SMSInterface {
    private static Logger logger = LoggingFactory.getLogger(SMSValueFirstService.class);

    private String smsvaluefirstusername;
    private String smsvaluefirstpassword;
    private String smsvaluefirsturl;
    private String smsvaluefirstfrom;

    @Autowired
    private FCProperties fcProperties;

    public void setSmsvaluefirstfrom(String smsvaluefirstfrom) {
        this.smsvaluefirstfrom = smsvaluefirstfrom;
    }

    public void setSmsvaluefirstusername(String smsvaluefirstusername) {
        this.smsvaluefirstusername = smsvaluefirstusername;
    }

    public void setSmsvaluefirstpassword(String smsvaluefirstpassword) {
        this.smsvaluefirstpassword = smsvaluefirstpassword;
    }

    public void setSmsvaluefirsturl(String smsvaluefirsturl) {
        this.smsvaluefirsturl = smsvaluefirsturl;
    }

    // private static String SMS_SERVICE_URL = "http://api.myvaluefirst.com/psms/servlet/psms.Eservice2";
    private static String ADDRESS_TAG = "<ADDRESS FROM=\"{0}\" TO=\"{1}\" SEQ=\"{2}\" />";

    //Appended text <!DOCTYPE...........
    private static String DATA_TAG = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" +
            "<!DOCTYPE MESSAGE SYSTEM \"http://127.0.0.1/psms/dtd/message.dtd\" >" +
            "<MESSAGE>" +
            "<USER USERNAME=\"{0}\" PASSWORD=\"{1}\"/>" +
            "<SMS UDH=\"0\" CODING=\"1\" TEXT=\"{2}\" PROPERTY=\"0\" ID=\"1\">{3}</SMS>" +
            "</MESSAGE>";


    public boolean sendSMS(String Message, String MobileNo) {
        logger.info("[SMS Gateway] smsvaluefirstusername : " + smsvaluefirstusername + " ,smsvaluefirstpassword : XXXXXXXX and smsvaluefirsturl : " + smsvaluefirsturl);

        boolean isSMSSend = false;
        MobileNo = "91"+MobileNo;
        Object[] array = new Object[]{MobileNo};
        java.util.List toList = Arrays.asList(array);
        HttpClient client = new HttpClient();

        HttpConnectionManagerParams connectionParams = new HttpConnectionManagerParams();
        connectionParams.setConnectionTimeout(fcProperties.getGupshupConnectionTimeout());
        connectionParams.setSoTimeout(fcProperties.getGupshupReadTimeout());
        client.getHttpConnectionManager().setParams(connectionParams);

        PostMethod method = new PostMethod(smsvaluefirsturl);
        try {

            String dataParameter = getDataParameter(smsvaluefirstfrom, toList, encodeHTML(Message));
            logger.info("[SMS Gateway] dataParameter for SMSValueFirst SMS Gateway for Mobile no : " + MobileNo + " : " + dataParameter, null);                            
            
            method.addParameter("data", dataParameter);
            method.addParameter("action", "send");
            int result = client.executeMethod(method);
            logger.info("[SMS Gateway] result from SMSValueFirst SMS Gateway for Mobile no : " + MobileNo + " = " + result+ " and HttpStatus : "+HttpStatus.SC_OK, null);
            if (result == HttpStatus.SC_OK) {
                logger.info("[SMS Gateway] Status Line from SMSValueFirst SMS Gateway for Mobile no : " + MobileNo + " = " + method.getStatusLine(), null);
                isSMSSend = true;
            }
            
        } catch (HttpException e) {
            logger.error("[SMS Gateway] Error in sending SMS from  SMSValueFirst SMS Gateway for Mobile No : " + MobileNo, e);
        } catch (IOException e) {
            logger.error("[SMS Gateway] Error in sending SMS from  SMSValueFirst SMS Gateway for Mobile No : " + MobileNo, e);
        } finally {
            // Release the connection.
            method.releaseConnection();
        }
        return isSMSSend;

    }

    private String getDataParameter(String from, List toList, String text) {

        final String userName = smsvaluefirstusername;
        final String password = smsvaluefirstpassword;

        StringBuffer addressTags = new StringBuffer();
        for (int i = 0; i < toList.size(); i++) {
            String address = (String) toList.get(i);
            addressTags.append(MessageFormat.format(ADDRESS_TAG, new Object[]{
                    from, address, new Integer(i + 1)}));
        }
        return MessageFormat.format(DATA_TAG, new Object[]{userName, password, text, addressTags});
    }


    private static String encodeHTML(String s) {
        StringBuffer out = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if ((c >= 32 && c <= 46) || (c >= 58 && c <= 64) || (c >= 91 && c <= 96) || (c >= 123 && c <= 126)) {
                out.append("&#" + (int) c + ";");
            } else {
                out.append(c);
            }
        }
        return out.toString();
    }
}
