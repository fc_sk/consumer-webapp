package com.freecharge.common.comm.sms;

import com.amazonaws.services.sqs.model.SendMessageResult;
import com.freecharge.app.domain.dao.CartDAO;
import com.freecharge.app.domain.dao.jdbc.UserWriteDao;
import com.freecharge.app.domain.entity.Cart;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.app.service.CartService;
import com.freecharge.app.service.OrderService;
import com.freecharge.app.service.RechargeInitiateService;
import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.properties.FCPropertyPlaceholderConfigurer;
import com.freecharge.common.util.CouponInventoryUtil;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.notification.client.sms.AmazonSqsServiceForSms;
import com.freecharge.notification.service.common.INotificationToolApiService;
import com.freecharge.notification.sms.SendSmsRequest;
import com.freecharge.payment.dao.ConvinenceFeeDetailsDao;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.util.RechargeConstants;
import com.freecharge.web.webdo.FeeCharge;
import com.google.common.base.Strings;
import com.google.gson.Gson;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class SMSService {

	private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private FCPropertyPlaceholderConfigurer fcPropertyPlaceholderConfigurer;

	@Autowired
	private FCProperties fcProperties;

	@Autowired
	SMSFactory smsFactory;

	@Autowired
	private VelocityEngine velocityEngine;

	@Autowired
	private DelayedUnderProcessSmsService delayedUnderProcessSmsService;
	
	@Autowired
	private ConvinenceFeeDetailsDao convinenceFeeDetailsDao;
	
	@Autowired
	private UserWriteDao userWriteDao;

	@Autowired
	private CartService cartService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private SMSTemplateService smsTemplateService;

	@Autowired
	private UserServiceProxy userServiceProxy;

	@Autowired
	private RechargeInitiateService rechargeInitiateService;

	@Autowired
	@Qualifier("notificationApiServiceClient")
	private INotificationToolApiService notificationService;

	@Autowired
	@Qualifier("notificationConnectionManager")
	private MultiThreadedHttpConnectionManager notificationConnectionManager;

	@Autowired
	private AmazonSqsServiceForSms amazonSqsService;

	private static final ObjectMapper jsonMapper = new ObjectMapper();
	
	private static final String BILLDESK_AGGREGATOR_NAME = "bdk";


	private static final Map<String, String> SMS_TEMPLATE_ENABLE_PROPERTY_MAP = new HashMap<String, String>(4) {
		{
			put(FCConstants.SMS_TEMPLATE_RECHARGE_UNDER_PROCESS, FCConstants.PROPERTY_SMS_RECHARGE_UNDER_PROCESS);
			put(FCConstants.SMS_TEMPLATE_RECHARGE_SUCCESSFUL, FCConstants.PROPERTY_SMS_RECHARGE_SUCCESSFUL);
			put(FCConstants.SMS_TEMPLATE_RECHARGE_FAILED, FCConstants.PROPERTY_SMS_RECHARGE_FAILED);
			put(FCConstants.SMS_TEMPLATE_RECHARGE_REFUND, FCConstants.PROPERTY_SMS_RECHARGE_REFUND);
			put(FCConstants.SMS_TEMPLATE_M_COUPONS, FCConstants.PROPERTY_SMS_MCOUPON);
			put(FCConstants.SMS_TEMPLATE_E_COUPONS, FCConstants.PROPERTY_SMS_ECOUPON);
			put(FCConstants.SMS_TEMPLATE_EMULTI_COUPONS, FCConstants.PROPERTY_SMS_ECOUPON);
			put(FCConstants.SMS_TEMPLATE_BANK_REFUND_SUCCESS,FCConstants.PROPERTY_SMS_BANK_REFUND_SUCCESS);
			put(FCConstants.SMS_TEMPLATE_POSTPAID_SUCCESSFUL, FCConstants.PROPERTY_SMS_POSTPAID_SUCCESSFUL);
			put(FCConstants.SMS_TEMPLATE_POSTPAID_FAILED, FCConstants.PROPERTY_SMS_POSTPAID_FAILED);
			put(FCConstants.SMS_TEMPLATE_SPECIAL_OFFER_CASHBACK_SUCCESS, FCConstants.PROPERTY_SMS_SPECIAL_OFFER_SUCCESS);
			put(FCConstants.SMS_TEMPLATE_DIWALI_OFFER_COMPETITION_ENTERED_NOTIFICATION, FCConstants.PROPERTY_SMS_DIWALI_OFFER_COMPETETION_ENTERED);
			put(FCConstants.SMS_TEMPLATE_REISSUED_PROMOCODE_HANDLE_ON_RECHARGE_FAILURE, FCConstants.PROPERTY_REISSUED_PROMOCODE_ON_RECHARGE_FAILURE);
			put(FCConstants.SMS_TEMPLATE_GOSF, FCConstants.PREPERTY_SMS_TEMPLATE_GOSF);
			put(FCConstants.SMS_TEMPLATE_OLA_COUPON, FCConstants.PROPERTY_SMS_TEMPLATE_OLA);
			put(FCConstants.SMS_RECHARGE_REMINDER, FCConstants.PROPERTY_SMS_RECHARGE_REMINDER);
		}
	};

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	public void setSmsFactory(SMSFactory smsFactory) {
		this.smsFactory = smsFactory;
	}

	public void sendSMS(SMSBusinessDO smsBusinessDO) throws FCRuntimeException {
		logger.info("[SMS Gateway] Start sending sms for " + smsBusinessDO);
				if (smsBusinessDO == null) {
					logger.error("[SMS Gateway] SMS could not be sent. SMSBusinessDO object is null");
					return;
				}

				if (FCUtil.isEmailBlockedForDomain(smsBusinessDO.getRegisteredUserEmail())) {
					logger.info("User Email: " + smsBusinessDO.getRegisteredUserEmail() + " is currently disabled for SMS.");
					return;
				}

				if(smsBusinessDO.getText() == null) {
					boolean isEnabled = isTemplateEnabled(smsBusinessDO);
					if (!isEnabled) {
						logger.info("SMS template: " + smsBusinessDO.getTemplateName() + " is currently disabled.");
						return;
					}
					generateSmsContent(smsBusinessDO);
				}

				try {

					String smssenderpriority = fcPropertyPlaceholderConfigurer.getStringValueStatic("smssenderpriority");

					if (fcProperties.shouldMockSMS()) {
						logger.info("[MOCK] For sending SMS Text to: " + smsBusinessDO.getReceiverNumber()
								+ ", SMS sender priority was: " + smssenderpriority + " - changing to: smsdummy");
						smssenderpriority = "smsdummy";
					}

					if (smssenderpriority != null && !smssenderpriority.equalsIgnoreCase("")) {
						StringTokenizer smsprioritytokens = new StringTokenizer(smssenderpriority, ",");
						while (smsprioritytokens.hasMoreElements()) {
							String smsSenderName = smsprioritytokens.nextElement().toString().toLowerCase();
							logger.info("[SMS Gateway] Processing SMS Service for " + smsSenderName, null);

							SMSInterface smsInterface = smsFactory.getSmsSender(smsSenderName);
							// logger.info("[SMS Gateway] SMS Template text sixe : "+smsBusinessDO.getText().length()+" and Receiver Number : "+smsBusinessDO.getReceiverNumber(),null);
							boolean isSMSSended = smsInterface.sendSMS(smsBusinessDO.getText(),
									smsBusinessDO.getReceiverNumber());

							if (isSMSSended) {
								logger.info("[SMS Gateway] Send SUCCESS using GATEWAY: " + smsSenderName + " TO: "
										+ smsBusinessDO.getReceiverNumber() + " TEXT: " + smsBusinessDO.getText());
								break;
							} else {
								logger.error("[SMS Gateway] Send FAILURE sms using GATEWAY: " + smsSenderName + " TO: "
										+ smsBusinessDO.getReceiverNumber() + " TEXT: " + smsBusinessDO.getText());
							}
						}
					}
				} catch (Exception ex) {
					logger.error("[SMS Gateway] Error in sending sms" + ex);
				}
	}

	/**
	 * This method requires text, placeholder values and receiverNumber in smsBusinessDo to be
	 * filled.
	 * @param smsBusinessDO
	 **/
	public boolean sendRewardSMSWithPlaceholders(SMSBusinessDO smsBusinessDO) {
		Map<String, String> placeholderMap = smsBusinessDO.getVariableValues();

		String smsText = smsBusinessDO.getText();
		if (StringUtils.isNotBlank(smsText) && !FCUtil.isEmpty(placeholderMap)) {
			logger.info("Replacing placeholders " + placeholderMap + " in the SMS text " + smsText +
					" to be sent to " + smsBusinessDO.getReceiverNumber());
			for (Map.Entry<String, String> entry : placeholderMap.entrySet()) {
				smsText = smsText.replaceAll(entry.getKey(), entry.getValue());
			}
		}

		smsBusinessDO.setText(smsText);
		return sendRewardSMS(smsBusinessDO);
	}

	/**
	 * This method requires only text and receiverNumber in smsBusinessDo to be
	 * filled. It will be used for sending promotional sms using sinfini
	 * 
	 * @param smsBusinessDO
	 * @return
	 * @throws FCRuntimeException
	 */
	public Boolean sendSMSText(SMSBusinessDO smsBusinessDO, String smsSenderName) throws FCRuntimeException {
		logger.info("[SMS Text Gateway] Start sending sms ", null);
		if (smsBusinessDO == null) {
			logger.error("[SMS Text Gateway] SMS could not be sent. SMSBusinessDO object is null");
			return false;
		}
		if (FCUtil.isEmailBlockedForDomain(smsBusinessDO.getRegisteredUserEmail())) {
			logger.info("User Email: " + smsBusinessDO.getRegisteredUserEmail() + " is currently disabled for SMS.");
			return false;
		}
		SMSInterface smsInterface = smsFactory.getSmsSender(smsSenderName);
		boolean isSMSSended = smsInterface.sendSMS(smsBusinessDO.getText(), smsBusinessDO.getReceiverNumber());
		if (isSMSSended) {
			logger.info("[SMS Gateway] Send SUCCESS using GATEWAY: " + smsSenderName + " TO: "
					+ smsBusinessDO.getReceiverNumber() + " TEXT: " + smsBusinessDO.getText());

		} else {
			logger.error("[SMS Gateway] Send FAILURE sms using GATEWAY: " + smsSenderName + " TO: "
					+ smsBusinessDO.getReceiverNumber() + " TEXT: " + smsBusinessDO.getText());
		}
		return isSMSSended;
	}

	private boolean isTemplateEnabled(SMSBusinessDO smsBusinessDO) {
		boolean isTemplateEnabled = false;
		String propertyName = SMS_TEMPLATE_ENABLE_PROPERTY_MAP.get(smsBusinessDO.getTemplateName());
		String smsTemplateEnabled = FCPropertyPlaceholderConfigurer.getStringValueStatic(propertyName);
		if ("true".equalsIgnoreCase(smsTemplateEnabled)) {
			isTemplateEnabled = true;
		}
		return isTemplateEnabled;
	}

	private void generateSmsContent(SMSBusinessDO smsBusinessDO) {
		try 
		{
			String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, getVelocityTemplate(smsBusinessDO),
					smsBusinessDO.getVariableValues());
			smsBusinessDO.setText(text);
			logger.info("[SMS Gateway] Setting sms text ....");
		} catch (VelocityException v) {
			logger.info("Exception while merging velocity template ", v);
		}
	}

	private String getVelocityTemplate(SMSBusinessDO smsBusinessDO) {
		logger.info("[SMS Gateway] VM template name is " + smsBusinessDO.getTemplateName());
		return smsBusinessDO.getTemplateName();

	}

	public String getProfileNumber(String lookupID) {
		String mobileNumber = null;
		try {
			if (lookupID != null && !lookupID.isEmpty()) {
				Cart cartObj = cartService.getCartBylookUp(lookupID);
				if (cartObj != null) {
					Users userObj = userServiceProxy.getUserByUserId(cartObj.getFkUserId());
					if (userObj != null && userObj.getMobileNo() != null && !userObj.getMobileNo().isEmpty()) {
						mobileNumber = userObj.getMobileNo();
					}
				}
			}
		} catch (Exception exception) {
			logger.error("Error while fetching mobile number from cartObj :", exception);
		}
		return mobileNumber;
	}

	public void triggerTransactionSmsNew(UserDetails userDetailsObj, TransactionStatus transactionStatus,
			Map<String, String> variableValues) {
		logger.info(" New sms sending process start for mobile number " + userDetailsObj.getUserMobileNo() + " orderId: " + userDetailsObj.getOrderId());
		try {

			String productType = userDetailsObj.getProductType();
			Boolean isAppOrder = orderService.isAppOrder(userDetailsObj.getOrderId());
			Boolean hasCoupons = cartService.hasCoupons(userDetailsObj.getOrderId());
			String rechargeAmount = String.valueOf(Math.round(Double.valueOf(userDetailsObj.getAmount().trim())));
			String rechargeNumber = transactionStatus.getSubscriberIdentificationNumber();
			String profileNumber = getProfileNumber(userDetailsObj.getLookupId());
			String smsSendingNumber = profileNumber;
			String operatorTxnId = variableValues.get("operatorTxnId");
			Boolean isSelfRecharge = false;
			if(FCUtil.isMobileRecharge(userDetailsObj.getProductType())) {
				if(StringUtils.equals(profileNumber, rechargeNumber)) {
					isSelfRecharge = true; 
				} else {
					smsSendingNumber = rechargeNumber;
				}
			} 

			Map<String, String> templateDataMap = new HashMap<String, String>();
			logger.info("operator Name:"+userDetailsObj.getOperatorName());
			String ussdCode = getOperatorUssdCode(userDetailsObj.getOperatorName());
			String aggregatorTxnId = variableValues.get("aggregatorTxnid");
			if (!Strings.isNullOrEmpty(ussdCode)) {
				templateDataMap.put("ussd", ussdCode);
			}
			logger.info("USSD CODE:"+ussdCode);

			templateDataMap.put("orderno", userDetailsObj.getOrderId());
			templateDataMap.put("refundTime", String.valueOf(FCConstants.REFUND_RESPONSE_TIME_HOURS));
			templateDataMap.put("amount", rechargeAmount);
			templateDataMap.put("mobile", rechargeNumber);

			if (!Strings.isNullOrEmpty(operatorTxnId)) {
				templateDataMap.put("operatorTxnId", operatorTxnId);
				templateDataMap.put("operatorTxnText", "For queries, contact operator with reference no.");

			} else {
				templateDataMap.put("operatorTxnId", "");
				templateDataMap.put("operatorTxnText", "");
			}

			if (!Strings.isNullOrEmpty(aggregatorTxnId)) {
				templateDataMap.put("aggregatorTxt", "For queries, contact biller with reference no.");
				templateDataMap.put("aggregatorTxnId", aggregatorTxnId);
			} else {
				templateDataMap.put("aggregatorTxt", "");
				templateDataMap.put("aggregatorTxnId", "");
			}

			if (FCUtil.isUtilityPaymentType(productType)) {
				templateDataMap.put("productname", ProductName.fromCode(productType).name());
				templateDataMap.put("billercode", userDetailsObj.getOperatorName());
			}
			templateDataMap.put("operatorName", userDetailsObj.getOperatorName());
			if (!Strings.isNullOrEmpty(userDetailsObj.getUserFirstName())) {
				templateDataMap.put("firstName", userDetailsObj.getUserFirstName());
			} else {
				templateDataMap.put("firstName", "Customer");
			}


			String smsText = null;
			String sms2Text = null;
			if ("0".equals(transactionStatus.getTransactionStatus())
					|| "00".equals(transactionStatus.getTransactionStatus())) {
				smsText =  smsTemplateService.getSuccessSMSText(productType, isAppOrder, isSelfRecharge, hasCoupons, templateDataMap);
				if(!isSelfRecharge && FCUtil.isMobileRecharge(userDetailsObj.getProductType())) {
					//In case its not a self recharge. Send sms to profile number also
					sms2Text = smsTemplateService.getSuccessSMSText(productType, isAppOrder, true, hasCoupons, templateDataMap);
					logger.info(" SMS2 for " + userDetailsObj.getUserMobileNo() + " orderId: " + userDetailsObj.getOrderId() +" is " + sms2Text);
				}
			} else if (transactionStatus.getTransactionStatus().equalsIgnoreCase(
					RechargeConstants.IN_UNDER_PROCESS_CODE)) {
				smsText =  smsTemplateService.getPendingSMSText(productType, templateDataMap);
				if (!isSelfRecharge && FCUtil.isMobileRecharge(userDetailsObj.getProductType())) {
					sms2Text = smsText;
					logger.info(" SMS2 for " + userDetailsObj.getUserMobileNo() + " orderId: "
							+ userDetailsObj.getOrderId() + " is " + sms2Text);
				}
			} else {
				String refundTo = rechargeInitiateService.getRefundTo(userDetailsObj.getOrderId());
				if(refundTo.equalsIgnoreCase(PaymentConstants.ONE_CHECK_WALLET_CODE)) {
					templateDataMap.put("refundTo", "FreeCharge wallet");
				} else if(refundTo.equals(PaymentConstants.SOURCE_PG)) {
					templateDataMap.put("refundTo", "original payment instrument");
				} else {
					templateDataMap.put("refundTo", "FreeCharge Credits");
				}
				smsText =  smsTemplateService.getFailureSMSText(productType, templateDataMap);
			}
			if(smsText == null) {
				throw new Exception("SMS template not found");
			}
			logger.info("message:"+sms2Text);
			logger.info(" SMS for " + userDetailsObj.getUserMobileNo() + " orderId: " + userDetailsObj.getOrderId() +" is " + smsText);
			SMSBusinessDO smsBusinessDO = new SMSBusinessDO();
			smsBusinessDO.setReceiverNumber(smsSendingNumber);
			smsBusinessDO.setText(smsText);
			smsBusinessDO.setRegisteredUserEmail(userDetailsObj.getUserEmail());
			sendSMS(smsBusinessDO);
			if(sms2Text != null) {
				//In case its not a self recharge. Send sms to profile number also
				smsBusinessDO.setReceiverNumber(profileNumber);
				smsBusinessDO.setText(sms2Text);
				smsBusinessDO.setRegisteredUserEmail(userDetailsObj.getUserEmail());
				sendSMS(smsBusinessDO);
			}
		} catch (Exception e) {
			logger.error(" Error sending sms to  mobile: " + userDetailsObj.getUserMobileNo() + " orderId: " + userDetailsObj.getOrderId(), e);
		}
	}

	public void sendTransactionSMS(UserDetails userDetailsObj, TransactionStatus transactionStatus,
			Map<String, String> variableValues, String aggregatorName) {
		String productType = userDetailsObj.getProductType();
		Integer smsTemplateId = 0;
		Boolean isSelfRecharge = false;
		String profileNumber = getProfileNumber(userDetailsObj.getLookupId());
		String rechargeNumber = userDetailsObj.getUserMobileNo();
		String rechargeAmount = String.valueOf(Math.round(Double.valueOf(userDetailsObj.getAmount().trim())));
		String operatorTxnId = variableValues.get("operatorTxnId");
		String aggregatorTxnId = variableValues.get("aggregatorTxnid");
		Map<String, String> templateDataMap = new HashMap<String, String>();

		if(FCUtil.isMobileRecharge(userDetailsObj.getProductType()) || FCUtil.isGoogleCreditRechargeType(userDetailsObj.getProductType())) {
			if(StringUtils.equals(profileNumber, rechargeNumber)) {
				isSelfRecharge = true; 
			}
		}
		String ussdCode = getOperatorUssdCode(userDetailsObj.getOperatorName());
		if (!Strings.isNullOrEmpty(ussdCode)) {
			templateDataMap.put("ussd", ussdCode);
		}
		logger.info("USSD CODE:"+ussdCode);
		templateDataMap.put("orderno", userDetailsObj.getOrderId());
		templateDataMap.put("refundTime", String.valueOf(FCConstants.REFUND_RESPONSE_TIME_HOURS));

		Integer amountInteger = 0;
		if(rechargeAmount != null) {
			Double amountDouble = Double.parseDouble(rechargeAmount);
			amountInteger = amountDouble.intValue();
			FeeCharge feeCharge = null;
			try {
				feeCharge = convinenceFeeDetailsDao.getFeeDetails(userDetailsObj.getLookupId());
			} catch (Exception e) {
				logger.error("Exception caught while getting conv fee",e );
			}
			Integer amountTotal = amountInteger;
			if(feeCharge != null) {
				amountTotal = amountTotal + feeCharge.getFeeAmount().intValue();
			}
			templateDataMap.put("amount", String.valueOf(amountTotal));
		}
		templateDataMap.put("mobile", rechargeNumber);
		if (!Strings.isNullOrEmpty(operatorTxnId)) {
			templateDataMap.put("operatorTxnId", operatorTxnId);
			templateDataMap.put("operatorTxnText", "For queries, contact operator with reference no.");
		} else {
			templateDataMap.put("operatorTxnId", "");
			templateDataMap.put("operatorTxnText", "");
		}

		if (!Strings.isNullOrEmpty(aggregatorTxnId)) {
			templateDataMap.put("aggregatorTxt", "For queries, contact biller with reference no.");
			templateDataMap.put("aggregatorTxnId", aggregatorTxnId);
		} else {
			templateDataMap.put("aggregatorTxt", "");
			templateDataMap.put("aggregatorTxnId", "");
		}

		if (FCUtil.isUtilityPaymentType(productType)) {
			templateDataMap.put("productname", ProductName.fromCode(productType).name());
			templateDataMap.put("billercode", userDetailsObj.getOperatorName());
		}
		templateDataMap.put("operatorName", userDetailsObj.getOperatorName());
		if (!Strings.isNullOrEmpty(userDetailsObj.getUserFirstName())) {
			templateDataMap.put("firstName", userDetailsObj.getUserFirstName());
		} else {
			templateDataMap.put("firstName", "Customer");
		}
		
		if(FCUtil.isGoogleCreditRechargeType(productType)){
			templateDataMap.put("googleSerial", variableValues.get("googleSerial"));
			templateDataMap.put("googleCode", variableValues.get("googleCode"));
		}
		if ("0".equals(transactionStatus.getTransactionStatus())
				|| "00".equals(transactionStatus.getTransactionStatus())) {
			if (!isSelfRecharge) {
				if (FCUtil.isPrePaidRecharge(userDetailsObj.getProductType())) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.PREPAID_RECHARGE_SUCCESS_SMS_TEMPLATE_ID);
				} else if (FCUtil.isDataCard(userDetailsObj.getProductType())) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.DATACARD_RECHARGE_SUCCESS_SMS_TEMPLATE_ID);
				} else if (FCUtil.isDTH(userDetailsObj.getProductType())) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.DTH_RECHARGE_SUCCESS_SMS_TEMPLATE_ID);
				} else if (FCUtil.isBillPostpaidType(productType)) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.POSTPAID_BILL_PAY_SUCCESS_SMS_TEMPLATE_ID);
				} else if (FCUtil.isMetroPaymentType(productType)) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.METRO_PAYMENT_SUCCESS_SMS_TEMPLATE_ID);
				} else if (FCUtil.isUtilityPaymentType(productType)) {
					if (aggregatorName != null && aggregatorName.equals(BILLDESK_AGGREGATOR_NAME)) {
						smsTemplateId = fcProperties.getIntProperty(FCProperties.BILLPAY_SUCCESS_SMS_TEMPLATE_ID);
					} else {
						smsTemplateId = fcProperties
								.getIntProperty(FCProperties.NON_BILLDESK_BILLPAY_SUCCESS_SMS_TEMPLATE_ID);
					}
				} else if (FCUtil.isGoogleCreditRechargeType(productType)) {
					smsTemplateId = fcProperties
							.getIntProperty(FCProperties.GOOGLE_CREDIT_RECHARGE_SUCCESS_SMS_TEMPLATE_ID);
				}
			} else if (FCUtil.isPrePaidRecharge(userDetailsObj.getProductType())) {
				smsTemplateId = fcProperties.getIntProperty(FCProperties.PREPAID_SELF_RECHARGE_SUCCESS_SMS_TEMPLATE_ID);
			} else if (FCUtil.isBillPostpaidType(productType)) {
				smsTemplateId = fcProperties
						.getIntProperty(FCProperties.POSTPAID_SELF_BILL_PAY_SUCCESS_SMS_TEMPLATE_ID);
			} else if (FCUtil.isGoogleCreditRechargeType(productType)) {
				smsTemplateId = fcProperties
						.getIntProperty(FCProperties.GOOGLE_CREDIT_RECHARGE_SUCCESS_SMS_TEMPLATE_ID);
			}
		} else if (transactionStatus.getTransactionStatus().equalsIgnoreCase(
				RechargeConstants.IN_UNDER_PROCESS_CODE)) {
			
			if (FCUtil.isLandlineRecharge(userDetailsObj.getProductType())) { 
				smsTemplateId = fcProperties.getIntProperty(FCProperties.LANDLINE_RECHARGE_PENDING_SMS_TEMPLATE_ID);
				sendSMSByNotificationService(smsTemplateId, profileNumber, templateDataMap);
				logger.info(" SMSTemplateId:" + smsTemplateId + " " + " contactNumber:"
						+ profileNumber);
				return;
			}
			
			if(FCUtil.isBroadbandRecharge(userDetailsObj.getProductType())){
				smsTemplateId = fcProperties.getIntProperty(FCProperties.BROADBAND_RECHARGE_PENDING_SMS_TEMPLATE_ID);
				sendSMSByNotificationService(smsTemplateId, profileNumber, templateDataMap);
				logger.info(" SMSTemplateId:" + smsTemplateId + " " + " contactNumber:"
						+ profileNumber);
				return;
			}
			
//			Put it into queue and return.
//			In the consumer check if order status is still UP then send the SMS else do nothing as staus check job will take care of it.
			if (!isSelfRecharge) {
				if (FCUtil.isPrePaidRecharge(userDetailsObj.getProductType())) { 
					smsTemplateId = fcProperties.getIntProperty(FCProperties.PREPAID_RECHARGE_PENDING_SMS_TEMPLATE_ID);
				} else if (FCUtil.isDataCard(userDetailsObj.getProductType())) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.DATACARD_RECHARGE_PENDING_SMS_TEMPLATE_ID);
				} else if (FCUtil.isDTH(userDetailsObj.getProductType())) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.DTH_RECHARGE_PENDING_SMS_TEMPLATE_ID);
				} else if (FCUtil.isBillPostpaidType(productType)) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.POSTPAID_BILLPAY_PENDING_SMS_TEMPLATE_ID);
				} else if (FCUtil.isMetroPaymentType(productType)) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.METRO_PAYMENT_PENDING_SMS_TEMPLATE_ID);
				} else if (FCUtil.isUtilityPaymentType(productType)) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.BILLPAY_PENDING_SMS_TEMPLATE_ID);
				}
			} else if (FCUtil.isPrePaidRecharge(userDetailsObj.getProductType())) {
				smsTemplateId = fcProperties.getIntProperty(FCProperties.PREPAID_SELF_RECHARGE_PENDING_SMS_TEMPLATE_ID);
			} else if (FCUtil.isBillPostpaidType(productType)) {
				smsTemplateId = fcProperties.getIntProperty(FCProperties.POSPAID_BILLPAY_SELF_PENDING_SMS_TEMPLATE_ID);
			}
			delayedUnderProcessSmsService.sendSmsToDelayedSmsQueue(smsTemplateId, profileNumber, rechargeNumber, templateDataMap , isSelfRecharge, userDetailsObj);
			return;
			
		} else {
			String refundTo = rechargeInitiateService.getRefundTo(userDetailsObj.getOrderId());
			String refundSource = null;
			if(refundTo.equalsIgnoreCase(PaymentConstants.ONE_CHECK_WALLET_CODE)) {
				refundSource = "FreeCharge wallet";
			} else if(refundTo.equals(PaymentConstants.SOURCE_PG)) {
				refundSource = "original payment instrument";
			} else {
				refundSource = "FreeCharge Credits";
			}
			templateDataMap.put("refundTo", refundSource);
			if (!isSelfRecharge) {
				if (FCUtil.isPrePaidRecharge(userDetailsObj.getProductType())) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.PREPAID_RECHARGE_FAILURE_SMS_TEMPLATE_ID);
				} else if (FCUtil.isDataCard(userDetailsObj.getProductType())) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.DATACARD_RECHARGE_FAILURE_SMS_TEMPLATE_ID);
				} else if (FCUtil.isDTH(userDetailsObj.getProductType())) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.DTH_RECHARGE_FAILURE_SMS_TEMPLATE_ID);
				} else if (FCUtil.isBillPostpaidType(productType)) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.POSTPAID_BILLPAY_FAILURE_SMS_TEMPLATE_ID);
				} else if (FCUtil.isMetroPaymentType(productType)) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.METRO_PAYMENT_FAILURE_SMS_TEMPLATE_ID);
				} else if (FCUtil.isUtilityPaymentType(productType)) {
					smsTemplateId = fcProperties.getIntProperty(FCProperties.BILLPAY_FAILURE_SMS_TEMPLATE_ID);
				}
			} else if (FCUtil.isPrePaidRecharge(userDetailsObj.getProductType())) {
				smsTemplateId = fcProperties.getIntProperty(FCProperties.PREPAID_SELF_RECHARGE_FAILURE_SMS_TEMPLATE_ID);
			} else if (FCUtil.isBillPostpaidType(productType)) {
				smsTemplateId = fcProperties.getIntProperty(FCProperties.POSTPAID_SELF_BILLPAY_FAILURE_SMS_TEMPLATE_ID);
			}
		}
		logger.info("selfRecharge:"+isSelfRecharge);
		logger.info("ProfileNumber:"+profileNumber+"\tRechargenumber:"+rechargeNumber);
		logger.info("smsservice - producttype:"+userDetailsObj.getProductType()+"isMobileRecharge:"+FCUtil.isMobileRecharge(userDetailsObj.getProductType()));
		//We are sending single SMS only in 3 cases a. Self recharge b. Success case of prepaid recharge and c. Utility Recarge
		if(isSelfRecharge || FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())  || (FCUtil.isMobilePrepaidRecharge(userDetailsObj.getProductType()) &&  
				("0".equals(transactionStatus.getTransactionStatus()) || "00".equals(transactionStatus.getTransactionStatus()))))  {
			sendSMSByNotificationService(smsTemplateId, profileNumber, templateDataMap);
			logger.info(" SMSTemplateId:" + smsTemplateId + " " + " contactNumber:"
					+ profileNumber);
			// For rest of the cases we are sending 2 SMSes	
		}else {
			sendSMSByNotificationService(smsTemplateId, profileNumber, templateDataMap);
			logger.info(" SMSTemplateId:" + smsTemplateId + " contactNumber:" + profileNumber);
			sendSMSByNotificationService(smsTemplateId, rechargeNumber, templateDataMap);
			logger.info(" SMSTemplateId:" + smsTemplateId + " contactNumber:" + rechargeNumber);
		}
	}

	public void triggerTransactionSms(UserDetails userDetailsObj, TransactionStatus transactionStatus,
			Map<String, String> variableValues) {
		logger.info(" sms sanding process start for mobile number " + userDetailsObj.getUserMobileNo());
		SMSBusinessDO smsBusinessDO = new SMSBusinessDO();
		if(FCUtil.isRechargeProductType(userDetailsObj.getProductType() )) {
			smsBusinessDO.setReceiverNumber(userDetailsObj.getUserMobileNo().trim());
			if ("0".equals(transactionStatus.getTransactionStatus())
					|| "00".equals(transactionStatus.getTransactionStatus())) {
				if (orderService.isAppOrder(userDetailsObj.getOrderId())){
					// Do not send success sms for app orders.. as we are currently sending app download ad in sms.
					return;
				}
				smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_RECHARGE_SUCCESSFUL);
			} else if (transactionStatus.getTransactionStatus()
					.equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE)) {
				smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_RECHARGE_UNDER_PROCESS);
			} else {
				smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_RECHARGE_FAILED);
			}
			smsBusinessDO.setVariableValues(variableValues);
		} else if(FCUtil.isBillPostpaidType(userDetailsObj.getProductType()) || FCUtil.isUtilityPaymentType(userDetailsObj.getProductType())) {

			String mobileNumber = getProfileNumber(userDetailsObj.getLookupId());

			if (mobileNumber == null || mobileNumber.isEmpty() || mobileNumber.equals("null")) {
				smsBusinessDO.setReceiverNumber(transactionStatus.getSubscriberIdentificationNumber());
			} else {
				smsBusinessDO.setReceiverNumber(mobileNumber);
			}

			if ("0".equals(transactionStatus.getTransactionStatus())
					|| "00".equals(transactionStatus.getTransactionStatus())) {
				smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_POSTPAID_SUCCESSFUL);
			} else if (transactionStatus.getTransactionStatus().equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE)) {
				smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_RECHARGE_UNDER_PROCESS);
			} else {
				smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_POSTPAID_FAILED);
			}
			smsBusinessDO.setVariableValues(variableValues);
		}
		smsBusinessDO.setRegisteredUserEmail(userDetailsObj.getUserEmail());
		sendSMS(smsBusinessDO);
	}

	public void sendCrossCellCouponSMS(List<Map<String, Object>> couponList, String mobileNumber, String userEmail) {
		List<String> couponTypeList = new ArrayList<String>();
		couponTypeList.add(FCConstants.COUPON_TYPE_C);
		sendCouponSms(couponList, couponTypeList, mobileNumber, userEmail);

	}

	public void sendCouponSms(List<Map<String, Object>> couponList, List<String> couponTypeList, String mobileNumber,
			String userEmail) {
		if (couponList == null || couponList.isEmpty() || couponTypeList == null || couponTypeList.isEmpty()) {
			return;
		}
		String merchantName = "";
		try {
			boolean isMultiMerchantECoupon = CouponInventoryUtil.isMultiMerchantCouponSelected(couponList,
					couponTypeList);

			if (!isMultiMerchantECoupon) {
				for (Map<String, Object> couponMap : couponList) {

					if (couponTypeList.contains(couponMap.get("couponType").toString())) {

						merchantName = couponMap.get("couponName").toString();
						break;
					}
				}
			}

			SMSBusinessDO smsBusinessDO = new SMSBusinessDO();
			Map<String, String> params = new HashMap<String, String>();

			if (isMultiMerchantECoupon) {

				smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_EMULTI_COUPONS);
			} else {
				smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_E_COUPONS);
				params.put("merchantName", merchantName);
			}
			params.put("userEmail", userEmail);
			smsBusinessDO.setReceiverNumber(mobileNumber);
			smsBusinessDO.setVariableValues(params);
			smsBusinessDO.setRegisteredUserEmail(userEmail);
			sendSMS(smsBusinessDO);
		} catch (Exception e) {
			logger.error("Exception while sending SMS for eCoupons: userEmail= " + userEmail + ",mobileNumber= "
					+ mobileNumber + ",merchantName= " + merchantName, e);
		}
	}

	public void sendECouponSMS(List<Map<String, Object>> couponList, String mobileNumber, String userEmail) {
		List<String> couponTypeList = new ArrayList<String>();
		couponTypeList.add(FCConstants.COUPON_TYPE_E);
		couponTypeList.add(FCConstants.COUPON_TYPE_C);
		Users userObj = userServiceProxy.getUserByEmailId(userEmail);
		if (userObj != null && userObj.getMobileNo() != null && !userObj.getMobileNo().isEmpty()) {
			sendCouponSms(couponList, couponTypeList, userObj.getMobileNo(), userEmail);
		} else {
			sendCouponSms(couponList, couponTypeList, mobileNumber, userEmail);
		}

	}

	public void sendMCouponSMS(List<Map<String, Object>> couponList, String mobileNumber, String email) {
		DateFormat df = new SimpleDateFormat("yyyy-MMM-dd");

		if (couponList == null || couponList.isEmpty()) {
			return;
		}

		for (Map<String, Object> couponMap : couponList) {
			if ((Boolean) couponMap.get("isMCoupon")) {
				SMSBusinessDO smsBusinessDO = new SMSBusinessDO();

				Timestamp couponExpiryDate = (Timestamp) couponMap.get("expireDate");

				Map<String, String> params = new HashMap<String, String>();
				params.put("merchantName", couponMap.get("couponName").toString());
				params.put("couponCode", couponMap.get("couponCode").toString());
				params.put("validityDate", df.format(couponExpiryDate));

				smsBusinessDO.setReceiverNumber(mobileNumber);
				smsBusinessDO.setTemplateName(FCConstants.SMS_TEMPLATE_M_COUPONS);
				smsBusinessDO.setVariableValues(params);
				smsBusinessDO.setRegisteredUserEmail(email);
				sendSMS(smsBusinessDO);
			}
		}
	}

	/*
	 * This method used to send Reward SMS
	 */
	public boolean sendRewardSMS(SMSBusinessDO smsBusinessDO) {

		logger.info("[Reward SMS Gateway] Start sending sms for " + smsBusinessDO);
		boolean isSMSSent = false;

		if (fcProperties.shouldMockSMS()) {
			logger.info("[Reward SMS Gateway] SMS sending mocked for " + smsBusinessDO);        
			return isSMSSent;
		}

		if (smsBusinessDO == null) {
			logger.error("[Reward SMS Gateway] SMS could not be sent. SMSBusinessDO object is null");
			return isSMSSent;
		}

		if(FCUtil.isEmpty(smsBusinessDO.getText()) || FCUtil.isEmpty(smsBusinessDO.getReceiverNumber())) {
			logger.error("[Reward SMS Gateway] SMS template is emnpty for " + smsBusinessDO);
			return isSMSSent;
		}

		if (FCUtil.isEmailBlockedForDomain(smsBusinessDO.getRegisteredUserEmail())) {
			logger.info("[Reward SMS Gateway] User Email: " + smsBusinessDO.getRegisteredUserEmail() 
					+ " is currently disabled for SMS. " + smsBusinessDO);
			return isSMSSent;
		}

		try {

			logger.debug("[Reward SMS Gateway] Processing SMS Service using GATEWAY: smsgupshup, TO: " 
					+ smsBusinessDO.getReceiverNumber() + " TEXT: " + smsBusinessDO.getText());

			SMSInterface smsInterface = smsFactory
					.getSmsSender(fcProperties.getProperty(FCProperties.REWARD_SMS_SENDER));       

			isSMSSent = smsInterface.sendSMS(smsBusinessDO.getText(), smsBusinessDO.getReceiverNumber());
			logger.info("[Reward SMS Gateway] isSMSSent : " + isSMSSent + " using GATEWAY: smsgupshup, TO: " 
					+ smsBusinessDO.getReceiverNumber() + " TEXT: " + smsBusinessDO.getText());

		} catch (Exception ex) {
			logger.error("[SMS Gateway] Error in sending sms" + ex);
		}  

		return isSMSSent;
	}

	public String getOperatorUssdCode(String operatorName) {
		String ussdCode ="";
		ussdCode = FCConstants.operatorUSSDMap.get(operatorName);
		return ussdCode;
	}


	public void sendSMSByNotificationService(Integer smsTemplateId, String contactNumber,
			Map<String, String> templateDataMap) {
		List<String> tags = new ArrayList<String>();
		tags.add(contactNumber);
		SendSmsRequest sendSMSRequest = new SendSmsRequest();
		sendSMSRequest.setSmsId(smsTemplateId);
		sendSMSRequest.setContactNumber(contactNumber);
		sendSMSRequest.setTemplateDataMap(templateDataMap);
		sendSMSRequest.setTags(tags);
		try {
			SendMessageResult msgResult = amazonSqsService.sendMessageToQueue(jsonMapper.writeValueAsString(sendSMSRequest));
			if (msgResult != null) {
				logger.info("messageId:" + msgResult.getMessageId());
			}
		} catch (Exception e) {
			logger.error("Exception:", e);
		}
	}   
}

