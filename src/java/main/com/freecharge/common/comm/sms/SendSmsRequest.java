package com.freecharge.common.comm.sms;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class SendSmsRequest implements Serializable{

	private Integer smsId;
	private Map<String, String> templateDataMap;
	private String contactNumber;
	private List<String> tags;

	public Integer getSmsId() {
		return smsId;
	}

	public void setSmsId(Integer smsId) {
		this.smsId = smsId;
	}

	public Map<String, String> getTemplateDataMap() {
		return templateDataMap;
	}

	public void setTemplateDataMap(Map<String, String> templateDataMap) {
		this.templateDataMap = templateDataMap;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

}
