package com.freecharge.common.comm.sms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.freecharge.common.comm.fulfillment.FulfillmentService;
import com.freecharge.common.comm.fulfillment.TransactionStatus;
import com.freecharge.common.comm.fulfillment.UserDetails;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.payment.dos.entity.DelayedUnderProcessSmsDetails;
import com.freecharge.recharge.util.RechargeConstants;
import com.google.gson.Gson;

@Service
public class DelayedUnderProcessSmsService {
	
	@Autowired
    @Qualifier("producerSQSClient")
    AmazonSQSAsync sqsClient;
	
	 @Autowired
	 @Qualifier("producerSQSMumbaiClient")
	 AmazonSQSAsync sqsMumbaiClient;
	
	@Autowired
    private FCProperties fcProperties;
	
	@Autowired
	private FulfillmentService fulfillmentService;
	
	@Value("${fulfillment.aws.underprocess.sms.queue.name}")
	private String queueName;
	
	@Autowired
	private SMSService smsService;

	private static Logger logger = LoggingFactory.getLogger(DelayedUnderProcessSmsService.class);
	
	public void sendSmsToDelayedSmsQueue(Integer smsTemplateId, String profileNumber, String rechargeNumber, Map<String, String> templateDataMap ,
			Boolean isSelfRecharge, UserDetails userDetailsObj) {
		
		try {
			logger.info("Recieved request for delayed SMS");
			SendSmsRequest smsRequest = new SendSmsRequest();
			smsRequest.setContactNumber(profileNumber);
			smsRequest.setSmsId(smsTemplateId);
			List<String> tags = new ArrayList();
			tags.add(profileNumber);
			smsRequest.setTags(tags);
			smsRequest.setTemplateDataMap(templateDataMap);
			DelayedUnderProcessSmsDetails requestObject = new DelayedUnderProcessSmsDetails();
			requestObject.setSendSmsRequest(smsRequest);
			requestObject.setUserDetails(userDetailsObj);
			ObjectMapper mapper = new ObjectMapper();
			SendMessageRequest sendRequest = new SendMessageRequest();
			sendRequest.setQueueUrl(this.getSQSQueueEndPoint(queueName)); 
			sendRequest.setMessageBody(mapper.writeValueAsString(requestObject));
			sqsMumbaiClient.sendMessage(sendRequest);
			logger.info("Send message to queue for number: " + profileNumber);
			if(!isSelfRecharge) { //Here we have to send SMS to rechargeNumber as well
				SendSmsRequest rechargeSmsRequest = new SendSmsRequest();
				rechargeSmsRequest.setContactNumber(rechargeNumber);
				rechargeSmsRequest.setSmsId(smsTemplateId);
				List<String> rechargeSmsTagList = new ArrayList();
				rechargeSmsTagList.add(rechargeNumber);
				rechargeSmsRequest.setTags(rechargeSmsTagList);
				rechargeSmsRequest.setTemplateDataMap(templateDataMap);
				DelayedUnderProcessSmsDetails rechargeSmsRequestObject = new DelayedUnderProcessSmsDetails();
				rechargeSmsRequestObject.setSendSmsRequest(rechargeSmsRequest);
				rechargeSmsRequestObject.setUserDetails(userDetailsObj);
				SendMessageRequest rechargeSmsSendRequest = new SendMessageRequest();
				rechargeSmsSendRequest.setQueueUrl(this.getSQSQueueEndPoint(queueName)); 
				rechargeSmsSendRequest.setMessageBody(mapper.writeValueAsString(rechargeSmsRequestObject));
				sqsMumbaiClient.sendMessage(rechargeSmsSendRequest);
				logger.info("Send 2nd message to queue for number: " + rechargeNumber);
			}
		} catch (Exception e) {
			logger.error("Exception caught while sending delayed Sms request to SQS for templateId: "  + smsTemplateId +" for "
					+ "rechargeNumber " + rechargeNumber + " and profileNumber: " + profileNumber, e);
		}
        
        
		
	}
	
	private String getSQSQueueEndPoint(String queueName) {
        final String queueEndPoint =  fcProperties.getConsumerSQSQueueMumbaiEndPoint() + 
                "/" + getStackID() + "_" + queueName;
        return queueEndPoint;
    }
	
	public void sendUpSms(DelayedUnderProcessSmsDetails delayedSmsObject) {
		ObjectMapper jsonMapper = new ObjectMapper();
		logger.info("SQS message recived, will check the transaction status now for user mail: " + delayedSmsObject.getUserDetails().getUserEmail());
		try {
			TransactionStatus transactionStatus = fulfillmentService.getTransactionStatus(delayedSmsObject.getUserDetails(), delayedSmsObject.getUserDetails().getOrderId() );
			logger.info("Transaction status : " + transactionStatus.getTransactionStatus());
			if(transactionStatus.getTransactionStatus().equalsIgnoreCase(RechargeConstants.IN_UNDER_PROCESS_CODE)) { //It is still under process after delayed Q. Now send the SMS
				smsService.sendSMSByNotificationService(delayedSmsObject.getSendSmsRequest().getSmsId(), delayedSmsObject.getSendSmsRequest().getContactNumber(), delayedSmsObject.getSendSmsRequest().getTemplateDataMap());
			}else {
				//Do nothing as state is terminal and status check job will take care of sending sms in case of terminal state
			}
			
		} catch (Exception e) {
			logger.error("Exception caught while sending SMS for mobileNumber: " + delayedSmsObject.getSendSmsRequest().getContactNumber() + " and templateId: " + delayedSmsObject.getSendSmsRequest().getSmsId());
		}
	}
	
	public String getStackID() {
		/*
		 * For prod return hard-coded value.
		 */
		if (fcProperties.isProdMode() && !fcProperties.isPreProdMode()) {
			return "prod";
		}
		
		if(fcProperties.isQaMode()) {
			return "qamobile4";
		}

		return "qa";
	}
}
