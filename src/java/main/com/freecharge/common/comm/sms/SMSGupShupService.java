package com.freecharge.common.comm.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;

/**
 * Created by IntelliJ IDEA.
 * User: Rananjay
 * Date: May 31, 2012
 * Time: 3:04:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class SMSGupShupService implements SMSInterface {

    private static Logger logger = LoggingFactory.getLogger(SMSGupShupService.class);

    private String gupshupusername;
    private String gupshuppassword;
    private String gupshupsmsurl;

    @Autowired
    private FCProperties fcProperties;

    public void setGupshupusername(String gupshupusername) {
        this.gupshupusername = gupshupusername;
    }

    public void setGupshuppassword(String gupshuppassword) {
        this.gupshuppassword = gupshuppassword;
    }

    public void setGupshupsmsurl(String gupshupsmsurl) {
        this.gupshupsmsurl = gupshupsmsurl;
    }


    public boolean sendSMS(String Message, String MobileNo) {
        boolean isSMSSend = false;

        try {
            //Date mydate = new Date(System.currentTimeMillis());
            logger.info("[SMS Gateway] gupshupusername : " + gupshupusername + " ,gupshuppassword : XXXXXXXX and gupshupsmsurl : " + gupshupsmsurl);
            logger.info("message : " + Message + " ,MobileNo : " + MobileNo);
            String data = "";
            data += "method=sendMessage";
            data += "&userid=" + gupshupusername;
            data += "&password=" + URLEncoder.encode(gupshuppassword, "UTF-8");
            data += "&msg=" + URLEncoder.encode(Message,"UTF-8");
            //data += "&send_to=" + URLEncoder.encode("9xxxxxxxxx", "UTF-8");
            data += "&send_to="+URLEncoder.encode(MobileNo,"UTF-8");
            data += "&v=1.1";
            data += "&msg_type=TEXT";
            data += "&auth_scheme=PLAIN";

            logger.info("[SMS Gateway] Data with url for sms gateway gupshup : " + gupshupsmsurl+"?"+data, null);

            URL url = new URL(gupshupsmsurl+"?"+data);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();

            conn.setRequestMethod("GET");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setConnectTimeout(fcProperties.getGupshupConnectionTimeout());
            conn.setReadTimeout(fcProperties.getGupshupReadTimeout());
            conn.connect();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            StringBuffer buffer = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                buffer.append(line).append("\n");
            }
            logger.info("[SMS Gateway] Output from sms gateway gupshup : " + buffer.toString(), null);

            rd.close();
            conn.disconnect();
            String responseContent[] = buffer.toString().split("\\|");
            if(responseContent.length>1){
               String responseStatus = responseContent[0].trim();
                logger.info("[SMS Gateway]responseStatus from sms gateway gupshup : " + responseStatus, null);
                if(responseStatus.equalsIgnoreCase("success")){
                        isSMSSend = true;
                }
            }            
        }
        catch (Exception e) {
            logger.error("[SMS Gateway] Error in sending sms for gupshup (sendSMS) : ", e);
        }

        return isSMSSend;
    }
}
