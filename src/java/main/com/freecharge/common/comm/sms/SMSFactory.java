package com.freecharge.common.comm.sms;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Created by IntelliJ IDEA.
 * User: Rananjay
 * Date: Jun 1, 2012
 * Time: 2:14:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class SMSFactory implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    public SMSInterface getSmsSender(String smsSenderName) {

        if (this.getApplicationContext().containsBean(smsSenderName.toLowerCase()))
            return (SMSInterface) this.getApplicationContext().getBean(smsSenderName.toLowerCase());
        else
            return null;
    }

    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        this.applicationContext = ctx;
    }

    public ApplicationContext getApplicationContext() {
        return this.applicationContext;
    }

}
