package com.freecharge.common.comm.sms;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.web.service.CommonHttpService;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 8/30/13
 * Time: 2:34 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("sinfinismsservice")
public class SinfiniSmsService implements SMSInterface {


    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private CommonHttpService commonHttpService;

    @Override
    public boolean sendSMS(String message, String mobile) {
        String apiKey = fcProperties.getProperty("sinfini.sms.key");
        Map<String, String> params = new HashMap<>();
        params.put("workingkey", apiKey);
        params.put("sender", "FCHRGE");
        params.put("to", mobile);
        params.put("message", message);

        logger.info("calling_sinfini_sms_url: "+sinfiniUrl);
        try {
            CommonHttpService.HttpResponse response = this.commonHttpService.fireGetRequest(sinfiniUrl, params);
            String responseText = response.getResponseBody();
            logger.info("sinfini_sms_response: "+responseText+" mobile: "+mobile+" message: "+message);
            if (responseText.contains("GID")){
                return true;
            }
            return false;
        }catch (Exception e){
            logger.error("Error occurred while sending sms. Mobile: "+mobile+" error: "+e.getMessage(), e);
            return false;
        }
    }
    private static Logger logger = LoggingFactory.getLogger(SinfiniSmsService.class);
    public static final String sinfiniUrl = "http://alerts.sinfini.com/api/web2sms.php";
}
