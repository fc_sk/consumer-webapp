package com.freecharge.common.comm.sms;

public class ACLProps {

    private boolean enabled;
    private String smsurl;
    private String mutlismsurl;
    private String userid;
    private String pass;
    private String appid;
    private String subappid;
    private String from;
    
    public boolean isEnabled() {
        return enabled;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
    public String getSmsurl() {
        return smsurl;
    }
    public void setSmsurl(String smsurl) {
        this.smsurl = smsurl;
    }
    
    public String getMutlismsurl() {
        return mutlismsurl;
    }
    public void setMutlismsurl(String mutlismsurl) {
        this.mutlismsurl = mutlismsurl;
    }
    public String getUserid() {
        return userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }
    public String getPass() {
        return pass;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }
    public String getAppid() {
        return appid;
    }
    public void setAppid(String appid) {
        this.appid = appid;
    }
    public String getSubappid() {
        return subappid;
    }
    public void setSubappid(String subappid) {
        this.subappid = subappid;
    }
    public String getFrom() {
        return from;
    }
    public void setFrom(String from) {
        this.from = from;
    }
    
    
}
