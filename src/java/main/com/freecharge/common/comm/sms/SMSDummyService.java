package com.freecharge.common.comm.sms;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;

public class SMSDummyService implements SMSInterface {

    private static Logger logger = LoggingFactory.getLogger(SMSDummyService.class);

    public boolean sendSMS(String Message, String MobileNo) {
	logger.info("[MOCK] Sent SMS to: " + MobileNo + ", message: " + Message);
	return true;
    }
}
