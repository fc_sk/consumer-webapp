package com.freecharge.common.comm.notification.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.freecharge.common.comm.notification.GCMNotificationService;
import com.freecharge.common.framework.annotations.NoLogin;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.csrf.annotations.Csrf;

@Controller
public class GCMNotificationController {

    @Autowired
    private GCMNotificationService  gcmNotificationService;

    @Autowired
    private FCProperties fcProperties;

    private final Logger                logger                     = LoggingFactory.getLogger(getClass());

    @RequestMapping(value="/rest/notification/send", method=RequestMethod.POST, produces = "application/json")
    @NoLogin
    @Csrf(exclude = true)
    public String sendNotification(HttpServletRequest request, Model model) {

        String apiSecret = request.getHeader("secret");
        String secret =  fcProperties.getProperty(FCConstants.REFERRAL_API_SECRET_PROPERTY);

        if (FCStringUtils.isBlank(apiSecret) || !apiSecret.equals(secret)) {
            model.addAttribute("status", "failure");
            model.addAttribute("message", "Authentication failure");
            return "jsonView";
        }

        String userMail = request.getParameter(GCMNotificationService.USER_EMAIL);
        String shopNo = request.getParameter(GCMNotificationService.SHOP_NO);
        String locationId = request.getParameter(GCMNotificationService.LOCATION_ID);
        String messageTitle = request.getParameter(GCMNotificationService.MESSAGE_TITLE);
        String messageUrl = request.getParameter(GCMNotificationService.MESSAGE_URL);
        String messageDesc = request.getParameter(GCMNotificationService.MESSAGE_DESC);
        String html = request.getParameter(GCMNotificationService.HTML);
        String messageAction = request.getParameter(GCMNotificationService.MESSAGE_ACTION);
        String lmd = request.getParameter(GCMNotificationService.LMD);
        String lma = request.getParameter(GCMNotificationService.LMA);
        String rmd = request.getParameter(GCMNotificationService.RMD);
        String rma = request.getParameter(GCMNotificationService.RMA);
        String eventName = request.getParameter(GCMNotificationService.EVENT_NAME);
        String fcChannel = request.getParameter(FCConstants.FC_CHANNEL);

        gcmNotificationService.sendGCMNotification(userMail, shopNo, locationId, messageTitle,
                messageUrl, messageDesc, html, messageAction, lmd, lma, rmd, rma, eventName, fcChannel);
        logger.info("Notification initiated for email : " + userMail);
        return "jsonView";
    }
}
