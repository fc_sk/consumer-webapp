package com.freecharge.common.comm.notification;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.exception.NetworkConnectTimeoutException;
import com.freecharge.recharge.services.HttpMethodExecutorTask;

@Service("gcmNotificationService")
public class GCMNotificationService{

    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    protected MetricsClient metricsClient;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private KestrelWrapper kestrelWrapper;

    private ExecutorService notificationExecutor = Executors.newCachedThreadPool();
    protected final List<Integer> retriableHttpCodes = Arrays.asList(404, 500, 503);

    public static final String USER_EMAIL = "usermail";
    public static final String SHOP_NO = "shopno";
    public static final String LOCATION_ID = "locationid";
    public static final String MESSAGE_TITLE = "messagetitle";
    public static final String MESSAGE_URL = "messageurl";
    public static final String MESSAGE_DESC = "messagedesc";
    public static final String HTML = "html";
    public static final String MESSAGE_ACTION = "messageaction";
    public static final String LMD = "lmd";
    public static final String LMA = "lma";
    public static final String RMD = "rmd";
    public static final String RMA = "rma";
    public static final String URL = "url";
    public static final String EVENT_NAME = "eventName";
    public static final String MESSAGE_TYPE = "messagetype";

    public boolean sendNotification(Map<String, String> postData) {

        long startTime = System.currentTimeMillis();
        boolean isNotificationSend = false;
        String url = postData.get(GCMNotificationService.URL);

        boolean shouldTimeOut = isGCMNotificationPropertyEnabled(AppConfigService.GCM_NOTIFICATION_TIME_OUT_ENABLED);
        int timeOutValue = getGCMNotificationProperty(AppConfigService.GCM_NOTIFICATION_TIME_OUT_VALUE);
        int connTimeout = getGCMNotificationProperty(AppConfigService.GCM_NOTIFICATION_CONNECTION_TIME_OUT_VALUE);
        int soTimeout = getGCMNotificationProperty(AppConfigService.GCM_NOTIFICATION_SO_TIME_OUT_VALUE);

        logger.info("Notification processing started [" + url + "] for postData : " + postData + " with "
                + "shouldTimeOut : " + shouldTimeOut + ", timeOutValue : " + timeOutValue
                + ", connTimeout : " + connTimeout + ", soTimeout : " + soTimeout);

        int postStatus = 0;
        String notificationResponse = null;

        final HttpConnectionManagerParams gcmNotificationConnectionConfig = new HttpConnectionManagerParams();
        gcmNotificationConnectionConfig.setConnectionTimeout(connTimeout);
        gcmNotificationConnectionConfig.setSoTimeout(soTimeout);

        final SimpleHttpConnectionManager gcmNotificationConnectionManager = new SimpleHttpConnectionManager(true);
        gcmNotificationConnectionManager.setParams(gcmNotificationConnectionConfig);

        HttpClient client = new HttpClient(gcmNotificationConnectionManager);
        PostMethod postMethod = new PostMethod(url);

        try {

            postMethod.setRequestBody(FCStringUtils.mapToJson(postData));
            postMethod.setRequestHeader("Content-Type", "application/json");

            postStatus = executeWithTimeout(client, postMethod, shouldTimeOut, timeOutValue);
            notificationResponse = postMethod.getResponseBodyAsString();

            if(HttpStatus.SC_OK == postStatus) {

                isNotificationSend = true;
                logger.info("Notification response [" + url + "] is HTTP Code [" + postStatus + "] "
                        + "Post Response [" + notificationResponse + "] for postData : " + postData);

                metricsClient.recordEvent("Notifications." + postData.get(FCConstants.FC_CHANNEL), FCConstants.SUCCESS, postData.get(EVENT_NAME));

            } else {
                logger.error(getErrorString(url, postStatus, notificationResponse, postData));
                metricsClient.recordEvent("Notifications." + postData.get(FCConstants.FC_CHANNEL), FCConstants.FAIL, postData.get(EVENT_NAME));
            }

        } catch (IOException e) {
            logger.error(getErrorString(url, postStatus, notificationResponse, postData), e);
            metricsClient.recordEvent("Notifications." + postData.get(FCConstants.FC_CHANNEL), FCConstants.FAIL, postData.get(EVENT_NAME));
            throw new RuntimeException(e);

        } catch (Exception e) {
            logger.error(getErrorString(url, postStatus, notificationResponse, postData), e);
            metricsClient.recordEvent("Notifications." + postData.get(FCConstants.FC_CHANNEL), FCConstants.FAIL, postData.get(EVENT_NAME));

        } finally {
            postMethod.releaseConnection();
            long endTime = System.currentTimeMillis();
            metricsClient.recordLatency("GCMNOtification", "Fulfillment", endTime - startTime);
        }

        return isNotificationSend;
    }

    private String getErrorString(String url, int postStatus, String notificationResponse, Map<String, String> postData){
        return "Notification Sending Failure [" + url + "] HTTP Code [" + postStatus + "]. "
                + "Post Response [" + notificationResponse + "] for postData : " + postData;
    }

    private int getGCMNotificationProperty(String property){
        JSONObject notificationConfig = appConfigService.getCustomProp(AppConfigService.GCM_NOTIFICATION_CONFIG);
        int propteryValue = 10000;

        if(notificationConfig.containsKey(property)) {
            propteryValue = Integer.parseInt(String.valueOf(notificationConfig.get(property)));
        }

        return propteryValue;
    }

    private boolean isGCMNotificationPropertyEnabled(String property){
        JSONObject notificationConfig = appConfigService.getCustomProp(AppConfigService.GCM_NOTIFICATION_CONFIG);

        if(notificationConfig.containsKey(property)) {
            Boolean isEnabled = Boolean.parseBoolean(String.valueOf(notificationConfig.get(property)));

            if(isEnabled) {
                return true;
            }
        }

        return false;
    }

    public void sendGCMNotification(String userMail, String shopNo, String locationId,
            String messageTitle, String messageUrl, String messageDesc, String html, String messageAction, String lmd,
            String lma, String rmd, String rma, String eventName, String fcChannel){

        Map<String, String> postData = new HashMap<String, String>();
        postData.put(GCMNotificationService.USER_EMAIL, userMail);
        postData.put(GCMNotificationService.SHOP_NO, shopNo);
        postData.put(GCMNotificationService.LOCATION_ID, locationId);
        postData.put(GCMNotificationService.MESSAGE_TITLE, messageTitle);
        postData.put(GCMNotificationService.MESSAGE_URL, messageUrl);
        postData.put(GCMNotificationService.MESSAGE_DESC, messageDesc);
        postData.put(GCMNotificationService.HTML, html);
        postData.put(GCMNotificationService.MESSAGE_ACTION, messageAction);
        postData.put(GCMNotificationService.LMD, lmd);
        postData.put(GCMNotificationService.LMA, lma);
        postData.put(GCMNotificationService.RMD, rmd);
        postData.put(GCMNotificationService.RMA, rma);
        postData.put(GCMNotificationService.URL, fcProperties.getProperty(FCConstants.POST_CUSTOM_GCMMC_URL));
        postData.put(GCMNotificationService.EVENT_NAME, eventName);
        postData.put(FCConstants.FC_CHANNEL, fcChannel);
        sendNotificationWithAsyncConfig(postData);
        return;
    }

    public void sendGCMNotification(String userMail, String shopNo, String locationId,
            String messageTitle, String messageUrl, String messageDesc, String html, String messageAction, String lmd,
            String lma, String rmd, String rma, String eventName, String fcChannel, String messageType){

        Map<String, String> postData = new HashMap<String, String>();
        postData.put(GCMNotificationService.USER_EMAIL, userMail);
        postData.put(GCMNotificationService.SHOP_NO, shopNo);
        postData.put(GCMNotificationService.LOCATION_ID, locationId);
        postData.put(GCMNotificationService.MESSAGE_TITLE, messageTitle);
        postData.put(GCMNotificationService.MESSAGE_URL, messageUrl);
        postData.put(GCMNotificationService.MESSAGE_DESC, messageDesc);
        postData.put(GCMNotificationService.HTML, html);
        postData.put(GCMNotificationService.MESSAGE_ACTION, messageAction);
        postData.put(GCMNotificationService.LMD, lmd);
        postData.put(GCMNotificationService.LMA, lma);
        postData.put(GCMNotificationService.RMD, rmd);
        postData.put(GCMNotificationService.RMA, rma);
        postData.put(GCMNotificationService.URL, fcProperties.getProperty(FCConstants.POST_CUSTOM_GCMMC_URL));
        postData.put(GCMNotificationService.EVENT_NAME, eventName);
        postData.put(GCMNotificationService.MESSAGE_TYPE, messageType);
        postData.put(FCConstants.FC_CHANNEL, fcChannel);
        sendNotificationWithAsyncConfig(postData);
        return;
    }

    private int executeWithTimeout(final HttpClient client, final HttpMethod method, final boolean shouldTimeOut, final Integer timeOutValue) throws HttpException, IOException {

        long startTime = System.currentTimeMillis();

        try {
            if (shouldTimeOut) {

                logger.info("Notification timeout is enabled for " + method.getURI());

                Future<Integer> notificationTask = notificationExecutor.submit(new HttpMethodExecutorTask(client, method));

                try {

                    final Integer statusCode = notificationTask.get(timeOutValue, TimeUnit.MILLISECONDS);

                    if (statusCode != HttpStatus.SC_OK) {
                        logger.error("Received HTTP status code " + statusCode);
                        if (retriableHttpCodes.contains(statusCode)) {
                            throw new NetworkConnectTimeoutException();
                        }
                    }

                    return statusCode;

                } catch (TimeoutException te) {
                    logger.warn("TimeoutException while sending notification for " + method.getURI()
                            + " [ " + method.getResponseBodyAsString() + " ]", te);
                    throw new ConnectTimeoutException();

                } catch (InterruptedException ie) {
                    logger.error("InterruptedException while sending notification for " + method.getURI()
                            + " [ " + method.getResponseBodyAsString() + " ]", ie);
                    throw new ConnectTimeoutException();

                } catch (ExecutionException ee) {
                    logger.error("ExecutionException while sending notification for " + method.getURI()
                            + " [ " + method.getResponseBodyAsString() + " ]", ee);

                    final Throwable cause = ee.getCause();
                    if (cause != null) {
                        if (isConnectionFailure(cause)) {
                            throw new NetworkConnectTimeoutException();
                        }
                    }
                    throw new ConnectTimeoutException();

                } finally {
                    notificationTask.cancel(true);
                }
            } else {
                logger.info("Notification timeout is disabled for " + method.getURI());
                return client.executeMethod(method);
            }

        } finally {
            long endTime = System.currentTimeMillis();
            metricsClient.recordLatency("GCMNOtification.Execution", "Latency", endTime - startTime);
        }

    }

    private boolean isConnectionFailure(Throwable e) throws NetworkConnectTimeoutException {
        return (e instanceof SocketException || e instanceof ConnectException || e instanceof SocketTimeoutException);
    }

    public boolean isAsyncNotificationEnabled() {
        return isGCMNotificationPropertyEnabled(AppConfigService.ASYNC_GCM_NOTIFICATION) && 
                (fcProperties.isProdMode() || fcProperties.isQaMode());
    }

    public void sendNotificationWithAsyncConfig(Map<String, String> postData) {

        if(!isGCMNotificationPropertyEnabled(AppConfigService.GCM_NOTIFICATION_ENABLED)) {
            logger.info("GCM notification [Disabled] not sent for email : " + postData.get(GCMNotificationService.USER_EMAIL));
            return;
        }

        if (isAsyncNotificationEnabled()) {

            try {
                kestrelWrapper.enqueueNotification(postData);
                logger.info("Asynchronous notification enqueued for email : " + postData.get(GCMNotificationService.USER_EMAIL));
            } catch(RuntimeException e) {
                logger.error("Error queueing notification for email : " +  postData.get(GCMNotificationService.USER_EMAIL)
                        + ". Going ahead with synchronous notification");
                sendNotification(postData);
            }

        } else {
            sendNotification(postData);
        }
    }

}