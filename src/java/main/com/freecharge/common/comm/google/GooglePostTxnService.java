package com.freecharge.common.comm.google;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.postTxn.google.dto.GoogleInstallRefererRequest;
import com.freecharge.postTxn.google.dto.GooglePostTxnDTO;
import com.freecharge.postTxn.google.dto.GoogleSurface;
import com.freecharge.postTxn.google.response.GoogleInstallRefererResponse;
import com.freecharge.postTxn.google.response.GoogleReError;
import com.freecharge.postTxn.google.response.GoogleReResponse;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class GooglePostTxnService {

    private Logger logger = LoggingFactory.getLogger(GooglePostTxnService.class);

    @Autowired
    @Qualifier("googlePostTxnClient")
    private HttpClient googlePostTxnClient;

    @Value("${google.post.txn.api.key}")
    private String googleApiKey;

    @Value("${google.post.txn.api.url}")
    private String googlePostTxnURL;

    @Autowired
    private GoogleOAuthService oAuthService;


    public void post(String orderId, GooglePostTxnDTO googlePostTxnDTO, String refID) throws Exception {

        String postURL = googlePostTxnURL + refID;
        postToGoogle(googlePostTxnDTO, postURL);

    }


    public GoogleInstallRefererResponse postInstallReference(GoogleInstallRefererRequest googleInstallRefererRequest, String channelId) {


        GoogleInstallRefererResponse googleInstallRefererResponse = new GoogleInstallRefererResponse();


        logger.info("postInstallReference  method : request :" + googleInstallRefererRequest);

        try {
            GooglePostTxnDTO googlePostTxnDTO = new GooglePostTxnDTO();

            googlePostTxnDTO.setSurface(GoogleSurface.getSurfaceByChannel(channelId));

            googlePostTxnDTO.setStatus(googleInstallRefererRequest.getStatus().name());
            googlePostTxnDTO.setCreateTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(new Date()));

            String postURL = new StringBuilder().append(googlePostTxnURL).append(googleInstallRefererRequest.getReferenceId()).toString();

            logger.info("postInstallReference  method : to postToGoogle request :" + googlePostTxnDTO);

            int responseCode = postToGoogle(googlePostTxnDTO, postURL);

            logger.info("returned from postToGoogle ,responseCode=>" + responseCode);

            if (responseCode == 200) {
                GoogleReResponse googleReResponse = new GoogleReResponse();
                googleReResponse.setMessage("Install referred successfully");
                googleInstallRefererResponse.setData(googleReResponse);

            } else {
                GoogleReError googleReError = new GoogleReError("GGLRE-110", "Something went wrong with Google API's");
                googleInstallRefererResponse.setError(googleReError);
            }
        } catch (IOException e) {
            logger.error("Exception occurred while posting data to google : ", e);
            GoogleReError googleReError = new GoogleReError("GGLRE-111", "Something went wrong with Google API's");
            googleInstallRefererResponse.setError(googleReError);
        }

        return googleInstallRefererResponse;
    }


    private int postToGoogle(GooglePostTxnDTO googlePostTxnDTO, String finalPostUrl) throws IOException {

        logger.info("googlePostTxnClient : " + googlePostTxnClient + ", googleApiKey : " + googleApiKey
                + ", googlePostTxnURL : " + googlePostTxnURL);

        logger.info("postURL  ###  : " + finalPostUrl);


        ObjectMapper mp = new ObjectMapper();
        String postData = mp.writeValueAsString(googlePostTxnDTO);

        logger.info("postData   ###  : " + postData);


        HttpClient client = new HttpClient();
        HttpConnectionManagerParams connectionParams = new HttpConnectionManagerParams();
        connectionParams.setConnectionTimeout(10000);
        connectionParams.setSoTimeout(10000);
        client.getHttpConnectionManager().setParams(connectionParams);
        PutMethod postMethod = new PutMethod(finalPostUrl);
        String authHeader = "Bearer " + new String(oAuthService.get());
        postMethod.setRequestHeader("Authorization", authHeader);
        StringRequestEntity requestEntity = new StringRequestEntity(postData, "application/json", "UTF-8");
        postMethod.setRequestEntity(requestEntity);
        int responseCode = client.executeMethod(postMethod);
        String response = postMethod.getResponseBodyAsString();

        logger.info("response : ###  : " + response);

        return responseCode;

    }
}
