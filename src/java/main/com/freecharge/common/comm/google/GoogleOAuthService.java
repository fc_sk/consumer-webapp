package com.freecharge.common.comm.google;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.interfaces.RSAPrivateKey;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;

//TODO : to be moved to KMS
@Service
public class GoogleOAuthService {
    private Logger logger = LoggingFactory.getLogger(getClass());

    public String get() throws FileNotFoundException, IOException {
        try {
        	GoogleCredential credential = null;
			try {
				credential = GoogleCredential
						.fromStream(this.getClass().getClassLoader().getResourceAsStream("google-credentials.json"));
			} catch (IOException ex) {
				logger.error("IOException : " + ex);
				logger.info("retrying...");
				String userPath = System.getProperty("user.dir") + "/conf/google-credentials.json";
				credential = GoogleCredential.fromStream(new FileInputStream(userPath));
			}
            logger.info("credential : "+credential);
            
            RSAPrivateKey privateKey = (RSAPrivateKey) credential.getServiceAccountPrivateKey();
            String privateKeyId = credential.getServiceAccountPrivateKeyId();

            long now = System.currentTimeMillis();

            Algorithm algorithm = Algorithm.RSA256(null, privateKey);
            String signedJwt = JWT.create().withKeyId(privateKeyId)
                    .withIssuer("google-recharges@freecharge-recharges.iam.gserviceaccount.com")
                    .withSubject("google-recharges@freecharge-recharges.iam.gserviceaccount.com")
                    .withAudience(
                            "https://mobilerecharge.googleapis.com/google.ads.nbu.mobilerecharge.v1.ReferralTransactions")
                    .withIssuedAt(new Date(now)).withExpiresAt(new Date(now + 3600 * 1000L)).sign(algorithm);
            return signedJwt;
        } catch (Exception ex) {
            logger.error("Exception : " + ex);
            throw new RuntimeException(ex);
        }

    }
}
