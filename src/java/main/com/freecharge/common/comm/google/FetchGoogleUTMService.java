package com.freecharge.common.comm.google;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.util.RechargeConstants;
import com.google.common.collect.ImmutableMap;

@Service
public class FetchGoogleUTMService {
    private Logger logger = LoggingFactory.getLogger(getClass().getName());

    @Autowired
    private AmazonDynamoDBAsync dynamoClientMumKP;
	
	
	/*@Value("${dynamodb.region}")
	private String dynamoDbRegion;*/

	/*@PostConstruct
	void init() {
		logger.info("Calling Init For FetchGoogleUtmService");
		dynamoDBClient = AmazonDynamoDBAsyncClientBuilder.standard().withRegion(dynamoDbRegion).build();
	}*/

    public Map<String, String> fetchOrderAttributes(FetchUTMKey key, String value) {
        logger.info("Finding UTM for key : " + key + ", value : " + value);
        GetItemRequest getItemRequest = getUTMFetchRequest(key, value);
        logger.info("getItemRequest  : " + getItemRequest);

        Map<String, String> result = new HashMap<String, String>();
        try {

            GetItemResult getItemResult = dynamoClientMumKP.getItem(getItemRequest);
            logger.info("getItemResult  : " + getItemResult);
            if (getItemResult != null && getItemResult.getItem() != null) {
                Map<String, AttributeValue> mappingItem = getItemResult.getItem();
                logger.info("mappingItem ###  : " + mappingItem);
                if (mappingItem.get(RechargeConstants.UTM_REF_KEY) != null) {
                    result.put(RechargeConstants.UTM_REF_KEY, mappingItem.get(RechargeConstants.UTM_REF_KEY).getS());
                }
                if (mappingItem.get(RechargeConstants.UTM_SOURCE_TYPE_KEY) != null) {
                    result.put(RechargeConstants.UTM_SOURCE_TYPE_KEY, mappingItem.get(RechargeConstants.UTM_SOURCE_TYPE_KEY).getS());
                }
                if (mappingItem.get(RechargeConstants.UTM_ORDER_KEY) != null) {
                    result.put(RechargeConstants.UTM_ORDER_KEY, mappingItem.get(RechargeConstants.UTM_ORDER_KEY).getS());
                }
                if (mappingItem.get(RechargeConstants.UTM_SOURCE_CHANNEL_ID) != null) {
                    result.put(RechargeConstants.UTM_SOURCE_CHANNEL_ID, mappingItem.get(RechargeConstants.UTM_SOURCE_CHANNEL_ID).getS());
                }
            }

            logger.info("Fetched data from FetchGoogleUTMService " + result);
        } catch (AmazonServiceException e) {
            logger.error("AmazonServiceException while fetching FetchGoogleUTMService Data", e);
        } catch (AmazonClientException e) {
            logger.error("AmazonClientException while fetching FetchGoogleUTMService Data", e);
        }
        return result;

    }

    private GetItemRequest getUTMFetchRequest(FetchUTMKey key, String value) {
        GetItemRequest getItemRequest = null;
        switch (key) {
            case ORDER_ID: {
                getItemRequest = new GetItemRequest(RechargeConstants.UTM_SOURCE_TABLE,
                        ImmutableMap.of(RechargeConstants.UTM_ORDER_KEY, new AttributeValue(value)));
                break;
            }
            case REF_ID: {
                getItemRequest = new GetItemRequest(RechargeConstants.UTM_SOURCE_TABLE,
                        ImmutableMap.of(RechargeConstants.UTM_REF_KEY, new AttributeValue(value)));
                break;
            }
            case UTM_SOURCE: {
                getItemRequest = new GetItemRequest(RechargeConstants.UTM_SOURCE_TABLE,
                        ImmutableMap.of(RechargeConstants.UTM_SOURCE_TYPE_KEY, new AttributeValue(value)));
                break;
            }
            default: {
                logger.info("Default is going with orderId");
                getItemRequest = new GetItemRequest(RechargeConstants.UTM_SOURCE_TABLE,
                        ImmutableMap.of(RechargeConstants.UTM_ORDER_KEY, new AttributeValue(value)));
            }
        }
        return getItemRequest;
    }
}
