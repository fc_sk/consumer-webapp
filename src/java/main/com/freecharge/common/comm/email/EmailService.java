package com.freecharge.common.comm.email;

import com.freecharge.app.domain.dao.HomeBusinessDao;
import com.freecharge.app.domain.dao.jdbc.OrderIdWriteDAO;
import com.freecharge.app.domain.entity.jdbc.FreefundClass;
import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.app.service.CouponService;
import com.freecharge.app.service.VoucherService;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.CouponInventoryUtil;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;
import com.freecharge.freefund.dos.entity.AdditionalReward;
import com.freecharge.freefund.dos.entity.FreefundCoupon;
import com.freecharge.promo.util.PromocodeConstants;
import com.freecharge.rest.model.coupon.BlockedCouponAndCouponCategory;
import com.freecharge.useragent.UserAgentController;
import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class EmailService {

	private Logger logger = LoggingFactory.getLogger(getClass());

	@Autowired
	private VelocityEngine velocityEngine;
	
	@Autowired
    private FCProperties fcProperties;
	
	@Autowired
    HomeBusinessDao homeBusinessDao;
	
	@Autowired
    private OrderIdWriteDAO orderIdDao;
	
	@Autowired
    private VoucherService voucherService;
	
	@Autowired
    private CouponService couponService;
	
	@Autowired
    private UserAgentController userAgentController;
	
    @Autowired
    @Qualifier(value = "emailSenderAsync")
    private IEmailSender mailSender;

	public void sendEmail(EmailBusinessDO emailBusinessDO) {
		if (emailBusinessDO == null) {
			logger.error("Email could not be sent. EmaiilBusinessDO object is null");
			return;
		}
		logger.info("Start sending email : " + emailBusinessDO.getId());
		generateEmailContent(emailBusinessDO);

		try {
			mailSender.sendEmail(emailBusinessDO);
		} catch (MailException ex) {
			logger.error("Error in sending email" + ex);
			throw new FCRuntimeException(ex);
		}
	}
	
	public void sendNonVelocityEmail(EmailBusinessDO emailBusinessDO) throws FCRuntimeException {
		if (emailBusinessDO == null) {
			logger.error("Email could not be sent. EmailBusinessDO object is null");
			return;
		}
		logger.info("Start sending email : " + emailBusinessDO.getId());
		if (emailBusinessDO.getText() == null && emailBusinessDO.getContent() != null) {
			emailBusinessDO.setText(emailBusinessDO.getContent());
		}

		try {
			mailSender.sendEmail(emailBusinessDO);
		} catch (MailException ex) {
			logger.error("Error in sending email" + ex);
			throw new FCRuntimeException(ex);
		}
	}

	public boolean sendEmailWithCouponDetails(EmailBusinessDO emailBusinessDO) throws FCRuntimeException {
		logger.info("Start sending email : " + emailBusinessDO.getSubject());
		if (emailBusinessDO == null) {
			logger.error("Email could not be sent. EmaiilBusinessDO object is null");
			return false;
		}

		emailBusinessDO = generateEmailContentWith2Map(emailBusinessDO);

		try {
			mailSender.sendEmail(emailBusinessDO);
			return true;
		} catch (MailException ex) {
			logger.error("Error in sending email" + ex);
			return false;
		}
	}

	private void generateEmailContent(EmailBusinessDO eb) {
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, getVelocityTemplate(eb), eb.getVariableValues());
		logger.info("generateEmailContent : Setting email text ....");
		eb.setText(text);

	}

	public EmailBusinessDO generateEmailContentWith2Map(EmailBusinessDO eb) {

		Map<String, Object> newMap = new HashMap();

		newMap.put("stringMap", eb.getVariableValues());
		newMap.put("objectMap", eb.getCouponsList());
		if (eb.getCouponsList() != null){
			newMap.put("allCouponsCount", eb.getCouponsList().size());
		}
		
		try {
			String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, getVelocityTemplate(eb), newMap);
			logger.info("generateEmailContentWith2Map : Setting email text ...." + text, null);
			// logger.info("generateEmailContentWith2Map : Setting email text ....",null);
			eb.setText(text);
		}
		catch (Exception e) {
			logger.error("generateEmailContentWith2Map : Setting email text ...." + e);
		}

		return eb;

	}

	private String getVelocityTemplate(EmailBusinessDO emailBusinessDO) {
		logger.info("VM template name is" + emailBusinessDO.getTemplateName());
		return emailBusinessDO.getTemplateName();

	}

	public void sendErrorNotification(String emailAddress, String emailIdFrom, String emailSubject, String velocityTemplate, Map<String, String> templateValue, boolean isHtml) {
		EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
		emailBusinessDO.setVariableValues(templateValue);
		emailBusinessDO.setFrom(emailIdFrom);
		emailBusinessDO.setTo(emailAddress);
		emailBusinessDO.setTemplateName(velocityTemplate);
		emailBusinessDO.setSubject(emailSubject);
		emailBusinessDO.setReplyTo(emailAddress);
		emailBusinessDO.setContentHtml(isHtml);
		this.sendEmail(emailBusinessDO);

	}

	public void inventoryMail(String emailAddress, String emailIdFrom, String emailSubject, String velocityTemplate, Map<String, String> templateValue, boolean isHtml) {
		EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
		emailBusinessDO.setVariableValues(templateValue);
		emailBusinessDO.setFrom(emailIdFrom);
		emailBusinessDO.setTo(emailAddress);
		emailBusinessDO.setTemplateName(velocityTemplate);
		emailBusinessDO.setSubject(emailSubject);
		emailBusinessDO.setReplyTo(emailAddress);
		emailBusinessDO.setContentHtml(isHtml);
		this.sendEmail(emailBusinessDO);

	}

	public void sendCouponinventoryNotificationMail(String emailAddress, String emailIdFrom, String emailSubject, String velocityTemplate, List<Map<String, Object>> couponList, boolean isHtml) {
		EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
		emailBusinessDO.setCouponsList(couponList);
		emailBusinessDO.setFrom(emailIdFrom);
		emailBusinessDO.setTo(emailAddress);
		emailBusinessDO.setTemplateName(velocityTemplate);
		emailBusinessDO.setSubject(emailSubject);
		emailBusinessDO.setReplyTo(emailIdFrom);
		emailBusinessDO.setContentHtml(isHtml);
		this.sendEmailWithCouponDetails(emailBusinessDO);
	}

	public void sendEmailNotificationWithMap(String emailToAddress, String emailIdFrom, String emailSubject, String velocityTemplate, Map<String, Object> parameterMap, boolean isHtml) {

		EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
		emailBusinessDO.setFrom(emailIdFrom);
		emailBusinessDO.setTo(emailToAddress);
		emailBusinessDO.setTemplateName(velocityTemplate);
		emailBusinessDO.setSubject(emailSubject);
		emailBusinessDO.setReplyTo(emailIdFrom);
		emailBusinessDO.setContentHtml(isHtml);
        emailBusinessDO.setHasAttachment(false);

		try {
			String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, velocityTemplate, parameterMap);
			logger.info("Sending email to " + emailToAddress + " with text: " + text);
			emailBusinessDO.setText(text);
			mailSender.sendEmail(emailBusinessDO);
		} catch (MailException ex) {
			logger.error("Error in sending email" + ex);
			throw new FCRuntimeException(ex);
		}
	}

    public void sendEmailNotificationWithAttachment(String emailToAddress, String emailIdFrom, String emailSubject,
                                                    String velocityTemplate, String attachmentFileName, Object attachmentData, Map<String, Object> parameterMap,
                                                    boolean isHtml){
        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setFrom(emailIdFrom);
        emailBusinessDO.setTo(emailToAddress);
        emailBusinessDO.setTemplateName(velocityTemplate);
        emailBusinessDO.setSubject(emailSubject);
        emailBusinessDO.setReplyTo(emailIdFrom);
        emailBusinessDO.setContentHtml(isHtml);
        emailBusinessDO.setHasAttachment(true);
        emailBusinessDO.setAttachmentFileName(attachmentFileName);
        emailBusinessDO.setAttachmentData(attachmentData);

        try {
            String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, velocityTemplate, parameterMap);
            logger.info("Sending email to " + emailToAddress + " with text: " + text);
            emailBusinessDO.setText(text);
            mailSender.sendEmail(emailBusinessDO);
        } catch (MailException ex) {
            logger.error("Error in sending email" + ex);
            throw new FCRuntimeException(ex);
        }
    }

    public void sendFreeFundCodeUploadNotificationEmail(String messages, String fileName, String emailToAddress,
                                                        List<String> inFileDuplicates, List<String> duplicateCodes) {
        String emailIdFrom = fcProperties.getProperty(FCConstants.ADMIN_ACTION_ALERT_FROM);
        String emailSubject = fileName + " : upload status notification";
        String velocityTemplate = "templates/mail/freeFundCodeUploadNotificationTemplate.vm";
        StringBuffer emailToAddresses = new StringBuffer(emailToAddress);
        String adminUpdateEmailId = fcProperties.getProperty(FCConstants.FREEFUND_CODES_UPDATE_NOTIFICATION_EMAIL);
        if(!FCUtil.isEmpty(adminUpdateEmailId)) {
            emailToAddresses.append("," + adminUpdateEmailId);
        }
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("message", messages);
        DataSource attachmentDataSource = getFreeFundEmailAttachmentDataSource(inFileDuplicates, duplicateCodes);
        if(attachmentDataSource == null) {
            sendEmailNotificationWithMap(emailToAddresses.toString(), emailIdFrom, emailSubject, velocityTemplate,
                    paramsMap, true);
        } else {
            sendEmailNotificationWithAttachment(emailToAddresses.toString(), emailIdFrom, emailSubject, velocityTemplate,
                    "duplicate_codes.txt", attachmentDataSource, paramsMap, true);
        }
    }

    public void sendFreeFundCodeUploadNotificationEmailForLargeUploads(String messages, String fileName, String emailToAddress,
                                                        List<FreefundCoupon> duplicates, List<FreefundCoupon> replacements) {
        String emailIdFrom = fcProperties.getProperty(FCConstants.ADMIN_ACTION_ALERT_FROM);
        String emailSubject = fileName + " : upload status notification";
        String velocityTemplate = "templates/mail/freeFundCodeUploadNotificationTemplate.vm";
        StringBuffer emailToAddresses = new StringBuffer(emailToAddress);
        String adminUpdateEmailId = fcProperties.getProperty(FCConstants.FREEFUND_CODES_UPDATE_NOTIFICATION_EMAIL);
        if(!FCUtil.isEmpty(adminUpdateEmailId)) {
            emailToAddresses.append("," + adminUpdateEmailId);
        }
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("message", messages);
        DataSource attachmentDataSource = getEmailAttachmentDataSource(duplicates, replacements);
        if(attachmentDataSource == null) {
            sendEmailNotificationWithMap(emailToAddresses.toString(), emailIdFrom, emailSubject, velocityTemplate,
                    paramsMap, true);
        } else {
            sendEmailNotificationWithAttachment(emailToAddresses.toString(), emailIdFrom, emailSubject, velocityTemplate,
                    "duplicate_codes_with_replacements.csv", attachmentDataSource, paramsMap, true);
        }
    }

    private DataSource getEmailAttachmentDataSource(List<FreefundCoupon> duplicates,
                                                    List<FreefundCoupon> replacements) {
        StringBuffer attachmentDataBuffer = new StringBuffer();
        for (int count = 0; count < duplicates.size(); count++) {
            attachmentDataBuffer.append(duplicates.get(count).getFreefundCode() + "," +
                    replacements.get(count).getFreefundCode() + "\n");
        }
        byte[] bytes = attachmentDataBuffer.toString().getBytes();
        return new ByteArrayDataSource(bytes, "text/plain");
    }

    private DataSource getFreeFundEmailAttachmentDataSource(List<String> inFileDuplicates, List<String> duplicateCodes) {
        if(FCUtil.isEmpty(inFileDuplicates) && FCUtil.isEmpty(duplicateCodes)) {
            return null;
        }
        StringBuffer attachmentDataBuffer = new StringBuffer();
        if(!FCUtil.isEmpty(duplicateCodes)) {
            attachmentDataBuffer.append("Duplicate codes\n");
            for(String duplicateCode: duplicateCodes) {
                attachmentDataBuffer.append(duplicateCode + "\n");
            }
        }
        if(!FCUtil.isEmpty(inFileDuplicates)) {
            attachmentDataBuffer.append("In file duplicate codes\n");
            for(String inFileDuplicate: inFileDuplicates) {
                attachmentDataBuffer.append(inFileDuplicate + "\n");
            }
        }
        byte[] bytes = attachmentDataBuffer.toString().getBytes();
        return new ByteArrayDataSource(bytes, "text/plain");
    }

     /*
	 * A simple function to send email with promotional reward code.
	 */
	public void sendPromocodeRewardMail(String userName, String emailId, String rewardCode, Integer rewardValue, String expiresOn) {
	    Map<String, Object> parameterMap = new HashMap<String, Object>();
	    parameterMap.put("userName", userName);
	    parameterMap.put("emailId", emailId);
	    parameterMap.put("rewardCode", rewardCode);
	    parameterMap.put("rewardValue", rewardValue);
	    parameterMap.put("expiresOn", expiresOn);
	    
	    String imgPrefix = fcProperties.getProperty("imgprefix1");
        imgPrefix = imgPrefix.trim();

        String hostPrefix = fcProperties.getProperty("hostprefix");
        hostPrefix = hostPrefix.trim();

        parameterMap.put("hostPrefix", hostPrefix);
        parameterMap.put("imgPrefix", imgPrefix);

        String from = fcProperties.getProperty(FCConstants.PROMOREWARD_FROM_EMAIL);

        sendEmailNotificationWithMap(emailId, from, "Your freecharge reward code !!",  "templates/mail/promoreward.vm", parameterMap, true); 
	}
	
	/*
	 * A simple function to send email with bin based offer - coupon code.
	 */
	public void sendBinBasedCouponMail(String userName, String emailId, List<String> couponCodes, String validFromStr, 
	                                   String validUptoStr, String mailSubject, String mailTemplate) {
	    Map<String, Object> parameterMap = new HashMap<String, Object>();
	    parameterMap.put("userName", userName);
	    parameterMap.put("emailId", emailId);
	    parameterMap.put("couponCodes", couponCodes);
	    parameterMap.put("validFrom", validFromStr);
	    parameterMap.put("validUpto", validUptoStr);

	    String from = fcProperties.getProperty(FCConstants.PROMOREWARD_FROM_EMAIL);

	    sendEmailNotificationWithMap(emailId, from, mailSubject, "templates/mail/" + mailTemplate + ".vm", parameterMap, true);
	}
	
	/**
	 * A function to send promocode cashback success/failure email 
	 */
	public void sendPromocodeCashbackMail(String userName, String emailId, String mailSubject, String mailTemplate,
                                          String offerName, String offerDescription, String couponCode,
                                          Double cashbackValue, String reasons, String orderId,
                                          Float additionalCashbackAmount) {
	    
	    Map<String, Object> parameterMap = new HashMap<String, Object>();
	    parameterMap.put("userName", userName);
	    parameterMap.put("offerName", offerName);
	    parameterMap.put("couponCode", couponCode);
	    parameterMap.put("cashbackValue", cashbackValue.intValue());
	    parameterMap.put("reasons", reasons);
	    parameterMap.put("orderId", orderId);
	    String userAgentRecordUrl = userAgentController.getRecordUrl(emailId);
	    parameterMap.put("userAgentRecordUrl", userAgentRecordUrl);
	    parameterMap.put("userAgentTrack", String.valueOf(fcProperties.isUserAgentTrackingEnabled()));
        parameterMap.put("additionalCashbackAmount", additionalCashbackAmount);

	    String from = fcProperties.getProperty(FCConstants.PROMOREWARD_FROM_EMAIL);

	    sendEmailNotificationWithMap(emailId, from, mailSubject,  "templates/mail/" + mailTemplate + ".vm", parameterMap, true); 
	}

    /**
     * A function to send special offer cashback success mail
     */
    public void sendSpecialOfferCashbackMail(String userName, String emailId, String mailSubject, String mailTemplate,
                                          String offerName, Double cashbackValue, String reasons, String orderId) {

        Map<String, Object> parameterMap = new HashMap<String, Object>();
        parameterMap.put("userName", FCUtil.isEmpty(userName)? "customer" : userName);
        parameterMap.put("offerName", offerName);
        parameterMap.put("cashbackValue", cashbackValue.intValue());
        parameterMap.put("reasons", reasons);
        parameterMap.put("orderId", orderId);
        String userAgentRecordUrl = userAgentController.getRecordUrl(emailId);
        parameterMap.put("userAgentRecordUrl", userAgentRecordUrl);
        parameterMap.put("userAgentTrack", String.valueOf(fcProperties.isUserAgentTrackingEnabled()));

        String from = fcProperties.getProperty(FCConstants.PROMOREWARD_FROM_EMAIL);

        sendEmailNotificationWithMap(emailId, from, mailSubject,  "templates/mail/" + mailTemplate + ".vm", parameterMap, true);
    }

	/**
     * A function to send Freefund Campaign create or update notification email
     */
	public void sendCampaignCreateOrUpdateMail(FreefundClass freefundClass, FreefundClass oldFreefundClass, String mailSubject, String curUserEmail) {
	    Map<String, Object> parameterMap = new HashMap<String, Object>();
	    SimpleDateFormat  sdf  = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	    String mailTemplate = FCConstants.CAMPAIGN_CREATE_UPDATE_NOTIFCATION_VM; 
	    String from = fcProperties.getProperty(FCConstants.PROMOREWARD_FROM_EMAIL);      
	    String to = fcProperties.getProperty(FCConstants.ADMIN_UPDATE_NOTIFICATION_EMAIL);;

	    if(to == null || to.equals("")) {
	        to = curUserEmail;
	    } 

	    parameterMap.put("curUserEmail", curUserEmail);       
	    parameterMap.put("freefundClass", freefundClass);     
	    parameterMap.put("validFrom", sdf.format(freefundClass.getValidFrom().getTime()));
	    parameterMap.put("validUpto", sdf.format(freefundClass.getValidUpto().getTime()));
	    parameterMap.put("createdAt", sdf.format(freefundClass.getCreatedAt().getTime()));
	    parameterMap.put("updatedAt", sdf.format(new Date()));

	    if(oldFreefundClass != null) {
	        parameterMap.put("oldFreefundClass", oldFreefundClass);    
	        parameterMap.put("oldValidFrom", sdf.format(oldFreefundClass.getValidFrom().getTime()));
	        parameterMap.put("oldValidUpto", sdf.format(oldFreefundClass.getValidUpto().getTime()));
	        parameterMap.put("oldCreatedAt", sdf.format(oldFreefundClass.getCreatedAt().getTime()));
	        parameterMap.put("oldUpdatedAt", sdf.format(oldFreefundClass.getUpdatedAt().getTime()));
	    }   
	    sendEmailNotificationWithMap(to, from, mailSubject,  "templates/mail/" + mailTemplate, parameterMap, true); 
	}

	/**
	 * A simpler method which sets usual mundane stuff like:
	 *   - hostPrefix, imgPrefix 
	 *   - from and bcc email address 
	 */
    public void sendEmailNotificationWithMap2(String subject, String toEmailId, String velocityTemplate,
                Map<String, Object> parameterMap, boolean isHtml) {
        String imgPrefix = fcProperties.getProperty("imgprefix1");
        imgPrefix = imgPrefix.trim();

        String hostPrefix = fcProperties.getProperty("hostprefix");
        hostPrefix = hostPrefix.trim();

        parameterMap.put("hostPrefix", hostPrefix);
        parameterMap.put("imgPrefix", imgPrefix);

        String from = fcProperties.getProperty(FCConstants.RECHARGE_FAILURE_FROM_EMAILID);

        sendEmailNotificationWithMap(toEmailId, from, subject, velocityTemplate, parameterMap, isHtml);    
    }
    
    public void sendEmailForARDealDisable(String emailToAddress, String emailSubject) {
    	
    	String from = fcProperties.getProperty(FCConstants.RECHARGE_FAILURE_FROM_EMAILID);
         
		EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
		emailBusinessDO.setFrom(from);
		emailBusinessDO.setTo(emailToAddress);
		emailBusinessDO.setSubject(emailSubject);
		emailBusinessDO.setReplyTo(from);

		try {
			logger.info("Sending email to " + emailToAddress + " with text: " + emailSubject);
			emailBusinessDO.setContent(emailSubject);
			this.sendNonVelocityEmail(emailBusinessDO);
		} catch (MailException ex) {
			logger.error("Error in sending email" + ex);
			throw new FCRuntimeException(ex);
		}
	}
    
    public void sendTeamMail(String teamEmail, String subject) {
        
        String from = fcProperties.getProperty(FCConstants.RECHARGE_FAILURE_FROM_EMAILID);
         
        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setFrom(from);
        emailBusinessDO.setTo(teamEmail);
        emailBusinessDO.setSubject(subject);
        emailBusinessDO.setReplyTo(from);

        try {
            logger.info("Sending email to " + teamEmail + " with text: " + subject);
            emailBusinessDO.setContent(subject);
            this.sendNonVelocityEmail(emailBusinessDO);
        } catch (MailException ex) {
            logger.error("Error in sending email" + ex);
            throw new FCRuntimeException(ex);
        }
    }

    public void sendEmailForArDeal(String to, String subject, String body, String bcc) {
        String from = fcProperties.getProperty(FCConstants.ARDEAL_FROM);
        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setTo(to);
        emailBusinessDO.setText(body);
        emailBusinessDO.setContentHtml(true);
        emailBusinessDO.setSubject(subject);
        emailBusinessDO.setReplyTo(from);
        emailBusinessDO.setFrom(from);
        emailBusinessDO.setBcc(bcc);
        mailSender.sendEmail(emailBusinessDO);
    }

    public boolean retriggerECouponMailer(String orderId) {
        boolean retriggerStatus = false;

        Map<String, Object> userDetail = homeBusinessDao.getUserRechargeDetailsFromOrderId(orderId);
    
        String userEmail = (String) userDetail.get("email");
        String userMobileNo = (String) userDetail.get("mobileno");
        String userFirstName = (String) userDetail.get("firstname");
        String userRechargeAmount = userDetail.get("amount").toString();
        String lookupId = (String) userDetail.get("lookupId");
        String operatorName = (String) userDetail.get("operatorName");
        String createdAt = userDetail.get("createdAt").toString();

        String from = null, bcc = null, replyTo = null, subject = null;

        Map<String, String> variableValues = new HashMap<String, String>();

        String imgPrefix = fcProperties.getProperty("imgprefix1");
        imgPrefix = imgPrefix.trim();

        String hostPrefix = fcProperties.getProperty("hostprefix");
        hostPrefix = hostPrefix.trim();

        String voucherPrefix = fcProperties.getProperty("voucherprefix");
        voucherPrefix = voucherPrefix.trim();

        variableValues.put("orderno", orderId.trim());
        variableValues.put("createdAt", createdAt);
        variableValues.put("rechargeamount", userRechargeAmount.trim());
        variableValues.put("mobileno", userMobileNo.trim());
        variableValues.put("operator", operatorName.trim());
        variableValues.put("firstname", userFirstName.trim());
        variableValues.put("userEmail", userEmail);

        variableValues.put("hostPrefix", hostPrefix);
        variableValues.put("imgPrefix", imgPrefix);
        variableValues.put("voucherPrefix", voucherPrefix);

        List<OrderId> orderIdList = orderIdDao.getByOrderId(orderId);
        if (orderIdList == null || orderIdList.isEmpty())
            return retriggerStatus;

        List<Map<String, Object>> couponList = null;
        couponList = voucherService.getCouponDetailsBylookupId(lookupId);

        // Check for P & E Coupons
        Boolean pCouponsSelected = CouponInventoryUtil.couponTypeExists(couponList, FCConstants.COUPON_TYPE_P);
        Boolean eCouponsSelected = CouponInventoryUtil.couponTypeExists(couponList, FCConstants.COUPON_TYPE_E);

        variableValues.put("pCouponsSelected", pCouponsSelected.toString());
        variableValues.put("eCouponsSelected", eCouponsSelected.toString());

        if (couponList != null && couponList.size() > 0) {
            List<Map<String, Object>> modifiedcouponListForEmail = new ArrayList<Map<String, Object>>();
            modifiedcouponListForEmail = couponService.addCouponCodesinList(couponList, orderId, lookupId);

            from = fcProperties.getProperty(FCConstants.RECHARGE_SUCCESS_FROM_EMAILID);
            subject = fcProperties.getProperty(FCConstants.RECHARGE_COUPON_SUBJECT) + " " + orderId.trim();
            replyTo = fcProperties.getProperty(FCConstants.RECHARGE_SUCCESS_REPLYTO);

            EmailBusinessDO couponEmailBusinessDO = new EmailBusinessDO();

            couponEmailBusinessDO.setVariableValues(variableValues);
            couponEmailBusinessDO.setFrom(from);
            couponEmailBusinessDO.setTo(userEmail);
            couponEmailBusinessDO.setBcc(bcc);
            couponEmailBusinessDO.setReplyTo(replyTo);
            couponEmailBusinessDO.setSubject(subject);
            couponEmailBusinessDO.setCouponsList(modifiedcouponListForEmail);
            couponEmailBusinessDO.setTemplateName("templates/mail/successMailCoupon.vm");

            retriggerStatus = sendEmailWithCouponDetails(couponEmailBusinessDO);
        }
        return retriggerStatus;
    }

    public void sendDiwaliOfferCompetitionEnteredEmail(String firstName, String emailId, String subject,
                                                       String mailTemplate, String name, String reasons, String orderId, Map<String, List<BlockedCouponAndCouponCategory>> blockedCouponAndCouponCategoryMap) {
        Map<String, Object> parameterMap = new HashMap<String, Object>();
        parameterMap.put("userName", FCUtil.isEmpty(firstName)? "customer" : firstName);
        parameterMap.put("orderId", orderId);
        String userAgentRecordUrl = userAgentController.getRecordUrl(emailId);
        parameterMap.put("userAgentRecordUrl", userAgentRecordUrl);
        parameterMap.put("userAgentTrack", String.valueOf(fcProperties.isUserAgentTrackingEnabled()));
        parameterMap.put("coupons", blockedCouponAndCouponCategoryMap);
        parameterMap.put("imgPrefix", fcProperties.getProperty("voucherprefix"));
        parameterMap.put("imgPrefix1", fcProperties.getProperty("imgprefix1"));
        String from = fcProperties.getProperty(FCConstants.PROMOREWARD_FROM_EMAIL);

        EmailBusinessDO emailBusinessDO = new EmailBusinessDO();
        emailBusinessDO.setTemplateName("templates/mail/" + mailTemplate);
        emailBusinessDO.setBcc("support@freecharge.in");
        emailBusinessDO.setFrom(from);
        emailBusinessDO.setTo(emailId);
        emailBusinessDO.setSubject(subject);
        emailBusinessDO.setReplyTo(from);
        Boolean isHtml = true;
        emailBusinessDO.setContentHtml(isHtml);
        emailBusinessDO.setHasAttachment(false);
        String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/mail/" + mailTemplate, parameterMap);
        emailBusinessDO.setText(text);

        mailSender.sendEmail(emailBusinessDO);
    }

    public void sendAdditionalRewardCreatedNotificationEmail(AdditionalReward additionalReward) {
        String to = fcProperties.getProperty(FCConstants.ADMIN_UPDATE_NOTIFICATION_EMAIL);
        String from = fcProperties.getProperty(FCConstants.PROMOREWARD_FROM_EMAIL);
        String subject = "Additional Reward created - " + additionalReward.getRewardName();
        sendEmailNotificationWithMap(to,
                from,
                subject,
                "templates/mail/" + FCConstants.DOUBLE_REWARD_CREATE_EMAIL_TEMPLATE,
                additionalReward.toMap(), true);
    }

    public void sendAdditionalRewardEditedNotificationEmail(AdditionalReward additionalReward,
                                                            AdditionalReward oldAdditionalReward,
                                                            String name,
                                                            String emailId) {
        String to = fcProperties.getProperty(FCConstants.ADMIN_UPDATE_NOTIFICATION_EMAIL);
        String from = fcProperties.getProperty(FCConstants.PROMOREWARD_FROM_EMAIL);
        String subject = "Additional Reward edited for campaign " + name;
        SimpleDateFormat  sdf  = new SimpleDateFormat(PromocodeConstants.DATE_FORMAT);
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("additionalReward", additionalReward);
        paramMap.put("oldAdditionalReward", oldAdditionalReward);
        paramMap.put("curUserEmail", emailId);
        sendEmailNotificationWithMap(to,
                from,
                subject,
                "templates/mail/" + FCConstants.DOUBLE_REWARD_EDIT_EMAIL_TEMPLATE,
                paramMap, true);
    }

    public void sendPromocodeReIssuedMail(String firstName, String freefundCode, String campaignName,
                                          Float freefundValue, String reIssuedCode, Date validUpto, String emailId, String orderId) {
        Map<String, Object> parameterMap = new HashMap<String, Object>();
        parameterMap.put("userName", FCUtil.isEmpty(firstName)? "customer" : firstName);
        parameterMap.put("orderId", orderId);
        parameterMap.put("usedCode", freefundCode);
        parameterMap.put("campaignName", campaignName);
        parameterMap.put("freefundValue", freefundValue);
        parameterMap.put("reIssuedCode", reIssuedCode);
        parameterMap.put("validUpto", validUpto.toString());
        String userAgentRecordUrl = userAgentController.getRecordUrl(emailId);
        parameterMap.put("userAgentRecordUrl", userAgentRecordUrl);
        parameterMap.put("userAgentTrack", String.valueOf(fcProperties.isUserAgentTrackingEnabled()));
        String from = fcProperties.getProperty(PromocodeConstants.RECHARGE_FAILURE_REISSUE_PROMOCODE_FROM);
        sendEmailNotificationWithMap(emailId, from, "New Promocode for your order " + orderId, "templates/mail/"
                + "ReIssuedPromocodes.vm", parameterMap, true);
    }

	public void sendFreeFundCodeDeleteOrBlockNotificationEmail(int count, Map<String, FreefundCoupon> nonExistingMap,
															   Map<String, FreefundCoupon> redeemedMap, String action,
															   String name) {
		Map<String, Object> parameterMap = new HashMap<>();
		parameterMap.put("action", action);
		parameterMap.put("count", count);
		parameterMap.put("name", name);
		String from = fcProperties.getProperty(PromocodeConstants.RECHARGE_FAILURE_REISSUE_PROMOCODE_FROM);
		String adminUpdateEmailId = fcProperties.getProperty(FCConstants.FREEFUND_CODES_UPDATE_NOTIFICATION_EMAIL);

		DataSource attachmentDataSource = getEmailAttachmentDataSourceForDeleteCdes(nonExistingMap, redeemedMap);
		String emailSubject = action + " freefund codes status";
		String velocityTemplate="templates/mail/FreefundDeleteCodesStatus.vm";
		if(attachmentDataSource == null) {
			sendEmailNotificationWithMap(adminUpdateEmailId, from, emailSubject, velocityTemplate,
					parameterMap, true);
		} else {
			sendEmailNotificationWithAttachment(adminUpdateEmailId, from, emailSubject, velocityTemplate,
					"NonExistingAndRedeemedCodes.csv", attachmentDataSource, parameterMap, true);
		}
	}

	private DataSource getEmailAttachmentDataSourceForDeleteCdes(Map<String, FreefundCoupon> nonExistingMap,
																 Map<String, FreefundCoupon> redeemedMap) {
		StringBuffer attachmentDataBuffer = new StringBuffer();
		boolean rowsExist = true;

		Iterator<Map.Entry<String, FreefundCoupon>> nonExistingItr = null;
		Iterator<Map.Entry<String, FreefundCoupon>> redeemedIterator = null;

		if (!FCUtil.isEmpty(nonExistingMap)) {
			nonExistingItr = nonExistingMap.entrySet().iterator();
		}

		if (!FCUtil.isEmpty(redeemedMap)) {
			redeemedIterator = redeemedMap.entrySet().iterator();
		}

		attachmentDataBuffer.append("Non Existing codes" + "," + "Redeemed codes" + "\n");
		while (rowsExist) {
			if (nonExistingItr != null && nonExistingItr.hasNext()) {
				attachmentDataBuffer.append(nonExistingItr.next().getValue().getFreefundCode() + ",");
				rowsExist = true;
			} else {
				attachmentDataBuffer.append(",");
				rowsExist = false;
			}

			if (redeemedIterator != null && redeemedIterator.hasNext()) {
				attachmentDataBuffer.append(redeemedIterator.next().getValue().getFreefundCode());
				rowsExist = true;
			} else {
				attachmentDataBuffer.append("");
			}
			attachmentDataBuffer.append("\n");
		}
		byte[] bytes = attachmentDataBuffer.toString().getBytes();
		return new ByteArrayDataSource(bytes, "text/plain");
	}
}
