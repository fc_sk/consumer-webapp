package com.freecharge.common.comm.email;

public interface IEmailappTemplateNameResolver {

    public String resolveEmailappTemplateName();

}
