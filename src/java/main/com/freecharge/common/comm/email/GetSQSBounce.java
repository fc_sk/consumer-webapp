package com.freecharge.common.comm.email;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.freecharge.app.domain.dao.jdbc.UserWriteDao;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.google.common.base.CaseFormat;

@Service
public class GetSQSBounce extends FreeChargeJob {
    private static final Logger       logger      = LoggingFactory.getLogger(GetSQSBounce.class);
    private BasicAWSCredentials       credentials;
    private AmazonSQSClient                sqs;
    private String                    simpleQueue = "bounce-queue";
    @Autowired
    private FCProperties              fcProperties;

    @Autowired
    private UserWriteDao               userWriteDao;
    
    @Autowired
    private UserServiceProxy userServiceProxy;

    /**
     * returns the queueurl for for sqs queue if you pass in a name
     * 
     * @param queueName
     * @return
     */
    public String getQueueUrl(String queueName) {
        GetQueueUrlRequest getQueueUrlRequest = new GetQueueUrlRequest(queueName);
        return this.sqs.getQueueUrl(getQueueUrlRequest).getQueueUrl();
    }

    /**
     * gets messages from your queue
     * 
     * @param queueUrl
     * @return
     */
    public List<Message> getMessagesFromQueue(String queueUrl) {
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl);
        receiveMessageRequest.withMaxNumberOfMessages(10);
        List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
        return messages;
    }

    /**
     * deletes a single message from your queue.
     * 
     * @param queueUrl
     * @param message
     */
    public void deleteMessageFromQueue(String queueUrl, Message message) {
        String messageRecieptHandle = message.getReceiptHandle();
        logger.info("Message to be deleted : " + message.getBody() + "." + message.getReceiptHandle());
        sqs.deleteMessage(new DeleteMessageRequest(queueUrl, messageRecieptHandle));
        logger.info("Message Deleted...");
    }

    public static String getBeanName() {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, GetSQSBounce.class.getSimpleName());
    }

    public static void main(String[] args) throws JSONException, IOException {
        logger.info("Starting to run the Job....");

        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        GetSQSBounce awssqsUtil = ((GetSQSBounce) ctx.getBean(GetSQSBounce.getBeanName()));

        try {
            // Fetch the credentials and create sqs client
            awssqsUtil.credentials = new BasicAWSCredentials(awssqsUtil.fcProperties.getAwsAccessKey(),
                    awssqsUtil.fcProperties.getAwsSecretKey());
            awssqsUtil.sqs = new AmazonSQSClient(awssqsUtil.credentials);

            String queueUrl = awssqsUtil.getQueueUrl(awssqsUtil.simpleQueue);
            logger.info("Fetching 10 messages from bounce-queue");
            List<Message> messages = awssqsUtil.getMessagesFromQueue(queueUrl);
            logger.info("Fetch Done");

            // For 1000 messages from the queue
            int count = 1;
            for (int i = 0; i < count; i++) {
                for (Message message : messages) {
                    try {
                        JSONObject jsn_msg = new JSONObject(message.getBody());
                        String msg = jsn_msg.getString("Message");
                        JSONObject jsn_bounce = new JSONObject(msg);
                        String bounce = jsn_bounce.getString("bounce");
                        JSONObject jsn_bounced = new JSONObject(bounce);
                        logger.info(jsn_bounced.getString("bouncedRecipients"));
                        if (!jsn_bounced.getString("bouncedRecipients").equals("[]")) {
                            JSONArray bounced = jsn_bounced.getJSONArray("bouncedRecipients");
                            String bouncedRecipients = bounced.getString(0);
                            JSONObject jsn_email = new JSONObject(bouncedRecipients);

                            // Handle the bounced email
                            try {
                                Users users = awssqsUtil.userServiceProxy.getUserByEmailId(jsn_email.getString("emailAddress"));
                                String userID = users.getUserId().toString();
                                logger.info("User Id: " + userID);
                                logger.info("Calling bounce email handler...");
                            } catch (NullPointerException e) {
                                logger.error(
                                        "NullPointerException on fetching user id from email: "
                                                + jsn_email.getString("emailAddress"), e);
                            }

                            logger.info("Deleting message from Queue...");
                            // Delete the message from queue
                            awssqsUtil.deleteMessageFromQueue(queueUrl, message);
                        }
                    } catch (Exception e) {
                        logger.error("Error : ", e);
                    }
                }
                logger.info("Fetching 10 messages from bounce-queue");
                messages = awssqsUtil.getMessagesFromQueue(queueUrl);
                logger.info("Fetch Done");
            }
        } catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, which means your request made it to Amazon SQS, but was " +
            		"rejected with an error response for some reason."
                    + "Error Message: "
                    + ase.getMessage()
                    + ",HTTP Status Code: "
                    + ase.getStatusCode()
                    + ",AWS Error Code: "
                    + ase.getErrorCode()
                    + ",Error Type: "
                    + ase.getErrorType()
                    + ",Request ID: "
                    + ase.getRequestId());
        } catch (AmazonClientException ace) {
            logger.error("Caught an AmazonClientException, which means the client encountered a serious internal " +
            		"problem while trying to communicate with SQS, such as not being able to access the network."
                    + "Error Message: " + ace.getMessage());
        } finally {
            logger.info("Trying to destroy BatchJobUtil object....");
            BatchJobUtil.destroyBatchJobContext(ctx);
        }
        logger.info("Job Completed");
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        // TODO Auto-generated method stub
        
    }
}
