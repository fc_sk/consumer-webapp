package com.freecharge.common.comm.email;

import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.apache.xerces.impl.dv.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.recharge.util.RechargeConstants;

@Component
public class EmailSenderOctane implements IEmailSender {
	private Logger logger = LoggingFactory.getLogger(getClass());
	@Autowired
	private FCProperties fcProperties;
	@Autowired
	private VelocityEngine velocityEngine;
	@Autowired
	private MailSender mailSender;

	public void sendEmail(final EmailBusinessDO emailBusinessDO) {
		Thread mailThread = new Thread() {
			public void run() {
				try {
                    if (FCUtil.isEmailBlockedForDomain(emailBusinessDO.getTo())) {
                        logger.info("User Email: " + emailBusinessDO.getTo() + " is currently disabled for Emails.");
                        return;
                    }
					HttpClient httpClient = new HttpClient();
					PostMethod method = new PostMethod(fcProperties.getProperty(RechargeConstants.OCTANE_EMAIL_API_URL));
					method.setParameter("Request", getEmailRechargeInputXml(emailBusinessDO));
					int statusCode = httpClient.executeMethod(method);
					logger.info("" + method.getResponseBodyAsString());
					if (statusCode == RechargeConstants.HTTP_SUCCESS_STATUS_CODE) {
						logger.info("Mail sent successfully ");
					} else {
						sendMailUsingJavaApi(emailBusinessDO);
					}
				} catch (Exception exception) {
					logger.info("Error occured during mail sending process through Octane API successfully");
					sendMailUsingJavaApi(emailBusinessDO);
				}
			}
		};
		mailThread.start();
		BatchJobUtil.handleThread(mailThread);
	}

	private String getEmailRechargeInputXml(EmailBusinessDO emailBusinessDO) {
		String inputXml = "";
		Map<String, Object> templateValues = new HashMap<String, Object>();

		templateValues.put("emailSubject", getEncodedString(emailBusinessDO.getSubject()));
		templateValues.put("fromName", getEncodedString(emailBusinessDO.getFrom()));
		templateValues.put("fromEmailId", emailBusinessDO.getFrom());
		templateValues.put("userReplyEmailId", emailBusinessDO.getFrom());
		templateValues.put("emailFormat", "2");
		templateValues.put("emailContentSource", "1");
		templateValues.put("emailContent", getEncodedString(emailBusinessDO.getText()));
		templateValues.put("octaneAccessPassword", fcProperties.getProperty(RechargeConstants.OCTANE_ACCESS_PASSWORD));
		templateValues.put("octaneAccesskey", fcProperties.getProperty(RechargeConstants.OCTANE_ACCESS_KEY));
		templateValues.put("recipientMailId", emailBusinessDO.getTo());
		templateValues.put("recipientFirstName", emailBusinessDO.getTo().split("@")[0]);
		templateValues.put("recipientLastName", emailBusinessDO.getTo().split("@")[0]);
		inputXml = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "octaneEmailRequestTemplate.vm", templateValues);
		return inputXml;
	}

	private String getEncodedString(String input) {
		return Base64.encode(input.getBytes());
	}

	private void sendMailUsingJavaApi(EmailBusinessDO emailBusinessDO) {
		MimeMessage msg = ((JavaMailSenderImpl) mailSender).createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(msg, true);
			helper.setTo(emailBusinessDO.getTo());
			helper.setReplyTo(emailBusinessDO.getReplyTo());
			helper.setSubject(emailBusinessDO.getSubject());
			if (emailBusinessDO.getBcc() != null && !emailBusinessDO.getBcc().isEmpty()) {
				helper.setBcc(emailBusinessDO.getBcc().split(","));
			}
			helper.setText(emailBusinessDO.getText(), true);
			helper.setFrom(emailBusinessDO.getFrom());
			((JavaMailSenderImpl) mailSender).send(msg);
			logger.info("Mail sent successfully");
		} catch (Exception e) {
			logger.info("Error occured during mail sending process through Java API successfully");
		}
	}
}
