package com.freecharge.common.comm.email;

import com.freecharge.emailapp.client.TemplateName;

public class WebappToEmailappTemplateNameResolver implements IEmailappTemplateNameResolver {

    public final String webappEmailTemplateName;

    public WebappToEmailappTemplateNameResolver(String webappEmailTemplateName) {
        this.webappEmailTemplateName = webappEmailTemplateName;
    }

    @Override
    public String resolveEmailappTemplateName() {
        switch (webappEmailTemplateName) {
            case "templates/mail/rechargeSuccessMail.vm":
                return TemplateName.RECHARGE_SUCCESS;
            default:
                throw new IllegalArgumentException(String.format(
                        "Webapp template name %s has no equivalent Email template name", webappEmailTemplateName));
        }
    }

}
