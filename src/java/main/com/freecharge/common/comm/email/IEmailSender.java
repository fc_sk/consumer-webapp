package com.freecharge.common.comm.email;


public interface IEmailSender {
    public void sendEmail(EmailBusinessDO emailBusinessDO);
}
