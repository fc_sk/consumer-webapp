package com.freecharge.common.comm.email;

import java.io.File;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;

@Component
public class EmailSenderAsync implements IEmailSender {
	private Logger logger = LoggingFactory.getLogger(getClass());
    @Autowired
	private MailSender mailSender;

    @Autowired
    private FCProperties fcProperties;

	public void sendEmail(final EmailBusinessDO emailBusinessDO) {
		Thread mailThread = new Thread() {
            public void run() {
                MimeMessage msg = ((JavaMailSenderImpl) mailSender).createMimeMessage();
                MimeMessageHelper helper;

                try {
                    if (FCUtil.isEmailBlockedForDomain(emailBusinessDO.getTo())) {
                        logger.info("User Email: " + emailBusinessDO.getTo() + " is currently disabled for Emails.");
                        return;
                    }
                    helper = new MimeMessageHelper(msg, true);
                    helper.setTo(emailBusinessDO.getTo().split(","));
                    helper.setReplyTo(emailBusinessDO.getReplyTo());
                    helper.setSubject(emailBusinessDO.getSubject());
                    helper.setText(emailBusinessDO.getText(), true);

                    /* For sending bcc only for coupon mail,crosssell mails,reissued coupon-crosssell mails */
                    if(emailBusinessDO.getBcc()!=null && !emailBusinessDO.getBcc().isEmpty()) {
                        helper.setBcc(emailBusinessDO.getBcc().split(","));
                    }
                    	 
                    String fromName = fcProperties.getEmailFromName();

                    InternetAddress from = new InternetAddress(emailBusinessDO.getFrom());

                    if (StringUtils.isNotEmpty(fromName)) {
                        from.setPersonal(fromName);
                    }

                    helper.setFrom(from);

                    if(emailBusinessDO.getHasAttachment()) {
                        Object attachmentData = emailBusinessDO.getAttachmentData();
                        if(attachmentData instanceof DataSource){
                            helper.addAttachment(emailBusinessDO.getAttachmentFileName(), (DataSource) emailBusinessDO.getAttachmentData());
                        } else if(attachmentData instanceof File){
                            helper.addAttachment(emailBusinessDO.getAttachmentFileName(), (File) emailBusinessDO.getAttachmentData());
                        } else if(attachmentData instanceof InputStreamSource){
                            helper.addAttachment(emailBusinessDO.getAttachmentFileName(), (InputStreamSource) emailBusinessDO.getAttachmentData());
                        } else {
                            logger.error("attachmentData can only be of type javax.activation.DataSource, java.io.File, org.springframework.core.io.InputStreamResource");
                            logger.error("Sending email without attachment.");
                        }
                    }
                    ((JavaMailSenderImpl) mailSender).send(msg);
                    logger.info("Mail sent successfully ");
                } catch (MessagingException ex) {
                    logger.error("exception raised while sendding email ");
                    logger.error("Exception: msg", ex);
                } catch (Exception e) {
                    logger.error("Exception: msg", e);
                }

            }
        };
		mailThread.start();
		BatchJobUtil.handleThread(mailThread);
	}
}
