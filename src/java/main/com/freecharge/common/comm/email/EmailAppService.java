package com.freecharge.common.comm.email;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.freecharge.emailapp.client.EmailappClient;
import com.freecharge.emailapp.client.TemplateName;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.web.util.AtomicPercentageCounter;

@Service
public class EmailAppService {

    private final String thisName = getClass().getName();
    private final Log log = LogFactory.getLog(getClass());

    private final AtomicPercentageCounter apc = new AtomicPercentageCounter();

    private static final Integer HARD_BOUNCE = Integer.valueOf(1);
    private static final Integer SOFT_BOUNCE = Integer.valueOf(2);

    @Autowired
    AmazonDynamoDBAsync dynamoClient;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private EmailService emailService;

    @Autowired
    @Qualifier(value = "emailSenderAsync")
    private IEmailSender mailSender;

    @Autowired
    @Qualifier("emailAppExecutor")
    private ThreadPoolTaskExecutor emailAppAsyncThreadPool;

    @Autowired
    private AppConfigService appConfigService;

    private EmailappClient emailAppClient;
    private int percentAllowed = 0;
    private boolean emailAppAsyncEnabled = false;

    @Autowired
    private void setFcProperties(FCProperties fcprops) {
        final String awsID = fcprops.getEmailAppAwsAccessKey();
        final String awsSecret = fcprops.getEmailAppAwsAccessSecret();
        final String awsEndpoint = fcprops.getEmailAppAwsEndpoint();
        String queueURL = fcprops.getEmailAppSqsUrlPrefix() + fcprops.getEmailAppSqsQueueName();
        if (fcprops.isEmailAppSqsUrlHostnameAsSuffix()) {
            try {
                queueURL += "-" + InetAddress.getLocalHost().getHostName().replace('.', '_');
            } catch (UnknownHostException e) {
                throw new IllegalStateException("Unable to determine hostname", e);
            }
        }
        this.emailAppClient = new EmailappClient(awsID, awsSecret, awsEndpoint, queueURL);
        this.percentAllowed = fcprops.getEmailAppPercent();
        this.emailAppAsyncEnabled = fcprops.isEmailAppAsyncEnabled();
    }

    public void submitEmailRequest(final EmailBusinessDO emailBusinessDO) {
        submitEmailRequest(emailBusinessDO, new WebappToEmailappTemplateNameResolver(emailBusinessDO.getTemplateName()));
    }

    public void submitEmailRequest(final EmailBusinessDO emailBusinessDO, final IEmailappTemplateNameResolver templateNameResolver) {
        if (log.isTraceEnabled()) {
            log.trace("Entered " + thisName + ".submitEmailRequest(EmailBusinessDO), arg=" + emailBusinessDO);
        }
        if (emailBusinessDO == null) {
            throw new IllegalArgumentException("Expected a valid EmailBusinessDO object but found NULL");
        }
        final String templateName = templateNameResolver.resolveEmailappTemplateName();
        if (FCUtil.isEmpty(templateName)) {
            throw new IllegalStateException(String.format(
                    "Expected a valid Emailapp template name, but found: '%s'", templateName));
        }
        // do not send email if banned
        try {
            final GetItemRequest getter = new GetItemRequest("bounceList",
                    Collections.singletonMap("emailId", new AttributeValue(emailBusinessDO.getTo())));
            getter.setConsistentRead(false);
            final GetItemResult got = dynamoClient.getItem(getter);
            if (got != null && got.getItem() != null &&  HARD_BOUNCE.equals(got.getItem().get("bouncetype"))) {
                log.warn(String.format("Email address %s is blacklisted; not sending email", emailBusinessDO.getTo()));
                return;
            }
        } catch (final RuntimeException e) {
            log.error("Error determining whether recipient email address is blacklisted", e);
        }
        if (appConfigService.isEmailAppEnabled() && apc.incrementAndGet() <= percentAllowed) {
            log.debug("Determined email to be sent via emailAppService (emailapp)");
            final Map<String, Object> data = new HashMap<>();
            try {
                data.putAll(emailBusinessDO.getConsolidatedEmailMap());
            } catch (RuntimeException e) {
                log.error("Unable to convert EmailBusinessDO.getVariableValues to Map<String, Object>", e);
                throw new IllegalStateException(String.format(
                        "Unable to convert EmailBusinessDO.getVariableValues (%s) to Map<String, Object>",
                        emailBusinessDO.getVariableValues()), e);
            }
            if (this.emailAppAsyncEnabled) {
                log.debug("Sending email asynchronously via emailapp");
                try {
                    emailAppAsyncThreadPool.submit(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                final long start = System.currentTimeMillis();
                                final String messageID = emailAppClient.submitEmailRequest(
                                        templateNameResolver.resolveEmailappTemplateName(), data,
                                        fcProperties.getEmailFromName(), emailBusinessDO.getFrom(),
                                        emailBusinessDO.getTo());
                                metricsClient.recordLatency("emailappClient", "emailRequestSubmission",
                                        System.currentTimeMillis() - start);
                                metricsClient.recordEvent("emailappClient", "emailRequestSubmission", "enqueue");
                                log.info(String.format(
                                        "Sent email asynchronously via emailapp, Message-ID: %s, Email: %s",
                                        messageID, emailBusinessDO));
                            } catch (RuntimeException e) {
                                log.error("Unable to send email asynchronously via emailapp", e);
                            }
                        }
                    });
                } catch (RuntimeException e) {
                    log.error("Unable to send email asynchronously via emailapp", e);
                    throw e;
                }
            } else {
                log.debug("Sending email synchronously via emailapp");
                try {
                    final long start = System.currentTimeMillis();
                    final String messageID = emailAppClient.submitEmailRequest(
                            templateNameResolver.resolveEmailappTemplateName(), data, fcProperties.getEmailFromName(),
                            emailBusinessDO.getFrom(), emailBusinessDO.getTo());
                    metricsClient.recordLatency("emailappClient", "emailRequestSubmission",
                            System.currentTimeMillis() - start);
                    metricsClient.recordEvent("emailappClient", "emailRequestSubmission", "enqueue");
                    log.info(String.format(
                            "Sent email synchronously via emailapp, Message-ID: %s, Email: %s",
                            messageID, emailBusinessDO));
                } catch (RuntimeException e) {
                    log.error("Unable to send email synchronously via emailapp", e);
                    throw e;
                }
            }
        } else {
            log.debug("Sending email via emailService (webapp)");
            try {
                final EmailBusinessDO preProcessed = preProcess(emailBusinessDO);
                mailSender.sendEmail(preProcessed);
            } catch (RuntimeException e) {
                log.error("Unable to send email via webapp: " + emailBusinessDO, e);
            }
        }

        if (log.isTraceEnabled()) {
            log.trace("Exiting " + thisName + ".submitEmailRequest(EmailBusinessDO)");
        }
    }

    private String getEmailAppTemplateName(String webappTemplateName) {
        switch (webappTemplateName) {
            case "templates/mail/rechargeSuccessMail.vm":
                return TemplateName.RECHARGE_SUCCESS;
            case "templates/mail/successMailCoupon.vm":
            	//return TemplateName.RECHARGE_SUCCESS_WITH_COUPON;
            default:
                return null;
        }
    }

    private EmailBusinessDO preProcess(EmailBusinessDO emailBusinessDO) {
        EmailBusinessDO result = emailBusinessDO;
        switch (emailBusinessDO.getTemplateName()) {
        case "templates/mail/rechargeSuccessMail.vm":
            result = emailService.generateEmailContentWith2Map(emailBusinessDO);
            break;
        case "templates/mail/successMailCoupon.vm":
            result = emailService.generateEmailContentWith2Map(emailBusinessDO);
            break;
        }
        return result;
    }

}
