package com.freecharge.common.comm.email;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class EmailBusinessDO extends BaseBusinessDO {

    private String to;
    private String from;
    private String subject;
    private String templateName;
    private Integer templateCode;
    private Map<String,String> variableValues;
    private String content;
    private Boolean isContentHtml;
    private String cc;
    private String bcc;
    private String replyTo;
    private String text;
    private List<Map<String, Object>> couponsList;
    private boolean hasAttachment;
    private String attachmentFileName;
    private Object attachmentData;
    

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Integer getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(Integer templateCode) {
        this.templateCode = templateCode;
    }

    public Map<String, String> getVariableValues() {
        return variableValues;
    }

    public void setVariableValues(Map<String, String> variableValues) {
        this.variableValues = variableValues;
    }
    
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getContentHtml() {
        return isContentHtml;
    }

    public void setContentHtml(Boolean contentHtml) {
        isContentHtml = contentHtml;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Map<String, Object>> getCouponsList() {
        return couponsList;
    }

    public void setCouponsList(List<Map<String, Object>> couponsList) {
        this.couponsList = couponsList;
    }

    public boolean getHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(boolean hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public String getAttachmentFileName() {
        return attachmentFileName;
    }

    public void setAttachmentFileName(String attachmentFileName) {
        this.attachmentFileName = attachmentFileName;
    }

    public Object getAttachmentData() {
        return attachmentData;
    }

    public void setAttachmentData(Object attachmentData) {
        this.attachmentData = attachmentData;
    }

    private final Log log = LogFactory.getLog(getClass());
    
    public Map<String, Object> getConsolidatedEmailMap(){
    	Map<String, Object> data = new HashMap<>();
        if(getVariableValues() != null){
        	try {
                for (Map.Entry<String, String> pair: getVariableValues().entrySet()) {
                    data.put(pair.getKey(), pair.getValue());
                }
            } catch (RuntimeException e) {log.error(String.format("Unable to convert EmailBusinessDO.getVariableValues (%s) to "
            		+ "Map<String, Object>",getVariableValues()), e);
            }
        }
        if(getCouponsList() != null){
        	try {
        		for(Map<String, Object> couponMap : getCouponsList())
	                for (Map.Entry<String, Object> pair: couponMap.entrySet()) {
	                    data.put(pair.getKey(), pair.getValue());
	                }
            } catch (RuntimeException e) {log.error(String.format("Unable to convert EmailBusinessDO.getCouponsList (%s) to "
            		+ "Map<String, Object>",getCouponsList()), e);
            }
        }
        return data;
    }
}
