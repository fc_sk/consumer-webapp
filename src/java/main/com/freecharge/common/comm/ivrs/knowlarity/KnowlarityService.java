package com.freecharge.common.comm.ivrs.knowlarity;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.springframework.stereotype.Component;

import com.freecharge.common.comm.ivrs.IVRSService;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCConstants;

@Component
public class KnowlarityService extends IVRSService {

    @Override
    public boolean makeCall(Map<String, String> params) {

        long startTime = System.currentTimeMillis();
        boolean isNotificationSend = false;
        String url = fcProperties.getKnowlarityQucikcallUrl() + getQueryString(params);

        boolean shouldTimeOut = isIVRSCallPropertyEnabled(AppConfigService.IVRS_CALL_TIME_OUT_ENABLED);
        int timeOutValue = getIVRSCallProperty(AppConfigService.IVRS_CALL_TIME_OUT_VALUE);
        int connTimeout = getIVRSCallProperty(AppConfigService.IVRS_CALL_CONNECTION_TIME_OUT_VALUE);
        int soTimeout = getIVRSCallProperty(AppConfigService.IVRS_CALL_SO_TIME_OUT_VALUE);

        logger.info("Making knowlarity call: [" + url + "] for params : " + params + " with "
                + "shouldTimeOut : " + shouldTimeOut + ", timeOutValue : " + timeOutValue
                + ", connTimeout : " + connTimeout + ", soTimeout : " + soTimeout);

        int getStatus = 0;
        String knowlarityCallResponse = null;

        final HttpConnectionManagerParams knowlarityConnectionConfig = new HttpConnectionManagerParams();
        knowlarityConnectionConfig.setConnectionTimeout(connTimeout);
        knowlarityConnectionConfig.setSoTimeout(soTimeout);

        final SimpleHttpConnectionManager knowlarityConnectionManager = new SimpleHttpConnectionManager(true);
        knowlarityConnectionManager.setParams(knowlarityConnectionConfig);

        HttpClient client = new HttpClient(knowlarityConnectionManager);
        GetMethod getMethod = new GetMethod(url);

        try {

            getMethod.setRequestHeader("Content-Type", "application/json");
            getStatus = executeWithTimeout(client, getMethod, shouldTimeOut, timeOutValue);
            knowlarityCallResponse = getMethod.getResponseBodyAsString();

            if(HttpStatus.SC_OK == getStatus) {

                isNotificationSend = true;
                logger.info("Knowlarity response [" + url + "] is HTTP Code [" + getStatus + "] "
                        + " Response [" + knowlarityCallResponse + "] for params : " + params);

                metricsClient.recordEvent("IVRSService.", FCConstants.SUCCESS, "knowlairty");

            } else {
                logger.error(getErrorString(url, getStatus, knowlarityCallResponse, params));
                metricsClient.recordEvent("IVRSService", FCConstants.FAIL, "knowlairty");
            }

        } catch (IOException e) {
            logger.error(getErrorString(url, getStatus, knowlarityCallResponse, params), e);
            metricsClient.recordEvent("IVRSService", FCConstants.FAIL, "knowlairty");
            throw new RuntimeException(e);

        } catch (Exception e) {
            logger.error(getErrorString(url, getStatus, knowlarityCallResponse, params), e);
            metricsClient.recordEvent("IVRSService", FCConstants.FAIL, "knowlairty");

        } finally {
            getMethod.releaseConnection();
            long endTime = System.currentTimeMillis();
            metricsClient.recordLatency("IVRSService", "knowlairty", endTime - startTime);
        }

        return isNotificationSend;
    }

    private String getQueryString(Map<String, String> params) {
        String queryString = "username=<USERNAME>&password=<PASSWORD>&ivr_id=<IVR_ID>&format=json&is_transactional=yes&phone_book=<PHONE_NUMBER>";
        String phoneNumber = params.get(FCConstants.PHONE_NUMBER);
        String orderId = params.get(FCConstants.ORDER_ID);
        String userName = fcProperties.getKnowlarityUserName();
        String password = fcProperties.getKnowlarityPassword();
        String ivrId = String.valueOf(params.get("ivrId"));
        queryString = queryString.replace("<USERNAME>", userName);
        queryString = queryString.replace("<PASSWORD>", password);
        queryString = queryString.replace("<IVR_ID>", ivrId);
        queryString = queryString.replace("<PHONE_NUMBER>", phoneNumber + "," + orderId);
        return queryString;
    }
}
