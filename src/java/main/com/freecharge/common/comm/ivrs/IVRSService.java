package com.freecharge.common.comm.ivrs;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.timeout.HttpExecutorService;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.exception.NetworkConnectTimeoutException;
import com.freecharge.recharge.services.HttpMethodExecutorTask;

public abstract class IVRSService {

    protected final Logger logger = LoggingFactory.getLogger(getClass());
    protected final List<Integer> retriableHttpCodes = Arrays.asList(404, 500, 503);
    protected ExecutorService ivrsCallExecutor = Executors.newCachedThreadPool();

    @Autowired
    protected FCProperties fcProperties;
    @Autowired
    protected HttpExecutorService httpService;
    @Autowired
    protected HttpConnectionManager connectionManager;
    @Autowired
    protected AppConfigService appConfigService;
    @Autowired
    protected MetricsClient metricsClient;

    public abstract boolean makeCall(Map<String, String> params);

    protected int executeWithTimeout(final HttpClient client, final HttpMethod method, final boolean shouldTimeOut,
                                   final Integer timeOutValue) throws IOException {

        long startTime = System.currentTimeMillis();
        try {
            if (shouldTimeOut) {
                logger.info("IVRS call timeout is enabled for " + method.getURI());
                Future<Integer> notificationTask = ivrsCallExecutor.submit(new HttpMethodExecutorTask(client, method));
                try {
                    final Integer statusCode = notificationTask.get(timeOutValue, TimeUnit.MILLISECONDS);
                    if (statusCode != HttpStatus.SC_OK) {
                        logger.error("Received HTTP status code " + statusCode);
                        if (retriableHttpCodes.contains(statusCode)) {
                            throw new NetworkConnectTimeoutException();
                        }
                    }
                    return statusCode;
                } catch (TimeoutException te) {
                    logger.warn("TimeoutException while making IVRS call " + method.getURI()
                            + " [ " + method.getResponseBodyAsString() + " ]", te);
                    throw new ConnectTimeoutException();
                } catch (InterruptedException ie) {
                    logger.error("InterruptedException while making IVRS call " + method.getURI()
                            + " [ " + method.getResponseBodyAsString() + " ]", ie);
                    throw new ConnectTimeoutException();
                } catch (ExecutionException ee) {
                    logger.error("ExecutionException while making call IVRS call " + method.getURI()
                            + " [ " + method.getResponseBodyAsString() + " ]", ee);
                    final Throwable cause = ee.getCause();
                    if (cause != null) {
                        if (isConnectionFailure(cause)) {
                            throw new NetworkConnectTimeoutException();
                        }
                    }
                    throw new ConnectTimeoutException();
                } finally {
                    notificationTask.cancel(true);
                }
            } else {
                logger.info("IVRS timeout is disabled for " + method.getURI());
                return client.executeMethod(method);
            }
        } finally {
            long endTime = System.currentTimeMillis();
            metricsClient.recordLatency("IVRS.Execution", "Latency", endTime - startTime);
        }

    }

    private boolean isConnectionFailure(Throwable e) throws NetworkConnectTimeoutException {
        return (e instanceof SocketException || e instanceof ConnectException || e instanceof SocketTimeoutException);
    }

    protected boolean isIVRSCallPropertyEnabled(String property){
        JSONObject notificationConfig = appConfigService.getCustomProp(AppConfigService.IVRS_CALL_TIME_OUT_ENABLED);

        if(notificationConfig.containsKey(property)) {
            Boolean isEnabled = Boolean.parseBoolean(String.valueOf(notificationConfig.get(property)));

            if(isEnabled) {
                return true;
            }
        }

        return false;
    }

    protected int getIVRSCallProperty(String property){
        JSONObject notificationConfig = appConfigService.getCustomProp(AppConfigService.IVRS_NOTIFICATION_CONFIG);
        int propteryValue = 10000;

        if(notificationConfig.containsKey(property)) {
            propteryValue = Integer.parseInt(String.valueOf(notificationConfig.get(property)));
        }

        return propteryValue;
    }

    protected String getErrorString(String url, int postStatus, String ivrsCallResponse, Map<String, String> postData){
        return "IVRS call failure [" + url + "] HTTP Code [" + postStatus + "]. "
                + "Post Response [" + ivrsCallResponse + "] for postData : " + postData;
    }


}
