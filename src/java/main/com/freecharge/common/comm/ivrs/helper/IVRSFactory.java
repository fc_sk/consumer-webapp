package com.freecharge.common.comm.ivrs.helper;


import com.freecharge.common.comm.ivrs.IVRSService;
import com.freecharge.common.comm.ivrs.knowlarity.KnowlarityService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service
public class IVRSFactory {

    @Autowired
    private KnowlarityService knowlarityService;

    public IVRSService getIVRSClient(String enabledIVRSName) {
        switch (enabledIVRSName) {
            case "knowlarity":
                return knowlarityService;
            default:
                return null;
        }
    }
}
