package com.freecharge.common.amazon.s3.service;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.Region;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCUtil;
import com.google.common.base.Optional;

@Service
public class AmazonS3Service {

    private static Logger  logger = LoggingFactory.getLogger(AmazonS3Service.class);

    private AmazonS3Client amazonS3Client;

    private AWSCredentials awsCredentials;

    @Autowired
    private FCProperties   fcProperties;

    public AmazonS3Service(String accessKey, String secretKey) {
        awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        amazonS3Client = new AmazonS3Client(awsCredentials);
        amazonS3Client.setRegion(com.amazonaws.regions.Region.getRegion(Regions.AP_SOUTH_1));
    }

    public Optional<PutObjectResult> uploadRegularFileToS3(File file, String bucketName) throws IOException {
        if (file != null) {
            logger.info("Uploading file " + file.getName() + " to bucket " + bucketName);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(file.length());
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, file.getName(), file);
            return Optional.of(amazonS3Client.putObject(putObjectRequest));
        }
        return null;
    }

    public Optional<S3ObjectInputStream> getFileFromS3(String fileName, String bucketName) {
        if (FCUtil.isNotEmpty(fileName)) {
            logger.info("Getting file " + fileName + " from bucket " + bucketName);
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, fileName);
            S3Object s3Object = amazonS3Client.getObject(getObjectRequest);
            if (s3Object != null) {
                return Optional.of(s3Object.getObjectContent());
            } else {
                return null;
            }
        }
        return null;
    }
}
