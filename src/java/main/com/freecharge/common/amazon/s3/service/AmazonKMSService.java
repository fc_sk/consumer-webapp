package com.freecharge.common.amazon.s3.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.kms.AWSKMSClient;
import com.amazonaws.services.kms.AWSKMSClientBuilder;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.amazonaws.services.kms.model.DecryptResult;
import com.amazonaws.services.kms.model.EncryptRequest;
import com.amazonaws.services.kms.model.EncryptResult;
import com.amazonaws.util.IOUtils;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * A wrapper class for the Amazon KMS client.
 *
 */
@Service
public class AmazonKMSService {

    private AWSKMSClient   amazonKMSClient;

    private static Logger  logger         = LoggingFactory.getLogger(AmazonKMSService.class);

    private AWSCredentials awsCredentials;

    private final String   AWS_KMS_KEY    = "AKIAJMPVA5CAKABCQY2Q";

    private final String   AWS_KMS_SECRET = "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";

    public void init() {
        awsCredentials = new BasicAWSCredentials(AWS_KMS_KEY, AWS_KMS_SECRET);
        amazonKMSClient = (AWSKMSClient) AWSKMSClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).withRegion(Regions.AP_SOUTHEAST_1).build();
    };

    /**
     * Get the plain text for the given file. This function contacts the KMS to
     * get the plain text from the given file contents.
     * 
     * @param fileName
     *            File with the cipher text.
     * @return Plain text on success, else null
     */
    public String getFileContents(String fileName) {
        try {
            byte[] cipherText = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream(fileName));
            ByteBuffer cipherTextBlob = ByteBuffer.wrap(cipherText);
            DecryptRequest decryptReq = new DecryptRequest().withCiphertextBlob(cipherTextBlob);
            DecryptResult decryptResult = amazonKMSClient.decrypt(decryptReq);

            return new String(decryptResult.getPlaintext().array());
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    public void encryptFile(String inFile, String outFile) {
        try {
            Resource resource = new ClassPathResource(inFile);
            BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()), 1024);
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
            br.close();
            String fileContent = stringBuilder.toString();
            byte[] plainText = fileContent.getBytes();
            ByteBuffer plainTextBlob = ByteBuffer.wrap(plainText);
            String KeyId = "arn:aws:kms:ap-southeast-1:695210568016:key/655eddbd-fd12-4e50-a90c-7ac6129021f3";
            EncryptRequest enReq = new EncryptRequest().withKeyId(KeyId).withPlaintext(plainTextBlob);

            EncryptResult enRes = amazonKMSClient.encrypt(enReq);

            File file = new File(outFile);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileChannel wChannel = new FileOutputStream(file).getChannel();
            ByteBuffer cipherText = enRes.getCiphertextBlob();
            wChannel.write(cipherText);
            wChannel.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
        }
    }

    public static void main(String[] args) {
        AmazonKMSService kmsService = new AmazonKMSService();
        kmsService.init();

        // Load the common.passwords from the class path (webapp/conf/)
        String fileContents = kmsService.getFileContents("prod.passwords");

        /*String inFile = "e2e.passwords";
        String outFile = "/home/abhiuppala/webapp/conf/prod.passwords";*/

//        kmsService.encryptFile(inFile, outFile);
        System.out.println(fileContents);
        
        System.out.println("Execution completed");
    }

}
