package com.freecharge.common.businessdo;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.freecharge.api.coupon.service.web.model.VoucherCat;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA. User: Toshiba Date: May 9, 2012 Time: 11:46:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class HomeBusinessDo extends BaseBusinessDO {

	private String mobileNumber;
	private String amount;
	private String operator;
	private String circle;
	private List<Map<String, Object>> operatorList;
	private String type;
	private Integer txnHomePageId;
	private List<String> itemIdArr;
	private Integer remainingBalance = 0;
	private Integer couponWorthAmt = 0;
	private Integer tabId = 0;
	private String displayServiceNo;
	private String displayOperatorName;
	private Integer displayAmount;
	private String dthNumber;
	private String dthAmount;
	private String dataCardNumber;
	private String dataCardAmount;
	private Integer noOfCoupons=0;
	private Integer payableAmount=0;
	private List<StateMaster> stateMaster;
	private List<VoucherCat> categories;
	private List circleList;
	private List<String> rechargePlanNameList;
	

	public List<VoucherCat> getCategories() {
		return categories;
	}

	public void setCategories(List<VoucherCat> categories) {
        //This is done so that the voucher categories in the front end are in alphabetical order
        Collections.sort(categories, new VoucherCat.CategoryDetailComparator());
		this.categories = categories;
	}

	public Integer getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(Integer payableAmount) {
		this.payableAmount = payableAmount;
	}

	public Integer getNoOfCoupons() {
		return noOfCoupons;
	}

	public void setNoOfCoupons(Integer noOfCoupons) {
		this.noOfCoupons = noOfCoupons;
	}

	public Integer getDisplayAmount() {
		return displayAmount;
	}

	public void setDisplayAmount(Integer displayAmount) {
		this.displayAmount = displayAmount;
	}

	public String getDisplayServiceNo() {
		return displayServiceNo;
	}

	public void setDisplayServiceNo(String displayServiceNo) {
		this.displayServiceNo = displayServiceNo;
	}

	public String getDisplayOperatorName() {
		return displayOperatorName;
	}

	public void setDisplayOperatorName(String displayOperatorName) {
		this.displayOperatorName = displayOperatorName;
	}

	public Integer getTabId() {
		return tabId;
	}

	public void setTabId(Integer tabId) {
		this.tabId = tabId;
	}

	public Integer getCouponWorthAmt() {
		return couponWorthAmt;
	}

	public void setCouponWorthAmt(Integer couponWorthAmt) {
		this.couponWorthAmt = couponWorthAmt;
	}

	public Integer getRemainingBalance() {
		return remainingBalance;
	}

	public void setRemainingBalance(Integer remainingBalance) {
		this.remainingBalance = remainingBalance;
	}

	public List<String> getItemIdArr() {
		return itemIdArr;
	}

	public void setItemIdArr(List<String> itemIdArr) {
		this.itemIdArr = itemIdArr;
	}

	public Integer getTxnHomePageId() {
		return txnHomePageId;
	}

	public void setTxnHomePageId(Integer txnHomePageId) {
		this.txnHomePageId = txnHomePageId;
	}

	private int productMasterId;

	public String getDthNumber() {
		return dthNumber;
	}

	public void setDthNumber(String dthNumber) {
		this.dthNumber = dthNumber;
	}

	

	public String getDthAmount() {
		return dthAmount;
	}

	public void setDthAmount(String dthAmount) {
		this.dthAmount = dthAmount;
	}

	public String getDataCardNumber() {
		return dataCardNumber;
	}

	public void setDataCardNumber(String dataCardNumber) {
		this.dataCardNumber = dataCardNumber;
	}

	public String getDataCardAmount() {
		return dataCardAmount;
	}

	public void setDataCardAmount(String dataCardAmount) {
		this.dataCardAmount = dataCardAmount;
	}

	public int getProductMasterId() {
		return productMasterId;
	}

	public void setProductMasterId(int productMasterId) {
		this.productMasterId = productMasterId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getCircle() {
		return circle;
	}

	public void setCircle(String circle) {
		this.circle = circle;
	}

	public List<Map<String, Object>> getOperatorList() {
		return operatorList;
	}

	public void setOperatorList(List<Map<String, Object>> operatorList) {
		this.operatorList = operatorList;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<StateMaster> getStateMaster() {
		return stateMaster;
	}
	public void setStateMaster(List<StateMaster> stateMaster) {
		this.stateMaster = stateMaster;
	}

	public List getCircleList() {
		return circleList;
	}

	public void setCircleList(List circleList) {
		this.circleList = circleList;
	}

	public List<String> getRechargePlanNameList() {
		return rechargePlanNameList;
	}

	public void setRechargePlanNameList(List<String> rechargePlanNameList) {
		this.rechargePlanNameList = rechargePlanNameList;
	}

}