package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: Apr 12, 2012
 * Time: 4:09:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class PGTransactionRequestBusinessDo extends BaseWebDO {
    private Integer pgTransRequestId;
    private String pgTransRequestName;

    public Integer getPgTransRequestId() {
        return pgTransRequestId;
    }

    public void setPgTransRequestId(Integer pgTransRequestId) {
        this.pgTransRequestId = pgTransRequestId;
    }

    public String getPgTransRequestName() {
        return pgTransRequestName;
    }

    public void setPgTransRequestName(String pgTransRequestName) {
        this.pgTransRequestName = pgTransRequestName;
    }
}
