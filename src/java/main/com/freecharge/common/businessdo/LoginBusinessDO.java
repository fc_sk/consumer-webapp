package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: Manoj Kumar
 * Date: Apr 30, 2012
 * Time: 12:19:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoginBusinessDO extends BaseBusinessDO{
    
    /**
     * Enum to encapsulate different
     * types of users of FreeCharge.
     * As of now there are just two.
     * <ul>
     * <li> <i>Buyer:</i> One who uses FreeCharge for recharging mobile phones, paying electricity bills etc., </li>
     * <li> <i>Merchant: </i> One who uses FreeCharge to redeem coupon, create offers etc., </li> 
     * </ul>
     * 
     * @author arun
     *
     */
    public static enum UserType {
        Buyer, Merchant;

        /*
         * Some randomly chosen prime number.
         */
        private static final int MERCHANT_TYPE_VALUE = 23;
        
        /**
         * Convert DB values used to represent
         * various types to {@link UserType};
         * @return
         */
        public static UserType fromDB(Integer type) {
            if (type == null) {
                /*
                 * Default dude is a buyer.
                 */
                return Buyer;
            }
            
            if (type.intValue() == MERCHANT_TYPE_VALUE) {
                return Merchant;
            } else {
                return Buyer;
            }
        }
    }
    
    private String email;
    private String password;
    private String action;
    private boolean login;
    private String rememberMe;
    private String userId;
    private String firstName;
    private UserType userType;
    private Integer affiliateProfileId;
    
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

	public String getRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(String rememberMe) {
		this.rememberMe = rememberMe;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Integer getAffiliateProfileId() {
        return affiliateProfileId;
    }
    public void setAffiliateProfileId(Integer affiliateProfileId) {
        this.affiliateProfileId = affiliateProfileId;
    }
 
}