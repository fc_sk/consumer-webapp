package com.freecharge.common.businessdo;

import java.util.List;
import java.util.Map;

import com.freecharge.api.coupon.service.web.model.CouponVO;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class VouchersBusinessDO extends BaseBusinessDO {
	private	Map<String, List<CouponVO>> voucherData;
	private List<Integer> voucherIdList;
	
	public void setVoucherData(Map<String, List<CouponVO>> voucherData) {
		this.voucherData = voucherData;
	}

	public Map<String, List<CouponVO>> getVoucherData() {
		return voucherData;
	}

	public List<Integer> getVoucherIdList() {
		return voucherIdList;
	}

	public void setVoucherIdList(List<Integer> voucherIdList) {
		this.voucherIdList = voucherIdList;
	}

}
