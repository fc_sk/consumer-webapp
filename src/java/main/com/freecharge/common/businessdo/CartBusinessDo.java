package com.freecharge.common.businessdo;

import java.util.Date;
import java.util.List;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: May 11, 2012
 * Time: 10:33:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class CartBusinessDo extends BaseBusinessDO {
    private Integer orderId;
    private Integer userId;
    private Boolean isOpen;
    private Date createdOn;
    private Date expiresOn;
    private Integer finalStatus;

    private List<CartItemsBusinessDO> cartItems;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(Date expiresOn) {
        this.expiresOn = expiresOn;
    }

    public Integer getFinalStatus() {
        return finalStatus;
    }

    public void setFinalStatus(Integer finalStatus) {
        this.finalStatus = finalStatus;
    }

    public List<CartItemsBusinessDO> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItemsBusinessDO> cartItems) {
        this.cartItems = cartItems;
    }

    @Override
    public String toString() {
        return "CartBusinessDo{" +
                "orderId=" + orderId +
                ", userId=" + userId +
                ", isOpen=" + isOpen +
                ", createdOn=" + createdOn +
                ", expiresOn=" + expiresOn +
                ", finalStatus=" + finalStatus +
                ", cartItems=" + cartItems +
                '}';
    }
}
