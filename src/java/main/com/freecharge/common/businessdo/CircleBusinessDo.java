package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: Apr 11, 2012
 * Time: 3:41:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class CircleBusinessDo extends BaseBusinessDO {
    private Integer circleId;
    private String circleName;

    public Integer getCircleId() {
        return circleId;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }
}
