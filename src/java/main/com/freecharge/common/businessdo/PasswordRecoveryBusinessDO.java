package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 10, 2012
 * Time: 11:43:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class PasswordRecoveryBusinessDO extends BaseBusinessDO{
    private String newPassword;
    private String confirmPassword;
    private String email;
    private String userId;
    private String status;
    private boolean userExist;
    private String encryptValue;

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getStatus() {
           return status;
    }

    public void setStatus(String status) {
           this.status = status;
       }

       public String getEmail() {
           return email;
       }

       public void setEmail(String email) {
           this.email = email;
       }

       public String getNewPassword() {
           return newPassword;
       }

       public void setNewPassword(String newPassword) {
           this.newPassword = newPassword;
       }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean getUserExist() {
        return userExist;
    }

    public void setUserExist(boolean userExist) {
        this.userExist = userExist;
    }

	public String getEncryptValue() {
		return encryptValue;
	}

	public void setEncryptValue(String encryptValue) {
		this.encryptValue = encryptValue;
	}
}
