package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: Apr 12, 2012
 * Time: 3:53:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class BankServiceBusinessDo extends BaseBusinessDO {
    public Integer serviceId;
    public String serviceName;

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
