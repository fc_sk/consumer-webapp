package com.freecharge.common.businessdo;

import com.freecharge.api.coupon.service.web.model.CouponStore;
import com.freecharge.app.domain.entity.jdbc.Crosssell;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: Manoj Kumar
 * Date: May 11, 2012
 * Time: 11:37:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class CrosssellBusinessDo extends BaseBusinessDO{

	private String category;
    private Integer price;
    private String title;
	private String order;
	private boolean refundable;
	private String imgurl;	
	private String tnc;
	private String description;
	private String headDescription;
    private String primaryproduct;
    private Integer primaryproductamount;
    private String othercondition;
    private Integer maxuses;
    private Integer threshold;
    private Boolean uniqueCode = false;
    private Integer storeId;
    private String className;
    private String template;
    private String mailTnc;
    private String mailSubject;
    private String mailImgUrl;
    private Crosssell crosssell;
    private CouponStore couponStore;
    
    public String getMailTnc() {
		return mailTnc;
	}

	public void setMailTnc(String mailTnc) {
		this.mailTnc = mailTnc;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getMailImgUrl() {
		return mailImgUrl;
	}

	public void setMailImgUrl(String mailImgUrl) {
		this.mailImgUrl = mailImgUrl;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getHeadDescription() {
		return headDescription;
	}

	public void setHeadDescription(String headDescription) {
		this.headDescription = headDescription;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public boolean isRefundable() {
        return refundable;
    }

    public void setRefundable(boolean refundable) {
        this.refundable = refundable;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getTnc() {
        return tnc;
    }

    public void setTnc(String tnc) {
        this.tnc = tnc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrimaryproduct() {
        return primaryproduct;
    }

    public void setPrimaryproduct(String primaryproduct) {
        this.primaryproduct = primaryproduct;
    }

    public Integer getPrimaryproductamount() {
        return primaryproductamount;
    }

    public void setPrimaryproductamount(Integer primaryproductamount) {
        this.primaryproductamount = primaryproductamount;
    }

    public String getOthercondition() {
        return othercondition;
    }

    public void setOthercondition(String othercondition) {
        this.othercondition = othercondition;
    }

    public Integer getMaxuses() {
        return maxuses;
    }

    public void setMaxuses(Integer maxuses) {
        this.maxuses = maxuses;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

    public Boolean getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(Boolean uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

	public Crosssell getCrosssell() {
		return crosssell;
	}

	public void setCrosssell(Crosssell crosssell) {
		this.crosssell = crosssell;
	}

	public CouponStore getCouponStore() {
		return couponStore;
	}

	public void setCouponStore(CouponStore couponStore) {
		this.couponStore = couponStore;
	}
}
