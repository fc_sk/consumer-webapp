package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class TestBusinessDO extends BaseBusinessDO {

    private Integer one;
    private Integer two;
    private Integer constants = 200;
    private Integer constants2 = 400;
    private String dbName ;

    public Integer getOne() {
        return one;
    }

    public void setOne(Integer one) {
        this.one = one;
    }

    public Integer getTwo() {
        return two;
    }

    public void setTwo(Integer two) {
        this.two = two;
    }

    public Integer getConstants() {
        return constants;
    }

    public void setConstants(Integer constants) {
        this.constants = constants;
    }

    public Integer getConstants2() {
        return constants2;
    }

    public void setConstants2(Integer constants2) {
        this.constants2 = constants2;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
