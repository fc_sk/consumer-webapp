package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: Jun 9, 2012
 * Time: 3:07:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class CartCrosssellBusinessDO extends BaseBusinessDO{
    private Integer txnHomePageId;
    private Integer cartId;
    private Integer crosssellId;
    private Float crosssellAmount;
    private Integer productTypeId;
    private Integer txncrossellId;
    private Integer cartItemsId;
    private String title;

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public Integer getCrosssellId() {
        return crosssellId;
    }

    public void setCrosssellId(Integer crosssellId) {
        this.crosssellId = crosssellId;
    }

    public Float getCrosssellAmount() {
        return crosssellAmount;
    }

    public void setCrosssellAmount(Float crosssellAmount) {
        this.crosssellAmount = crosssellAmount;
    }

    public Integer getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Integer getTxncrossellId() {
        return txncrossellId;
    }

    public void setTxncrossellId(Integer txncrossellId) {
        this.txncrossellId = txncrossellId;
    }

    public Integer getCartItemsId() {
        return cartItemsId;
    }

    public void setCartItemsId(Integer cartItemsId) {
        this.cartItemsId = cartItemsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
