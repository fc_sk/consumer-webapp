package com.freecharge.common.businessdo;

import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.PaymentTypeOptions;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA. User: user Date: May 5, 2012 Time: 2:31:06 PM To change this template use File | Settings | File Templates.
 */
public class PaymentOptionListBusinessDO extends BaseBusinessDO {
	private List<Map<String, Object>> paymentOptionsList;
	private String txnFulfilmentStatus;
	private Integer productMasterId;

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	private String paymentType;

	public List<PaymentTypeOptions> getPaymentTypeOptionsList() {
		return paymentTypeOptionsList;
	}

	public void setPaymentTypeOptionsList(List<PaymentTypeOptions> paymentTypeOptionsList) {
		this.paymentTypeOptionsList = paymentTypeOptionsList;
	}

	private List<PaymentTypeOptions> paymentTypeOptionsList;

	public List<Map<String, Object>> getPaymentOptionsList() {
		return paymentOptionsList;
	}

	public void setPaymentOptionsList(List<Map<String, Object>> paymentOptionsList) {
		this.paymentOptionsList = paymentOptionsList;
	}

	public String getTxnFulfilmentStatus() {
		return txnFulfilmentStatus;
	}

	public void setTxnFulfilmentStatus(String txnFulfilmentStatus) {
		this.txnFulfilmentStatus = txnFulfilmentStatus;
	}

	public Integer getProductMasterId() {
		return productMasterId;
	}

	public void setProductMasterId(Integer productMasterId) {
		this.productMasterId = productMasterId;
	}
}
