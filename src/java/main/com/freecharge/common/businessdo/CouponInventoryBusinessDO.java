package com.freecharge.common.businessdo;

import java.util.List;
import java.util.Map;

import com.freecharge.common.framework.basedo.BaseBusinessDO;


public class CouponInventoryBusinessDO extends BaseBusinessDO {
	
	private static final long serialVersionUID = 1L;

	private String orderId;
	private List<VouchersQuantityBusinessDO> couponInventoryList;
	private Map<String, List<String>> couponCodes;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public List<VouchersQuantityBusinessDO> getCouponInventoryList() {
		return couponInventoryList;
	}
	public void setCouponInventoryList(
			List<VouchersQuantityBusinessDO> couponInventoryList) {
		this.couponInventoryList = couponInventoryList;
	}
	public Map<String, List<String>> getCouponCodes() {
		return couponCodes;
	}
	public void setCouponCodes(Map<String, List<String>> couponCodes) {
		this.couponCodes = couponCodes;
	}
}
