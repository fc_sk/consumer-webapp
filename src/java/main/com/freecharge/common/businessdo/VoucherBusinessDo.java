package com.freecharge.common.businessdo;

import java.util.List;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: Apr 12, 2012
 * Time: 3:26:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class VoucherBusinessDo extends BaseBusinessDO {
    private Integer voucherId;
    private String voucherName;
    private String voucherImgUrl;
    private Integer voucherAmount;
    private String voucherOrder;
    private String voucherType;
    private String voucherCat;
    private String termsConditions;
    private String voucherUrl;
    private Integer couponRemainingCount;
    private List<Object[]> voucherSet;
    private String cityNames;
    private Integer faceValue;

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public void setVoucherName(String voucherName) {
        this.voucherName = voucherName;
    }

    public String getVoucherImgUrl() {
        return voucherImgUrl;
    }

    public void setVoucherImgUrl(String voucherImgUrl) {
        this.voucherImgUrl = voucherImgUrl;
    }

    public Integer getVoucherAmount() {
        return voucherAmount;
    }

    public void setVoucherAmount(Integer voucherAmount) {
        this.voucherAmount = voucherAmount;
    }

    public String getVoucherOrder() {
        return voucherOrder;
    }

    public void setVoucherOrder(String voucherOrder) {
        this.voucherOrder = voucherOrder;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

     public String getVoucherCat() {
        return voucherCat;
    }

    public void setVoucherCat(String voucherCat) {
        this.voucherCat = voucherCat;
    }

    public String getTermsConditions() {
        return termsConditions;
    }

    public void setTermsConditions(String termsConditions) {
        this.termsConditions = termsConditions;
    }

    public String getVoucherUrl() {
        return voucherUrl;
    }

    public void setVoucherUrl(String voucherUrl) {
        this.voucherUrl = voucherUrl;
    }

    public Integer getCouponRemainingCount() {
        return couponRemainingCount;
    }

    public void setCouponRemainingCount(Integer couponRemainingCount) {
        this.couponRemainingCount = couponRemainingCount;
    }

    public List<Object[]> getVoucherSet() {
        return voucherSet;
    }

    public void setVoucherSet(List<Object[]> voucherSet) {
        this.voucherSet = voucherSet;
    }

    public String getCityNames() {
        return cityNames;
    }

    public void setCityNames(String cityNames) {
        this.cityNames = cityNames;
    }

    public Integer getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(Integer faceValue) {
        this.faceValue = faceValue;
    }
}
