package com.freecharge.common.businessdo;

import java.util.List;
import java.util.Map;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class MyRechargesBusinessDO extends BaseBusinessDO {
	private List<Map<String,String>> rechargeHistoryList;
	private String email;
	private Integer userid;
	private String channel;
	private Integer appVersion;
	private boolean returnPaymentTxns;
	public List<Map<String, String>> getRechargeHistoryList() {
		return rechargeHistoryList;
	}
	public void setRechargeHistoryList(List<Map<String, String>> rechargeHistoryList) {
		this.rechargeHistoryList = rechargeHistoryList;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
    public String getChannel() {
        return channel;
    }
    public void setChannel(String channel) {
        this.channel = channel;
    }
    public Integer getAppVersion() {
        return appVersion;
    }
    public void setAppVersion(Integer appVersion) {
        this.appVersion = appVersion;
    }
	public boolean isReturnPaymentTxns() {
		return returnPaymentTxns;
	}
	public void setReturnPaymentTxns(boolean returnPaymentTxns) {
		this.returnPaymentTxns = returnPaymentTxns;
	}
	
	
}
