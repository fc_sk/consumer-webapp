package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: Apr 12, 2012
 * Time: 3:31:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class MerchantBusinessDo extends BaseWebDO {
      private Integer merchantId;
      private String merchantName;
      private String merchantImgPath;
      private String merchantVoucherValue;
      private String merchantVoucherType;

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantImgPath() {
        return merchantImgPath;
    }

    public void setMerchantImgPath(String merchantImgPath) {
        this.merchantImgPath = merchantImgPath;
    }

    public String getMerchantVoucherValue() {
        return merchantVoucherValue;
    }

    public void setMerchantVoucherValue(String merchantVoucherValue) {
        this.merchantVoucherValue = merchantVoucherValue;
    }

    public String getMerchantVoucherType() {
        return merchantVoucherType;
    }

    public void setMerchantVoucherType(String merchantVoucherType) {
        this.merchantVoucherType = merchantVoucherType;
    }
}
