package com.freecharge.common.businessdo;

import java.util.List;

import com.freecharge.app.domain.entity.CountryMaster;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 23, 2012
 * Time: 4:08:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddressBusinessDO extends BaseBusinessDO{
    private String Address;
    private String city;
    private Integer cityId;
    private String postalCode;
    private String state;
    private String country;
    private Integer stateId;
    private Integer countryId;
    private String status;
    private Integer txnHomePageId;
    private String comboAddr;
    private Integer txnfulfilmentId;
    private Integer userProfileId;
    private Integer userRechargeContactId;
    private Integer userId;
    private String firstName;
    private String defaultProfile;
    private String actionParam;
    private String street;
    private String area;
    private String landmark;
    private String title; 
    private String isEdit;
    

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public String getComboAddr() {
        return comboAddr;
    }

    public void setComboAddr(String comboAddr) {
        this.comboAddr = comboAddr;
    }
    private List<StateMaster> stateMaster;
    private List<CountryMaster> countryMaster;

    public List<StateMaster> getStateMaster() {
        return stateMaster;
    }

    public void setStateMaster(List<StateMaster> stateMaster) {
        this.stateMaster = stateMaster;
    }

    public List<CountryMaster> getCountryMaster() {
        return countryMaster;
    }

    public void setCountryMaster(List<CountryMaster> countryMaster) {
        this.countryMaster = countryMaster;
    }

    public Integer getTxnfulfilmentId() {
        return txnfulfilmentId;
    }

    public void setTxnfulfilmentId(Integer txnfulfilmentId) {
        this.txnfulfilmentId = txnfulfilmentId;
    }

    public Integer getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(Integer userProfileId) {
        this.userProfileId = userProfileId;
    }

    public Integer getUserRechargeContactId() {
        return userRechargeContactId;
    }

    public void setUserRechargeContactId(Integer userRechargeContactId) {
        this.userRechargeContactId = userRechargeContactId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public String getActionParam() {
        return actionParam;
    }

    public void setActionParam(String actionParam) {
        this.actionParam = actionParam;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
}
