package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: Apr 11, 2012
 * Time: 4:21:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class AggregatorBusinessDo extends BaseBusinessDO {
    private Integer aggregatorId;
    private String aggregatorName;

    public Integer getAggregatorId() {
        return aggregatorId;
    }

    public void setAggregatorId(Integer aggregatorId) {
        this.aggregatorId = aggregatorId;
    }

    public String getAggregatorName() {
        return aggregatorName;
    }

    public void setAggregatorName(String aggregatorName) {
        this.aggregatorName = aggregatorName;
    }
}
