package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class PaymentTypeMasterBusinessDO extends BaseBusinessDO {

    private String name;
    private String code;
    private Integer relationId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getRelationId() {
        return relationId;
    }

    public void setRelationId(Integer relationId) {
        this.relationId = relationId;
    }
    
    @Override
    public String toString() {
    	return name;
    }
}
