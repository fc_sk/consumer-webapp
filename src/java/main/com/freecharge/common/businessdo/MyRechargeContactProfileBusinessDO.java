package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class MyRechargeContactProfileBusinessDO extends BaseBusinessDO {

	private static final long serialVersionUID = 1L;

	private Integer userId;
	private Integer userProfileId;
	private String rechargeContactNo;
	private String address1;
	private String landmark;
	private String city;
	private String state;
	private String pincode;
	private String area;
	private String title;
	private String name;
	private String email;
	private String country;
	private String nickAlias;
	private String circle;
	private String operatorCode;
	private String product;
	private Double lastRechargeAmount;
	private Integer userRechargeContactId;
	
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(Integer userProfileId) {
		this.userProfileId = userProfileId;
	}
	public String getRechargeContactNo() {
		return rechargeContactNo;
	}
	public void setRechargeContactNo(String rechargeContactNo) {
		this.rechargeContactNo = rechargeContactNo;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getNickAlias() {
		return nickAlias;
	}
	public void setNickAlias(String nickAlias) {
		this.nickAlias = nickAlias;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getOperatorCode() {
		return operatorCode;
	}
	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Double getLastRechargeAmount() {
		return lastRechargeAmount;
	}
	public void setLastRechargeAmount(Double lastRechargeAmount) {
		this.lastRechargeAmount = lastRechargeAmount;
	}
	public Integer getUserRechargeContactId() {
		return userRechargeContactId;
	}
	public void setUserRechargeContactId(Integer userRechargeContactId) {
		this.userRechargeContactId = userRechargeContactId;
	}
}
