package com.freecharge.common.businessdo;

import java.util.List;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class MyContactsListBusinessDo extends BaseBusinessDO{

	private Integer userid;
	private String email;
	private List<MyContactsBussinessDO> contactslist;
	private List operatorlist;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<MyContactsBussinessDO> getContactslist() {
		return contactslist;
	}
	public void setContactslist(List<MyContactsBussinessDO> contactslist) {
		this.contactslist = contactslist;
	}
	public List getOperatorlist() {
		return operatorlist;
	}
	public void setOperatorlist(List operatorlist) {
		this.operatorlist = operatorlist;
	}
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	
	
}
