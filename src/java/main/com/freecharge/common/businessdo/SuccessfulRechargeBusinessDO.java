package com.freecharge.common.businessdo;

import java.util.List;

import com.freecharge.app.domain.entity.jdbc.RechargeDetails;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class SuccessfulRechargeBusinessDO extends BaseBusinessDO {

	private static final long serialVersionUID = 1L;
	private List<RechargeDetails> successfulRecharges;
	private String userEmail;
	private List<String> successOrderIds;
	public List<RechargeDetails> getSuccessfulRecharges() {
		return successfulRecharges;
	}
	public void setSuccessfulRecharges(List<RechargeDetails> successfulRecharges) {
		this.successfulRecharges = successfulRecharges;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public List<String> getSuccessOrderIds() {
		return successOrderIds;
	}
	public void setSuccessOrderIds(List<String> successOrderIds) {
		this.successOrderIds = successOrderIds;
	}
}
