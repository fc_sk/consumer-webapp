package com.freecharge.common.businessdo;

import java.util.List;
import java.util.Map;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA. User: user Date: May 23, 2012 Time: 12:08:46 PM To change this template use File | Settings | File Templates.
 */
public class OperatorCircleBusinessDO extends BaseBusinessDO {
	private String prefix;
	private String productType;
	private List<Map<String, Object>> AllOperatorsList;
	private List<Map<String, Object>> prefixData;

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public  List<Map<String, Object>> getPrefixData() {
		return prefixData;
	}

	public void setPrefixData( List<Map<String, Object>> prefixData) {
		this.prefixData = prefixData;
	}

	public List<Map<String, Object>> getAllOperatorsList() {
		return AllOperatorsList;
	}

	public void setAllOperatorsList(List<Map<String, Object>> allOperatorsList) {
		AllOperatorsList = allOperatorsList;
	}

}
