package com.freecharge.common.businessdo;


import java.util.Date;
import java.util.List;

import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * @author CST 1
 *
 */
public class ProfileBusinessDO extends BaseBusinessDO{
	
	private Integer userid;
	private String title;
	private String name;
	private String email;
	private String oldpassword;
	private String newpassword;
	private String confirmpassword;
	private String area;
	private String address;
	private String landmar;
	private String pincode;
	private String state;
	private String city;
	private List<StateMaster> statelist;
	private List<CityMaster> citylist;
	private boolean ispasswordupdate=false;
	private boolean isoldpswdcorrect=false;
	private Integer userProfileId;
	private String morf;
	private Date dob;
	private String dobstring;
	private String mobileNo;
	private boolean isDefaultProfile=false;
	
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitel(String titel) {
		this.title = titel;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOldpassword() {
		return oldpassword;
	}
	public void setOldpassword(String oldpassword) {
		this.oldpassword = oldpassword;
	}
	public String getNewpassword() {
		return newpassword;
	}
	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}
	public String getConfirmpassword() {
		return confirmpassword;
	}
	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLandmar() {
		return landmar;
	}
	public void setLandmar(String landmar) {
		this.landmar = landmar;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public List<StateMaster> getStatelist() {
		return statelist;
	}
	public void setStatelist(List<StateMaster> statelist) {
		this.statelist = statelist;
	}
	public List<CityMaster> getCitylist() {
		return citylist;
	}
	public void setCitylist(List<CityMaster> citylist) {
		this.citylist = citylist;
	}
	public boolean isIspasswordupdate() {
		return ispasswordupdate;
	}
	public void setIspasswordupdate(boolean ispasswordupdate) {
		this.ispasswordupdate = ispasswordupdate;
	}
	public boolean isIsoldpswdcorrect() {
		return isoldpswdcorrect;
	}
	public void setIsoldpswdcorrect(boolean isoldpswdcorrect) {
		this.isoldpswdcorrect = isoldpswdcorrect;
	}
	public Integer getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(Integer userProfileId) {
		this.userProfileId = userProfileId;
	}
	public String getMorf() {
		return morf;
	}
	public void setMorf(String morf) {
		this.morf = morf;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getDobstring() {
		return dobstring;
	}
	public void setDobstring(String dobstring) {
		this.dobstring = dobstring;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public boolean isIsDefaultProfile() {
		return isDefaultProfile;
	}
	public void setIsDefaultProfile(boolean isDefaultProfile) {
		this.isDefaultProfile = isDefaultProfile;
	}
   	
	

}
