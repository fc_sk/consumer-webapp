package com.freecharge.common.businessdo;

import java.util.Date;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class CartItemsBusinessDO extends BaseBusinessDO {

    private Integer itemId;
    private String entityId;
    private Integer productMasterId;
    private String displayLabel;
    private Double price;
    private Date addedOn;
    private Date deletedOn;
    private Boolean isDeleted;
    private Integer quantity;


    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDisplayLabel() {
        return displayLabel;
    }

    public void setDisplayLabel(String displayLabel) {
        this.displayLabel = displayLabel;
    }

    public Integer getProductMasterId() {
        return productMasterId;
    }

    public void setProductMasterId(Integer productMasterId) {
        this.productMasterId = productMasterId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    @Override
    public String toString() {
        return "CartItemsBusinessDO{" +
                "itemId=" + itemId +
                ", entityId='" + entityId + '\'' +
                ", productMasterId=" + productMasterId +
                ", displayLabel='" + displayLabel + '\'' +
                ", price=" + price +
                ", addedOn=" + addedOn +
                ", deletedOn=" + deletedOn +
                ", isDeleted=" + isDeleted +
                ", quantity=" + quantity +
                '}';
    }
}
