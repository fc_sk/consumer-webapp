package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class VouchersQuantityBusinessDO extends BaseBusinessDO {
	
	private static final long serialVersionUID = 1L;

	private Integer couponStoreId;
	private Integer quantity;
	private Boolean isMCoupon;
	private Integer merchantId;
	
	private boolean isComplimentary;
	private boolean isUnique;
	
	public Integer getCouponStoreId() {
		return couponStoreId;
	}
	public void setCouponStoreId(Integer couponStoreId) {
		this.couponStoreId = couponStoreId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public boolean isComplimentary() {
		return isComplimentary;
	}
	public void setComplimentary(boolean isComplimentary) {
		this.isComplimentary = isComplimentary;
	}
	public boolean isUnique() {
		return isUnique;
	}
	public void setUnique(boolean isUnique) {
		this.isUnique = isUnique;
	}
	
    public Boolean getIsMCoupon() {
    	if (isMCoupon == null){
    		return false;
    	}
        return isMCoupon;
    }
    public void setIsMCoupon(Boolean isMCoupon) {
        this.isMCoupon = isMCoupon;
    }
    
    public Integer getMerchantId() {
        return merchantId;
    }
    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }
	

}
