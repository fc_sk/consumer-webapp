package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: Apr 12, 2012
 * Time: 3:51:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class PaymentGatewayBusinessDo extends BaseWebDO {
    private Integer pgId;
    private String pgName;

    public Integer getPgId() {
        return pgId;
    }

    public void setPgId(Integer pgId) {
        this.pgId = pgId;
    }

    public String getPgName() {
        return pgName;
    }

    public void setPgName(String pgName) {
        this.pgName = pgName;
    }
}
