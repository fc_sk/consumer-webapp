package com.freecharge.common.businessdo;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.CountryMaster;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 7, 2012
 * Time: 5:03:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyProfileBusinessDO extends BaseBusinessDO{

    private Integer userId;
	private Integer fkAffiliateProfileId;
	private Integer type;
	private String email;
	private String password;
	private String mobileNo;
	private String dob;
    private Integer userProfileId;
	private Integer fkUserId;
	private Integer fkUserRechargeContactId;
	private String nickAlias;
    private String firstName;
    private Integer stateId;
    private CartBusinessDo cartBusinessDo;
    private Integer totalRecords;
    private Integer pageNo;
    private List<CrosssellBusinessDo> crosssellBusinessDos;
    private String address1;
	private String landmark;
    private String serviceNumber;
    private List<Map<String,String>> rechargeHistoryList;
    private Integer cityId;
	private String postalCode;
	private Integer fkStateMasterId;
	private Integer fkCountryMasterId;
	private Boolean isDefault;
	private Timestamp createdOn;
    private Boolean isActive;
	private Timestamp dateAdded;
	private Timestamp lastLoggedin;
	private Boolean isLoggedin;
	private String morf;
	private Timestamp lastUpdate;
	private String forgotPasswordKey;
	private Date forgotPasswordExpiry;
	private Integer fkRefferredByUserId;

	
    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private List<StateMaster> stateMaster;
    private List<CountryMaster> countryMaster;    

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public List<Map<String, String>> getRechargeHistoryList() {
        return rechargeHistoryList;
    }

    public void setRechargeHistoryList(List<Map<String, String>> rechargeHistoryList) {
        this.rechargeHistoryList = rechargeHistoryList;
    }

    public Integer getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(Integer userProfileId) {
        this.userProfileId = userProfileId;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    public Integer getFkCountryMasterId() {
        return fkCountryMasterId;
    }

    public void setFkCountryMasterId(Integer fkCountryMasterId) {
        this.fkCountryMasterId = fkCountryMasterId;
    }

    public Integer getFkStateMasterId() {
        return fkStateMasterId;
    }

    public void setFkStateMasterId(Integer fkStateMasterId) {
        this.fkStateMasterId = fkStateMasterId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getNickAlias() {
        return nickAlias;
    }

    public void setNickAlias(String nickAlias) {
        this.nickAlias = nickAlias;
    }

    public Integer getFkUserRechargeContactId() {
        return fkUserRechargeContactId;
    }

    public void setFkUserRechargeContactId(Integer fkUserRechargeContactId) {
        this.fkUserRechargeContactId = fkUserRechargeContactId;
    }

    public Integer getFkUserId() {
        return fkUserId;
    }

    public void setFkUserId(Integer fkUserId) {
        this.fkUserId = fkUserId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFkAffiliateProfileId() {
        return fkAffiliateProfileId;
    }

    public void setFkAffiliateProfileId(Integer fkAffiliateProfileId) {
        this.fkAffiliateProfileId = fkAffiliateProfileId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Timestamp getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Timestamp dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Timestamp getLastLoggedin() {
        return lastLoggedin;
    }

    public void setLastLoggedin(Timestamp lastLoggedin) {
        this.lastLoggedin = lastLoggedin;
    }

    public Boolean getLoggedin() {
        return isLoggedin;
    }

    public void setLoggedin(Boolean loggedin) {
        isLoggedin = loggedin;
    }

    public String getMorf() {
        return morf;
    }

    public void setMorf(String morf) {
        this.morf = morf;
    }

    public String getForgotPasswordKey() {
        return forgotPasswordKey;
    }

    public void setForgotPasswordKey(String forgotPasswordKey) {
        this.forgotPasswordKey = forgotPasswordKey;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Date getForgotPasswordExpiry() {
        return forgotPasswordExpiry;
    }

    public void setForgotPasswordExpiry(Date forgotPasswordExpiry) {
        this.forgotPasswordExpiry = forgotPasswordExpiry;
    }

    public Integer getFkRefferredByUserId() {
        return fkRefferredByUserId;
    }

    public void setFkRefferredByUserId(Integer fkRefferredByUserId) {
        this.fkRefferredByUserId = fkRefferredByUserId;
    }

    public List<StateMaster> getStateMaster() {
        return stateMaster;
    }

    public void setStateMaster(List<StateMaster> stateMaster) {
        this.stateMaster = stateMaster;
    }

    public List<CountryMaster> getCountryMaster() {
        return countryMaster;
    }

    public void setCountryMaster(List<CountryMaster> countryMaster) {
        this.countryMaster = countryMaster;
    }

    public CartBusinessDo getCartBusinessDo() {
        return cartBusinessDo;
    }

    public void setCartBusinessDo(CartBusinessDo cartBusinessDo) {
        this.cartBusinessDo = cartBusinessDo;
    }

    public List<CrosssellBusinessDo> getCrosssellBusinessDos() {
        return crosssellBusinessDos;
    }

    public void setCrosssellBusinessDos(List<CrosssellBusinessDo> crosssellBusinessDos) {
        this.crosssellBusinessDos = crosssellBusinessDos;
    }
}
