package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletEligibility;

public class CheckoutBusinessDO extends BaseBusinessDO {
    private Integer txnHomePageId;
    private String defaultProfile;
    private String satus;
    private String ProductType;
    private String operatorImgUrl;
    private WalletEligibility walletEligibility;
    private PaymentBreakup paymentBreakup;
    private CartBusinessDo cartBusinessDo;
    
    /*
     * This is a catch all boolean used to
     * indicate if *any* kind of discount 
     * (freefund/cash-back/offer-code/etc.,) is
     * enabled for a given cart. Introduced for
     * add-cash-to-wallet product. Keep that in mind
     * if you want to re-use this field.
     */
    private Boolean isOffersEnabled = true;
    
    
    private Boolean walletEnabled;
    private Boolean walletDisplayEnabled;

    private Boolean cardStorageOptIn;
    
    public void setIsOffersEnabled(Boolean isOffersEnabled) {
        this.isOffersEnabled = isOffersEnabled;
    }
    
    public Boolean getIsOffersEnabled() {
        return isOffersEnabled;
    }

    public PaymentBreakup getPaymentBreakup() {
        return paymentBreakup;
    }
    
    public void setPaymentBreakup(PaymentBreakup paymentBreakup) {
        this.paymentBreakup = paymentBreakup;
    }

    public WalletEligibility getWalletEligibility() {
        return walletEligibility;
    }
    
    public void setWalletEligibility(WalletEligibility walletEligibility) {
        this.walletEligibility = walletEligibility;
    }

    public Boolean getWalletDisplayEnabled() {
        return walletDisplayEnabled;
    }
    
    public void setWalletDisplayEnabled(Boolean walletDisplayEnabled) {
        this.walletDisplayEnabled = walletDisplayEnabled;
    }
    
    public Boolean getWalletEnabled() {
        return walletEnabled;
    }
    
    public void setWalletEnabled(Boolean walletEnabled) {
        this.walletEnabled = walletEnabled;
    }

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public String getSatus() {
        return satus;
    }

    public void setSatus(String satus) {
        this.satus = satus;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    public String getOperatorImgUrl() {
        return operatorImgUrl;
    }

    public void setOperatorImgUrl(String operatorImgUrl) {
        this.operatorImgUrl = operatorImgUrl;
    }

    public Boolean getCardStorageOptIn() {
        return cardStorageOptIn;
    }

    public void setCardStorageOptIn(Boolean cardStorageOptIn) {
        this.cardStorageOptIn = cardStorageOptIn;
    }

    public CartBusinessDo getCartBusinessDo() {
        return cartBusinessDo;
    }

    public void setCartBusinessDo(CartBusinessDo cartBusinessDo) {
        this.cartBusinessDo = cartBusinessDo;
    }
}
