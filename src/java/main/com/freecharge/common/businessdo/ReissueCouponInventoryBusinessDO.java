package com.freecharge.common.businessdo;

import java.util.List;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class ReissueCouponInventoryBusinessDO extends BaseBusinessDO {
	
	private static final long serialVersionUID = 1L;

	private String orderId;
	private String deletedCouponInventoryDetails;
	private String reissueCouponInventoryDetails;
	
	private List deletedCouponInventoryList;
	private List reissueCouponInventoryList;
	
	private Integer fcTicketId;
	private String remarks;
	private boolean is_deleted;
	private String createdBy;
	private String deletedBy;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getDeletedCouponInventoryDetails() {
		return deletedCouponInventoryDetails;
	}
	public void setDeletedCouponInventoryDetails(
			String deletedCouponInventoryDetails) {
		this.deletedCouponInventoryDetails = deletedCouponInventoryDetails;
	}
	public String getReissueCouponInventoryDetails() {
		return reissueCouponInventoryDetails;
	}
	public void setReissueCouponInventoryDetails(
			String reissueCouponInventoryDetails) {
		this.reissueCouponInventoryDetails = reissueCouponInventoryDetails;
	}
	public List getDeletedCouponInventoryList() {
		return deletedCouponInventoryList;
	}
	public void setDeletedCouponInventoryList(List deletedCouponInventoryList) {
		this.deletedCouponInventoryList = deletedCouponInventoryList;
	}
	public List getReissueCouponInventoryList() {
		return reissueCouponInventoryList;
	}
	public void setReissueCouponInventoryList(List reissueCouponInventoryList) {
		this.reissueCouponInventoryList = reissueCouponInventoryList;
	}
	public Integer getFcTicketId() {
		return fcTicketId;
	}
	public void setFcTicketId(Integer fcTicketId) {
		this.fcTicketId = fcTicketId;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public boolean isIs_deleted() {
		return is_deleted;
	}
	public void setIs_deleted(boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}
	
}
