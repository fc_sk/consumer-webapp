package com.freecharge.common.businessdo;

import java.util.List;

import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: Jun 1, 2012
 * Time: 4:54:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class CityMasterBusinessDO extends BaseBusinessDO{
    private Integer stateId;
    private List<CityMaster> cityMasterList;
    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public List<CityMaster> getCityMasterList() {
        return cityMasterList;
    }

    public void setCityMasterList(List<CityMaster> cityMasterList) {
        this.cityMasterList = cityMasterList;
    }
}
