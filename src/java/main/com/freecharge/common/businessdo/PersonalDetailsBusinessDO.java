package com.freecharge.common.businessdo;

import java.util.List;

import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.util.TxnCrosssellItemsVO;
import com.freecharge.wallet.service.PaymentBreakup;
import com.freecharge.wallet.service.WalletEligibility;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: Jun 16, 2012
 * Time: 10:55:11 AM
 * To change this template use File | Settings | File Templates.
 */
public class PersonalDetailsBusinessDO extends BaseBusinessDO{
    private String serviceNumber;
    private Integer txnHomePageId;
    private UserContactDetail userContactDetail;
    private String defaultProfile;
    private String satus;
    private CartBusinessDo cart;
    private List<StateMaster> stateMaster;
    private List<CrosssellBusinessDo> crosssellBusinessDos;
    private String ProductType;
    private List<TxnCrosssellItemsVO> txnCrossSellIdList;
    private List<CityMaster> cityMasterList;
    private String operatorImgUrl;
    private String operatorName;
    private WalletEligibility walletEligibility;
    private PaymentBreakup paymentBreakup;
    
    private String circleName;
    
    private Boolean walletEnabled;
    private Boolean walletDisplayEnabled;

    private Boolean cardStorageOptIn;
    
    private Boolean contactDetailsReq;

    public PaymentBreakup getPaymentBreakup() {
        return paymentBreakup;
    }
    
    public void setPaymentBreakup(PaymentBreakup paymentBreakup) {
        this.paymentBreakup = paymentBreakup;
    }

    public WalletEligibility getWalletEligibility() {
        return walletEligibility;
    }
    
    public void setWalletEligibility(WalletEligibility walletEligibility) {
        this.walletEligibility = walletEligibility;
    }

    public Boolean getWalletDisplayEnabled() {
        return walletDisplayEnabled;
    }
    
    public void setWalletDisplayEnabled(Boolean walletDisplayEnabled) {
        this.walletDisplayEnabled = walletDisplayEnabled;
    }
    
    public Boolean getWalletEnabled() {
        return walletEnabled;
    }
    
    public void setWalletEnabled(Boolean walletEnabled) {
        this.walletEnabled = walletEnabled;
    }

	public List<CrosssellBusinessDo> getCrosssellBusinessDos() {
		return crosssellBusinessDos;
	}

	public void setCrosssellBusinessDos(List<CrosssellBusinessDo> crosssellBusinessDos) {
		this.crosssellBusinessDos = crosssellBusinessDos;
	}

	public CartBusinessDo getCart() {
		return cart;
	}

	public void setCart(CartBusinessDo cart) {
		this.cart = cart;
	}

	public List<StateMaster> getStateMaster() {
		return stateMaster;
	}

	public void setStateMaster(List<StateMaster> stateMaster) {
		this.stateMaster = stateMaster;
	}

	public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public UserContactDetail getUserContactDetail() {
        return userContactDetail;
    }

    public void setUserContactDetail(UserContactDetail userContactDetail) {
        this.userContactDetail = userContactDetail;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public String getSatus() {
        return satus;
    }

    public void setSatus(String satus) {
        this.satus = satus;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

	public void setTxnCrossSellIdList(List<TxnCrosssellItemsVO> txnCrossSellIdList) {
		this.txnCrossSellIdList = txnCrossSellIdList;
	}

	public List<TxnCrosssellItemsVO> getTxnCrossSellIdList() {
		return txnCrossSellIdList;
	}

	public List<CityMaster> getCityMasterList() {
		return cityMasterList;
	}

	public void setCityMasterList(List<CityMaster> cityMasterList) {
		this.cityMasterList = cityMasterList;
	}

	public String getOperatorImgUrl() {
		return operatorImgUrl;
	}

	public void setOperatorImgUrl(String operatorImgUrl) {
		this.operatorImgUrl = operatorImgUrl;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

    public Boolean getCardStorageOptIn() {
        return cardStorageOptIn;
    }

    public void setCardStorageOptIn(Boolean cardStorageOptIn) {
        this.cardStorageOptIn = cardStorageOptIn;
    }

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public Boolean getContactDetailsReq() {
		return contactDetailsReq;
	}

	public void setContactDetailsReq(Boolean contactDetailsReq) {
		this.contactDetailsReq = contactDetailsReq;
	}
}
