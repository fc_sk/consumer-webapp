package com.freecharge.common.businessdo;

import java.util.List;

import com.freecharge.app.domain.entity.PaymentTypeMaster;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 5, 2012
 * Time: 12:02:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class PaymentOptionsBusinessDO extends BaseBusinessDO{

    public List<PaymentTypeMaster> getPaymentTypeMastersList() {
        return paymentTypeMastersList;
    }

    public void setPaymentTypeMastersList(List<PaymentTypeMaster> paymentTypeMastersList) {
        this.paymentTypeMastersList = paymentTypeMastersList;
    }

    private List<PaymentTypeMaster> paymentTypeMastersList ;
}
