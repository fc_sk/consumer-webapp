package com.freecharge.common.businessdo;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 5/3/12
 * Time: 12:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class ForgotPasswordBusinessDO extends BaseBusinessDO{
    private boolean userValid;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String       email;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;

    public boolean getUserValid() {
        return userValid;
    }

    public void setUserValid(boolean userValid) {
        this.userValid = userValid;
    }
}
