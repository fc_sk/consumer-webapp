package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class PaymentTypeOptionBusinessDO extends BaseBusinessDO {

    private String displayName;
    private String displayImg;
    private String imgUrl;
    private String code;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayImg() {
        return displayImg;
    }

    public void setDisplayImg(String displayImg) {
        this.displayImg = displayImg;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "PaymentTypeOptionBusinessDO{" +
                "displayName='" + displayName + '\'' +
                ", displayImg='" + displayImg + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
