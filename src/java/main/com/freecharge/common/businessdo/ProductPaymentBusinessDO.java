package com.freecharge.common.businessdo;

import org.apache.commons.lang.StringUtils;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.payment.dos.business.PaymentRequestCardDetails;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.web.webdo.CardDetailsWebDO;

import java.util.Map;

public class ProductPaymentBusinessDO extends BaseBusinessDO {
    private static final long serialVersionUID = 4022342348091915796L;
    private String upiInfo;
    private String paymenttype;
    private String productId;
    private String affiliateid;
    private String pyop;
    private String cartId;
    private String breaker;
    private String productType;

    private String couponCode;
    private int offerType;
    
    private int binOfferId;

    private Double amount;
    private String orderId;
    private String ccNumber;
    private String dcNumber;
    
    private Boolean usePartialWalletPayment;
    private Boolean dbCardSaveBool;

    private Boolean unCheckedStoreCard;
    private String dbCardToken;
    
    private Integer appVersion;
    private PaymentRequestCardDetails paymentRequestCardDetails;
    
    private CardDetailsWebDO cardDetailsDO;
    
    private String deviceId;
    
    private int userId;
    
    private boolean isQCPayment;
    
    private Users user;

    private int channelId;
    
    private String utmDetails;

    private Map<String,String> transactionTypeStatus;
    
    private String mtxnId;
    
	public Map<String, String> getTransactionTypeStatus() {
        return transactionTypeStatus;
    }

    public void setTransactionTypeStatus(Map<String, String> transactionTypeStatus) {
        this.transactionTypeStatus = transactionTypeStatus;
    }

    public String getUtmDetails() {
        return utmDetails;
    }

    public void setUtmDetails(String utmDetails) {
        this.utmDetails = utmDetails;
    }

    public boolean isQCPayment() {
        return isQCPayment;
    }

    public void setQCPayment(boolean isQCPayment) {
        this.isQCPayment = isQCPayment;
    }

    public String getUpiInfo() {
        return this.upiInfo;
    }

    public void setUpiInfo(String upiInfo) {
        this.upiInfo = upiInfo;
    }

    public Boolean getUsePartialWalletPayment() {
        return usePartialWalletPayment;
    }
    
    public void setUsePartialWalletPayment(Boolean usePartialWalletPayment) {
        this.usePartialWalletPayment = usePartialWalletPayment;
    }
    
    public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getPaymenttype() {
		return paymenttype;
	}

	public void setPaymenttype(String paymenttype) {
		this.paymenttype = paymenttype;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getAffiliateid() {
		return affiliateid;
	}

	public void setAffiliateid(String affiliateid) {
		this.affiliateid = affiliateid;
	}

    public String getPaymentoption() {
        return pyop;
    }

    public void setPaymentoption(String paymentoption) {
        this.pyop = paymentoption;
    }

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public String getBreaker() {
		return breaker;
	}

	public void setBreaker(String breaker) {
		this.breaker = breaker;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public int getOfferType() {
		return offerType;
	}

	public void setOfferType(int offerType) {
		this.offerType = offerType;
	}

	public int getBinOfferId() {
		return binOfferId;
	}

	public void setBinOfferId(int binOfferId) {
		this.binOfferId = binOfferId;
	}

	public String getCcNumber() {
		return ccNumber;
	}

	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}

	public String getDcNumber() {
		return dcNumber;
	}

	public void setDcNumber(String dcNumber) {
		this.dcNumber = dcNumber;
	}

	public Boolean getDbCardSaveBool() {
		return dbCardSaveBool;
	}

	public void setDbCardSaveBool(Boolean dbCardSaveBool) {
		this.dbCardSaveBool = dbCardSaveBool;
	}

	public Boolean getUnCheckedStoreCard() {
		return unCheckedStoreCard;
	}

	public void setUnCheckedStoreCard(Boolean unCheckedStoreCard) {
		this.unCheckedStoreCard = unCheckedStoreCard;
	}

	public String getDbCardToken() {
		return dbCardToken;
	}

	public void setDbCardToken(String dbCardToken) {
		this.dbCardToken = dbCardToken;
	}

    public Integer getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(Integer appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public PaymentRequestCardDetails getPaymentRequestCardDetails() {
        return paymentRequestCardDetails;
    }

    public void setPaymentRequestCardDetails(PaymentRequestCardDetails paymentRequestCardDetails) {
        this.paymentRequestCardDetails = paymentRequestCardDetails;
    }

    public CardDetailsWebDO getCardDetailsDO() {
        return cardDetailsDO;
    }

    public void setCardDetailsDO(CardDetailsWebDO cardDetailsDO) {
        this.cardDetailsDO = cardDetailsDO;
    }

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
    public boolean isCreditOrDebitCardTransaction() {
        return (this.paymenttype.equals(PaymentConstants.PAYMENT_TYPE_CREDITCARD)
                || paymenttype.equals(PaymentConstants.PAYMENT_TYPE_DEBITCARD))
                && this.getPaymentRequestCardDetails() != null
                && StringUtils.isNotEmpty(this.getPaymentRequestCardDetails().getCardNumber());
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }
    
    public String getMtxnId() {
		return mtxnId;
	}

	public void setMtxnId(String mtxnId) {
		this.mtxnId = mtxnId;
	}

}
