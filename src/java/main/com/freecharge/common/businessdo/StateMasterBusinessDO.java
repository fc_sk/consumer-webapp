package com.freecharge.common.businessdo;

import java.util.List;

import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.web.webdo.StateMaster;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 20, 2012
 * Time: 2:31:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class StateMasterBusinessDO extends BaseBusinessDO{
   List<com.freecharge.web.webdo.StateMaster> stateList;

    public List<StateMaster> getStateList() {
        return stateList;
    }

    public void setStateList(List<StateMaster> stateList) {
        this.stateList = stateList;
    }
}
