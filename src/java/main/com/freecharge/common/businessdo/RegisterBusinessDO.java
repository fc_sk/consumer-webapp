package com.freecharge.common.businessdo;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.CountryMaster;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.util.SessionConstants;
import com.freecharge.common.util.FCConstants;

/**
 * Registration Data Object
 */
public class RegisterBusinessDO extends BaseBusinessDO {
    private Integer                   userId;
    private String                    email;
    private String                    password;
    private String                    mobileNo;
    private String                    dob;
    private Boolean                   isActive;
    private Boolean                   isLoggedin;
    private String                    morf;
    private String                    imei;
    private String                    uniqueDeviceId;
    private Timestamp                 lastUpdate;
    private String                    registrationStatus;
    private String                    nickAlias;
    private String                    address1;
    private String                    landmark;
    private String                    city;
    private Integer                   cityId;
    private String                    postalCode;
    private String                    serviceNumber;
    private String                    state;
    private Integer                   stateId;
    private UserContactDetail         userContactDetails;
    private Integer                   userProfileId;
    private Integer                   userRechargeContactId;
    private Integer                   txnHomePageId;
    private Integer                   countryId;
    private String                    country;
    private String                    title;
    private String                    comboAddr;
    private Integer                   txnfulfilmentId;
    private String                    area;
    private String                    confirmPassword;
    private String                    street;
    private List<CrosssellBusinessDo> crosssellBusinessDos;
    private String                    productType;
    private String                    defaultProfile;
    private Integer                   fkAffiliateProfileId;
    private List<CityMaster>          cityMasterList;
    private Integer                   type;
    private SessionConstants.LoginSource   loginSource;

    private String                    firstName;
    private Map<String, Object>       sessionData;

    private List<StateMaster>         stateMaster;
    private List<CountryMaster>       countryMaster;

    public SessionConstants.LoginSource getLoginSource() {
        return loginSource;
    }

    public void setLoginSource(SessionConstants.LoginSource loginSource) {
        this.loginSource = loginSource;
    }

    public Integer getType() {
        return type;
    }

    public void setType(final Integer type) {
        this.type = type;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(final Boolean active) {
        isActive = active;
    }

    public Boolean getLoggedin() {
        return isLoggedin;
    }

    public void setLoggedin(final Boolean loggedin) {
        isLoggedin = loggedin;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(final Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setMobileNo(final String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getDob() {
        return this.dob;
    }

    public void setDob(final String dob) {
        this.dob = dob;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(final Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsLoggedin() {
        return this.isLoggedin;
    }

    public void setIsLoggedin(final Boolean isLoggedin) {
        this.isLoggedin = isLoggedin;
    }

    public String getMorf() {
        return this.morf;
    }

    public void setMorf(final String morf) {
        this.morf = morf;
    }

    public Timestamp getLastUpdate() {
        return this.lastUpdate;
    }

    public void setLastUpdate(final Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getRegistrationStatus() {
        return this.registrationStatus;
    }

    public void setRegistrationStatus(final String registrationStatus) {
        this.registrationStatus = registrationStatus;
    }

    public String getNickAlias() {
        return this.nickAlias;
    }

    public void setNickAlias(final String nickAlias) {
        this.nickAlias = nickAlias;
    }

    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(final String address1) {
        this.address1 = address1;
    }

    public String getLandmark() {
        return this.landmark;
    }

    public void setLandmark(final String landmark) {
        this.landmark = landmark;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(final Integer cityId) {
        this.cityId = cityId;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    public Map<String, Object> getSessionData() {
        return sessionData;
    }

    public void setSessionData(final Map<String, Object> sessionData) {
        this.sessionData = sessionData;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(final String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public UserContactDetail getUserContactDetails() {
        return userContactDetails;
    }

    public void setUserContactDetails(final UserContactDetail userContactDetails) {
        this.userContactDetails = userContactDetails;
    }

    public Integer getUserProfileId() {
        return userProfileId;
    }

    public void setUserProfileId(final Integer userProfileId) {
        this.userProfileId = userProfileId;
    }

    public Integer getUserRechargeContactId() {
        return userRechargeContactId;
    }

    public void setUserRechargeContactId(final Integer userRechargeContactId) {
        this.userRechargeContactId = userRechargeContactId;
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(final Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(final Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(final Integer countryId) {
        this.countryId = countryId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public List<StateMaster> getStateMaster() {
        return stateMaster;
    }

    public void setStateMaster(final List<StateMaster> stateMaster) {
        this.stateMaster = stateMaster;
    }

    public List<CountryMaster> getCountryMaster() {
        return countryMaster;
    }

    public void setCountryMaster(final List<CountryMaster> countryMaster) {
        this.countryMaster = countryMaster;
    }

    public String getComboAddr() {
        return comboAddr;
    }

    public void setComboAddr(final String comboAddr) {
        this.comboAddr = comboAddr;
    }

    public Integer getTxnfulfilmentId() {
        return txnfulfilmentId;
    }

    public void setTxnfulfilmentId(final Integer txnfulfilmentId) {
        this.txnfulfilmentId = txnfulfilmentId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(final String area) {
        this.area = area;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(final String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public List<CrosssellBusinessDo> getCrosssellBusinessDos() {
        return crosssellBusinessDos;
    }

    public void setCrosssellBusinessDos(final List<CrosssellBusinessDo> crosssellBusinessDos) {
        this.crosssellBusinessDos = crosssellBusinessDos;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(final String productType) {
        this.productType = productType;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(final String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public Integer getFkAffiliateProfileId() {
        return fkAffiliateProfileId;
    }

    public void setFkAffiliateProfileId(final Integer fkAffiliateProfileId) {
        this.fkAffiliateProfileId = fkAffiliateProfileId;
    }

    public List<CityMaster> getCityMasterList() {
        return cityMasterList;
    }

    public void setCityMasterList(final List<CityMaster> cityMasterList) {
        this.cityMasterList = cityMasterList;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(final String imei) {
        this.imei = imei;
    }

    public String getUniqueDeviceId() {
        return uniqueDeviceId;
    }

    public void setUniqueDeviceId(final String deviceId) {
        this.uniqueDeviceId = deviceId;
    }
}
