package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: Apr 12, 2012
 * Time: 3:23:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class ModeOfPaymentBusinessDo extends BaseWebDO {
    private Integer mopId;
    private String mopName;

    public Integer getMopId() {
        return mopId;
    }

    public void setMopId(Integer mopId) {
        this.mopId = mopId;
    }

    public String getMopName() {
        return mopName;
    }

    public void setMopName(String mopName) {
        this.mopName = mopName;
    }
}
