package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 12, 2012
 * Time: 12:42:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class CheckUserBusinessDO extends BaseBusinessDO{
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    private String email;
    private String statusMsg;
}
