package com.freecharge.common.businessdo;

import com.freecharge.app.domain.entity.UserContactDetail;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 17, 2012
 * Time: 6:05:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class TxnHomePageBusinessDO extends BaseBusinessDO{
    private Integer txnHomePageId;
    private String serviceNumber;
    private String operatorName;
    private String circleName;
    private Float amount;
    private String productType;
    private String productId;
    private String sessionId;
    private String lookupId;
    private String serviceStatus;
    private UserContactDetail contactDetails;
    private String defaultProfile;
    private String rechargePlanType;
    private String festiveMessage;
    private String opName;
    private String cirName;

	public Integer getTxnHomePageId() {
        return txnHomePageId;
    }

    public void setTxnHomePageId(Integer txnHomePageId) {
        this.txnHomePageId = txnHomePageId;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getLookupId() {
        return lookupId;
    }

    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public UserContactDetail getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(UserContactDetail contactDetails) {
        this.contactDetails = contactDetails;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }
    
    public String getRechargePlanType() {
		return rechargePlanType;
	}

	public void setRechargePlanType(String rechargePlanType) {
		this.rechargePlanType = rechargePlanType;
	}

    public String getFestiveMessage() {
        return festiveMessage;
    }

    public void setFestiveMessage(String f) {
        this.festiveMessage = f;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

	public String getOpName() {
		return opName;
	}

	public void setOpName(String opName) {
		this.opName = opName;
	}
	
	public String getCirName() {
		return cirName;
	}

	public void setCirName(String cirName) {
		this.cirName = cirName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TxnHomePageBusinessDO [txnHomePageId=").append(txnHomePageId).append(", serviceNumber=")
				.append(serviceNumber).append(", operatorName=").append(operatorName).append(", circleName=")
				.append(circleName).append(", amount=").append(amount).append(", productType=").append(productType)
				.append(", productId=").append(productId).append(", sessionId=").append(sessionId).append(", lookupId=")
				.append(lookupId).append(", serviceStatus=").append(serviceStatus).append(", contactDetails=")
				.append(contactDetails).append(", defaultProfile=").append(defaultProfile).append(", rechargePlanType=")
				.append(rechargePlanType).append(", festiveMessage=").append(festiveMessage).append(", opName=")
				.append(opName).append(", cirName=").append(cirName).append("]");
		return builder.toString();
	}
	
	
}
