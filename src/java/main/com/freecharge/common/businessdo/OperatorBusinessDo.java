package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: Apr 11, 2012
 * Time: 3:07:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class OperatorBusinessDo extends BaseWebDO {
      private Integer operatorId;
      private String operatorName;

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
}
