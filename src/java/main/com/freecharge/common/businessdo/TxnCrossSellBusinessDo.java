package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseBusinessDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: May 16, 2012
 * Time: 11:18:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class TxnCrossSellBusinessDo extends BaseBusinessDO{
    private Integer homePageId;
    private Integer couponId;
    private Integer crossSellId;
    private String itemIdArray;
    private String type;
    private String name;
    private String imagPath;
    private String proType;
    private Float amount;
    private String serviceNo;
    private Integer serviceCharge;
    private String operatorName;
    private Integer planId;
    private String planCategory;

    public Integer getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(Integer serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	public Integer getHomePageId() {
        return homePageId;
    }

    public void setHomePageId(Integer homePageId) {
        this.homePageId = homePageId;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getCrossSellId() {
        return crossSellId;
    }

    public void setCrossSellId(Integer crossSellId) {
        this.crossSellId = crossSellId;
    }

    public String getItemIdArray() {
        return itemIdArray;
    }

    public void setItemIdArray(String itemIdArray) {
        this.itemIdArray = itemIdArray;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagPath() {
        return imagPath;
    }

    public void setImagPath(String imagPath) {
        this.imagPath = imagPath;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getServiceNo() {
        return serviceNo;
    }

    public void setServiceNo(String serviceNo) {
        this.serviceNo = serviceNo;
    }

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getPlanCategory() {
        return planCategory;
    }

    public void setPlanCategory(String planCategory) {
        this.planCategory = planCategory;
    }
}