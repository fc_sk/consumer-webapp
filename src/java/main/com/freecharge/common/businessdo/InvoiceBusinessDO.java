package com.freecharge.common.businessdo;

import java.sql.Timestamp;
import java.util.List;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.web.webdo.CartAndCartItemsVO;
import com.freecharge.web.webdo.RechargeInfoVO;

public class InvoiceBusinessDO extends BaseBusinessDO {
	private Double totalAmount;
	
	private Timestamp paymentTime;
	
	private ProductName primaryProduct;
	
	public void setPaymentTime(Timestamp paymentTime) {
        this.paymentTime = paymentTime;
    }
	
	public Timestamp getPaymentTime() {
        return paymentTime;
    }
	
	public void setPrimaryProduct(ProductName primaryProduct) {
        this.primaryProduct = primaryProduct;
    }
	
	public ProductName getPrimaryProduct() {
        return primaryProduct;
    }

	public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }


    private RechargeInfoVO rechargeInfoVO;
	private List<CartAndCartItemsVO> cartAndCartItemsList;
	
	public List<CartAndCartItemsVO> getCartAndCartItemsList() {
		return cartAndCartItemsList;
	}

	public void setCartAndCartItemsList(List<CartAndCartItemsVO> cartAndCartItemsList) {
		this.cartAndCartItemsList = cartAndCartItemsList;
	}

	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public RechargeInfoVO getRechargeInfoVO() {
		return rechargeInfoVO;
	}

	public void setRechargeInfoVO(RechargeInfoVO rechargeInfoVO) {
		this.rechargeInfoVO = rechargeInfoVO;
	}

}
