package com.freecharge.common.businessdo;

import com.freecharge.common.framework.basedo.BaseWebDO;

/**
 * Created by IntelliJ IDEA.
 * User: Jitender
 * Date: Apr 12, 2012
 * Time: 4:07:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class TransactionDataBusinessDo extends BaseWebDO {
    private Integer transactionId;
    private String transactionName;

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }
}
