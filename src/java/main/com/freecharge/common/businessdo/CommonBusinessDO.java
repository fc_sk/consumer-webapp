package com.freecharge.common.businessdo;

import java.util.List;

import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.common.framework.basedo.BaseBusinessDO;

public class CommonBusinessDO extends BaseBusinessDO{
	private List<StateMaster> stateMaster;
	private List<CityMaster> cityMasterList;

	public List<StateMaster> getStateMaster() {
		return stateMaster;
	}

	public void setStateMaster(List<StateMaster> stateMaster) {
		this.stateMaster = stateMaster;
	}

	public List<CityMaster> getCityMasterList() {
		return cityMasterList;
	}

	public void setCityMasterList(List<CityMaster> cityMasterList) {
		this.cityMasterList = cityMasterList;
	}
}
