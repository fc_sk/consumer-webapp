package com.freecharge.common.api.authenticaton;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.common.cache.RedisCacheManager;
import com.freecharge.common.framework.logging.LoggingFactory;

@Component
public class ApiAuthenticatorCache extends RedisCacheManager {
    
    private static final String PREFIX = "API_AUTHENTICATOR_";
    private final long  TIME_TO_CACHE = 60;
    private final Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private RedisTemplate<String, Object> apiAuthenticatorCacheTemplate;
    
    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.apiAuthenticatorCacheTemplate;
    }
    
    public ApiAuthenticatorCacheDo getApiAuthenticatorCache(String accessKey) {
        try {
            return (ApiAuthenticatorCacheDo) get(PREFIX+accessKey);
        } catch (Exception e) {
            logger.error("Error in getting cache data for access key "+accessKey, e);
            return null;
        }
    }

    public void setApiAuthenticatorCache(ApiAuthenticatorCacheDo apiAuthenticatorCacheDo) {
        if(apiAuthenticatorCacheDo==null){
            return;
        }
        try {
            set(PREFIX+apiAuthenticatorCacheDo.getAccessKey(), apiAuthenticatorCacheDo, TIME_TO_CACHE, TimeUnit.MINUTES);
        } catch (Exception e) {
            logger.error("Error in setting cache data for access key "+apiAuthenticatorCacheDo.getAccessKey(), e);
        }
    }
}
