package com.freecharge.common.api.authenticaton;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class ApiAuthenticatorCacheDo implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private String accessKey;
    private String secretKey;
    private Set<String> accessibleApi = new HashSet<String>();

    public ApiAuthenticatorCacheDo(String accessKey, String secretKey, Set<String> accessibleApi) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
        this.accessibleApi = accessibleApi;
    }

    public String getAccessKey() {
        return accessKey;
    }
    
    public String getSecretKey() {
        return secretKey;
    }

    public Set<String> getAccessibleApis() {
        return accessibleApi;
    }

}
