package com.freecharge.common.api.authenticaton;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.dao.ApiAuthenticatorDao;
import com.freecharge.app.domain.entity.AccessDetails;
import com.freecharge.app.domain.entity.AccessInfo;

@Component
public class ApiAuthenticationService {
    
    @Autowired
    private ApiAuthenticatorCache apiAuthenticatorCache;
    
    @Autowired
    private ApiAuthenticatorDao apiAuthenticatorDao;

    public ApiAuthenticatorCacheDo getApiAithenticatorDetails(String accessKey) {
        
        ApiAuthenticatorCacheDo apiAuthenticatorCacheDo = apiAuthenticatorCache.getApiAuthenticatorCache(accessKey);
        if(apiAuthenticatorCacheDo == null){
            AccessInfo accessInfo = apiAuthenticatorDao.getAccessInfo(accessKey);
            if(accessInfo!=null){
                List<AccessDetails> accessDetails = apiAuthenticatorDao.getAccessDetails(accessKey);
                apiAuthenticatorCacheDo = getApiAuthenticatorCacheDo(accessInfo, accessDetails);
                apiAuthenticatorCache.setApiAuthenticatorCache(apiAuthenticatorCacheDo);
            }
        }
        return apiAuthenticatorCacheDo;
    }

    private ApiAuthenticatorCacheDo getApiAuthenticatorCacheDo(AccessInfo accessInfo, List<AccessDetails> accessDetails) {
        Set<String> accessibleApi = new HashSet<String>();
        for(AccessDetails accessDetail : accessDetails){
            accessibleApi.add(accessDetail.getApi());
        }
        
        return new ApiAuthenticatorCacheDo(accessInfo.getAccessKey(), accessInfo.getSecretKey(), accessibleApi);
    }

}
