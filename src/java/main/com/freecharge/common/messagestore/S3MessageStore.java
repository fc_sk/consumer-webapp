package com.freecharge.common.messagestore;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.common.util.FCUtil;
import com.freecharge.usermessages.model.Channel;
import com.freecharge.usermessages.model.HookPoint;
import com.freecharge.usermessages.model.MessageType;
import com.freecharge.usermessages.model.UserMessage;
import com.freecharge.usermessages.s3.S3AsyncMessageStore;

@Service
public class S3MessageStore {

    private Logger logger = LoggingFactory.getLogger(getClass());
    
    @Autowired
    private AppConfigService appConfigService;

    @Autowired
    private S3AsyncMessageStore s3AsyncMessageStore;
    
    /**
     * Calls S3AsyncMessageStore to upload a message to S3.
     * @param userId - userId logged in
     * @param orderId - orderId associated with message
     * @param channel - channel on which user is logged in
     * @param content - content of the message
     * @param type - type of message
     * @param hookPoint - point at which message occurred
     * @param date - time of message
     * @param channelId - id of user associated with channel. For eg, for an SMS, it could be mobile no.
     */
    public void storeUserMessage(int userId, String orderId, Channel channel, String content, MessageType type,
            HookPoint hookPoint, java.util.Date date, String channelId) {
        logger.info("Storing user message for userId = " + userId + " , channel = " + channel);
        if (!appConfigService.isUserMessageStoreEnabled()) {
            logger.info("Aborted. Storing User messages is disabled");
            return;
        }
        if (FCUtil.isEmpty(content)) {
            logger.info("Aborted. Content cannot be empty");
            return;
        }
        UserMessage msg = null;
        try {
            if (date == null)
                date = new java.util.Date();
            msg = new UserMessage(userId, date, channel); 
            msg.setContent(content);
            msg.setHookPoint(hookPoint);
            msg.setMsgType(type);
            msg.setOrderId(orderId);
            msg.setChannelId(channelId);
            logger.info("Storing user message " + msg);
            s3AsyncMessageStore.store(msg);
        }
        catch (IOException e) {
            logger.error("IOException while storing user message: " + msg, e);
        }
        catch (RuntimeException re) {
            logger.error("RuntimeException while storing user message: " + msg, re);
        }
    }
}
