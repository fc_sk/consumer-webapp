package com.freecharge.common.json;

/**
 * Created by IntelliJ IDEA.
 * User: Manoj Kumar
 * Date: May 12, 2012
 * Time: 5:02:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class JsonResponse {
    private String status = null;
    private Object result = null;
    private String user = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
