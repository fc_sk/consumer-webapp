package com.freecharge.common.json;

import flexjson.JSONSerializer;

/**
 * Wrapper class for JSONSerializer
 *
 */
public class JsonSerializer {

    private JSONSerializer jsonSerializer = new JSONSerializer();

    public String serialize(final Object obj) {
        return jsonSerializer.serialize(obj);
    }
}
