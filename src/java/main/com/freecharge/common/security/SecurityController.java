package com.freecharge.common.security;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.freecharge.common.framework.annotations.NoLogin;

@Controller
@RequestMapping("/security/*")
public class SecurityController {

    @NoLogin
    @RequestMapping(value = "disclosure")
    public String disclosure() {
        return "security/disclosure";
    }

}
