package com.freecharge.common.timeout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.services.HttpMethodExecutorTask;

@Service
public class HttpExecutorService {
    private ExecutorService executor = Executors.newCachedThreadPool();
    private static Logger logger = LoggingFactory.getLogger(HttpExecutorService.class);
    
    public static enum HttpMethodType{
    	POST, GET;
    }
    public HttpResponse executeWithTimeOut(final HttpClient client, HttpMethod method, final Integer timeOutValue) throws ConnectTimeoutException{
    	HttpResponse response = null;
    	Future<Integer> rechargeTask = executor.submit(new HttpMethodExecutorTask(client, method));
    	try {
			int statusCode = rechargeTask.get(timeOutValue, TimeUnit.MILLISECONDS);
			response = new HttpResponse();
			response.setStatusCode(statusCode);
			response.setResponse(method.getResponseBodyAsString());
			return response;
		} catch (InterruptedException e) {
			logger.warn("InterruptedException during http request.", e);
		} catch (ExecutionException e) {
			logger.warn("ExecutionException during http request.", e);
		} catch (TimeoutException e) {
			logger.warn("TimeoutException during http request.", e);
		} catch (IOException e) {
			logger.error("TimeoutException during http request.", e);
		}finally {
            rechargeTask.cancel(true);
            logger.info("Done with http timeout task.");
        }
    	throw new ConnectTimeoutException();
    }
    
	public HttpMethod getMethod(String url, HttpMethodType methodType,
			Map<String, String> requestParams, Map<String, String> requestHeaders) {
		HttpMethod method = null;
		if (methodType == HttpMethodType.POST){
			method = new PostMethod(url);
			if (requestParams != null && requestParams.size() > 0){
				setPostMethodParams((PostMethod)method, requestParams);	
			}
		} else if (methodType == HttpMethodType.GET){
			method = new GetMethod(url);
			if (requestParams != null && requestParams.size() > 0){
				setGetMethodParams(method, requestParams);	
			}
		}
		
		if (requestHeaders != null && requestHeaders.size() > 0){
			setRequestHeaders(method, requestHeaders);	
		}
		
		return method;
	}

	private void setRequestHeaders(HttpMethod method, Map<String, String> requestHeaders) {
		Iterator<String> keyIterator =  requestHeaders.keySet().iterator();
		while (keyIterator.hasNext()){
			String key = keyIterator.next();
			method.setRequestHeader(key, requestHeaders.get(key));
		}
	}

	private void setPostMethodParams(PostMethod method, Map<String, String> requestParams) {
		Iterator<String> keyIterator =  requestParams.keySet().iterator();
		List<NameValuePair> nameValuePairs = new ArrayList<>();
		while (keyIterator.hasNext()){
			String key = keyIterator.next();
			nameValuePairs.add(new NameValuePair(key, requestParams.get(key)));
		}
		method.setRequestBody(nameValuePairs.toArray(new NameValuePair[0]));
	}
    
	private void setGetMethodParams(HttpMethod method, Map<String, String> requestParams) {
		Iterator<String> keyIterator =  requestParams.keySet().iterator();
		HttpMethodParams params = new HttpMethodParams();
		while (keyIterator.hasNext()){
			String key = keyIterator.next();
			params.setParameter(key, requestParams.get(key));
		}
		method.setParams(params);
	}
}
