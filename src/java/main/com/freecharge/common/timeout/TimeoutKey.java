package com.freecharge.common.timeout;

public enum TimeoutKey {

    FORTKNOX_ADD_CARD(1000), 
    FORTKNOX_DELETE_CARD(500), 
    FORTKNOX_GET_CARD(2500), 
    FORTKNOX_LIST_CARD(5000), 
    UMS_GET_USER_BY_TOKEN(500), 
    UMS_GET_USER_BY_EMAIL(1500), 
    UMS_GET_UPGRADE_STATUS(500),
    UMS_GET_USER_BY_TRANSFER_TOKEN(500),
    UMS_GET_TRANSFER_TOKEN(500), 
    UMS_GET_FC_WALLET_MIGRATION_STATUS(500), 
    UMS_GET_FC_WALLET_TOKEN(500),
	ONE_CHECK_OTP_TIME_OUT(3000), 
	NOTIFICATION_SERVICE_TIME_OUT(300), 
	INTRANSACTION_VALIDATION_TIMEOUT(2000),
	DEFAULT_VALIDATION_TIMEOUT(5000);

    private final int timeoutInMillis;

    private TimeoutKey(int timeoutInMillis) {
        this.timeoutInMillis = timeoutInMillis;
    }

    public int getTimeoutInMillis() {
        return timeoutInMillis;
    }

}
