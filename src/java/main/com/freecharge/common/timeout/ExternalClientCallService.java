package com.freecharge.common.timeout;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.platform.metrics.MetricsClient;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.*;

@Service
public class ExternalClientCallService {

    private static final String EXCEPTION = "Exception";

    private static final String SUCCESS = "Success";

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    private FCProperties fcProperties;

    private ExecutorService executor = Executors.newCachedThreadPool();

    private static final int QA_TIMEOUT_IN_MILLISECONDS = 20000;

    private static Logger logger = LoggingFactory.getLogger(ExternalClientCallService.class);

    public <T> T executeWithTimeOut(ExternalClientCallCommand<T> externalCallCommand) {
        T returnObj = null;
        Future<T> future = null;
        long startTime = System.currentTimeMillis();
        try {
            future = executor.submit(externalCallCommand.getCallable());
            long timeOutInMillis = fcProperties.isQaMode() ? QA_TIMEOUT_IN_MILLISECONDS : externalCallCommand.getTimeOutInMillis();
            returnObj = future.get(timeOutInMillis, TimeUnit.MILLISECONDS);
            metricsClient.recordEvent(externalCallCommand.getMetricServiceName(),
                    externalCallCommand.getMetricMethodName(), SUCCESS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
        	Throwable throwableException = ExceptionUtils.getRootCause(e);
			if (e instanceof TimeoutException) {
				logger.error("Error calling command :" + externalCallCommand + " TotalTime taken:"
						+ (System.currentTimeMillis() - startTime) + "Exception:", e);
			} else {
				logger.error("Error calling command :" + externalCallCommand + " TotalTime taken:"
						+ (System.currentTimeMillis() - startTime), e);
			}
			if (throwableException != null) {
				metricsClient.recordEvent(externalCallCommand.getMetricServiceName(),
						externalCallCommand.getMetricMethodName(), throwableException.getClass().getSimpleName());
			} else {
				metricsClient.recordEvent(externalCallCommand.getMetricServiceName(),
						externalCallCommand.getMetricMethodName(), e.getClass().getSimpleName());
			}
        } finally {
            if (future != null) {
                future.cancel(true);
            }
            metricsClient.recordLatency(externalCallCommand.getMetricServiceName(),
                    externalCallCommand.getMetricMethodName() + ".Latency", System.currentTimeMillis() - startTime);
        }
        return returnObj;
    }

}
