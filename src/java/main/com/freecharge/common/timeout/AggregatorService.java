package com.freecharge.common.timeout;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.tempuri.IEPayServiceProxy;

import com.freecharge.common.cache.CommonCacheService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.recharge.businessDo.AggregatorBalanceStatus;
import com.freecharge.recharge.businessDo.AggregatorBalanceStatus.BalanceStatus;
import com.freecharge.recharge.businessdao.AggregatorInterface;
import com.freecharge.recharge.exception.NetworkConnectTimeoutException;
import com.freecharge.recharge.services.EuronetHttpMethodExecutorTask;
import com.freecharge.recharge.services.HttpMethodExecutorTask;

/**
 * This is the base class for all the aggregators. 
 * @author arun
 *
 */
public abstract class AggregatorService implements AggregatorInterface {

	@Autowired
	protected MetricsClient metricsClient;

	@Autowired
	private CommonCacheService commonCacheService;

	private static Logger logger = LoggingFactory.getLogger(AggregatorService.class);

	private ExecutorService rechargeExecutor = Executors.newCachedThreadPool();

	protected abstract Double getBalanceWarnThreshold();

	protected abstract Double getBalanceCriticalThreshold();

	public abstract String getBalanceCachekey();

	public abstract String getBalanceUpdateTimeCachekey();

	protected final List<Integer> retriableHttpCodes = Arrays.asList(404, 500, 503);

	protected ExecutorService aggrRequestExecutor = Executors.newCachedThreadPool();


	public int executeWithoutTimeout(String affiliateTransactionId, final HttpClient client, final HttpMethod method, String aggregator) throws Exception {
        logger.info("inside execute withOUT timeout method");
	    long startTime = System.currentTimeMillis();
		metricsClient.timeStampEvent(affiliateTransactionId, MetricsClient.EventName.RechargeAttempt);
		metricsClient.recordLatency("UserSession", "RechargeAttempt", affiliateTransactionId,
				MetricsClient.EventName.PaySuccess);
		try {
			final Integer statusCode = client.executeMethod(method);
            metricsClient.recordEvent("Recharge", this.getClass().getSimpleName() + ".HttpStatusCode",
                    String.valueOf(statusCode));
            return statusCode;
		}   catch (ConnectTimeoutException | SocketException e) {
        	logger.error("Something bad happened; got ConnectTimeoutException while recharging for orderID: ["
                    + affiliateTransactionId + "]", e);
        	 metricsClient.recordEvent("Recharge",  aggregator+".ConnTimeOut",
                     "ConnTimeOut");
            throw e;
        } catch (IOException e) {
        	 logger.warn("Something bad happened; got TimeoutException while recharging for orderID: ["
                     + affiliateTransactionId + "]", e);
             metricsClient.recordEvent("Recharge", aggregator+".AppTimeOut", "AppTimeOut");
             throw e;
        } catch (Exception e) {
        	logger.error("Something bad happened; got Exception while recharging for orderID: ["
                    + affiliateTransactionId + "]", e);
        	 metricsClient.recordEvent("Recharge", aggregator+".UnknowunException",
                     e.getCause().getClass().getSimpleName());
            throw e;
        }finally {
            long endTime = System.currentTimeMillis();
            String aggregatorName = this.getClass().getSimpleName();
            metricsClient.recordLatency("Recharge" + "." + aggregatorName, "Latency", endTime - startTime);
            metricsClient.recordEvent("Recharge", aggregatorName, "MethodCall");
        }
	}

    /**
     * A utility method to execute a given HTTP method in a time bound fashion.
     * Experience has shown that {@link HttpClient} provided timeout parameters
     * http.connection.timeout and http.socket.timeout don't work all the times.
     * Hence this method.
     *
     * @param rechargeDo
     * @param client
     * @param method
     * @param shouldTimeOut
     * @param timeOutValue
     * @return
     * @throws IOException
     * @throws HttpException
     */

	public int executeWithTimeout(String affiliateTransactionId, final HttpClient client, final HttpMethod method,
			final Boolean shouldTimeOut, final Integer timeOutValue) throws HttpException, IOException {
		logger.info("inside execute with timeout method");
		long startTime = System.currentTimeMillis();
		metricsClient.timeStampEvent(affiliateTransactionId, MetricsClient.EventName.RechargeAttempt);
		metricsClient.recordLatency("UserSession", "RechargeAttempt", affiliateTransactionId,
				MetricsClient.EventName.PaySuccess);
		try {
			if (shouldTimeOut) {
				logger.info("Applevel timeout is enabled for orderID: [" + affiliateTransactionId + "]");
				Future<Integer> rechargeTask = rechargeExecutor.submit(new HttpMethodExecutorTask(client, method));

				try {
					final Integer statusCode = rechargeTask.get(timeOutValue, TimeUnit.MILLISECONDS);

					metricsClient.recordEvent("Recharge", this.getClass().getSimpleName() + ".HttpStatusCode",
							String.valueOf(statusCode));

					if (statusCode != HttpStatus.SC_OK) {
						logger.error("Received HTTP status code " + statusCode);
						logger.error("Error response: " + method.getResponseBodyAsString());
						if (retriableHttpCodes.contains(statusCode)) {
							throw new NetworkConnectTimeoutException();
						}
					}

					return statusCode;
				} catch (TimeoutException te) {
					logger.warn("Something bad happened; got TimeoutException while recharging for orderID: ["
							+ affiliateTransactionId + "]", te);
					metricsClient.recordEvent("Recharge", this.getClass().getSimpleName(), "AppTimeOut");
					throw new ConnectTimeoutException();
				} catch (InterruptedException ie) {
					logger.error("Something bad happened; got InterruptedException while recharging for orderID: ["
							+ affiliateTransactionId + "]");
					metricsClient.recordEvent("Recharge", this.getClass().getSimpleName(), "Interrupted");
					throw new ConnectTimeoutException();
				} catch (ExecutionException ee) {
					logger.error("Something bad happened; got ExecutionException while recharging for orderID: ["
							+ affiliateTransactionId + "]", ee);
					metricsClient.recordEvent("Recharge", this.getClass().getSimpleName(), "ExecutionException");

					final Throwable cause = ee.getCause();

					if (cause != null) {
						metricsClient.recordEvent("Recharge", this.getClass().getSimpleName(),
								cause.getClass().getSimpleName());
						if (isConnectionFailure(cause)) {
							throw new NetworkConnectTimeoutException();
						}
					}

					throw new ConnectTimeoutException();
				} finally {
					rechargeTask.cancel(true);
					logger.info("Done with recharge task for orderID: [" + affiliateTransactionId + "]");
				}
			} else {
				logger.info("App level timeout is disabled for orderID: [" + affiliateTransactionId + "]");
				return client.executeMethod(method);
			}
		} finally {
			long endTime = System.currentTimeMillis();
			String aggregatorName = this.getClass().getSimpleName();
			metricsClient.recordLatency("Recharge" + "." + aggregatorName, "Latency", endTime - startTime);
			metricsClient.recordEvent("Recharge", aggregatorName, "MethodCall");
		}
	}

	public String executeEurovas2WithTimeout(String affiliateTransactionId, IEPayServiceProxy client,
			final String request, final Boolean shouldTimeOut, final Integer timeOutValue)
			throws HttpException, IOException {
		long startTime = System.currentTimeMillis();
		metricsClient.timeStampEvent(affiliateTransactionId, MetricsClient.EventName.RechargeAttempt);
		metricsClient.recordLatency("UserSession", "RechargeAttempt", affiliateTransactionId,
				MetricsClient.EventName.PaySuccess);
		try {
			if (shouldTimeOut) {
				logger.info("Applevel timeout is enabled for orderID: [" + affiliateTransactionId + "]");
				Future<String> rechargeTask = rechargeExecutor
						.submit(new EuronetHttpMethodExecutorTask(client, request));
				try {
					final String response = rechargeTask.get(timeOutValue, TimeUnit.MILLISECONDS);
					return response;
				} catch (TimeoutException te) {
					logger.warn("Something bad happened; got TimeoutException while recharging for orderID: ["
							+ affiliateTransactionId + "]", te);
					metricsClient.recordEvent("Recharge", this.getClass().getSimpleName(), "AppTimeOut");
					throw new ConnectTimeoutException();
				} catch (InterruptedException ie) {
					logger.error("Something bad happened; got InterruptedException while recharging for orderID: ["
							+ affiliateTransactionId + "]");
					metricsClient.recordEvent("Recharge", this.getClass().getSimpleName(), "Interrupted");
					throw new ConnectTimeoutException();
				} catch (ExecutionException ee) {
					logger.error("Something bad happened; got ExecutionException while recharging for orderID: ["
							+ affiliateTransactionId + "]", ee);
					metricsClient.recordEvent("Recharge", this.getClass().getSimpleName(), "ExecutionException");

					final Throwable cause = ee.getCause();

					if (cause != null) {
						metricsClient.recordEvent("Recharge", this.getClass().getSimpleName(),
								cause.getClass().getSimpleName());
						if (isConnectionFailure(cause)) {
							throw new NetworkConnectTimeoutException();
						}
					}

					throw new ConnectTimeoutException();
				} finally {
					rechargeTask.cancel(true);
					logger.info("Done with recharge task for orderID: [" + affiliateTransactionId + "]");
				}
			} else {
				logger.info("App level timeout is disabled for orderID: [" + affiliateTransactionId + "]");
				return client.sendEuroVasRequest(request);
			}
		} finally {
			long endTime = System.currentTimeMillis();
			String aggregatorName = this.getClass().getSimpleName();
			metricsClient.recordLatency("Recharge" + "." + aggregatorName, "Latency", endTime - startTime);
			metricsClient.recordEvent("Recharge", aggregatorName, "MethodCall");
		}
	}

	private boolean isConnectionFailure(Throwable e) throws NetworkConnectTimeoutException {
		if (e instanceof SocketException) {
			return true;
		}

		if (e instanceof ConnectException) {
			return true;
		}

		if (e instanceof SocketTimeoutException) {
			return true;
		}

		return false;
	}

	public AggregatorBalanceStatus getBalanceStatus() throws IOException {
		Double currentBalance = this.getBalanceFromCache();

		AggregatorBalanceStatus balanceStatus = new AggregatorBalanceStatus();
		balanceStatus.setCurrentBalance(currentBalance);
		balanceStatus.setWarnThresholdValue(this.getBalanceWarnThreshold());
		balanceStatus.setCriticalThresholdValue(this.getBalanceCriticalThreshold());

		if (currentBalance < this.getBalanceCriticalThreshold()) {
			balanceStatus.setBalanceStatus(BalanceStatus.Critical);
		} else if (currentBalance < this.getBalanceWarnThreshold()) {
			balanceStatus.setBalanceStatus(BalanceStatus.Warn);
		} else {
			balanceStatus.setBalanceStatus(BalanceStatus.OK);
		}

		return balanceStatus;
	}

	public Double getBalanceFromCache() {
		return (Double) commonCacheService.get(this.getBalanceCachekey());
	}

	protected void pollAndCacheWalletBalance() {
		String aggregatorName = this.getClass().getSimpleName();
		Double currentBalance = null;
		try {
			currentBalance = this.getWalletBalance();
		} catch (Exception e) {
			logger.error("Unknown Exception polling " + aggregatorName + " for balance check " + e.getMessage(), e);
			metricsClient.recordEvent("Polling", aggregatorName, "Exception");
		}
		if (currentBalance != null) {
			commonCacheService.set(this.getBalanceCachekey(), currentBalance);
			commonCacheService.set(this.getBalanceUpdateTimeCachekey(), Calendar.getInstance());
		}
	}

	protected Integer executeWithTimeout(final HttpClient client, final HttpMethod method, final Integer timeOutValue,
			boolean shouldTimeout, String serviceName, String metricName) throws HttpException, IOException {
		long startTime = System.currentTimeMillis();
		if (shouldTimeout) {
			Future<Integer> rechargeTask = aggrRequestExecutor.submit(new HttpMethodExecutorTask(client, method));
			try {
				final Integer statusCode = rechargeTask.get(timeOutValue, TimeUnit.MILLISECONDS);
				if (statusCode != HttpStatus.SC_OK) {
					logger.error("Received HTTP status code " + statusCode);
				}
				metricsClient.recordEvent(serviceName, metricName, "SUCCESS");
				return statusCode;
			} catch (TimeoutException te) {
				logger.info("Connection Timeout Exception ", te);
				metricsClient.recordEvent("Recharge", metricName + "." + this.getClass().getSimpleName(), "AppTimeOut");
				throw new ConnectTimeoutException();
			} catch (InterruptedException ie) {
				logger.info("Interrupted exception ", ie);
				metricsClient.recordEvent("Recharge", metricName + "." + this.getClass().getSimpleName(), "Interrupted");
				throw new ConnectTimeoutException();
			} catch (ExecutionException ee) {
				logger.info("Execution timeout exception ", ee);
				metricsClient.recordEvent("Recharge", metricName + "." + this.getClass().getSimpleName(), "ExecutionException");
				throw new ConnectTimeoutException();
			} finally {
				rechargeTask.cancel(true);
				logger.info("Done with web service call task");
				metricsClient.recordLatency(serviceName, metricName + "." + this.getClass().getSimpleName()+".Latency", System.currentTimeMillis() - startTime);
			}
		} else {
			return client.executeMethod(method);
		}
	}
}
