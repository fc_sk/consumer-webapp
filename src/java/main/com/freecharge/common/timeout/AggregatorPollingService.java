package com.freecharge.common.timeout;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class AggregatorPollingService {
    
    private static Logger logger = LoggingFactory.getLogger(AggregatorPollingService.class);
    
    @Autowired
    @Qualifier(value="euronet")
    private AggregatorService euronetService;
    
    @Autowired
    @Qualifier(value="oxigen")
    private AggregatorService oxigenService;
    
    public void pollForBalance() {
        euronetService.pollAndCacheWalletBalance();
        oxigenService.pollAndCacheWalletBalance();
    }
    
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml", "web-delegates.xml");
        AggregatorPollingService aggregatorPollingService = ((AggregatorPollingService) ctx.getBean("aggregatorPollingService"));
        aggregatorPollingService.pollForBalance();
    }
}
