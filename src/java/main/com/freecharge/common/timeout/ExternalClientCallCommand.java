package com.freecharge.common.timeout;

import java.util.concurrent.Callable;

public class ExternalClientCallCommand<T> {

    private static final int ONE_SECOND = 1000;
    private String           metricServiceName;
    private String           metricMethodName;
    private TimeoutKey       timeoutKey;
    private Callable<T>      callable;

    public ExternalClientCallCommand(String metricServiceName, String metricMethodName, Callable<T> callable,
            TimeoutKey timeoutKey) {
        this.metricServiceName = metricServiceName;
        this.metricMethodName = metricMethodName;
        this.callable = callable;
        this.timeoutKey = timeoutKey;
    }

    public String getMetricServiceName() {
        return metricServiceName;
    }

    public String getMetricMethodName() {
        return metricMethodName;
    }

    public Callable<T> getCallable() {
        return callable;
    }

    public long getTimeOutInMillis() {
        return timeoutKey != null ? timeoutKey.getTimeoutInMillis() : ONE_SECOND;
    }

    @Override
    public String toString() {
        return "[ServiceName: " + metricServiceName + ",MethodName: " + metricMethodName + "]";
    }

}
