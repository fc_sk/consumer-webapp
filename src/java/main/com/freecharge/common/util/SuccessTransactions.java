package com.freecharge.common.util;

import java.io.Serializable;

public class SuccessTransactions implements Serializable{
	private String lookupId;
	private Double amount;
	public String getLookupId() {
		return lookupId;
	}
	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	} 
}
