package com.freecharge.common.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This file should contain common methods that can be used by anyone for hashing purposes
 * Created by vishal on 18/11/15.
 */
public class HashUtil {

    private static final String SHA256 = "SHA-256";
    private static final String UTF8   = "UTF-8";

    public static String getSHA256Hash(String data) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance(SHA256);
        md.update(data.getBytes(UTF8));
        byte byteData[] = md.digest();
        return toHexString(byteData);
    }

    public static String toHexString(byte[] byteData)
    {
        StringBuffer hexString = new StringBuffer();
        for (int i=0;i<byteData.length;i++) {
            String hex=Integer.toHexString(0xff & byteData[i]);
            if(hex.length()==1) hexString.append('0');
            hexString.append(hex);
        }
        if(hexString == null)
            return null;
        return hexString.toString();
    }
}
