package com.freecharge.common.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.businessdao.StringUtils;

public final class FCFileUtil {
	private static Logger logger = LoggingFactory.getLogger(FCFileUtil.class);
	
	/**
	 * Takes file path as input and returns the string array of columns.
	 * @param filePath	the complete path to the file.
	 * @return	the columns parsed in to a String array. 
	 */
	public static final List<String[]> parseCSVFile(String filePath){
        String line = null;
        BufferedReader bufferedReader = null;
        List<String[]> data = new ArrayList<>();
        try {
        	bufferedReader = new BufferedReader(new FileReader(filePath));
			while ((line = bufferedReader.readLine()) != null) {
			    String refundData = StringUtils.trim(line);
			    String[] split = refundData.split(",");
			    data.add(split);
			}
		} catch (IOException e) {
			logger.error("Error occured in reading file from path : " + filePath, e);
		} finally{
			if (bufferedReader != null){
				try {
					bufferedReader.close();
				} catch (IOException e) {
					logger.error("Error occured closing buffered reader.", e);
				}
			}
		}
        return data;
	}
}
