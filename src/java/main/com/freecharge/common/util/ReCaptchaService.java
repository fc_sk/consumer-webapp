package com.freecharge.common.util;

import javax.servlet.http.HttpServletRequest;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;

@Service
public class ReCaptchaService {
    private static final Logger LOGGER = LoggingFactory.getLogger(ReCaptchaService.class);
    @Autowired
    FCProperties fcProperties;
    private static final String RECAPTCHA_CHALLENGE_FIELD = "recaptcha_challenge_field";
    private static final String RECAPTCHA_RESPONSE_FIELD = "recaptcha_response_field";

    public boolean isValid(HttpServletRequest request) {
        try {
            String remoteAddr = request.getRemoteAddr();
            ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
            reCaptcha.setPrivateKey(fcProperties.getRecaptchaPrivateKey());
            String challenge = request.getParameter(RECAPTCHA_CHALLENGE_FIELD);
            String uresponse = request.getParameter(RECAPTCHA_RESPONSE_FIELD);
            ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, uresponse);
            return reCaptchaResponse.isValid();
        } catch (RuntimeException e) {
            LOGGER.error("Failed to validate captcha.", e);
        }
        return false;
    }

    public boolean isValid(String challenge, String response, String ip){
        try {
            ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
            reCaptcha.setPrivateKey(fcProperties.getRecaptchaPrivateKey());
            ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(ip, challenge, response);
            return reCaptchaResponse.isValid();
        } catch (RuntimeException e) {
            LOGGER.error("Failed to validate captcha.", e);
        }
        return false;
    }
}
