package com.freecharge.common.util;

public class DynamoConstants {
    public static final String MOBILE_AMOUNT_FIELD            = "recharge";
    public static final String POSTPAID_AMOUNT_FIELD          = "postpaid";
    public static final String DTH_AMOUNT_FIELD               = "dth";
    public static final String DATACARD_AMOUNT_FIELD          = "data";
    public static final String POSTAPID_DATACARD_AMOUNT_FIELD = "postpaiddata";
    public static final String ELECTRICITY_AMOUNT_FIELD       = "electricity";
    public static final String GAS_AMOUNT_FIELD               = "gas";
    public static final String LANDLINE_AMOUNT_FIELD          = "landline";
    public static final String BROADBAND_AMOUNT_FIELD          = "broadband";
    public static final String METRO_AMOUNT_FIELD             = "metro";
    public static final String WATER_AMOUNT_FIELD			  = "water";
    public static final String GOOGLE_CREDITS_AMOUNT_FIELD    = "googlecredits";
}
