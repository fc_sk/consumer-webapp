package com.freecharge.common.util;

public class KlickPayConstants {

    public static final String SUCCESS_CODE                = "E000";
    public static final String PAYMENT_SUCCESS_STATUS_CODE = "true";
    public static final String MERCHANT_ORDER_ID           = "merchant_txn_id";
    public static final String PG_URL                      = "pg_url";
    public static final String REQUEST_DATA                = "request_data";
}
