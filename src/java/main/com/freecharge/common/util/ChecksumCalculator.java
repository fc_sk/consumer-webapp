package com.freecharge.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ChecksumCalculator {

	private static final String DELIMITER                                 = "=!&";

	private static final String HASH_CALCULATION_ALGORITHM                = "SHA-512";

	private static final int    REQUEST_HASH_CALCULATION_ITERATION_COUNT  = 3;

	private static final Logger LOGGER                                    = LoggerFactory.getLogger(ChecksumCalculator.class);


	public static String calculateCheckSum(String key, String text)
			throws NoSuchAlgorithmException
	{
		String plainText = text.concat(key);
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(plainText.getBytes(Charset.defaultCharset()));
		byte[] mdbytes = md.digest();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < mdbytes.length; i++) {
			sb.append(Integer.toString((mdbytes[i] & 0xFF) + 256, 16).substring(1));
		}
		return sb.toString();
	}

	public static String calculateRequestChecksum(String key, String userId, String applicationSerNo){
		StringBuffer dataToHash = new StringBuffer();
		dataToHash.append(DELIMITER);
		dataToHash.append(key.hashCode());
		dataToHash.append(DELIMITER);
		dataToHash.append("|||||");
		dataToHash.append(DELIMITER);
		dataToHash.append(userId);
		dataToHash.append(DELIMITER);
		dataToHash.append(applicationSerNo);
		String hashedText = dataToHash.toString();
		try {
			for (int i = 0; i < REQUEST_HASH_CALCULATION_ITERATION_COUNT; i++) {
				hashedText = getHashedString(dataToHash);
			}
		} catch (Exception exception) {
			LOGGER.error("Exception generating hash", exception);
		}
		return hashedText;

	}

	public static String getHashedString(StringBuffer dataToHash) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(HASH_CALCULATION_ALGORITHM);
		md.reset();
		byte[] buffer = dataToHash.toString().getBytes();
		md.update(buffer);
		byte[] digest = md.digest();

		String hexStr = "";
		for (int i = 0; i < digest.length; i++) {
			hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
		}
		return hexStr;
	}


}
