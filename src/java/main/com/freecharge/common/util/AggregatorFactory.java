package com.freecharge.common.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.freecharge.recharge.businessdao.AggregatorInterface;

/**
 * Created by IntelliJ IDEA. User: Rananjay Date: May 9, 2012 Time: 4:51:23 PM
 */

public class AggregatorFactory implements ApplicationContextAware {
	private ApplicationContext applicationContext;

	public AggregatorInterface getAggregator(String aggregatorName) {

		if (this.getApplicationContext().containsBean(aggregatorName.toLowerCase()))
			return (AggregatorInterface) this.getApplicationContext().getBean(aggregatorName.toLowerCase());
		else
			return null;
	}

	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		this.applicationContext = ctx;
	}

	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
