package com.freecharge.common.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.freecharge.common.framework.logging.LoggingFactory;

public class FCDateUtil {
	private final static Logger logger = LoggingFactory.getLogger(FCDateUtil.class);
	
	private final static String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static Timestamp convertStringToTimestamp(String timeStampStr, String dateTimeformat) {
		if (dateTimeformat == null || dateTimeformat.isEmpty())
			dateTimeformat = DEFAULT_DATE_TIME_FORMAT;
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateTimeformat);
		try {
			Date parsedDate = dateFormat.parse(timeStampStr);
			Timestamp timestamp = new Timestamp(parsedDate.getTime());
		    return timestamp;
		} catch (ParseException e) {
			logger.info("Ignoring parsing failure while converting String: " + timeStampStr +" to Timestamp");
        }
		return null;
	}
	
	public static Date convertStringToDate(String dateStr, String dateTimeformat) {

		if (dateTimeformat == null || dateTimeformat.isEmpty())
			dateTimeformat = DEFAULT_DATE_TIME_FORMAT;
		
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateTimeformat);
		try {
			Date parsedDate = dateFormat.parse(dateStr);
		    return parsedDate;
		} catch (ParseException e) {
            logger.info("Ignoring parsing failure while converting String: " + dateStr +" to Date");
		}
		return null;
	}

    public static Date getStartOfDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
    * @param datetime
    * @param format
    * @return if given dateTime make a valid date in the format given
    **/
   public static boolean isValidDate(String dateTime, String format) {
       SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
       Calendar calendar = Calendar.getInstance();

       try {
           calendar.setTime(simpleDateFormat.parse(dateTime));
       } catch (ParseException e) {
           return false;
       }
       return true;
   }
   
   public static boolean isValidDateTimeFormat(String dateString, String format) {
		SimpleDateFormat validformat = new SimpleDateFormat(format);
		try {
			validformat.parse(dateString);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
}
