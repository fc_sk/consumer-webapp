package com.freecharge.common.util;

import java.text.ParseException;

import com.freecharge.app.domain.entity.MigrationStatus;
import com.freecharge.app.domain.entity.UserDetails;
import com.freecharge.app.domain.entity.jdbc.UserProfile;
import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.mobile.util.MapperUtil;
import com.snapdeal.ims.dto.UserDetailsDTO;
import com.snapdeal.ims.response.UserUpgradationResponse;

public class FCUserUtil {

	public static Users getUser(UserDetailsDTO userDTO) {
		Users user = null;
		if(userDTO!=null){
			user = new Users();
			user.setEmail(userDTO.getEmailId());
			user.setUserId(userDTO.getFcUserId());
			user.setForgotPasswordKey(null);
			user.setMobileNo(userDTO.getMobileNumber());
			user.setPassword(null);
			user.setIsActive(userDTO.isEnabledState());
			user.setDateAdded(userDTO.getCreatedTime());
			user.setLastUpdate(userDTO.getCreatedTime());
			user.setForgotPasswordExpiry(null);
		}
		return user;
	}

	public static UserDetails getUserDetails(UserDetailsDTO userDTO) throws ParseException{
		UserDetails user = null;
		if(userDTO!=null){
			user = new UserDetails();
			user.setEmail(userDTO.getEmailId());
			user.setUserId(userDTO.getFcUserId());
			user.setForgotPasswordKey(null);
			user.setMobileNo(userDTO.getMobileNumber());
			user.setPassword(null);
			user.setIsActive(userDTO.isEnabledState());
			user.setName(userDTO.getFirstName());
			user.setDateAdded(userDTO.getCreatedTime());
			user.setLastUpdate(userDTO.getCreatedTime());
			user.setForgotPasswordExpiry(null);
		}
		return user;
	}

	public static com.freecharge.app.domain.entity.Users getUsers(UserDetailsDTO userDetails) throws ParseException {
		com.freecharge.app.domain.entity.Users userData = null;
		if (userDetails != null ) {
			Users user = getUser(userDetails);
			userData = MapperUtil.map(user,com.freecharge.app.domain.entity.Users.class);
		}
		return userData;
	}

	public static UserProfile getUserProfile(UserDetailsDTO userDTO) {
		UserProfile userProfile = null;
		if(userDTO!=null) {
			userProfile = new UserProfile();
			userProfile.setFirstName(userDTO.getFirstName());
			userProfile.setFkUserId(userDTO.getFcUserId());
			userProfile.setIsDefault(true);
			userProfile.setIsActive(userDTO.isEnabledState());
			userProfile.setLastName(userDTO.getLastName());
			userProfile.setCreatedOn(userDTO.getCreatedTime());
			userProfile.setLastUpdate(userDTO.getCreatedTime());
		}
		return userProfile;
	}
	
	public static MigrationStatus getUserMigrationStatus(UserUpgradationResponse migrationStatus) throws ParseException {
		MigrationStatus miStatus = null;
		if (migrationStatus != null ) {
			miStatus = new MigrationStatus();
			miStatus.setMigrationStatus(migrationStatus.getUpgradationInformation().getUpgrade().toString());
		}
		return miStatus;
	}
	
}
