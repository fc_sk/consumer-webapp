package com.freecharge.common.util;

import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;

public final class ABTestingUtil {
	public static boolean isMcafeeLogoEnabled(){
		boolean isMcAfeeHeaderEnabled = false;
		FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
		if (fs != null && fs.getSessionData() != null && fs.getSessionData().get(ABTestingConstants.EXPERIMENT_HEADER_REQUEST_PARAM_NAME) != null)
    	{
			isMcAfeeHeaderEnabled = (Boolean) fs.getSessionData().get(ABTestingConstants.EXPERIMENT_HEADER_REQUEST_PARAM_NAME);
    	}
		return isMcAfeeHeaderEnabled;
	}
}
