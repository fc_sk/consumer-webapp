package com.freecharge.common.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;

import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.context.WebContext;

public class DelegateHelper {

	public static void processRequest(HttpServletRequest request, HttpServletResponse response, String delegateName) {

	}

	public static WebContext processRequest(HttpServletRequest request, HttpServletResponse response, String delegateName, Model model, BaseWebDO baseWebDO) throws Exception {
		WebContext webContext = WebContextFactory.createWebContext(delegateName);
		webContext.setRequest(request);
		webContext.setResponse(response);
		webContext.setModel(model);
		webContext.setBaseWebDO(baseWebDO);

		webContext.getWebDelegate().delegate(webContext);
		return webContext;

	}

}
