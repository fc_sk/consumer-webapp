package com.freecharge.common.util;

import com.freecharge.common.framework.util.SessionConstants;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpRequestInterceptor;

import com.freecharge.common.framework.session.FreechargeContext;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.payment.dos.business.PaymentRequestBusinessDO;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.web.util.WebConstants;
import com.freecharge.web.webdo.CommonSessionWebDo;


public class FCSessionUtil {

	public static Object getSessionPropertyValue(String propertyName) {
		Object value = null;
		if (FreechargeContextDirectory.get() != null && FreechargeContextDirectory.get().getFreechargeSession() != null){
			FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
			value = fs.getSessionData().get(propertyName);
		}
		return value;
	}
	
	public static boolean isAnotherUserLoggedIn(String newEmail) {
		Object userLoggedIn = FCSessionUtil.getSessionPropertyValue(WebConstants.SESSION_USER_IS_LOGIN);
		if ( userLoggedIn != null && (Boolean)userLoggedIn) {

			String loggedInEmail = (String) FCSessionUtil.getSessionPropertyValue(WebConstants.SESSION_USER_EMAIL_ID);
			if (!StringUtils.isEmpty(loggedInEmail) && loggedInEmail.compareToIgnoreCase(newEmail) != 0) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isUserLoggedIn() {
		boolean isUserLoggedIn = false;
		Object userLoggedIn = FCSessionUtil.getSessionPropertyValue(WebConstants.SESSION_USER_IS_LOGIN);
		if (userLoggedIn != null && userLoggedIn instanceof Boolean) {
			isUserLoggedIn = (Boolean) userLoggedIn;
		}
		return isUserLoggedIn;
	}
	
	public static String getLoggedInEmailId() {
		String loggedInEmail = null;
		Object userLoggedIn = FCSessionUtil.getSessionPropertyValue(WebConstants.SESSION_USER_IS_LOGIN);
		if (userLoggedIn != null && userLoggedIn instanceof Boolean) {
			loggedInEmail = (String) FCSessionUtil.getSessionPropertyValue(WebConstants.SESSION_USER_EMAIL_ID);
		}
		return loggedInEmail;
	}
	
	public static Integer getLoggedInUserId() {
        Object loggedInUserId = FCSessionUtil.getSessionPropertyValue(WebConstants.SESSION_USER_USERID);
        if (loggedInUserId != null) {
            return (Integer) loggedInUserId;
        } else {
            return null;
        }
	}
	
	public static String getPermanentCookie() {
		FreechargeSession session = getCurrentSession();
		String permanentCookie = null;
		if (session != null){
			permanentCookie = session.getPermanentCookie();
		}
		return permanentCookie;
	}
	
	public static String getIpAddress() {
		FreechargeSession session = getCurrentSession();
		String ipAddress = null;
		if (session != null){
			ipAddress = session.getIpAddress();
		}
		return ipAddress;
	}
	
	public static String getIMEINumber() {
		FreechargeSession session = getCurrentSession();
		String imeiNumber = null;
		if (session != null){
			imeiNumber = session.getIMEINumber();
		}
		return imeiNumber;
	}
	
	
	/**
	 * Returns the current session associated with the thread.
	 * @return
	 * 			the current associated session.
	 * 			returns null if there is no session (can happen in non web threads where {@link HttpRequestInterceptor} has not been called).
	 */
	public static final FreechargeSession getCurrentSession(){
		FreechargeSession session = null;
		FreechargeContext context = FreechargeContextDirectory.get();
		if (context != null){
			session = context.getFreechargeSession();	
		}
		return session;
	}

    public static SessionConstants.LoginSource getLoginSource(FreechargeSession fs){
        if (fs==null){
            return null;
        }
        return fs.getSessionData().get(SessionConstants.SESSION_LOGIN_SOURCE)!=null
                ? (SessionConstants.LoginSource) fs.getSessionData().get(SessionConstants.SESSION_LOGIN_SOURCE):null;
    }
    
	public static String getAppCookie(HttpServletRequest request) {
		if (request != null) {

			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equalsIgnoreCase("app_fc")) {
						return cookie.getValue();
					}
				}
			}
		} else {
			FreechargeSession session = FCSessionUtil.getCurrentSession();
			return session.getUuid();
		}
		return null;
	}

    public static String getRechargeType(String lookUpId, FreechargeSession session) {
        String type = session == null ? null : (String) session.getSessionData().get(
                lookUpId + WebConstants.SESSION_RECHARGE_TYPE);
        if (type == null) {
            CommonSessionWebDo commonSessionWebDo = session == null ? null : (CommonSessionWebDo) session
                    .getSessionData().get(lookUpId + WebConstants.SESSION_COMMON_POJO);
            if (commonSessionWebDo != null) {
                type = commonSessionWebDo.getType();
            }
        }
        return type;
    }

    public static String getOperatorName(String lookUpId, FreechargeSession session) {
        String operator = session == null ? null : (String) session.getSessionData().get(
                lookUpId + WebConstants.SESSION_OPERATOR_NAME);
        if (operator == null) {
            CommonSessionWebDo commonSessionWebDo = session == null ? null : (CommonSessionWebDo) session
                    .getSessionData().get(lookUpId + WebConstants.SESSION_COMMON_POJO);
            if (commonSessionWebDo != null) {
                operator = commonSessionWebDo.getOperatorName();
            }
        }
        return operator;
    }

    public static String getRechargeAmount(String lookUpId, FreechargeSession session) {
        String rechargeAmnt = session == null ? null : (String) session.getSessionData().get(
                lookUpId + WebConstants.SESSION_RECHARGE_AMOUNT);
        if (rechargeAmnt == null) {
            CommonSessionWebDo commonSessionWebDo = session == null ? null : (CommonSessionWebDo) session
                    .getSessionData().get(lookUpId + WebConstants.SESSION_COMMON_POJO);
            if (commonSessionWebDo != null) {
                rechargeAmnt = commonSessionWebDo.getRechargeAmount();
            }
        }
        return rechargeAmnt;
    }

    public static String getProductOrderId(String mtxnId, FreechargeSession session) {
        String productOrderId = session == null ? null : (String) session.getSessionData().get(mtxnId + "_order_id");

        if (productOrderId == null) {
            PaymentRequestBusinessDO paymentRequestBusinessDO = session == null ? null
                    : (PaymentRequestBusinessDO) session.getSessionData().get(mtxnId);
            if (paymentRequestBusinessDO != null) {
                productOrderId = paymentRequestBusinessDO.getOrderId();
            }
        }
        return productOrderId;
    }

    public static String getSuccessUrl(String mtxnId, FreechargeSession session) {
        String successUrl = session == null ? null : (String) session.getSessionData().get(mtxnId + "_success_url");

        if (successUrl == null) {
            PaymentRequestBusinessDO paymentRequestBusinessDO = session == null ? null
                    : (PaymentRequestBusinessDO) session.getSessionData().get(mtxnId);
            if (paymentRequestBusinessDO != null) {
                successUrl = paymentRequestBusinessDO.getSuccessUrl();
            }
        }
        return successUrl;
    }

    public static String getErrorUrl(String mtxnId, FreechargeSession session) {
        String errorUrl = session == null ? null : (String) session.getSessionData().get(mtxnId + "_error_url");

        if (errorUrl == null) {
            PaymentRequestBusinessDO paymentRequestBusinessDO = session == null ? null
                    : (PaymentRequestBusinessDO) session.getSessionData().get(mtxnId);
            if (paymentRequestBusinessDO != null) {
                errorUrl = paymentRequestBusinessDO.getErrorUrl();
            }
        }
        return errorUrl;
    }

    public static String getOrderAmount(String mtxnId, FreechargeSession session) {
        String amount = session == null ? null : (String) session.getSessionData().get(mtxnId + "_amount");

        if (amount == null) {
            PaymentRequestBusinessDO paymentRequestBusinessDO = session == null ? null
                    : (PaymentRequestBusinessDO) session.getSessionData().get(mtxnId);
            if (paymentRequestBusinessDO != null) {
                amount = paymentRequestBusinessDO.getAmount();
            }
        }
        return amount;
    }

    public static String getPaymentOption(String mtxnId, FreechargeSession session) {
        String paymentOption = session == null ? null : (String) session.getSessionData().get(
                mtxnId + "_payment_option");

        if (paymentOption == null) {
            PaymentRequestBusinessDO paymentRequestBusinessDO = session == null ? null
                    : (PaymentRequestBusinessDO) session.getSessionData().get(mtxnId);
            if (paymentRequestBusinessDO != null) {
                paymentOption = paymentRequestBusinessDO.getPaymentOption();
            }
        }
        return paymentOption;
    }
    
	public static String getIpAddress(HttpServletRequest request) {
		String remoteAddress = request.getHeader("x-forwarded-for");
		String ipAddress = null;

		int index = remoteAddress.indexOf(',');
		if (index > -1) {
			ipAddress = remoteAddress.substring(0, index);
			return ipAddress;
		} else {
			return remoteAddress;
		}
	}

}

