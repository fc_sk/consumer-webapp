package com.freecharge.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.freecharge.api.error.ErrorCode;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.StateMaster;
import com.freecharge.app.domain.entity.jdbc.CityMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.common.businessdo.CrosssellBusinessDo;
import com.freecharge.common.businessdo.PaymentTypeMasterBusinessDO;
import com.freecharge.common.businessdo.PaymentTypeOptionBusinessDO;
import com.freecharge.common.framework.properties.FCProperties;
import com.google.common.collect.ImmutableSet;
import com.freecharge.app.domain.entity.ProductMaster;

public class FCConstants {

    public static final String IS_RETRIGGER = "isRetrigger";
    public static final String IS_ADMIN_REVERSAL = "isAdminReversal";

    
	// Application constants to be used to configure app during startup

	public static final String SYSTEM_LOG_CONF_PATH = "system.logConfPath";
	public static final String SYSTEM_LOG_DIR = "system.logDir";
	public static final String SYSTEM_DEPLOYED_PATH = "system.deployedPath";
	public static final String SYSTEM_RUNNING_MODE = "system.ctx.mode";
	public static final String SYSTEM_APPNAME = "system.ctx.appname";

	// Poc Constants
	
	//Campaign Timeout
	public static final long CAMPAIGN_SERVICE_TIMEOUT = 3000;

	// beand defination constants
	public static final String BEAN_DEFINATION_NAME_FCPROPERTIES = "core.prop";
	public static final String BEAN_DEFINATION_NAME_SESSIONSERVICE = "sessionService";
	public static final String BEAN_DEFINATION_NAME_SESSIONMANAGER = "sessionManager";
	public static final String BEAN_DEFINATION_NAME_AUDITTRAILMANAGER = "auditTrailManger";
	public static final String BEAN_DEFINATION_NAME_COUPON_REDEEMPTION_SERVICE = "couponRedeemptionService";
	public static final String BEAN_DEFINATION_NAME_MAIL_SENDER = "mailSender";
	public static final String BEAN_DEFINATION_NAME_JSON_AUTOFILL_OPR_LIST = "jsonOperatorPrefixList";
	public static final String BEAN_DEFINATION_NAME_APP_CONFIG_SERVICE = "appConfigService";
	public static final String FC_GENERAL_ERROR_MESSAGE = "error_message";

	// appconstants
	public static final String APPLICATION_COOKIE_NAME = "app.cookie.name";
	public static final String APPLICATION_COOKIE_PATH = "app.cookie.path";
	public static final String XFF_REQUEST_HEADER = "X-Forwarded-For";
	public static final String APPLICATION_REMEMBERME_COOKIE_USERID = "app.rememberme.cookie.userid";
	public static final String APPLICATION_REMEMBERME_COOKIE_AGE = "app.rememberme.cookie.age";
	public static final String APPLICATION_REMEMBERME_COOKIE_DOMAINNAME = "app.rememberme.cookie.domain";
	public static final String APPLICATION_REMEMBERME_COOKIE_PATH = "app.rememberme.cookie.path";

	public static final String APPLICATION_SERVICE_CHARGE_KEY = "application.service.charges";

	public static final String FREEBILL_CONVENIENCE_CHARGE_KEY = "freebill.convenience.charges";
	
	public static final String HANDLING_CHARGE_MOBILE = "mobile.application.service.charges";
    public static final String HANDLING_CHARGE_BILL = "bill.application.service.charges";
	public static final String HANDLING_CHARGE_DTH = "dth.application.service.charges";
	public static final String HANDLING_CHARGE_DATACARD = "datacard.application.service.charges";
	
	public static final String MOBILE_APPLICATION_SERVICE_CHARGE_KEY = "application.service.charges";
	public static final String DTH_APPLICATION_SERVICE_CHARGE_KEY = "application.service.charges";
	public static final String DATACARD_APPLICATION_SERVICE_CHARGE_KEY = "application.service.charges";
	
	public static final String MEMCACHE_READ_TIMEOUT_SECONDS = "memcache.read.timeout.seconds";
	
	// IN status check URL
	public static final String OXI_STATUS_CHECK_URL = "in.status.url.oxigen";
	
	// Timeouts for aggregators.
	public static final String OXI_TIMEOUT_MILLISECONDS = "oxigen.api.timeout";
	public static final String EURONET_TIMEOUT_MILLISECONDS = "euronet.api.timeout";
    public static final String SUVIDHAA_TIMEOUT_MILLISECONDS = "suvidhaa.api.timeout";
    
    //Timeout for Validation API
    public static final String EURONET_VALIDATION_API_TIMEOUT = "euronet.validation.api.timeout";
    
    //Euronet Host
    public static final String EURONET_HOST ="euronet.host";
    public static final String EURONET_HTTPS_HOST ="euronet.https.host";
    
    // Timeouts for bill payment
    public static final String BILLDESK_BILL_PAYMENT_CONN_TIMEOUT = "billdesk.bill.payment.conn.timeout.millis";
    public static final String BILLDESK_BILL_PAYMENT_READ_TIMEOUT = "billdesk.bill.payment.read.timeout.millis";
    
    // Timeouts on/off for aggregators.
    public static final String OXI_TIMEOUT_ENABLED = "oxigen.api.timeout.enabled";
    public static final String EURONET_TIMEOUT_ENABLED = "euronet.api.timeout.enabled";
    public static final String SUVIDHAA_TIMEOUT_ENABLED = "suvidhaa.api.timeout.enabled";
    
	// default content url

	public static final String DEFAULT_CONTENT_URL = "http://localhost:8080/content";
	public static final String HOSTPREFIX = "hostprefix";
	public static final String IMGPREFIX = "imgprefix1";
	public static final String VOUCHERPREFIX = "voucherprefix";
	public static final String COUPON_TNC_PREFIX = "coupon_tnc_prefix";
	
	// recon related
	public static final String RECON_BUCKET = "recon.s3.bucket";
    public static final String RECON_UPLOAD_DIRECTORY = "recon.upload.dir";
    public static final String RECON_REPORTS_DIRECTORY = "recon.reports.dir";
    
    // login interceptor enabled
    public static final String LOGIN_INTERCEPTOR_ENABLED = "login.check.enabled";

	// payment portal url property this is with slash
	public static final String PAYMENT_PORTAL_URL_PROPERTY = "freecharge.payment.portal.url";
	public static final String PAYMENT_PORTAL_HASHKEY_KEY = "freecharge.payment.portal.securekey";

	public static final String SESSION_DATA_USERCONTACTDETAIL = "ContactDetails";
	/**
	 * ************* CouponRedeemptionService Constants *************
	 */

	public static final String COUPON_ENTITY_ID = "site_merchant";
	public static final String COUPON_REDEEM_STATUS_KEY = "redeemStatus";
	public static final String COUPON_REDEEM_VALUE_KEY = "redeemValue";
	public static final String COUPON_REDEEM_MESSAGE_KEY = "redeemMessagec";

	public static final String COUPON_ADDITIONAL_THRESHOLD_COUNT = "coupon.additional.threshold.count";
	
	// Number of coupons which if an order ID crosses indicates that something is wrong.
	public static final String COUPON_ORDER_THRESHOLD = "coupon.orderid.threshold.count";
	
	public static final String REDEEM_EXPIRY_WINDOW = "coupon.redeem.expiry.window";
	
	public static final String API_AUTH_POLICY = "api.auth.policy";
	public static final String IP_WHITE_LIST = "api.ip.white.list";


    //facebook clientid and clientsecret
    public static final String FB_CLIENT_ID = "fb.client.id";
    public static final String FB_CLIENT_SECRET = "fb.client.secret";
    public static final String FB_SCOPE = "fb.scope";
    public static final String FB_REDIRECT_URI = "fb.redirect.url";
    public static final String FB_DEFAULT_MIN_POSTS_COUNT = "fb.default.min.posts.count";
    public static final String FB_DEFAULT_LAST_POST_DAY_DIFFERENCE = "fb.default.last.post.day.difference";

    //facebook clientid and clientsecret
    public static final String GOOGLE_CLIENT_ID = "google.client.id";
    public static final String GOOGLE_CLIENT_SECRET = "google.client.secret";
    public static final String GOOGLE_REDIRECT_URI = "google.login.redirect.uri";
    public static final String APP_ID = "app.id";
    public static final String APP_SECRET = "app.secret";
    public static final String ORDER_ID = "orderId";
    public static final String PAYMENT_TYPES = "paymentTypes";
    public static final String EMAIL = "email";
    public static final String CAMPAIGN_ID = "campaignId";
    public static final String CODE_PREFIX = "codePrefix";
    public static final String CODE_LENGTH = "codeLength";
    public static final String CODE_TYPE = "codeType";
    public static final String REFERRAL_API_SECRET = "secret";
    public static final String REFERRAL_API_SECRET_PROPERTY = "referral.api.secret";
    public static final String GENERATE_CODES_DIRECTORY = "generate.codes.directory";
    public static final String FILE_DIVIDING_FACTOR = "generate.codes.filedividingfactor";
    public static final int    ANDROID_APP_CHANNEL_ID = 3;
    public static final String PROMOCODE_APP_VERSION = "promocodeAppVersion";
    public static final String FC_CHANNEL = "fcChannel";
    public static final String FC_ANDROID_APP_VERSION = "fcversion";
    public static final Object ANDROID_VERSION_KEY = "android";
    public static final int UPLOAD_CACHE_LIMIT = 500000;
    public static final String BATCH_SIZE_FREEFUND_CODE = "batchSize";
    public static final String FREEFUND_CLASS_ID = "freefundClassId";
    public static final String FREEFUNDCLASS   = "ffClass";
    public static final String USERID = "userId";
    public static final String PROMOCODE = "promocode";
    public static final String CREATED_ON = "created_on";
    public static final String STATUS = "status";
    public static final String BLOCKED = "BLOCKED";
    public static final String REDEEMED = "REDEEMED";
    public static final String UNBLOCKED = "UNBLOCKED";
    public static final String REWARDED = "REWARDED";
    public static final String DEVICE_UNIQUE_ID = "deviceUniqueId";
    public static final String IMSI = "imsi";
    public static final String IS_EMULATOR = "isEmulator";
    public static final int RETRIEVE_BATCH_SIZE_FREEFUND_CODE = 10000;
    public static final String CARDHASH   = "cardHash";
    public static final String CREATEDAT = "createdAt";
    public static final String DEALID = "dealId";
    public static final String MSG_MAX_REDEMPTION_REACHED = "msg.max.limit.reached.contact.cs";
    public static final String TAILSTORM_SPECIAL_OFFER = "TailStormSpecialOfferKey";

    public static final String NOTIFICATIONS_SENT = "notificationsSent";
    public static final String GOSF_OFFER = "GOSFOfferKey";
    public static final String DIWALI_OFFER = "DiwaliOfferKey";
    public static final String CS_REISSUE_PROMOCODES_CAMPAIGN_KEY = "CSRechargeFailurePromocodeCampaign";
    public static final String PROMOCODE_API_FREECHARGE_AFFILIATE_ID = "freecharge";
    public static final String AWS_ACCESS_KEY = "freefund.aws.access.key";
    public static final String AWS_SECRET = "freefund.aws.secret";
    public static final String AWS_FREEFUND_BUCKET = "promocodefiles-s3";
    public static final String ADVERTISEMENT_ID_CHECK_ENABLED = "advertisement.id.check.enabled";
    public static final String IMEI_ADVID_CONDITION_PROPERTIES = "ImeiAdvIdConditionProperties";
    public static final String SHOULD_RETURN_ADVERTISEMENTID_FRAUD = "should.return.advertisementid.fraud";
    public static final String REFERRAL_CAMPAIGN_KEY = "ReferralCampaignKey";
    public static final String IS_REFERRAL_ENABLED = "isReferralEnabled";
    public static final String DOUBLE_REWARD_CREATE_EMAIL_TEMPLATE = "additionalRewardCreated.vm";
    public static final String DOUBLE_REWARD_EDIT_EMAIL_TEMPLATE = "additionalRewardEdited.vm";
    public static final String PROMO_HANDLING_KEY = "PromoHandlingKey";
    public static final String IS_PROMO_HANDLING_ENABLED = "isPromoHandlingEnabled";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String TIMESTAMP = "timestamp";
    public static final String IVRSClient = "IVRSClient";
    public static final String IVRID = "ivrId";
    public static final String LENGTH = "length";
    public static final String MSG_TAC_CONDITION_FAILURE = "tac.failure.message";
    public static final String SLEEP_TIME_UPLOAD_CODES = "sleepTime";
    public static final String UPLOAD_CODE_CONFIG = "uploadCodeConfig";
    public static final String OPERATOR_CIRCLE = "operatorCircle";
    public static final String OPERATOR = "operator";
    public static final String MSG_OPERATOR_CIRCLE_CONDITION = "msg.operator.circle.condition";
    public static final String MAPPING_KEYS = "mappingKeys";
    public static final String PROMO_API_KEY = "promocodeApiKey";
    public static final String NICKNAME = "nickName";
    public static final String SERVICE_NUMBER = "serviceNumber";

    public static final String MTXN_ID = "mtxnId";
    public static final String SD_IDENTITY = "sdIdentity";
    public static final String EVENT_CONTEXT = "eventContext";
    public static final String AMOUNT_WALLET = "Amount";
    public static final String EXPIRY_DURATION = "expiryDuration";
    public static final String ONECHECK_TXN_TYPE = "oneCheckTxnType";
    public static final String CODE = "code";
	public static final String LUHN_VALIDATION_KEY = "luhnValidationKey";
	public static final String LUHN_VALIDATION_ENABLED = "luhn.validation.enabled";
	public static final String UPGRADE_COMPLETED = "UPGRADE_COMPLETED";
	public static final String MESSAGE_NOT_MIGRATED_USER_ERROR = "msg.user.not.migrated.error";
	public static final String LINK_SD_ACCOUNT = "LINK_SD_ACCOUNT";
	public static final String LINK_FC_ACCOUNT = "LINK_FC_ACCOUNT";
	public static final String PAYMENT_OPTIONS_KEY = "paymentOptionsKey";
	public static final String ALL_ENABLED = "allEnabled";
	public static final String PERCENTCREDITS = "PERCENTCREDITS";
	public static final Integer ADD_CASH_CAMPAIGN_MERCHANT_ID = 6;
	public static final String PROMOCODE_STATUS = "promocodeStatus";
	public static final String FREEFUND_CLASS_REWARD_ID_MAPPING = "freefundClassRewardIdMapping";
	public static final String REWARD_REQUEST_QUEUE_NAME = "reward.request.queue.name";
	public static final String REWARD_REQUEST_QUEUE_URL = "reward.request.queue.url";
	public static final String REWARD_SQS_SECRET = "fdn.sqs.secret.key";
	public static final String REWARD_REQUEST_SQS_ACCESS_KEY = "fdn.sqs.access.key";
	public static final String PROMOCODE_HASH_CHECK = "promocode.hash.fraud.check";
	public static final String IS_CHECK_ENABLED = "fraud.check.enabled";
	public static final String ENABLED_CHANNELS = "fraud.check.enabled.channels";
	public static final String CUT_OFF_VERSION_CHECK = "cutoff.version.check";
	public static final String CUT_OFF_CHECK_ENABLED = "cutoff.check.enabled";
	public static final String CUT_OFF_VERSION = "cutoff.version";
	public static final int FREEFUND_MERCHANT_ID = 9;
	public static final String MSG_CHANNEL_BLOCKED_FOR_OLD_API = "msg.channel.blocked.for.old.api";
	public static final String NEW_PLANS_GET_CATEGORY_API = "new.plans.api.get.category";
	public static final String CAMPAIGN_PAYMENT_CONDITION_FAILURE = "CAMPAIGN_PAYMENT_CONDITION_FAILURE";
	public static final String PROMOCODE_PAYMENT_INITIATE_CHECK = "payment.initiate.check";
	public static final String WALLET_S2S_KEY = "walletS2SKey";
	public static final String S2S_ANDROID_VERSION_KEY = "s2sAndroidVersionKey";
	public static final String S2S_ANDROID_VERSION = "s2sAndroidVersion";
	public static final String ITTP_ANDROID_VERSION_KEY = "ittpAndroidVersionKey";
	public static final String ITTP_ANDROID_VERSION = "ittpAndroidVersion";
	public static final String ITTP_IOS_VERSION_KEY = "ittpIosVersionKey";
	public static final String ITTP_IOS_VERSION = "ittpIosVersion";
	public static final String WALLET_S2S_ENABLED = "walletS2SEnabled";
	public static final String WALLET_S2S_PERC_KEY = "walletS2SPercKey";
	public static final String WALLET_S2S_PERC = "walletS2SPerc";
	public static final String FPS_FLAG = "fpsFlag";
	public static final String FPS_ENABLED = "fpsEnabled";
	public static final Object IS_CHECK_ENABLED_FOR_BLOCK = "fraud.check.enabled.for.block";

	public static long SIX_MONTHS_IN_SECONDS = 185; //Expiry in number of days
    public static final int VOUCHER_SIZE_LIMIT = 25;

	public static final String DEFAULT_WALLET_LOAD_REVERSE_REASON = "NO REASON ASSIGNED FOR REVERSAL";
	public static final String WALLET_LOAD_REVERSAL_SUCCESS = "SUCCESSFULLY REVERSED TRANSACTION";
	public static final String WALLET_LOAD_REVERSAL_FAILURE = "TRANSACTION REVERSAL FAILURE";

	
    public static final String CAMPAIGN_SERVICE_CLIENT = "webapp";
    public static final String PROMO_FAILED_EMAIL_KEY = "promocodeFailedEmailEnabled";
    public static final String SEND_MAIL = "sendMail";

    public static final String PROMOTION_CORP_ID = "onecheck.promotion.corpId";
    public static final String COMPENSATION_CORP_ID = "onecheck.compensation.corpId";
    public static final String CORP_ID = "corpId";

    public static final String CORP_ID_UTIL_ELECTRICITY = "fc.corp.id.util.electricity";
    public static final String CORP_ID_UTIL_GAS = "fc.corp.id.util.gas";
    public static final String CORP_ID_UTIL_OTHERS = "fc.corp.id.util.others";
    
    public static final String REFERENCE_ID = "referenceId";
    public static final String IS_PARKED = "isParked";
    public static final String AMOUNT = "amount";
    // Order Id constants	@Autowired
	private FCProperties fcProperties;

	

	public static final int ORDER_ID_LENGTH = 7;
	public static final int INITIAL_ORDER_ID = 1;
	public static final int ORDER_ID_INC = 1;
	public static final int DEFAULT_AFFILIATE_ID = 1;
	public static final int DEFAULT_CHANNEL_ID = 1;

	// Company Code

	public static final String COMPANY_CODE = "FC";
	public static final String INVALID_ORDER_ID_SUFFIX = "_NA";
	
	
	
	public static final String SUCCESS = "Success";
	public static final String FAIL = "Fail";
	public static final String ERROR = "Error";
	public static final String SUCCESS_STATUS = "success";
    public static final String FAILURE = "failure";
	public static final String MESSAGE = "message";

	public static final int PRODUCT_ID_RECHARGE_MOBILE     = 1;
	public static final int PRODUCT_ID_RECHARGE_DTH        = 2;
	public static final int PRODUCT_ID_RECHARGE_DATACARD   = 3;
	public static final int PRODUCT_ID_POSTPAID_DATACARD   = 12;
	public static final int PRODUCT_ID_COUPONS             = 4;
	public static final int PRODUCT_ID_CROSS_SELL          = 5;
	public static final int PRODUCT_ID_CHARGES             = 6;
	public static final int PRODUCT_ID_MASTERCARD          = 7;
	public static final int PRODUCT_ID_FREEFUND_DISCOUNT   = 8;
	public static final int PRODUCT_ID_FREEFUND_CASHBACK   = 81;
	public static final int PRODUCT_ID_FREEFUND_BINOFFER   = 9;
	public static final int PRODUCT_ID_WALLET_CASH         = 10;
	public static final int PRODUCT_ID_BILL_MOBILE         = 201;
	public static final int PRODUCT_ID_PAID_COUPON 		   = 15;
    public static final int PRODUCT_ID_BILL                = 311;
    public static final int PRODUCT_ID_LANDLINE            = 312;
    public static final int PRODUCT_ID_ELECTRICITY         = 313;
    public static final int PRODUCT_ID_INSURANCE           = 314;
    public static final int PRODUCT_ID_GAS                 = 315;
    public static final int PRODUCT_ID_METRO               = 316;
    public static final int PRODUCT_ID_BROADBAND           = 317;
    public static final int PRODUCT_ID_HCOUPONS            = 16;
    public static final int PRODUCT_ID_WATER			   = 319;
    public static final int PRODUCT_ID_GOOGLE_CREDITS	   = 21;
    
    public static final String PRODUCT_TYPE_MOBILE = "V";
    public static final String PRODUCT_TYPE_DTH = "D";
    public static final String PRODUCT_TYPE_DATACARD = "C";
    public static final String PRODUCT_TYPE_POSTPAID_DATACARD = "F";
    public static final String PRODUCT_TYPE_POSTPAID = "M";
    public static final String PRODUCT_TYPE_BILL = "B";
    public static final String PRODUCT_TYPE_LANDLINE = "L";
    public static final String PRODUCT_TYPE_ELECTRICITY = "E";
    public static final String PRODUCT_TYPE_INSURANCE = "I";
    public static final String PRODUCT_TYPE_GAS = "G";
    public static final String PRODUCT_TYPE_METRO = "Z";
    public static final String PRODUCT_TYPE_BROADBAND = "Y";
    public static final String PRODUCT_TYPE_HCOUPON = "H";
    public static final String PRODUCT_TYPE_WATER = "W";
    public static final String PRODUCT_TYPE_GOOGLE_CREDITS = "X";

    // PPI changes
    public static final String PPI_SWITCHES = "ppiSwitches";
    public static final String PPI_GVLOAD_IS_CREDIT_CARD_ALLOWED = "gvLoadIsCreditCardAllowed";


	public static final Map<Integer, String> productMasterMap = FCUtil.createMap(
			PRODUCT_ID_RECHARGE_MOBILE,   "V",
			PRODUCT_ID_RECHARGE_DTH,      "D",
			PRODUCT_ID_RECHARGE_DATACARD, "C",
			PRODUCT_ID_POSTPAID_DATACARD, "F",
			PRODUCT_ID_WALLET_CASH,       "T",
			PRODUCT_ID_BILL_MOBILE,       "M",
			PRODUCT_ID_BILL, "B",
			PRODUCT_ID_ELECTRICITY, "E",
			PRODUCT_ID_LANDLINE, "L",
			PRODUCT_ID_INSURANCE, "I",
			PRODUCT_ID_GAS, "G",
			PRODUCT_ID_METRO, "Z",
			PRODUCT_ID_COUPONS, "N",
			PRODUCT_ID_HCOUPONS, "H",
			PRODUCT_ID_BROADBAND, "Y",
			PRODUCT_ID_WATER, "W",
			PRODUCT_ID_GOOGLE_CREDITS, "X");

	public static final Map<String, Integer> productMasterMapReverse = FCUtil.createMap(   
			"V", PRODUCT_ID_RECHARGE_MOBILE,
			"D", PRODUCT_ID_RECHARGE_DTH,
			"C", PRODUCT_ID_RECHARGE_DATACARD,
			"F", PRODUCT_ID_POSTPAID_DATACARD,
			"T", PRODUCT_ID_WALLET_CASH,
			"M", PRODUCT_ID_BILL_MOBILE,
			"B", PRODUCT_ID_BILL,
			"E", PRODUCT_ID_ELECTRICITY,
            "L", PRODUCT_ID_LANDLINE, 
            "I", PRODUCT_ID_INSURANCE,
            "G", PRODUCT_ID_GAS,
            "Z", PRODUCT_ID_METRO,
			"H", PRODUCT_ID_HCOUPONS,
			"Y", PRODUCT_ID_BROADBAND,
			"W", PRODUCT_ID_WATER,
			"X", PRODUCT_ID_GOOGLE_CREDITS);

	public static final Map<String, String> productTypeToNameMap = FCUtil.createMap(
			"V", "Mobile",
			"D", "DTH",
			"C", "DataCard",
			"F", "PostPaidDataCard",
			"T", "WalletCash",
			"M", "MobilePostpaid",
			"B", "Bill",
			"L", "Landline",
			"E", "Electricity",
			"I", "Insurance",
			"G", "Gas",
			"Z", "Metro",
			"H", "HCoupon",
			"Y", "Broadband",
			"W", "Water",
			"X", "GoogleCredits");
	
	public static final Map<String,String> productNameToTypeMap = FCUtil.createMap(
			"Mobile", PRODUCT_TYPE_MOBILE,
			"DTH", PRODUCT_TYPE_DTH,
			"DataCard", PRODUCT_TYPE_DATACARD,
			"PostPaidDataCard", PRODUCT_TYPE_POSTPAID_DATACARD,
			"MobilePostpaid", PRODUCT_TYPE_POSTPAID,
			"WalletCash", "T",
			"Bill", PRODUCT_TYPE_BILL,
			"Landline", PRODUCT_TYPE_LANDLINE,
            "Electricity", PRODUCT_TYPE_ELECTRICITY,
            "Insurance", PRODUCT_TYPE_INSURANCE,
            "Gas", PRODUCT_TYPE_GAS,
            "Metro", PRODUCT_TYPE_METRO,
			"HCoupon", PRODUCT_TYPE_HCOUPON,
			"Broadband", PRODUCT_TYPE_BROADBAND,
			"Water", PRODUCT_TYPE_WATER,
			"GoogleCredits", PRODUCT_TYPE_GOOGLE_CREDITS);

	public static final String PRODUCT_IDS_FOR_RECHARGE_PLANS[] = {"V", "C", "D"};
	public static final String RECHARGE_PLAN_TYPES[] = {"topup", "special"};
	public static final ArrayList<String> PRODUCT_IDS_FOR_IN_REQUEST = new ArrayList<String>() {
        private static final long serialVersionUID = 1L;
    {
	    add("V");
	    add("C");
	    add("D");
	    add("F");
	    add("X");
	}};
	public static final Map<Integer, String> afflicateMasterMap = FCUtil.createMap(
			1, "FC",
			2, "XX",
			3, "SD",
			50, "AT",
			70, "BD");

	/**
	 * W- Web
	 * M- Mobile
	 * A- Mobile App
	 * S- Service
	 */
	public static final Map<Integer, String> channelMasterMap = FCUtil.createMap(
			1, "W",
			2, "M",
			3, "A",
			4, "S",
			5, "I",
			6, "C",
			11, "Z",
			12, "T",
			17, "U",
			18, "P");
	
	public static final Map<String, Integer> channelNameMasterMapReverse = FCUtil.createMap(
            "WEB",1,
            "Moblie",2,
            "Android",3,
            "ANDROID", 3,
            "Service",4,
            "IOS",5,
            "Windows",6,
            "Moolah",11,
            "Tizen",12,
            "UWP",17,
            "Autopay",18);
	
	public static final Map<String, String> channelNameMasterMap = FCUtil.createMap(
            "W", "Web",
            "M", "Moblie",
            "A", "Android",
            "S", "Service",
            "I", "IOS",
            "C", "Windows",
            "Z", "Moolah",
            "T", "Tizen",
            "U", "UWP",
            "P", "Autopay");
	
	public static final Map<String, String> operatorUSSDMap = FCUtil.createMap(
			"Aircel", "*125#",
			"Airtel", "*123#",
			"BSNL", "*123#",
			"Idea", "*130#",
			"MTNL Delhi", "*444#",
			"MTNL Mumbai", "*444#",
			"MTS", "*225#",
			"Reliance-CDMA", "*367#",
			"Reliance-GSM", "*367#",
			"T24", "*111#",
			"Tata Docomo CDMA", "*111#",
			"Tata Docomo GSM", "*111#",
			"Uninor", "*222*2#",
			"Videocon Mobile", "*123*1#",
			"Virgin CDMA", "*111#",
			"Virgin GSM", "*111#",
			"Vodafone", "*111#",
			"Indicom-Walky", "*141#",
			"Tata Indicom", "*141#",
			"JIO","*333#"
			);
	
	public static final String PAYMENT_REQUEST_CHANNEL_KEY="fcChannel";
	public static final String CHANNEL_KEY = "channelKey";
	public static final String IMEI = "imei";
	public static final int CHANNEL_ID_WEB = 1;
	public static final int CHANNEL_ID_MOBILE = 2;
	public static final int VERSION_MOBILE_APP = 1;
	public static final int CHANNEL_ID_ANDROID_APP = 3;
	public static final int CHANNEL_ID_SERVICE = 4;
	public static final int CHANNEL_ID_IOS_APP = 5;
	public static final int CHANNEL_ID_WINDOWS_APP = 6;
	public static final int CHANNEL_ID_MOOLAH_APP = 11;
	public static final int CHANNEL_ID_TIZEN_PHONE = 12;
	public static final int CHANNEL_ID_UWP_APP = 17;
	public static final int CHANNEL_ID_AUTO_PAY = 18;
	
	public static final int AFFILIATE_ID_WEB = 11;
	public static final int AFFILIATE_ID_MOBILE = 12;
	public static final int AFFILIATE_ID_ANDROID_APP = 13;
	public static final int AFFILIATE_ID_IOS_APP = 15;
	public static final int AFFILIATE_ID_WINDOWS_APP = 16;
	public static final int AFFILIATE_ID_MOOLAH_APP = 211;
	public static final int AFFILIATE_ID_TIZEN_PHONE = 212;
	public static final int AFFILIATE_ID_UWP_APP = 213;
	public static final int AFFILIATE_ID_ATM = 50;
	public static final int AFFILIATE_ID_BDK = 70;
	
	
	public static final String KEY_COUPON_MERCHANT_ID = "merchantId";
	public static final String ATM_ID = "atmId";
	
	// Look up id
	public static final String LOOKUPID = "lid";
	public static final String FREEBILL = "freebill";

	
	public static final Map<String, String> productNameToIdMap = FCUtil.createMap(
			"Mobile",   "1",
			"DTH",      "2",
			"DataCard", "3");
	
	public static final int MOBILE_PRODUCT_MASTER_ID = 1;
	public static final int DTH_PRODUCT_MASTER_ID = 2;
	public static final int DATACARD_PRODUCT_MASTER_ID = 3;
	public static final int FREEFUND_PRODUCT_MASTER_ID = 8;

	public static final int PRODUCT_TYPE_PRIMARY    = 1;
	public static final int PRODUCT_TYPE_SECONDARY  = 2;
	public static final int PRODUCT_TYPE_CHARGES    = 3;
	public static final int PRODUCT_TYPE_FREEFUND   = 4;
	public static final int PRODUCT_TYPE_WALLETCASH = 5;
	
	
	public static final Map<Integer, String> productTypeMap = new HashMap<Integer, String>() {
		{
			put(1, "primary");
			put(2, "secondary");
			put(3, "charges");
            put(4, "freefund");
            put(5, "WalletCash");
		}
	};

	public static final Map<String, Integer> productTypeMapReverse = new HashMap<String, Integer>() {
		{
			put("primary", 1);
			put("secondary", 2);
			put("charges", 3);
            put("freefund", 4);
            put("WalletCash", 5);
		}
	};
	
	public static final Map<String, String> productTypeToCorpID = new HashMap<String, String>() {
		{
			put(ProductName.Electricity.getProductType(), CORP_ID_UTIL_ELECTRICITY);
			put(ProductName.Gas.getProductType(), CORP_ID_UTIL_GAS);
			put(ProductName.Water.getProductType(), CORP_ID_UTIL_OTHERS);
		}
	};

	public static final String PRIMARY_PRODUCT_TYPE = "primary";
	public static final String SECONDARY_PRODUCT_TYPE = "secondary";
	public static final String CHARGES_PRODUCT_TYPE = "charges";
    public static final String FFREEFUND_PRODUCT_TYPE = "FreeFund";
    public static final String WALLET_FUND_PRODUCT_TYPE = "T";
    public static final String WALLET_FUND_DISPLAY_LABLE = "Balance Fund";
    public static final String FORCE_SOCIAL_CONNECT = "forceSocialConnect";

	public static final Map<Integer, Integer> productMasterType = FCUtil.createMap(
			PRODUCT_ID_RECHARGE_MOBILE,   PRODUCT_TYPE_PRIMARY,
			PRODUCT_ID_RECHARGE_DTH,      PRODUCT_TYPE_PRIMARY,
			PRODUCT_ID_RECHARGE_DATACARD, PRODUCT_TYPE_PRIMARY,
			PRODUCT_ID_POSTPAID_DATACARD, PRODUCT_TYPE_PRIMARY,
			PRODUCT_ID_COUPONS,           PRODUCT_TYPE_SECONDARY,
			PRODUCT_ID_CROSS_SELL,        PRODUCT_TYPE_SECONDARY,
			PRODUCT_ID_CHARGES,           PRODUCT_TYPE_CHARGES,
			PRODUCT_ID_MASTERCARD,        PRODUCT_TYPE_FREEFUND,
			PRODUCT_ID_WALLET_CASH,       PRODUCT_TYPE_WALLETCASH,
			PRODUCT_ID_BILL_MOBILE,       PRODUCT_TYPE_PRIMARY,
			PRODUCT_ID_BILL,              PRODUCT_TYPE_PRIMARY,
			PRODUCT_ID_HCOUPONS,		  PRODUCT_TYPE_SECONDARY,
			PRODUCT_ID_GOOGLE_CREDITS,	  PRODUCT_TYPE_PRIMARY);

	public static Map<String, List<PaymentTypeMasterBusinessDO>> paymentTypesForProductAffiliate = new HashMap<String, List<PaymentTypeMasterBusinessDO>>();
	public static Map<Integer, List<PaymentTypeOptionBusinessDO>> paymentTypesForRelation = new HashMap<Integer, List<PaymentTypeOptionBusinessDO>>();
    public static List<StateMaster> stateMasterList;
    public static Map<Integer, CityMaster> allCitiesMasterMap;
    public static final String CITY_ALL = "All";
    public static final String NO_CITY_FOUND = "NO_CITY_FOUND";
    
    public static List<CrosssellBusinessDo> crosssellCoupons = null;

	public static final String CROSSSELL_PRODUCT_DELIMITER = "|";

	public static final Integer RECORDSPERPAGE = 1;

	public static final String ENTITY_NAME_FOR_CROSSSELL = "Crosssell";
	public static final String ENTITY_NAME_FOR_FREEFUND = "FreeFund";
	public static final String STATUS_REDEEMED = "REDEEMED";
	public static final String STATUS_BLOCKED = "BLOCKED";
	public static final String STATUS_ACTIVE = "ACTIVE";
	public static final Integer DEFAULT_COUNTRY_ID = 99;
	public static final Integer DEFAULT_STATEID = 1;
	public static final Integer MASTERCARD_MULTIPLY_FACTOR = 2;

	public static final String DISCOUNT_TYPE_FLAT = "FLAT";
	public static final String DISCOUNT_TYPE_PERCENT = "PERCENT";	
	
    //meta and seo data
    public static final String KEY_HOMEPAGE_TITLE = "meta.homepage.title";

	//EMAIL NOTIFICATION CONSTANT
	public static final String NOTIFICATION_ADMIN_EMAIL_KEY="exception.notification.admin.email";
	public static final String AGGREGATOR_BALANCE_USGE="aggregator.balance.usage";
	public static final String NOTIFICATION_SENDER = "mail.smtp.sender";
	public static final String SERVER_EXCEPTION_TEMPLATE = "serverExceptionNotification.vm";
	public static final String RECHARGE_RESPONSE_TIME = "rechargeResponseTime.vm";
	public static final String INVENTORY_REPORT = "inventoryReport.vm";
	public static final String DEFAULT_DTH_CIRCLE  = "ALL";
	public static final String COUPON_INVENTORY_NOTIFICATION_WHEN_REACHED_THRESHOLD_VALUE_VM  = "couponInventoryNotification.vm";
	public static final String COUPON_STATUS_NOTIFICATION_VM ="couponStatusNotification.vm";
	public static final String HOURLY_STATISTICS_NOTIFCATION_VM = "hourlyPerformanceReport.vm";
	public static final String DAILY_STATISTICS_NOTIFCATION_VM = "dailyPerformanceReport.vm";
	public static final String UPSELL_STATISTICS_NOTIFCATION_VM = "upsellHourlyReport.vm";	
	public static final String  GROUP_TEAM_FROM_EMAIL_ID= "group.team.from.emailid";
	public static final String  GROUP_TEAM_TO_EMAIL_ID= "group.team.to.emailid";
	public static final String  GROUP_TEAM_BCC_EMAIL_ID="group.team.bcc.emailid";
	public static final String  PROMOREWARD_FROM_EMAIL= "promoreward.email.from";
	public static final String CAMPAIGN_CREATE_UPDATE_NOTIFCATION_VM = "campaignCreateUpdateNotification.vm";
	public static final String ADMIN_UPDATE_NOTIFICATION_EMAIL = "admin.update.notification.email";
	public static final String FREEFUND_CODES_UPDATE_NOTIFICATION_EMAIL = "freefund.codes.upload.notification.email";
	public static final String FREEFUND_COUPON_UPLOAD_NOTIFICATION_ENABLED = "freefund.coupon.upload.notification.enabled";
	
	//Recharge Retry Expiry Alert Constant
	public static final String RETRY_ALERT_EMAIL_TO = "retry.alert.email.to";

	//Recharge Retry Expiry Alert Constant
	public static final String RECHARGE_COUNTER_WINDOW_INTERVAL_DAYS = "recharge.frequency.window.size";

	//GCM NOTIFICATION CONSTANT
    public static final String POST_CUSTOM_GCMMC_URL = "post.custom.gcmmc.url";
	//Recharge Amount Related
	public static final Integer MAX_RECHARGE_AMOUNT  = 10000;
	public static final Integer RECHARGE_INC_AMOUNT  = 50;
	public static final Integer MIN_RECHARGE_AMOUNT  = 10;
	public static final Integer MAX_RECHARGE_AMOUNT_MOBILE  = 1000;
	public static final Integer MAX_RECHARGE_AMOUNT_DTH  = 10000;
	public static final Integer MAX_RECHARGE_AMOUNT_DATACARD  = 1500;
	public static final Integer PREFIX_LENGTH_FIVE_DIGIT  = 5;
	public static final Integer PREFIX_LENGTH_FOUR_DIGIT  = 4;
	public static final String ELIGIABLECOUPONS  = "eligiableCoupons";
	
	// Recharge Type Related
	public static final String RECHARGE_TYPE_TOPUP    = "topup";
	public static final String RECHARGE_TYPE_SPECIAL  = "special";
	
	//DTH Recharge Amount for particular operator
	public static final Integer MIN_RECHARGE_AMOUNT_DTH_DISH_TV  = 10;
	public static final Integer MAX_RECHARGE_AMOUNT_DTH_DISH_TV  = 5000;
	
	public static final Integer MIN_RECHARGE_AMOUNT_DTH_BIG_TV  = 100;
	public static final Integer MAX_RECHARGE_AMOUNT_DTH_BIG_TV  = 5000;
	
	public static final Integer MIN_RECHARGE_AMOUNT_DTH_SUN_TV  = 5;
	public static final Integer MAX_RECHARGE_AMOUNT_DTH_SUN_TV  = 5000;
	
	public static final Integer MIN_RECHARGE_AMOUNT_DTH_AIRTEL_TV  = 100;
	public static final Integer MAX_RECHARGE_AMOUNT_DTH_AIRTEL_TV  = 5000;
	
	public static final Integer MIN_RECHARGE_AMOUNT_DTH_TATA_SKY  = 100;
	public static final Integer MAX_RECHARGE_AMOUNT_DTH_TATA_SKY  = 5000;
	
	public static final Integer MIN_RECHARGE_AMOUNT_DTH_VIDEOCON_D2H  = 10;
	public static final Integer MAX_RECHARGE_AMOUNT_DTH_VIDEOCON_D2H  = 5000;
	
	//OperatorMaster related
	public static List<OperatorMaster> operatorMasterList;
	public static List<OperatorMaster> mobileOperatorMasterList;
	//for DTH operator master list
	public static List<OperatorMaster> dthOperatorMasterList;
	//for Datacard operator master list
	public static List<OperatorMaster> datacardOperatorMasterList;
	
	public static List<OperatorMaster> postPaidDatacardOperatorMasterList;

	// postpaid bill payment operator master list
	public static List<OperatorMaster> postpaidMobileOperatorMasterList;
	// bill payment operator master list

	public static Map<Integer,OperatorMaster> operatorMasterMap = new HashMap<Integer, OperatorMaster>();
	public static Map<Integer, StateMaster> stateMasterMap = new HashMap<Integer, StateMaster>();
	
	public static final String RECHARGE_SUCCESS_FROM_EMAILID = "recharge.success.from.emailid";
	public static final String RECHARGE_SUCCESS_REPLYTO = "recharge.success.replyto";
	public static final String RECHARGE_SUCCESS_SUBJECT = "recharge.success.subject";
	public static final String RECHARGE_COUPON_SUBJECT = "recharge.coupon.subject";
	public static final String RECHARGE_FAILURE_FROM_EMAILID = "recharge.failure.from.emailid";
	public static final String RECHARGE_FAILURE_REPLYTO = "recharge.failure.replyto";
	public static final String RECHARGE_FAILURE_SUBJECT = "recharge.failure.subject";
	public static final String TXN_RECIEPT_BCC = "txn.reciept.bcc";
	
	
	// Refund success mailer properties
	public static final String BANK_REFUND_SUCCESS_SUBJECT = "bank.refund.success.subject";
	public static final String REFUND_SUCCESS_SUBJECT = "refund.success.subject";
	public static final String REVOK_SUCCESS_SUBJECT = "revoke.success.subject";
	public static final String REFUND_SUCCESS_FROM = "refund.success.from.emailid";
	public static final String REFUND_SUCCESS_BCC = "refund.success.bcc";
	public static final String COUPON_SUCCESS_BCC = "coupon.success.bcc";
	public static final String REFUND_SUCCESS_REPLYTO = "refund.success.replyto";
    public static final String REFUND_SUCCESS_CC = "refund.upload.email.cc";
    
    //Appconfig email property
    public static final String APPCONFIG_PROPERTY_TOGGLED_FROM = "appconfig.property.toggled.from";
    public static final String APPCONFIG_PROPERTY_TOGGLED_TO = "appconfig.property.toggled.to";
    public static final String APCONFIG_PROPERTY_TOGGLED_SUBJECT = "appconfig.property.toggled.subject";
    
    
    //Operator Alert mailer properties
    public static final String OPERATOR_ALERT_CREATED_SUCCESS_SUBJECT = "operator.alert.create.subject";
    public static final String OPERATOR_ALERT_DELETED_SUCCESS_SUBJECT = "operator.alert.delete.subject";
    public static final String OPERATOR_ALERT_SUCCESS_FROM = "operator.alert.from.emailid";
    public static final String OPERATOR_ALERT_SUCCESS_REPLYTO = "operator.alert.to.emailid";
    
    
    //Aggregator Priority mailer properties
    public static final String AGGREGATOR_PRIORITY_CHANGED_SUBJECT = "aggregator.priority.changed.subject";
    public static final String AGGREGATOR_PRIORITY_FROM = "aggregator.priority.from.emailid";
    public static final String AGGREGATOR_PRIORITY_REPLYTO = "aggregator.priority.to.emailid";
    
    // recon mail properties
    public static final String RECON_SUCCESS_SUBJECT = "recon.success.subject";
    public static final String RECON_SUCCESS_FROM = "recon.success.from.emailid";
    public static final String RECON_SUCCESS_BCC = "recon.success.bcc";
    public static final String RECON_SUCCESS_CC = "recon.success.cc";
    public static final String RECON_SUCCESS_REPLYTO = "recon.success.replyto";

    //ARDeal mail
    public static final String ARDEAL_FROM = "ardeal.email.from";

    // coupon threshold mailer properties
    public static final String COUPON_THRESHOLD_BREACH_FROM = "coupon.order.threshold.from.mailid";
    public static final String COUPON_THRESHOLD_BREACH_TO = "coupon.order.threshold.to.mailid";
    public static final String COUPON_THRESHOLD_BREACH_REPLY_TO = "coupon.order.threshold.replyto.mailid";

	// Fraud detection related
	public static final String ANTIFRAUD_EMAIL_NOTIFICATION_FROM   = "antifraud.email.notification.from";
	public static final String ANTIFRAUD_EMAIL_NOTIFICATION_TO     = "antifraud.email.notification.to";
	
	// Recharge Plan related
    public static final String RECHARGE_PLAN_EMAIL_NOTIFICATION_FROM   = "recharge.plan.email.notification.from";
    public static final String RECHARGE_PLAN_EMAIL_NOTIFICATION_TO     = "recharge.plan.email.notification.to";
	
	//Revenue leak
	public static final String REVENUELEAK_EMAIL_NOTIFICATION_FROM   = "revenueleak.email.notification.from";
	public static final String REVENUELEAK_EMAIL_NOTIFICATION_TO     = "revenueleak.email.notification.to";
	

    // Fraud rule application maximum benchmark days
    public static final String ANTI_FRAUD_APPLY_BENCHMARK_DAYS = "antifraud.max.benchmark.days";

	// FraudMaxamount
	public static final String ANTIFRAUD_AMOUNT_MOBILE             = "antifraud.amount.mobile";
	public static final String ANTIFRAUD_AMOUNT_DTH                = "antifraud.amount.dth";
	public static final String ANTIFRAUD_AMOUNT_DATACARD           = "antifraud.amount.datacard";

	// FraudFresh
	public static final String ANTIFRAUD_FRAUDFRESH_ENABLED        = "antifraud.fraudfresh.enabled";
	public static final String ANTIFRAUD_FRAUDFRESH_BENCHMARK_DAYS = "antifraud.fraudfresh.benchmark.days";
	public static final String ANTIFRAUD_FRAUDFRESH_PERIOD_HOURS   = "antifraud.fraudfresh.period.hours";
	public static final String ANTIFRAUD_FRAUDFRESH_MOBILE_MIN_AMT = "antifraud.fraudfresh.mobile.min.amt";
	public static final String ANTIFRAUD_FRAUDFRESH_DTH_MIN_AMT    = "antifraud.fraudfresh.dth.min.amt";
	public static final String ANTIFRAUD_FRAUDFRESH_RECHARGE_COUNT = "antifraud.fraudfresh.recharge.count";
	public static final String ANTIFRAUD_FRAUDFRESH_POSTPAID_MIN_AMT = "antifraud.fraudfresh.postpaid.min.amt";
	public static final String ANTIFRAUD_FRAUDFRESH_ADDCASH_MIN_AMT = "antifraud.fraudfresh.addcash.min.amt";
	// Fraud3000
	public static final String ANTIFRAUD_FRAUD3000_ENABLED        = "antifraud.fraud3000.enabled";
	public static final String ANTIFRAUD_FRAUD3000_BENCHMARK_DAYS = "antifraud.fraud3000.benchmark.days";
	public static final String ANTIFRAUD_FRAUD3000_PERIOD_HOURS   = "antifraud.fraud3000.period.hours";
	public static final String ANTIFRAUD_FRAUD3000_MOBILE_MIN_AMT = "antifraud.fraud3000.mobile.min.amt";
	public static final String ANTIFRAUD_FRAUD3000_DTH_MIN_AMT    = "antifraud.fraud3000.dth.min.amt";
	public static final String ANTIFRAUD_FRAUD3000_RECHARGE_COUNT = "antifraud.fraud3000.recharge.count";
	public static final String ANTIFRAUD_FRAUD3000_POSTPAID_MIN_AMT = "antifraud.fraud3000.postpaid.min.amt";
	public static final String ANTIFRAUD_FRAUD3000_ADDCASH_MIN_AMT = "antifraud.fraud3000.addcash.min.amt";
	// Fraud5000
	public static final String ANTIFRAUD_FRAUD5000_ENABLED        = "antifraud.fraud5000.enabled";
	public static final String ANTIFRAUD_FRAUD5000_BENCHMARK_DAYS = "antifraud.fraud5000.benchmark.days";
	public static final String ANTIFRAUD_FRAUD5000_PERIOD_HOURS   = "antifraud.fraud5000.period.hours";
	public static final String ANTIFRAUD_FRAUD5000_MOBILE_MIN_AMT = "antifraud.fraud5000.mobile.min.amt";
	public static final String ANTIFRAUD_FRAUD5000_DTH_MIN_AMT    = "antifraud.fraud5000.dth.min.amt";
	public static final String ANTIFRAUD_FRAUD5000_RECHARGE_COUNT = "antifraud.fraud5000.recharge.count";
	public static final String ANTIFRAUD_FRAUD5000_POSTPAID_MIN_AMT = "antifraud.fraud5000.postpaid.min.amt";
    // Fraud Add Cash
    public static final String ANTIFRAUD_CREDITSPAY_ENABLED        = "antifraud.creditspay.enabled";
    public static final String ANTIFRAUD_CREDITSPAY_BENCHMARK_DAYS = "antifraud.creditspay.benchmark.days";
    public static final String ANTIFRAUD_CREDITSPAY_PERIOD_HOURS   = "antifraud.creditspay.period.hours";
    public static final String ANTIFRAUD_CREDITSPAY_MAX_COUNT       = "antifraud.creditspay.max.count";
    public static final String ANTIFRAUD_CREDITSPAY_WALLET_LIMIT       = "antifraud.creditspay.walletlimit.max.amount";
    // Fraud Add Cash
    public static final String ANTIFRAUD_ADDCASH_VELOCITY_ENABLED        = "antifraud.addcashvelocity.enabled";
    public static final String ANTIFRAUD_ADDCASH_VELOCITY_BENCHMARK_DAYS = "antifraud.addcashvelocity.benchmark.days";
    public static final String ANTIFRAUD_ADDCASH_VELOCITY_PERIOD_HOURS   = "antifraud.addcashvelocity.period.hours";
    public static final String ANTIFRAUD_ADDCASH_VELOCITY_COUNT   = "antifraud.addcashvelocity.count";
    public static final String ANTIFRAUD_ADDCASH_AMOUNT_MAX_AMT = "antifraud.addcashamount.max.amt";
    public static final String ANTIFRAUD_ADDCASH_VELOCITY_MAX_AMT   = "antifraud.addcashvelocity.amount";
    
    public static final String ANTIFRAUD_FRAUD_REGISTERED_TO_TXN_ENABLED        = "antifraud.fraudregistertotxn.enabled";
    public static final String ANTIFRAUD_FRAUD_REGISTERED_TO_TXN_BENCHMARK_PERCENT = "antifraud.fraudregistertotxn.benchmark.percentage";
    public static final String ANTIFRAUD_FRAUD_REGISTERED_TO_TXN_PERIOD_HOURS   = "antifraud.fraudregistertotxn.period.hours";
    public static final String ANTIFRAUD_FRAUD_REGISTERED_TO_TXN_EMAIL_SUB    = "antifraud.fraudregistertotxn.email.sub";
    
    public static final String ANTIFRAUD_FRAUD_TICKET_SIZE_TXN_ENABLED        = "antifraud.fraudticketsize.enabled";
    public static final String ANTIFRAUD_FRAUD_TICKET_SIZE_TXN_BENCHMARK_AVERAGE = "antifraud.fraudticketsize.benchmark";
    public static final String ANTIFRAUD_FRAUD_TICKET_SIZE_TXN_PERIOD_MINUTES   = "antifraud.fraudticketsize.period.minutes";
    public static final String ANTIFRAUD_FRAUD_TICKET_SIZE_TXN_EMAIL_SUB    = "antifraud.fraudticketsize.email.sub";
    
    // Fraud Addcash  Device Fingerprint
    public static final String ANTIFRAUD_ADDCASH_DEVICE_FINGERPRINT_ENABLED = "antifraud.devicefingerprint.addcash.enabled";
    public static final String ANTIFRAUD_ADDCASH_DEVICE_FINGERPRINT_MAX_USERS = "antifraud.devicefingerprint.addcash.max.users";
    public static final String ANTIFRAUD_ADDCASH_IMEI_ENABLED = "antifraud.imei.addcash.enabled";
    public static final String ANTIFRAUD_ADDCASH_IMEI_MAX_USERS = "antifraud.imei.addcash.max.users";
    
    public static final String ANTIFRAUD_REFUND_TO_PG_ENABLED        = "antifraud.refundtopg.enabled";
	
	public static final String CROSS_SELL_TYPE_CROSS_SELL = "CROSS_SELL";
	public static final String CROSS_SELL_TYPE_LEAD_GEN = "LEAD_GEN";
	public static final String FULFILLMENT_COUPON_TYPE = "COUPON";
	public static final String FULFILLMENT_CROSS_SELL_TYPE = "CROSS_SELL";

	// Payment Gateway New Window related properties
	public static final String PG_OPEN_NEW_WINDOW = "pg.open.new.window";
	public static final String MODEL_PG_OPEN_NEW_WINDOW = "pgOpenNewWindow";

	public static final String COUPON_TYPE_C = "C";
	public static final String COUPON_TYPE_P = "P";
	public static final String COUPON_TYPE_E = "E";
	
	public static final String COUPON_NATURE_MISSION = "Mission";
	
	public static final String COUPON_INV_EXHAUST = "Coupon Inventory Exhausted";
	public static final String COUPON_EXPIRE = "Coupon Expired";


	public static final int SUCCESSFULL_TRANSACTION_STATUS = 1;
    public static final int FAILED_TRANSACTION_STATUS = 0;
	
    public static final String MOCK_PG = "mock.pg";
    public static final String MOCK_AG = "mock.ag";
    public static final String MOCK_SMS = "mock.sms";
    public static final String MOCK_BILL_PAYMENT = "mock.bill.payment";
    

	// FC Balance properties
	public static final String WALLET_CREDIT_BALANCE_LIMIT = "wallet.credit.balance.limit";

    public static final String OLD_DB_SUCCESSFUL_TRANSACTIONS = "oldDb.transactions.successful";
    public static final String NEW_DB_SUCCESSFUL_RECHARGES_RESP_CODE_0 = "newDb.recharge.successful.aggrRespCode.0";

    public static final String WALLET_DISPLAY_NAME = "Balance";

    //todo - Move to properties file
    public static final int REFUND_RESPONSE_TIME_HOURS = 6;
    public static final int RECHARGE_PROCESS_TIME = 2;

    /**
     * Property name to be read from properites file for configuring batch mode.
     */
    public static final String BATCH_MODE_PROPERTY = "batch.mode";
    public static final String WAIT_FOR_FULFILLMENT = "fulfillment.wait";
    
    /**
     * SMS templates
     */
    public static final String SMS_TEMPLATE_RECHARGE_UNDER_PROCESS = "templates/sms/rechargeunderprocess.vm";
    public static final String SMS_TEMPLATE_RECHARGE_SUCCESSFUL = "templates/sms/rechargesuccesssms.vm";
    public static final String SMS_TEMPLATE_RECHARGE_FAILED = "templates/sms/rechargefailsms.vm";
    public static final String SMS_TEMPLATE_RECHARGE_REFUND = "templates/sms/refund.vm";
    public static final String SMS_TEMPLATE_M_COUPONS = "templates/sms/mcoupon.vm";
    public static final String SMS_TEMPLATE_E_COUPONS = "templates/sms/ecouponsms.vm";
    public static final String SMS_TEMPLATE_EMULTI_COUPONS = "templates/sms/multi_merchant_ecoupon_sms.vm";
    public static final String SMS_TEMPLATE_BANK_REFUND_SUCCESS = "templates/sms/refundtobanksms.vm";
    public static final String SMS_TEMPLATE_POSTPAID_SUCCESSFUL = "templates/sms/postpaidsuccesssms.vm";
    public static final String SMS_TEMPLATE_POSTPAID_FAILED = "templates/sms/postpaidfailsms.vm";
    public static final String SMS_TEMPLATE_SPECIAL_OFFER_CASHBACK_SUCCESS = "templates/sms/specialOfferCashbackSms.vm";
    public static final String SMS_TEMPLATE_DIWALI_OFFER_COMPETITION_ENTERED_NOTIFICATION = "templates/sms/diwaliOfferCompetitionEntered.vm";
    public static final String SMS_TEMPLATE_REISSUED_PROMOCODE_HANDLE_ON_RECHARGE_FAILURE = "templates/sms/ReIssuedPromocodes.vm";
    public static final String SMS_TEMPLATE_GOSF = "templates/sms/gosfCoupon.vm";
    public static final String SMS_TEMPLATE_OLA_COUPON = "templates/sms/olaCoupon.vm";
    
    public static final String SMS_RECHARGE_REMINDER = "templates/sms/rechargeReminder.vm";

    /**
     * SMS enable property names
     */
    public static final String PROPERTY_SMS_RECHARGE_UNDER_PROCESS = "sms.recharge.under.process.enable";
    public static final String PROPERTY_SMS_RECHARGE_SUCCESSFUL = "sms.recharge.successful.enable";
    public static final String PROPERTY_SMS_RECHARGE_FAILED = "sms.recharge.failed.enable";
    public static final String PROPERTY_SMS_RECHARGE_REFUND = "sms.recharge.refund.enable";
    public static final String PROPERTY_SMS_MCOUPON = "sms.mcoupon.enable";
    public static final String PROPERTY_SMS_ECOUPON = "sms.ecoupon.enable";
    public static final String PROPERTY_SMS_BANK_REFUND_SUCCESS = "sms.bank.refund.enable";    
    public static final String PROPERTY_SMS_POSTPAID_SUCCESSFUL = "sms.postpaid.successful.enable";
    public static final String PROPERTY_SMS_POSTPAID_FAILED = "sms.postpaid.failed.enable";
    public static final String PROPERTY_SMS_SPECIAL_OFFER_SUCCESS = "sms.special.offer.cashback.success.enable";
    public static final String PROPERTY_SMS_DIWALI_OFFER_COMPETETION_ENTERED = "sms.diwali.offer.competetion.entered.success.enable";
    public static final String PROPERTY_SMS_DIWALI_OFFER_INVITATION = "sms.diwali.offer.invite.success.enable";
    public static final String PROPERTY_REISSUED_PROMOCODE_ON_RECHARGE_FAILURE = "sms.reissued.promocode.on.recharge.failure.enable";

    public static final String PREPERTY_SMS_TEMPLATE_GOSF = "sms.gosf.template.enabled";
    public static final String PROPERTY_SMS_TEMPLATE_OLA = "sms.ola.template.enabled";
    
    public static final String PROPERTY_SMS_RECHARGE_REMINDER = "sms.reminder.template.enabled";

    public static final String CARD_STORAGE_ENABLED = "cardstorage.enabled";

    public static final String JUSPAY_CARDSTORAGE_ID  = "juspay.cardstorage.id";
    public static final String JUSPAY_CARDSTORAGE_KEY = "juspay.cardstorage.key";
    public static final String JUSPAY_CARDSTORAGE_CALL_TIMEOUT = "juspay.cardstorage.ws.call.timeout";
    
    public static final String ONE_CHECK_JUSPAY_CARDSTORAGE_ID = "onecheck.juspay.cardstorage.id";


    public static final String ONE_CHECK_JUSPAY_CARDSTORAGE_KEY = "onecheck.juspay.cardstorage.key";
    
    public static final String PG = "pg";
    public static final String PRODUCT_MASTER_MOBILE = "Mobile";
    
    public static final String RECAPTCHA_ENABLED = "recaptcha_enabled";
    public static final String RECAPTCHA_PUBLIC_KEY = "recaptcha_public_key";
    public static final String RECAPTCHA_PRIVATE_KEY = "recaptcha_private_key";
    public static final String RECAPTCHA_LOGIN_THRESHOLD_MINS = "recaptcha_login_threshold_mins";
    public static final String CAPTCHA_FRAUD_CHECK_COUNT = "captcha.fraud.check.count";
	public static final String CAMPAIGN_CAPTCHA_FRAUD_CHECK_COUNT = "campaign.captcha.fraud.check.count";

    public static final String RECHARGE_SCHEDULER_TIME_DIFFERENCE = "recharge.scheduler.time.diff";
    public static final String TIMED_OUT_STATUS_CHECK_DELAY = "recharge.scheduler.timeout.delay";
    public static final String UNDER_PROCESS_STATUS_CHECK_DELAY = "recharge.scheduler.underprocess.delay";
    public static final String RECHARGE_TRANSACTION_PENDING_STATUS_CODE = "recharge.scheduler.pending.status.code";

    public static final String ACTIVITY_TYPE = "activity_type";
    //App Config Attributes

    public static final String ADMIN_CUR_USER_EMAIL = "curAdminUserEmail";
	public static final String ARDEAL_BCC = "ardeal.email.bcc";
	
	public static final String REDEEM_FREEFUND_TO_FC_BALANCE = "redeem-to-fcbalance";

    public static final String FRAUD_CHECK_METRIC_SERVICE = "FraudService";
    
    //Used for Voucher Category- Others
    public static final  List<String> VoucherCategoryList = new ArrayList<String>(Arrays.asList("ALL", "Food", "Store", "Entertainment", "Travel", "Care"));

    public static final String FDN_LOG_PREFIX = "fdn";
    
    public static final int APP_VERSION = 1;

    public static final String CUSTOM_LOGIN_PROPS = "custom.login.props";

    public static enum LoginSource {
        FACEBOOK, GOOGLE, EMAIL
    }
    public static final String          PREFIX                       = "REF";
    public static final String          NUMBER_OF_CODES              = "1";
    public static final String          LENGTH_OF_CODES              = "10";
    
    /*Admin action mailer properties*/
    public static final String ADMIN_ACTION_ALERT_SUBJECT = "admin.action.processed.subject";
    public static final String ADMIN_ACTION_ALERT_FROM = "admin.action.from.emailid";
    public static final String ADMIN_ACTION_ALERT_TO = "admin.action.to.emailid";

    //Freefund Class Constants
    public static final String          PREFIX_PARAM_NAME                     = "prefix";
    public static final String          NUMBER_OF_CODES_PARAM_NAME            = "codes";
    public static final String          LENGTH_PARAM_NAME                     = "length";
    public static final String          REDEEM_CONDITION_ID                   = "redeem_condition_id";
    public static final String          SUCCESS_MESSAGE_PARAM_NAME            = "successMessage";
    public static final String          RECHARGE_SUCCESS_MESSAGE_PARAM_NAME   = "rechargeSuccessMessage"; 
    public static final String          REWARD_ID                             = "rewardId";
    public static final String          IVR_ID                                = "ivrId";

    // AR Reward Properties
    public static final String          AR_REWARD_ID                          = "arRewardId";

    public static final String PROMOCODE_REWARD_SMS_SENDER = "smsgupshup";

    // Promocode reward notification attributes
    public static final String REWARD_MAIL_ENABLED = "reward.mail.enabled";
    public static final String REWARD_SMS_ENABLED = "reward.sms.enabled";
    public static final String REWARD_NOTIFICATION_ENABLED = "reward.notification.enabled";
    public static final String PROMOCODE_VALIDITY_IN_DAYS = "promocode.validity.in.days";
    public static final String PROMOCODE_VALIDITY_DATE = "promocode.validity.date";

    public static final String ADMIN_TOOL_ACTION_ALERT = "admin.tool.action.alertemail";
    public static final String REACHED_NINETY_PERCENT_LIMIT_ALERT_SUBJECT = "reached.ninety.percent.limit.subject";

    // Coupon Reward properties
    public static final String COUPON_IDS = "coupon.ids";
    public static final String SMS_TEXT = "sms.text";

    // Cumulative Cashback Reward properties
    public static final String CASHBACK_TYPE = "cashback.type";
    public static final String CASHBACK_VALUE = "cashback.value";
    public static final String PERCENT_CASHBACK = "percent";
    public static final String FLAT_CASHBACK = "flat";
    public static final String MAX_CASHBACK_PER_NUMBER = "max.cashback.per.number";
    public static final String MAX_CASHBACK_PER_USER = "max.cashback.per.user";
    public static final String MAX_CASHBACK_PER_TRANSACTION = "max.cashback.per.transaction";

    //GrowthEventRepository properties
    public static final String GROWTH_EVENT_PAY_LOAD_ID = "payLoadId";
    public static final String GROWTH_EVENT_TYPE = "eventType";
    public static final String GROWTH_EVENT_IDS = "growthEventIds";
    public static final String GROWTH_EVENT_ID = "eventId";
    public static final String GROWTH_EVENT_USER_EMAIL = "email";
    public static final String GROWTH_EVENT_USER_SESSION_ID = "sessionId";
    public static final String GROWTH_EVENT_USER_LOOKUP_ID = "lookupId";
    public static final String GROWTH_EVENT_USER_GENDER = "gender";
    public static final String GROWTH_EVENT_CREATED_ON = "created_on";
    public static final String IMEI_NUMBER = "imeiNumber";
    public static final String RECHARGE_AMOUNT = "rechargeAmount";
    public static final String GROWTH_EVENT_FRAUD = "EventFraud";
    public static final String GROWTH_EVENT_ORDERID = "orderId";
    public static final String FREEFUND_FRAUD = "FreefundFraud";
    public static final String ADVERTISEMENT_ID = "advertisementId";
    
    public static final String DUMMY_MOBILE_NO = "0000000000";

    public static String DUMMY_VISIT_ID = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"; //This is used in coupons for tracking experiments

    public static final String SOCIAL_LOGIN_MOBILE_NUMBER = "0000000000";

    public static final String RECHARGE_PLAN_EVENT_NAME = "RechargePlan";

    public static final String PLACEHOLDER_COUPON_CODE = "__couponCode__";
    public static final String PLACEHOLDER_REWARD_VALUE = "__rewardValue__";
    public static final String PLACEHOLDER_AMOUNT = "__amount__";

    // mongo properties
    public static final int MONGO_ASC_INDEX = 1;
    public static final int MONGO_DESC_INDEX = -1;
    public static final String FREEFUND_PAYMENT_CONDITION_CLASS_INDENT = "freefundpaymenttypecondition";
    public static final String AND_OPERATOR_CONDITION_CLASS_INDENT = "andOperatorARCondition";
    public static final String IS_NODE = "isMWeb";

    //Merchant Id for aggregators from billdesk
    public static final String BILLDESK_MERCHANT_ID = "billdesk.merchant.id";
    
    // TODO: Make this configurable at runtime.
	public static Map<ErrorCode, String[]> errorCodeToMessageMap = new HashMap<ErrorCode, String[]>() {
		{
			put(ErrorCode.RECHARGE_REQUEST_NOT_PRESENT, new String[] {"Oops! Something went wrong. Please try again later"});
			put(ErrorCode.SERVICE_NUMBER_NOT_PRESENT, new String[] {"Oops! Something went wrong. Please try again later"});
			put(ErrorCode.INVALID_SERVICE_NUMBER_FORMAT, new String[] {"Please enter a valid %s number", "Please enter a valid number"});
			put(ErrorCode.OPERATOR_NOT_PRESENT, new String[] {"Oops! Something went wrong. Please try again later"});
			put(ErrorCode.INVALID_OPERATOR, new String[] {"Oops! Something went wrong. Please try again later"});
			put(ErrorCode.INVALID_CIRCLE, new String[] {"Oops! Something went wrong. Please try again later"});
			put(ErrorCode.INVALID_ACCOUNT_NUMBER, new String[] {"Please enter a valid account number", "Please enter a valid number"});
			put(ErrorCode.INVALID_MOBILE_NUMBER, new String[] {"Please enter a valid mobile number", "Please enter a valid mobile number"});
			put(ErrorCode.AMOUNT_NOT_PRESENT, new String[] {"Oops! Something went wrong. Please try again later"});
			put(ErrorCode.INVALID_AMOUNT_FORMAT, new String[] {"Please enter amount greater than %s", "Please enter a valid amount"});
			put(ErrorCode.INVALID_AMOUNT_FOR_OPERATOR, new String[] {"Please enter amount greater than %s", "Please enter a valid amount for %s"});
			put(ErrorCode.INVALID_SERVICE_NUMBER_LENGTH, new String[] {"Please enter a valid %s number of %s digits", "Please enter a valid %s number"});
			put(ErrorCode.INVALID_BILLING_CYCLE, new String[] {"Please enter a valid %s digit billing cycle."});
		}
	};


	public static final  String PREPAID_AMOUNT_THRESHOLD = "prepaid.amount.threshold";
	
	public static final String AWS_S3_BILLPAY_BUCKET = "fc.billpayment.billdesk";
	public static final Map<Integer, String> dthToPostpaidOperatorCodeMap = FCUtil.createMap(
            47,   "VODA",                   //Vodafone DataCard
            44, "AIRT",                      //Airtel DataCard
            49,"IDEA",                       //Idea DataCard
            46, "MTNL",                     //MTNL
            45, "BSNL",                     //BSNL
            50,"RELG",                      // Reliance GSM
            21, "INDI",                     // Tata Indicom DataCard
            51, "INDI",
            52, "DOCO",                     //Tata Docomo GSM DataCard
            23,"RELC"
	        );

    public static final String METRICS_CONTROLLER_USED = "controllerUsed";
	
	// Admin User Types
	public static final String ADMIN_USER_TYPE_SUPER_ADMIN="Super Admin";
	public static final int ADMIN_USER_TYPE_SUPER_ADMIN_PRECEDENCE=5;
	
	public static final String ADMIN_USER_TYPE_ADMIN="Admin";
	public static final int ADMIN_USER_TYPE_ADMIN_PRECEDENCE=3;

	public static final String ADMIN_USER_TYPE_REGULAR_WRITER="Regular Writer";
	public static final int ADMIN_USER_TYPE_REGULAR_WRITER_PRECEDENCE=2;
	
	public static final String ADMIN_USER_TYPE_REGULAR_READER="Regular Reader";
	public static final int ADMIN_USER_TYPE_REGULAR_READER_PRECEDENCE=1;

    public static final String  APPLICATION_COOKIE_DOMAIN_NAME = "app.cookie.domain";

    public static final String  UPGRADE_RECOMMENDED_STATUS_MSG = "Non Wallet User";
    public static final String  UPGRADE_RECOMMENDED_TOOLTIP_MSG ="Action: Go to FreeCharge website / latest app and verify mobile number to upgrade";
    		
    public static final String  UPGRADE_COMPLETED_STATUS_MSG = "Wallet User";
    public static final String  UPGRADE_COMPLETED_TOOLTIP_MSG = "Action: Use your FC account credentials and login. In case of an issue, click on forgot password";
    
    public static final String  NO_UPGRADE_REQRUIRED_STATUS_MSG = "Non Wallet User";
    public static final String  NO_UPGRADE_REQRUIRED_TOOLTIP_MSG = "Action: Go to website / app and login";
    
    public static final String  LINK_SD_ACCOUNT_STATUS_MSG = "Non Wallet User";
    public static final String  LINK_SD_ACCOUNT_TOOLTIP_MSG = "Action: Go to snapdeal website and verify the FC Wallet password";
    
    public static final String  LINK_FC_ACCOUNT_STATUS_MSG = "Non Wallet User";
    public static final String  LINK_FC_ACCOUNT_TOOLTIP_MSG = "Action: Go to freecharge website and verify the FC Wallet password";
    
    public static final String  FORCE_UPGRADE_STATUS_MSG =  " ";
    public static final String  FORCE_UPGRADE_TOOLTIP_MSG = "Not required to be handled";
    
    //For transaction status type
    public static final String CREDIT_DEFAULT_TOOLTIP = "PG refunds OR FC Credits -> FC wallet during account upgrade";
    public static final String CREDIT_LOAD_TOOLTIP = "Add cash to wallet via account.freecharge.in";
    public static final String CREDIT_CASHBACK_TOOLTIP = "Freefund codes or promotional money (either via SD or via FC)";
    public static final String CREDIT_REFUND_TOOLTIP = "Reversal of a transaction on FC or SD";
    public static final String DEBIT_WITHDRAW_TOOLTIP = "Debit money from wallet to bank";
    public static final String DEBIT_DEFAULT_TOOLTIP = "Wallet payment for purchase on FC or SD";
    public static final String DEBIT_EXPIRY_TOOLTIP = "Promotional Voucher Expired.";
    public static final String DEBIT_TRANSFER_TOOLTIP = "Peer to peer payments";
    public static final String PAY_FAILURE_TOOLTIP = "Payment to PG fails";
    public static final String PAY_PENDING_TOOLTIP = "Payment Pending";
    public static final String PAY_SUCCESS_TOOLTIP = "Payment Success";

    
	// One Check Wallet Exceptions
	public static final String OCW_INTERNAL_SERVER_EXCEPTION = "InternalServerException";

    public static final String               MGAGE_URL                                  = "http://lookup.unicel.in/LOOKUP";
    public static final String               MGAGE_USERNAME                             = "LookTest";
    public static final String               MGAGE_PASSWORD                             = "d(4Id~2W";

    // New Campaign Framework Constants
    public static final Integer              FREECHARGE_CAMPAIGN_MERCHANT_ID            = 1;

    public static final String               GIFTVOUCHER                                = "GIFTVOUCHER";
    public static final String               CREDITED_AMOUNT                            = "creditedAmount";

    public static final String				 BUSINESS_ENTITY							= "BUSINESS_ENTITY";
    public static final String				 KPAY_BUSINESS_ENTITY						= "CLICKPAY";
    
    public static final Set<String>          SUCCESS_OR_PENDING_UTH_STATUS              = ImmutableSet.of("00", "0",
                                                                                                "08", "-1","09");
    public static final String               DEFAULT_PAYMENT_TYPE_ARG                   = "V_1";


	public static final String SMSGUPSHUPHLR_URL = "http://enterprise.smsgupshup.com/LookUp/rest";
	public static final String SMSGUPSHUPHLR_USERID = "2000154651";
	public static final String SMSGUPSHUPHLR_PWD = "BVjFhF";
	public static final String SMSGUPSHUPHLR_METHOD = "zonecheck";
	public static final String SMSGUPSHUPHLR_VERSION = "1.1";
	public static final String SMSGUPSHUPHLR_FORMAT = "JSON";

	public static final Double				 ZERO										= 0.0d;
	
	public static final String CCS_INPUT_VALIDATION_KEY = "inputValidation";
	public static final String CCS_ERROR_MSG_KEY = "errorMsg";
	
	public static final String OPERATIONS_ACCESS_KEY = "accessKey";
	public static final String OPERATIONS_SECRET_KEY = "secretKey";
	public static final String GIFT_CARDS = "GIFT_CARDS";
	public static final String INSURANCE = "INSURANCE";
	public static final String MUNICIPAL = "MUNICIPAL";
	public static final String GOOGLE_CREDITS_SUCCESS_TITLE = "Payment Successful";
	public static final String GOOGLE_CREDITS_FAILURE_TITLE = "Payment Failed";
	public static final String GOOGLE_CREDITS_SUCCESS_REASON_MSG = "Congratulations! You have successfully purchased your Google Play Recharge Code";
	public static final String GOOGLE_CREDITS_FAILURE_REASON_MSG = "Sorry! Your purchase of Google Play Recharge Code failed";
	public static final String GOOGLE_CREDITS_SUCCESS_NEXT_MSG = "googleCreditsSuccessNextMsg";
	public static final String GOOGLE_CREDITS_FAILURE_NEXT_MSG = "Your money will be refunded shortly to your account";
	public static final String GOOGLE_CREDITS_UNDER_PROCESS_REASON_MSG = "We are waiting for confirmation from Google";
	public static final String GOOGLE_CREDITS_UNDER_PROCESS_NEXT_MSG = "You should shortly receive your Google Play Recharge Code on your registered email address and mobile no";
	
	public static final String GOOGLE_CREDITS_QA_STATUS_URL = "/eurovas_uat/content/statusmn.asp";
	
	public static final String ERROR_CODE           = "errorCode";
	public static final String ERROR_MSG            = "errorMessage";
	public static final String UNAUTHORIZED_CODE = "ER-2005";
   public static final String UNAUTHORIZED_MSG = "Invalid user session";
   public static final String CARD_TYPE_CREDIT_CARD = "CREDIT";
   public static final String CARD_TYPE_DEBIT_CARD = "DEBIT";
   
	
	public static final Map<String, String> txnTypeAccountMap = FCUtil.createMap(
			"CUSTOMER_PAYMENT", "fcptl",
			"MoneyOutSync", "fcptl",
			"TRANSLATOR_CASHBACK", "fcptl",
			"TRANSLATOR_DTH", "aspl",
			"TRANSLATOR_ELECTRICITY", "aspl",
			"TRANSLATOR_GAS", "aspl",
			"TRANSLATOR_LANDLINE", "aspl",
			"TRANSLATOR_METRO", "aspl",
			"TRANSLATOR_POSTPAID_DATACARD", "aspl",
			"TRANSLATOR_POSTPAID_MOBILE", "aspl",
			"TRANSLATOR_PREPAID_DATACARD", "aspl",
			"TRANSLATOR_REFUND", "aspl",
			"TRANSLATOR_PREPAID_MOBILE", "aspl",
			"TRANSLATOR_BROADBAND", "aspl",
			"TRANSLATOR_WATER", "aspl",
			"ORDER_FULFILLMENT","fcptl"
			);
	public static final Map<String,String> productNameToTxnTypeMap = FCUtil.createMap(
			"Mobile", "TRANSLATOR_PREPAID_MOBILE",
			"DTH", "TRANSLATOR_DTH",
			"DataCard", "TRANSLATOR_PREPAID_DATACARD",
			"PostPaidDataCard", "TRANSLATOR_POSTPAID_DATACARD",
			"MobilePostpaid", "TRANSLATOR_POSTPAID_MOBILE",
			"WalletCash", "TRANSLATOR_CASHBACK",
			"Bill", PRODUCT_TYPE_BILL,
			"Landline", "TRANSLATOR_LANDLINE",
            "Electricity", "TRANSLATOR_ELECTRICITY",
            "Gas", "TRANSLATOR_GAS",
            "Metro", "TRANSLATOR_METRO",
			"Broadband", "TRANSLATOR_BROADBAND",
			"Water", "TRANSLATOR_WATER",
			"Coupons","ORDER_FULFILLMENT");
	
	public static final Map<String,ProductMaster.ProductName> TxnTypeToProductNameMap = FCUtil.createMap(
			"TRANSLATOR_PREPAID_MOBILE",ProductMaster.ProductName.Mobile, 
			"TRANSLATOR_DTH",ProductMaster.ProductName.DTH, 
			"TRANSLATOR_PREPAID_DATACARD",ProductMaster.ProductName.DataCard, 
			"TRANSLATOR_POSTPAID_DATACARD",ProductMaster.ProductName.PostPaidDataCard, 
			"TRANSLATOR_POSTPAID_MOBILE",ProductMaster.ProductName.MobilePostpaid, 
			"TRANSLATOR_CASHBACK",ProductMaster.ProductName.WalletCash, 
			PRODUCT_TYPE_BILL,ProductMaster.ProductName.Bill, 
			"TRANSLATOR_LANDLINE",ProductMaster.ProductName.Landline, 
			"TRANSLATOR_ELECTRICITY",ProductMaster.ProductName.Electricity, 
			"TRANSLATOR_GAS",ProductMaster.ProductName.Gas, 
			"TRANSLATOR_METRO",ProductMaster.ProductName.Metro, 
			"TRANSLATOR_BROADBAND",ProductMaster.ProductName.Broadband, 
			"TRANSLATOR_WATER",ProductMaster.ProductName.Water, 
			"ORDER_FULFILLMENT",ProductMaster.ProductName.Coupons);


	public static final String TICKET = "TICKET";
	public static final String COUPON = "COUPON";

	public static final String INSURANCE_BILLER_NAME = "billerName";
	public static final String INSURANCE_POLICY_NUMBER = "policyNumber";

	public static final String MUNICIPALITY_NAME = "municipalityName";
	public static final String CONSUMER_ACCOUNT_NUMBER = "consumerAccountNumber";
	public static final String SERVICE_TYPE = "serviceType";
	public static final String RECEIPT_NUMBER = "receiptNumber";
	public static final String ONDECK_ORDER_ID_QA_SUFFIX = "Q";

}
