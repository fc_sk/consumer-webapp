package com.freecharge.common.util;

import java.math.BigDecimal;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.mobile.service.util.CommonUtil;
import com.freecharge.sns.bean.GSTFee;
import com.freecharge.sns.bean.PaymentAlertBean;

@Service
public class GSTService {

    @Autowired
    private AppConfigService appConfigService;
    
    private static final String GST_PRODUCT_NAME_LIST = "PRODUCT_NAME_LIST";
    private static final String P2A_OPERATOR_NAME_LIST = "P2A_OPERATOR_NAME_LIST";
    private static final String NAME_DELIMITER = ",";
    
    public void populateGST(PaymentAlertBean paymentAlertBean, ProductName primaryProduct,String operator) {
	boolean isGSTEnabled = isGSTEnabled(primaryProduct, paymentAlertBean,operator);
	if (isGSTEnabled) {
	    GSTFee gstValue = null;
	    if (paymentAlertBean.getFeeCharge() != null && paymentAlertBean.getFeeCharge().getFeeAmount() != null
		    && paymentAlertBean.getFeeCharge().getFeeAmount().compareTo(BigDecimal.ZERO) != 0) {
		gstValue = CommonUtil.getGSTAmount(paymentAlertBean.getFeeCharge().getFeeAmount().doubleValue(),
			primaryProduct); // Find the GST Calulation values.
	    } else {
		gstValue = CommonUtil.getGSTAmount(paymentAlertBean.getAmount(), primaryProduct); // Find the GST
												  // Calulation values.
	    }
	    paymentAlertBean.setGstAmount(gstValue);
	    paymentAlertBean.setIsGSTEnabled(isGSTEnabled);
	}
    }

    private boolean isGSTEnabled(ProductName primaryProduct, PaymentAlertBean paymentAlertBean,String operator) {
	JSONObject object = appConfigService.getCustomProp(AppConfigService.GST_PRODUCT_ENABLED);
	if (object == null || object.isEmpty())
	    return false;
	if (object.containsKey(GST_PRODUCT_NAME_LIST)) {
	    String value = object.get(GST_PRODUCT_NAME_LIST).toString();
	    if (value != null && !value.isEmpty()) {
		if (paymentAlertBean.getFeeCharge() != null && paymentAlertBean.getFeeCharge().getFeeAmount() != null
			&& paymentAlertBean.getFeeCharge().getFeeAmount().compareTo(BigDecimal.ZERO) != 0) {
		    return true;
		}
	    }
	    
	    boolean isGSTEnable= true;
	    if (object.containsKey(P2A_OPERATOR_NAME_LIST)) {
		String operators= object.get(P2A_OPERATOR_NAME_LIST).toString();
		String[] operatorList=operators.split(NAME_DELIMITER);
		for(String operatorName:operatorList) {
		    if(operatorName.equalsIgnoreCase(operator)) {
			isGSTEnable=false;
			break;
		    }
		}
		
	    }
	    return (value != null && value.contains(primaryProduct.getProductType()) && isGSTEnable) ? true : false;

	}
	return false;
    }
}
