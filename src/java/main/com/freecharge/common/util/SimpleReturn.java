package com.freecharge.common.util;

import java.util.HashMap;
import java.util.Map;

import com.freecharge.api.error.ErrorCode;

public class SimpleReturn {
    public static final String MESSAGE_KEY = "message";
    private boolean valid;
    private boolean status;
    private ErrorCode errorCode = ErrorCode.DEFAULT_ERROR;
    
    private Map<String, Object> objMap = new HashMap<String, Object>();
    
    public Map<String, Object> getObjMap() {
        return objMap;
    }
    public void setObjMap(Map<String, Object> objMap) {
        this.objMap = objMap;
    }
    public boolean isValid() {
        return valid;
    }
    public void setValid(boolean valid) {
        this.valid = valid;
    }
    public boolean isStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public ErrorCode getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
