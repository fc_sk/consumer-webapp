package com.freecharge.common.util;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.github.cage.ACage;
import com.github.cage.Cage;

@Service
public class CaptchaService {
    private final Logger      logger                    = LoggingFactory.getLogger(getClass());
    private static final long serialVersionUID          = 1490947492185481844L;

    private static final Cage cage                      = new ACage();
    
    @Autowired
    private FCProperties fcProperties;
    
    /**
     * Generates a captcha token and stores it in the session.
     * 
     * @param fcSession
     *            where to store the captcha.
     */
    public void generateToken(FreechargeSession fcSession) {
        String token = cage.getTokenGenerator().next();

        fcSession.getSessionData().put("captchaToken", token);
        markTokenUsed(fcSession, false);
    }

    /**
     * Used to retrieve previously stored captcha token from session.
     * 
     * @param session
     *            where the token is possibly stored.
     * @return token or null if there was none
     */
    public String getToken(FreechargeSession fcSession) {
        Object val = fcSession.getSessionData().get("captchaToken");

        return val != null ? val.toString() : null;
    }

    /**
     * Marks token as used/unused for image generation.
     * 
     * @param fcSession
     *            where the token usage flag is possibly stored.
     * @param used
     *            false if the token is not yet used for image generation
     */
    public void markTokenUsed(FreechargeSession fcSession, boolean used) {
        fcSession.getSessionData().put("captchaTokenUsed", used);
    }

    /**
     * Checks if the token was used/unused for image generation.
     * 
     * @param session
     *            where the token usage flag is possibly stored.
     * @return true if the token was marked as unused in the session
     */
    public boolean isTokenUsed(FreechargeSession fcSession) {
        return !Boolean.FALSE.equals(fcSession.getSessionData().get("captchaTokenUsed"));
    }

    public void draw(String token, ServletOutputStream outputStream) throws IOException {
        cage.draw(token, outputStream);
    }

    /**
     * Helper method, disables HTTP caching.
     * 
     * @param resp
     *            response object to be modified
     */
    public void setResponseHeaders(HttpServletResponse resp) {
        resp.setContentType("image/" + cage.getFormat());
        resp.setHeader("Cache-Control", "no-cache, no-store");
        resp.setHeader("Pragma", "no-cache");
        long time = System.currentTimeMillis();
        resp.setDateHeader("Last-Modified", time);
        resp.setDateHeader("Date", time);
        resp.setDateHeader("Expires", time);
    }

    public SimpleReturn isCaptchValid(HttpServletRequest request) {
        SimpleReturn simpleReturn = new SimpleReturn();
        boolean isValid = false;

        FreechargeSession fcSession = FreechargeContextDirectory.get().getFreechargeSession();
        String sessionToken = getToken(fcSession);
        String requestToken = request.getParameter("captcha");
        isValid = sessionToken != null && sessionToken.equals(requestToken) && !isTokenUsed(fcSession);

        simpleReturn.getObjMap().put("sessionToken", sessionToken);
        simpleReturn.getObjMap().put("requestToken", requestToken);
        simpleReturn.getObjMap().put("isTokenUsed", isTokenUsed(fcSession));
        simpleReturn.setValid(isValid);

        return simpleReturn;
    }

    public SimpleReturn isCaptchValid(String requestToken) {
        SimpleReturn simpleReturn = new SimpleReturn();
        boolean isValid = false;

        FreechargeSession fcSession = FreechargeContextDirectory.get().getFreechargeSession();
        String sessionToken = getToken(fcSession);
        isValid = sessionToken != null && sessionToken.equals(requestToken) && !isTokenUsed(fcSession);

        simpleReturn.getObjMap().put("sessionToken", sessionToken);
        simpleReturn.getObjMap().put("requestToken", requestToken);
        simpleReturn.getObjMap().put("isTokenUsed", isTokenUsed(fcSession));
        simpleReturn.setValid(isValid);

        return simpleReturn;
    }

    public boolean shouldCheckCaptcha(FreechargeSession fcSession) {
        boolean shouldCheckCaptcha = false;
        int CAPTCHA_FRAUD_CHECK_COUNT = 1;
        try {
            CAPTCHA_FRAUD_CHECK_COUNT = Integer.parseInt(fcProperties.getCaptchaFraudCheckCount());
        } catch (Exception e) {
            logger.error("Error initializing CAPTCHA_FRAUD_CHECK_COUNT "+e, e);
        }
        try {
            int fraudCount = 0;
            Object fraudCountCaptcha = fcSession.getSessionData().get("fraudCountCaptcha");
            if (fraudCountCaptcha != null) {
                fraudCount = Integer.valueOf(fraudCountCaptcha.toString());
            }
            if (fraudCount >= CAPTCHA_FRAUD_CHECK_COUNT) {
                shouldCheckCaptcha = true;
            }
        } catch (Exception e) {
            logger.error("Exception in shouldCheckCaptcha " + e, e);
        }
        return shouldCheckCaptcha;
    }

    public boolean forceCaptcha(FreechargeSession fcSession, boolean reset) {
        boolean forceCaptcha = false;
        int CAPTCHA_FRAUD_CHECK_COUNT = 1;
        try {
            CAPTCHA_FRAUD_CHECK_COUNT = Integer.parseInt(fcProperties.getCaptchaFraudCheckCount());
        } catch (Exception e) {
            logger.error("Error initializing CAPTCHA_FRAUD_CHECK_COUNT "+e, e);
        }
        try {
            int fraudCount = fraudCount(fcSession, reset);
            if (fraudCount >= CAPTCHA_FRAUD_CHECK_COUNT) {
                forceCaptcha = true;
            }
        } catch (Exception e) {
            logger.error("Exception in shouldForceCaptcha " + e, e);
        }
        return forceCaptcha;
    }
    
    public boolean shouldCheckCaptchaForCampaign(FreechargeSession fcSession) {
		boolean shouldCheckCaptcha = false;
		int CAPTCHA_FRAUD_CHECK_COUNT = 1;
		try {
			CAPTCHA_FRAUD_CHECK_COUNT = Integer.parseInt(fcProperties.getCaptchaFraudCheckCountForCampaign());
		} catch (Exception e) {
			logger.error("Error initializing CAPTCHA_FRAUD_CHECK_COUNT " + e, e);
		}
		try {
			int fraudCount = 0;
			Object fraudCountCaptcha = fcSession.getSessionData().get("fraudCountCaptcha");
			if (fraudCountCaptcha != null) {
				fraudCount = Integer.valueOf(fraudCountCaptcha.toString());
			}
			if (fraudCount >= CAPTCHA_FRAUD_CHECK_COUNT) {
				shouldCheckCaptcha = true;
			}
		} catch (Exception e) {
			logger.error("Exception in shouldCheckCaptcha " + e, e);
		}
		return shouldCheckCaptcha;
	}

    public boolean forceCaptchaForCampaign(FreechargeSession fcSession, boolean reset) {
        boolean forceCaptcha = false;
        int CAPTCHA_FRAUD_CHECK_COUNT = 1;
        try {
            CAPTCHA_FRAUD_CHECK_COUNT = Integer.parseInt(fcProperties.getCaptchaFraudCheckCountForCampaign());
        } catch (Exception e) {
            logger.error("Error initializing CAPTCHA_FRAUD_CHECK_COUNT "+e, e);
        }
        try {
            int fraudCount = fraudCount(fcSession, reset);
            if (fraudCount >= CAPTCHA_FRAUD_CHECK_COUNT) {
                forceCaptcha = true;
            }
        } catch (Exception e) {
            logger.error("Exception in shouldForceCaptcha " + e, e);
        }
        return forceCaptcha;
    }
    
    private int fraudCount(FreechargeSession fcSession, boolean reset) {
        int fraudCount = 0;
        try {
            Object fraudCountCaptcha = fcSession.getSessionData().get("fraudCountCaptcha");
            if (reset || fraudCountCaptcha == null) {
                fraudCount = 0;
            } else {
                fraudCount = Integer.valueOf(fraudCountCaptcha.toString()) + 1;
            }
            fcSession.getSessionData().put("fraudCountCaptcha", fraudCount);
        } catch (Exception e) {
            logger.error("Exception in fraudCount " + e, e);
        }
        return fraudCount;
    }
}
