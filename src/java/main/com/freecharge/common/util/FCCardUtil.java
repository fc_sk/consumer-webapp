package com.freecharge.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.cardstorage.CardStorageKey;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.payment.services.CardDataHandler;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.rest.onecheck.model.CustomerCard;
import com.freecharge.rest.onecheck.model.FCWalletErrorCode;

@Component
public class FCCardUtil {
   
   @Autowired
   private AppConfigService appConfig;

   @Autowired
   @Qualifier("klickPayGatewayHandler")
   private CardDataHandler klickpayGatewayHandler;
   
   public void graylistCardsByPaymentPurpose(List<CustomerCard> customerCards, String paymentPurpose) {
      
      if(paymentPurpose==null){
         return;
      }
      
      JSONObject ppiSwitches = appConfig.getCustomProp(FCConstants.PPI_SWITCHES);
      Boolean isCreditCardsEnabledForGVLoad = Boolean.valueOf((String)ppiSwitches.get(FCConstants.PPI_GVLOAD_IS_CREDIT_CARD_ALLOWED));

      
      switch (paymentPurpose) {
         case PaymentConstants.PAYMENT_PAYMENT_PURPOSE_GIFT_VOUCHER_LOAD:
            if(!isCreditCardsEnabledForGVLoad) {    
                sortCardsBasedOnDebitCard(customerCards);
            }
            for(CustomerCard customerCard:customerCards){
               if(!isCreditCardsEnabledForGVLoad && FCConstants.CARD_TYPE_CREDIT_CARD.equals(customerCard.getCardType())){
                  customerCard.setIsAllowedForPurpose(false);
                  customerCard.setNotAllowedReason("Credit cards are not applicable on adding voucher.");
               }else{
                  customerCard.setIsAllowedForPurpose(true);
               }
            }
            break;

         default:
            break;
      }
   }
   
   private void sortCardsBasedOnDebitCard(List<CustomerCard> customerCards) {
       Collections.sort(customerCards, new Comparator<CustomerCard>() {
           @Override
           public int compare(CustomerCard c1, CustomerCard c2) {
               if(FCConstants.CARD_TYPE_DEBIT_CARD.equals(c1.getCardType())) return -1;
               else return 1;
           }
       });
   }

   public Map<String, String> checkForNonCreditCards(Map<String, String> cardDetails) {
      String cardType = cardDetails.get(CardStorageKey.CARD_TYPE);

      if(cardType==null || FCConstants.CARD_TYPE_CREDIT_CARD.equals(cardType)){
         Map<String, String> errorMap = FCWalletErrorCode.E34.toErrorMap();
         errorMap.put("errorMessage", "Credit cards are not applicable on adding voucher.");
         return errorMap;
      }

      return null;
   }

   public Map<String, String> checkForNonCreditCards(HashMap<String, Object> klickpayCardDetails) {
      Map<String, String> cardDetails = new HashMap<>();
      for(Entry<String, Object> cardField : klickpayCardDetails.entrySet()){
         if(cardField.getValue()!=null){
            cardDetails.put(cardField.getKey(), cardField.getValue().toString());            
         }
      }
      return checkForNonCreditCards(cardDetails);
   }

}
