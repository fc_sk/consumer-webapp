package com.freecharge.common.util;

import javax.servlet.ServletContext;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.freecharge.common.framework.context.FCContextLoaderListener;

public class SpringBeanLocator  {

    private static SpringBeanLocator _springBeanLocator = new SpringBeanLocator();
    private ServletContext servletContext = FCContextLoaderListener.servletContext;
    private ApplicationContext applicationContext;

    private SpringBeanLocator(){}

    public static SpringBeanLocator getInstance()
    {
      return _springBeanLocator;
    }

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public Object getSpringBean(String beanName) {
        ApplicationContext applicationContext = null;
        if (this.servletContext != null) {
            applicationContext = WebApplicationContextUtils.getWebApplicationContext(this.servletContext);
        } else {
            applicationContext = this.applicationContext;
        }

        return applicationContext.getBean(beanName);
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
