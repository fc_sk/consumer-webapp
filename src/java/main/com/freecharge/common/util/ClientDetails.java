package com.freecharge.common.util;



public class ClientDetails {

	private String clientKey = "testKey";
	private String url = "http://localhost:8080";
	private String clientName = "Snapdeal";
	private int connectionRequestTimeout = 5000;
	private int connectTimeout = 5000;
	private int socketTimeout = 7000;
	private Long keepAlive = -1L;
	private Boolean mutualAuthenticationRequired = false;
	private String keyStoreType = "jks";
	private String clientKeystoreFile = "/opt/configuration/clientkeystore.jks";
	private String clientKeystorePassword = "snapdeal";
	private Integer maxConnPerRoute = 50;
	private Integer maxConnTotal = 50;
	
	public String getClientKey() {
		return clientKey;
	}

	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getConnectionRequestTimeout() {
		return connectionRequestTimeout;
	}

	public void setConnectionRequestTimeout(int connectionRequestTimeout) {
		this.connectionRequestTimeout = connectionRequestTimeout;
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public Long getKeepAlive() {
		return keepAlive;
	}

	public void setKeepAlive(Long keepAlive) {
		this.keepAlive = keepAlive;
	}

	public Boolean getMutualAuthenticationRequired() {
		return mutualAuthenticationRequired;
	}

	public void setMutualAuthenticationRequired(Boolean mutualAuthenticationRequired) {
		this.mutualAuthenticationRequired = mutualAuthenticationRequired;
	}

	public String getKeyStoreType() {
		return keyStoreType;
	}

	public void setKeyStoreType(String keyStoreType) {
		this.keyStoreType = keyStoreType;
	}

	public String getClientKeystoreFile() {
		return clientKeystoreFile;
	}

	public void setClientKeystoreFile(String clientKeystoreFile) {
		this.clientKeystoreFile = clientKeystoreFile;
	}

	public String getClientKeystorePassword() {
		return clientKeystorePassword;
	}

	public void setClientKeystorePassword(String clientKeystorePassword) {
		this.clientKeystorePassword = clientKeystorePassword;
	}

	public Integer getMaxConnPerRoute() {
		return maxConnPerRoute;
	}

	public void setMaxConnPerRoute(Integer maxConnPerRoute) {
		this.maxConnPerRoute = maxConnPerRoute;
	}

	public Integer getMaxConnTotal() {
		return maxConnTotal;
	}

	public void setMaxConnTotal(Integer maxConnTotal) {
		this.maxConnTotal = maxConnTotal;
	}

	

	private void validate(String param) throws Exception {
		if (param == null)
			throw new Exception(
					"Client details are not initialized. Please initialize client before calling any API.");
	}

}
