package com.freecharge.common.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ProductMasterMapping {
	public static Integer getProductIdbyType(String type) {
		Integer proId = 0;
		if (type != null && type != "") {
			Map<Integer, String> productMasterMap = FCConstants.productMasterMap;
			Set set = productMasterMap.entrySet();
			Iterator ite = set.iterator();

			while (ite.hasNext()) {
				Map.Entry me = (Map.Entry) ite.next();
				if (me.getValue().equals(type)) {
					proId = (Integer) me.getKey();
				}
			}
		}
		return proId;
	}
}
