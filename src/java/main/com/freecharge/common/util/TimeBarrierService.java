package com.freecharge.common.util;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * Provide a simple Redis based utility to put up a barrier for threads
 * This is similar to locking - only it expires in a certain specified time
 *
 * For example, if we do not want 2 threads with the same value of "email" to proceed "simultaneously",
 * we can put up a timedBarrier("abc@gmail.com", 1000)
 *
 * Note that there is no "removeTimeBarrier() method. The expiry should be set appropriately.
 *
 * Based on idea from: http://redis.io/commands/setnx
 */
@Component
public class TimeBarrierService {
    private Logger logger = LoggingFactory.getLogger(getClass());

    @Autowired
    private TimeBarrierCacheManager timeBarrierCacheManager;

    public boolean putTimeBarrier(final String k, final long timeOutMillis) {
        long currMillis = System.currentTimeMillis();

        try {
            if (timeBarrierCacheManager.setIfAbsent(k, currMillis + timeOutMillis)) {
                timeBarrierCacheManager.expire(k, timeOutMillis);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            logger.error("Exception for key=" + k + ": ", e);
            // If we face some problem, we return true to avoid permanent blocks
            return true;
        }
    }
}
