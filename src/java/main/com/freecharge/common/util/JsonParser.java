package com.freecharge.common.util;

import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class JsonParser {

	private static final ObjectMapper mapper = new ObjectMapper();
	public static String objectToString(Object object) {
		String value = "";
		try {
			value = mapper.writeValueAsString(object);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return value;
	}
}
