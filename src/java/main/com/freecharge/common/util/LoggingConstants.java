package com.freecharge.common.util;

public class LoggingConstants {

	public static final String PLAN_RETRY_TABLE = "freecharge";
	public static final String PLAN_RETRY_DATABASE = "topupspecialretry";
	
	
	public static final String OPERATOR_ID = "operatorId";
	public static final String CIRCLE_ID = "circleId";
	public static final String SERVICE_NUMBER = "serviceNumber";
	public static final String AGGR_NAME = "aggrName";
	public static final String REQUEST_TIMESTAMP = "requestTimestamp";
	public static final String RAW_REQUEST = "rawRequest";
	public static final String RAW_RESPONSE = "rawResponse";
	public static final String RESPONSE_CODE = "responseCode";
	public static final String CALL_ID = "callId";
	public static final String RESPONSE_TIMESTAMP = "responseTimestamp";
	
	public static final String TPT_VALIDATION_DATABASE = "freecharge";
	public static final String TPT_VALIDATION_TABLE = "validation_log_table";
	public static final String ORDER_ID = "orderId";
	public static final String SCHEDULE_CREATION_TS = "scheduleCreationTs";
	public static final String RETRY_TS = "retryTs";
	public static final String AG_NAME = "agName";
	public static final String STATUS = "status";
	public static final String SCHEDULE_INFO_LOGGER_DATABASE = "freecharge";
	public static final String SCHEDULE_INFO_LOGGER_TABLE = "schedule_attempt_status";

	public static enum DBActionType {
		INSERT, UPDATE, DELETE, SELECT
	}

	public static enum ValidationCallType {

		IN_TRANSACTION_VALIDATION(1), POSTPAID_FETCH_BILL(2);

		private int callId;

		private ValidationCallType(int id) {
			this.callId = id;
		}

		public int getName() {
			return this.callId;
		}

		public static ValidationCallType fromId(int id) {

			switch (id) {
			case 1:
				return IN_TRANSACTION_VALIDATION;

			case 2:
				return POSTPAID_FETCH_BILL;

			default:
				throw new IllegalArgumentException("Unknown CallType ID : " + id);
			}
		}
	}

}
