package com.freecharge.common.util;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 30/9/12
 * Time: 7:12 PM
 * To change this template use File | Settings | File Templates.
 */
//Borrowed from here - http://stackoverflow.com/questions/1786206/is-there-a-java-equivalent-of-pythons-defaultdict
public class DefaultDict<K, V> extends HashMap<K, V> {

    Class<V> klass;
    public DefaultDict(Class klass) {
        this.klass = klass;
    }

    @Override
    public V get(Object key) {
        V returnValue = super.get(key);
        if (returnValue == null) {
            try {
                returnValue = klass.newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            this.put((K) key, returnValue);
        }
        return returnValue;
    }
}

