package com.freecharge.common.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.freecharge.recharge.util.RechargeConstants;


/**
 * Created by yashveer on 23/5/16.
 */
public enum RechargeProductType {
    MOBILE("recharge"),
    POSTPAID("postpaid"),
    DTH("dth"),
    DATACARD("data"),
    POSTPAID_DATACARD("postpaiddata"),
    ELECTRICITY("electricity"),
    GAS("gas"),
    LANDLINE("landline"),
    BROADBAND("broadband"),
    METRO("metro"),
    WATER("water"),
    GOOGLECREDITS("googlecredits");

    private String name;

    private static final Set<RechargeProductType> billProductTypeSet
            = new HashSet<>();

    private static final Map<RechargeProductType, String> productTypeCodeMap = new HashMap<>();

    static {
        billProductTypeSet.add(ELECTRICITY);
        billProductTypeSet.add(GAS);
        billProductTypeSet.add(LANDLINE);
        billProductTypeSet.add(BROADBAND);
        billProductTypeSet.add(METRO);
        billProductTypeSet.add(WATER);

        productTypeCodeMap.put(MOBILE, RechargeConstants.MOBILE_PRODUCT_TYPE);
        productTypeCodeMap.put(POSTPAID, RechargeConstants.POST_PAID_PRODUCT_TYPE);
        productTypeCodeMap.put(DTH, RechargeConstants.DTH_PRODUCT_TYPE);
        productTypeCodeMap.put(DATACARD, RechargeConstants.DATA_CARD_PRODUCT_TYPE);
        productTypeCodeMap.put(POSTPAID_DATACARD, RechargeConstants.POSTPAID_DATA_CARD_PRODUCT_TYPE);
        productTypeCodeMap.put(ELECTRICITY, RechargeConstants.ELECTRICITY_PRODUCT_TYPE);
        productTypeCodeMap.put(GAS, RechargeConstants.GAS_PRODUCT_TYPE);
        productTypeCodeMap.put(LANDLINE, RechargeConstants.LANDLINE_PRODUCT_TYPE);
        productTypeCodeMap.put(BROADBAND, RechargeConstants.BROADBAND_PRODUCT_TYPE);
        productTypeCodeMap.put(METRO, RechargeConstants.METRO_PRODUCT_TYPE);
        productTypeCodeMap.put(WATER, RechargeConstants.WATER_PRODUCT_TYPE);
        productTypeCodeMap.put(GOOGLECREDITS, RechargeConstants.GOOGLE_CREDITS_PRODUCT_TYPE);
    }

    private RechargeProductType(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public boolean isBillType(){
        return billProductTypeSet.contains(this);
    }

    public static String getProductTypeCode(RechargeProductType rechargeProductType){
        return productTypeCodeMap.get(rechargeProductType);
    }


}
