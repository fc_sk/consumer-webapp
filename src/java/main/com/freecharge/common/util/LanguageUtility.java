package com.freecharge.common.util;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.LocaleUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.session.service.CookieHandler;

@Service
public class LanguageUtility {

	@Autowired
	CookieHandler cookieHandler;

	@Autowired
	private ReloadableResourceBundleMessageSource messageSource;

	public String getLocaleMessage(HttpServletRequest request, String msg, Object[] params) {
		
		Locale localeLanguageParam = null;
		String message = null;
		String langaugeParam = cookieHandler.getLocaleParameterCookie(request);
		localeLanguageParam = LocaleUtils.toLocale(langaugeParam);
		message = messageSource.getMessage(msg, params, "#" + msg + "#", localeLanguageParam);
		return message;
	}
	
	public String getLocaleMessage(String msg, Object[] params) {
		String message = null;
		Locale locale = LocaleContextHolder.getLocale();
		message = messageSource.getMessage(msg, params, "#" + msg + "#", locale);
		return message;
	}	
}
