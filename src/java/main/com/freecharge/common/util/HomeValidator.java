package com.freecharge.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.web.webdo.HomeWebDo;

public class HomeValidator{

	private final Logger logger = LoggingFactory.getLogger(HomeValidator.class);
	public void validate(Object object, Errors error) {

		if(object instanceof HomeWebDo) {
			HomeWebDo homeWebDo = (HomeWebDo)object;
			if(homeWebDo.getType()==null) {
				logger.info("homeWebDo.getType() is null");
				logger.info("mobileNumber : " + homeWebDo.getMobileNumber() + " amount: " + homeWebDo.getAmount() + " operator: " + homeWebDo.getOperator() + " circle: " + homeWebDo.getCircle() + " type: " + homeWebDo.getType() + " couponWorthAmt: " + homeWebDo.getCouponWorthAmt());
				error.rejectValue("type", "Product Type cannot be null");
			} else if(homeWebDo.getType().equalsIgnoreCase("V")){
					
					//MObile Number validations
					ValidationUtils.rejectIfEmptyOrWhitespace(error,"mobileNumber","Mobile Number should not be blank and no white space allowed.");
					if(error.getFieldError("mobileNumber") == null){
						Pattern p = Pattern.compile("^\\d+$");
		                Matcher m = p.matcher(homeWebDo.getMobileNumber());
		                //check  whether any match is found
		                boolean matchFound = m.matches();
		                if (!matchFound)
		                {
		                   error.rejectValue("mobileNumber", "Mobile Number field should contain only numbers");
		                }
		                
						if(error.getFieldError("mobileNumber") == null && homeWebDo.getMobileNumber().length() != 10)
							error.rejectValue("mobileNumber", "Mobile Number field should contain Min and Max 10 numbers");
					}

					//operator validations
					ValidationUtils.rejectIfEmptyOrWhitespace(error,"operator","Operator should not blank");
					
					getAmountValidations(homeWebDo.getAmount(),"amount", error);
					if(! error.hasFieldErrors("amount") &&( Integer.parseInt(homeWebDo.getAmount())<FCConstants.MIN_RECHARGE_AMOUNT || Integer.parseInt(homeWebDo.getAmount())>FCConstants.MAX_RECHARGE_AMOUNT_MOBILE))
					error.rejectValue("amount", "Please ensure recharge amount is between Rs."+FCConstants.MIN_RECHARGE_AMOUNT+ " to Rs."+FCConstants.MAX_RECHARGE_AMOUNT_MOBILE);
				
			}else if(homeWebDo.getType().equalsIgnoreCase("D")){

				ValidationUtils.rejectIfEmptyOrWhitespace(error,"dthNumber","DTH Number should not be blank and no white space allowed.");
				
				if(error.getFieldError("dthNumber") == null){
					Pattern p = Pattern.compile("^\\d+$");
	                Matcher m = p.matcher(homeWebDo.getDthNumber());
	                //check  whether any match is found
	                boolean matchFound = m.matches();
	                if (!matchFound)
	                {
	                   error.rejectValue("dthNumber", "DTH Number field should contain only numbers");
	                }
				
				}
				
				//operator validations
				ValidationUtils.rejectIfEmptyOrWhitespace(error,"operator","operator should not be blank");
				
				getAmountValidations(homeWebDo.getDthAmount(),"dthAmount",error);
				
				if(error.getFieldError("dthAmount") == null && (Integer.parseInt(homeWebDo.getDthAmount()) < FCConstants.MIN_RECHARGE_AMOUNT || Integer.parseInt(homeWebDo.getDthAmount()) >FCConstants.MAX_RECHARGE_AMOUNT_DTH)){
					error.rejectValue("dthAmount", "Please ensure recharge amount is between Rs."+FCConstants.MIN_RECHARGE_AMOUNT+ " to Rs."+FCConstants.MAX_RECHARGE_AMOUNT_DTH);
			 	}
				
				if(error.getFieldError("operator") == null && error.getFieldError("dthNumber") == null) {
					//dth validations based on operator
					
					String operatorName = homeWebDo.getOperatorName();
					String serviceNo = homeWebDo.getDthNumber();
					
					if(operatorName.equalsIgnoreCase("Big Tv")){
				    	 if(serviceNo.length()!=12 || Integer.parseInt(serviceNo.substring(0,1))!=2)
				    		 error.rejectValue("dthNumber", "DTH Number starts with 2 and has maximum of 12 digits allowed for " +operatorName);
				    } else if(operatorName.equalsIgnoreCase("Dish Tv ")){
				    	 if(serviceNo.length()!=11 || Integer.parseInt(serviceNo.substring(0,1))!=0)
				    		 error.rejectValue("dthNumber","DTH Number starts with 0 and has maximum of 11 digits allowed for "+operatorName);
				    }else if(operatorName.equalsIgnoreCase("Sun Tv")){
	 			    	 if(serviceNo.length()!=11 || Integer.parseInt(serviceNo.substring(0,1))!=4)
				        	 error.rejectValue("dthNumber","DTH Number starts with 4 and has maximum of 11 digits allowed for "+operatorName);
				    }else if(operatorName.equalsIgnoreCase("Airtel Tv")){
			    		 if(serviceNo.length()!=10 || Integer.parseInt(serviceNo.substring(0,1))!=3)
				    		 error.rejectValue("dthNumber","DTH Number starts with 3 and has maximum of 10 digits allowed for "+operatorName);
				    }else if(operatorName.equalsIgnoreCase("Tata Sky")){
			    		 if(serviceNo.length()!=10 || Integer.parseInt(serviceNo.substring(0,1))!=1)
				    		 error.rejectValue("dthNumber","DTH Number starts with 1 and has maximum of 10 digits allowed for "+operatorName);
	         	    }/*else if(operatorName.equalsIgnoreCase("Videocon Tv")){
			    		 if(serviceNo.length()!=8 )
				    			 error.rejectValue("dthNumber","DTH Number allow Only  8 digit number for "+operatorName);
	 	    	    }*/
					
				}
				
			}else if(homeWebDo.getType().equalsIgnoreCase("C") || homeWebDo.getType().equalsIgnoreCase("F")){
				ValidationUtils.rejectIfEmptyOrWhitespace(error,"dataCardNumber","DataCard Number should not be blank and no white space allowed.");
				
				if(error.getFieldError("dataCardNumber") == null){
					Pattern p = Pattern.compile("^\\d+$");
	                Matcher m = p.matcher(homeWebDo.getDataCardNumber());
	                boolean matchFound = m.matches();
	                if (!matchFound)
	                {
	                   error.rejectValue("dataCardNumber", "DataCard Number field should contain only numbers");
	                }
	                
	                if(error.getFieldError("dataCardNumber") == null && homeWebDo.getDataCardNumber().length() != 10)
						error.rejectValue("mobileNumber", "DataCard Number field should contain Min and Max 10 numbers");
				
				}
				
				//operator validations
				ValidationUtils.rejectIfEmptyOrWhitespace(error,"operator","operator should not be blank");

				//amount
				getAmountValidations(homeWebDo.getDataCardAmount(),"dataCardAmount", error);
				if(! error.hasFieldErrors("dataCardAmount") &&( Integer.parseInt(homeWebDo.getDataCardAmount())<FCConstants.MIN_RECHARGE_AMOUNT || Integer.parseInt(homeWebDo.getDataCardAmount())>FCConstants.MAX_RECHARGE_AMOUNT_DATACARD))
					error.rejectValue("dataCardAmount", "Please ensure recharge amount is between Rs."+FCConstants.MIN_RECHARGE_AMOUNT+ " to Rs."+FCConstants.MAX_RECHARGE_AMOUNT_DATACARD);
			}
			
		}
	}
	
	public void getAmountValidations(String amount,String amountField,Errors error){
		Pattern p = Pattern.compile("^\\d+$");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(error,amountField,"Amount should not be blank and no white space allowed.");
		if(error.getFieldError(amountField) == null){
			Matcher m = p.matcher(amount);
            //check  whether any match is found
			boolean matchFound = m.matches();
	        if (!matchFound){
	               error.rejectValue(amountField, "Amount field should contain only numbers");
	         }
		}
	}

}
