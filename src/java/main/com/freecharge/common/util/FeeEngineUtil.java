package com.freecharge.common.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.ClientDetails;
import com.freecharge.common.util.AuthorizationParams;
import com.freecharge.common.util.SystemInformationFetcher;
import com.freecharge.web.webdo.FeeEngineException;
import com.freecharge.web.webdo.InternalClientException;
import com.freecharge.common.util.RequestParamValidator;
import com.freecharge.common.util.RequestHeaders;
import com.freecharge.common.util.ServiceResponse;


@Component
public class FeeEngineUtil {

	private RequestParamValidator requestParamValidator = new RequestParamValidator();
	private Logger log = LoggingFactory.getLogger(getClass());

	public static final String APPLICATION_JSON = "application/json";
	public static final int DEFAULT_ERROR_CODE = 500;

	private AuthorizationClient authClient = new AuthorizationClient();
	private static FeeEngineUtil instance = new FeeEngineUtil();
	
	public static FeeEngineUtil getInstance() {
		return instance;
	}

	public <T, R> R processHttpRequest(String completeUrl, TypeReference<ServiceResponse<R>> typeReference, T request,
			HttpClient httpClient, ClientDetails clientDetails) throws FeeEngineException {
		if (request != null)
			requestParamValidator.validate(request);
		final Map<String, String> parameters = getMap(request);
		R response = null;
		int statusCode = DEFAULT_ERROR_CODE;
		Map<String, String> header = new HashMap<String, String>();
		try {
			header = createHeader(request, clientDetails);
			log.info("For Client :" + clientDetails.getClientName() + " Request Json for Checksum generation on Client:"
					+ request + " [CheckSumGenerated]:" + header.get("CheckSum"));
			HttpResponse result = executeHttpPostMethod(completeUrl, parameters, header, httpClient);
			if (result != null && result.getStatusLine() != null) {
				statusCode = result.getStatusLine().getStatusCode();
				String json = EntityUtils.toString(result.getEntity());
				ObjectMapper mapper = new ObjectMapper();
				//				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				if (statusCode == ValidResponseEnum.OK.getValue()
						|| statusCode == ValidResponseEnum.CREATED.getValue()) {
					log.info("Response from Fee Engine: " + json);
					response = mapper.readValue(json, typeReference);
				} else {
					throw new Exception(
							"Status code: " + statusCode + " " + result.getStatusLine().getReasonPhrase());
				}

			}
		} catch (Exception e) {
			log.error("For Request ID:[" + header.get(RequestHeaders.APP_REQUEST_ID.getName()) + "] " + e.getMessage()
			+ ":",e);
			throw new InternalClientException(e.getMessage(), e.getCause());
		}
		log.info("For Request ID:[" + header.get(RequestHeaders.APP_REQUEST_ID.getName()) + "]" + response);
		return response;
	}
	

	@SuppressWarnings("unchecked")
	private Map getMap(Object request) {
		if (request == null)
			return null;
		Map<String, Object> map = new HashMap<String, Object>();
		ObjectMapper mapper = new ObjectMapper();
		map = mapper.convertValue(request, map.getClass());
		Map<String, Object> mapNew = new HashMap<String, Object>();
		for (String key : map.keySet()) {
			if (map.get(key) != null)
				mapNew.put(key, map.get(key));
		}
		return mapNew;
	}

	private Map<String, String> createHeader(Object request, ClientDetails clientDetails) throws Exception {
		Map<String, String> header = new HashMap<String, String>();
		header.put(RequestHeaders.CONTENT_TYPE.getName(), APPLICATION_JSON);
		header.put(RequestHeaders.ACCEPT.getName(), APPLICATION_JSON);
		SystemInformationFetcher.setApplicationInformationInHeader(header);
		AuthorizationParams authorizationParams = new AuthorizationParams(clientDetails.getClientName(),
				clientDetails.getClientKey());
		return authClient.UpdateRequest(header, request, authorizationParams);
	}

	public HttpResponse executeHttpPostMethod(String url, Map<String, String> params,
			Map<String, String> headers, HttpClient httpClient) throws Exception {

		HttpPost httpPost = new HttpPost(url);
		setHeaders(httpPost, headers);
		try {
			if (params != null) {
				httpPost.setEntity(createStringEntity(params));
			}
			HttpResponse response = httpClient.execute(httpPost);
			return response;
		} catch (Exception e) {
			log.error("Exception caught while executing httpPost", e);
			throw new Exception("Unable to execute http post", e);
		}
	}

	private HttpEntity createStringEntity(Map<String, String> params)   {

		JSONObject keyArg = new JSONObject();
		for (Map.Entry<String, String> pEntry : params.entrySet()) {
			keyArg.put(pEntry.getKey(), pEntry.getValue());
		}
		StringEntity input = null;
		try {
			input = new StringEntity(keyArg.toString());
		} catch (Exception e) {
			log.error("Exception caught");
		}
		return input;
	}
	
	private void setHeaders(HttpRequestBase httpObject, Map<String, String> headers) {
	      if (headers != null) {
	         for (Map.Entry<String, String> hEntry : headers.entrySet()) {
	            httpObject.addHeader(hEntry.getKey(), hEntry.getValue());
	         }
	      }
	   }

}
