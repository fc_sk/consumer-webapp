package com.freecharge.common.util;

import java.util.HashMap;

/**
 * A HashMap implementation that has case-insensitive string as the key. Beware
 * of the skew in keys - e.g. keySet()
 * @author shantanukumar
 *
 * @param <V>
 */
public class HashMapIgnoreCase<V> extends HashMap<String, V> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public boolean containsKey(Object key) {
		if (!(key instanceof String)) {
			throw new IllegalArgumentException(
					"Expected String, but found " + key.getClass().getName() +
					key.toString());
		}
		return super.containsKey(((String) key).toLowerCase());
	}

	@Override
	public V put(String key, V value) {
		return super.put(key.toLowerCase(), value);
	}

	@Override
	public V get(Object key) {
		if (!(key instanceof String)) {
			throw new IllegalArgumentException(
					"Expected String, but found " + key.getClass().getName() +
					key.toString());
		}
		return super.get(((String) key).toLowerCase());
	}

	@Override
	public V remove(Object key) {
		if (!(key instanceof String)) {
			throw new IllegalArgumentException(
					"Expected String, but found " + key.getClass().getName() +
					key.toString());
		}
		return super.remove(((String) key).toLowerCase());
	}

}
