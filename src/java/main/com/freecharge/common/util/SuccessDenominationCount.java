package com.freecharge.common.util;

import java.math.BigDecimal;

import com.freecharge.common.framework.basedo.AbstractDO;

public class SuccessDenominationCount extends AbstractDO implements Comparable<SuccessDenominationCount> {
    private final BigDecimal successAmount;
    private final Double     successCount;
    private final boolean    isRechargeTypeSpecial;

    public SuccessDenominationCount(final BigDecimal successAmount, final Double successCount,
            final boolean isRechargeTypeSpecial) {
        this.successAmount = successAmount;
        this.successCount = successCount;
        this.isRechargeTypeSpecial = isRechargeTypeSpecial;
    }

    public BigDecimal getAmount() {
        return this.successAmount;
    }

    public Double getCount() {
        return this.successCount;
    }

    public boolean isSpecial() {
        return this.isRechargeTypeSpecial;
    }

    @Override
    public int compareTo(final SuccessDenominationCount other) {
        int countDiff = (int) (other.getCount() - this.getCount());
        if (countDiff == 0) {
            return (other.getAmount().intValue() - this.getAmount().intValue());
        }
        return countDiff;
    }
}
