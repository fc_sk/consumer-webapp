package com.freecharge.common.util;

public class AuthorizationParams {

	private String clientName;
	private String key;
	
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	public AuthorizationParams(String clientName, String key){
		setClientName(clientName);
		setKey(key);
	}
}
