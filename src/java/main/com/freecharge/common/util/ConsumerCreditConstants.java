package com.freecharge.common.util;

public class ConsumerCreditConstants {
	
	// Debit EMI Constants
	public static final String DEBIT_MERCHANT_NAME = "Freecharge Wallet";
	public static final String DEBIT_MERCHANT_ID = "AEUvagL322Jets";
	public static final String DEBIT_LENDER_NAME = "Axis Bank";
	
	
	public static final String LOAN_DESTINATION_BANK = "BANK";
	public static final String LOAN_DESTINATION_WALLET = "WALLET";
}
