package com.freecharge.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;

public final class OrderIdUtil {
	private static final Logger logger = LoggingFactory.getLogger(OrderIdUtil.class);
	
    public static String getOrderIdPrefix(Integer afficiateId, Integer producMasterId, Integer channelId) {
		StringBuffer sb = new StringBuffer();
		Map<Integer, String> afflicateMasterMap = FCConstants.afflicateMasterMap;
		Map<Integer, String> productMasterMap = FCConstants.productMasterMap;
		Map<Integer, String> channelMasterMap = FCConstants.channelMasterMap;
		logger.info("entered into the appendOrderId method");
		try {
		    
			if (afflicateMasterMap.get(afficiateId) == null) {
				sb.append(FCConstants.DEFAULT_AFFILIATE_ID);
			} else {
				sb.append(afflicateMasterMap.get(afficiateId));
			}
			if (productMasterMap.get(producMasterId) == null) {
				throw new IllegalAccessException("Product id cannot be null");
			} else {
				sb.append(productMasterMap.get(producMasterId));
			}
			if (channelMasterMap.get(channelId) == null) {
				sb.append(FCConstants.DEFAULT_CHANNEL_ID);
			} else {
				sb.append(channelMasterMap.get(channelId));
			}
			DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
			String date = dateFormat.format(new Date());
			sb.append(date);

			logger.debug("Leaving from the getOrderIdPrefix method");
		}

		catch (Exception ex) {
			logger.error("Some exception raised while generating orderId the exception is " + ex);
		}
		return sb.toString();
	}
    
    public static String getFullOrderId(String orderIdPrefix, String orderIdSuffix) {
		StringBuffer orderIdStringBuffer = new StringBuffer();
		orderIdStringBuffer.append(orderIdSuffix);
		
		if(orderIdStringBuffer.length() < 7) {
			while(orderIdStringBuffer.length() < 7) {
				orderIdStringBuffer.insert(0, "0");
			}			
		}
		orderIdStringBuffer.insert(0, orderIdPrefix);
		return orderIdStringBuffer.toString();
	}

    
    public static final String generateWebRedemptionOrderIdForBlockingCodes(int couponStoreId) {
		String orderId = couponStoreId + "_"
				+ Calendar.getInstance().getTimeInMillis();
		if (orderId.length() > 20) {
			orderId = orderId.substring(0, 20);
		}
		return orderId;
	}
}
