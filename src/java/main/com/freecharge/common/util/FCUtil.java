package com.freecharge.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.NumberUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.joda.time.DateMidnight;
import org.joda.time.Days;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.freecharge.app.domain.entity.CommonLogDetails;
import com.freecharge.app.domain.entity.ProductMaster.ProductName;
import com.freecharge.app.domain.entity.jdbc.InAggrOprCircleMap;
import com.freecharge.app.domain.entity.jdbc.InResponse;
import com.freecharge.cardstorage.CardDetailsDo;
import com.freecharge.cardstorage.CardStorageKey;
import com.freecharge.ccsclient.AbstractCCSConfig;
import com.freecharge.ccsclient.CCS;
import com.freecharge.ccsclient.ICCS;
import com.freecharge.ccsclient.ICCSConfig;
import com.freecharge.ccsclient.exception.CCSException;
import com.freecharge.common.framework.basedo.AbstractDO;
import com.freecharge.common.framework.exception.FCRuntimeException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;
import com.freecharge.mobile.constant.MobileConstants;
import com.freecharge.payment.dos.entity.PaymentRefundTransaction;
import com.freecharge.payment.dos.entity.PaymentTransaction;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.businessdao.AggregatorInterface.AggregatorName;
import com.freecharge.recharge.services.EuronetService;
import com.freecharge.recharge.services.OxigenService;
import com.google.api.client.util.Strings;
import com.google.common.base.CaseFormat;
import com.google.common.base.CharMatcher;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import flexjson.JSONDeserializer;

public class FCUtil {
    //todo - this is not the best approach to do this, hacky move

    private static final Logger logger = LoggingFactory.getLogger(FCUtil.class);

    // List of domains to which we do not want SMS and mails to be sent.
    private static final  List<String> blockedDomainListForSmsAndMails = new ArrayList<String>(Arrays.asList("@moolahapp.com"));
    public static <T> T notNull(T inst) {
        if (inst == null) {
            throw new NullPointerException();
        }
        return inst;
    }

    public static String stringify(Object any) {
        return any == null? "NULL": ("'" + any + "'");
    }

    public final static String blankIf(String str) {
        return (str == null ? "" : str);
    }

    public final static String URLEncode(String str) {
        if (str == null)
            return null;
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new FCRuntimeException("Unsupported encoding", e);
        }
    }
    
    @SuppressWarnings({"resource", "rawtypes"})
    public final static String getBuildVersionNumber(Class clazz) {
        URL url = clazz.getProtectionDomain().getCodeSource().getLocation();
        logger.debug("examining url " + url);
        String version = "unknown";
        try {
            Manifest manifest = null;
            if (url.toString().endsWith("jar")) {
                JarFile jar = new JarFile(new File(url.toURI()));
                manifest = jar.getManifest();
            } else {
                // assuming a webapp in WEB-INF classes dir
                String path = url.getPath();
                if (path.indexOf("WEB-INF") != -1) {
                    path = path.substring(0, path.indexOf("WEB-INF"));
                    File manFile = new File(path, "META-INF/MANIFEST.MF");
                    if (manFile.exists()) {
                        manifest = new Manifest(new FileInputStream(manFile));
                    } else {
                        logger.debug("Can't find Manifest: " + manFile.toString());
                    }
                } else {
                    logger.debug("Path not in webapp? : " + path);
                }
            }
            if (manifest != null) {
                Attributes attr = manifest.getMainAttributes();
                Attributes.Name key = new Attributes.Name("Implementation-Version");
                if ((attr != null) && attr.containsKey(key)) {
                    version = attr.getValue(key);
                    logger.debug("found version # " + version);
                }
            }
        } catch (IOException ioe) {
            logger.error(ioe.getMessage(), ioe);
        } catch (URISyntaxException urise) {
            logger.error(urise.getMessage(), urise);
        }
        return version;
    }

    public static String getRandomMerchantRefNoEuro() {
        Random random = new Random();
        String val = random.nextInt() + "";
        val = val.replace("-", "");
        //System.out.println(val);
        return val;
    }

    public static InAggrOprCircleMap getInAggrOprCircleMap(List<InAggrOprCircleMap> inAggrOprCircleMaps, String inIdentifier) {
        InAggrOprCircleMap inAggrOprCircleMapRet = null;

        if (CollectionUtils.isNotEmpty(inAggrOprCircleMaps)) {
            for (InAggrOprCircleMap inAggrOprCircleMap : inAggrOprCircleMaps) {
                if (inAggrOprCircleMap.getAggrName().equalsIgnoreCase(inIdentifier)) {
                    inAggrOprCircleMapRet = inAggrOprCircleMap;
                }
            }
        }

        return inAggrOprCircleMapRet;
    }
    
    private static Map<String, Integer> PRODUCT_TYPE_ID_MAP = FCUtil.createMap(
    		"V", 1,
    		"D", 2,
    		"C", 3,
    		"F", 12,
    		"M", 201,
    		"X", 21);

    public static Integer getProductIntFromProductName(String productName){
    	Integer id = PRODUCT_TYPE_ID_MAP.get(productName);
    	return id==null? 0: id;
    }
    
    public static Map<String, String> stringToHashMap(String responseString) {
        String[] splitArray = responseString.split("&");
        Map<String, String> map = new HashMap<String, String>();
        if (splitArray.length > 1) {
            for (int i = 0; i < splitArray.length; i++) {
                String[] splitEqual = splitArray[i].split("=");
                String part0 = splitEqual[0].trim();
                String part1 = "";

                if (splitEqual.length == 2) {
                    part1 = splitEqual[1];
                }

                map.put(part0, part1);
            }
        }
        return map;
    }

    public static String getLookupID() {
        return UUID.randomUUID().toString();
    }

    public static void assertNonNullArgument(Object target, String name) {
        if (target == null) {
            throw new IllegalArgumentException(String.format("Expected valid '%s' argument, but found NULL", name));
        }
    }

    public static void assertNonEmptyArgument(String target, String name) {
    	if (target == null || target.trim().isEmpty()) {
    		throw new IllegalArgumentException(String.format(
    				"Expected non-empty '%s' argument, but found: ",
    				name, target == null? "NULL": "" + '"' + target + '"'));
    	}
    }

    public static void assertAnyNonEmptyArgument(Map<String, String> args) {
    	for (String key: args.keySet()) {
    		if (!isEmpty(args.get(key))) {
    			return;
    		}
    	}
    	throw new IllegalArgumentException(String.format(
    			"Either of the arguments %s must be non-empty",
    			args.values().toString()));
    }

    public static void assertAllNonEmptyArgument(Map<String, String> args) {
    	for (String key: args.keySet()) {
    		assertNonEmptyArgument(key, args.get(key));
    	}
    }

    /**
     * Make the passed in integer null safe i.e if the passed in integer is null, convert it to 0.
     *
     * @param integer
     * @return int
     */
    public static int makeNullSafe(Integer integer) {
        if (integer == null) {
            return 0;
        }

        return integer.intValue();
    }

    public static <T> T getFirstElement(Collection<T> collection) {
        if (collection != null && !collection.isEmpty()) {
            return collection.iterator().next();
        }

        return null;
    }

    public static boolean shouldMockAPIs() {
        return false;
    }
    
    public static boolean areEqual(BigDecimal bigDecimal0, BigDecimal bigDecimal1) {
        return bigDecimal0.compareTo(bigDecimal1) == 0;
    }
    
    public static boolean isRetriedOrderId(String original, String retried) {
        return (original.length() != retried.length()) && (retried.startsWith(original));
    }

    //todo : Should be using google collections library to achieve this instead of hand coding.
    public static<K, V> Map<V, K> flip(Map<K, V> map) {
        Map<V, K> flippedMap = new HashMap<V, K>(map.size());

        for (Map.Entry<K, V> entry : map.entrySet()) {
            K key = entry.getKey();
            V value = entry.getValue();

            flippedMap.put(value, key);
        }

        return flippedMap;
    }

    //todo : Should be using google collections library to achieve this instead of hand coding.
    @SuppressWarnings("unchecked")
	public static<T> List<T> asList(T...t) {
        return new LinkedList<T>(Arrays.asList(t));
    }

    public static boolean containsPartialMatch(String toBeMatched, Collection<String> matchingPatterns) {
        for (String matchingPattern : matchingPatterns) {
            if (toBeMatched.endsWith(matchingPattern.trim())) {
                return true;
            }
        }

        return false;
    }

    public static String getNewLineCharacter() {
        return System.getProperty("line.separator");
    }

    @SuppressWarnings("deprecation")
	public static int noOfDays(Date from, Date to) {
        return Days.daysBetween(new DateMidnight(from), new DateMidnight(to)).getDays();
    }

    public static Date floor(Date date) {
        Calendar cal = Calendar.getInstance();       // get calendar instance
        cal.setTime(date);                           // set cal to date
        cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight (beginning of day)
        cal.set(Calendar.MINUTE, 0);                 // set minute in hour   (beginning of day)
        cal.set(Calendar.SECOND, 0);                 // set second in minute (beginning of day)
        cal.set(Calendar.MILLISECOND, 0);            // set millis in second (beginning of day)
        return cal.getTime();
    }

    public static Date ceiling(Date date) {
        Calendar cal = Calendar.getInstance();       // get calendar instance
        cal.setTime(date);                           // set cal to date
        cal.set(Calendar.HOUR_OF_DAY, 23);           // set hour to midnight (end of day)
        cal.set(Calendar.MINUTE, 59);                // set minute in hour   (end of day)
        cal.set(Calendar.SECOND, 59);                // set second in minute (end of day)
        cal.set(Calendar.MILLISECOND, 999);          // set millis in second (end of day)
        return cal.getTime();
    }

    public static Date currentDate() {
        Date date = new Date();
        return date;
    }
    
    public static Calendar getAsiaKolkataTimeNow() {
        /*Calendar gmtCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        long gmtTime = gmtCal.getTime().getTime();

        long asiaKolkataTime = gmtTime + TimeZone.getTimeZone("Asia/Calcutta").getRawOffset();
        Calendar asiaKolkataCal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
        asiaKolkataCal.setTimeInMillis(asiaKolkataTime);*/
        Calendar asiaKolkataCal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
        return asiaKolkataCal;
    }

    public static String getLowerCamelCase(String name) {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, name);
    }
    
    public static String getRedirectAction(String url) {
        return String.format("redirect:%s", url);
    }

    public static <T extends AbstractDO> String objectListTableString(List<T> data, String newline) {
    	List<Map<String, Object>> newData = new ArrayList<Map<String,Object>>();
    	for (T each: data) {
    		newData.add(each.toMap());
    	}
    	return tableString(newData, newline);
    }

    public static String tableString(List<Map<String, Object>> rows, String newline) {
    	if (rows == null || rows.isEmpty() || rows.get(0).isEmpty()) return "(no table data)";
    	// pass 1: determine column widths
    	Map<String, Object> firstRow = rows.get(0);
    	int columnCount = firstRow.size();
    	int[] columnWidths = new int[columnCount];
    	String[] columnNames = new String[columnCount];
    	int i = 0;
    	// find column name widths
    	for (String colname: firstRow.keySet()) {
    		if (columnWidths[i] < colname.length()) {
    			columnWidths[i] = colname.length();
    		}
    		columnNames[i++] = colname;
    	}
    	// find column content widths
    	for (Map<String, Object> each: rows) {
    		for (i = 0; i < columnCount; i++) {
    			Object val = each.get(columnNames[i]);
    			String str = val==null? "(null)": val.toString();
    			if (columnWidths[i] < str.length()) {
    				columnWidths[i] = str.length();
    			}
    		}
    	}
    	// pass 2: print it out
    	StringBuilder sb = new StringBuilder();
    	// print column names
    	for (i = 0; i < columnCount; i++) {
    		sb.append("| ");
    		sb.append(columnNames[i]);
    		sb.append(' ');
    	}
    	sb.append('|');
    	sb.append(newline);
    	// print column values
    	for (Map<String, Object> each: rows) {
    		printRow(sb, each, columnNames, columnWidths);
    		sb.append(newline);
    	}
    	return sb.toString();
    }

    private static StringBuilder printRow(StringBuilder sb, Map<String, Object> row,
    		String[] columnNames, int[] columnWidths) {
    	for (int i = 0; i < columnNames.length; i++) {
			sb.append("| ");
			Object val = row.get(columnNames[i]);
			String str = val==null? "(null)": val.toString();
			sb.append(str + repeat(" ", columnWidths[i] - str.length()));
			sb.append(' ');
    	}
    	sb.append('|');
    	return sb;
    }

    public static String repeat(String str, int times) {
    	if (times > 0) {
        	StringBuilder sb = new StringBuilder(str.length() * times);
        	for (int i = 0; i < times; i++) {
        		sb.append(str);
        	}
        	return sb.toString();
    	}
    	return "";
    }
    
	public static double roundOff(double doubleValue) {
		double finalValue = Math.round(doubleValue);
		return finalValue;
	}

    public static String trim(String str) {
    	if (str != null) {
    		return str.trim();
    	}
    	return null;
    }

    /**
     * Safe String.left operation
     */
    public static String left(String str, int count) {
    	if (str == null) return null;
    	return str.substring(0, Math.min(count, str.length()));
    }

    public static boolean isEmpty(String str) {
    	return str == null || "".equals(str);
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static boolean isEmpty(Collection<?> obj) {
    	return obj == null || obj.isEmpty();
    }

    public static boolean isEmpty(Map<?, ?> map) {
    	return map == null || map.isEmpty();
    }
    public static boolean isEmpty(String[] stringArr) {
    	return (stringArr == null || (stringArr.length < 1));
    }

    public static boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }

	public static String throwIfEmpty(String str, String name) {
		if (FCUtil.isEmpty(str)) {
			String msg = String.format("Expected a valid %s, but found %s",
					name, str==null? "NULL": "empty string");
			throw new IllegalArgumentException(msg);
		}
		return str;
	}
	

    // creating a hashmap quickly, for the lack of map literals in Java

    public static <K, V> Map<K, V> createMap(K key, V value) {
    	return Collections.singletonMap(key, value);
    }

    public static <K, V> Map<K, V> createMap(K key, V value, K key2, V value2) {
    	Map<K, V> m = new LinkedHashMap<K, V>(2);
    	m.put(key,  value);
    	m.put(key2,  value2);
    	return m;
    }

    public static <K, V> Map<K, V> createMap(K key, V value, K key2, V value2, K key3, V value3) {
    	Map<K, V> m = new LinkedHashMap<K, V>(3);
    	m.put(key,  value);
    	m.put(key2,  value2);
    	m.put(key3,  value3);
    	return m;
    }

    public static <K, V> Map<K, V> createMap(K key, V value, K key2, V value2, K key3, V value3, K key4, V value4) {
    	Map<K, V> m = new LinkedHashMap<K, V>(4);
    	m.put(key,  value);
    	m.put(key2,  value2);
    	m.put(key3,  value3);
    	m.put(key4,  value4);
    	return m;
    }

    public static <K, V> Map<K, V> createMap(K key, V value, K key2, V value2, K key3, V value3, K key4, V value4, K key5, V value5) {
    	Map<K, V> m = new LinkedHashMap<K, V>(4);
    	m.put(key,  value);
    	m.put(key2,  value2);
    	m.put(key3,  value3);
    	m.put(key4,  value4);
    	m.put(key5,  value5);
    	return m;
    }

    public static <K, V> Map<K, V> createMap(K key, V value, K key2, V value2, K key3, V value3, K key4, V value4, K key5, V value5, K key6, V value6) {
    	Map<K, V> m = new LinkedHashMap<K, V>(4);
    	m.put(key,  value);
    	m.put(key2,  value2);
    	m.put(key3,  value3);
    	m.put(key4,  value4);
    	m.put(key5,  value5);
    	m.put(key6,  value6);
    	return m;
    }

    public static <K, V> Map<K, V> createMap(K key, V value, K key2, V value2, K key3, V value3, K key4, V value4, K key5, V value5, K key6, V value6, K key7, V value7) {
    	Map<K, V> m = new LinkedHashMap<K, V>(4);
    	m.put(key,  value);
    	m.put(key2,  value2);
    	m.put(key3,  value3);
    	m.put(key4,  value4);
    	m.put(key5,  value5);
    	m.put(key6,  value6);
    	m.put(key7,  value7);
    	return m;
    }

    public static <K, V> Map<K, V> createMap(K key, V value, K key2, V value2, K key3, V value3, K key4, V value4, K key5, V value5, K key6, V value6, K key7, V value7, K key8, V value8) {
    	Map<K, V> m = new LinkedHashMap<K, V>(4);
    	m.put(key,  value);
    	m.put(key2,  value2);
    	m.put(key3,  value3);
    	m.put(key4,  value4);
    	m.put(key5,  value5);
    	m.put(key6,  value6);
    	m.put(key7,  value7);
    	m.put(key8,  value8);
    	return m;
    }

    @SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> createMap(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7, K k8, V v8, K k9, Object... more) {
    	if (more.length % 2 != 1) {
    		throw new IllegalArgumentException("Expected even number of arguments, but found " + (9 + more.length));
    	}
    	Map<K, V> m = new LinkedHashMap<K, V>(9 + more.length / 2);
    	m.put(k1, v1);
    	m.put(k2, v2);
    	m.put(k3, v3);
    	m.put(k4, v4);
    	m.put(k5, v5);
    	m.put(k6, v6);
    	m.put(k7, v7);
    	m.put(k8, v8);
		V v9 = (V) more[0];
    	m.put(k9, v9);
    	for (int i = 1; i < more.length; i += 2) {
    		m.put((K) more[i], (V) more[i+1]);
    	}
    	return m;
    }

    public static boolean isAjaxCall(HttpServletRequest request){
    	return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

    public static long minutesToSeconds(long minutes) {
        return TimeUnit.SECONDS.convert(minutes, TimeUnit.MINUTES);
    }
    
    public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    return true;
	}

    public static boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean isValidTxnAmount(String str) {
    	try {
    		 if (!Strings.isNullOrEmpty(str)) {
                Double.parseDouble(str);
        		return true;
    		 } else {
    			 return false;
    		 }
    	} catch(NumberFormatException e) {
    		return false;
    	}
        
    }
    
    public static boolean isBoolean(String s) {
	    try { 
	        Boolean.parseBoolean(s); 
	    } catch(Exception e) { 
	        return false; 
	    }
	    return true;
	}
    
    public static String getStringValue(Object o){
    	String s = null;
    	if (o != null){
    		s = String.valueOf(o);
    	}
    	return s;
    }
    
    public static boolean isMobileRecharge(String productType){
        boolean isMobileRecharge = false;
        if (productType != null && (productType.equals("V") || productType.equals("M"))){
            isMobileRecharge = true;
        }
        return isMobileRecharge;
    }
    
	public static boolean isDataCard(String productType) {
		boolean isDataCard = false;
		if (productType != null && (productType.equals("C") || productType.equals("F"))) {
			isDataCard = true;
		}
		return isDataCard;
	}
	
	public static boolean isDTH(String productType) {
		boolean isDTH = false;
		if (productType != null && (productType.equals("D"))) {
			isDTH = true;
		}
		return isDTH;
	}

    
	public static boolean isMobilePrepaidRecharge(String productType) {
		boolean isMobilePrepaid = false;
		if (productType != null && (productType.equals("V"))) {
			isMobilePrepaid = true;
		}
		return isMobilePrepaid;
	}
    
    public static boolean isPrePaidRecharge(String productType){
        boolean isPrePaidRecharge = false;
        if (productType != null && productType.equals("V")){
            isPrePaidRecharge = true;
        }
        return isPrePaidRecharge;
    }
    
    public static boolean isLandlineRecharge(String productType){
        boolean isLandlineRecharge = false;
        if (productType != null && productType.equals(FCConstants.PRODUCT_TYPE_LANDLINE)){
            isLandlineRecharge = true;
        }
        return isLandlineRecharge;
    }
    
    public static boolean isBroadbandRecharge(String productType){
        boolean isBroadbandRecharge = false;
        if (productType != null && productType.equals(FCConstants.PRODUCT_TYPE_BROADBAND)){
            isBroadbandRecharge = true;
        }
        return isBroadbandRecharge;
    }
    
    public static boolean isPrePaidDatacard(String productType){
        boolean isPrePaidRecharge = false;
        if (productType != null && productType.equals("C")){
            isPrePaidRecharge = true;
        }
        return isPrePaidRecharge;
    }
    
    public static boolean isRechargeProductType(String productType){
    	boolean isRechargeProductType = false;
    	if (productType != null && (productType.equals("V") || productType.equals("D") || productType.equals("C") || productType.equals("F") || productType.equals("X"))){
    		isRechargeProductType = true;
    	}
    	return isRechargeProductType;
    }
    
	public static boolean isRechargeBillerProductType(String productType) {
		boolean isRechargeBillerProductType = false;
		if (productType != null && (productType.equals("V") || productType.equals("D") || productType.equals("C")
				|| productType.equals("Z") || productType.equals("X"))) {
			isRechargeBillerProductType = true;
		}
		return isRechargeBillerProductType;
	}
    
    public static boolean isBillPostpaidType(String productType){
        boolean isRechargeProductType = false;
        if (productType != null && (productType.equals("M"))){
            isRechargeProductType = true;
        }
        return isRechargeProductType;
    }
    
    public static boolean isUtilityPaymentType(String productType){
        if (productType != null){
            ProductName productName = ProductName.fromCode(productType);
            boolean flag = productName.isUtilityPaymentProduct();
            logger.info("isUtilityPaymentType flag : "+flag);
            return flag;
        }
        logger.info("isUtilityPaymentType flag : false");
        return false;
    }
    
    public static boolean isMetroPaymentType(String productType){
        if (productType != null){
            ProductName productName = ProductName.fromCode(productType);
            return productName == ProductName.Metro;
        }
        return false;
    }
    
    public static boolean isGoogleCreditRechargeType(String productType){
    	boolean isGoogleCreditRechargeType = false;
        if (productType != null && (productType.equalsIgnoreCase(FCConstants.PRODUCT_TYPE_GOOGLE_CREDITS))){
        	isGoogleCreditRechargeType = true;
        }
        return isGoogleCreditRechargeType;
    }
    
    public static String getUtilityProductTypes() {
        StringBuffer productListForMysql = new StringBuffer("");
        for (ProductName productName : ProductName.getUtilityProducts()) {
            productListForMysql.append("'" + productName.getProductType() + "',");
        }
        if (productListForMysql.toString().endsWith(",")) {
            return productListForMysql.substring(0, productListForMysql.toString().length() - 1);
        }
        return productListForMysql.toString();
    }
    
    /**
     * Returns a random integer between two boundaries, with lower being inclusive and the upper being exclusive.
     * @param lower
     * @param upper
     * @return
     */
    public static int randomIntBetween(int lower, int upper) {
        return new Random().nextInt(upper - lower) + lower;
    }
    
    public static Date getCurrentMonthEndDate(){
    	int year = Calendar.getInstance().get(Calendar.YEAR);
    	int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
		int[] daysInAMonth = { 29, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30,
				31 };
		int day = daysInAMonth[month];
		boolean isLeapYear = new GregorianCalendar().isLeapYear(year);

		if (isLeapYear && month == 2) {
			day++;
		}
		GregorianCalendar gc = new GregorianCalendar(year, month - 1, day, 23, 59);
		java.util.Date monthEndDate = new java.util.Date(gc.getTime().getTime());
		return monthEndDate;
    }
    
    public static String delimitedString(String delimiter, Iterable<String> iterable) {
        Iterator<String> iterator = iterable.iterator();
        StringBuilder description= new StringBuilder();
        boolean startFlag = false; 
        while(iterator.hasNext()) {
            if(startFlag) {
                description.append(delimiter);
            }
            description.append(iterator.next());
            startFlag=true;
        }
        return description.toString();
    }
    
    public static String getStackTrace(Throwable e)  {
        StringWriter w = new StringWriter();
        e.printStackTrace(new PrintWriter(w));
        return w.getBuffer().toString();
        
    }
    
    public static Date getNDaysPastDate(int days){
    	Date today = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(today);
		cal.add(Calendar.DAY_OF_MONTH, -days);
		return cal.getTime();
    }

    /*Validation for Customer trial search */
    public static Boolean isEmailIdFormat(final String searchInput) {
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        Boolean isEmail = searchInput.matches(EMAIL_REGEX);
        return isEmail;
    }
    
    /*Validation for email for new user registration */
    public static Boolean isValidEmailFormat(final String searchInput) {
        String EMAIL_REGEX = "^[\\w_\\.]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        Boolean isEmail = searchInput.matches(EMAIL_REGEX);
        return isEmail;
    }

    public static boolean isValidSearchEmailFormat(String searchInput) {
        String EMAIL_REGEX = "^[A-Za-z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]+$";
        Boolean isEmail = searchInput.matches(EMAIL_REGEX);
        return isEmail;
    }
    
	public static Boolean validateOrderIdFormat(final String searchInput) {
		if (searchInput.length() >= 4) {
			String firstTwoChars = searchInput.substring(0, 2);
			String thirdChar = String.valueOf(searchInput.charAt(2));
			String fourthChar = String.valueOf(searchInput.charAt(3));

			if (FCConstants.afflicateMasterMap.containsValue(firstTwoChars)) {
				if (FCConstants.productMasterMap.containsValue(thirdChar)) {
					if (FCConstants.channelMasterMap.containsValue(fourthChar)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static Boolean isMobileNoFormat(final String searchInput) {
		String MOBILE_NO_REGEX = "\\d{10}";
		if (Strings.isNullOrEmpty(searchInput)) {
			return false;
		} else {
			return searchInput.matches(MOBILE_NO_REGEX);
		}
	}
    
	public static Boolean isValidLandLineFormat(final String searchInput) {
		String LAND_NO_REGEX = "\\d{6,12}";
		if (Strings.isNullOrEmpty(searchInput)) {
			return false;
		} else {
			return searchInput.matches(LAND_NO_REGEX);
		}
	}
    
	public static Boolean isPostalCodeFormat(final String searchInput) {
		String POSTAL_NO_REGEX = "\\d{6}";
		if (Strings.isNullOrEmpty(searchInput)) {
			return false;
		} else {
			return searchInput.matches(POSTAL_NO_REGEX);
		}
	}
    
    public static String getChannelTypeAsDescriptor(String channelType) {
        switch (Integer.parseInt(channelType)){
            case 1: return "WEB";
            case 2: return "MOBILE";
            case 3: return "APP";
            case 5: return "IOS";
            case 6: return "WINDOWS";
            default: return "Not a valid Channel";
        }
    }

	public static boolean isCommaSeparatedIntegers(String categoryIdsStr) {
		boolean isValid = false;
		if (categoryIdsStr == null){
			return isValid;
		}
		StringTokenizer tokenizer = new StringTokenizer(categoryIdsStr, ",");
		while (tokenizer.hasMoreTokens()){
			if (!isInteger(tokenizer.nextToken())){
				return isValid;
			}
		}
		return true;
	}

	public static List<Integer> getIntegerList(String categoryIdsStr) {
		List<Integer> intList = new ArrayList<>();
		StringTokenizer tokenizer = new StringTokenizer(categoryIdsStr, ",");
		while (tokenizer.hasMoreTokens()){
			String tokenStr  = tokenizer.nextToken();
			if (isInteger(tokenStr)){
				intList.add(Integer.parseInt(tokenStr));
			}
		}
		return intList;
	}
    
    public static String getChannelFromOrderId(final String orderId) {
        if (orderId != null && validateOrderIdFormat(orderId)) {
            String fourthChar = String.valueOf(orderId.charAt(3));
            return FCConstants.channelNameMasterMap.get(fourthChar);
        } else {
            logger.info("Malformed order id : " + orderId);
            return null;
        }
    }
    
    public static String getChannelFromChannelType(final String channelType) {
        return FCConstants.channelNameMasterMap.get(channelType);
    }
    
    public static boolean isEmailBlockedForDomain(String email) {
        if (!FCUtil.isEmpty(email)) {
            for (String domain : blockedDomainListForSmsAndMails) {
                if (email.endsWith(domain)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     * @param calender
     * @return Date as a string in yyyy-MM-dd HH:mm:ss format from calender
     * @throws ParseException
     */
    public static String getFormattedDateFromCalender(Calendar calender) throws ParseException {
        SimpleDateFormat sdfActual = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy");
        Date validFromParsed = sdfActual.parse(String.valueOf(calender.getTime()));
        SimpleDateFormat sdfWanted = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdfWanted.format(validFromParsed);
    }

    public static <T> T deserialize(String dataMapStr) {
        return  new JSONDeserializer<T>().deserialize(dataMapStr);
    }
    /**
     * Compare two objects for equality.
     * @param one first object
     * @param two second object
     * @return true if both objects are equal, false otherwise (either being null results in false)
     */
    public static boolean equal(Object one, Object two) {
        if (one == null || two == null) {
            return false;
        }
        return one.equals(two);
    }

    /**
     * Compare two objects of the same type for equality.
     * @param one first object
     * @param two second object
     * @return true if both objects are equal, false otherwise (either being null results in false)
     */
    public static <T> boolean equal(T one, T two, Class<T> clazz) {
        if (one == null || two == null) {
            return false;
        }
        return one.equals(two);
    }

    /**
     *
     * @param s
     * @return if given string is a long
     */
    public static boolean isLong(String s) {
        try {
            Long.parseLong(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param s
     * @return ifgiven string is a float
     */
    public static boolean isFloat(String s) {
        try {
            Float.parseFloat(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param date
     * @param time
     * @param format
     * @return if given date and time make a valid date in the format given
     */
    public static boolean isValidDate(String date, String time, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        if (StringUtils.isEmpty(time)) {
            time = "00:00:00";
        }
        try {
            calendar.setTime(simpleDateFormat.parse(date + " " + time));
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param date
     * @param time
     * @param format
     * @return
     */
    public static Optional<Calendar> getCalenderFromDateAndTime(String date, String time, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        if (StringUtils.isEmpty(time)) {
            time = "00:00:00";
        }
        try {
            calendar.setTime(simpleDateFormat.parse(date + " " + time));
        } catch (ParseException e) {
            return Optional.absent();
        }
        return Optional.of(calendar);
    }
    
    @SuppressWarnings("unchecked")
	public static JSONObject getAddressFromAppLocation(String address){
        if (FCStringUtils.isBlank(address)){
            return null;
        }
        //Sample address
        //"Address[addressLines=[0:\"648/L, Old Madras Rd\",1:\"Indira Nagar II Stage, Binnamangala, Hoysala Nagar, Indira Nagar\",2:\"Bengaluru, Karnataka 560038\",3:\"India\"],feature=648/L,admin=Karnataka,sub-admin=Bangalore Urban,locality=Bengaluru,thoroughfare=Old Madras Rd,postalCode=null,countryCode=IN,countryName=India,hasLatitude=true,latitude=12.983034,hasLongitude=true,longitude=77.6387139,phone=null,url=null,extras=null]";

        String re1 = "\\[.*\\]";
        Pattern p1 = Pattern.compile(re1);
        Matcher m1 = p1.matcher(address);
        m1.find(); //Removes Address from the start

        String addressPart = address.substring(m1.start()+1, m1.end()-1);

        //Now run same regex again for addressLine
        m1 = p1.matcher(addressPart);
        m1.find();

        int adrLineEnd = m1.end();
        String addrLine = addressPart.substring(m1.start()+1, adrLineEnd-1);

        JSONObject res = new JSONObject();
        JSONArray list = new JSONArray();
        String city = null;

        String [] adrAdminParts = addressPart.substring(adrLineEnd+1).split(",");
        for (String part: adrAdminParts){
            String keyVal[] = part.split("=");
            if (keyVal.length>1){
                Map<String, Object> map = new HashMap<>();
                map.put(keyVal[0], keyVal[1]);
                list.add(map);
                if (keyVal[0].equals("locality")){
                    city = keyVal[1];
                }
            }
        }
        res.put("address_parts", list);
        res.put("city", city);

        //Now put full address
        String re2 = "\"[^\"]+\"";
        Pattern p2 = Pattern.compile(re2);
        Matcher m2 = p2.matcher(addrLine);

        String fullAddress = "";
        while (m2.find()){
            fullAddress+=" "+addrLine.substring(m2.start()+1, m2.end()-1);
        }
        res.put("address", fullAddress.substring(1));

        return res;
    }

    public static boolean checkStringInCommaSeparatedStringList(String list, String s ) {
        List<String> splitList = Arrays.asList(list.toLowerCase().split("[,]"));
        if (!FCUtil.isEmpty(splitList) && splitList.contains(s)) {
            return true;
        }
        return false;
    }

    public static boolean isValidCity(String city){
        if (FCStringUtils.isBlank(city)){
            return false;
        }
        for (char c: city.toCharArray()){
            if (c<32 || (c>32&&c<65) || (c>90&&c<97) || c>122){
                return false;
            }
        }
        return true;
    }

    public static List<String> getStringList(String str, String delimiter) {
		List<String> stringList = new ArrayList<>();
    	StringTokenizer tokenizer = new StringTokenizer(str, delimiter);
		while (tokenizer.hasMoreTokens()){
			String tokenStr  = tokenizer.nextToken();
			stringList.add(tokenStr);
		}
		return stringList;
	}    

    public static boolean isWelFormedName(String name) {
        if (Strings.isNullOrEmpty(name)) {
            return false;
        }
        return !(CharMatcher.anyOf("[\\<\\>\\'\\\"=%\\[@#!?!$;,]\\(\\)\\\\\\]").matchesAnyOf(name));
    }
    
    public static boolean isWelFormedFieldValue(String dataFiledValue) {
        if (Strings.isNullOrEmpty(dataFiledValue)) {
            return false;
        }
        return !(CharMatcher.anyOf("[\\<\\>\\'\\\"[=%!$;,?]\\(\\)\\\\]").matchesAnyOf(dataFiledValue));
    }

    public static boolean isValidBankAccountNumber(String accountNumber) {
    	if(StringUtils.isNumeric(accountNumber) && accountNumber.length() > 8 &&  
        		accountNumber.length() < 19) {
    		return true;
    	}
    	return false;
    }

    public static boolean isValidIfscCode(String ifscCode) {
    	
    	if(ifscCode.length() == 11) {
    		String alphaString = ifscCode.substring(0, 4);
    		String pattern = "[A-za-z]*";
    		Pattern r = Pattern.compile(pattern);
    		Matcher m = r.matcher(alphaString);
    		if (m.matches()) {
    			String alphaNumStr = ifscCode.substring(ifscCode.length()-7, ifscCode.length());
    			String numericStr = ifscCode.substring(4,5);
    			 if (numericStr.equals("0") && StringUtils.isAlphanumeric(alphaNumStr)) {
        			  return true;
        		 }   		           
       }
    }
   return false;
  }
    
    public static String getJsonString(Object object) throws JsonGenerationException,
    JsonMappingException, IOException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(object);
        return json;
    }
    
    private static ObjectMapper mapper = new ObjectMapper();
	public static String serialize(Object o) {
		if (o == null) {
			throw new NullPointerException("Object passed for serialization is null!");
		}
		try {
			return mapper.writeValueAsString(o);
		} catch (IOException e) {
			logger.error("Error while serializing the object to json.", e);
			throw new RuntimeException(e);
		}
	}

    public static Optional<String> getTac(String imei) {
        if (isNotEmpty(imei) && imei.length() > 8 && validateImei(imei)) {
            return Optional.of(imei.substring(0,8));
        }
        return Optional.absent();
    }

    @SuppressWarnings("deprecation")
	public static boolean validateImei(String imeiNumber) {
        if(isNotEmpty(imeiNumber) &&
                NumberUtils.isDigits(imeiNumber) && imeiNumber.length() == 15) {
            return true;
        }
        return false;
    }
    
    public static Set<String> spiltStringOnNewLine(final String inputIdsString) {
    	String[] inputIds = inputIdsString.split("\n");
		Set<String> inputIdSet = new HashSet<String>();
		for (String inputid : inputIds) {
			String inputId = StringUtils.trim(inputid);
			if (StringUtils.isNotBlank(inputId)) {
				inputIdSet.add(inputId);
			}
		}
		return inputIdSet;
    }
    
    public static boolean isVirtualCardBin(String cardNumber) {
        if (StringUtils.isEmpty(cardNumber)) {
            return false;
        }
        String cardPrefix = StringUtils.left(cardNumber, 8);
        if (PaymentConstants.VIRTUAL_CARD_BIN_LIST.contains(cardPrefix)) {
            return true;
        }
        return false;
    }
    
    /*Splits the file contents on every newline and returns Set of strings */
    public static Set<String> getStringSetFromFile(final MultipartFile file)
			throws IOException {
		Set<String> inputIdSet = new HashSet<>();
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(file.getInputStream()));
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			String inputId = StringUtils.trim(line);
			inputIdSet.add(inputId);
		}
		return inputIdSet;
	}
    
    public static Double calculateTotalRefunds(List<PaymentRefundTransaction> refundList) {
        Double refundAmount = new Double(FCConstants.ZERO);
        for (PaymentRefundTransaction paymentRefundTransaction : refundList) {
            refundAmount += paymentRefundTransaction.getRefundedAmount().getAmount().doubleValue();
        }
        return refundAmount;
    }

    public static Double calculateTotalPaid(List<PaymentTransaction> paymentTransactionList) {
        Double totalPaidAmount = new Double(FCConstants.ZERO);
        for (PaymentTransaction paymentTransaction : paymentTransactionList) {
            totalPaidAmount += paymentTransaction.getAmount();
        }
        return totalPaidAmount;
    }
    
    public static boolean isRefundableUserTxnForPaySuccess(String txnStaus) {
        if (StringUtils.isEmpty(txnStaus)) {
            return false;
        }

        if (!FCConstants.SUCCESS_OR_PENDING_UTH_STATUS.contains(txnStaus)) {
            return true;
        }
        return false;
    }

    public static String getSuccessOrPendingOrFinalTransactionStatus() {
        StringBuffer successOrPendingListMysql = new StringBuffer("");
        
        for (String txnStatus : PaymentConstants.paymentTransactionStatusToExcludeForRefund) {
            successOrPendingListMysql.append("'" + txnStatus + "',");
        }
        
        for (String txnStatus : FCConstants.SUCCESS_OR_PENDING_UTH_STATUS) {
            successOrPendingListMysql.append("'" + txnStatus + "',");
        }
        
        successOrPendingListMysql.append("'101','104','111'");
        
        if (successOrPendingListMysql.toString().endsWith(",")) {
            return successOrPendingListMysql.substring(0, successOrPendingListMysql.toString().length() - 1);
        }
        return successOrPendingListMysql.toString();
    }
    
    public static InResponse getLatestResponse(List<InResponse> responseList) {
        InResponse latestResponse = responseList.get(0);
        
        if (responseList.size() > 1) {
            for(int i = 1; i < responseList.size(); i++) {
                if (responseList.get(i).getResponseId() > latestResponse.getResponseId()) {
                    latestResponse = responseList.get(i);
                }
            }
        }
        
        return latestResponse;
    }
    
    public static String getOperatorCode(String rawRequest, String aggrName) {
        final String NULL_OPERATOR = "NULLOPERATOR";
        if (StringUtils.isEmpty(rawRequest) || StringUtils.isEmpty(aggrName)) {
            return NULL_OPERATOR;
        }
        String[] nameValue = null;
        String operatorCode = null;
        if (aggrName.equalsIgnoreCase(AggregatorName.EURONET.name())) {
            String[] rawRequestTokens = rawRequest.split(",");
            for (String nameValuePair : rawRequestTokens) {
                nameValue = nameValuePair.split("=");
                if (StringUtils.isNotEmpty(EuronetService.SPCODE)
                        && EuronetService.SPCODE.equalsIgnoreCase(nameValue[0].trim())) {
                    operatorCode = nameValue[1].trim();
                    break;
                }
            }
        }
        if (aggrName.equalsIgnoreCase(AggregatorName.OXIGEN.name())) {
            Matcher masterMatcher = OxigenService.masterPattern.matcher(rawRequest);
            while (masterMatcher.find()) {
                nameValue = masterMatcher.group().split("=");
                if (StringUtils.isNotEmpty(OxigenService.MERCHANTREFNO)
                        && OxigenService.MERCHANTREFNO.equalsIgnoreCase(nameValue[0].trim())) {
                    String merchantRefNo = nameValue[1].trim();
                    if (merchantRefNo.matches(OxigenService.pattern1.pattern())) {
                        Matcher match = OxigenService.pattern1.matcher(merchantRefNo);
                        match.matches();
                        operatorCode = match.group(3).trim();
                        break;
                    }
                    if (merchantRefNo.matches(OxigenService.pattern2.pattern())) {
                        Matcher match = OxigenService.pattern2.matcher(merchantRefNo);
                        match.matches();
                        operatorCode = match.group(2).trim();
                        break;
                    }
                    if (merchantRefNo.matches(OxigenService.pattern3.pattern())) {
                        Matcher match = OxigenService.pattern3.matcher(merchantRefNo);
                        match.matches();
                        operatorCode = match.group(2).trim();
                        break;
                    }
                    if (merchantRefNo.matches(OxigenService.pattern4.pattern())) {
                        Matcher match = OxigenService.pattern4.matcher(merchantRefNo);
                        match.matches();
                        operatorCode = match.group(1).trim();
                        break;
                    }
                }
            }
        }

       return StringUtils.isEmpty(operatorCode) ? NULL_OPERATOR : operatorCode;
    }
    
    public static String[] getCCSValues(String key, String basePath, String ccsHost) {
		String value = getValueFromCCS(key, basePath, ccsHost);
		String[] urlList = null;
		if (!FCUtil.isEmpty(value)) {
			urlList = value.split(",");
		}
		return urlList;
	}
    
    private static final Cache<String,String> localCache  = CacheBuilder.newBuilder().expireAfterWrite(15, TimeUnit.MINUTES).build();
    
    private static final String NULL_VALUE_REPRESENTATION = "cmVkaXMgZGVhZCBiZWVm";
    
    public static String getValueFromCCS(String key, String basePath, String ccsHost)
    {
    	String value = localCache.getIfPresent(basePath + key);
    	if( NULL_VALUE_REPRESENTATION.equals(value) ) {
    		return null;
    	}
    	if( !FCUtil.isEmpty(value) ) {
    		return value;
    	}
    	
    	value = null;
    		
		ICCS ccs;
		ICCSConfig ccsConfig = new AbstractCCSConfig();
        
		ccsConfig.setCcsHost(ccsHost);
		ccsConfig.setDefaultAttachPath(basePath);

		ccs = new CCS();
		ccs.init(ccsConfig);
		try {
			value = ccs.getConf(key);
			if (!FCUtil.isEmpty(value)) {
				localCache.put(basePath + key, value);
				return value;
			} else {
				localCache.put(basePath + key, NULL_VALUE_REPRESENTATION);
				return null;
			}
		} catch (CCSException e) {
			logger.error("Caught Exception is ", e);
		} catch (Exception e) {
			logger.error("Caught Exception is ", e);
		}

		return null;
    }
    
    public static boolean containsCaseInsensitive(String s, String[] list) {
        if (list != null && list.length > 0) {
            for (String string : list) {
                if (string.trim().equalsIgnoreCase(s)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean containsPartialMatchCaseInsensitive(String s, String[] list) {
        try {
            if (list != null && list.length > 0) {
                for (String string : list) {
                    if (string.trim().toLowerCase().contains(s.trim().toLowerCase())
                            || s.trim().toLowerCase().contains(string.trim().toLowerCase())) {
                        return true;
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Error finding partial match.", ex);
        }
        return false;
    }
    
    public static JSONObject getJsonObject(String value){
        try {
            if (StringUtils.isNotBlank(value)) {
                JSONParser parser = new JSONParser();
                return  (JSONObject) parser.parse(value);
            }
        } catch (Exception e){
            logger.error("Exception on parsing string " + value, e);
        }

        return null;
    }
    
    
	/**
	 * This method takes in a map and converts it into a string. The method
	 * takes as argument a map and an exclusion list which contains the keys we want
	 * to exclude from the map in the final string.
	 * 
	 * @param responseMap
	 * @param exclusionList
	 * @return String 
	 * 				- the string formed out of hashmap key value pair
	 */
    public static String hashMapToString(Map<String, String> responseMap, List<String> exclusionList) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : responseMap.entrySet()) {
			if (!exclusionList.contains(entry.getKey())) {
				sb.append(entry.getKey() + "=" + entry.getValue() + "&");
			}
		}
		String returnString = sb.toString();
		if (!StringUtils.isEmpty(returnString)) {
			returnString = returnString.substring(0, returnString.length() - 1);
		}
		return returnString;
	}
    
    
	/**
	 * Used to check if the time to be checked lies between the given set of to
	 * and from times
	 * 
	 * @param from
	 * @param to
	 * @param toBeChecked
	 * @return boolean 
	 * 				- the boolean value
	 */
	public static boolean doesTimeLieInBetween(final Date from, final Date to, final Date toBeChecked) {
		return toBeChecked.after(from) && toBeChecked.before(to);
	}
	
	/**
	 * Used to get a list from a comma separated string value
	 * @param operatorString
	 * @return List
	 * 			- list of comma separated values from the string
	 */
	public static List<String> getListFromCommaSeparatedString(String operatorString){
		List<String> operatorsList = new ArrayList<String>();
		String [] opArray = operatorString.split(",");
		
		for(String operator : opArray){
			operatorsList.add(operator.trim());
		}
		return operatorsList;
	}
	
	/**
	 * Used to get a map from a JSON String
	 * @param configString
	 * @return Map
	 * 			- the map from the JSON string
	 */
	@SuppressWarnings("serial")
	public static Map<String, Object> getMapFromString(String configString){
		Map<String, Object> map = new Gson().fromJson(configString, new TypeToken<HashMap<String, String>>() {
		}.getType());
		return map;
	}
	
	 /**
	  * Used to return the complimentary plan type for a given plan
	 * @param rechargePlanType
	 * @return
	 */
	public static String getComplimentaryPlanType(String rechargePlanType) {
	        if (!FCUtil.isEmpty(rechargePlanType)) {
	            if (rechargePlanType.equals("topup")) {
	                return "special";
	            } else {
	                return "topup";
	            }
	        }
	        return "special";
	}
	 /**
	  * Used to convert string value to a hashmap with lower case keys
	 * @param responseString
	 * @return hashmap 
	 * 				- the hashmap value
	 */
	public static Map<String, String> stringToHashMapInLowerCaseKeys(String responseString) {
	        String[] splitArray = responseString.split("&");
	        Map<String, String> map = new HashMap<String, String>();
	        if (splitArray.length > 1) {
	            for (int i = 0; i < splitArray.length; i++) {
	                String[] splitEqual = splitArray[i].split("=");
	                String part0 = splitEqual[0].trim().toLowerCase();
	                String part1 = "";

	                if (splitEqual.length == 2) {
	                    part1 = splitEqual[1];
	                }

	                map.put(part0, part1);
	            }
	        }
	        return map;
	    }

	public static CommonLogDetails createLoggingObject(String database, String table, String type,
			Map<String, Object> data) {
		long epochTimeStamp = System.currentTimeMillis() / 1000;
		CommonLogDetails commonLogDetails = new CommonLogDetails();		
		commonLogDetails.setDatabase(database);
		commonLogDetails.setTable(table);
		commonLogDetails.setType(type);
		commonLogDetails.setTimestamp(epochTimeStamp);
		commonLogDetails.setData(data);
		return commonLogDetails;
	}
	
	/**
	 * This method is used to find the channel id value from incoming request
	 * either from request header or request parameter
	 * 
	 * @param request
	 * @return int - the chhanel id value
	 */
	public static int getChannelFromRequest(HttpServletRequest request) {
		int channelId = 1;
		try {
			if (request.getParameter(MobileConstants.COOKIE_NAME_CHANNEL_ID) != null) {
				channelId = Integer.valueOf(request.getParameter(MobileConstants.COOKIE_NAME_CHANNEL_ID));
			} else if (request.getHeader(MobileConstants.COOKIE_NAME_CHANNEL_ID) != null) {
				channelId = Integer.valueOf(request.getHeader(MobileConstants.COOKIE_NAME_CHANNEL_ID));
			}
		} catch (Exception e) {
			logger.error("An exception occurred trying to find the channel id from request", e);
		}
		return channelId;
	}
	
	/**
	 * Used to get the value corresponding to the key from a JSON string 
	 * 
	 * @param configString
	 * @param keyName
	 * @return Object 
	 * 			- the return object
	 */
	public static Object getCustomPropertyFromAppConfig(String configString, String keyName){
		Object returnValue=null;
		try {
			Map<String, Object> configMap = getMapFromString(configString);
			returnValue = (Object) configMap.get(keyName);
		} catch (Exception e) {
			logger.error("An error occured trying to get the timeout details from App config", e);
		}
		return returnValue;
	}
	
    public static CardDetailsDo searchCardInResponse(String fingerprint, List<CardDetailsDo> debitATMStatusList) {
        if(fingerprint.isEmpty())
            return null;
        for (CardDetailsDo each : debitATMStatusList) {
            logger.info(each.getCardFingerprint());
            if (fingerprint.equalsIgnoreCase(each.getCardFingerprint()))
                return each;
        }
        return null;
    }
}
