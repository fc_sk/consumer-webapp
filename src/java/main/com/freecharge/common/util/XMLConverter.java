package com.freecharge.common.util;

/**
 * Created by IntelliJ IDEA.
 * User: Manoj Kumar
 * Date: May 11, 2012
 * Time: 10:38:46 AM
 * To change this template use File | Settings | File Templates.
 */
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

public class XMLConverter {

	private Marshaller marshaller;
	private Unmarshaller unmarshaller;

	public Marshaller getMarshaller() {
		return marshaller;
	}

	public void setMarshaller(Marshaller marshaller) {
		this.marshaller = marshaller;
	}

	public Unmarshaller getUnmarshaller() {
		return unmarshaller;
	}

	public void setUnmarshaller(Unmarshaller unmarshaller) {
		this.unmarshaller = unmarshaller;
	}

	public void convertFromObjectToXML(Object object, String filepath)
		throws IOException {

		FileOutputStream os = null;
		try {
			os = new FileOutputStream(filepath);
			getMarshaller().marshal(object, new StreamResult(os));
		} finally {
			if (os != null) {
				os.close();
			}
		}
	}
	
	public String convertFromObjectToXML(Object object)
			throws IOException {

			StringWriter sw = null;
			try {
				sw = new StringWriter();
				getMarshaller().marshal(object, new StreamResult(sw));
			} finally {
				if (sw != null) {
					sw.close();
				}
			}
			return sw.toString();
		}

	public Object convertFromXMLToObject(String xmlfile) throws IOException {

		InputStream is = null;
		try {

			is = this.getClass().getClassLoader().getResourceAsStream(xmlfile);
			return getUnmarshaller().unmarshal(new StreamSource(is));
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}
   public Object convertFromXMLStringToObject(String xmlString) throws IOException {

		InputStream is = null;
		try {
			//is = new FileInputStream(xmlString);
            is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));
			return getUnmarshaller().unmarshal(new StreamSource(is));
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}
}
