package com.freecharge.common.util;

import java.io.Serializable;

public class InTransactionsForStatistics implements Serializable{
	private Double amount;
	private String productType;
	private String serviceNumber;
	private String txnStatus;
	private String inResponseCode;
	 
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getServiceNumber() {
		return serviceNumber;
	}
	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}
	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}
	public String getTxnStatus() {
		return txnStatus;
	}
	public void setInResponseCode(String inResponseCode) {
		this.inResponseCode = inResponseCode;
	}
	public String getInResponseCode() {
		return inResponseCode;
	} 
}
