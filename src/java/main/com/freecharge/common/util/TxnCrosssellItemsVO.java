package com.freecharge.common.util;

public class TxnCrosssellItemsVO {
	private Integer id;
    private Integer txnHomePageId;
    private Integer couponId;
    private Integer corssSellId;
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCouponId() {
		return couponId;
	}
	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}
	public Integer getCorssSellId() {
		return corssSellId;
	}
	public void setCorssSellId(Integer corssSellId) {
		this.corssSellId = corssSellId;
	}
	public Integer getId() {
		return id;
	}
	public void setTxnHomePageId(Integer txnHomePageId) {
		this.txnHomePageId = txnHomePageId;
	}
	public Integer getTxnHomePageId() {
		return txnHomePageId;
	}
}
