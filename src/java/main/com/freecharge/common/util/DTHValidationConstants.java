package com.freecharge.common.util;

public class DTHValidationConstants {
	public static final String INVALID_DTH_NUMBER_RESPONSE_MESSAGE = "DTH number entered is incorrect. Please enter valid details.";
	public static final String VALID_DTH_NUMBER_RESPONSE_MESSAGE = "Valid DTH number";
	public static final String NO_ERROR = "00";
	public static final String INVALID_DTH_NUMBER_RESPONSE_CODE = "IDN";
	public static final String DTH_INVALID_NUMBER_ERROR_CODE = "20051";
}
