package com.freecharge.common.util;

import java.security.NoSuchAlgorithmException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.freecharge.common.util.AuthorizationRequestHeaders;
import com.freecharge.common.util.ChecksumCalculator;
import com.freecharge.common.util.JsonParser;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.AuthorizationParams;

public class RequestFormatter {
	
	private final static Logger logger = LoggingFactory.getLogger(RequestFormatter.class);
	public static Map<String, String> setApplicationInformationInHeader(Map<String, String> headers, Object clientRequest, AuthorizationParams authorizationParams)
	{
		
		String checkSum = null;
		String text = JsonParser.objectToString(clientRequest);
		logger.info("Text from clientRequeste for calculating checksum : " + text);
		try
		{
			checkSum = ChecksumCalculator.calculateCheckSum(authorizationParams.getKey(), text);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
		headers.put(AuthorizationRequestHeaders.CLIENT_NAME.toString(), authorizationParams
				.getClientName());
		headers.put(AuthorizationRequestHeaders.CHECKSUM.toString(), checkSum);
		return headers;
	}
}
