package com.freecharge.common.util;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.service.AuditTrailService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;

/**
 * Created by IntelliJ IDEA.
 * User: Toshiba
 * Date: Apr 23, 2012
 * Time: 1:09:07 PM
 * To change this template use File | Settings | File Templates.
 */
@Component("auditTrailManger")
public class AuditTrailFilterManager {

    private static final Logger logger = LoggingFactory.getLogger(AuditTrailFilterManager.class);
    private AuditTrailService auditTrailService;

    @Autowired
    public void setAuditTrailService
            (AuditTrailService
                    auditTrailService) {
        this.auditTrailService = auditTrailService;
    }

    public void auditTrailFilterManager(HttpServletRequest httpServletRequest, String uri) {
        logger.info("Request URI is " + uri);
       
        String methodName = auditTrailService.doAuditTrailService(uri);
         if (methodName != null && !methodName.equalsIgnoreCase("")) {
            FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
            Class[] parametertype = new Class[]{String.class, HttpServletRequest.class};
            Object[] arguments = new Object[]{fs.getUuid(), httpServletRequest};
            String result = null;
            try {
                Method concatMethod = auditTrailService.getClass().getMethod(methodName, parametertype);
                result = (String) concatMethod.invoke(auditTrailService, fs.getUuid(), httpServletRequest);
                //System.out.println(" Concatenated String is =>" + result);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("exception raised in the auditTrailFilterManager ... details are  " + e);
               
            }
        }

    }

}
