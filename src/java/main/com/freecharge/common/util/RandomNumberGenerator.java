package com.freecharge.common.util;

import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: abc
 * Date: May 14, 2012
 * Time: 12:22:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class RandomNumberGenerator {
    static Random rnd = new Random();
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static String getRandomValue( int len ){
           StringBuilder sb = new StringBuilder( len );
           for( int i = 0; i < len; i++ )
              sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
           return sb.toString();
     }
    
}
