package com.freecharge.common.util;

import java.io.Serializable;
import java.math.BigDecimal;

import org.junit.Test;

import com.google.common.base.Preconditions;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 5/10/12
 * Time: 4:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class Amount implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private BigDecimal amount;

    public static final Amount ZERO = new Amount(BigDecimal.ZERO);

    public Amount(BigDecimal amount) {
        //this.amount = amount;
        this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public Amount(String val) {
        this.amount = new BigDecimal(val);
        this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public Amount(double val) {
        this(new Double(val).toString());
    }

    public Amount(float val) {
        this(new Float(val).toString());
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void negativeCheck() {
        Preconditions.checkState(this.amount.compareTo(BigDecimal.ZERO) >= 0, "Amount must be positive.");
    }

    public int compareTo(Amount amount) {
        return this.amount.compareTo(amount.getAmount());
    }
    
    @Override
    public String toString() {
        return getAmount().toString();
    }

    public static void main(String[] args) {
        Amount amount = new Amount(10.9999d);
        System.out.println(amount.getAmount());
    }

    public static class AmountTest {
        @Test(expected = IllegalStateException.class)
        public void negativeCheck() {
            Amount amount = new Amount("-1");
            amount.negativeCheck();
        }

        @Test
        public void zeroCheck() {
            Amount amount = new Amount("0");
            amount.negativeCheck();
        }

        @Test
        public void positiveCheck() {
            Amount amount = new Amount("1");
            amount.negativeCheck();
        }

        @Test
        public void doubleAmountCheck() {
            Amount amount = new Amount(10.999999d);
            org.junit.Assert.assertEquals(amount.getAmount(), new BigDecimal("10.99"));
        }

        @Test
        public void floatAmountCheck() {
            Amount amount = new Amount(10.999999f);
            org.junit.Assert.assertEquals(amount.getAmount(), new BigDecimal("10.99"));
        }

        @Test
        public void compareToCheck() {
            Amount amount = new Amount(5);
            org.junit.Assert.assertEquals(amount.compareTo(new Amount(4)), 1);
            org.junit.Assert.assertEquals(amount.compareTo(new Amount(5)), 0);
            org.junit.Assert.assertEquals(amount.compareTo(new Amount(6)), -1);
        }
    }
}
