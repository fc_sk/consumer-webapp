package com.freecharge.common.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.common.cache.RedisCacheManager;

/**
 */
@Component
public class TimeBarrierCacheManager extends RedisCacheManager {
    @Autowired
    private RedisTemplate<String, Object> timeBarrierCacheTemplate;

    @Override
    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.timeBarrierCacheTemplate;
    }
}
