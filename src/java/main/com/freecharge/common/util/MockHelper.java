package com.freecharge.common.util;

import java.util.Map;

public class MockHelper {

    public static String getResponseCodeFromAddressFields(Map<String, Object> umap, String rcSetterStr) {
		String deliveryAdd = (String) umap.get("deliveryAdd");
		String responseCode = getResponseCodeFromStr(deliveryAdd, rcSetterStr);
		if (responseCode == null)
		    responseCode = getResponseCodeFromStr((String) umap.get("deliveryArea"), rcSetterStr);
		if (responseCode == null)
		    responseCode = getResponseCodeFromStr((String) umap.get("deliveryLandmark"), rcSetterStr);
		
		return responseCode;	
    }
    
    public static String getResponseCodeFromStr(String str, String rcSetterStr) {
		if (str == null)
		    return null;
	
		int index = str.indexOf(rcSetterStr);
		if (index == -1)
		    return null;
	
		int rcIndex = index + rcSetterStr.length();
		int lastIndex = str.length() - 1;
		int rcEndIndex = Math.min(rcIndex+3, lastIndex+1);
		return str.substring(rcIndex , rcEndIndex);	    
    }

}
