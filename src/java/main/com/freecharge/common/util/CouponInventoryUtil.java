package com.freecharge.common.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.freecharge.app.domain.entity.jdbc.OrderId;
import com.freecharge.common.businessdo.VouchersQuantityBusinessDO;
import com.freecharge.web.webdo.CouponInventoryWebDO;
import com.freecharge.web.webdo.ReissueCouponInventoryWebDO;

public class CouponInventoryUtil {

	public static void preProcessInventoryDetails(CouponInventoryWebDO couponInventoryWebDO) {
		List<String> vouchersQuantityBusinessDOList = FCStringUtils.getStringList(couponInventoryWebDO.getCouponInventoryDetails(), ",");
	
		List<VouchersQuantityBusinessDO> couponList = new ArrayList<VouchersQuantityBusinessDO>();
		
		for ( String voucherQtyBusinessString : vouchersQuantityBusinessDOList) {
			List<String> voucherQtyDetails = FCStringUtils.getStringList(voucherQtyBusinessString, "-");
			VouchersQuantityBusinessDO voucherQuantityBusinessDO = new VouchersQuantityBusinessDO();
			
			voucherQuantityBusinessDO.setCouponStoreId(Integer.parseInt(voucherQtyDetails.get(0)));
			voucherQuantityBusinessDO.setQuantity(Integer.parseInt(voucherQtyDetails.get(1)));
			voucherQuantityBusinessDO.setComplimentary(Boolean.parseBoolean(voucherQtyDetails.get(2)));
	
			couponList.add(voucherQuantityBusinessDO);
		}
		
		couponInventoryWebDO.setCouponInventoryList(couponList);
		
	}
	
	public static void preProcessReissueInventoryDetails(ReissueCouponInventoryWebDO reissueCouponInventoryWebDO) {
		List<String> vouchersQuantityBusinessDOList = FCStringUtils.getStringList(reissueCouponInventoryWebDO.getReissueCouponInventoryDetails(), ",");
	
		List<VouchersQuantityBusinessDO> couponList = new ArrayList<VouchersQuantityBusinessDO>();
		
		for ( String voucherQtyBusinessString : vouchersQuantityBusinessDOList) {
			List<String> voucherQtyDetails = FCStringUtils.getStringList(voucherQtyBusinessString, "-");
			VouchersQuantityBusinessDO voucherQuantityBusinessDO = new VouchersQuantityBusinessDO();
			
			voucherQuantityBusinessDO.setCouponStoreId(Integer.parseInt(voucherQtyDetails.get(0)));
			voucherQuantityBusinessDO.setQuantity(Integer.parseInt(voucherQtyDetails.get(1)));
			voucherQuantityBusinessDO.setComplimentary(Boolean.parseBoolean(voucherQtyDetails.get(2)));
	
			couponList.add(voucherQuantityBusinessDO);
		}
		
		reissueCouponInventoryWebDO.setReissueCouponInventoryList(couponList);
		
		vouchersQuantityBusinessDOList = FCStringUtils.getStringList(reissueCouponInventoryWebDO.getDeletedCouponInventoryDetails(), ",");
		
		couponList = new ArrayList<VouchersQuantityBusinessDO>();
		
		for ( String voucherQtyBusinessString : vouchersQuantityBusinessDOList) {
			List<String> voucherQtyDetails = FCStringUtils.getStringList(voucherQtyBusinessString, "-");
			VouchersQuantityBusinessDO voucherQuantityBusinessDO = new VouchersQuantityBusinessDO();
			
			voucherQuantityBusinessDO.setCouponStoreId(Integer.parseInt(voucherQtyDetails.get(0)));
			voucherQuantityBusinessDO.setQuantity(Integer.parseInt(voucherQtyDetails.get(1)));
			voucherQuantityBusinessDO.setComplimentary(Boolean.parseBoolean(voucherQtyDetails.get(2)));
	
			couponList.add(voucherQuantityBusinessDO);
		}
		
		reissueCouponInventoryWebDO.setDeletedCouponInventoryList(couponList);
	}
	
	 /**
     * Given an order ID determins whether it was from *before* 
     * generic coupons release. Super ugly but had to be done. 
     * @param orderId
     * @return
     */
    public static boolean isGenericCoupon(OrderId orderId) {
        /*
         * "May 10, 2013 7:33:35 PM" was when generic coupon
         * *feature* went live.
         */
        Timestamp releaseTs = Timestamp.valueOf("2013-05-10 19:33:35");
        return orderId.getCreatedOn().before(releaseTs);
    }
    
    public static Boolean couponTypeExists( List<Map<String, Object>> couponList, String couponType) {
		 if (couponList != null && couponList.size() > 0) {
            for (int i = 0; i < couponList.size(); i++) {
  
                if (couponList.get(i).get("couponType") != null ) {
                  	String couponTypeStr = (String)couponList.get(i).get("couponType");
                  	if (couponTypeStr.compareToIgnoreCase(couponType) == 0)
                  		return true;
                }
                	
            }
        }
		return false;
	}
    
    public static boolean isMultiMerchantCouponSelected(
			List<Map<String, Object>> couponList,List<String> couponTypeList) {
    	
    	
    	boolean isEcoupon = false;
		  for(int i=0;i<couponList.size();i++) {
			  
		  	if(couponList.get(i) != null && (couponTypeList.contains(couponList.get(i).get("couponType").toString())))
			  {
				  if(isEcoupon)
				  {
					  return true;
				  }
				  isEcoupon = true;
				  
			  }
		 }
		
		return false;
	}
    
	}
