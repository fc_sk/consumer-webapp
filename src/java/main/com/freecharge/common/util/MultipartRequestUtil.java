package com.freecharge.common.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;

public class MultipartRequestUtil {
	public Map<String, List<String>> fRegularParams = new LinkedHashMap<String, List<String>>();

	public Map<String, FileItem> fFileParams = new LinkedHashMap<String, FileItem>();

	public void convertToMaps(List<FileItem> aFileItems) {
		for (FileItem item : aFileItems) {
			if (isFileUploadField(item)) {
				fFileParams.put(item.getFieldName(), item);
			}
			else {
				if (alreadyHasValue(item)) {
					addMultivaluedItem(item);
				}
				else {
					addSingleValueItem(item);
				}
			}
		}
	}

	public boolean isFileUploadField(FileItem aFileItem) {
		return !aFileItem.isFormField();
	}

	public boolean alreadyHasValue(FileItem aItem) {
		return fRegularParams.get(aItem.getFieldName()) != null;
	}

	public void addSingleValueItem(FileItem aItem) {
		List<String> list = new ArrayList<String>();
		list.add(aItem.getString());
		fRegularParams.put(aItem.getFieldName(), list);
	}

	public void addMultivaluedItem(FileItem aItem) {
		List<String> values = fRegularParams.get(aItem.getFieldName());
		values.add(aItem.getString());
	}
}
