package com.freecharge.common.util;

public enum AuthorizationRequestHeaders {

	HOST_IP_ADDRESS("hostIpAddress"),  CLIENT_NAME("ClientName"),  REQUEST_ID("RequestId"), 
	CHECKSUM("CheckSum"),  OPERATION_PERFORMED("OperationPerformed"), 
	DOMAIN("Domain"),  CLIENT_TIMESTAMP("clientTimestamp"), 
	AUTHORIZATION_MODE("authorizationMode"),  KEY_VERSION_NUMBER("keyVersionNumber"), 
	IS_AUTHORIZED("isAuthorized"),  IS_AUTHENTICATED("isAuthenticated");
	/*    */   
	private String description;
	/*    */   
	private AuthorizationRequestHeaders(String description)
	{
		this.description = description;
	}

	public String toString() {
		return description;
	}
}
