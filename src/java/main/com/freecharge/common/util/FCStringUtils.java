package com.freecharge.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.common.base.Joiner;
import com.mongodb.DBObject;
import com.freecharge.common.framework.logging.LoggingFactory;

public class FCStringUtils {

    public static boolean isBlank(String str)
    {
        int strLen;
        if(str == null || (strLen = str.length()) == 0)
            return true;
        for(int i = 0; i < strLen; i++)
            if(!Character.isWhitespace(str.charAt(i)))
                return false;

        return true;
    }
    
    public static boolean isNotBlank(String str)
    {
        return !isBlank(str);
    }

    public static List<String> getStringList(String propertyValue, String delimiter) {

    	StringTokenizer stringtokens = new StringTokenizer(propertyValue, delimiter);
    	List<String> stringList = new ArrayList<String>();

    	while(stringtokens.hasMoreTokens()) {
    		stringList.add(stringtokens.nextToken());
    	}
    	return stringList;
    }

    public static String join(List<String> terms, String delimiter){
        if (terms==null || terms.size()<1){
            return null;
        }
        String res = "";
        for (String t : terms){
            res+=delimiter+t;
        }
        return res.substring(delimiter.length());
    }

    public static JSONObject jsonToMap(String jsonString){
        JSONParser parser = new JSONParser();
        try {
            return (JSONObject) parser.parse(jsonString);
        }catch (ParseException pe) {
            logger.error(String.format("Parsing Error - json string: %s", jsonString), pe);
            return null;
        }
    }

    public static Long tryParseLong(String str){
        if (isBlank(str)){
            return null;
        }
        try {
            return Long.parseLong(str);
        }catch (NumberFormatException ne){
            return null;
        }
    }

    /*Works only for a (String,String) map*/
    public static String mapToJson(Map map) {
        try {
            return new JSONObject(map).toJSONString();
        } catch (Exception e){
            logger.info("Couldnt parse map " + map);
            return null;
        }
    }

    public static String capitalizeWords(String input){
        String inputWords[] = input.split(" ");
        String result = "";
        for(String word: inputWords)
        {
            char first = word.charAt(0);
            result += Character.toUpperCase(first);
            result += word.substring(1,word.length());
            result += " ";
        }
        result = result.substring(0,result.length()-1);
        return result;
    }

    public static String join(String[] mappingKeys, String delimiter) {
        if(!FCUtil.isEmpty(mappingKeys)) {
            return Joiner.on(delimiter).skipNulls().join(mappingKeys);
        }
        return null;
    }

    public static boolean hasSpecialChar(String str) {
        Pattern p = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = p.matcher(str);
        return m.find();
    }
    
    //checking for duplicate in a DBObject List
    public boolean checkForDuplicateInDBObjectList(String string,List<DBObject> list,String keyName){
		for(DBObject str:list)
		{
			if(str!=null&&str.get(keyName)!=null){
				if(string.equalsIgnoreCase(String.valueOf(str.get(keyName))))
				{
					return true;
				}
			}
		}
		return(false);
	}

    private static final Logger logger = LoggingFactory.getLogger(FCStringUtils.class);
}
