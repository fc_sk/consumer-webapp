package com.freecharge.common.util;

import com.freecharge.common.framework.context.WebContext;


public class WebContextFactory {



    public static WebContext createWebContext(String delegateName)
    {
       return (WebContext)SpringBeanLocator.getInstance().getSpringBean(delegateName);
    }

}
