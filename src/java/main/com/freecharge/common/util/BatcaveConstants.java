package com.freecharge.common.util;

public class BatcaveConstants {

	// VCRecon //
	public static final String BATCAVE_HOST_URI = "batcave.host.uri";
	public static final String VCRECON_BY_TRANSCTION_REF_NO_URI = "vcrecon.by.transaction.ref.no.uri";
	public static final String VCRECON_BY_CARD_HASH_ID_URI = "vcrecon.by.card.hash.id.uri";
	public static final String VCRECON_BY_ARN_URI = "vcrecon.by.arn.uri";

	public static final String SUCCESS = "SUCCESS";
	public static final String FAILURE = "FAILURE";

	public static final String FROM_DATE = "from";
	public static final String TO_DATE = "to";
	
	
	public static final String VC_TRANSACTIONS = "VCTransactons";
	public static final String VC_CARDHASH_ID = "vcardId";
	public static final String VC_REFUND_ARN = "vcardRefundARN";
	

}
