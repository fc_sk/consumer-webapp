package com.freecharge.common.util;

import java.util.Date;

import com.freecharge.web.webdo.FeeEngineException;

public class ServiceResponse<T> {
	
	private T response;
	private FeeEngineException exception;
	private Date serverTimeStamp;
	
	public T getResponse() {
		return response;
	}
	public void setResponse(T response) {
		this.response = response;
	}
	public FeeEngineException getException() {
		return exception;
	}
	public void setException(FeeEngineException exception) {
		this.exception = exception;
	}
	public Date getServerTimeStamp() {
		return serverTimeStamp;
	}
	public void setServerTimeStamp(Date serverTimeStamp) {
		this.serverTimeStamp = serverTimeStamp;
	}
	

}
