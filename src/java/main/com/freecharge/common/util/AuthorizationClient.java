package com.freecharge.common.util;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.freecharge.common.util.RequestFormatter;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.AuthorizationParams;

@Component
public class AuthorizationClient {
	private Logger logger = LoggingFactory.getLogger(getClass());

	public Map<String, String> UpdateRequest(Map<String, String> headers, Object clientRequest, AuthorizationParams authorizationParams){
		validateRequest(authorizationParams);
		return RequestFormatter.setApplicationInformationInHeader(headers, clientRequest, authorizationParams);
	}

	private void validateRequest(AuthorizationParams authorizationParams) {
		if ((authorizationParams == null) || 
				(authorizationParams.getClientName() == null) || 
				(authorizationParams.getKey() == null)) {
			logger.error("Error caught");
		}
	}

}
