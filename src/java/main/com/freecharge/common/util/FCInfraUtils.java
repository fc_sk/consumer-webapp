package com.freecharge.common.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class FCInfraUtils {

    public static String getRemoteAddr(HttpServletRequest request) {
        String remoteIp = request.getHeader(FCConstants.XFF_REQUEST_HEADER);
        if (remoteIp == null) {
            remoteIp = request.getRemoteAddr();
        }
        return remoteIp;
    }

    public static String getLocalIP() {
        String ipaddress = "";

        try {
            InetAddress inetAddress = InetAddress.getLocalHost();

            // Get IP Address
            byte[] bytes = inetAddress.getAddress();

            for (int i = 0; i < bytes.length; i++) {
                if (i > 0) {
                    ipaddress += ".";
                }
                ipaddress += bytes[i] & 0xFF;
            }

        } catch (UnknownHostException uhe) {
            ipaddress = "";
        }

        return ipaddress;
    }

    public static String getSecureCookie(HttpServletRequest request) {
        String secureCookie = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("vsc".equals(cookie.getName())) {
                    secureCookie = cookie.getValue().trim();
                    break;
                }
            }
        }
        return secureCookie;
    }
}
