package com.freecharge.common.util;

import java.math.BigDecimal;

import com.freecharge.common.framework.basedo.AbstractDO;

public class SuccessDenominationType extends AbstractDO{

    private BigDecimal successAmount;
    private Boolean    isSpecial;

    public BigDecimal getAmount() {
        return this.successAmount;
    }

    public void setAmount(final BigDecimal successAmount) {
        this.successAmount = successAmount;
    }

    public void setSpecial(final Boolean isSpecial) {
        this.isSpecial = isSpecial;
    }

    public Boolean isSpecial() {
        return this.isSpecial;
    }

    public String toString() {
        return "{ID is: " + successAmount + "; isSpecial: " + isSpecial + "}";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + successAmount.intValue();
        result = prime * result + ((isSpecial == null) ? 0 : isSpecial.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SuccessDenominationType other = (SuccessDenominationType) obj;
        if (!successAmount.equals(other.getAmount())) {
            return false;
        }
        if (!isSpecial.equals(other.isSpecial())) {
            return false;
        }
        return true;
    }
}
