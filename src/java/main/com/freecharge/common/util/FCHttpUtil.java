package com.freecharge.common.util;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

@Service
public class FCHttpUtil {
    public void doNotCache(HttpServletResponse response) {
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0, private");
        response.addHeader("Pragma", "no-cache");
        response.addHeader("Expires", "Sat, 26 Jul 1997 05:00:00 GMT");
    }

    public void infiniteCache(HttpServletResponse response) {
        response.setHeader("Cache-Control", "max-age=315360000000, public");
        response.addHeader("Expires", "Wed, 31 Jul 2030 00:00:00 GMT");
    }
}
