package com.freecharge.common.encryption.mdfive;

/**
 * Created by IntelliJ IDEA.
 * User: Praveen
 * Date: 9 Mar, 2010
 * Time: 12:11:42 PM
 * To change this template use File | Settings | File Templates.
 */
import java.text.ParseException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class EPinEncrypt {
    public static boolean mbanking = false;
    public static boolean microbanking = false;
    public static boolean remittance = false;

    /**
     * Turns array of bytes into string
     *
     * @param buf Array of bytes to convert to hex string
     * @return Generated hex string
     */
    public static String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10)
                strbuf.append("0");

            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }

      public String getEncryptedPin(String pin) throws Exception {

        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(256); // 192 and 256 bits may not be available


        // Generate the secret key specs.
        SecretKey skey = buildKey();  //kgen.generateKey();
        //System.out.println(skey);
        byte[] raw = skey.getEncoded();
        //System.out.println(raw);
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");


        // Instantiate the cipher

        Cipher cipher = Cipher.getInstance("AES");

        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

        byte[] encrypted =
                cipher.doFinal(pin.getBytes());
        //System.out.println("encrypted string: " + asHex(encrypted));
        String encHex = asHex(encrypted);
        return encHex.toUpperCase(); 
    }

    private static SecretKey buildKey()
            throws Exception {
        String masterKey = "1234567890123456";
        SecretKeySpec keyfactory = new SecretKeySpec(masterKey.getBytes(), "AES");
        return keyfactory;
    }

    public static boolean isMbanking() {
        return mbanking;
    }

    public static boolean isMicrobanking() {
        return microbanking;
    }

    public static boolean isRemittance() {
        return remittance;
    }


    public static String getDecryptedPin(String encryptedHex) throws Exception, ParseException {

        byte[] pinkey = hexToBytes(encryptedHex);
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128); // 192 and 256 bits may not be available


        // Generate the secret key specs.
        SecretKey skey = buildKey();  //kgen.generateKey();
        //System.out.println(skey);
        byte[] raw = skey.getEncoded();
        //System.out.println(raw);
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");


        // Instantiate the cipher

        Cipher cipher = Cipher.getInstance("AES");

        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] original =
                cipher.doFinal(pinkey);
        String originalString = new String(original);
        return originalString;

    }

    public static byte[] hexToBytes(String str) {
        if (str == null) {
            return null;
        } else if (str.length() < 2) {
            return null;
        } else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];
            for (int i = 0; i < len; i++) {
                buffer[i] = (byte) Integer.parseInt(
                        str.substring(i * 2, i * 2 + 2), 16);
            }
            return buffer;
        }

    }

}

