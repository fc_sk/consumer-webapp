package com.freecharge.common.encryption.mdfive;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.digest.DigestUtils;

public class MD5 {
    private static String SALT = "i am too salty for your salt";

    public static String hash(String data) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            return DigestUtils.md5Hex(messageDigest.digest(getSaltedData(data).getBytes()));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private static String getSaltedData(String data) {
        return SALT + data;
    }

    public static void main(String[] args) {
        String data = "i am being tested here";
        System.out.println(new String(MD5.hash(data)));
    }

    public static class Test {
        @org.junit.Test
        public void test() {
            String data = "i am being tested here";
            org.junit.Assert.assertEquals(MD5.hash(data), MD5.hash(data));
        }
    }
    
    public static String hashWithoutSalt(String data) {
		try {
			byte[] bytesOfMessage = data.getBytes();
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(bytesOfMessage);

			byte byteData[] = md.digest();

			// convert the byte to hex format method 1
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
						.substring(1));
			}

			// convert the byte to hex format method 2
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				String hex = Integer.toHexString(0xff & byteData[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}
			return hexString.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
    }
}
