package com.freecharge.common.framework.basedo;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractDO {
    
    private final List<String> sensitiveKeys = Arrays.asList("cardNumber", "cardExpiryMonth", "cardExpiryYear", "nameOnCard");

	protected String str(Object val) {
		if (val == null) return "NULL";
		if (val instanceof AbstractDO) return val.toString();
		return "(" + val.getClass() + ") " + val.toString();
	}

	protected Map<String, String> getFields() throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = getClass().getDeclaredFields();
		Map<String, String> data = new LinkedHashMap<String, String>(fields.length);
		for (Field each: fields) {
			each.setAccessible(true);
			Object val = each.get(this);
			data.put(each.getName(), str(val));
		}
		return data;
	}

	public Map<String, Object> toMap() {
		Map<String, String> fields;
		Map<String, Object> result = new LinkedHashMap<String, Object>();
		try {
			fields = getFields();
			for (String each: fields.keySet()) {
				result.put(each, fields.get(each));
			}
			return result;
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String toString() {
		Map<String, String> map;
		try {
			map = getFields();
			StringBuilder sb = new StringBuilder();
			sb.append(getClass().getName()).append(" {\n");
			for (String each: map.keySet()) {
                sb.append("  ");

                if (sensitiveKeys.contains(each)) {
                    sb.append(each).append('=').append("********");
			    } else {
		             sb.append(each).append('=').append(map.get(each));
			    }
				sb.append('\n');
			}
			sb.append('}');
			return sb.toString();
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int hashCode() {
		return (getClass()+toString()).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj==null || !(getClass().isInstance(obj))) {
			return false;
		}
		return toString().equals(obj.toString());
	}

}
