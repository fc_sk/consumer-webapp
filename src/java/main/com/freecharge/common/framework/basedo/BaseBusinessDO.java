package com.freecharge.common.framework.basedo;

import java.io.Serializable;

public class BaseBusinessDO extends AbstractDO implements Serializable {

    private static final long serialVersionUID = 101L;
    private Long id;
    private String lookupID;

    public String getLookupID() {
        return lookupID;
    }

    public void setLookupID(String lookupID) {
        this.lookupID = lookupID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
