package com.freecharge.common.framework.exception;

public class FCPaymentGatewayException extends FCBaseException{

    public FCPaymentGatewayException() {
    }

    public FCPaymentGatewayException(String message) {
        super(message);
    }

    public FCPaymentGatewayException(String message, Throwable cause) {
        super(message, cause);
    }

    public FCPaymentGatewayException(Throwable cause) {
        super(cause);
    }
}
