package com.freecharge.common.framework.exception;

public class IdentityAuthorizedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public IdentityAuthorizedException(String string) {
        super(string);
    }
}
