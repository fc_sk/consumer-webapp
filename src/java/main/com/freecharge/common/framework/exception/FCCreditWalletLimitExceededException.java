package com.freecharge.common.framework.exception;

public class FCCreditWalletLimitExceededException extends IllegalStateException{
    private static final long serialVersionUID = 1L;

    public FCCreditWalletLimitExceededException(){
        
    }
    public FCCreditWalletLimitExceededException(String message) {
        super(message);
    }
}
