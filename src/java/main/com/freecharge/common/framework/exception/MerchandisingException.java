package com.freecharge.common.framework.exception;

public class MerchandisingException extends FCRuntimeException{


    public MerchandisingException() {
    }

    public MerchandisingException(String message) {
        super(message);
    }

    public MerchandisingException(String message, Throwable cause) {
        super(message, cause);
    }

    public MerchandisingException(Throwable cause) {
        super(cause);
    }

}
