package com.freecharge.common.framework.exception;

public class FCPaymentPortalException extends FCBaseException{

    public FCPaymentPortalException() {
    }

    public FCPaymentPortalException(String message) {
        super(message);
    }

    public FCPaymentPortalException(String message, Throwable cause) {
        super(message, cause);
    }

    public FCPaymentPortalException(Throwable cause) {
        super(cause);
    }
}
