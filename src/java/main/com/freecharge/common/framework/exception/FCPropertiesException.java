package com.freecharge.common.framework.exception;

public class FCPropertiesException extends FCRuntimeException{

    private static final long serialVersionUID = -10234L;

    public FCPropertiesException() {
    }

    public FCPropertiesException(String message) {
        super(message);
    }

    public FCPropertiesException(String message, Throwable cause) {
        super(message, cause);
    }

    public FCPropertiesException(Throwable cause) {
        super(cause);
    }
}
