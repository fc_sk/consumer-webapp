package com.freecharge.common.framework.exception;

public class FCValidationException extends FCRuntimeException {

    // this key can be from resource bussnle as well...
    private String errorMessage;

    public FCValidationException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    public FCValidationException(String message, Throwable cause) {
        super(message, cause);
        this.errorMessage = message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
