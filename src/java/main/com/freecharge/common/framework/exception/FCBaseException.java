package com.freecharge.common.framework.exception;

public class FCBaseException extends Exception{

    private Throwable exception;
    private String errorCode = null;          // this can specify severity of the exception

    public FCBaseException() {
    }

    public FCBaseException(String message) {
        super(message);
    }

    public FCBaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public FCBaseException(Throwable cause) {
        super(cause);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
