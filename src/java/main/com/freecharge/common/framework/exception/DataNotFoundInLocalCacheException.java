package com.freecharge.common.framework.exception;

public class DataNotFoundInLocalCacheException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DataNotFoundInLocalCacheException(String string) {
        super(string);
    }
}
