package com.freecharge.common.framework.exception;

public class FCRuntimeException extends RuntimeException{

    private Throwable exception;
    private String errorCode = null;          // this can specify severity of the exception

    public FCRuntimeException() {
    }

    public FCRuntimeException(String message) {
        super(message);
    }

    public FCRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public FCRuntimeException(Throwable cause) {
        super(cause);
    }

    public Throwable getException() {
        return exception;
    }

    public void setException(Throwable exception) {
        this.exception = exception;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
