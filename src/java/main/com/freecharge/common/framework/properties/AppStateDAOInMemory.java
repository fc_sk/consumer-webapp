package com.freecharge.common.framework.properties;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

@Primary
@Service
public class AppStateDAOInMemory implements IAppStatePersistence {
    private static final Logger logger = LoggingFactory.getLogger(AppStateDAOInMemory.class);

	private static final String APPLICATION_KEY_PREFIX = "app_";
	
    @Autowired
    @Qualifier("appStateRedisDao")
    IAppStatePersistence appStateDBDao;
    
    private volatile Cache<String,Object> localCache;
    
    @Autowired
    MetricsClient metricsClient;
    
    @Value("${inmemory.cache.ttl}")
    private String ttl;

    private final String SERVICE_NAME = "AppStateDAOInMemory";
    
    private final String NULL_VALUE_REPRESENTATION = "cmVkaXMgZGVhZCBiZWVm";
    
    private void initializeCache()
    {
    	if( localCache == null ) {
    		localCache = CacheBuilder.newBuilder().expireAfterWrite(Integer.parseInt(ttl), TimeUnit.MINUTES).build();
    	}
    }

	@Override
	public Object getAppValue(String key) {
        Object appValue = getFromCache(key);
        
        if(appValue==null){
            appValue = appStateDBDao.getAppValue(key);
            if (appValue!=null){
                setInCache(key, appValue);
            } else {
            	setValueAsNullInCache(key);
            }
        } else if (isNullRepresentationValue(appValue)) {
            return null;
        }
        return appValue;
	}

	private void setValueAsNullInCache(String key) {
		setInCache(key, NULL_VALUE_REPRESENTATION);
	}

	private boolean isNullRepresentationValue(Object appValue) {
		return NULL_VALUE_REPRESENTATION.equals(appValue);
	}

	@Override
	public void setAppValue(String key, Object value) {
        this.appStateDBDao.setAppValue(key, value);
        this.setInCache(key, value);
	}

	@Override
	public void deleteAppValue(String key) {
		try {
			appStateDBDao.deleteAppValue(APPLICATION_KEY_PREFIX + key);
			localCache.invalidate(key);
            metricsClient.recordEvent(SERVICE_NAME, "Delete", "Success");
        } catch (Exception e) {
            metricsClient.recordEvent(SERVICE_NAME, "Delete", "Exception");
            logger.error("Exception while deleting appvalue from cache for key: "+key, e);
        }
	}

    @Override
    public void setCustomAppValue(String key, Object value) {
        this.appStateDBDao.setCustomAppValue(key, value);
        this.setInCache(key, value);
    }

    @Override
    public List<Map<String, Object>> getCustomProps() {
        //No caching here
        return this.appStateDBDao.getCustomProps();
    }

    private void setInCache(String key, Object appValue) {
    	initializeCache();
    	localCache.put(key, appValue);
    }

    private Object getFromCache(String key) {
    	initializeCache();
    	return localCache.getIfPresent(key);
    }
    
    public static void main(String[] args) throws InterruptedException {
		AppStateDAOInMemory dao = new AppStateDAOInMemory();
		dao.appStateDBDao = new IAppStatePersistence() {
			
			@Override
			public void setCustomAppValue(String key, Object value) {
				System.out.println("Set Custome app Value was called");
			}
			
			@Override
			public void setAppValue(String key, Object value) {
				System.out.println("Set was called");
			}
			
			@Override
			public List<Map<String, Object>> getCustomProps() {
				return null;
			}
			
			@Override
			public Object getAppValue(String key) {
				System.out.println("GetWasCalled for " + key);
				return key + "VALUE";
			}
			
			@Override
			public void deleteAppValue(String key) {
				System.out.println("Delete was called");
			}
		};
		
		System.out.println("Test1 : Calling get");
		System.out.println(dao.getAppValue("KEY1"));
		System.out.println("Test2 : Calling get twice");
		System.out.println(dao.getAppValue("KEY2"));
		System.out.println(dao.getAppValue("KEY2"));
		System.out.println("Test3 : Checking expiry");
		System.out.println(dao.getAppValue("KEY3"));
//		Thread.sleep(16*1000);
		System.out.println(dao.getAppValue("KEY3"));
		
	}
}
