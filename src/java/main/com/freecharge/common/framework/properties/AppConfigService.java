package com.freecharge.common.framework.properties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freecharge.app.domain.entity.RechargeRetryConfig;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import org.springframework.util.CollectionUtils;

@Service
public class AppConfigService {


    

    private final Logger logger = LoggingFactory.getLogger(getClass());

    public static final String MONGO_SIZE_RECORD_ENABLED     = "mongo.size.record.enabled";

    public static final String ASYNC_RECHARGE     = "async.recharge.enabled";
    public static final String ASYNC_BILL_PAYMENT = "async.bill.payment.enabled";
    
    public static final String BBPS_BILL_PAYMENT_ENABLED = "bbps.bill.payment.enabled";

    public static final String IMS_DIRECT_HIT = "ims.direct.hit.enabled";
    public static final String EMAIL_BANNER_ENABLE = "email.banner.enabled";
    public static final String EURONET_HTTPS_ENABLE = "euronet.https.enabled";
    // Whether to attempt recharge for delayed payment success cases.
    public static final String MISSED_RECHARGE_ATTEMPT = "missed.recharge.attempt";

    public static final String UNDER_MAINTENANCE_HEADER_BANNER_ENABLED = "under.maintenance.header.banner";
    public static final String PAYMENT_UNDER_MAINTENANCE = "payment.under.maintenance";

    public static final String CASH_BACK_CAMPAIGN_ENABLED = "cash.back.campaign.enalbed";
    public static final String ARDEAL_ENABLED = "ardeal.enabled";
    public static final String PRIVILEGED_USER_IDS = "privileged.user.ids";
    public static final String BLACKLISTED_EMAIL_IDS = "blacklisted.email.ids";

    public static final String CLOSED_IMEI_GROUP_KEY = "closed.imei.group";
    public static final String CLOSED_IMEI_GROUP_ENABLED = "closed.imei.group.enabled";
    
    public static final String CITY_NAME_KEY = "city.name";

    public static final String SPECIAL_CASHBACK_OFFER_ENABLED  = "special.cashback.offer.enabled";
    private static final String CONVENIENCE_FEE_ENABLED ="convenience.fee.enabled";
    public static final String SPECIAL_CASHBACK_OFFER_CAMPAIGN_NAME = "special.cashback.offer.campaign.name";
    public static final String SPECIAL_CASHBACK_OFFER_CUG_ENABLED = "special.cashback.offer.cug.enabled";
    public static final String SPECIAL_CASHBACK_OFFER_CUG_EMAIL_IDS = "special.cashback.offer.cug.email.ids";
    public static final String SPECIAL_CASHBACK_OFFER_SEND_FAILED_EMAIL = "special.cashback.offer.send.failed.email";
    public static final String SPECIAL_CASHBACK_OFFER_SEND_FAILED_SMS = "special.cashback.offer.send.failed.sms";
    public static final String SPECIAL_CASHBACK_OFFER_NOTIFICATION_ENABLED = "notification.enabled";
    public static final String SPECIAL_CASHBACK_OFFER_CHECK_REDEEM_COUNT = "special.cashback.offer.redeem.count.check.enabled";
    public static final Object GOSF_OFFER_ENABLED = "gosf.offer.enabled";
    public static final Object GOSF_OFFER_CUG_ENABLED = "gosf.offer.cug.enabled";
    public static final String GOSF_CAMPAIGN_IDS = "gosf.campaign.ids";
    public static final String GOSF_COUPON_IDS = "gosf.coupon.ids";

    public static final String PROMO_DISTRIBUTION_API_CODES_ENABLED = "promoDistributionApiCodesEnabled";

    //GCM NOTIFICATION CONFIG
    public static final String ASYNC_GCM_NOTIFICATION    = "async.gcm.notification.enabled";
    public static final String GCM_NOTIFICATION_CONFIG = "gcm.notification.config";
    public static final String GCM_NOTIFICATION_ENABLED = "enabled";
    public static final String GCM_NOTIFICATION_TIME_OUT_ENABLED = "time.out.enabled";
    public static final String GCM_NOTIFICATION_TIME_OUT_VALUE = "time.out.value";
    public static final String GCM_NOTIFICATION_CONNECTION_TIME_OUT_VALUE = "conn.time.out.value";
    public static final String GCM_NOTIFICATION_SO_TIME_OUT_VALUE = "so.time.out.value";
    public static final String GCM_REFERRAL_NOTIFICATION_ENABLED = "referral.notification.enabled";
    public static final String GCM_PROMOCODE_CASHBACK_NOTIFICATION_ENABLED = "cashback.notification.enabled";

    //IVRS CALL CONFIG
    public static final String IVRS_NOTIFICATION_CONFIG = "ivrs.notification.config";
    public static final String IVRS_CALL_TIME_OUT_ENABLED = "time.out.enabled";
    public static final String IVRS_CALL_TIME_OUT_VALUE = "time.out.value";
    public static final String IVRS_CALL_CONNECTION_TIME_OUT_VALUE = "connection.time.out.value";
    public static final String IVRS_CALL_SO_TIME_OUT_VALUE = "so.time.out.value";

    //EVENT CAPTURE CONFIG
    public static final String GROWTH_EVENT_CONFIG = "growth.event.config";
    public static final String GROWTH_EVENT_CAPTURE_ENABLED = "enabled";
    public static final String GROWTH_EVENT_CUG_EMAILS = "cug.emails";
    public static final String REGISTRATION_EVENT_CAPTURE_ENABLED = "registartion.event.enabled";
    public static final String ASYNC_GROWTH_EVENT = "async.processing.enabled";
    public static final String RECHARGE_SUCCESS_EVENT_CAPTURE_ENABLED = "recharge.success.event.enabled";
    
    // NEW REWARD SERVICE CONFIG
    public static final String NEW_REWARD_SERVICE_CONFIG = "new.reward.service.config";
    public static final String NEW_REWARD_SERVICE_ENABLED = "enabled";
    public static final String NEW_REWARD_SERVICE_EVENT_IDS =  "event.ids";
    public static final String REWARD_CALLER_ID_MAP = "reward.caller.id.map";
    public static final String EVENT_REWARD_ID_MAP = "event.reward.id.map";
    
    // REFERRAL TEMPLATE SETTING
    public static final String REFERRAL_TEMPLATE_CONFIG = "referral.template.config";
    public static final String REFERRAL_TEMPLATE_MIN_APP_VERSION = "min.app.version";
    public static final String REFERRAL_TEMPLATE_DEAL_ID_FOR_OLD_VERSION = "deal.id.old.version";
    public static final String REFERRAL_TEMPLATE_DEAL_ID_FOR_NEW_VERSION = "deal.id.new.version";
     
    // MENU OPTIONS SETTING
    public static final String MENU_OPTIONS_CONFIG = "menu.options.config";
    public static final String MENU_OPTIONS_MODEL = "menu.model";
    
    
    // Banner Config
    public static final String APP_BANNER_CONFIG = "app.banner.config";
    public static final String EMAIL_BANNER_CONFIG = "email.banner.config";
    public static final String MERCHANT_ID_MAPPING = "merchant.id.mapping";
    
    // Withdraw Balance Config
    public static final String WITHDRAW_BALANCE_CONFIG = "withdraw.balance.config";
    public static final String GV_LOAD_MONEY_ALLOWED_CONFIG = "gv.loadMoney.allowed.config";
   
    
  


    //FREEFUND FULFILLMENT CONFIG
    public static final String FREEFUND_FULFILLMENT_CONFIG = "freefund.fulfillment.config";
    public static final String SYNC_FREEFUND_FULFILLMENT_ENABLED = "sync.enabled";

    //BLACKLISTED DOMAIN CONFIG
    public static final String BLACKLISTED_DOMAIN_CONFIG = "blacklisted.domain.config";
    public static final String BLACKLIST_WRONG_SPELLED_DOMAIN = "block.wrong.spelled.domain";
    public static final String BLACKLIST_INCORRECT_DOMAIN = "block.incorrect.domain";

    // APP DOWNLOAD SMS CONFIG PROPERTIES
    public static final String APP_DOWNLOAD_SMS_CONFIG = "app.download.sms.config";
    public static final String MIN_SMS_REPEAT_INTERVAL = "minimum.repeat.interval";

    public static final String VIRTUAL_CARD_BIN_CONFIG = "virtual.card.bin.config";
    public static final String VIRTUAL_CARD_BIN_NOS = "card.bin.nos";

    public static final String WEB_ANALYTICS = "web.site.analytics.enabled";

    public static final String MOBILE_ANALYTICS = "mobile.site.analytics.enabled";

    public static final String FINAL_PAGE_COUPONS = "final.page.coupons.enabled";

    public static final String RECHARGE_RETRY_BLOCK = "recharge.retry.block.enabled";

    public static final String PERMANENT_FAILURE_BLOCK = "recharge.failure.block.enabled";
    public static final String POSTPAID_PERMANENT_FAILURE_BLOCK = "postpaid.failure.block.enabled";
    public static final String LAPU_ENABLE = "lapu.enabled";
    public static final String DENOMINATION_NUMBER_BLOCK = "recharge.denomination.number.block.enabled";
    public static final String EG_NUMBER_BLOCK = "recharge.eg.number.block.enabled";
    public static final String INVALID_DENOMINATION_BLOCK = "recharge.invalid.denomination.block.enabled";
    public static final String OPERATOR_ALERT_BLOCK = "recharge.operator.alert.block.enabled";

    public static final String RECAPTCHA_ENABLED = "recaptcha.enabled";
    public static final String FAILED_LOGIN_RECAPTCHA_ENABLED = "failed.login.recaptcha.enabled";
    public static final String PROMOCODE_ENABLED = "promocode.enabled";
    public static final String PROMOREWARD_ENABLED = "promoreward.enabled";
    public static final String ZEDO_AD_ENABLED = "zedo.ad.enabled";

    public static final String PEPSI_CAMPAIGN_ENABLED = "pepsi.campaign.enabled";
    public static final String DID_CAMPAIGN_ENABLED = "did.campaign.enabled";
    public static final String MCD_CAMPAIGN_ENABLED = "mcd.campaign.enabled";
    public static final String BBPS_ENABLED_BILLERS = "bbps.enabled.billers";
    private static final String ENABLED_BILLDESK_BBPS = "billdesk.bbps.enabled";
    


    public static final String FESTIVE_GIFT_ENABLED = "festive.gift.enabled";
    public static final String GST_PRODUCT_ENABLED = "gst.product.enabled";

    public static final String CARD_STORAGE_ENABLED = "cardstorage.enabled";

    public static final String FREEFUND_APPLY_CHECK_ENABLED = "freefund.apply.check.enabled";

    public static final String FB_FRAUD_CHECK_ENABLED = "freefund.fraud.fbcheck.enabled";

    public static final String FB_FRAUD_CONDITIONS = "freefund.fraud.fbcheck.conditions";

    public static final String IP_CHECK_ENABLED = "freefund.fraud.ipcheck.enabled";

    public static final String IP_CHECK_CONDITIONS = "freefund.fraud.ipcheck.conditions";

    public static final String SPLIT_TRAFFIC_WEIGHT = "split.traffic.weight";

    public static final String COUPON_FRAUD_VALUES = "coupon.fraud.values";
    
    public static final String METRO_REPORT_EMAILIDS = "metro.report.emailids";

    public static final String ATM_COUPONS = "atm.coupons";

    public static final String USER_AGENT_ENQUEUING  = "user.agent.enqueuing";

    public static final String SOKARTI_PIXEL_ENABLED = "sokrati.pixel.enabled";

    public final static String BILLDESK_S2S_ENABLED = "paymentresponse.billdesk.s2s.enabled";

    public final static String BILLDESK_PAYU_ENABLED = "paymentresponse.payu.s2s.enabled";

    public static final String FACEBOOK_LOGIN_ENABLED = "facebook.login.enabled";

    public static final String GOOGLE_LOGIN_ENABLED = "google.login.enabled";
    public static final String CONVENIENCE_FEE_TITLE = "convenience.fee.title";

    public static final String FORCE_PAID_COUPONS_ENABLED  = "force.paid.coupons.enabled";
    public static final String IS_RUPAY_ENABLED  = "is.rupay.enabled";
    public static final String FORCE_RANDOM_COUPON_ORDER_ENABLED  = "force.random.coupons.order.enabled";

    public static final String EXCLUSIVE_COUPONS_CHECK  = "exclusive.coupon.enabled";
    public static final String RECO_COUPONS_CHECK  = "reco.coupon.enabled";
    public static final String IS_AB_TEST_RECO_COUPONS  = "ab.reco.coupon.enabled";
    public static final String IS_AB_TEST_CITY_FILTER_COUPONS  = "ab.city.filter.coupon.enabled";

    public static final String COUPON_REMOTING_ENABLED  = "coupon.remote.enabled";
    public static final String COUPON_BLOCK_REMOTING_ENABLED  = "coupon.block.remote.enabled";
    public static final String UMS_REMOTING_ENABLED = "ums.remote.enabled";
    public static final String BILLPAY_REMOTING_ENABLED  = "billpay.remote.enabled";
    public static final String HCOUPON_FULFILLMENT_ENABLED  = "hcoupon.fulfillment.enabled";
    public static final String COUPON_FRAUD_CHECK  = "coupon.fraud.check.enabled";
    public static final String COUPON_FRAUD_COOKIE  = "coupon.fraud.cookie.enabled";
    public static final String COUPON_FRAUD_IP  = "coupon.fraud.ip.enabled";
    public static final String COUPON_FRAUD_EMAIL  = "coupon.fraud.email.enabled";
    public static final String COUPON_FRAUD_PROFILE_NO  = "coupon.fraud.profileno.enabled";
    public static final String COUPON_FRAUD_IMEI_NO = "coupon.fraud.imeino.enabled";
    public static final String COUPON_FRAUD_USER_ID = "coupon.fraud.userid.enabled";
    
    public static final String FINGER_PRINTING_ENABLED = "finger.printing.enabled";

    public static final String RECHARGE_RETRY_CHECK  = "recharge.retry.enabled";

    public static final String SQS_ENQUEUE_ENABLED = "sqs.enqueue.enabled";

    public static final String RECHARGE_AUTO_ALERT_ENABLED = "recharge.auto.alert";

    public static final String COUPON_SERVICE_PROFILING_ENABLED = "couponservice.profiling.enabled";
    public static final String COUPON_CITY_FILTER_DOWN_ENABLED = "couponservice.cityfilterdown.enabled";

    public static final String SURVEY_ENABLED = "survey.enabled";
    
    public static final String APPGAL_ENABLED = "appgal.enabled";
    
    public static final String OPERATOR_RETRY_ALL_ERROR_CODES_ENABLED = "operator.retry.all.error.enabled";
    
    public static final String OPERATOR_RETRY_ONLY_NUTS_ENABLED = "operator.retry.only.nuts.enabled";
    
    public static final String TRACKING_SQS_ENABLED = "tracking.sqs.enabled";

    public static final String CONFIGURABLE_COUPON_CATEGORIES_ENABLED = "configurable.coupon.categories.enabled";

    public final String RECHARGE_CUSTOM_CONFIG = "rechargeCustomConfig";
    
    public final String USER_AGENT_SPLITTER = "userAgentSplitter";
    
    public final String POSTPAID_BILLER_VALIDATION_OPERATORS = "postpaidBillerValidationOperators";
    private final String STATUS_CHECK_TYPE = "aggressiveStatusCheck";
    public  final String POSTRECHARGE_CUSTOM_CONFIG = "postRechargeCustomConfig";
    public final String VALIDATE_RECHARGE_CUSTOM_CONFIG = "validateRechargeCustomConfig";
    public final String GOOGLE_CREDITS_POST_RECHARGE_CUSTOM_CONFIG = "googleCreditsPostRechargeCustomConfig";
    public final String PLAN_LEVEL_RETRY_CONFIG = "planLevelRetryConfig";
    public final String DTH_PLAN_VALIDATION_CONFIG = "dthPlanValidationConfig";

    public final String RECHARGE_CUSTOM_FAILURE_CONFIG = "rechargeCustomFailureConfig";

    private static final String BILLDESK_POC_ENABLED = "cc.billdesk.poc";

    public final static String READ_DENOMINATION_VALIDITY_FROM_DYNAMO = "read.denomination.data.from.dynamo.enabled";
    public final static String OPERATOR_DOWN_CONFIG = "opDownCustonConfig";
    public static final String TRACKER_SERVER_ENABLED = "tracker.server.enabled";
    private static final String EMAIL_APP_ENABLED = "emailapp.enabled";
    private static final String EMAIL_APP_ENABLED_FOR_COUPON = "emailapp.enabled.coupon";
    private static final String ENABLE_RECHARGE_API = "recharge.api.enabled";
    private static final String OPERATOR_RECHARGE_RETRY = "recharge.postpaid.retry";
    private static final String AGGREGATOR_RECHARGE_RETRY = "recharge.aggregator.retry";
    private static final String TIME_BASED_REFUND_ENABLED = "time.based.refund.enabled";
    private static final String PLAN_LEVEL_RETRY_ENABLED = "plan.level.retry.enabled";
    
    private static final String DTH_PLAN_VALIDATION_ENABLED = "dth.plan.validation.enabled";
    private static final String JIO_NUMBER_VALIDATION_ENABLED = "jio.number.validation.enabled";

    private static final String ENABLE_AG_PREFERENCE = "ag.preference.enabled";
    
    private static final String ENABLE_AG_EURONET_VAS2_WSDL = "ag.euronet.vas2.wsdl.enabled";
    private static final String ENABLE_AG_EURONET_AIRTEL_WALLET="euronet.airtel.new.wallet.enabled";

    private static final String RECHARGE_RETRY_ON_POSTPAID = "recharge.postpaid.retry";
    private static final String ENABLE_SMS_TEMPLATES = "sms.templates.enabled";

    private static final String RECHARGE_CALLBACK_ENABLED = "recharge.callback.enabled";
    private static final String TEST_CONDITIONS = "test.condition";

    private static final String WEB_ASSETS_VERSION_NO = "assets.version.no";
    private static final String APP_COUPON_CITY_FILTER = "app.coupon.city.filter";
    private static final String CITY_FILTER_ENABLED = "city.filter.enabled";
    private static final String LOCATION_SAVING_ENABLED = "location.saving.enabled";
    private static final String SAVE_IMPRESSION_ENABLED = "save.impression.enabled";
    private static final String POSTPAID_DATACARD_RETRY_ENABLED = "postpaid.datacard.retry.enabled";
    private static final String MARS_ENABLED = "mars.enabled";
    private static final String COUPON_CONFIG = "coupon.config";
    private static final String IVRS_ENABLED = "ivrs.enabled";
    private static final String RECHARGE_REMINDER_ENABLED = "recharge.reminder.enabled";
    private static final String USER_MESSAGE_STORE_ENBALED = "user.message.store.enabled";
    private static final String CENTAUR_RANDOM_COUPON = "centaur.remote.coupon.enabled";
    private static final String ONE_CHECK_FK_ENABLED = "onecheck.fk.enabled";
    public static final String FORTKNOX_CARD_MIGRATION = "cardMigrationConfig";
    public static final String ONECHECK_API_MOCK_ENABLED = "oneCheckAPIConfig";
    private static final String ONE_CHECK_WALLET_ENABLED = "one.check.wallet.enabled";
    private static final String VCARD_CREATE_ENABLED = "vcard.create.enabled";
    private static final String LOAD_ANTI_FRAUD_PROPS_FROM_FILE = "loadAntiFraudPropsFromFile";

    private static final String FORCE_UPGRADE_DETAILS ="force.upgrade.details";

    private static final String  DEBIT_ATM_ENABLED                           = "debitatmenabled";
    private static final String  ICICI_QC_ENABLED                            = "iciciqcenabled";
    private static final String  CARD_STATUS_ENABLED                         = "cardstatusenabled";


    /*
     * ID that uniquely identifies a
     * freecharge stack. E.g., NetMagicQA3, AWSQA1 and so on.
     * This is needed to disambiguate between stacks
     * when they contact the same external services such as
     * aggregators, SQS.
     */
    public static final String STACK_ID = "freecharge.stack.id";

    public static final String NEW_REFUND_PERCENTAGE = "new.refund.percentage";

    public static final String GENERIC_PROMOCODE_VALID_KEYS = "generic.promocode.valid.keys";

	private static final String SMSGUPSHUP_HLR_ENABLED = "smsgupshup.hlr.enable";

	private static final String INVALID_NUMBER_BLOCKING_ENABLED = "invalidnumber.blocking.enabled";

	private static final String TD_PLAN_VALIDATION_ENABLED = "td.plan.validation.enabled";

	private static final String REPEAT_RECHARGE_BLOCK_ENABLED = "repeatrecharge.blocking.enabled";
	
    private static final String  KLICKPAY_API_DOWNTIME                       = "klickpay.downtime.api";
    
    private static final String  KLICKPAY_NB_API_DOWNTIME                    = "klickpay.nb.downtime.api";
    
    public final static String KLICKPAY_S2S_ENABLED = "paymentreponse.klickpay.s2s.enabled";

    private static final String ALWAYS_REFUND_ON_DELAYED_PAYMENT_SUCCESS = "always.refund.on.delayed.payment.success";

    private static final String NULL_FAILURE_REFUND_COMMUNICATION_ENABLED = "null.failure.refund.communication.enabled";
    
    private static final String NULL_SUCCESS_REFUND_COMMUNICATION_ENABLED = "null.success.refund.communication.enabled";
    
    private static final String CREATE_BILL_V2_ENABLED = "create.bill.v2.enabled";
    
    private static final String CENTAUR_ENABLED                                      = "centaur.ab.testing.enabled";

    private static final String NEW_FAILURE_FULFILMENT_ENABLED = "new.failure.fulfilment";
    
    private static final String DTH_NUMBER_VALIDATION_ENABLED	= "dth.number.validation.enabled";

	private static final String DB_UPDATE_ON_CALLBACK_ENABLED = "db.update.callback.enabled";

	private static final String POSTPAID_FETCHBILL_ENABLED = "postpaid.fetchbill.enabled";
	
	private static final String AUTO_PAY_ENABLED = "autopay.enabled";
	
	private static final String AUTO_PAY_ENABLED_FOR_UTILITY = "autopay.utility.enabled";

	private static final String INVALID_PLANS_DENOMINATION_VALIDATION_ENABLED = "invalid.plans.denomination.validation.enabled";
	
	private static final String DATA_CARD_OPERATOR_CIRCLE_BLOCKING_ENABLED = "datacard.operator.circle.block.enabled";

	private static final String INTRANSACTION_PLAN_VALIDATION_ENABLED = "intransaction.plan.validation.enabled";
	
	private static final String FULFILLBILL_NEW_ENABLED ="fulfill.bill.new.enabled";

	private static final String FETCHBILL_NEW_ENABLED = "fetch.bill.new.enabled";
	
	private static final String EURONET_NEW_ERROR_CODE_ENABLED = "euronet.new.error.code.enabled";
	
	private static final String BILLPAY_OPERATOR_CHANNEL_BLOCKING_ENABLED = "billpay.operator.channel.block.enabled";
	
	private static final String COUPON_CAROUSEL = "coupon.carousel";
	
	private static final String RECHARGE_RETRY_CODES_CONFIG = "rechargeRetryCodesConfig";
	private static final String RECHARGE_RETRY_CODES_CONFIG_OBJECT = "rechargeRetryCodesConfigObjects";
	private static final String RETRY_RECHARGE_CODE_CONFIG_ENABLED = "retryRechargeCodeConfigEnabled";
	private static final String CUSTOM_EMAIL_FAILURE_AGGR_RESP_CODE_CONFIG = "customEmailFailureAggrRespCodeConfig";
	
	private static final String APP_DEEP_LINK_SITE_ASSOCIATION = "app.deep.link.site.association";
	
	@Autowired
	private IAppStatePersistence appState;

	@Autowired
	private FCProperties fcProperties;

    public void setLocationSavingEnabled(Boolean value){
        this.setBooleanValue(LOCATION_SAVING_ENABLED, value);
    }

    public Boolean isLocationSavingEnabled(){
        return this.getBooleanValue(LOCATION_SAVING_ENABLED);
    }

    public void setCityFilterEnabled(Boolean value){
        this.setBooleanValue(CITY_FILTER_ENABLED, value);
    }

    public Boolean isCityFilterEnabled(){
        return this.getBooleanValue(CITY_FILTER_ENABLED);
    }
    
    public void setSaveImpressionEnabled(Boolean val){
        this.setBooleanValue(SAVE_IMPRESSION_ENABLED, val);
    }

    public Boolean isSaveImpressionEnabled(){
        return this.getBooleanValue(SAVE_IMPRESSION_ENABLED);
    }

    public Boolean isAppCouponCityFilterEnabled(){
        return this.getBooleanValue(APP_COUPON_CITY_FILTER);
    }

    public void setAppCouponCityFilterEnabled(Boolean enabled){
        this.setBooleanValue(APP_COUPON_CITY_FILTER, enabled);
    }

	public Boolean isConfigurableCouponCategoriesEnabled() {
	    return this.getBooleanValue(CONFIGURABLE_COUPON_CATEGORIES_ENABLED);
	}

	public void setConfigurableCouponCategoriesEnabled(Boolean val) {
	    this.setBooleanValue(CONFIGURABLE_COUPON_CATEGORIES_ENABLED, val);
	}

   public Boolean isOperatorAlertBlockEnabled() {
        return this.getBooleanValue(OPERATOR_ALERT_BLOCK);
    }

    public void setOperatorAlertBlockEnabled(Boolean val) {
        this.setBooleanValue(OPERATOR_ALERT_BLOCK, val);
    }


	public Boolean isMissedRechargeAttemptEnabled() {
	    return this.getBooleanValue(MISSED_RECHARGE_ATTEMPT);
	}

	public void setMissedRechargeAttemptEnabled(Boolean val) {
	    this.setBooleanValue(MISSED_RECHARGE_ATTEMPT, val);
	}
	
	public void setConvenienceFeeEnabled(boolean enabled) {
        setBooleanValue(CONVENIENCE_FEE_ENABLED, enabled);
    }
    
    public boolean isConvenienceFeeEnabled() {
        return this.getBooleanValue(CONVENIENCE_FEE_ENABLED);
    }


    public Boolean isIpCheckEnabled(){
        return this.getBooleanValue(IP_CHECK_ENABLED);
    }

    public void setIpCheckEnabled(Boolean val){
        this.setBooleanValue(IP_CHECK_ENABLED, val);
    }
    
    public Boolean isRechargeRetryCodesConfigEnabled() {
    	JSONObject configJson = getCustomProp(RECHARGE_CUSTOM_CONFIG);
    	Boolean isRechargeRetryCodeConfigsEnabled = false;
        if (configJson != null && configJson.get(RETRY_RECHARGE_CODE_CONFIG_ENABLED) != null) {
        	try {
        		isRechargeRetryCodeConfigsEnabled = Boolean.parseBoolean((String)configJson.get(RETRY_RECHARGE_CODE_CONFIG_ENABLED));
        	}catch (Exception e) {
				logger.error("Unable to parse retryRechargeCodeConfigEnabled value : " + configJson.get(RETRY_RECHARGE_CODE_CONFIG_ENABLED));
			}
        }
        return isRechargeRetryCodeConfigsEnabled;
    }
    
    public List<RechargeRetryConfig> getRechargeRetryCodesConfigs(){
    	if(appState.getAppValue(RECHARGE_RETRY_CODES_CONFIG) != null) {
       		JSONObject rechargeRetryCodesConf = getCustomProp(RECHARGE_RETRY_CODES_CONFIG);
       		ObjectMapper mapper = new ObjectMapper();
   			try {
   				List<RechargeRetryConfig> rechargeRetryCodesConfs = mapper.readValue((String)rechargeRetryCodesConf.get(RECHARGE_RETRY_CODES_CONFIG_OBJECT), 
   					    new TypeReference<ArrayList<RechargeRetryConfig>>() {});
   				return rechargeRetryCodesConfs;
   			} catch (Exception e){
   				logger.error("Unable to parse recharge retry config because of error : ", e);
   			}
    	}
    	return null;
    }
    
    public boolean isCustomFailureAggrRespCode(Integer opearatorMasterId, String aggrRespCode){
    	if(appState.getAppValue(CUSTOM_EMAIL_FAILURE_AGGR_RESP_CODE_CONFIG) != null) {
       		JSONObject customAggrRespCodesConf = getCustomProp(CUSTOM_EMAIL_FAILURE_AGGR_RESP_CODE_CONFIG);
       		if(customAggrRespCodesConf != null && customAggrRespCodesConf.get(String.valueOf(opearatorMasterId)) != null) {
       			try {
       				Set<String> customAggrRespCodes = new HashSet<String>(Arrays.asList(((String)customAggrRespCodesConf.get(String.valueOf(opearatorMasterId))).split(",")));
       				if(customAggrRespCodes.contains(aggrRespCode)) {
       					return true;
       				}
       			} catch (Exception e){
       				logger.error("Unable to check whether custom email aggrrespcode present because of error : ", e);
       			}
       		}
    	}
    	return false;
    }

    public JSONObject getIpCheckConditions(){
        JSONObject obj = getJsonValue(IP_CHECK_CONDITIONS);
        return obj!=null?obj:new JSONObject();
    }

    public String getSplitTrafficWeight() {
        JSONObject obj = getJsonValue(SPLIT_TRAFFIC_WEIGHT);
        return obj != null ? ("" + obj.get("weights")) : "";
    }

    public void setSplitTrafficWeight(String weightSequence){
        this.setJsonValue(SPLIT_TRAFFIC_WEIGHT, new JSONObject(Collections.singletonMap("weights", weightSequence)));
    }

    public void setIpCheckConditions(JSONObject conditions) {
        this.setJsonValue(IP_CHECK_CONDITIONS, conditions);
    }

    public List<Integer> getCouponIdsForFraudCheck(){
    	JSONObject json = getCouponFraudValues();
    	List<Integer> couponIdsForFraudCheck = new ArrayList<>();
    	if (json != null){
    		String couponIds = String.valueOf(json.get("couponIds"));
    		if (couponIds != null && !couponIds.isEmpty()){
    			StringTokenizer tokenizer = new StringTokenizer(couponIds, ",");
    			while (tokenizer.hasMoreTokens()){
    				String couponIdStr = tokenizer.nextToken();
    				if (FCUtil.isInteger(couponIdStr)){
    					couponIdsForFraudCheck.add(Integer.parseInt(couponIdStr));
    				}
    			} // end while
    		} // end if
    	} // end if
    	return couponIdsForFraudCheck;
    }

    public List<String> getGenericPromocodeValidKeyList() {
        JSONObject json = getJsonValue(GENERIC_PROMOCODE_VALID_KEYS);
        if (null != json) {
            String keyList = String.valueOf(json.get("keyList"));
            if (FCUtil.isNotEmpty(keyList)) {
                return Arrays.asList(keyList.split(","));
            }
        }
        return null;
    }

    public Map<Integer, Integer> getCouponFraudMaxCountForCookie(){
    	return getCouponFraudMaxCountForKey("MAX_COOKIE_COUNT");
    }

    public Map<Integer, Integer> getCouponFraudMaxCountForIP(){
    	return getCouponFraudMaxCountForKey("MAX_IP_COUNT");
    }

    public Map<Integer, Integer> getCouponFraudMaxCountForProfile(){
    	return getCouponFraudMaxCountForKey("MAX_PROFILENO_COUNT");
    }
    
    public Map<Integer, Integer> getCouponFraudMaxCountForEmail(){
    	return getCouponFraudMaxCountForKey("MAX_EMAIL_COUNT");
    }

    public Map<Integer, Integer> getCouponFraudMaxCountForImei(){
    	return getCouponFraudMaxCountForKey("MAX_IMEINO_COUNT");
    }
    
    public Map<Integer, Integer> getCouponFraudMaxCountForUserId(){
    	return getCouponFraudMaxCountForKey("MAX_USERID_COUNT");
    }

    private Map<Integer, Integer> getCouponFraudMaxCountForKey(String key){
    	JSONObject json = getCouponFraudValues();
    	Map<Integer, Integer> couponIdsForFraudCheck = new HashMap<>();
    	if (json != null){
    		String couponIdQuatities = String.valueOf(json.get(key)).trim();
    		if (couponIdQuatities != null && !couponIdQuatities.isEmpty() && !couponIdQuatities.equalsIgnoreCase("null")){
    			StringTokenizer tokenizer = new StringTokenizer(couponIdQuatities, ";");
    			while (tokenizer.hasMoreTokens()){
    				String couponIdQuatityStr = tokenizer.nextToken();
					StringTokenizer commaTokenizer = new StringTokenizer(couponIdQuatityStr, "=");
					Integer couponId = null;
					Integer quantity = null;
					if (commaTokenizer.hasMoreTokens()){
						String couponIdStr = commaTokenizer.nextToken();
						if (commaTokenizer.hasMoreTokens()){
							String couponQtyStr = commaTokenizer.nextToken();
							if (FCUtil.isInteger(couponIdStr) && FCUtil.isInteger(couponQtyStr)){
								couponId = Integer.parseInt(couponIdStr);
								quantity = Integer.parseInt(couponQtyStr);
								couponIdsForFraudCheck.put(couponId, quantity);
							}
						}
					}
    			} // end while
    		} // end if
    	} // end if
    	return couponIdsForFraudCheck;
    }

    public List<Integer> getCouponIdsForATM(){
    	JSONObject json = getATMCoupons();
    	List<Integer> couponIdsForATM = new ArrayList<>();
    	if (json != null){
    		String couponIds = String.valueOf(json.get("couponIds"));
    		if (couponIds != null && !couponIds.isEmpty()){
    			StringTokenizer tokenizer = new StringTokenizer(couponIds, ",");
    			while (tokenizer.hasMoreTokens()){
    				String couponIdStr = tokenizer.nextToken();
    				if (FCUtil.isInteger(couponIdStr)){
    					couponIdsForATM.add(Integer.parseInt(couponIdStr));
    				}
    			} // end while
    		} // end if
    	} // end if
    	return couponIdsForATM;
    }

    public JSONObject getATMCoupons(){
        JSONObject obj = getJsonValue(ATM_COUPONS);
        return obj!=null?obj:new JSONObject();
    }

    public void setATMCoupons(JSONObject conditions){
        this.setJsonValue(ATM_COUPONS, conditions);
    }

    public JSONObject getCouponFraudValues(){
        JSONObject obj = getJsonValue(COUPON_FRAUD_VALUES);
        return obj!=null?obj:new JSONObject();
    }

    public void setCouponFraudValues(JSONObject conditions){
        this.setJsonValue(COUPON_FRAUD_VALUES, conditions);
    }
    
    public JSONObject getMetroReportEmailIds(){
        JSONObject obj = getJsonValue(METRO_REPORT_EMAILIDS);
        return obj!=null?obj:new JSONObject();
    }

    public void setMetroReportEmailIds(JSONObject conditions){
        this.setJsonValue(METRO_REPORT_EMAILIDS, conditions);
    }

    public JSONObject getFbFraudConditions(){
        JSONObject obj = getJsonValue(FB_FRAUD_CONDITIONS);
        return obj!=null?obj:new JSONObject();
    }

    public void setFbFraudConditions(JSONObject conditions){
        this.setJsonValue(FB_FRAUD_CONDITIONS, conditions);
    }

    public Boolean isFbFraudCheckEnabled(){
        return getBooleanValue(FB_FRAUD_CHECK_ENABLED);
    }

    public void setFbFraudCheckEnabled(Boolean val){
        setBooleanValue(FB_FRAUD_CHECK_ENABLED, val);
    }

	public boolean isCardStorageEnabled() {
	    return getBooleanValue(CARD_STORAGE_ENABLED, fcProperties.getBooleanProperty(CARD_STORAGE_ENABLED));
	}

	public void setCardStorageEnabled(boolean flag) {
	    setBooleanValue(CARD_STORAGE_ENABLED, flag);
	}

    public void setPostpaidDatacardRetryEnabled(boolean flag) {
        setBooleanValue(POSTPAID_DATACARD_RETRY_ENABLED, flag);
    }
    
    public boolean isPostpaidDatacardRetryEnabled() {
        return getBooleanValue(POSTPAID_DATACARD_RETRY_ENABLED,
                fcProperties.getBooleanProperty(POSTPAID_DATACARD_RETRY_ENABLED));
    }
    
    public boolean isForcePaidCouponsEnabled() {
        return getBooleanValue(FORCE_PAID_COUPONS_ENABLED, getBooleanValue(FORCE_PAID_COUPONS_ENABLED));
    }

    public void setForcePaidCouponsEnabled(boolean flag) {
        setBooleanValue(FORCE_PAID_COUPONS_ENABLED, flag);
    }

    public boolean isRupayEnabled() {
        return getBooleanValue(IS_RUPAY_ENABLED, getBooleanValue(IS_RUPAY_ENABLED));
    }

    public void setRupayEnabled(boolean flag) {
        setBooleanValue(IS_RUPAY_ENABLED, flag);
    }

    public boolean isForceRandomCouponsOrderEnabled() {
        return getBooleanValue(FORCE_RANDOM_COUPON_ORDER_ENABLED, getBooleanValue(FORCE_RANDOM_COUPON_ORDER_ENABLED));
    }

    public void setForceRandomCouponsOrderEnabled(boolean flag) {
        setBooleanValue(FORCE_RANDOM_COUPON_ORDER_ENABLED, flag);
    }

    public boolean isCouponRemotingEnabled(){
    	return getBooleanValue(COUPON_REMOTING_ENABLED, getBooleanValue(COUPON_REMOTING_ENABLED));
    }
    
    public void setCouponRemotingEnabled(boolean flag){
    	setBooleanValue(COUPON_REMOTING_ENABLED, flag);
    }

    public boolean isCouponBlockRemotingEnabled(){
    	return getBooleanValue(COUPON_BLOCK_REMOTING_ENABLED, getBooleanValue(COUPON_BLOCK_REMOTING_ENABLED));
    }
    
    public void setCouponBlockRemotingEnabled(boolean flag){
    	setBooleanValue(COUPON_BLOCK_REMOTING_ENABLED, flag);
    }
    
    public boolean isUmsRemotingEnabled(){
    	return getBooleanValue(UMS_REMOTING_ENABLED, getBooleanValue(UMS_REMOTING_ENABLED));
    }
    
    public void setUmsRemotingEnabled(boolean flag){
    	setBooleanValue(UMS_REMOTING_ENABLED, flag);
    }
    
    public boolean isBillPayRemotingEnabled(){
        return getBooleanValue(BILLPAY_REMOTING_ENABLED, getBooleanValue(BILLPAY_REMOTING_ENABLED));
    }
    
    public void setBillPayRemotingEnabled(boolean flag){
        setBooleanValue(BILLPAY_REMOTING_ENABLED, flag);
    }
    
    public boolean isHCouponFulfillmentEnabled(){
    	return getBooleanValue(HCOUPON_FULFILLMENT_ENABLED, getBooleanValue(HCOUPON_FULFILLMENT_ENABLED));
    }
    
    public void setHCouponFulfillmentEnabled(boolean flag){
    	setBooleanValue(HCOUPON_FULFILLMENT_ENABLED, flag);
    }

    public boolean isCouponFraudCheckEnabled() {
        return getBooleanValue(COUPON_FRAUD_CHECK, getBooleanValue(COUPON_FRAUD_CHECK));
    }

    public void setCouponFraudCheckEnabled(boolean flag) {
        setBooleanValue(COUPON_FRAUD_CHECK, flag);
    }

    public boolean isExclusiveCouponsEnabled() {
        return getBooleanValue(EXCLUSIVE_COUPONS_CHECK, getBooleanValue(EXCLUSIVE_COUPONS_CHECK));
    }

    public void setExclusiveCouponsEnabled(boolean flag) {
        setBooleanValue(EXCLUSIVE_COUPONS_CHECK, flag);
    }

    public boolean isABRecoCouponsEnabled() {
        return getBooleanValue(IS_AB_TEST_RECO_COUPONS, getBooleanValue(IS_AB_TEST_RECO_COUPONS));
    }

    public void setABRecoCouponsEnabled(boolean flag) {
        setBooleanValue(IS_AB_TEST_RECO_COUPONS, flag);
    }
    
    public boolean isABCityFilterCouponsEnabled() {
        return getBooleanValue(IS_AB_TEST_CITY_FILTER_COUPONS, getBooleanValue(IS_AB_TEST_CITY_FILTER_COUPONS));
    }

    public void setABCityFilterCouponsEnabled(boolean flag) {
        setBooleanValue(IS_AB_TEST_CITY_FILTER_COUPONS, flag);
    }

    public boolean isRecoCouponsEnabled() {
        return getBooleanValue(RECO_COUPONS_CHECK, getBooleanValue(RECO_COUPONS_CHECK));
    }

    public void setRecoCouponsEnabled(boolean flag) {
        setBooleanValue(RECO_COUPONS_CHECK, flag);
    }

    public boolean isRechargeRetryEnabled() {
        return getBooleanValue(RECHARGE_RETRY_CHECK, getBooleanValue(RECHARGE_RETRY_CHECK));
    }

    public void setRechargeRetryEnabled(boolean flag) {
        setBooleanValue(RECHARGE_RETRY_CHECK, flag);
    }

    public boolean isCouponFraudCookieEnabled() {
        return getBooleanValue(COUPON_FRAUD_COOKIE, getBooleanValue(COUPON_FRAUD_COOKIE));
    }

    public void setCouponFraudCookieEnabled(boolean flag) {
        setBooleanValue(COUPON_FRAUD_COOKIE, flag);
    }

    public boolean isCouponFraudIPEnabled() {
        return getBooleanValue(COUPON_FRAUD_IP, getBooleanValue(COUPON_FRAUD_IP));
    }

    public void setCouponFraudIPEnabled(boolean flag) {
        setBooleanValue(COUPON_FRAUD_IP, flag);
    }

    public boolean isCouponFraudEmailEnabled() {
        return getBooleanValue(COUPON_FRAUD_EMAIL, getBooleanValue(COUPON_FRAUD_EMAIL));
    }

    public void setCouponFraudEmailEnabled(boolean flag) {
        setBooleanValue(COUPON_FRAUD_EMAIL, flag);
    }

    public boolean isCouponFraudProfileNoEnabled() {
        return getBooleanValue(COUPON_FRAUD_PROFILE_NO, getBooleanValue(COUPON_FRAUD_PROFILE_NO));
    }

    public void setCouponFraudProfileNoEnabled(boolean flag) {
        setBooleanValue(COUPON_FRAUD_PROFILE_NO, flag);
    }
    
    public boolean isCouponFraudImeiNoEnabled() {
        return getBooleanValue(COUPON_FRAUD_IMEI_NO, getBooleanValue(COUPON_FRAUD_IMEI_NO));
    }

    public void setCouponFraudImeiNoEnabled(boolean flag) {
        setBooleanValue(COUPON_FRAUD_IMEI_NO, flag);
    }
    
    public boolean isCouponFraudUserIdEnabled() {
    	return getBooleanValue(COUPON_FRAUD_USER_ID, getBooleanValue(COUPON_FRAUD_USER_ID));
    }

    public void setCouponFraudUserIdEnabled(boolean flag) {
    	setBooleanValue(COUPON_FRAUD_USER_ID, flag);
    }

	public boolean isRechargeRetryBlockEnabled() {
        return getBooleanValue(RECHARGE_RETRY_BLOCK);
	}

	public void setRechargeRetryBlockEnabled(boolean flag) {
        setBooleanValue(RECHARGE_RETRY_BLOCK, flag);
	}

    public boolean isPermanentFailureBlockEnabled() {
        return getBooleanValue(PERMANENT_FAILURE_BLOCK);
    }

    public boolean isPostpaidPermanentFailureBlockEnabled() {
        return getBooleanValue(POSTPAID_PERMANENT_FAILURE_BLOCK);
    }

    public boolean isLapuEnabled() {
        return getBooleanValue(LAPU_ENABLE);
    }

    public void setLapuEnabled(boolean flag) {
        setBooleanValue(LAPU_ENABLE, flag);
    }

    public boolean setPostpaidPermanentFailureBlockEnabled(boolean flag) {
        return getBooleanValue(POSTPAID_PERMANENT_FAILURE_BLOCK);
    }

    public void setPermanentFailureBlockEnabled(boolean flag) {
        setBooleanValue(PERMANENT_FAILURE_BLOCK, flag);
    }

    public boolean isDenominationNumberBlockEnabled() {
        return getBooleanValue(DENOMINATION_NUMBER_BLOCK);
    }

    public void setDenominationNumberBlockEnabled(boolean flag) {
        setBooleanValue(DENOMINATION_NUMBER_BLOCK, flag);
    }

    public boolean isEGNumberBlockEnabled() {
        return getBooleanValue(EG_NUMBER_BLOCK);
    }

    public void setEGNumberBlockEnabled(boolean flag) {
        setBooleanValue(EG_NUMBER_BLOCK, flag);
    }

    public boolean isInvalidDenominationBlockEnabled() {
        return getBooleanValue(INVALID_DENOMINATION_BLOCK);
    }

    public void setInvalidDenominationBlockEnabled(boolean flag) {
        setBooleanValue(INVALID_DENOMINATION_BLOCK, flag);
    }

	public boolean isFinalPageCouponsEnabled() {
        return getBooleanValue(FINAL_PAGE_COUPONS);
    }

    public void setFinalPageCouponsEnabled(boolean flag) {
        setBooleanValue(FINAL_PAGE_COUPONS, flag);
    }

	public boolean isMobileSiteAnalyticsEnabled() {
        return getBooleanValue(MOBILE_ANALYTICS);
    }

    public void setMobileSiteAnalyticsEnabled(boolean flag) {
        setBooleanValue(MOBILE_ANALYTICS, flag);
    }

    public boolean isWebSiteAnalyticsEnabled() {
        return getBooleanValue(WEB_ANALYTICS);
    }

    public void setWebSiteAnalyticsEnabled(boolean flag) {
        setBooleanValue(WEB_ANALYTICS, flag);
    }

    public boolean isAsyncRechargeEnabled() {
        return getBooleanValue(ASYNC_RECHARGE) || fcProperties.isProdMode();
    }

    public void setAsyncRechargeEnabled(boolean flag) {
        setBooleanValue(ASYNC_RECHARGE, flag);
    }
    public boolean isIMSDirectHitEnabled(){
    	return getBooleanValue(IMS_DIRECT_HIT);
    }
    public void setImsDirectHitEnabled(boolean flag){
    	setBooleanValue(IMS_DIRECT_HIT,flag);
    }

    public boolean isEmailBannerEnabled(){
    	return getBooleanValue(EMAIL_BANNER_ENABLE);
    }
    public void setEmailBannerEnabled(boolean flag){
    	setBooleanValue(EMAIL_BANNER_ENABLE,flag);
    }
    
    public boolean isEuronetHttpsEnabled(){
    	return getBooleanValue(EURONET_HTTPS_ENABLE);
    }
    public void setEuronetHttpsEnabled(boolean flag){
    	setBooleanValue(EURONET_HTTPS_ENABLE,flag);
    }
    
    public boolean isMongoSizeRecordEnabled() {
        return getBooleanValue(MONGO_SIZE_RECORD_ENABLED);
    }

    public void setMongoSizeRecordEnabled(boolean flag) {
        setBooleanValue(MONGO_SIZE_RECORD_ENABLED, flag);
    }

    public boolean isAsyncBillPaymentEnabled() {
        return getBooleanValue(ASYNC_BILL_PAYMENT);
    }

    public void setAsyncBillPaymentEnabled(boolean flag) {
        setBooleanValue(ASYNC_BILL_PAYMENT, flag);
    }
    
    public boolean isBBPSBillPaymentEnabled() {
        return getBooleanValue(BBPS_BILL_PAYMENT_ENABLED);
    }

    public void setBBPSBillPaymentEnabled(boolean flag) {
        setBooleanValue(BBPS_BILL_PAYMENT_ENABLED, flag);
    }

    public boolean isUnderMaintenanceHeaderBannerEnabled() {
        return getBooleanValue(UNDER_MAINTENANCE_HEADER_BANNER_ENABLED);
    }

    public void setUnderMaintenanceHeaderBanner(boolean flag) {
        setBooleanValue(UNDER_MAINTENANCE_HEADER_BANNER_ENABLED, flag);
    }

    public boolean isPaymentUnderMaintenance() {
        return getBooleanValue(PAYMENT_UNDER_MAINTENANCE);
    }

    public void setPaymentUnderMaintenance(boolean flag) {
        setBooleanValue(PAYMENT_UNDER_MAINTENANCE, flag);
    }

    public boolean isCashBackCampaignEnabled() {
        return getBooleanValue(CASH_BACK_CAMPAIGN_ENABLED);
    }

    public void setCashBackCampaign(boolean flag) {
        setBooleanValue(CASH_BACK_CAMPAIGN_ENABLED, flag);
    }

    public boolean isARDealEnabled() {
        return getBooleanValue(ARDEAL_ENABLED);
    }

    public void setARDealEnabled(boolean flag) {
        setBooleanValue(ARDEAL_ENABLED, flag);
    }

    public void setUserAgentEnqueuing(boolean flag) {
        setBooleanValue(USER_AGENT_ENQUEUING, flag);
    }

    public boolean isUserAgentEnqueuingEnabled() {
        return this.getBooleanValue(USER_AGENT_ENQUEUING);
    }

    public void setRecaptchaEnabled(boolean enabled) {
        setBooleanValue(RECAPTCHA_ENABLED, enabled);
    }

    public boolean isRecaptchaEnabled() {
        return this.getBooleanValue(RECAPTCHA_ENABLED);
    }

    public void setFailedLoginRecaptchaEnabled(boolean enabled) {
        setBooleanValue(FAILED_LOGIN_RECAPTCHA_ENABLED, enabled);
    }

    public boolean isFailedLoginRecaptchaEnabled() {
        return this.getBooleanValue(FAILED_LOGIN_RECAPTCHA_ENABLED);
    }

    public void setPromocodeEnabled(boolean enabled) {
        setBooleanValue(PROMOCODE_ENABLED, enabled);
    }

    public boolean isPromocodeEnabled() {
        return this.getBooleanValue(PROMOCODE_ENABLED);
    }

    public void setPromoRewardEnabled(boolean enabled) {
        setBooleanValue(PROMOREWARD_ENABLED, enabled);
    }

    public boolean isPromoRewardEnabled() {
        return this.getBooleanValue(PROMOREWARD_ENABLED);
    }

    public void setPepsiCampaignEnabled(boolean enabled) {
        setBooleanValue(PEPSI_CAMPAIGN_ENABLED, enabled);
    }

    public boolean isPepsiCampaignEnabled() {
        return this.getBooleanValue(PEPSI_CAMPAIGN_ENABLED);
    }

    public void setDIDCampaignEnabled(boolean enabled) {
        setBooleanValue(DID_CAMPAIGN_ENABLED, enabled);
    }

    public boolean isDIDCampaignEnabled() {
        return this.getBooleanValue(DID_CAMPAIGN_ENABLED);
    }

    public void setMCDCampaignEnabled(boolean enabled) {
        setBooleanValue(MCD_CAMPAIGN_ENABLED, enabled);
    }

    public boolean isMCDCampaignEnabled() {
        return this.getBooleanValue(MCD_CAMPAIGN_ENABLED);
    }

    public void setFestiveGiftEnabled(boolean enabled) {
        setBooleanValue(FESTIVE_GIFT_ENABLED, enabled);
    }

    public boolean isFestiveGiftEnabled() {
        return this.getBooleanValue(FESTIVE_GIFT_ENABLED);
    }

    public void setZedoAdEnabled(boolean enabled) {
        setBooleanValue(ZEDO_AD_ENABLED, enabled);
    }

    public boolean isZedoAdEnabled() {
        return this.getBooleanValue(ZEDO_AD_ENABLED);
    }

    public void setSokartiPixelEnabled(boolean enabled) {
        setBooleanValue(SOKARTI_PIXEL_ENABLED, enabled);
    }

    public boolean isSokratiPixelEnabled() {
        return this.getBooleanValue(SOKARTI_PIXEL_ENABLED);
    }

    public void setBillDeskS2SEnabled(boolean enabled) {
        setBooleanValue(BILLDESK_S2S_ENABLED, enabled);
    }

    public boolean isBillDeskS2SEnabled() {
        return this.getBooleanValue(BILLDESK_S2S_ENABLED);
    }

    public void setPayUS2SEnabled(boolean enabled) {
        setBooleanValue(BILLDESK_PAYU_ENABLED, enabled);
    }

    public boolean isPayUS2SEnabled() {
        return this.getBooleanValue(BILLDESK_PAYU_ENABLED);
    }

    private JSONObject getJsonValue(String key){
        try {
            String value = (String) appState.getAppValue(key);

            if (StringUtils.isNotBlank(value)) {
                JSONParser parser = new JSONParser();
                return  (JSONObject) parser.parse(value);
            }

        }catch (ParseException pe) {
            logger.error(String.format("Error parsing attribute '%s' from application state", key), pe);
        }catch (Exception e){
            logger.error(String.format("Error reading attribute '%s' from application state", key), e);
        }

        return null;
    }

    private void setJsonValue(String key, JSONObject value){
        String json = value.toJSONString();
        try {
            this.appState.setAppValue(key, json);
        }catch (Exception e){
            logger.error(String.format("Error updating attribute '%s' from application state", key), e);
        }
    }

    private boolean getBooleanValue(String key) {
        try {
            Object value = appState.getAppValue(key);
            return value == null? false: (Boolean) value;
        } catch(RuntimeException e) {
            logger.error(String.format("Error reading attribute '%s' from application state", key), e);
            return false;
        }
    }

    private boolean getBooleanValue(String key, boolean defaultValue) {
        try {
            Object value = appState.getAppValue(key);
            return value == null? defaultValue: (Boolean) value;
        } catch(RuntimeException e) {
            logger.error(String.format("Error reading attribute '%s' from application state", key), e);
            return defaultValue;
        }
    }

    private void setBooleanValue(String key, boolean value) {
        try {
            appState.setAppValue(key, value);
        } catch(RuntimeException e) {
            logger.error(String.format("Error updating attribute '%s' from application state", key), e);
        }
    }

    /**
     * Read <tt>java.lang.String</tt> value for given key. Return <tt>null</tt> when error key does not exist or when
     * any error occurs.
     * @param key the <tt>String</tt> key
     * @return <tt>java.lang.String</tt> value, else <tt>null</tt> if not found/error occurred
     */
    private String getStringValue(String key) {
        return getStringValue(key, null);
    }

    /**
     * Read <tt>java.lang.String</tt> value for given key. Return specified default value when error key does not exist
     * or when any error occurs.
      * @param key the <tt>String</tt> key
     * @param defaultValue the default <tt>java.lang.String</tt> value to return when key not found or some error occurs
     * @return <tt>java.lang.String</tt> value, else the <tt>defaultValue</tt> argument if not found/error occurred
     */
    private String getStringValue(String key, String defaultValue) {
        try {
            Object value = appState.getAppValue(key);
            return value == null? defaultValue: (String) value;
        } catch(RuntimeException e) {
            logger.error(String.format("Error reading attribute '%s' from application state", key), e);
            return defaultValue;
        }
    }

    /**
     * Set <tt>java.lang.String</tt> value against a <tt>java.lang.String</tt> key.
     * @param key the <tt>java.lang.String</tt> key
     * @param value the <tt>java.lang.String</tt> value
     */
    private void setStringValue(String key, String value) {
        try {
            appState.setAppValue(key, value);
        } catch(RuntimeException e) {
            logger.error(String.format("Error updating attribute '%s' from application state", key), e);
        }
    }

    public String getUserIds() {
    	Object value = appState.getAppValue(PRIVILEGED_USER_IDS);
    	return (String) value;
    }

    public String getClosedImeiGroup(){
        return (String) appState.getAppValue(CLOSED_IMEI_GROUP_KEY);
    }

    public void setClosedImeiGroupKey(String imeiGroup){
        this.appState.setAppValue(CLOSED_IMEI_GROUP_KEY, imeiGroup);
    }
    
    public String getBBPSEnabledBillers() {
    	Object value = appState.getAppValue(BBPS_ENABLED_BILLERS);
    	return (String) value;
    }

    public String getCityName(){
        return (String) appState.getAppValue(CITY_NAME_KEY);
    }
    
    public void setCityName(String cityName){
        this.appState.setAppValue(CITY_NAME_KEY, cityName);
    }
    
    public Boolean isClosedImeiGroupEnabled(){
        return getBooleanValue(CLOSED_IMEI_GROUP_ENABLED);
    }

    public void setClosedImeiGroupEnabled(boolean value){
        setBooleanValue(CLOSED_IMEI_GROUP_ENABLED, value);
    }

    public String getBlackListedEmailIds() {
        Object value = appState.getAppValue(BLACKLISTED_EMAIL_IDS);
        return (String) value;
    }

    public void setUserIds(String userIds) {
    	appState.setAppValue(PRIVILEGED_USER_IDS, userIds);
    }

    public void setBlackListedEmailIds(String emailIds) {
        appState.setAppValue(BLACKLISTED_EMAIL_IDS, emailIds);
    }

    public boolean isFreefundApplycheckEnabled() {
		return this.getBooleanValue(FREEFUND_APPLY_CHECK_ENABLED);
	}

	public void setFreefundApplycheckEnabled(boolean value) {
		this.setBooleanValue(FREEFUND_APPLY_CHECK_ENABLED, value);
	}

	public void setFacebookLoginEnabled(boolean value) {
        this.setBooleanValue(FACEBOOK_LOGIN_ENABLED, value);
    }

	public void setGoogleLoginEnabled(boolean value) {
        this.setBooleanValue(GOOGLE_LOGIN_ENABLED, value);
    }

	public boolean isFacebookLoginEnabled() {
        return this.getBooleanValue(FACEBOOK_LOGIN_ENABLED, true);
    }

	public boolean isGoogleLoginEnabled() {
        return this.getBooleanValue(GOOGLE_LOGIN_ENABLED, true);
    }
	
	public void setConvenienceFeeTitle(String value){
		appState.setAppValue(CONVENIENCE_FEE_TITLE, value);
	}
	
	public String getConvenienceFeeTitle() {
		return String.valueOf(appState.getAppValue(CONVENIENCE_FEE_TITLE));
	}

    public boolean isFingerPrintingEnabled() {
        //Switch on fingerprinting in QA by default
        return fcProperties.isQaMode() || this.getBooleanValue(FINGER_PRINTING_ENABLED);
    }

    public void setFingerPrintingEnabled(boolean flag) {
        this.setBooleanValue(FINGER_PRINTING_ENABLED, flag);
    }
    
    public JSONObject getCouponCarousel()
    {
    	return this.getJsonValue(COUPON_CAROUSEL);
    }
    
    public void setCouponCarousel(JSONObject json)
    {
    	this.setJsonValue(COUPON_CAROUSEL, json);
    }

    public JSONObject getCustomProp(String key){
        JSONObject obj = getJsonValue(key);
        return obj!=null?obj:new JSONObject();
    }

    public void setCustomProp(String key, JSONObject prop) {
        String json = prop.toJSONString();
        try {
            this.appState.setCustomAppValue(key, json);
        }catch (Exception e){
            logger.error(String.format("Error updating attribute '%s' from application state", key), e);
        }
    }
    
    public List<Map<String, Object>> getCustomProps() {
        return this.appState.getCustomProps();
    }



	public String getStackID() {
		/*
		 * For prod return hard-coded value.
		 */
		if (fcProperties.isProdMode() && !fcProperties.isPreProdMode()) {
			return "prod";
		}
		
		if(fcProperties.isQaMode() && StringUtils.isNotEmpty(fcProperties.getProperty(STACK_ID))) {
			return fcProperties.getProperty(STACK_ID);
		}

		Object value = appState.getAppValue(STACK_ID);
		return (String) value;
	}


    public void setStackID(String stackId) {
        appState.setAppValue(STACK_ID, stackId);
    }

    public void setNewRefundPercentage(Integer newRefundPercentage) {
        appState.setAppValue(NEW_REFUND_PERCENTAGE, newRefundPercentage);
    }

    public Integer getNewRefundPercentage() {
        Object value = appState.getAppValue(NEW_REFUND_PERCENTAGE);
        logger.info("Value retrieved from cache: " + value);
        Integer newRefundPercentage;
        try {
            newRefundPercentage = Integer.parseInt((String) value);
            logger.info("New refund percentage found is: "+newRefundPercentage);
        } catch (Exception e) {
            logger.error("Exception in casting new refund percentage: ",e);
            newRefundPercentage = null;
        }
        return newRefundPercentage;
    }
    public boolean isSQSEnqueueEnabled() {
        return this.getBooleanValue(SQS_ENQUEUE_ENABLED);
    }

    public void setSQSEnqueueEnabled(boolean flag) {
        this.setBooleanValue(SQS_ENQUEUE_ENABLED, flag);
    }

    public boolean isRechargeAutoAlertEnabled() {
        return this.getBooleanValue(RECHARGE_AUTO_ALERT_ENABLED);
    }
    public void setRechargeAutoAlertEnabled(boolean flag) {
        this.setBooleanValue(RECHARGE_AUTO_ALERT_ENABLED, flag);
    }

    public void setTrackingSqsEnabled(boolean enabled) {
        setBooleanValue(TRACKING_SQS_ENABLED, enabled);
    }

    public boolean isTrackingSqsEnabled() {
        return this.getBooleanValue(TRACKING_SQS_ENABLED);
    }

    public boolean isBillDeskForCCEnabled(){
        return this.getBooleanValue(BILLDESK_POC_ENABLED);
    }

    public void setBillDeskForCCEnabled(boolean enabled){
        setBooleanValue(BILLDESK_POC_ENABLED, enabled);
    }

    public void setReadDenominationValidityFromDynamo(boolean enabled) {
        setBooleanValue(READ_DENOMINATION_VALIDITY_FROM_DYNAMO, enabled);
    }

    public boolean isReadDenominationValidityFromDynamo() {
        return this.getBooleanValue(READ_DENOMINATION_VALIDITY_FROM_DYNAMO);
    }

    public void setTrackerServerEnabled(boolean enabled) {
        setBooleanValue(TRACKER_SERVER_ENABLED, enabled);
    }

    public boolean isTrackerServerEnabled() {
        return this.getBooleanValue(TRACKER_SERVER_ENABLED);
    }

    public void setEmailAppEnabled(boolean enabled) {
        setBooleanValue(EMAIL_APP_ENABLED, enabled);
    }

    public boolean isEmailAppEnabled() {
        return this.getBooleanValue(EMAIL_APP_ENABLED);
    }

    public void setEmailAppEnabledForCoupon(boolean enabled) {
        setBooleanValue(EMAIL_APP_ENABLED_FOR_COUPON, enabled);
    }

    public boolean isEmailAppEnabledForCoupon() {
        return this.getBooleanValue(EMAIL_APP_ENABLED_FOR_COUPON);
    }

    /*
     * This method takes configured condition values. If not available default values are returned
     */
    public Integer getConfigValue(String configProperty, Integer defaultValue){
        JSONObject configConditions = this.getFbFraudConditions();
        if (configConditions.containsKey(configProperty) && configConditions.get(configProperty) != null){
            try {
                return Integer.parseInt((String) configConditions.get(configProperty));
            }catch (Exception e){
                logger.warn("Invalid config " + configProperty + " value set for fraud check", e);
            }
        }
        return defaultValue;
    }
    
    public boolean isBBPSEnabled() {
        return this.getBooleanValue(ENABLED_BILLDESK_BBPS);
    }
    

    public boolean isSurveyEnabled(){
    	return getBooleanValue(SURVEY_ENABLED);
    }
    
    public void setSurveyEnabled(boolean surveyEnabled){
    	setBooleanValue(SURVEY_ENABLED, surveyEnabled);
    }
    
	public boolean isCouponCityFilterDownEnabled() {
		return getBooleanValue(COUPON_CITY_FILTER_DOWN_ENABLED);
	}

	public void setCouponCityFilterDownEnabled(boolean couponCityFilterEnable){
		setBooleanValue(COUPON_CITY_FILTER_DOWN_ENABLED, couponCityFilterEnable);
	}

    public void setRechargeApiEnabled(boolean enabled) {
        setBooleanValue(ENABLE_RECHARGE_API, enabled);
    }
    
    public void setAgPreferenceEnabled(boolean enabled) {
           setBooleanValue(ENABLE_AG_PREFERENCE, enabled);
    }
    public void setEuronetVas2Enabled(boolean enabled) {
        setBooleanValue(ENABLE_AG_EURONET_VAS2_WSDL, enabled);
    }
    
    public boolean isRechargeApiEnabled() {
        return this.getBooleanValue(ENABLE_RECHARGE_API);
    }
    
    public void setOperatorRetryEnabled(boolean enabled) {
        setBooleanValue(OPERATOR_RECHARGE_RETRY, enabled);
    }
    public boolean isAgPreferenceEnabled() {
           return this.getBooleanValue(ENABLE_AG_PREFERENCE);
    }
    public boolean isEuronetVas2Enabled() {
        return this.getBooleanValue(ENABLE_AG_EURONET_VAS2_WSDL);
    }
    
    public boolean isAirtelEuroNewWalletEnabled() {
        return this.getBooleanValue(ENABLE_AG_EURONET_AIRTEL_WALLET);
    }
    
    public void setAirtelEuroNewWalletEnabled(boolean enabled) {
        setBooleanValue(ENABLE_AG_EURONET_AIRTEL_WALLET, enabled);
    }
    
    public void setPostPaidRetryEnabled(boolean enabled) {
        setBooleanValue(RECHARGE_RETRY_ON_POSTPAID, enabled);
    }

    public boolean isOperatorRetryEnabled() {
        return this.getBooleanValue(OPERATOR_RECHARGE_RETRY);
    }
    
    public boolean isAggregatorRetryEnabled() {
        return this.getBooleanValue(AGGREGATOR_RECHARGE_RETRY);
    }
    
    public void setAggregatorRetryEnabled(boolean enabled) {
        setBooleanValue(AGGREGATOR_RECHARGE_RETRY,enabled);
    }
    
    public boolean isTimeBasedRefundEnabled() {
        return this.getBooleanValue(TIME_BASED_REFUND_ENABLED);
    }
    
    public void setTimeBasedRefundEnabled(boolean enabled) {
        setBooleanValue(TIME_BASED_REFUND_ENABLED,enabled);
    }
    
    public void setSMSTemplatesEnabled(boolean enabled) {
        setBooleanValue(ENABLE_SMS_TEMPLATES, enabled);
    }

    public boolean isSMSTemplatesEnabled() {
        return this.getBooleanValue(ENABLE_SMS_TEMPLATES);
    }
    
    public boolean isRechargeCallbackEnabled() {
        return this.getBooleanValue(RECHARGE_CALLBACK_ENABLED, fcProperties.getBooleanProperty(RECHARGE_CALLBACK_ENABLED));
    }

    public void setRechargeCallbackEnabled(boolean flag) {
        setBooleanValue(RECHARGE_CALLBACK_ENABLED, flag);
    }
    
    public boolean isAggressiveStatusCheckEnabled() {
        JSONObject configJson = this.getCustomProp(this.RECHARGE_CUSTOM_CONFIG);
        
        if (configJson != null) {
            final String aggressiveStatusCheckEnabledString = (String) configJson.get(STATUS_CHECK_TYPE);
            
            if (StringUtils.isNotBlank(aggressiveStatusCheckEnabledString)) {
                return Boolean.valueOf(aggressiveStatusCheckEnabledString);
            }
        }
        
        return false;
    }

	public JSONObject getTestConditions() {
		JSONObject obj = getJsonValue(TEST_CONDITIONS);
        return obj!=null?obj:new JSONObject();
	}

	public void setTestConditions(JSONObject obj) {
		this.setJsonValue(TEST_CONDITIONS, obj);
	}

    public boolean isChildPropertyEnabled(String property, String childProperty){
        Boolean isEnabled = false;
        JSONObject config = this.getCustomProp(property);
        if(config.containsKey(childProperty)) {
            isEnabled = Boolean.parseBoolean(String.valueOf(config.get(childProperty)));
        }
        return isEnabled;
    }

    public String getChildProperty(String property, String childProperty){
        String customProp = null;
        JSONObject config = this.getCustomProp(property);
        if(config.containsKey(childProperty)) {
            customProp = String.valueOf(config.get(childProperty));
        }
        return customProp;
    }

    public String getChildProperty(JSONObject object, String childProperty){
        String customProp = null;
        if(object.containsKey(childProperty)) {
            customProp = String.valueOf(object.get(childProperty));
        }
        return customProp;
    }
    
    public String getWebAssetsVersion() {
        final String version = getStringValue(WEB_ASSETS_VERSION_NO);
        if (version == null) {
            return setWebAssetsVersion();
        } else {
            return version;
        }
    }

    public String setWebAssetsVersion() {
        final String newVersion = "" + System.nanoTime();
        setStringValue(WEB_ASSETS_VERSION_NO, newVersion);
        return newVersion;
    }

    public List<String> getChildPropertyAsList(String property, String childProperty, String delimiter){     
        String propertyValue = this.getChildProperty(property, childProperty);
        List<String> propertyList = new ArrayList<String> ();
        if(!FCUtil.isEmpty(propertyValue)) {
            propertyList = Arrays.asList(propertyValue.split(delimiter));
        }
        return propertyList;
    }

    public Map<String, Object> getCouponsConfig(){
        return this.getCustomProp(COUPON_CONFIG);
    }

    public String[] getUtilityProductsConfig(String key, String delimiter) {
        String[] productList = null;
        JSONObject config = null;
        try {
            config = this.getCustomProp(key);
        } catch(Exception e) {
            logger.error("Exception while fetching from app config: ",e);
        }
        String utilities = new Gson().toJson(config);
        HashMap<String,String> valuesMap = new Gson().fromJson(utilities, new TypeToken<HashMap<String, String>>(){}.getType());
        String values = valuesMap.get("values");
        logger.info("Utility products: "+values);
        if (!com.freecharge.util.FCUtil.isEmpty(values)) {
            productList = values.split(delimiter);
        }

        return productList;
    }

    public String[] getFCVpaList(String key, String delimiter) {
        String[] vpaList = null;
        JSONObject list = null;
        try {
            list = this.getCustomProp(key);
            String gson = new Gson().toJson(list);
            HashMap<String,String> vpasMap = new Gson().fromJson(gson, new TypeToken<HashMap<String, String>>(){}.getType());
            String vpas = !CollectionUtils.isEmpty(vpasMap) ? vpasMap.get("vpas") : null;
            logger.info("List of beneficiary VPAs : "+vpas);
            if(!FCUtil.isEmpty(vpas)) {
                vpaList = vpas.split(delimiter);
            }
        } catch (Exception e) {
            logger.error("Exception while fetching vpa list from app config ",e);
        }
        return vpaList;
    }

    public boolean isMarsEnabled(){
        return getBooleanValue(MARS_ENABLED);
    }

    public void setMarsEnabled(boolean value){
        setBooleanValue(MARS_ENABLED, value);
    }

    public boolean isIVRSEnabled() {
        return getBooleanValue(IVRS_ENABLED);
    }

    public void setIVRSEnabled(boolean value){
        setBooleanValue(IVRS_ENABLED, value);
    }

	public boolean isappGalEnabled() {
    	return getBooleanValue(APPGAL_ENABLED);
	}
	
    public void setAppgalEnabled(boolean appgalEnabled){
    	setBooleanValue(APPGAL_ENABLED, appgalEnabled);
    }

    public boolean isRechargeReminderEnabled() {
        return this.getBooleanValue(RECHARGE_REMINDER_ENABLED);
    }

	public boolean isUserMessageStoreEnabled() {
	    return this.getBooleanValue(USER_MESSAGE_STORE_ENBALED);
	}

    public void setUserMessageStoreEnabled(boolean userMessageStoreEnabled){
        setBooleanValue(USER_MESSAGE_STORE_ENBALED, userMessageStoreEnabled);
    }

    public void setRechargeReminderEnabled(boolean rechargeReminderEnabled) {
        setBooleanValue(RECHARGE_REMINDER_ENABLED, rechargeReminderEnabled);
    }

    public boolean isOperatorRetryAllErrorCodeEnabled() {
        return getBooleanValue(OPERATOR_RETRY_ALL_ERROR_CODES_ENABLED);
    }

    public void setOperatorRetryAllErrorCodeEnabled(boolean b) {
        setBooleanValue(OPERATOR_RETRY_ALL_ERROR_CODES_ENABLED, b);
    }

    public boolean isOperatorRetryOnlyNutsEnabled() {
        return getBooleanValue(OPERATOR_RETRY_ONLY_NUTS_ENABLED);
    }

    public void setOperatorRetryOnlyNutsEnabled(boolean b) {
        setBooleanValue(OPERATOR_RETRY_ONLY_NUTS_ENABLED, b);
    }

	public boolean isRandomCouponEnabled() {
		return getBooleanValue(CENTAUR_RANDOM_COUPON);
	}
	
	public void setRandomCouponEnabled(boolean b) {
		setBooleanValue(CENTAUR_RANDOM_COUPON, b);
	}

    public boolean isOneCheckFKEnabled() {
        return getBooleanValue(ONE_CHECK_FK_ENABLED);
    }
    
    public void setOneCheckFKEnabled(boolean b) {
        setBooleanValue(ONE_CHECK_FK_ENABLED, b);
    }
    
    public boolean isOneCheckWalletEnabled() {
        return getBooleanValue(ONE_CHECK_WALLET_ENABLED);
    }
    
    public void setOneCheckWalletEnabled(boolean b) {
        setBooleanValue(ONE_CHECK_WALLET_ENABLED, b);
    }
    
    public boolean isVCardCreateEnabled() {
    	return getBooleanValue(VCARD_CREATE_ENABLED);
    }

    public void setVCardCreateEnabled(boolean b) {
    	setBooleanValue(VCARD_CREATE_ENABLED, b);
    }

    public boolean isLoadAntiFraudPropsFromFileEnabled() {
		return getBooleanValue(LOAD_ANTI_FRAUD_PROPS_FROM_FILE);
	}
    
	public void setLoadAntiFraudPropsFromFileEnabled(boolean b) {
		setBooleanValue(LOAD_ANTI_FRAUD_PROPS_FROM_FILE, b);
	}

	public JSONObject getForceUpgradeDetails(){
        JSONObject obj = getJsonValue(FORCE_UPGRADE_DETAILS);
        return obj!=null?obj:new JSONObject();
    }

    public void setForceUpgradeDetails(JSONObject conditions){
        this.setJsonValue(FORCE_UPGRADE_DETAILS, conditions);
    }

	
	public boolean isDebitATMEnabled() {
        return getBooleanValue(DEBIT_ATM_ENABLED);
    }
    
    public void setDebitATMEnabled(boolean b) {
        setBooleanValue(DEBIT_ATM_ENABLED, b);
    }
    
    public int getWalletBalanceTimeout() {
        String key = "wallet.balance.timeout";
        int timeOut = 1000;
        try {
            Object value = appState.getAppValue(key);
            timeOut = ((value != null) && ((int)value > 0))? (int)value : timeOut;
        } catch(RuntimeException e) {
            logger.error(String.format("Error reading attribute '%s' from application state", key), e);
        }
        return timeOut > 0 ? timeOut : 1000;
    }
	
    public boolean isICICIQCEnabled() {
        return getBooleanValue(ICICI_QC_ENABLED);
    }
    
    public void setICICIQCEnabled(boolean b) {
        setBooleanValue(ICICI_QC_ENABLED, b);

    }


	public boolean isSMSGupshupHLREnabled() {
		return getBooleanValue(SMSGUPSHUP_HLR_ENABLED, getBooleanValue(SMSGUPSHUP_HLR_ENABLED));
	}

	public void setSMSGupshupHLREnabled(boolean flag) {
		setBooleanValue(SMSGUPSHUP_HLR_ENABLED, flag);
	}

    public boolean isCardStatusEnabled() {
        return getBooleanValue(CARD_STATUS_ENABLED);
    }
    
    public void setCardStatusEnabled(boolean b) {
        setBooleanValue(CARD_STATUS_ENABLED, b);
    }
    
    public void setNumberBlockingEnabled(boolean b){
    	setBooleanValue(INVALID_NUMBER_BLOCKING_ENABLED, b);
    }

	public boolean isInvalidNumberBlockingEnabled() {
		return getBooleanValue(INVALID_NUMBER_BLOCKING_ENABLED, getBooleanValue(INVALID_NUMBER_BLOCKING_ENABLED));
	}
	
	public void setTDPlanValidationEnabled(boolean b){
		setBooleanValue(TD_PLAN_VALIDATION_ENABLED, b);
	}
	
	public boolean isPlanValidationEnabled(){
		return getBooleanValue(TD_PLAN_VALIDATION_ENABLED,getBooleanValue(TD_PLAN_VALIDATION_ENABLED));
	}
	public void setRepeatRechargeBlock(boolean b){
		setBooleanValue(REPEAT_RECHARGE_BLOCK_ENABLED,b);
	}
	
	public boolean isRepeatRechargeBlockingEnabled(){
		return getBooleanValue(REPEAT_RECHARGE_BLOCK_ENABLED,getBooleanValue(REPEAT_RECHARGE_BLOCK_ENABLED));
	}
	
	public boolean useKlickpayForNBDowntimeAPI(){
	    return getBooleanValue(KLICKPAY_NB_API_DOWNTIME,getBooleanValue(KLICKPAY_NB_API_DOWNTIME));
	}
	
	public void setKlickpayForNBDowntimeAPI(boolean b){
	    setBooleanValue(KLICKPAY_NB_API_DOWNTIME, b);
	}
	
	public boolean useKlickpayForDowntimeAPI(){
        return getBooleanValue(KLICKPAY_API_DOWNTIME,getBooleanValue(KLICKPAY_API_DOWNTIME));
    }
	
	public void setKlickpayForDowntimeAPI(boolean b){
        setBooleanValue(KLICKPAY_API_DOWNTIME, b);
    }
	
	public void setKlickPayS2SEnabled(boolean enabled) {
		setBooleanValue(KLICKPAY_S2S_ENABLED, enabled);
	}

	public boolean isKlickPayS2SEnabled() {
       return this.getBooleanValue(KLICKPAY_S2S_ENABLED);
	}

    public boolean isAlwaysRefundOnDelayedPaymentSuccess() {
        return this.getBooleanValue(ALWAYS_REFUND_ON_DELAYED_PAYMENT_SUCCESS);
    }
    
    public void setAlwaysRefundOnDelayedPaymentSuccess(boolean enabled) {
        setBooleanValue(ALWAYS_REFUND_ON_DELAYED_PAYMENT_SUCCESS, enabled);
    }
    
    public boolean isNullToFailureRefundCommunicationEnabled() {
        return this.getBooleanValue(NULL_FAILURE_REFUND_COMMUNICATION_ENABLED);
    }
    
    public void setNullToFailureRefundCommunicationEnabled(boolean enabled) {
        setBooleanValue(NULL_FAILURE_REFUND_COMMUNICATION_ENABLED, enabled);
    }
    
    public boolean isNullToSuccessRefundCommunicationEnabled() {
        return this.getBooleanValue(NULL_SUCCESS_REFUND_COMMUNICATION_ENABLED);
    }
    
    public void setNullToSuccessRefundCommunicationEnabled(boolean enabled) {
        setBooleanValue(NULL_SUCCESS_REFUND_COMMUNICATION_ENABLED, enabled);
    }
	
	public void setCreateBillV2Enabled(boolean b) {
		setBooleanValue(CREATE_BILL_V2_ENABLED, b);
	}

	public boolean isCreateBillV2Enabled() {
		return getBooleanValue(CREATE_BILL_V2_ENABLED, getBooleanValue(CREATE_BILL_V2_ENABLED));
	}

	public void setFetcBillSupportPostpaidEnabled(boolean b) {
		setBooleanValue(POSTPAID_FETCHBILL_ENABLED, b);
	}
	
	public boolean isPostPaidFetchBillSupportEnabled() {
		return getBooleanValue(POSTPAID_FETCHBILL_ENABLED, getBooleanValue(POSTPAID_FETCHBILL_ENABLED));
	}
	
    public boolean isCentaurEnabledForABTesting() {
        return this.getBooleanValue(CENTAUR_ENABLED);
    }

    public void setCentaurEnabled(boolean enabled) {
        setBooleanValue(CENTAUR_ENABLED, enabled);
    }

    public boolean isNewFailureFulfilmentEnabled() {
        return this.getBooleanValue(NEW_FAILURE_FULFILMENT_ENABLED);
    }

    public void setNewFailureFulfilmentEnabled(boolean enabled) {
        setBooleanValue(NEW_FAILURE_FULFILMENT_ENABLED, enabled);
    }


    public void setDTHNumberValidationEnabled(boolean b){
		setBooleanValue(DTH_NUMBER_VALIDATION_ENABLED, b);
	}
    
	public boolean isDTHNumberValidationEnabled() {
		return getBooleanValue(DTH_NUMBER_VALIDATION_ENABLED,getBooleanValue(DTH_NUMBER_VALIDATION_ENABLED));
	}
	public boolean isDBUpdateOnCallBackEnabled() {
		return getBooleanValue(DB_UPDATE_ON_CALLBACK_ENABLED, getBooleanValue(DB_UPDATE_ON_CALLBACK_ENABLED));
	}
	
	public void setDBUpdateOnCallBackEnabled(boolean enabled) {
		setBooleanValue(DB_UPDATE_ON_CALLBACK_ENABLED, enabled);
	}
	
	public void setInvalidPlansDenominationValidationEnabled(boolean b){
		setBooleanValue(INVALID_PLANS_DENOMINATION_VALIDATION_ENABLED, b);
	}
    
	public boolean isInvalidPlansDenominationValidationEnabled() {
		return getBooleanValue(INVALID_PLANS_DENOMINATION_VALIDATION_ENABLED,getBooleanValue(INVALID_PLANS_DENOMINATION_VALIDATION_ENABLED));
	}
	
	public boolean isDataCardCircleBlockingEnabled(){
		return getBooleanValue(DATA_CARD_OPERATOR_CIRCLE_BLOCKING_ENABLED,getBooleanValue(DATA_CARD_OPERATOR_CIRCLE_BLOCKING_ENABLED));
	}
	
	public void setDataCardCircleBlockingEnabled(boolean b){
		setBooleanValue(DATA_CARD_OPERATOR_CIRCLE_BLOCKING_ENABLED, b);
	}
	
	public boolean setMenuOptionsValue(String menuOptionConfigValue){
		JSONParser parser = new JSONParser();
		try {
			setCustomProp(MENU_OPTIONS_CONFIG,(JSONObject)parser.parse(menuOptionConfigValue));
		} catch (ParseException e) {
			logger.error("MenuOption parsing failed",e);
			return false;
		}
		return true;
	}

	public void setInTransactionValidationEnabled(boolean enabled){
		setBooleanValue(INTRANSACTION_PLAN_VALIDATION_ENABLED, enabled);
	}
	
	public boolean isInTransactionValidationEnabled() {
		return getBooleanValue(INTRANSACTION_PLAN_VALIDATION_ENABLED,
				getBooleanValue(INTRANSACTION_PLAN_VALIDATION_ENABLED));
	}

	public boolean isAutoPayEnabled() {
		return getBooleanValue(AUTO_PAY_ENABLED, getBooleanValue(AUTO_PAY_ENABLED));
	}
	
	public void setAutoPayEnabled(boolean enabled) {
		setBooleanValue(AUTO_PAY_ENABLED, enabled);
	}
	
	public boolean isAutoPayEnabledForUtility() {
		return getBooleanValue(AUTO_PAY_ENABLED_FOR_UTILITY, getBooleanValue(AUTO_PAY_ENABLED_FOR_UTILITY));
	}
	
	public void setAutoPayEnabledForUtility(boolean enabled) {
		setBooleanValue(AUTO_PAY_ENABLED_FOR_UTILITY, enabled);
	}
	
	public void setFulfillBillNewEnabled(boolean enabled){
		setBooleanValue(FULFILLBILL_NEW_ENABLED, enabled);
	}
	
	public boolean isFulfillNewEnabled(){
		return getBooleanValue(FULFILLBILL_NEW_ENABLED, getBooleanValue(FULFILLBILL_NEW_ENABLED));
	}

	public boolean isFetchBillNewEnabled() {
		return getBooleanValue(FETCHBILL_NEW_ENABLED, getBooleanValue(FETCHBILL_NEW_ENABLED));
	}

	public void setFetchBillNewEnabled(boolean enabled) {
		setBooleanValue(FETCHBILL_NEW_ENABLED, enabled);
	}
	
	public boolean isPlanLevelRetryEnabled() {
		return getBooleanValue(PLAN_LEVEL_RETRY_ENABLED, getBooleanValue(PLAN_LEVEL_RETRY_ENABLED));
	}
	 
	public void setPlanLevelRetryEnabled(boolean b) {
		setBooleanValue(PLAN_LEVEL_RETRY_ENABLED, b);
	}
	
	public boolean isEuronetNewErrorCodeEnabled() {
		return getBooleanValue(EURONET_NEW_ERROR_CODE_ENABLED, getBooleanValue(EURONET_NEW_ERROR_CODE_ENABLED));
	}
	 
	public void setEuronetNewErrorCodeEnabled(boolean b) {
		setBooleanValue(EURONET_NEW_ERROR_CODE_ENABLED, b);
	}
	
	public boolean isBillpayOperatorChannelBlockingEnabled() {
		return getBooleanValue(BILLPAY_OPERATOR_CHANNEL_BLOCKING_ENABLED, getBooleanValue(BILLPAY_OPERATOR_CHANNEL_BLOCKING_ENABLED));
	}
	
	public void setBillpayOperatorChannelBlockingEnabled(boolean b){
    	setBooleanValue(BILLPAY_OPERATOR_CHANNEL_BLOCKING_ENABLED, b);
    }

	public boolean isDTHPlanValidationEnabled() {
		return getBooleanValue(DTH_PLAN_VALIDATION_ENABLED, getBooleanValue(DTH_PLAN_VALIDATION_ENABLED));
	}
	
	public void setDTHPlanValidationEnabled(boolean b) {
		setBooleanValue(DTH_PLAN_VALIDATION_ENABLED, b);
	}
	
	public boolean isJIONumberValidationEnabled() {
		return getBooleanValue(JIO_NUMBER_VALIDATION_ENABLED, getBooleanValue(JIO_NUMBER_VALIDATION_ENABLED));
	}
	
	public void setJIONumberValidationEnabled(boolean b) {
		setBooleanValue(JIO_NUMBER_VALIDATION_ENABLED, b);
	}

	public String getAppDeepLinkSiteAssociation(String channel) {
		return getChildProperty(APP_DEEP_LINK_SITE_ASSOCIATION, channel);
	}
}
