package com.freecharge.common.framework.properties;

import java.io.IOException;
import java.io.NotSerializableException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.freecharge.api.coupon.service.APIAuthPolicy;
import com.freecharge.app.service.AppContextManager;
import com.freecharge.ccsclient.AbstractCCSConfig;
import com.freecharge.ccsclient.CCS;
import com.freecharge.ccsclient.ICCS;
import com.freecharge.ccsclient.ICCSConfig;
import com.freecharge.ccsclient.exception.CCSException;
import com.freecharge.ccsclient.exception.ZookeeperDownException;
import com.freecharge.common.amazon.s3.service.AmazonKMSService;
import com.freecharge.common.framework.exception.FCPropertiesException;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCStringUtils;
import com.freecharge.payment.autorefund.RefundUtil;
import com.freecharge.util.FCUtil;


/**
 * Class for managing properties from properties files
 */
public class FCProperties implements ResourceLoaderAware {

    public static final String CCS_HOST = "ccs.host.url";
    private static final String CCS_INIT_ENABLED = "ccs.init.enabled";
    private static final String CCS_ANTIFRAUD_BASEATTACHPATH = "ccs.antifraud.baseAttachPath";
    private static final String CCS_BASEPATH_PAYMENT = "ccs.payments.baseAttachPath";
    private static final String CCS_BASEPATH_FORCE_REFUND = "ccs.forcerefunds.baseAttachPath";
    private static final String CCS_BASEPATH_FULFILMENT ="ccs.fulfillment.baseAttachPath";
    
    private static final String AGGREGATOR_IP = "aggregator.host.ip";
    private static final String AGGREGATOR_PORT = "aggregator.port";
    private static final String AGGREGATOR_TIMEOUT = "aggregator.timeout";
    private static final String AGGREGATOR_CLIENT_ID = "aggregator.client.id";
    private static final String AGGREGATOR_CLIENT_KEY = "aggregator.client.key";
    private static final String AGGREGATOR_MERCHANT_ID = "aggregator.merchant.id";
    
    private static final String ENABLED_CONSUMERS = "active.consumers";
    
    private static final String CCS_ICICI_QC_LIST = "quick.checkout.products";
    private static final String CCS_DEBIT_ATM_LIST = "debit.atmpin.products";
    private static final String CCS_ITTP_PRODUCTS_LIST = "ittp.products";

    private static final String APP_CONFIG_BEAN = "appConfigService";
    
    public static final String  ATM_AFFILIATE_COUPON_IDS                             = "atm.affiliate.coupon.ids";

    public static final String  IN_TRANS_ID_PREFIX_KEY                               = "in.trans.id.prefix";
    public static final String  EMAIL_FROM_NAME                                      = "email.from.name";

    public static final String  WALLET_FEATURE_UNDER_MAINTAINENCE                    = "wallet.feature.under.maintainence";
    public static final String  SESSION_EXPIRE_TIME                                  = "session.expire.time";
    public static final String  WALLET_PAY_ENABLED                                   = "wallet.pay.enabled";
    public static final String  WALLET_DATA_DISPLAY_ENABLED                          = "wallet.data.display.enabled";

    public static final String  ZEROPAY_FEATURE_UNDER_MAINTAINENCE                   = "zeropay.feature.under.maintainence";
    public static final String  ZEROPAY_PAY_ENABLED                                  = "zeropay.enabled";
    public static final String  ZEROPAY_DATA_DISPLAY_ENABLED                         = "zeropay.data.display.enabled";

    public static final String  IMAGE_PREFIX_KEY                                     = "imgprefix1";

    public static final String  CSRF_CHECK_ENABLED                                   = "csrf.check.enable";

    public static final String  CAMPAIGN_START_DATE                                  = "campaign.start.date";
    public static final String  CAMPAIGN_END_DATE                                    = "campaign.end.date";

    public static final String  COUPON_CAMPAIGN_START_DATE                           = "coupon.email.campaign.start.date";
    public static final String  COUPON_CAMPAIGN_END_DATE                             = "coupon.email.campaign.end.date";
    public static final String  NEW_COUPON_TIME_WINDOW                               = "new.coupon.time.window";
    public static final String  POPULAR_COUPON_COUNT                                 = "popular.coupon.count";

    public static final String  MOBILE_SITE_INTERCEPTOR_KEY                          = "mobile.site.interceptors";

    public static final String  VERSION_NO                                           = "version.no";
    public static final String  REWARD_SMS_SENDER                                    = "reward.sms.sender";
    public static final String  BILLDESK_CHECKSUM_KEY                                = "billdesk.checksum.key";

    // General Failure Messages
    public static final String  MSG_TECHNICAL_ISSUE                                  = "msg.technical.issue";
    public static final String  MSG_MAX_LIMIT_REACHED_CONTACT_CS                     = "msg.max.limit.reached.contact.cs";
    public static final String  MSG_SERVER_BUSY                                      = "msg.server.busy";
    public static final String  MSG_UNKNOWN_ERROR                                    = "msg.something.wrong.try.later";
    public static final String  MSG_PROMOCODE_DOWN                                   = "msg.promocode.down";
    public static final String  MSG_INVALID_PROMOCDOE                                = "msg.invalid.promocode";
    public static final String  MSG_INCORRECT_CAPTCHA                                = "msg.incorrect.captcha";
    public static final String  MSG_CODE_EXPIRED                                     = "msg.code.expired";
    public static final String  MSG_CODE_ALREADY_USED                                = "msg.code.already.used";
    public static final String  MSG_CODE_BLOCKED                                     = "msg.code.blocked";
    public static final String  MSG_OFFER_MAX_LIMIT_REACHED                          = "msg.offer.max.limit.reached";
    public static final String  INVALID_ANDROID_APP_VERSION                          = "msg.invalid.android.app.version";
    public static final String  MSG_CODE_NOT_REMOVED                                 = "msg.code.notremoved";
    public static final String  MSG_CODE_REMOVED                                     = "msg.code.removed";
    public static final String  MSG_NO_CODE_TO_REMOVE                                = "msg.no.code.to.remove";

    // MongoDB Settings
    public static final String  SPRING_MONGO_DB_HOSTS                                = "spring.mongo.db.hosts";
    public static final String  MONGO_DB_HOSTS                                       = "mongo.db.hosts";
    public static final String  MONGO_DB_NAME                                        = "mongo.db.name";
    public static final String  MONGO_POOLSIZE                                       = "mongo.db.poolsize";
    public static final String  MONGO_USERNAME                                       = "mongo.db.username";
    public static final String  MONGO_PASSWORD                                       = "mongo.db.password";

    // MongoSessionDB Settings
    public static final String  MONGO_SESSION_DB_HOSTS                               = "mongo.session.db.hosts";
    public static final String  MONGO_SESSION_DB_NAME                                = "mongo.session.db.name";
    public static final String  MONGO_SESSION_POOLSIZE                               = "mongo.session.db.poolsize";
    public static final String  MONGO_SESSION_USERNAME                               = "mongo.session.db.username";
    public static final String  MONGO_SESSION_PASSWORD                               = "mongo.session.db.password";

    // Google auth redirect uris
    public static final String  GOOGLE_REDIRECT_ADMIN_URI                            = "google.admin.redirect.uri";
    public static final String  GOOGLE_LOGIN_REDIRECT_URI                            = "google.login.redirect.uri";

    public static final String  REDIS_TO_MONGO_PHASE                                 = "redis.to.mongo.phase";

    public static final String  FREECHARGE_URL                                       = "freecharge.url";
    public static final String  MSG_FB_FRAUD                                         = "msg.fb.fraud";

    // SMS ACL
    public static final String  SMSACL_ENABLED                                       = "smsacl.enabled";
    public static final String  SMSACL_SMSURL                                        = "smsacl.smsurl";
    public static final String  SMSACL_MULTISMSURL                                   = "smsacl.multismsurl";
    public static final String  SMSACL_MULTISMSSIZE                                  = "smsacl.multismssize";
    public static final String  SMSACL_USERID                                        = "smsacl.userid";
    public static final String  SMSACL_PASS                                          = "smsacl.pass";
    public static final String  SMSACL_APPID                                         = "smsacl.appid";
    public static final String  SMSACL_SUBAPPID                                      = "smsacl.subappid";
    public static final String  SMSACL_FROM                                          = "smsacl.from";

    // AWS Key Settings
    public static final String  AWS_SQS_ACCESS_KEY                                   = "aws.sqs.access.key";
    public static final String  AWS_SQS_SECRET_KEY                                   = "aws.sqs.secret.key";
    public static final String  AWS_SQS_URL                                          = "aws.sqs.url";
    public static final String  AWS_SQS_PRINCIPAL                                    = "aws.sqs.principal";
    public static final String  DID_MAIN_QUEUE                                       = "did.main.queue";
    public static final String  DID_SMS_QUEUE                                        = "did.sms.queue";

    public static final String  AWS_ACCESS_KEY                                       = "aws.access.key";
    public static final String  AWS_SECRET_KEY                                       = "aws.secret.key";
    public static final String  LOG_TRACKING_EVENTS                                  = "log.tracking.events";

    public static final String  YOPTIMA_ENABLED                                      = "yoptima.enabled";

    public static final String  GOOGLE_CODE_ENABLED                                  = "google.code.enabled";

    public static final String  FACEBOOK_CODE_ENABLED                                = "facebook.code.enabled";

    // AWS SNS Settings

    public static final String AWS_SNS_ACCESS_KEY                       = "aws.sns.access.key";
    public static final String AWS_SNS_SECRET_KEY                       = "aws.sns.secret.key";
    public static final String AWS_SNS_REGION                           = "aws.sns.region";
    public static final String AWS_SNS_REGION_NEW                       = "aws.sns.region.new";
    public static final String AWS_SNS_QA_REGION                        = "aws.sns.region.qa";
    public static final String AWS_SNS_TOPIC_ARN_RECHARGE               = "aws.sns.topic.arn.recharge";
    public static final String  AWS_SNS_TOPIC_ARN_RECHARGE_ATTEMPT                   = "aws.sns.topic.arn.recharge.attempt";
    public static final String AWS_SNS_TOPIC_ARN_PAYMENT_RETRY = "aws.sns.topic.arn.payment.retry";

    public static final String AWS_SNS_TOPIC_ARN_PAYMENT                = "aws.sns.topic.arn.payment";
    public static final String AWS_SNS_TOPIC_ARN_LOCATION                = "aws.sns.topic.arn.location";
    public static final String AWS_SNS_TOPIC_ARN_CAMPAIGN                = "aws.sns.topic.arn.campaign";
    public static final String AWS_SNS_TOPIC_ARN_CAMPAIGN_PROMO         = "aws.sns.topic.arn.campaign.promo";
    public static final String AWS_SNS_TOPIC_ARN_EMAIL_ID               = "aws.sns.topic.arn.emailid";
    public static final String AWS_SNS_TOPIC_ARN_PROFILE_NO             = "aws.sns.topic.arn.profileno";
    public static final String AWS_SNS_TOPIC_ARN_RECHARGE_NO            = "aws.sns.topic.arn.rechargeno";
    public static final String AWS_SNS_TOPIC_ARN_IMEI                   = "aws.sns.topic.arn.imei";
    public static final String AWS_SNS_TOPIC_ARN_DEVICE_ID              = "aws.sns.topic.arn.deviceid";
    public static final String AWS_SNS_TOPIC_ARN_PAYMENT_ATTEMPT        = "aws.sns.topic.arn.payment.attempt";
    public static final String AWS_SNS_TOPIC_ARN_REFUND                 = "aws.sns.topic.arn.refund";
    public static final String AWS_SNS_TOPIC_RECHARGE_PLAN              = "aws.sns.topic.arn.recharge.plan";
    private static final String AWS_SNS_TOPIC_ARN_WALLET_TXN            = "aws.sns.topic.arn.wallet.txn";
    private static final String AWS_SNS_TOPIC_ARN_PROMOCODE_REDEEM_DETAILS = "aws.sns.topic.arn.promocode.redeem.details";
    private static final String AWS_SNS_TOPIC_ARN_CREDIT_REWARD_SUCCESS_DETAILS = "aws.sns.topic.arn.credit.reward.success.details";
    private static final String AWS_SNS_TOPIC_ARN_REFUND_ATTEMPT_ALERT           = "aws.sns.topic.arn.refund.attempt.alert";
    private static final String AWS_SNS_TOPIC_ARN_REFUND_STATUS_ALERT            = "aws.sns.topic.arn.refund.status.alert";
    private static final String AWS_SNS_TOPIC_ARN_REVOKE_FUND_ALERT             = "aws.sns.topic.arn.revoke.fund.alert";
    private static final String AWS_SNS_TOPIC_INVALID_DENOMINATION_PLANS		= "aws.sns.topic.arn.invalid.denominations.plan";
    public static final String SITE_VERSION_NEW                         = "site.version.new";
    public static final String TRAFFIC_SPLIT_ENABLED                    = "traffic.split.enabled";

    public static final String  REGISTRATION_SUCCESS_MSG                             = "registration.success.msg";
    public static final String  REGISTRATION_FAILED_MSG                              = "registration.failed.msg";
    public static final String  REGISTRATION_FROM_EMAILID                            = "fc.registration.from.emailid";
    public static final String  REGISTRATION_SUBJECT                                 = "fc.registration.sub";
    public static final String  REGISTRATION_REPLYTO                                 = "fc.registration.replyto";
    public static final String  REGISTRATION_FAILED_EMPTY_REQUEST                    = "registration.failed.empty.request";
    public static final String  REGISTRATION_FAILED_EMPTY_PASSWORD                   = "registration.failed.empty.password";
    public static final String  REGISTRATION_FAILED_WRONG_PASSWORD_LENGTH            = "registration.failed.wrong.password.length";
    public static final String  REGISTRATION_FAILED_EMPTY_CONF_PASSWORD              = "registration.failed.empty.confirmation.password";
    public static final String  REGISTRATION_FAILED_EMPTY_EMAIL                      = "registration.failed.empty.email";
    public static final String  REGISTRATION_FAILED_INVALID_EMAIL                    = "registration.failed.invalid.email";
    public static final String  REGISTRATION_FAILED_INVALID_EMAIL_FORMAT             = "registration.failed.invalid.email.format";
    public static final String  REGISTRATION_FAILED_INCORRECT_DOMAIN                 = "registration.failed.incorrect.domain";
    public static final String  REGISTRATION_FAILED_DOMAIN_SPELLED_WRONG             = "registration.failed.domain.spelled.wrong";
    public static final String  REGISTRATION_FAILED_PASSWORD_MISMATCH                = "registration.failed.password.mismatch";
    public static final String  REGISTRATION_FAILED_ALREADY_LOGGEDIN                 = "registration.failed.already.loggedin";
    public static final String  REGISTRATION_FAILED_EXCEPTION                        = "registration.failed.exception";
    public static final String  REGISTRATION_FAILED_INVALID_NAME                     = "registration.failed.invalid.name";
    
    // Euronet Airtel wallet store code
    public static final String  EURONET_AIRTEL_STORE_CODE                            = "euronet.airtel.storecode";
    public static final String  EURONET_AIRTEL_USERNAME                              = "euronet.airtel.username";
    public static final String  EURONET_AIRTEL_USERPASS                              = "euronet.airtel.userpass";
    public static final String  EURONET_AIRTEL_TERMINAL_CODE                         = "euronet.airtel.terminalcode";

    public static final String  MSG_APP_DOWNLOAD                                     = "msg.app.download";

    public static final String  ONE_CHECK_SERVICE_HOST_URL                           = "one.check.service.host.url";
    public static final String  ONE_CHECK_SERVICE_CLIENT_NAME                        = "one.check.service.client.name";
    public static final String  ONE_CHECK_SERVICE_CLIENT_KEY                         = "one.check.service.client.key";
    public static final String  ONE_CHECK_BUSINESS_ENTITY                            = "one.check.business.entity";
    public static final String  ONE_CHECK_CONNECTION_TIMEOUT                         = "one.check.connection.timeout";
    public static final String  ONE_CHECK_CONNECTION_REQUEST_TIMEOUT                 = "one.check.connection.request.timeout";
    public static final String  ONE_CHECK_SOCKET_TIMEOUT                             = "one.check.socket.timeout";
    public static final String  ONE_CHECK_WALLET_ENABLED                             = "one.check.wallet.enabled";
    public static final String  WALLET_API_TIMEOUT                                   = "wallet.api.timeout";
    public static final String  ADMIN_WALLET_API_TIMEOUT                          	 = "admin.wallet.api.timeout";

    public static final String  NEW_REFUND_URI                                       = "new.refund.uri";
    public static final String  CAMPAIGN_SERVICE_URI                          	 	 = "campaign.service.uri";
    public static final String  REFERRAL_SERVICE_URI                          	 	 = "referral.service.uri";
    public static final String  LIFELINE_SERVICE_URI                          	 	 = "points.lifeline.service.uri";
    public static final String INTERIM_CLIENT_KEY                                    = "interim.client.key";
    public static final String INTERIM_CLIENT_ID                                     = "interim.client.id";

    public static final String  IMS_CLIENT_URL                                       = "ims.client.url";
    public static final String  IMS_CLIENT_PORT                                      = "ims.client.port";
    public static final String  IMS_CLIENT_ID                                        = "ims.client.id";
    public static final String  IMS_CLIENT_KEY                                       = "ims.client.key";
    public static final String  IMS_CLIENT_TIMEOUT                                   = "ims.client.timeout";
    public static final String  IMS_SLAVE_URL                                        = "ims.client.slaveUrl";
    public static final String  IMS_SLAVE_PORT                                       = "ims.client.slavePort";

    private static final String CONSUMER_SQS_ENDPOINT                                = "consumer.sqs.queue.endpoint";
    private static final String CONSUMER_SQS_MUMBAI_ENDPOINT                         = "consumer.sqs.queue.mumbai.endpoint";
    private static final String SQS_POLL_SLEEP_PERIOD_SECONDS                        = "consumer.sqs.poll.sleepPeriodSeconds";

    private static final String REDIS_SESSION_ENABLED                                = "redis.session.enabled";

    private static final String MONGO_SESSION_ENABLED                                = "mongo.session.enabled";

    private static final String REDIS_CACHE_ENABLED                                  = "redis.cache.enabled";

    private static final String MOCK_MONGO                                           = "mock.mongo";

    private static final String MOCK_SESSION_ENABLED                                 = "mock.session.enabled";

    private static final String MOCK_CACHE_ENABLED                                   = "mock.cache.enabled";
    private static final String CASH_BACK_OFFER_CODE                                 = "cash.back.offer.code";
    private static final String PAYU_CASH_BACK_OFFER_KEY                             = "payu.cash.back.offer.key";
    private static final String CASH_BACK_OFFER_AMOUNT                               = "cash.back.offer.amount";
    private static final String CASH_BACK_OFFER_MINIMUM_RECHARGE_AMOUNT              = "cash.back.offer.minimum.recharge.amount";
    private static final String CASH_BACK_OFFER_LIMIT                                = "cash.back.offer.limit";

    private static final String COOKIE_HTTPS_ONLY                                    = "cookie.https.only";

    private static final String CONSUMER_FRAMEWORK_TELNET_PORT                       = "consumer.framework.telnet.port";

    // Queue names
    private static final String EMAIL_QUEUE                                          = "email.consumer.queue.name";
    private static final String RECHARGE_QUEUE                                       = "recharge.consumer.queue.name";
    private static final String RECHARGE_REWARD_QUEUE                                = "recharge.reward.consumer.queue.name";
    private static final String RECHARGE_REWARD_RESPONSE_QUEUE                       = "recharge.reward.response.queue.name";
    private static final String HCOUPON_QUEUE                                        = "hcoupon.consumer.queue.name";

    private static final String RECHARGE_RETRY_QUEUE                                 = "recharge.retry.consumer.queue.name";
    private static final String RECHARGE_RETRY_STATUS_SCAN_INTERVAL                  = "recharge.retry.status.scan.interval";

    private static final String PAY_SYNC_QUEUE                                       = "paySync.consumer.queue.name";
    private static final String CARD_BIN_QUEUE                                       = "cardBin.consumer.queue.name";
    private static final String POSTPAID_BILL_PAYMENT                                = "bill.payment.consumer.queue.name";
    private static final String BILL_PAYMENT_QUEUE                                   = "bill.payment.generic.consumer.queue.name";
    private static final String FULFILLMENT_QUEUE                                    = "fulfillment.consumer.queue.name";
    private static final String ANTIFRAUD_DATA_COLLECTOR_QUEUE                       = "antifrauddatacollector.consumer.queue.name";
    private static final String REFUND_QUEUE                                         = "refund.consumer.queue.name";
    private static final String NEW_REFUND_QUEUE                                     = "new.refund.consumer.queue.name";
    private static final String FAILURE_FULFILMENT_QUEUE                             = "failure.fulfilment.consumer.queue.name";
    private static final String UP_UPDATE_QUEUE                                      = "up.update.consumer.queue.name";
    private static final String BILL_PAYMENT_REFUND_QUEUE                            = "bill.payment.refund.consumer.queue.name";
    private static final String RECON_QUEUE                                          = "recon.consumer.queue.name";
    private static final String LEAD_QUEUE                                           = "lead.consumer.queue.name";
    private static final String TEST_QUEUE                                           = "test.consumer.queue.name";
    private static final String STATUS_CHECK_QUEUE                                   = "statuscheck.consumer.queue.name";
    private static final String QUICK_STATUS_CHECK_QUEUE                             = "quick.statuscheck.consumer.queue.name";
    private static final String BILL_PAY_STATUS_CHECK_QUEUE                          = "billpay.statuscheck.consumer.queue.name";
    private static final String POSTPAID_STATUS_CHECK_QUEUE                          = "postpaid.statuscheck.consumer.queue.name";
    private static final String NOTIFICATION_QUEUE                                   = "notification.consumer.queue.name";
    private static final String REWARD_QUEUE                                         = "reward.consumer.queue.name";
    private static final String FREEFUND_QUEUE                                       = "freefund.consumer.queue.name";
    private static final String IVRS_QUEUE                                           = "ivrs.consumer.queue.name";

    private static final String RECHARGE_DENOM_INTERVAL                              = "recharge.denomination.job.window.hours";
    private static final String RECHARGE_DENOM_MIN_ATTEMPTS                          = "recharge.denomination.job.min.attempts";
    private static final String RECHARGE_DENOM_MIN_SUCCESS_RATE                      = "recharge.denomination.job.min.successrate";
    private static final String RECHARGE_DENOM_MAX_DAYS                              = "recharge.denomination.job.max.days";
    private static final String RECHARGE_DENOM_RECOMMEND_PERCENT                     = "recharge.denomination.recommend.percent";
    private static final String RECHARGE_DENOM_DECAY_RATE                            = "recharge.denomination.decay.rate";

    // invalid denomination knobs
    private static final String DENOMINATION_BENCHMARK_COUNT                         = "recharge.denomination.benchmark.count";
    private static final String DENOMINATION_BENCHMARK_SUCCESS_PERCENT               = "recharge.denomination.benchmark.successpercent";

    private static final String DATACARD_DENOMINATION_BENCHMARK_COUNT                = "datacard.denomination.benchmark.count";
    private static final String DATACARD_DENOMINATION_BENCHMARK_SUCCESS_PERCENT      = "datacard.denomination.benchmark.successpercent";

    private static final String INVALID_DENOMINCATION_CHECK_DATA_WINDOW_DAYS         = "invalid.denomincation.check.data.window.days";
    private static final String AIRTEL_INVALID_DENOMINCATION_CHECK_DATA_WINDOW_DAYS  = "airtel.invalid.denomincation.check.data.window.days";

    private static final String BSNL_INVALID_DENOMINCATION_CHECK_DATA_WINDOW_DAYS    = "bsnl.invalid.denomincation.check.data.window.days";
    private static final String BSNL_MAX_FAILURE_THRESHOLD                           = "bsnl.invalid.denomination.failure.threshold";
    private static final String BSNL_DENOMINATION_BENCHMARK_FAILURE_PERCENT          = "bsnl.denomination.benchmark.failurepercent";

    private static final String AIRTEL_MAX_FAILURE_THRESHOLD                         = "airtel.invalid.denomination.failure.threshold";
    private static final String AIRTEL_MAX_FAILURE_PULS_SUCCESS_THRESHOLD            = "airtel.invalid.denomination.success.failure.threshold";
    private static final String AIRTEL_DENOMINATION_BENCHMARK_FAILURE_PERCENT        = "airtel.denomination.benchmark.failurepercent";
    private static final String INVALID_DENOMINCATION_CHECK_SPECIAL_DATA_WINDOW_DAYS = "invalid.denomincation.check.special.data.window.days";
    
    private static final String ALL_DENOMINATION_DATA_WINDOW 						 = "all.denomination.data.window";

    private static final String KESTREL_TIMEOUT                                      = "kestrel.queue.timeout";

    private static final String VISIT_TRACKING_COOKIE_NAME                           = "visit.tracking.cookie.name";
    private static final String VISIT_TRACKING_COOKIE_INTERVAL                       = "visit.tracking.cookie.interval";

    private static final String CLIENT_TRACKING_COOKIE_NAME                          = "client.tracking.cookie.name";
    private static final String CLIENT_TRACKING_COOKIE_INTERVAL                      = "client.tracking.cookie.interval";

    private static final String MAX_PROMOCODE_APPLY_COUNT                            = "max.promocode.class.apply.cookie.count";
    private static final String PROMOCODE_REWARD_CLASS_NAME                          = "promocode.reward.class.name";
    private static final String PROMOCODE_REWARD_CONDITION_NAME                      = "promocode.reward.condition.name";
    private static final String FREEFUND_USAGE_HISTORY_SIZE                          = "freefund.usage.history.size";

    private static final String PVR_COUPON_STORE_ID                                  = "pvr.coupon.store.id";

    private static final String OXIGEN_BALANCE_WARN_THRESHOLD                        = "oxigen.balance.threshold.warn";
    private static final String OXIGEN_BALANCE_CRITICAL_THRESHOLD                    = "oxigen.balance.threshold.critical";

    private static final String EURONET_BALANCE_WARN_THRESHOLD     = "euronet.balance.threshold.warn";
    private static final String EURONET_BALANCE_CRITICAL_THRESHOLD = "euronet.balance.threshold.critical";

    private static final String OPERATOR_METRIC_ALERT_ENABLED = "operator.metric.alert.enable";

    private static final String COUPONS_PRICING_ENABLED = "coupon.pricing.enabled";
    // User Agent Tracking
    private static final String USER_AGENT_TRACKING     = "user.agent.tracking";

    // Customer support email address
    private static final String CUSTOMER_SUPPORT_EMAIL_ADDRESS       = "recharge.success.replyto";
    private static final String JUSPAY_PAYMENT_STATS_URL             = "juspay.consumer.payment.stats.url";
    private static final String JUSPAY_PAYMENT_STATS_CONNECT_TIMEOUT = "juspay.consumer.pyament.connection.timeout";
    private static final String JUSPAY_PAYMENT_STATS_READ_TIMEOUT    = "juspay.consumer.pyament.read.timeout";

    // Consumer AWS credential keys
    private static final String CONSUMER_AWS_ACCESS_KEY    = "sqs.access.key";
    private static final String CONSUMER_AWS_ACCESS_SECRET = "sqs.access.secret";

    // Email-App integration
    private static final String EMAIL_APP_AWS_ACCESS_KEY             = "email.app.aws.access.key";
    private static final String EMAIL_APP_AWS_ACCESS_SECRET          = "email.app.aws.access.secret";
    private static final String EMAIL_APP_AWS_ENDPOINT               = "email.app.aws.endpoint";
    private static final String EMAIL_APP_SQS_URL_PREFIX             = "email.app.sqs.url.prefix";
    private static final String EMAIL_APP_SQS_QUEUE_NAME             = "email.app.sqs.queue.name";
    private static final String EMAIL_APP_SQS_URL_HOSTNAME_AS_SUFFIX = "email.app.sqs.url.hostname.as.suffix";
    private static final String EMAIL_APP_PERCENT                    = "email.app.percent";
    private static final String EMAIL_APP_ASYNC_ENABLED              = "email.app.async.enabled";
    private static final String EMAIL_APP_ASYNC_POOLSIZE             = "email.app.async.poolsize";

    private static final String TESTIMONIAL_VERSION_NO   = "testimonial.version.no";
    private static final String DESKTOP_IMAGE_RESOLUTION = "desktop.image.resolution";
    private static final String MOBILE_IMAGE_RESOLUTION  = "mobile.image.resolution";
    private static final String APP_IMAGE_RESOLUTION     = "app.image.resolution";
    
    //BillDesk settings
    
    private static final String BDK_WORKING_KEY = "billdesk.working.key";
    private static final String BDK_URL = "billdesk.host";
    private static final String BDK_TIMEOUT = "billdesk.timeout";

    public static final  String KNOWLARITY_USERNAME                   = "knowlarity.username";
    public static final  String KNOWLARITY_PASSWORD                   = "knowlarity.password";
    public static final  String KNOWLARITY_QUICKCALL_URL              = "knowlarity.quickcall.url";
    public static final  String IVRS_CALL_ENABLED                     = "ivrs.call.enabled";

    public static final String FORTKNOX_ACCESS_KEY      = "fortknox.access.key";
    public static final String FORTKNOX_SECRET_KEY      = "fortknox.secret.key";
    public static final String FORTKNOX_READ_TIMEOUT    = "fortknox.read.timeout";
    public static final String FORTKNOX_CONNECT_TIMEOUT = "fortknox.connect.timeout";

    public static final String  ONECHECK_HOST_URL                                    = "onecheck.host.url";
    public static final String  ONECHECK_CONNECT_TIMEOUT                             = "onecheck.connect.timeout";
    public static final String  ONECHECK_READ_TIMEOUT                                = "onecheck.read.timeout";
    public static final String ONECHECK_CLIENT_ID       = "onecheck.client.id";
    public static final String ONECHECK_CLIENT_KEY      = "onecheck.client.key";
    public static final String FLOW_PERCENTAGE = "campaign.flow.percentage";
    
    public static final String  ONE_CHECK_FORTKNOX_ACCESS_KEY                        = "onecheck.fortknox.access.key";
    public static final String  ONE_CHECK_FORTKNOX_SECRET_KEY                        = "onecheck.fortknox.secret.key";
    
    public static final String KLICK_PAY_REQUEST_URL ="klick.pay.request.url";
    public static final String KLICK_PAY_MERCHANT_CODE="klick.pay.merchant.code";
    
    public static final String  FC_WALLET_API_END_POINT  = "fc.wallet.api.endpoint";
    public static final String  FC_VCARD_API_END_POINT  = "fc.vcard.api.endpoint";
    public static final String  FINAPP_VC_REFUND_URL    = "vcard.refund.url";
    public static final String FC_WALLET_REDIRECT_URL = "fc.wallet.redirect.url";
    public static final String ADMIN_ACS_ENABLED = "admin.acs.enabled";
    public static final String BINS_URL_KEY = "freecharge.bins.url";
    public static final String USER_AUTOPAY_LIST_URL = "user.autopay.list.url";
    
    public static String SUCCESS_MAIL_TRANSACTION = "recharge.success.mail.id";
    public static String FAILURE_MAIL_TRANSACTION = "recharge.failure.mail.id";
    public static String SUCCESS_COUPON_MAIL_TRANSACTION = "recharge.success.coupon.mail.id";
    public static String BILL_PAYMENT_SUCCESS_COUPON_MAIL_TRANSACTION = "billpayment.success.coupon.mail.id";
    public static String RECHARGE_COUPON_MAIL_TRANSACTION = "recharge.coupon.mail.id";
    public static String BILL_PAYMENT_COUPON_MAIL_TRANSACTION = "billpayment.coupon.mail.id";
    public static String RECHARGE_FAILURE_WITH_COUPON_MAIL_TRANSACTION = "recharge.fail.coupon.mail.id";
    public static String BILL_PAYMENT_FAILURE_WITH_COUPON_MAIL_TRANSACTION = "billpayment.fail.coupon.mail.id";   
    public static String GOOGLE_CREDITS_RECHARGE_SUCCESS_MAIL_TRANSACTION = "google.credits.success.mail.id";
    public static String CUSTOM_FAILURE_EMAIL_TEMPLATE_BSNL = "custom.failure.template.id.bsnl";
    public static String CUSTOM_FAILURE_EMAIL_TEMPLATE_DISH_TV = "custom.failure.template.id.dishtv";

    public final String PUSH_NOTIFICATION_URL = "push.notification.url";
    //Tracker api settings
    private final String TRACKER_API_URL                           = "tracker.api.url";
    private final String TRACKER_API_CONNECTION_TIMEOUT            = "tracker.api.connection.timeout";
    private final String TRACKER_API_SOCKET_TIMEOUT                = "tracker.api.socket.timeout";
    private final String TRACKER_API_MAXIMUM_CONNECTIONS           = "tracker.api.maximum.connections";
    private final String TRACKER_API_MAXIMUM_CONNECTIONS_PER_ROUTE = "tracker.api.maximum.connections.per.route";
    private final String TRACKER_API_THREAD_POOL_SIZE              = "tracker.api.thread.pool.size";
    private final String TRACKER_API_REQUEST_EXECUTION_TIME        = "tracker.api.request.execution.time";
    private final String TRACKER_API_CONN_MANAGER_TIMEOUT          = "tracker.api.conn.manager.timeout";

    // Repeat recharge properties
    private final String MAX_DENOM_IN_RECHARGE_HISTORY  = "recharge.history.max.denom";
    private final String MAX_NUMBER_IN_RECHARGE_HISTORY = "recharge.history.max.mobNum";
    private final String MAX_ENTRIES_IN_RECHARGE_HISTORY = "recharge.history.max.entries";

    private final String GUPSHUP_CONNECTION_TIMEOUT = "gupshup.connection.timeout";
    private final String GUPSHUP_READ_TIMEOUT       = "gupshup.read.timeout";

    private final Logger logger = LoggingFactory.getLogger(getClass());

    private final       String commonPropertiesFile       = "common.properties";
    private final       String systemPropertiesFile       = "system.properties";
    private final       String buildPropertiesFile        = "build.properties";
    private final       String antifraudPropertiesFile    = "antifraud.properties";
    private final       String localPropertiesFile        = "local.properties";
    private final       String cacheBustingPropertiesFile = "cacheBusting.properties";
    public static final String prodPasswordsFile          = "prod.passwords";
    public static final String preProdPasswordsFile       = "preProd.passwords";
	
    public static final String e2ePasswordsFile           = "e2e.passwords";

	public static final String GET_BALANCE_API_TIMEOUT = "balance.api.timeout";
	
    private final String revenueLeakPropertiesFile = "revenueleak.properties";

    private final String        EMAIL_ID_OCW_MIGRATION_SUCCESSFUL_REFUND_TO_BANK     = "ocw.migration.bank.refund.success.email.id";
    private final String        EMAIL_ID_OCW_MIGRATION_FAILED_REFUND_TO_BANK         = "ocw.migration.bank.refund.failed.email.id";

    // Client Names 
    public final static String FREECHARGE_CLIENT_NAME = "client.name.freecharge";
    public final static String SNAPDEAL_CLIENT_NAME = "client.name.snapdeal";
    public final static String TINYOWL_CLIENT_NAME = "client.name.tinyowl";
    public final static String VIRTUALCARD_CLIENT_NAME = "client.name.virtualcard";
    public final static String SHOPSTOP_CLIENT_NAME = "client.name.shoppersstop";
    public final static String MCD_CLIENT_NAME = "client.name.mcdonalds";
    public final static String CROSSWORD_CLIENT_NAME="client.name.crossword";
    public final static String HYPERCITY_CLIENT_NAME="client.name.hypercity";
    public final static String ZOOMIN_CLIENT_NAME = "client.name.zoomin";
    public final static String DAILYHUNT_CLIENT_NAME = "client.name.dailyhunt";
	private static final String CCS_PAYMENT_RETRY_LIST = "failure.retry.products";
	private static final String CCS_UTILITY_PRODUCTS_LIST = "utility.products";
	private static final String UTILITY_PRODUCTS_LIST_KEY = "utilityProducts";
	private static final String UPI_BENEFICIARY_VPA_KEY = "upi.freecharge.vpa";
    //Stack Id
    public final static String FREECHARGE_STACK_ID ="app.freecharge.stack.id";
    
    public final static String FREECHARGE_DESKTOP_AB_TEST = "fc.desktop.ab.testing.experiment.id";
    public final static String FREECHARGE_DESKTOP_NEW_SITE_BUCKET_ID = "fc.desktop.ab.testing.newsite.bucket";
    public final static String FREECHARGE_DESKTOP_NEW_SITE_URL = "fc.desktop.ab.testing.newsite.url";
    
    
	private static final String CCS_BASEPATH_SURVEY_URL = "ccs.survey.baseAttachPath";
	private static final String CCS_SURVEY_URL_KEY = "url.key";
	private static final String CCS_FORCE_REFUND_PRODUCT_LIST_KEY = "product.list";
	private static final String CCS_FORCE_REFUND_ORDER_AGE_MIN_KEY = "order.minage";
	private static final String CCS_FORCE_REFUND_ORDER_AGE_MAX_KEY = "order.maxage";
	private static final String CCS_FORCE_REFUND_MAX_LIMIT_PER_DAY = "day.maxamount";
	private static final String CCS_FORCE_REFUND_CHANNEL_LIST_KEY = "channel.list";
	private static final String CCS_FORCE_REFUND_OPERATOR_LIST_KEY = "operator.list";
	private static final String CCS_FORCE_REFUND_AGGREGATOR_LIST_KEY = "aggregator.list";
	
	
	public static final String PREPAID_RECHARGE_SUCCESS_SMS_TEMPLATE_ID = "prepaid.recharge.success.sms.template.id";
	public static final String PREPAID_SELF_RECHARGE_SUCCESS_SMS_TEMPLATE_ID = "prepaid.self.recharge.success.sms.template.id";
	public static final String PREPAID_SELF_RECHARGE_PENDING_SMS_TEMPLATE_ID = "prepaid.self.recharge.pending.success.sms.template.id";
	public static final String PREPAID_RECHARGE_PENDING_SMS_TEMPLATE_ID = "prepaid.recharge.pending.sms.template.id";
	public static final String PREPAID_RECHARGE_FAILURE_SMS_TEMPLATE_ID = "prepaid.recharge.failure.sms.template.id";
	public static final String PREPAID_SELF_RECHARGE_FAILURE_SMS_TEMPLATE_ID = "prepaid.self.recharge.failure.sms.template.id";
	
	
	public static final String POSTPAID_BILL_PAY_SUCCESS_SMS_TEMPLATE_ID = "postpaid.billpay.success.sms.template.id";
	public static final String POSTPAID_SELF_BILL_PAY_SUCCESS_SMS_TEMPLATE_ID = "postpaid.self.billpay.success.sms.template.id";
	public static final String POSPAID_BILLPAY_SELF_PENDING_SMS_TEMPLATE_ID = "postpaid.self.billpay.pending.sms.template.id";
	public static final String POSTPAID_BILLPAY_PENDING_SMS_TEMPLATE_ID = "postpaid.billpay.pending.sms.template.id";
	public static final String POSTPAID_BILLPAY_FAILURE_SMS_TEMPLATE_ID = "postpaid.billpay.failure.sms.template.id";
	public static final String POSTPAID_SELF_BILLPAY_FAILURE_SMS_TEMPLATE_ID = "postpaid.self.billpay.failure.sms.template.id";
	
	
	public static final String BILLPAY_SUCCESS_SMS_TEMPLATE_ID = "billpay.success.sms.template.id";
	public static final String NON_BILLDESK_BILLPAY_SUCCESS_SMS_TEMPLATE_ID = "non.billdesk.billpay.success.sms.template.id";
	public static final String BILLPAY_FAILURE_SMS_TEMPLATE_ID = "billpay.failure.sms.template.id";
	public static final String BILLPAY_PENDING_SMS_TEMPLATE_ID = "billpay.pending.sms.template.id";
	
	public static final String DTH_RECHARGE_SUCCESS_SMS_TEMPLATE_ID = "dth.recharge.success.sms.template.id";
	public static final String DTH_RECHARGE_PENDING_SMS_TEMPLATE_ID = "dth.recharge.pending.sms.template.id";
	public static final String DTH_RECHARGE_FAILURE_SMS_TEMPLATE_ID = "dth.recharge.failure.sms.template.id";
	
	public static final String DATACARD_RECHARGE_SUCCESS_SMS_TEMPLATE_ID = "datacard.recharge.success.sms.template.id";
	public static final String DATACARD_RECHARGE_PENDING_SMS_TEMPLATE_ID = "datacard.recharge.pending.sms.tempalte.id";
	public static final String DATACARD_RECHARGE_FAILURE_SMS_TEMPLATE_ID = "datacard.recharge.failure.sms.tempalte.id";
	
	public static final String METRO_PAYMENT_SUCCESS_SMS_TEMPLATE_ID ="metro.payment.success.sms.template.id";
	public static final String METRO_PAYMENT_PENDING_SMS_TEMPLATE_ID ="metro.payment.pending.sms.template.id";
	public static final String METRO_PAYMENT_FAILURE_SMS_TEMPLATE_ID ="metro.payment.failure.sms.template.id";
	
	public static final String LANDLINE_RECHARGE_PENDING_SMS_TEMPLATE_ID = "landline.recharge.pending.sms.template.id";
	public static final String BROADBAND_RECHARGE_PENDING_SMS_TEMPLATE_ID = "broadband.recharge.pending.sms.template.id";
	
	public static final String DOCOMO_POSTPAID_GSM = "docomo.postpaid.gsm";
	public static final String DOCOMO_POSTPAID_CDMA ="docomo.postpaid.cdma";
	
	
	public static final String GOOGLE_CREDIT_RECHARGE_SUCCESS_SMS_TEMPLATE_ID = "google.credit.recharge.success.sms.template.id";
	
	
	
    
    private Env                 env;
    private Properties          props;
    private Properties          antifraudProps;
    private ResourceLoader   resourceLoader;
    private AmazonKMSService amazonKMSService;

    private AppConfigService appConfig;
	private ICCS ccs;
	
	//Payment Retry Notification Messages
	public static final String MESSAGE_RECHARGE_RETRY = "payment.retry.recharge";
	public static final String MESSAGE_ELECTRICITY_RETRY = "payment.retry.electricity"; 
	public static final String MESSAGE_DTH_RETRY = "payment.retry.dth";
	public static final String MESSAGE_METRO_RETRY = "payment.retry.metro";
	public static final String MESSAGE_DATACARD_RETRY = "payment.retry.datacard";
	public static final String MESSAGE_GAS_RETRY = "payment.retry.gas";
	public static final String MESSAGE_LANDLINE_RETRY = "payment.retry.landline";
	
	//Notification service end-points
	public static final String RETRY_EMAIL_END_POINT = "retry.email.endpoint";
	public static final String RETRY_SMS_END_POINT = "retry.sms.endpoint";
	public static final String IOS_RETRY_PUSH_ENDPOINT = "retry.iospush.endpoint";
	public static final String ANDROID_RETRY_PUSH_ENDPOINT="retry.androidpush.endpoint";
	
	// Id's for payment retry in campaign tool
	public static final String RETRY_CAMPAIGN_TOOL_EMAILID = "retry.campaign.tool.emailid";
	public static final String RETRY_CAMPAIGN_TOOL_SMSID = "retry.campaign.tool.smsid";
	public static final String RETRY_CAMPAIGN_TOOL_IOSTEMPLATEID = "retry.campaign.tool.iosid";
	public static final String RETRY_CAMPAIGN_TOOL_ANDROIDTEMPLATEID = "retry.campaign.tool.androidid";

    // Ids for payment retry on Operator Uptime events
    public static final String OPERATOR_UPTIME_NOTIFY_SMSID = "operator.uptime.notify.sms.id";
    public static final String OPERATOR_UPTIME_NOTIFY_ANDROID_TEMPLATE_ID = "operator.uptime.notify.android.id";
    public static final String OPERATOR_UPTIME_NOTIFY_IOS_TEMPLATE_ID = "operator.uptime.notify.ios.id";
    public static final String OPERATOR_IMPACTED_USERS_MAX_SIZE = "operator.impacted.users.max.size";

	//Configuration of channels based on platform
	public static final String CCS_PAYMENT_RETRY_CHANNELS_WEB = "failure.retry.channels.web";
	public static final String CCS_PAYMENT_RETRY_CHANNELS_MOBILE = "failure.retry.channels.mobile";
	public static final String CCS_PAYMENT_RETRY_CHANNELS_ANDROID = "failure.retry.channels.android";
	public static final String CCS_PAYMENT_RETRY_CHANNELS_IOS = "failure.retry.channels.ios";
	public static final String SOUND_OF_SUCCESS_ANDROIDTEMPLATEID = "sos.campaign.tool.androidid";
	public static final String SOUND_OF_SUCCESS_IOSTEMPLATEID = "sos.campaign.tool.iosid";

    // Merchant ID redis storage timeout
    public static final String KPAY_MERCHANT_ID_REDIS_TIMEOUT_DAYS = "kpay.mid.redis.timeout";
	private static final String CCS_FULFILLMENT_FULFILLMENT_PRODUCTS_KEY = "notification.success.product";
	
	//Operations Panel access key and secret key
	public static final String OPERATIONS_ACCESS_KEY = "operations.access.key";
	public static final String OPERATIONS_SECRET_KEY = "operations.secret.key";
	private static final String ANDROID_NOTIFICATION_QUEUE = "android.notification.queue";
	private static final String NOTIFICATION_QUEUE_ENDPOINT = "notification.queue.endpoint";
	public static final String SOUND_OF_SUCCESS_ANDROIDTEMPLATEID_TWO = "sos.campaign.tool.androidid.two";
	public static final String SOUND_OF_SUCCESS_ANDROIDTEMPLATEID_THREE = "sos.campaign.tool.androidid.three";

	public static final String UPI_CLIENT_IP = "upi.client.ip";
	public static final String UPI_CLIENT_PORT = "upi.client.port";
	public static final String UPI_CLIENT_KEY = "upi.client.key";
	public static final String UPI_CLIENT_ID = "upi.client.id";
	public static final String UPI_CLIENT_ENV = "upi.client.env";
	public static final String FULFILLMENT_RETRY_NOTIFICATION_QUEUE="ff.notification.retry.consumer.queue.name";
	public static final String FULFILLMENT_SKIPPED_TASK_QUEUE= "ff.skipped.task.consumer.queue.name";
	public static final String UNKNOWN_STATUS_PROCESSOR_QUEUE="unknown.status.consumer.queue.name";
	public static final String FULFILLMENT_ADMIN_REFUNDS_QUEUE="ff.admin.refund.consumer.queue.name";



	//Instant Credit Card Keys
    public static final String 	KEYSTORE_PATH									 = "key.certificate.bundle.path";
    public static final String 	STORE_PASS									 	 = "store.pass";
    public static final String 	KEY_PASS									 	 = "key.pass";
    public static final String 	CLIENT_USER_ID									 = "client.userid";
    public static final String 	CLIENT_PASS									     = "client.pass";
    public static final String  VCC_FETCH_API_TIMEOUT			                 = "vcc.fetch.api.timeout";
    public static final String  ENCRYPTION_KEY                                   = "enryption.key";
    public static final String  FETCH_CARD_DETAIL_URL                            = "fetch.card.detail.url";
    public static final String  CHECKSUM_KEY                                     =  "checksum.key";
	
    public FCProperties(Env env, AmazonKMSService amazonKMSService) {
        this.env = env;
        this.amazonKMSService = amazonKMSService;
    }

    public void setResourceLoader(final ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    /*
     * this method is call by the IOC container.
     */
    @SuppressWarnings("unused") private void init() {
        logger.info("Lakshya: Env:"+env.getMode());
        // The order in which these property files are loaded matters. Do not
        // change this unless you know what you are doing.
        Properties properties = loadProperties(commonPropertiesFile, null);
        // The system.properties file gives us easy access to the mode -
        // dev/qa/prod - for use in the "mt" JSTL
        properties = loadProperties(systemPropertiesFile, properties);
        // The build.properties file is generated at every build by the ant init
        // task
        properties = loadProperties(buildPropertiesFile, properties);
        loadAntifraudProperties();
        properties = loadProperties(revenueLeakPropertiesFile, properties);

        // preprod inherits from prod properties
        if (this.isPreProdMode()) {
            properties = loadProperties("prod.properties", properties);
        }

        if (this.isAWSProdMode()) {
            properties = loadProperties("prod.properties", properties);
        }

        // and next load <mode>.properties (dev.properties and so forth)
        String modeFile = env.getMode() + ".properties";
        String passwordModeFile = env.getMode() + ".passwords";
        logger.info("value of modeFile variable in FCProperties " + modeFile);
        properties = loadProperties(modeFile, properties);
        properties = loadPasswordProperties(passwordModeFile, properties);
        // properties = loadProperties(cacheBustingPropertiesFile, properties);
        // finally load local.properties. Take precedence over everything.
        props = loadProperties(this.localPropertiesFile, properties);

        initializeCCS();
        if(isCCSInitEnabled()){
            readAndAddAntifraudProps();
        }

        if (shouldMockAG()) {
            logger.info("[MOCK] !!! Mocking AG    !!!");
        }

        if (shouldMockPG()) {
            logger.info("[MOCK] !!! Mocking PG    !!!");
        }

        if (shouldMockSMS()) {
            logger.info("[MOCK] !!! Mocking SMS   !!!");
        }

    }

    private void loadAntifraudProperties() {
        Resource resource = resourceLoader.getResource(ResourceLoader.CLASSPATH_URL_PREFIX + antifraudPropertiesFile);
        antifraudProps = new Properties();
        try {
            antifraudProps.load(resource.getInputStream());
        } catch (IOException e) {
            logger.fatal(e);
            System.exit(0);
        }
    }

    /**
     * For reinitializing properties in Production
     * call http://ccs.freecharge.in/ccs-service/webservice/putConf?key=ccs.init.enabled&appBasePath=fraudservice with content type application json and true in body
     * @param
     * @return
     */
    private boolean isCCSInitEnabled() {
        Boolean isCCSInitEnabled = false;
        if (ccs == null)
            return isCCSInitEnabled;
        try {
            isCCSInitEnabled = ccs.getConf(CCS_INIT_ENABLED);
            if (isCCSInitEnabled == null) {
                ccs.putConf(CCS_INIT_ENABLED, Boolean.TRUE);
                isCCSInitEnabled = true;
            }
        } catch (NotSerializableException | ClassNotFoundException | CCSException e) {
            logger.error("Not able to read "+CCS_INIT_ENABLED+" property.", e);
        } 
        return isCCSInitEnabled;
    }

    private void readAndAddAntifraudProps() {
    	Set<Entry<Object, Object>> entrySet = antifraudProps.entrySet();
    	String key = null;
    	try {
        	for(Entry<Object, Object> entry: entrySet) {
        	    key = (String)entry.getKey();
                ccs.putConf(key, entry.getValue());
    		}
        	ccs.putConf(CCS_INIT_ENABLED, Boolean.FALSE);
    	} catch (NotSerializableException| CCSException e) {
    	    logger.error("Not able to put "+key+" property. Fetching antifraud properties from file now.", e);
            appConfig.setLoadAntiFraudPropsFromFileEnabled(true);
    	}
	}

	private void initializeCCS() {
    	ICCSConfig ccsConfig = new AbstractCCSConfig(); 
    	ccsConfig.setCcsHost(props.getProperty(CCS_HOST)); 
    	ccsConfig.setDefaultAttachPath(props.getProperty(CCS_ANTIFRAUD_BASEATTACHPATH));
    	
    	ccs = new CCS(); 
    	ccs.init(ccsConfig); 
	}

	/**
     * Update properties with new values read from file. This is used to load
     * consumer properties for now. Be careful and know what you are doing if
     * you want to use this.
     *
     * @param name
     */
    public void updateProperties(final String name) {
        this.props = loadProperties(name, this.getProperties());
    }

    /**
     * The password properties are encrytped and stored using the Amazon KMS.
     * This function will get the actual contents of the file and load them to
     * the defaults, if it exists.
     * 
     * @param fileName
     * @param defaults
     * @return Updated properties
     */
    public Properties loadPasswordProperties(String fileName, Properties defaults) {
        String fileContents = amazonKMSService.getFileContents(fileName);
        return loadPropertiesFromString(fileContents, defaults);
    }

    /**
     * Load the properties from the resource and add it to the defaults
     * property.
     * 
     * @param resource
     * @param defaults
     * @return
     */
    private Properties loadPropertiesFromResource(Resource resource, Properties defaults) {
        Properties props = new Properties();
        if (defaults != null) {
            props = new Properties(defaults);
        }

        try {
            props.load(resource.getInputStream());
            // inFileContentStream.close();
        } catch (Exception e) {
            logger.fatal(e);
            System.exit(0);
        }
        return props;
    }

    /**
     * Load the new properties from the String newProp and updated it to the
     * defaults Properties, if it exists.
     * 
     * @param newProp
     * @param defaults
     * @return Updated properties
     */
    private Properties loadPropertiesFromString(String newProp, Properties defaults) {
        Resource resource = new ByteArrayResource(newProp.getBytes());
        return loadPropertiesFromResource(resource, defaults);
    }

    private Properties loadProperties(String name, Properties defaults) {
        logger.info("Lakshya: classpath: "+ResourceLoader.CLASSPATH_URL_PREFIX);
        Resource resource = resourceLoader.getResource(ResourceLoader.CLASSPATH_URL_PREFIX + name);
        return loadPropertiesFromResource(resource, defaults);
    }

    public String getProperty(final String key) {
        String value = props.getProperty(key);
        if (value == null) {
            return null;
        }
        // throw new FCPropertiesException("Cannot find property with the key "
        // + key);
        return value;
    }

    public String getAntiFraudProperty(String key){
    	if(appConfig==null){
    		ApplicationContext appContext = AppContextManager.getAppContext();
    		appConfig = (AppConfigService) appContext.getBean(APP_CONFIG_BEAN);
    	}
    	boolean loadAntiFraudPropsFromFile = appConfig.isLoadAntiFraudPropsFromFileEnabled();
    	String value = null;
    	if(!loadAntiFraudPropsFromFile){
	    	try{
	    		value = ccs.getConf(key);
	    	}catch(ZookeeperDownException e){
	    		//if CCS is down, log that exception and load from in-memory map.
	    		logger.error("CCS is down. Fetching antifraud properties from file now.", e);
	    		value = antifraudProps.getProperty(key);
	    	}catch(CCSException | ClassNotFoundException e){
	    		//if any other exception is thrown, log that exception and load from in-memory map.
	    		logger.error("Caught an exception while fetching property from CCS. Fetching antifraud properties from file now.", e);
	    		value = antifraudProps.getProperty(key);
	    	}
    	}else{
	    	value = antifraudProps.getProperty(key);
    	}
        return value;
    }
    
    public List<String> getPropertyValueList(final String key, final String delimiter) {

        String propertyValue = getProperty(key);
        List<String> propertyValueList = FCStringUtils.getStringList(propertyValue, delimiter);

        return propertyValueList;
    }

    public int getIntProperty(final String key) {
        String val = getProperty(key);
        try {
            return Integer.parseInt(val);
        } catch (NumberFormatException nfe) {
            throw new FCPropertiesException("Can't parse the value " + val + " as an int from key " + key);
        }
    }
    
    public int getAntifraudIntProperty(final String key) {
        String val = getAntiFraudProperty(key);
        try {
            return Integer.parseInt(val);
        } catch (NumberFormatException nfe) {
            throw new FCPropertiesException("Can't parse the value " + val + " as an int from key " + key);
        }
    }

    public long getLongProperty(final String key) {
        String val = getProperty(key);
        try {
            return Long.parseLong(val.trim());
        } catch (Exception nfe) {
            throw new FCPropertiesException("Can't parse the value " + val + " as an int from key " + key);
        }
    }
    
    public long getAntifraudLongProperty(final String key) {
        String val = getAntiFraudProperty(key);
        try {
            return Long.parseLong(val.trim());
        } catch (Exception nfe) {
            throw new FCPropertiesException("Can't parse the value " + val + " as an int from key " + key);
        }
    }

    public boolean getBooleanProperty(final String key) {
        String val = getProperty(key);
        if ("true".equalsIgnoreCase(val)) {
            return true;
        }

        if ("false".equalsIgnoreCase(val)) {
            return false;
        }
        throw new FCPropertiesException("Boolean value with key " + key + " must be 'true' or 'false'; we got " + val);
    }

    public boolean getAntiFraudBooleanProperty(final String key) {
        String val = getAntiFraudProperty(key);
        if ("true".equalsIgnoreCase(val)) {
            return true;
        }

        if ("false".equalsIgnoreCase(val)) {
            return false;
        }
        throw new FCPropertiesException("Boolean value with key " + key + " must be 'true' or 'false'; we got " + val);
    }
    
    public Date getDateProperty(final String key) {
        try {
            return RefundUtil.getDate(getProperty(key));
        } catch (ParseException e) {
            throw new FCPropertiesException(e);
        }
    }

    public Date getMailCampaignStartDate() {
        return getDateProperty(CAMPAIGN_START_DATE);
    }

    public Date getMailCampaignEndDate() {
        return getDateProperty(CAMPAIGN_END_DATE);
    }

    public Date getCouponCampaignStartDate() {
        return getDateProperty(COUPON_CAMPAIGN_START_DATE);
    }

    public Date getCouponCampaignEndDate() {
        return getDateProperty(COUPON_CAMPAIGN_END_DATE);
    }

    public Properties getProperties() {
        return props;
    }

    public boolean isQaMode() {
        return env.isQaMode();
    }

    public boolean isProdMode() {
        return env.isProdMode() || env.isPreProdMode() || env.isAWSProdMode();
    }

    public boolean isCouponPricingEnabled() {
        return getBooleanProperty(COUPONS_PRICING_ENABLED);
    }

    public boolean isDevMode() {
        return env.isDevMode();
    }

    public boolean isPreProdMode() {
        return env.isPreProdMode();
    }

    public boolean isE2eMode() {
       return env.isE2eMode();
    }

    public boolean isAWSProdMode() {
        return env.isAWSProdMode();
    }

    public boolean shouldMockAG() {
        return !this.isProdMode() && this.getBooleanProperty(FCConstants.MOCK_AG);
    }

    public boolean shouldMockPG() {
        return !this.isProdMode() && this.getBooleanProperty(FCConstants.MOCK_PG);
    }

    public boolean shouldMockSMS() {
        return !this.isProdMode() && this.getBooleanProperty(FCConstants.MOCK_SMS);
    }

    public boolean shouldMockBillPaymentAG() {
        return !this.isProdMode() && this.getBooleanProperty(FCConstants.MOCK_AG);
    }

    public String getInTransIdPrefix() {
        if (!this.isProdMode()) {
            return this.getProperty(IN_TRANS_ID_PREFIX_KEY);
        }

        return "";
    }

    public List<Integer> getCouponIdsForATMDispatch() {
        return getAsListOfInteger(ATM_AFFILIATE_COUPON_IDS);
    }

    public List<Integer> getAsListOfInteger(final String propertyName) {
        String commaSeparatedString = getProperty(propertyName);
        if (commaSeparatedString == null) {
            logger.error("No property found with name: " + propertyName);
        }
        StringTokenizer tokenizer = new StringTokenizer(commaSeparatedString, ",");
        List<Integer> couponIdList = new ArrayList<Integer>();
        while (tokenizer.hasMoreElements()) {
            couponIdList.add(Integer.parseInt(tokenizer.nextToken()));
        }
        return couponIdList;
    }

    public String getImagePrefix1() {
        return this.getProperty(IMAGE_PREFIX_KEY);
    }

    public String getMode() {
        return env.getMode();
    }

    public String getEmailFromName() {
        String name = this.getProperty(EMAIL_FROM_NAME);

        if (StringUtils.isEmpty(name)) {
            return "";
        }

        return name;
    }

    public boolean isUserAgentTrackingEnabled() {
        return this.getBooleanProperty(USER_AGENT_TRACKING);
    }

    public boolean isWalletUnderMaintenance() {
        return this.getBooleanProperty(WALLET_FEATURE_UNDER_MAINTAINENCE);
    }

    public boolean isWalletPayEnabled() {
        return this.getBooleanProperty(WALLET_PAY_ENABLED);
    }

    public boolean isWalletDataDisplayEnabled() {
        return this.getBooleanProperty(WALLET_DATA_DISPLAY_ENABLED);
    }

    public boolean isRedisToMongoPhase() {
        return this.getBooleanProperty(REDIS_TO_MONGO_PHASE);
    }

    public boolean isZeropayUnderMaintenance() {
        return this.getBooleanProperty(ZEROPAY_FEATURE_UNDER_MAINTAINENCE);
    }

    /**
     * Should CSRF check be made or not.
     *
     * @return - true if CSRF check is enabled.
     */
    public boolean isCSRFCheckEnabled() {
        return this.getBooleanProperty(CSRF_CHECK_ENABLED);
    }

    public boolean isRedisSessionEnabled() {
        return this.getBooleanProperty(REDIS_SESSION_ENABLED);
    }

    public boolean isMongoSessionEnabled() {
        return this.getBooleanProperty(MONGO_SESSION_ENABLED);
    }

    public boolean isZeropayPayEnabled() {
        return this.getBooleanProperty(ZEROPAY_PAY_ENABLED);
    }

    public boolean isZeropayDataDisplayEnabled() {
        return this.getBooleanProperty(ZEROPAY_DATA_DISPLAY_ENABLED);
    }

    public String getApplicationCookieName() {
        return this.getProperty(FCConstants.APPLICATION_COOKIE_NAME);
    }

    public boolean isRedisCacheEnabled() {
        return this.getBooleanProperty(REDIS_CACHE_ENABLED);
    }

    public boolean isMockSessionEnabled() {
        return this.getBooleanProperty(MOCK_SESSION_ENABLED);
    }

    public boolean isMockMongo() {
        return this.getBooleanProperty(MOCK_MONGO);
    }

    public boolean isMockCacheEnabled() {
        return this.getBooleanProperty(MOCK_CACHE_ENABLED);
    }

    public String getCashBackOfferCode() {
        return this.getProperty(CASH_BACK_OFFER_CODE);
    }

    public Map<String, String> getPayUCashBackOfferKeyMap() {
        Map<String, String> offerKeyMap = new HashMap<String, String>();
        offerKeyMap.put(this.getProperty(CASH_BACK_OFFER_CODE), this.getProperty(PAYU_CASH_BACK_OFFER_KEY));
        return offerKeyMap;
    }

    public int getCashBackOfferAmount() {
        return this.getIntProperty(CASH_BACK_OFFER_AMOUNT);
    }

    public int getSessionExpireTime() {
        return this.getIntProperty(SESSION_EXPIRE_TIME);
    }

    public int getCashBackOfferMinimumRechargeAmount() {
        return this.getIntProperty(CASH_BACK_OFFER_MINIMUM_RECHARGE_AMOUNT);
    }

    public int getCashBackOfferLimit() {
        return this.getIntProperty(CASH_BACK_OFFER_LIMIT);
    }

    // This is not applicable in prod environment, in production, cookies have
    // to be https only.
    public boolean isCookieHttpsOnly() {
        if (!isProdMode()) {
            return this.getBooleanProperty(COOKIE_HTTPS_ONLY);
        }
        return true;
    }

    public int getConsumerFrameworkTelnetPort() {
        return this.getIntProperty(CONSUMER_FRAMEWORK_TELNET_PORT);
    }

    public String getEmailQueue() {
        return this.getProperty(EMAIL_QUEUE);
    }

    public String getRechargeQueue() {
        return this.getProperty(RECHARGE_QUEUE);
    }

    public String getHCouponQueue() {
        return this.getProperty(HCOUPON_QUEUE);
    }

    public String getRechargeRetryQueue() {
        return this.getProperty(RECHARGE_RETRY_QUEUE);
    }

    public String getPaySyncQueue() {
        return this.getProperty(PAY_SYNC_QUEUE);
    }

    public String getCardBinQueue() {
        return this.getProperty(CARD_BIN_QUEUE);
    }

    public String getFulfillmentQueue() {
        return this.getProperty(FULFILLMENT_QUEUE);
    }

    public String getPostpaidFulfillmentQueue() {
        return this.getProperty(POSTPAID_BILL_PAYMENT);
    }

    public String getBillpayFulfillmentQueue() {
        return this.getProperty(BILL_PAYMENT_QUEUE);
    }
    
    public String getFFNotificationRetryQueue() {
        return this.getProperty(FULFILLMENT_RETRY_NOTIFICATION_QUEUE);
    }
    
    public String getFFAdminRefundQueue() {
        return this.getProperty(FULFILLMENT_ADMIN_REFUNDS_QUEUE);
    }
    
    
    public String getSkippedFFTaskQueue() {
    	 return this.getProperty(FULFILLMENT_SKIPPED_TASK_QUEUE);
    }
    public String getUnknownStatusProcessorQueue() {
        return this.getProperty(UNKNOWN_STATUS_PROCESSOR_QUEUE);
    }

    public String getAntiFraudDataCollectorQueue() {
        return this.getProperty(ANTIFRAUD_DATA_COLLECTOR_QUEUE);
    }

    public String getRefundQueue() {
        return this.getProperty(REFUND_QUEUE);
    }

    public String getNewRefundQueue() {
        return this.getProperty(NEW_REFUND_QUEUE);
    }
    
    public String getFailureFulfilmentQueue() {
        return this.getProperty(FAILURE_FULFILMENT_QUEUE);
    }

    public String getUPUpdateQueue() {
        return this.getProperty(UP_UPDATE_QUEUE);
    }
    
    public String getBillPaymentRefundQueue() {
        return this.getProperty(BILL_PAYMENT_REFUND_QUEUE);
    }

    public String getReconQueue() {
        return this.getProperty(RECON_QUEUE);
    }

    public String getTestQueue() {
        return this.getProperty(TEST_QUEUE);
    }

    public String getLeadQueue() {
        return this.getProperty(LEAD_QUEUE);
    }

    public String getStatusCheckQueue() {
        return this.getProperty(STATUS_CHECK_QUEUE);
    }
    
    public String getQuickStatusCheckQueue() {
        return this.getProperty(QUICK_STATUS_CHECK_QUEUE);
    }

    public String getBillPayStatusCheckQueue() {
        return this.getProperty(BILL_PAY_STATUS_CHECK_QUEUE);
    }

    public int getKestrelTimeOut() {
        return this.getIntProperty(KESTREL_TIMEOUT);
    }

    public String getPostpaidStatusCheckQueue() {
        return this.getProperty(POSTPAID_STATUS_CHECK_QUEUE);
    }

    public String getNotificationQueue() {
        return this.getProperty(NOTIFICATION_QUEUE);
    }

    public String getFreefundQueue() {
        return this.getProperty(FREEFUND_QUEUE);
    }

    public String getRewardQueue() {
        return this.getProperty(REWARD_QUEUE);
    }

    /*
     * Given a relative path to image converts it to absolute path
     * 
     * @param relativePath
     * 
     * @return Absolute path with img url prefixed.
     */
    public String getAbsoluteImgUrl(final String relativePath) {
        return this.getProperty(FCConstants.VOUCHERPREFIX) + relativePath;
    }

    public String getCouponTnCUrl(final String relativePath) {
        return this.getProperty(FCConstants.COUPON_TNC_PREFIX) + relativePath;
    }

    public String getReconBucket() {
        return this.getProperty(FCConstants.RECON_BUCKET);
    }

    public String getReconUploadDirectory() {
        return this.getProperty(FCConstants.RECON_UPLOAD_DIRECTORY);
    }

    public String getReconReportsDirectory() {
        return this.getProperty(FCConstants.RECON_REPORTS_DIRECTORY);
    }

    public Boolean isLoginCheckEnabled() {
        return this.getBooleanProperty(FCConstants.LOGIN_INTERCEPTOR_ENABLED);
    }

    public String getAppRootUrl() {
        return this.getProperty(FCConstants.HOSTPREFIX);
    }

    public static String getBeanName() {
        return "core.prop";
    }

    public String getVersionNo() {
        return this.getProperty(VERSION_NO);
    }

    public static void main(final String[] args) throws IOException {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        FCProperties fcProperties = ((FCProperties) ctx.getBean(FCConstants.BEAN_DEFINATION_NAME_FCPROPERTIES));
        System.out.println(fcProperties.getProperty("version.no"));
        ctx.close();
    }

    /*
     * Number of coupons which could be opted in for an order ID which if
     * breached indicates that something is wrong.
     */
    public Integer getCouponThresholdForOrder() {
        return Integer.parseInt(this.getProperty(FCConstants.COUPON_ORDER_THRESHOLD));
    }

    /**
     * Returns the time window in minutes after which an m_coupon in INITIALIZED
     * state will be marked as REDEEMED
     *
     * @return
     */
    public Integer getRedemptionExpiryWindow() {
        return Integer.parseInt(this.getProperty(FCConstants.REDEEM_EXPIRY_WINDOW));
    }

    public APIAuthPolicy getAPIAuthPolicy() {
        return APIAuthPolicy.valueOf(this.getProperty(FCConstants.API_AUTH_POLICY));
    }

    public List<String> getWhitelistedIPList() {
        String csvIPList = this.getProperty(FCConstants.IP_WHITE_LIST);

        if (csvIPList == null || csvIPList.isEmpty()) {
            return new ArrayList<String>();
        }

        return Arrays.asList(csvIPList.split(","));
    }

    public String getVisitTrackingCookieName() {
        return this.getProperty(VISIT_TRACKING_COOKIE_NAME);
    }

    public int getMaxPromoCodeApplyCount() {
        return this.getIntProperty(MAX_PROMOCODE_APPLY_COUNT);
    }

    public int getPvrCouponStoreId() {
        return this.getIntProperty(PVR_COUPON_STORE_ID);
    }

    public String getPromoCodeRewardClassName() {
        return this.getProperty(PROMOCODE_REWARD_CLASS_NAME);
    }

    public String getPromoCodeRewardConditionName() {
        return this.getProperty(PROMOCODE_REWARD_CONDITION_NAME);
    }

    public int getFreefundUsageHistorySize() {
        return this.getIntProperty(FREEFUND_USAGE_HISTORY_SIZE);
    }

    /**
     * Return visit tracking cookie expiry time in minutes.
     *
     * @return - Cookie expiry time in minutes
     */
    public int getVisitTrackingCookieInterval() {
        return this.getIntProperty(VISIT_TRACKING_COOKIE_INTERVAL);
    }

    public String getClientTrackingCookieName() {
        return this.getProperty(CLIENT_TRACKING_COOKIE_NAME);
    }


    /**
     * Client tracking cookie expiry time in seconds.
     *
     * @return - Cookie expiry time in seconds.
     */
    public int getClientTrackingCookieInterval() {
        return this.getIntProperty(CLIENT_TRACKING_COOKIE_INTERVAL);
    }

    public boolean isReCaptchaEnabled() {
        return this.getBooleanProperty(FCConstants.RECAPTCHA_ENABLED);
    }

    public String getReCaptchaLoginThresholdMins() {
        return this.getProperty(FCConstants.RECAPTCHA_LOGIN_THRESHOLD_MINS);
    }

    public String getCaptchaFraudCheckCount() {
        return this.getProperty(FCConstants.CAPTCHA_FRAUD_CHECK_COUNT);
    }

	public String getCaptchaFraudCheckCountForCampaign() {
		return this.getProperty(FCConstants.CAMPAIGN_CAPTCHA_FRAUD_CHECK_COUNT);
	}

    public String getRecaptchaPublicKey() {
        return this.getProperty(FCConstants.RECAPTCHA_PUBLIC_KEY);
    }

    public String getRecaptchaPrivateKey() {
        return this.getProperty(FCConstants.RECAPTCHA_PRIVATE_KEY);
    }

    public List<String> getAdminRootEmails() {
        String emailsStr = this.getProperty("admin.root.emails");
        return Arrays.asList(emailsStr.split(";"));
    }

    public String getFreechargeUrl() {
        return this.getProperty(FREECHARGE_URL);
    }

    public String getAwsAccessKey() {
        return this.getProperty(AWS_ACCESS_KEY);
    }

    public String getAwsSecretKey() {
        return this.getProperty(AWS_SECRET_KEY);
    }

    public String getAwsSqsAccessKey() {
        return this.getProperty(AWS_SQS_ACCESS_KEY);
    }

    public String getAwsSqsSecretKey() {
        return this.getProperty(AWS_SQS_SECRET_KEY);
    }

    public String getAwsSqsUrl() {
        return this.getProperty(AWS_SQS_URL);
    }

    public String getAwsSqsPrincipal() {
        return this.getProperty(AWS_SQS_PRINCIPAL);
    }

    public String getDidMainQueue() {
        return this.getProperty(DID_MAIN_QUEUE);
    }

    public String getDidSmsQueue() {
        return this.getProperty(DID_SMS_QUEUE);
    }

    public boolean isSmsAclEnabled() {
        return this.getBooleanProperty(SMSACL_ENABLED);
    }

    public String getSmsAclSmsUrl() {
        return this.getProperty(SMSACL_SMSURL);
    }

    public String getSmsAclMultiSmsUrl() {
        return this.getProperty(SMSACL_MULTISMSURL);
    }

    public Integer getSmsAclMultiSmsSize() {
        return this.getIntProperty(SMSACL_MULTISMSSIZE);
    }

    public String getSmsAclUserid() {
        return this.getProperty(SMSACL_USERID);
    }

    public String getSmsAclPass() {
        return this.getProperty(SMSACL_PASS);
    }

    public String getSmsAclAppid() {
        return this.getProperty(SMSACL_APPID);
    }

    public String getSmsAclSubAppid() {
        return this.getProperty(SMSACL_SUBAPPID);
    }

    public String getSmsAclFrom() {
        return this.getProperty(SMSACL_FROM);
    }

    public Double getOxigenBalanceWarnThreshold() {
        return new Double(this.getProperty(OXIGEN_BALANCE_WARN_THRESHOLD));
    }

    public Double getOxigenBalanceCriticalThreshold() {
        return new Double(this.getProperty(OXIGEN_BALANCE_CRITICAL_THRESHOLD));
    }

    public Double getEuronetBalanceWarnThreshold() {
        return new Double(this.getProperty(EURONET_BALANCE_WARN_THRESHOLD));
    }

    public Double getEuronetBalanceCriticalThreshold() {
        return new Double(this.getProperty(EURONET_BALANCE_CRITICAL_THRESHOLD));
    }

    public Boolean isOperatorMetricAlertEnabled() {
        return this.getBooleanProperty(OPERATOR_METRIC_ALERT_ENABLED);
    }

    public boolean shouldLogTrackingEvents() {
        return this.getBooleanProperty(LOG_TRACKING_EVENTS);
    }

    public long getRechargeRetryStatusScanInterval() {
        return getLongProperty(RECHARGE_RETRY_STATUS_SCAN_INTERVAL);
    }

    public Integer getBenchMarkCount() {
        return this.getIntProperty(DENOMINATION_BENCHMARK_COUNT);
    }

    public Integer getBenchMarkSuccessRate() {
        return this.getIntProperty(DENOMINATION_BENCHMARK_SUCCESS_PERCENT);
    }

    public Integer getDatacardBenchMarkCount() {
        return this.getIntProperty(DATACARD_DENOMINATION_BENCHMARK_COUNT);
    }

    public Integer getDatacardBenchMarkSuccessRate() {
        return this.getIntProperty(DATACARD_DENOMINATION_BENCHMARK_SUCCESS_PERCENT);
    }

    public Integer getBsnlBenchMarkfailureRate() {
        return this.getIntProperty(BSNL_DENOMINATION_BENCHMARK_FAILURE_PERCENT);
    }

    public Integer getBsnlInvalidDenominationScanWindowDays() {
        return this.getIntProperty(BSNL_INVALID_DENOMINCATION_CHECK_DATA_WINDOW_DAYS);
    }

    public Integer getBsnlMaxFailureThreshold() {
        return this.getIntProperty(BSNL_MAX_FAILURE_THRESHOLD);
    }

    public Integer getInvalidDenomincationCheckDataWindowDays() {
        return this.getIntProperty(INVALID_DENOMINCATION_CHECK_DATA_WINDOW_DAYS);
    }

    public Integer getAirtelInvalidDenominationScanWindowDays() {
        return this.getIntProperty(AIRTEL_INVALID_DENOMINCATION_CHECK_DATA_WINDOW_DAYS);
    }
    
    public Integer getAllDenominationCheckDataWindowDays(){
    	return this.getIntProperty(ALL_DENOMINATION_DATA_WINDOW);
    }

    public Integer getAirtelMaxFailureThreshold() {
        return this.getIntProperty(AIRTEL_MAX_FAILURE_THRESHOLD);
    }

    public Integer getAirtelMaxFailurePlusSuccessThreshold() {
        return this.getIntProperty(AIRTEL_MAX_FAILURE_PULS_SUCCESS_THRESHOLD);
    }

    public Integer getAirtelBenchMarkFailureRate() {
        return this.getIntProperty(AIRTEL_DENOMINATION_BENCHMARK_FAILURE_PERCENT);
    }

    public Integer getInvalidDenomincationCheckSpecialDataWindowDays() {
        return this.getIntProperty(INVALID_DENOMINCATION_CHECK_SPECIAL_DATA_WINDOW_DAYS);
    }

    public int getNewCouponTimeWindow() {
        return this.getIntProperty(NEW_COUPON_TIME_WINDOW);
    }

    public int getPopularCouponCount() {
        return this.getIntProperty(POPULAR_COUPON_COUNT);
    }

    public String getConsumerSQSQueueEndPoint() {
        return this.getProperty(CONSUMER_SQS_ENDPOINT);
    }
    
    public String getConsumerSQSQueueMumbaiEndPoint() {
        return this.getProperty(CONSUMER_SQS_MUMBAI_ENDPOINT);
    }

    public int getSQSPollSleepPeriodSeconds() {
        return this.getIntProperty(SQS_POLL_SLEEP_PERIOD_SECONDS);
    }

    public boolean isYOptimaEnabled() {
        return this.getBooleanProperty(YOPTIMA_ENABLED);
    }

    public boolean isGoogleCodeEnabled() {
        return this.getBooleanProperty(GOOGLE_CODE_ENABLED);
    }

    public boolean isFacebookCodeEnabled() {
        return this.getBooleanProperty(FACEBOOK_CODE_ENABLED);
    }

    public String getCustomerSupportEmailAddress() {
        return this.getProperty(CUSTOMER_SUPPORT_EMAIL_ADDRESS);
    }

    public String getConsumerAwsAccessKey() {
        return getProperty(CONSUMER_AWS_ACCESS_KEY);
    }

    public String getConsumerAwsAccessSecret() {
        return getProperty(CONSUMER_AWS_ACCESS_SECRET);
    }

    public String getEmailAppAwsAccessKey() {
        return getProperty(EMAIL_APP_AWS_ACCESS_KEY);
    }

    public String getEmailAppAwsAccessSecret() {
        return getProperty(EMAIL_APP_AWS_ACCESS_SECRET);
    }

    public String getEmailAppAwsEndpoint() {
        return getProperty(EMAIL_APP_AWS_ENDPOINT);
    }

    public String getEmailAppSqsUrlPrefix() {
        return getProperty(EMAIL_APP_SQS_URL_PREFIX);
    }

    public String getEmailAppSqsQueueName() {
        return getProperty(EMAIL_APP_SQS_QUEUE_NAME);
    }

    public String getEmailIdOcwMigrationSuccessfulRefundToBank(){
        return getProperty(EMAIL_ID_OCW_MIGRATION_SUCCESSFUL_REFUND_TO_BANK);
    }

    public String getEmailIdOcwMigrationFailedRefundToBank(){
        return getProperty(EMAIL_ID_OCW_MIGRATION_FAILED_REFUND_TO_BANK);
    }

    public boolean isEmailAppSqsUrlHostnameAsSuffix() {
        return getBooleanProperty(EMAIL_APP_SQS_URL_HOSTNAME_AS_SUFFIX);
    }

    public int getEmailAppPercent() {
        return this.getIntProperty(EMAIL_APP_PERCENT);
    }

    public boolean isEmailAppAsyncEnabled() {
        return this.getBooleanProperty(EMAIL_APP_ASYNC_ENABLED);
    }

    public int getEmailAppAsyncPoolsize() {
        return this.getIntProperty(EMAIL_APP_ASYNC_POOLSIZE);
    }

    public String getTestimonialVersion() {
        return this.getProperty(TESTIMONIAL_VERSION_NO);
    }

    public String getDesktopImageResolution() {
        return this.getProperty(DESKTOP_IMAGE_RESOLUTION);
    }

    public String getMobileimageResolution() {
        return this.getProperty(MOBILE_IMAGE_RESOLUTION);
    }

    public String getAppimageResolution() {
        return this.getProperty(APP_IMAGE_RESOLUTION);
    }

    public String getAwsSnsAccessKey() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_ACCESS_KEY + ".test");
        }
        return this.getProperty(AWS_SNS_ACCESS_KEY);
    }

    public String getAwsSnsSecretKey() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_SECRET_KEY + ".test");
        }
        return this.getProperty(AWS_SNS_SECRET_KEY);
    }

    public String getAwsSnsRegion() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_REGION + ".test");
        }
        return this.getProperty(AWS_SNS_REGION);
    }

    public String getAwsSnsNewRegion() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_REGION + ".test");
        }
        return this.getProperty(AWS_SNS_REGION_NEW);
    }
    
    public String getAwsSnsProdAccessKey(){
        return this.getProperty(AWS_SNS_ACCESS_KEY);
    }

    public String getAwsSnsProdSecret(){
        return this.getProperty(AWS_SNS_SECRET_KEY);
    }

    public String getAwsSnsProdQaRegion() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_QA_REGION);
        }
        return this.getProperty(AWS_SNS_REGION);
    }

    public String getAwsSnsTopicArnRecharge() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_RECHARGE + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_RECHARGE);
    }

    public String getAwsSnsTopicArnRechargeAttempt() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_RECHARGE_ATTEMPT + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_RECHARGE_ATTEMPT);
    }  

    public String getAwsSnsTopicArnPayment() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_PAYMENT + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_PAYMENT);
    }

    public String getAwsSnsTopicArnLocation() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_LOCATION + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_LOCATION);
    }

    public String getAwsSnsTopicArnEmail() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_EMAIL_ID + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_EMAIL_ID);
    }

    public String getAwsSnsTopicArnProfileNo() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_PROFILE_NO + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_PROFILE_NO);
    }

    public String getAwsSnsTopicArnRechargeNo() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_RECHARGE_NO + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_RECHARGE_NO);
    }

    public String getAwsSnsTopicArnIMEI() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_IMEI + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_IMEI);
    }

    public String getAwsSnsTopicArnDeviceId() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_DEVICE_ID + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_DEVICE_ID);
    }

    public String getAwsSnsTopicArnPaymentAttempt() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_PAYMENT_ATTEMPT + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_PAYMENT_ATTEMPT);
    }

    public String getAwsSnsTopicArnWalletTxn() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_WALLET_TXN + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_WALLET_TXN);
    }

    public String getAwsSnsTopicArnRefund() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_REFUND + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_REFUND);
    }

    public String getAwsSnsTopicArnRechargePlan() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_RECHARGE_PLAN + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_RECHARGE_PLAN);
    }

    public String getAwsSnsTopicArnCampaign() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_CAMPAIGN + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_CAMPAIGN);
    }

    public String getAwsSnsTopicArnCampaignPromo() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_CAMPAIGN_PROMO + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_CAMPAIGN_PROMO);
    }

    public String getAwsSnsTopicInvalidPlanDenominations() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_INVALID_DENOMINATION_PLANS + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_INVALID_DENOMINATION_PLANS);
    }
    
    public int trackerApiConnectionTimeout() {
        return this.getIntProperty(TRACKER_API_CONNECTION_TIMEOUT);
    }

    public int trackerApiSocketTimeout() {
        return this.getIntProperty(TRACKER_API_SOCKET_TIMEOUT);
    }

    public int trackerApiMaximumConnections() {
        return this.getIntProperty(TRACKER_API_MAXIMUM_CONNECTIONS);
    }

    public int trackerApiMaximumConnectionsPerRoute() {
        return this.getIntProperty(TRACKER_API_MAXIMUM_CONNECTIONS_PER_ROUTE);
    }

    public int trackerApiThreadPoolSize() {
        return this.getIntProperty(TRACKER_API_THREAD_POOL_SIZE);
    }

    public int trackerApiRequestExecutionTime() {
        return this.getIntProperty(TRACKER_API_REQUEST_EXECUTION_TIME);
    }

    public long trackerApiConnManagerTimeout() {
        return this.getLongProperty(TRACKER_API_CONN_MANAGER_TIMEOUT);
    }

    public boolean isTrafficSplitEnabled() {
        return getBooleanProperty(TRAFFIC_SPLIT_ENABLED);
    }

    public String getTrackerApiUrl() {
        return this.getProperty(TRACKER_API_URL);
    }

    public String getPushNotificationUrl() {
        return this.getProperty(PUSH_NOTIFICATION_URL);
    }

    public int getMaxDenomRechargeHistory() {
        return this.getIntProperty(MAX_DENOM_IN_RECHARGE_HISTORY);
    }

    public int getMaxMobNumRechargeHistory() {
        return this.getIntProperty(MAX_NUMBER_IN_RECHARGE_HISTORY);
    }

    public String getMaxEntriesRechargeHistory() {
        return MAX_ENTRIES_IN_RECHARGE_HISTORY;
    }

    public String getIVRSQueue() {
        return this.getProperty(IVRS_QUEUE);
    }

    public String getKnowlarityQucikcallUrl() {
        return this.getProperty(KNOWLARITY_QUICKCALL_URL);
    }

    public String getKnowlarityUserName() {
        return this.getProperty(KNOWLARITY_USERNAME);
    }

    public String getKnowlarityPassword() {
        return this.getProperty(KNOWLARITY_PASSWORD);
    }

    public int getGupshupConnectionTimeout() {
        String propertyName = GUPSHUP_CONNECTION_TIMEOUT;
        if (this.isQaMode() || this.isDevMode()) {
            propertyName += ".test";
        }
        return this.getIntProperty(propertyName);
    }

    public int getGupshupReadTimeout() {
        String propertyName = GUPSHUP_READ_TIMEOUT;
        if (this.isQaMode() || this.isDevMode()) {
            propertyName += ".test";
        }
        return this.getIntProperty(propertyName);
    }

    public String getJusPayPaymentStatsUrl() {
        return this.getProperty(JUSPAY_PAYMENT_STATS_URL);
    }

    public int getJusPayPaymentStatsConnectTimeout() {
        return this.getIntProperty(JUSPAY_PAYMENT_STATS_CONNECT_TIMEOUT);
    }

    public int getJusPayPaymentStatsReadTimeout() {
        return this.getIntProperty(JUSPAY_PAYMENT_STATS_READ_TIMEOUT);
    }

    public int getRechargeDenomWindowHours() {
        return this.getIntProperty(RECHARGE_DENOM_INTERVAL);
    }

    public int getRechargeDenomMaxDays() {
        return this.getIntProperty(RECHARGE_DENOM_MAX_DAYS);
    }

    public int getRechargeDenomMinAttempts() {
        return this.getIntProperty(RECHARGE_DENOM_MIN_ATTEMPTS);
    }

    public float getRechargeDenomMinSuccessRate() {
        return this.getIntProperty(RECHARGE_DENOM_MIN_SUCCESS_RATE);
    }

    public int getNumPercentPlansToRecommend() {
        return this.getIntProperty(RECHARGE_DENOM_RECOMMEND_PERCENT);
    }

    public int getRechargeDenomDecayRate() {
        return this.getIntProperty(RECHARGE_DENOM_DECAY_RATE);
    }

    public String getFortknoxAccessKey() {
        return this.getProperty(FORTKNOX_ACCESS_KEY);
    }

    public String getFortknoxSecretKey() {
        return this.getProperty(FORTKNOX_SECRET_KEY);
    }

    public int getFortknoxReadTimeout() {
        return this.getIntProperty(FORTKNOX_READ_TIMEOUT);
    }

    public int getFortknoxConnectTimeout() {
        return this.getIntProperty(FORTKNOX_CONNECT_TIMEOUT);
    }

    public String getOnecheckHostUrl() {
        return this.getProperty(ONECHECK_HOST_URL);
    }

    public int getOneCheckConnectTimeout() {
        return this.getIntProperty(ONECHECK_CONNECT_TIMEOUT);
    }

    public int getOnecheckReadTimeout() {
        return this.getIntProperty(ONECHECK_READ_TIMEOUT);
    }

    public int getFlowPercentage() {
        return this.getIntProperty(FLOW_PERCENTAGE);
    }

    public String getAwsSnsTopicArnPromocodeRedeemDetails() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_PROMOCODE_REDEEM_DETAILS + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_PROMOCODE_REDEEM_DETAILS);
    }
    
    public String getAwsSnsTopicArnCreditRewardSuccessDetails() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_CREDIT_REWARD_SUCCESS_DETAILS + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_CREDIT_REWARD_SUCCESS_DETAILS);
    }

    public String getOneCheckFortknoxAccessKey() {
        return this.getProperty(ONE_CHECK_FORTKNOX_ACCESS_KEY);
    }

    public String getOneCheckFortknoxSecretKey() {
        return this.getProperty(ONE_CHECK_FORTKNOX_SECRET_KEY);
    }

    public String getApplicationCookieDomainName() {
        return this.getProperty(FCConstants.APPLICATION_COOKIE_DOMAIN_NAME);
    }
    
    public String getFCWalletAPIEndPoint() {
        return this.getProperty(FC_WALLET_API_END_POINT);
    }

    public String getVCardAPIEndPoint() {
        return this.getProperty(FC_VCARD_API_END_POINT);
    }

    public String getFinappVcRefundUrl() {
        return this.getProperty(FINAPP_VC_REFUND_URL);
    }

	public String getAwsSnsTopicArnRefundAttemptAlert() {
		if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_REFUND_ATTEMPT_ALERT + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_REFUND_ATTEMPT_ALERT);
	}

	public String getAwsSnsTopicArnRefundStatusAlert() {
		if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_REFUND_STATUS_ALERT + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_REFUND_STATUS_ALERT);
	}
	


	public String getAwsSnsTopicArnRevokeAlert() {
        if (isDevMode() || isQaMode()) {
            return this.getProperty(AWS_SNS_TOPIC_ARN_REVOKE_FUND_ALERT + ".test");
        }
        return this.getProperty(AWS_SNS_TOPIC_ARN_REVOKE_FUND_ALERT);
    }

	

	public Set<Object> getAntiFraudKeyNames(){
	    return antifraudProps.keySet();
	}
	
	public String getBdkWorkingKey() {
		return this.getProperty(BDK_WORKING_KEY);
	}

	public String getBdkUrl() {
		return this.getProperty(BDK_URL);
	}

	public String getBdkTimeout() {
		return this.getProperty(BDK_TIMEOUT);
	}

	public boolean putAntiFraudProperty(String key, Object value){
	    boolean returnVal = false;
	    boolean loadAntiFraudPropsFromFile = appConfig.isLoadAntiFraudPropsFromFileEnabled();
	    try {
    	    if(!loadAntiFraudPropsFromFile && ccs!=null){
                ccs.putConf(key, value);
    	        returnVal = true;
    	    }
	    } catch (NotSerializableException| CCSException e) {
	        logger.error("Not able to put "+key+" property.", e);
	    }
	    return returnVal;
	}
	
	public String getWalletRedirectUrl() {
		return this.getProperty(FC_WALLET_REDIRECT_URL);
	}

	public boolean isAccessControlEnabled() {
        return this.getBooleanProperty(ADMIN_ACS_ENABLED);
    }

    public String getOnecheckClientKey() {
        return this.getProperty(ONECHECK_CLIENT_KEY);
    }

    public String getOnecheckClientId() {
        return this.getProperty(ONECHECK_CLIENT_ID);
    }

    public String getRechargeAsRewardQueue() {
        return this.getProperty(RECHARGE_REWARD_QUEUE);
    }
    
    public String getRechargeAsRewardResponseQueue() {
        return this.getProperty(RECHARGE_REWARD_RESPONSE_QUEUE);
    }
    

    public String getCCSHost() {
        return this.getProperty(CCS_HOST);
    }

    public String getCCSBasePathPayment() {
        return this.getProperty(CCS_BASEPATH_PAYMENT);
    }
    
    public String getCCSBasePathSurveyUrl() {
        return this.getProperty(CCS_BASEPATH_SURVEY_URL);
    }
    
    public String getCCSBasePathRefund() {
        return this.getProperty(CCS_BASEPATH_FORCE_REFUND);
    }
    
    public String getCCSBasePathFulfillment(){
    	return this.getProperty(CCS_BASEPATH_FULFILMENT);
    }
    
    public String getActiveConsumer() {
        return this.getProperty(ENABLED_CONSUMERS);
    }

    public String getCcsIciciQcList() {
        return CCS_ICICI_QC_LIST;
    }
    
    public String getSurveyUrlKey() {
        return CCS_SURVEY_URL_KEY;
    }
    
    public String getRefundProductListKey() {
        return CCS_FORCE_REFUND_PRODUCT_LIST_KEY;
    }
    
    public String getRefundChannelListKey() {
        return CCS_FORCE_REFUND_CHANNEL_LIST_KEY;
    }
    
    public String getRefundAggregatorListKey() {
        return CCS_FORCE_REFUND_AGGREGATOR_LIST_KEY;
    }
    
    public String getRefundOperatorListKey() {
        return CCS_FORCE_REFUND_OPERATOR_LIST_KEY;
    }
    
    public String getRefundOrderAgeMinKey() {
        return CCS_FORCE_REFUND_ORDER_AGE_MIN_KEY;
    }
    
    public String getForceRefundMaxAmountPerDayKey() {
        return CCS_FORCE_REFUND_MAX_LIMIT_PER_DAY;
    }


    public String getCcsDebitAtmList() {
        return CCS_DEBIT_ATM_LIST;
    }
    
    public String getCcsITTPProductsList() {
        return CCS_ITTP_PRODUCTS_LIST;
    }
    
    public String getBinsUrl(){
        return this.getProperty(BINS_URL_KEY);
    }


	public String getPaymentRetryProductsList() {
		return CCS_PAYMENT_RETRY_LIST;
	}
	
	public String getUtilityProductsList(){
	    return CCS_UTILITY_PRODUCTS_LIST;
	}
	public String getUtilityProductsListKey() {
        return UTILITY_PRODUCTS_LIST_KEY;
    }

    public String getBeneficiaryVpaList() {
        return UPI_BENEFICIARY_VPA_KEY;
    }

    public String getStackId(){
    	return this.getProperty(FREECHARGE_STACK_ID);
    }

    public Integer getDesktopABTestExperimentId() {
        return Integer.parseInt(this.getProperty(FREECHARGE_DESKTOP_AB_TEST));
    }

    public Integer getNewSiteBucketId() {
        return Integer.parseInt(this.getProperty(FREECHARGE_DESKTOP_NEW_SITE_BUCKET_ID));
    }
    
    public String getNewSiteUrl() {
        return this.getProperty(FREECHARGE_DESKTOP_NEW_SITE_URL);
    }

    public String getAutoPayListUrl() {
        return this.getProperty(USER_AUTOPAY_LIST_URL);
    }
    
    public String getAutoPayBaseUrl() {
        return this.getProperty("user.autopay.base.url");
    }
    
	public String getOperationsAccessKey() {
		return this.getProperty(OPERATIONS_ACCESS_KEY);
	}

	public String getOperationsSecretKey() {
		return this.getProperty(OPERATIONS_SECRET_KEY);
	}

	public String getRefundOrderAgeMaxKey() {
		return CCS_FORCE_REFUND_ORDER_AGE_MAX_KEY;
	}
	
	public String getFulfillmentSucceesNotificationProductKey(){
		return CCS_FULFILLMENT_FULFILLMENT_PRODUCTS_KEY;
	}

	public String getAndroidNotificationQueue() {
		return this.getProperty(ANDROID_NOTIFICATION_QUEUE);
	}

	public String getNotificationEndPoint() {
		return this.getProperty(NOTIFICATION_QUEUE_ENDPOINT);
	}

    public String getAggregatorIp() {
        return this.getProperty(AGGREGATOR_IP);
    }

    public String getAggregatorPort() {
        return this.getProperty(AGGREGATOR_PORT);
    }

    public int getAggregatorTimeout() {
        return this.getIntProperty(AGGREGATOR_TIMEOUT);
    }

    public String getAggregatorClientId() {
        return this.getProperty(AGGREGATOR_CLIENT_ID);
    }

    public String getAggregatorClientKey() {
        return this.getProperty(AGGREGATOR_CLIENT_KEY);
    }

    public String getAggregatorMerchantId() {
        return this.getProperty(AGGREGATOR_MERCHANT_ID);
    }

}
