package com.freecharge.common.framework.properties;

import java.util.List;
import java.util.Map;

public interface IAppStatePersistence {

	public Object getAppValue(String key);

	public void setAppValue(String key, Object value);

	public void deleteAppValue(String key);

    public void setCustomAppValue(String key, Object value);

    public List<Map<String, Object>> getCustomProps();

}
