package com.freecharge.common.framework.properties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 9/18/13
 * Time: 2:21 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository("appStateDbDao")
public class AppStateDBDao implements IAppStatePersistence {
    private NamedParameterJdbcTemplate jt;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jt = new NamedParameterJdbcTemplate(dataSource);
    }
    @Override
    public Object getAppValue(String key) {
        String sql = "select app_value from "+table+" where app_key = :appKey";
        Map<String, Object> params = new HashMap<>();
        params.put("appKey", key);
        try {
            Map<String, Object> result = jt.queryForMap(sql, params);
            if (result!=null && result.size()>0){
                return result.get("app_value");
            }
            return null;
        }catch (DataAccessException de) {
            logger.warn("AppConfig value for Key: "+key+" is not in DB");
            return null;
        }
    }

    @Override
    public void setAppValue(String key, Object value) {
        String sql = "insert into "+table+ " (app_key, app_value) values (:key, :value) on duplicate key update " +
                "app_key=values(app_key), app_value=values(app_value)" ;
        Map<String, Object> params = new HashMap<>();
        params.put("key", key);
        params.put("value", value);
        this.jt.update(sql, params);
    }

    @Override
    public void deleteAppValue(String key) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setCustomAppValue(String key, Object value) {
        String sql = "insert into "+table+ " (app_key, app_value, prop_type) values (:key, :value, :propType) on duplicate key update " +
                "app_key=values(app_key), app_value=values(app_value), prop_type=values (prop_type)" ;
        Map<String, Object> params = new HashMap<>();
        params.put("key", key);
        params.put("value", value);
        params.put("propType", "C");
        this.jt.update(sql, params);
    }

    @Override
    public List<Map<String, Object>> getCustomProps() {
        String sql = "select * from " + table + " where prop_type = :propType;";
        Map<String, Object> params = new HashMap<>();
        params.put("propType", "C");
        return this.jt.queryForList(sql, params);
    }

    private static final Logger logger = LoggingFactory.getLogger(AppStateDBDao.class);
    private static final String table = "app_config";
}
