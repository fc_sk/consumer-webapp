package com.freecharge.common.framework.properties;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import com.freecharge.common.util.FCStringUtils;

public class FCPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

	private FCProperties fcProperties;

	// instrument the placeholder configurer with the actual Properties of FCProperties
	public void setProperties(FCProperties fcp) {
	    fcProperties = fcp;
		super.setProperties(fcp.getProperties());
	}

	static Properties properties;

	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		try {

		    properties = mergeProperties();

			// Override the password properties again.
			if (fcProperties.isPreProdMode()) {
			    properties = fcProperties.loadPasswordProperties(FCProperties.preProdPasswordsFile, properties);
                        } else if (fcProperties.isE2eMode()) {
                            properties = fcProperties.loadPasswordProperties(FCProperties.e2ePasswordsFile, properties);
			} else if (fcProperties.isProdMode()) {
			    // this is true only for prod mode.
			   properties = fcProperties.loadPasswordProperties(FCProperties.prodPasswordsFile, properties);
			}

			convertProperties(properties);
			processProperties(beanFactory, properties);

		} catch (IOException ex) {
			// throw new BeanInitializationException("Could not load properties", ex);
			ex.printStackTrace();
		}
	}

	public static String getStringValueStatic(String key) {
		return properties.getProperty(key);
	}

	public static List<String> getPropertyValueList(String key, String delimiter) {

		String propertyValue = getStringValueStatic(key);
		List<String> propertyValueList = FCStringUtils.getStringList(propertyValue, delimiter);

		return propertyValueList;
	}

}
