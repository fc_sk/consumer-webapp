package com.freecharge.common.framework.properties;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.platform.metrics.MetricsClient;

@Repository("appStateRedisDao")
public class AppStateDAORedis implements IAppStatePersistence {
    private static final Logger logger = LoggingFactory.getLogger(AppStateDAORedis.class);

	private static final String APPLICATION_KEY_PREFIX = "app_";

	@Autowired
	@Qualifier("appRedisTemplate")
	private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    @Qualifier("appStateDbDao")
    IAppStatePersistence appStateDBDao;
    
    @Autowired
    MetricsClient metricsClient;

    private final String SERVICE_NAME = "AppStateDAORedis";
    
    private final String NULL_VALUE_REPRESENTATION = "cmVkaXMgZGVhZCBiZWVm";

	@Override
	public Object getAppValue(String key) {
        Object appValue = getFromCache(key);
        
        if(appValue==null){
            appValue = appStateDBDao.getAppValue(key);
            if (appValue!=null){
                //checking 0 or 1 in app value for boolean cast
                String appValueStr = (String)appValue;
                if (appValueStr.length() == 1){
                    if (appValueStr.equals("1")){
                        appValue = true;
                    }else {
                        appValue = false;
                    }
                }
                setInCache(key, appValue);
            } else {
            	setValueAsNullInCache(key);
            }
        } else if (isNullRepresentationValue(appValue)) {
            return null;
        }
        return appValue;
	}

	private void setValueAsNullInCache(String key) {
		setInCache(key, NULL_VALUE_REPRESENTATION);
	}

	private boolean isNullRepresentationValue(Object appValue) {
		return NULL_VALUE_REPRESENTATION.equals(appValue);
	}

	@Override
	public void setAppValue(String key, Object value) {
        this.appStateDBDao.setAppValue(key, value);
        this.setInCache(key, value);
	}

	@Override
	public void deleteAppValue(String key) {
		try {
		    redisTemplate.delete(APPLICATION_KEY_PREFIX + key);
            metricsClient.recordEvent(SERVICE_NAME, "Delete", "Success");
        } catch (RedisConnectionFailureException e) {
            metricsClient.recordEvent(SERVICE_NAME, "Delete", "RedisConnectionFailureException");
            logger.error("RedisConnectionFailureException while deleting appvalue from cache for key: "+key, e);
        } catch (Exception e) {
            metricsClient.recordEvent(SERVICE_NAME, "Delete", "Exception");
            logger.error("Exception while deleting appvalue from cache for key: "+key, e);
        }
	}

    @Override
    public void setCustomAppValue(String key, Object value) {
        this.appStateDBDao.setCustomAppValue(key, value);
        this.setInCache(key, value);
    }

    @Override
    public List<Map<String, Object>> getCustomProps() {
        //No caching here
        return this.appStateDBDao.getCustomProps();
    }

    private void setInCache(String key, Object appValue) {
        try {
            redisTemplate.opsForValue().set(APPLICATION_KEY_PREFIX + key, appValue);
            metricsClient.recordEvent(SERVICE_NAME, "Set", "Success");
        } catch (RedisConnectionFailureException e) {
            metricsClient.recordEvent(SERVICE_NAME, "Set", "RedisConnectionFailureException");
            logger.error("RedisConnectionFailureException while setting appvalue "+appValue+" in cache for key: "+key, e);
        } catch (Exception e) {
            metricsClient.recordEvent(SERVICE_NAME, "Set", "Exception");
            logger.error("Exception while setting appvalue "+appValue+" in cache for key: "+key, e);
        }
    }

    private Object getFromCache(String key) {
        Object appValue = null;
        try {
            appValue = redisTemplate.opsForValue().get(APPLICATION_KEY_PREFIX + key);
            metricsClient.recordEvent(SERVICE_NAME, "Get", "Success");
        } catch (RedisConnectionFailureException e) {
            metricsClient.recordEvent(SERVICE_NAME, "Get", "RedisConnectionFailureException");
            logger.error("RedisConnectionFailureException while getting appvalue from cache for key: "+key, e);
        } catch (Exception e) {
            metricsClient.recordEvent(SERVICE_NAME, "Get", "Exception");
            logger.error("Exception while getting appvalue from cache for key: "+key, e);
        }
        return appValue;
    }
}
