package com.freecharge.common.framework.properties;

import java.io.IOException;
import java.util.Properties;

import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

public class Env implements ResourceLoaderAware {

    private static final String ENV_PROPERTIES = "system.properties";
    private static final String MODE_PROPERTY_KEY = "system.ctx.mode";

    public static final String MODE_PROD = "prod";
    public static final String MODE_DEV = "dev";
    public static final String MODE_QA = "qa";
    public static final String MODE_E2E = "e2e";
    public static final String MODE_PREPROD = "preProd";
    public static final String MODE_AWSPROD = "awsProd";

    private String mode = MODE_DEV;

    public void setResourceLoader(ResourceLoader resourceLoader) {
        Properties defaults = new Properties();
        Resource resource = resourceLoader.getResource(ResourceLoader.CLASSPATH_URL_PREFIX + ENV_PROPERTIES);
        if (resource.exists()) {
            try {
                defaults.load(resource.getInputStream());
            } catch (IOException ioe) {
                System.out.println("Could not load properties file");
                System.exit(-1);
            }
        } else {
            System.out.println("Could not load properties file");
            System.exit(-1);
        }
        Properties props = new Properties(defaults);
        props.putAll(System.getProperties()); // override with system properties

        this.setMode(props.getProperty(MODE_PROPERTY_KEY));
    }

    public void setMode(String mode) {
        this.mode = (mode == null) ? "dev" : mode;
    }

    public String getMode() {
        return mode;
    }

    public boolean isProdMode() {
        return MODE_PROD.equals(mode);
    }

    public boolean isPreProdMode() {
        return MODE_PREPROD.equals(mode);
    }

    public boolean isDevMode() {
        return MODE_DEV.equals(mode);
    }

    public boolean isQaMode() {
        return MODE_QA.equals(mode);
    }
 
    public boolean isE2eMode() {
        return MODE_E2E.equals(mode);
    }
    
    public boolean isAWSProdMode() {
        return MODE_AWSPROD.equals(mode);
    }
}
