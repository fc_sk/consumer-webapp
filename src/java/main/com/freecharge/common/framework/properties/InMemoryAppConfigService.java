package com.freecharge.common.framework.properties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.model.CouponMerchant;
import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.util.JsonUtil;

@Service
public class InMemoryAppConfigService {

    private final Logger logger = LoggingFactory.getLogger(getClass());
    public static  Boolean configurableCouponCategoriesEnabled = null;
    public static  Boolean couponFraudCheck = null;
    public static  Boolean couponFraudCookie = null;
    public static  Boolean couponFraudIP  = null;
    public static  Boolean couponFraudEmail  = null;
    public static  Boolean couponFraudProfileNo  = null;
    public static  Boolean couponServiceProfilingEnabled  = null;
    private static Boolean isMongoLogSizeEnabled;
    public static Map<Integer, Integer> couponFraudMaxCountForCookie = null;
    public static Map<Integer, Integer> couponFraudMaxCountForIP = null;
    public static Map<Integer, Integer> couponFraudMaxCountForProfile = null;
    public static Map<Integer, Integer> couponFraudMaxCountForEmail = null;
    public static Map<Integer, Integer> couponFraudMaxCountForImei = null;
    public static List<Integer> couponIdsForFraudCheck = null;
    public static Map<Integer, Integer> couponFraudMaxCountForUserId = null;
    private Boolean isCouponRemotingEnabled = null;
    
    
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    
    private List<CouponMerchant> couponMerchants;
    
    @Autowired
	private AppConfigService appConfigService;
    
    @Autowired
    private FCProperties fcProperties;
    
    @Autowired
	@Qualifier("couponServiceProxy")
	private ICouponService couponServiceProxy;
    
    public List<CouponMerchant> getCouponMerchants(){
    	if (couponMerchants == null){
    		reInitCouponMerchants();
    	}// end if
    	return couponMerchants;
    }

    public void reInitCouponMerchants(){
    	try {
			couponMerchants = couponServiceProxy.getAllMerchants();
		} catch (Exception e) {
			logger.error(
					"error occur while calling merchantsDAO.getAllMerchants() in the getVouchersMap() method :"
							+ e, e);
		}
    }
    
    public Boolean isConfigurableCouponCategoriesEnabled() {
    	if (configurableCouponCategoriesEnabled == null){
    		configurableCouponCategoriesEnabled = appConfigService.isConfigurableCouponCategoriesEnabled();
    	}
    	return configurableCouponCategoriesEnabled;
	}
	
	public void setConfigurableCouponCategoriesEnabled(Boolean val) {
		configurableCouponCategoriesEnabled = val;
	}
	
    public boolean isCouponFraudCheckEnabled() {
    	if(couponFraudCheck == null){
    		couponFraudCheck = appConfigService.isCouponFraudCheckEnabled();
     	}
        return couponFraudCheck;
    }
    public void  setCouponFraudCheckEnabled(Boolean flag) {
        couponFraudCheck = flag;
    }
    
    public boolean isCouponFraudCookieEnabled() {
    	if(couponFraudCookie == null){
    		couponFraudCookie = appConfigService.isCouponFraudCookieEnabled();
    	}
        return couponFraudCookie;    
    }
    public void setCouponFraudCookieEnabled(Boolean flag) {
    	couponFraudCookie = flag;
    }
    
    public boolean isCouponFraudIPEnabled() {
    	if(couponFraudIP == null){
    		couponFraudIP = appConfigService.isCouponFraudIPEnabled();
    	}
        return couponFraudIP;
    }
    public void setCouponFraudIPEnabled(Boolean flag) {
        couponFraudIP =  flag;
    }
    
    public boolean isCouponFraudEmailEnabled() {
    	if(couponFraudEmail == null){
    		couponFraudEmail = appConfigService.isCouponFraudEmailEnabled();
     	}
        return couponFraudEmail;
    }
    public void setCouponFraudEmailEnabled(Boolean flag) {
        couponFraudEmail = flag;
    }
    
    public boolean isCouponFraudProfileNoEnabled() {
    	if(couponFraudProfileNo == null){
    		couponFraudProfileNo = appConfigService.isCouponFraudProfileNoEnabled();
     	}
        return couponFraudProfileNo; 
    }
    public void setCouponFraudProfileNoEnabled(Boolean flag) {
        couponFraudProfileNo =  flag ;
    }
    public boolean isCouponServiceProfilingEnabled() {
    	if(couponServiceProfilingEnabled == null){
    		couponServiceProfilingEnabled = fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_ENABLED);
     	}
        return couponServiceProfilingEnabled; 
    }
    public void setCouponServiceProfilingEnabled(Boolean flag) {
    	couponServiceProfilingEnabled =  flag ;
    }
    
    public boolean isMongoLogSizeEnabled() {
    	if(isMongoLogSizeEnabled == null){
    		isMongoLogSizeEnabled = appConfigService.isMongoSizeRecordEnabled();
     	}
        return isMongoLogSizeEnabled; 
    }
    public void setMongoLogSizeEnabled(Boolean flag) {
    	isMongoLogSizeEnabled =  flag ;
    }
    
    public List<Integer> getCouponIdsForFraudCheck(){
    	if(couponIdsForFraudCheck == null){
    		couponIdsForFraudCheck  = appConfigService.getCouponIdsForFraudCheck();
    	}
    	return couponIdsForFraudCheck;
    }
    public void setCouponIdsForFraudCheck(List<Integer> couponIds){
    	couponIdsForFraudCheck = couponIds;
    }
    
    public Map<Integer, Integer> getCouponFraudMaxCountForCookie(){
    	if(couponFraudMaxCountForCookie == null){
    		couponFraudMaxCountForCookie = appConfigService.getCouponFraudMaxCountForCookie();
    	}
    	return couponFraudMaxCountForCookie;
    }
    public void setCouponFraudMaxCountForCookie(Map<Integer, Integer> couponFraudCounts){
    	couponFraudMaxCountForCookie = couponFraudCounts;
    }
    
    public Map<Integer, Integer> getCouponFraudMaxCountForIP(){
    	if(couponFraudMaxCountForIP == null){
    		couponFraudMaxCountForIP = appConfigService.getCouponFraudMaxCountForIP();
    	}
    	return couponFraudMaxCountForIP;
    }
    public void setCouponFraudMaxCountForIP(Map<Integer, Integer> couponFraudCounts){
    	couponFraudMaxCountForIP = couponFraudCounts;
    }
    
    public Map<Integer, Integer> getCouponFraudMaxCountForProfile(){
    	if(couponFraudMaxCountForProfile == null){
    		couponFraudMaxCountForProfile = appConfigService.getCouponFraudMaxCountForProfile();
    	}
    	return couponFraudMaxCountForProfile;
    }
    public void setCouponFraudMaxCountForProfile(Map<Integer, Integer> couponIds){
    	couponFraudMaxCountForProfile = couponIds;
    }
    
    public Map<Integer, Integer> getCouponFraudMaxCountForEmail(){
    	if(couponFraudMaxCountForEmail == null){
    		couponFraudMaxCountForEmail = appConfigService.getCouponFraudMaxCountForEmail();
    	}
    	return couponFraudMaxCountForEmail;
    }
    public void setCouponFraudMaxCountForEmail(Map<Integer, Integer> couponIds){
    	couponFraudMaxCountForEmail = couponIds;
    }
    
    public Map<Integer, Integer> getCouponFraudMaxCountForUserId(){
    	if(couponFraudMaxCountForUserId == null){
    		couponFraudMaxCountForUserId = appConfigService.getCouponFraudMaxCountForUserId();
    	}
    	return couponFraudMaxCountForUserId;
    }

    public void setCouponFraudMaxCountForUserId(Map<Integer, Integer> couponIds){
    	couponFraudMaxCountForUserId = couponIds;
    }
    public Map<Integer, Integer> getCouponFraudMaxCountForImei(){
    	if(couponFraudMaxCountForImei == null){
    		couponFraudMaxCountForImei = appConfigService.getCouponFraudMaxCountForImei();
    	}
    	return couponFraudMaxCountForImei;
    }
    public void setCouponFraudMaxCountForImei(Map<Integer, Integer> couponIds){
    	couponFraudMaxCountForImei = couponIds;
    }
    
    public void reloadCacheFromRedis(){
    	setCouponServiceProfilingEnabled(fcProperties.getAntiFraudBooleanProperty(FCConstants.ANTIFRAUD_FRAUDFRESH_ENABLED));
		setCouponFraudCheckEnabled(appConfigService.isCouponFraudCheckEnabled());
		setCouponFraudCookieEnabled(appConfigService.isCouponFraudCookieEnabled());
		setCouponFraudIPEnabled(appConfigService.isCouponFraudIPEnabled());
		setCouponFraudEmailEnabled(appConfigService.isCouponFraudEmailEnabled());
		setCouponFraudProfileNoEnabled(appConfigService.isCouponFraudProfileNoEnabled());
		setCouponFraudMaxCountForCookie(appConfigService.getCouponFraudMaxCountForCookie());
		setCouponFraudMaxCountForIP(appConfigService.getCouponFraudMaxCountForIP());
		setCouponFraudMaxCountForProfile(appConfigService.getCouponFraudMaxCountForProfile());
		setCouponFraudMaxCountForEmail(appConfigService.getCouponFraudMaxCountForEmail());
		setCouponFraudMaxCountForImei(appConfigService.getCouponFraudMaxCountForImei());
		setCouponIdsForFraudCheck(appConfigService.getCouponIdsForFraudCheck());
		setConfigurableCouponCategoriesEnabled(appConfigService.isConfigurableCouponCategoriesEnabled());
		setCouponFraudMaxCountForUserId(appConfigService.getCouponFraudMaxCountForUserId());
		setMongoLogSizeEnabled(appConfigService.isMongoSizeRecordEnabled());
		setCouponRemotingEnabled(appConfigService.isCouponRemotingEnabled());
		reInitCouponMerchants();
	}
    
    public String getInMemoryCacheValues(){
    	Map<String,Object> values = new HashMap<String, Object>();
    	values.put("couponFraudCheck", couponFraudCheck );
    	values.put("couponFraudCookie", couponFraudCookie );
    	values.put("couponFraudIP", couponFraudIP );
    	values.put("couponFraudEmail", couponFraudEmail);
    	values.put("couponFraudProfileNo", couponFraudProfileNo);
    	values.put("couponServiceProfilingEnabled", couponServiceProfilingEnabled);
    	values.put("couponFraudMaxCountForCookie", couponFraudMaxCountForCookie);
    	values.put("couponFraudMaxCountForIP", couponFraudMaxCountForIP );
    	values.put("couponFraudMaxCountForProfile", couponFraudMaxCountForProfile );
    	values.put("couponFraudMaxCountForEmail", couponFraudMaxCountForEmail);
    	values.put("couponFraudMaxCountForImei", couponFraudMaxCountForImei);
    	values.put("couponIdsForFraudCheck", couponIdsForFraudCheck);
    	values.put("isMongoLogSizeEnabled", isMongoLogSizeEnabled);
    	values.put("couponFraudMaxCountForUserId", couponFraudMaxCountForUserId);
    	values.put("isCouponRemotingEnabled", isCouponRemotingEnabled);
    	return JsonUtil.getJson(values);
    }
    
    @PostConstruct
    public void scheduleCacheRefresh(){
    	if (!BatchJobUtil.isBatchMode()){
    		try{
            	scheduler.scheduleAtFixedRate(new  java.lang.Runnable() {
        			
        			@Override
        			public void run() {
        				try{
        					reloadCacheFromRedis();
        				}catch(Exception e){
        					logger.error("An exception occured during in-memory cache refresh", e);
        				}
        				
        			}
        		}, 0, 60*5, TimeUnit.SECONDS);
        	} catch(Exception e){
        		logger.warn("Thread failed to initliaze InMemoryAppConfigService in @PostConstruct.", e);
        	}    		
    	}
    }
    
    protected void finalize() throws Throwable { 
        super.finalize(); 
        logger.info("Stopping in-memory cache cleanUp"); 
        scheduler.shutdown(); 
    }

	public Boolean isCouponRemotingEnabled() {
		if (isCouponRemotingEnabled == null){
			isCouponRemotingEnabled = appConfigService.isCouponRemotingEnabled();
		}
		return isCouponRemotingEnabled;
	}

	public void setCouponRemotingEnabled(
			Boolean isCouponRemotingEnabled) {
		this.isCouponRemotingEnabled = isCouponRemotingEnabled;
	}

}
