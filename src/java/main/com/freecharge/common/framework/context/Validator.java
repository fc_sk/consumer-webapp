package com.freecharge.common.framework.context;

import java.util.Map;

import com.freecharge.common.framework.basedo.BaseWebDO;
import com.freecharge.common.framework.exception.FCValidationException;

public interface Validator
{

    public Map<String, String> validate(BaseWebDO actionForm) throws FCValidationException;
}
