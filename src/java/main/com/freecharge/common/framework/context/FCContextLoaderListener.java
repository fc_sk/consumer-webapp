package com.freecharge.common.framework.context;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;
import org.springframework.web.context.ContextLoaderListener;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.FCUtil;

public class FCContextLoaderListener extends ContextLoaderListener
{
    static File m_homePath;
    private static Logger logger;
    public static ServletContext servletContext;

    public static File getHomePath()
    {
        return m_homePath;
    }

    /**
     * Initialize the root web application context.
     */
    public void contextInitialized(ServletContextEvent event)
    {
        if (m_homePath == null)
        {
            m_homePath = new File(event.getServletContext().getRealPath("/"));
            System.out.println("FreeCharge Server starting up...");
        }
        servletContext = event.getServletContext();
        Properties props = getPropertiesMap();
        setSystemEnvProperties(props);
        initializeLogger();
        System.out.println("Running FreeCharge version " + FCUtil.getBuildVersionNumber(getClass()));

        logger.info("Logger initialized ...");

        super.contextInitialized(event);
        logger.info("Listener context initialized ...");
    }


    private void initializeLogger(){
        try{
            logger = LoggingFactory.getLogger(FCContextLoaderListener.class.getName());
        }catch(Exception exp){
            System.out.println("Problem initiallizing logger ....."+exp.getMessage());
        }
    }

    public static Properties getPropertiesMap() {
        Properties systemProperties = null;
      if (systemProperties == null) {
        systemProperties = new Properties();
        try {
            InputStream inputStream = FCContextLoaderListener.class.getClassLoader().getResourceAsStream("system.properties");
          systemProperties.load(inputStream);
        }catch(IOException expIO){
          systemProperties = null;
          System.out.println("................not finding the property file....");
        }
      }
      return systemProperties;
    }

    private void setSystemEnvProperties(Properties props){
        System.setProperty(FCConstants.SYSTEM_RUNNING_MODE, props.getProperty(FCConstants.SYSTEM_RUNNING_MODE));
        this.setTomcatHome();
    }

    private void setTomcatHome() {
        for (Map.Entry<String, String> entry : System.getenv().entrySet()) {
            String key = entry.getKey();
            if ("CATALINA_HOME".equals(key)) {
                System.setProperty(entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * Close the root web application context.
     */
    public void contextDestroyed(ServletContextEvent event)
    {
        super.contextDestroyed(event);
    }

    public static void main(String[] args) {
        FCContextLoaderListener fcContextLoaderListener = new FCContextLoaderListener();
        fcContextLoaderListener.setTomcatHome();
    }
}
