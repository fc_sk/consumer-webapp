package com.freecharge.common.framework.context;

public interface WebDelegate {

    public void delegate(WebContext webContext) throws Exception;
}
