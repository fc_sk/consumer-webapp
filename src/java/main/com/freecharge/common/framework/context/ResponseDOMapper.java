package com.freecharge.common.framework.context;

import com.freecharge.common.framework.basedo.BaseResponseDO;
import com.freecharge.common.framework.basedo.BaseWebDO;

public interface ResponseDOMapper <T extends BaseWebDO, S extends BaseResponseDO> {

	public T convertBusinessDOtoWebDO(S baseResponseDO, WebContext webContext);
	public S convertWebDOToResponseDO(T baseWebDO, WebContext webContext);
}