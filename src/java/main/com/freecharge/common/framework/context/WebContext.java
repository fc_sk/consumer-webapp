package com.freecharge.common.framework.context;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;

import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseResponseDO;
import com.freecharge.common.framework.basedo.BaseWebDO;

public class WebContext {

    private long id;

    private BaseBusinessDO baseBusinessDO;
    private BaseWebDO baseWebDO;
    private BaseResponseDO baseResponseDO;

    private WebDOMapper webDOMapper;
    private ResponseDOMapper responseDOMapper;
    
    private WebDelegate webDelegate;

    private HttpServletRequest request;
    private HttpServletResponse response;
    private Model model;

    private Validator validator;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BaseBusinessDO getBaseBusinessDO() {
        return baseBusinessDO;
    }

    public void setBaseBusinessDO(BaseBusinessDO baseBusinessDO) {
        this.baseBusinessDO = baseBusinessDO;
    }

    public WebDOMapper getWebDOMapper() {
        return webDOMapper;
    }

    public void setWebDOMapper(WebDOMapper webDOMapper) {
        this.webDOMapper = webDOMapper;
    }

    public WebDelegate getWebDelegate() {
        return webDelegate;
    }

    public void setWebDelegate(WebDelegate webDelegate) {
        this.webDelegate = webDelegate;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Validator getValidator() {
        return validator;
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    public BaseWebDO getBaseWebDO() {
        return baseWebDO;
    }

    public void setBaseWebDO(BaseWebDO baseWebDO) {
        this.baseWebDO = baseWebDO;
    }

	public BaseResponseDO getBaseResponseDO() {
		return baseResponseDO;
	}

	public void setBaseResponseDO(BaseResponseDO baseResponseDO) {
		this.baseResponseDO = baseResponseDO;
	}

	public ResponseDOMapper getResponseDOMapper() {
		return responseDOMapper;
	}

	public void setResponseDOMapper(ResponseDOMapper responseDOMapper) {
		this.responseDOMapper = responseDOMapper;
	}
}