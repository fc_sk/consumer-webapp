package com.freecharge.common.framework.context;

import com.freecharge.common.framework.basedo.BaseBusinessDO;
import com.freecharge.common.framework.basedo.BaseWebDO;

public interface WebDOMapper<T extends BaseWebDO, S extends BaseBusinessDO> {

    public T convertBusinessDOtoWebDO(S baseBusinessDO, WebContext webContext);
    public S convertWebDOToBusinessDO(T baseWebDO, WebContext webContext);
    public void mergeWebDos(T oldBaseWebDO,T newBaseWebDO);
    public T createWebDO(T baseWebDO);

}
