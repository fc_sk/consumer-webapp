package com.freecharge.common.framework.logging;

import org.apache.log4j.Logger;

public class LoggingFactory {
    public static final String REFUND_LOGGER_PREFIX = "refund";
    public static final String WALLET_LOGGER_PREFIX = "wallet";
    public static final String ZEROPAY_LOGGER_PREFIX = "zeropay";
    public static final String BILL_PAYMENT_REFUND_LOGGER_PREFIX = "billPaymentRefund";
    public static final String PLAN_RETRY_LOGGER_PREFIX = "planRetry";
    public static final String TPT_VALIDATION_LOGGER_PREFIX = "tptValidation";
	private static final String SCHEDULE_INFO_LOGGER_PREFIX = "scheduleInfoLog";

    public static Logger getLogger(String loggerName) {
        Logger logger = Logger.getLogger(loggerName);
        return logger;
    }

    public static Logger getLogger(Class className) {
        return Logger.getLogger(className);
    }

    public static String getRefundLoggerName(String str) {
        return REFUND_LOGGER_PREFIX + "." + str;
    }
    
    public static String getBillPaymentRefundLoggerName(String str) {
        return BILL_PAYMENT_REFUND_LOGGER_PREFIX + "." + str;
    }

    public static String getWalletLoggerName(String str) {
        return WALLET_LOGGER_PREFIX + "." + str;
    }
    
    public static String getZeropayLoggerName(String str) {
        return ZEROPAY_LOGGER_PREFIX + "." + str;
    }
    
    public static String getPlanRetryLoggerName(String str){
    	return PLAN_RETRY_LOGGER_PREFIX + "." + str;
    }
    
    public static String getTPTValidationLoggerName(String str){
    	return TPT_VALIDATION_LOGGER_PREFIX + "."+str;
    }
    
    public static String getScheduleInfoLoggerName(String str){
    	return SCHEDULE_INFO_LOGGER_PREFIX + "."+str;
    }
}
