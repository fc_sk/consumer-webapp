package com.freecharge.common.framework.session.service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.common.util.FCConstants;
import com.freecharge.platform.metrics.MetricsClient;
import com.freecharge.util.CookieUtil;

@Component("cookieHandler")
public class CookieHandler {
    private static final String NEW_COOKIE_PREFIX = "_";
    
    public static final String PERM_COOKIE_NAME = "fc.perm";

    private static Logger logger = LoggingFactory.getLogger(CookieHandler.class);

    private FCProperties fcproperties;

    @Autowired
    private MetricsClient metricsClient;

    @Autowired
    public void setFcproperties(FCProperties fcproperties) {
        this.fcproperties = fcproperties;
    }

    private Cookie getCookie(HttpServletRequest req, String cookieName) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookieName.equals(cookie.getName())) {
                    return cookie;
                }
            }
        }

        return null;
    }

    public Cookie getApplicationCookie(HttpServletRequest request) {
        return getCookie(request, fcproperties.getApplicationCookieName());
    }

    private Cookie createCookie(String name, String value,  String domain, String path, int expiry) {
        Cookie ck = new Cookie(name, value);
        ck.setMaxAge(expiry);
        ck.setPath(path);

        if (StringUtils.isNotBlank(domain)) {
            ck.setDomain(domain);
        }

        return ck;
    }

    private Cookie createApplicationCookie(String value, String domain, int expiry) {
        Cookie cookie = createCookie(fcproperties.getApplicationCookieName(), value, domain, fcproperties.getProperty(FCConstants.APPLICATION_COOKIE_PATH), expiry);

        if (cookieHttpsOnly()) {
            cookie.setSecure(true);
        }

        cookie.setHttpOnly(true);

        return cookie;
    }

    /**
     * Returns a globally unique permanent cookie value if request does not have one.
     * @param request
     * @return
     */
    public static String checkAndSetPermanentCookie(HttpServletRequest request, HttpServletResponse response) {
        String permanentCookie = CookieUtil.getCookieValue(request, PERM_COOKIE_NAME);
        if (permanentCookie == null){
            permanentCookie = getUUID();
        }
        CookieUtil.setCookie(response, PERM_COOKIE_NAME, permanentCookie, Integer.MAX_VALUE, true);
        return permanentCookie;
    }

    /**
     * Checks as to whether the application cookie is already present in the request. If yes, extracts it out and returns the application cookie
     * value corresponding to it. If not, creates a new application cookie and sets it in the response.
     * @param req
     * @param res
     * @param resetCookies - Forcibly create a new application cookie and set it in the response irrespective of whether the cookie is already
     *                     present in the request or not.
     * @return - The application cookie value.
     */
    public String checkAndSetCookies(HttpServletRequest req, HttpServletResponse res, boolean resetCookies) {
        Cookie cookie = getApplicationCookie(req);

        String sessionId = "";

        //Moving cookies to .freecharge.in domain only for cookies which are being set new
        String domain = fcproperties.getApplicationCookieDomainName();

        if (cookie == null || resetCookies) {
            sessionId = getUUID();
            logger.info("Generated SessionId: " + sessionId);
            logger.debug("Domain from cookie is - " + domain + " for request path - " + req.getServletPath());
            cookie = createApplicationCookie(sessionId, domain, (int)TimeUnit.SECONDS.convert(Long.valueOf(fcproperties.getSessionExpireTime()), TimeUnit.DAYS));
            res.addCookie(cookie);
        } else {
        	sessionId = cookie.getValue();
            logger.debug("Read SessionId: " + sessionId);
        }

        return cookie.getValue();
    }

    public String setApplicationCookie(final HttpServletResponse res, final String imsToken,final Long maxAge) {
        //Moving cookies to .freecharge.in domain only for cookies which are being set new
        String domain = fcproperties.getApplicationCookieDomainName();
        Cookie cookie = createApplicationCookie(imsToken, domain, maxAge.intValue());
        res.addCookie(cookie);
        return cookie.getValue();
    }
    
    public String setApplicationCookie(final HttpServletResponse res, final String imsToken) {
        //Moving cookies to .freecharge.in domain only for cookies which are being set new
    	 String domain = fcproperties.getApplicationCookieDomainName();
        int cookieExpiry = (int) TimeUnit.SECONDS.convert(Long.valueOf(fcproperties.getSessionExpireTime()), TimeUnit.DAYS);
        Cookie cookie = createApplicationCookie(imsToken, domain, cookieExpiry);
        res.addCookie(cookie);
        return cookie.getValue();
    }
    public void expireApplicationCookie(HttpServletRequest request, HttpServletResponse response) {
        String domain = fcproperties.getApplicationCookieDomainName();

        Cookie expiredCookieWithDomain = createApplicationCookie("", domain, 0);
        Cookie expiredCookieWithoutDomain = createApplicationCookie("", null, 0);

        response.addCookie(expiredCookieWithDomain);
        response.addCookie(expiredCookieWithoutDomain);
    }

    public String getUserIdUsingRememberMeCookie(HttpServletRequest request, HttpServletResponse response){
        String userId = null;
        String fcUsereId = fcproperties.getProperty(FCConstants.APPLICATION_REMEMBERME_COOKIE_USERID);
        Cookie cookies [] = request.getCookies ();
        if (cookies != null && fcUsereId!=null)
        {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies [i].getName().equals(fcUsereId))
                {
                    userId = cookies[i].getValue();
                    break;
                }
            }
        }
        return userId;
    }

    public static String getUUID() {
        return UUID.randomUUID().toString();
    }

    public String getLocaleParameterCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        String language = null;
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("language")) {
                    language = cookies[i].getValue();
                    logger.info("Cookie found for language : " + language);
                    break;
                }
            }
        }
        if (language == null) {
            language = "en_US";
        }
        return language;
    }

    /**
     * If production,then always returns true. If dev then always returns false. If QA, then the value as configured in the
     * properties file is returned.
     * @return - Should the cookie be set https only or not.
     */
    private boolean cookieHttpsOnly() {
        if (!fcproperties.isProdMode()) {
            if (fcproperties.isDevMode()) {
                return false;
            }
            return fcproperties.isCookieHttpsOnly();
        }
        return true;
    }
}