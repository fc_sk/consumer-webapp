package com.freecharge.common.framework.session.service;

import java.text.ParseException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.support.model.InternalServerErrorException;
import com.freecharge.app.domain.entity.UserDetails;
import com.freecharge.common.encryption.mdfive.EPinEncrypt;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.session.FreechargeContext;
import com.freecharge.common.framework.session.FreechargeContextDirectory;
import com.freecharge.common.framework.session.FreechargeSession;
import com.freecharge.common.framework.util.CSRFTokenManager;
import com.freecharge.common.framework.util.SessionConstants;
import com.freecharge.common.util.FCInfraUtils;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.web.util.WebConstants;
import com.snapdeal.ims.client.IUserServiceClient;
import com.snapdeal.ims.dto.UserDetailsDTO;
import com.snapdeal.ims.exception.ServiceException;
import com.snapdeal.ims.request.GetUserByTokenRequest;
import com.snapdeal.ims.response.GetUserResponse;

@Component("sessionManager")
public class SessionManager {

    private static final Logger logger = LoggingFactory.getLogger(SessionManager.class);
    private static final String SD_TOKEN_IS_INVALID = "ER-2104";
    public static final String SD_INVALID_GLOBAL_TOKEN = "ER-2103";

    private CookieHandler cookieHandler;
    private ISessionService sessionService;

    @Autowired
    private EPinEncrypt ePinEncrypt;
    
    @Autowired
    private UserServiceProxy userServiceProxy;

    @Autowired
    private IUserServiceClient userServiceClient;

    @Autowired
    public void setCookieHandler(CookieHandler cookieHandler) {
        this.cookieHandler = cookieHandler;
    }

    @Autowired
    public void setISessionService(ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void storeSession(HttpServletRequest request, HttpServletResponse response) {
    	FreechargeContext fcContext = FreechargeContextDirectory.get();
    	if (fcContext != null){
    		FreechargeSession freechargeSession = fcContext.getFreechargeSession();
            sessionService.storeSession(freechargeSession);
    	}
    }

    public void retrieveSession(HttpServletRequest request, HttpServletResponse response) {
        //Do not change the locality of this statement. Once the checkAndSetCookies method is called, this will return true anyways.
        MDC.put("sessionId", "sessionId: NA");
        boolean isApplicationCookiePresent = !(cookieHandler.getApplicationCookie(request) == null);
        String sessionId = cookieHandler.checkAndSetCookies(request, response, false);
        FreechargeSession freechargeSession = sessionService.retrieveSession(sessionId);
        GetUserByTokenRequest getUserRequest = new GetUserByTokenRequest();
        getUserRequest.setToken(sessionId);
        try {
        	GetUserResponse userResponse = null;
        	try {
        		userResponse = userServiceClient.getUserByToken(getUserRequest);
        		if(userResponse.getTokenInformationDTO()!=null && userResponse.getTokenInformationDTO().getExpiryTimeInSeconds()!=null) {
        			cookieHandler.setApplicationCookie(response, sessionId, userResponse.getTokenInformationDTO().getExpiryTimeInSeconds());
        		}
        	} catch (ServiceException se) {
        		String uri = request.getRequestURI();
            	if("/rest/session/create".equalsIgnoreCase(uri) && isApplicationCookiePresent) {
            		cookieHandler.setApplicationCookie(response, sessionId);
            	}
        		if (se.getErrCode().equals(SD_TOKEN_IS_INVALID) || se.getErrCode().equals(SD_INVALID_GLOBAL_TOKEN)) {
        			logger.error(sessionId + " token is invalid");
        		} else {
        			logger.error("ServiceException while fetching user by token for token " + sessionId,se);
        			throw new InternalServerErrorException("ServiceException while fetching user by token for token " + sessionId);
        		}
        	} catch (Exception e) {
        		logger.error("Error while fetching user by token for token " + sessionId,e);
        		throw new InternalServerErrorException("Error while fetching user by token " + sessionId);
        	}

			if (null != userResponse && null != userResponse.getUserDetails()) {
				UserDetailsDTO dto = userResponse.getUserDetails();
				if (null == freechargeSession) {
					freechargeSession = new FreechargeSession(sessionId, FCInfraUtils.getRemoteAddr(request), userResponse.getCsrfToken());
				}
				Map<String, Object> sessionData = freechargeSession.getSessionData();
				// set user related session data
				sessionData.put(SessionConstants.SESSION_USER_EMAIL_ID, dto.getEmailId());
				sessionData.put(SessionConstants.SESSION_USER_FIRSTNAME, dto.getFirstName());
				sessionData.put(SessionConstants.SESSION_USER_IS_LOGIN, true);
				sessionData.put(SessionConstants.SESSION_USER_USERID, dto.getFcUserId());
				sessionData.put(WebConstants.USER_ID, dto.getUserId());
				sessionData.put(SessionConstants.NEED_LOGIN, false);
				freechargeSession.setSessionData(sessionData);
			} else {
				if (null == freechargeSession ||
					(null != freechargeSession.getSessionData().get(SessionConstants.SESSION_USER_IS_LOGIN) &&
					 ((Boolean) freechargeSession.getSessionData().get(SessionConstants.SESSION_USER_IS_LOGIN)))) {
					sessionService.deleteSession(sessionId);
					//If the application cookie is already present in the request, we should have a session corresponding to it. This means the user is tampering
					//with the cookie or the session store got reset in between the requests.
					String oldSessionId = sessionId;
					if (isApplicationCookiePresent) {
						sessionId = cookieHandler.checkAndSetCookies(request, response, true); //forcibly create a new session cookie
						//logger.info("Session id forcibly created:" + sessionId);
						logger.info(String.format("%s session id not found. Forcibly creating session %s", oldSessionId, sessionId));
					}
					freechargeSession = new FreechargeSession(sessionId, FCInfraUtils.getRemoteAddr(request), CSRFTokenManager.generateToken());
				}
			}
			MDC.put("sessionId", "sessionId:" + sessionId);
			FreechargeContext freechargeContext = createContext(sessionId, freechargeSession);
			FreechargeContextDirectory.set(freechargeContext);
		} catch (Exception e) {
			logger.error("Error while getting user session", e);
		}
    }

	public void deleteSession() {
        String sessionId = FreechargeContextDirectory.get().getSessionId();
        logger.info("Reequest comes into session manager to delete session for sessionId: " + sessionId);
        sessionService.deleteSession(sessionId);
        FreechargeContextDirectory.invalidateData();
    }

    public static FreechargeContext createContext(String sessionid, FreechargeSession freechargeSession) {
        FreechargeContext freechargeContext = new FreechargeContext();
        freechargeContext.setSessionId(sessionid);
        freechargeContext.setFreechargeSession(freechargeSession);
        return freechargeContext;
    }

    public void checkAndSetRememberMeDetails(HttpServletRequest request, HttpServletResponse response) {
        String userId = cookieHandler.getUserIdUsingRememberMeCookie(request, response);
        if (userId != null) {
            try {
                userId = ePinEncrypt.getDecryptedPin(userId);
            } catch (ParseException e) {
                logger.error("Parse Exception occured while decrypting the user id in checkAndSetRememberMeDetails method SessionManager Class(Remember me)");
                return;
            } catch (Exception e) {
                logger.error("Exception occured while decrypting the user id in checkAndSetRememberMeDetails method SessionManager Class(Remember me)");
                return;
            }
            setUserDetailsIntheSession(userId);
        }
    }

    public void setUserDetailsIntheSession(String UserId) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        UserDetails user = userServiceProxy.getUserProfileByUserId(Integer.parseInt(UserId));
        if (user != null && user.getIsActive()) {
        	fs.getSessionData().put(WebConstants.SESSION_USER_USERID, user.getUserId());
            fs.getSessionData().put(WebConstants.SESSION_USER_EMAIL_ID, user.getEmail());
            fs.getSessionData().put(WebConstants.SESSION_USER_IS_LOGIN, true);
            fs.getSessionData().put(WebConstants.SESSION_USER_FIRSTNAME, user.getName());
        }
    }
    
    public void setUserDetailsIntheSessionWithoutDefaultProfile(String UserId) {
        FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
        UserDetails user = userServiceProxy.getUserProfileByUserId(Integer.parseInt(UserId));
        if (user != null && user.getIsActive()) {
        	fs.getSessionData().put(WebConstants.SESSION_USER_USERID, user.getUserId());
        	fs.getSessionData().put(WebConstants.SESSION_USER_EMAIL_ID, user.getEmail());
            fs.getSessionData().put(WebConstants.SESSION_USER_IS_LOGIN, true);
            fs.getSessionData().put(WebConstants.SESSION_USER_FIRSTNAME, user.getName());
        }
    }
    
    public FreechargeSession generateMockSession(Integer userId){
        FreechargeSession freechargeSession = new FreechargeSession(UUID.randomUUID().toString(), "0.0.0.0", CSRFTokenManager.generateToken());
        MDC.put("sessionId", "sessionId:" + freechargeSession.getUuid());
        FreechargeContext freechargeConext = createContext(freechargeSession.getUuid(), freechargeSession);
        FreechargeContextDirectory.set(freechargeConext);
        setUserDetailsIntheSession(userId.toString());
        freechargeSession.getSessionData().put("loggedinuserid", userId);
        return freechargeSession;
    }
    
    public FreechargeSession generateMockSession(Integer userId,HttpServletRequest request,HttpServletResponse response){
    	String sessionId = cookieHandler.checkAndSetCookies(request, response, true);
    	MDC.put("sessionId", "sessionId:" + sessionId);
    	FreechargeSession freechargeSession = new FreechargeSession(sessionId, FCInfraUtils.getRemoteAddr(request), CSRFTokenManager.generateToken());
        FreechargeContext freechargeConext = createContext(freechargeSession.getUuid(), freechargeSession);
        FreechargeContextDirectory.set(freechargeConext);
        setUserDetailsIntheSession(userId.toString());
        freechargeSession.getSessionData().put("loggedinuserid", userId);
    	return freechargeSession;
    }
    
    public FreechargeSession generateMockSessionForAutopay(Integer userId,HttpServletRequest request,HttpServletResponse response){
    	String sessionId = cookieHandler.checkAndSetCookies(request, response, true);
    	MDC.put("sessionId", "sessionId:" + sessionId);
    	FreechargeSession freechargeSession = new FreechargeSession(sessionId, FCInfraUtils.getRemoteAddr(request), CSRFTokenManager.generateToken());
        FreechargeContext freechargeConext = createContext(freechargeSession.getUuid(), freechargeSession);
        FreechargeContextDirectory.set(freechargeConext);
        setUserDetailsIntheSessionWithoutDefaultProfile(userId.toString());
        freechargeSession.getSessionData().put("loggedinuserid", userId);
    	return freechargeSession;
    }
}
