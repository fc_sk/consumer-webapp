package com.freecharge.common.cache;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.freecharge.platform.metrics.MetricsClient;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 4/4/13
 * Time: 10:54 AM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class RedisCacheManager implements Cache {
    @Autowired
    private RedisTemplate<String, Object> redisCacheTemplate;

    @Autowired
    MetricsClient metricsClient;

    private final String SERVICE_NAME = "RedisCacheManager";
    
    @Override
    public Object get(final String key) {
        try {
            return getCacheTemplate().opsForValue().get(key);
        } catch (RedisConnectionFailureException e) {
            metricsClient.recordEvent(SERVICE_NAME, "Get", "RedisConnectionFailureException");
            throw e;
        } catch (Exception e) {
            metricsClient.recordEvent(SERVICE_NAME, "Get", "Exception");
            throw e;
        }
    }

    @Override
    public void set(final String key, final Object value) {
        try {
            getCacheTemplate().opsForValue().set(key, value);
        } catch (RedisConnectionFailureException e) {
            metricsClient.recordEvent(SERVICE_NAME, "Set", "RedisConnectionFailureException");
            throw e;
        } catch (Exception e) {
            metricsClient.recordEvent(SERVICE_NAME, "Set", "Exception");
            throw e;
        }
    }

    @Override
    public void delete(final String key) {
        getCacheTemplate().delete(key);
    }

    @Override
    public void set(final String key, final Object value, final long timeout, final TimeUnit unit) {
        getCacheTemplate().opsForValue().set(key, value, timeout, unit);
    }

    @Override
    public  Set<?> getForScoreRange(final String key, final long minScore, final long maxScore) {
        return getCacheTemplate().opsForZSet().rangeByScore(key, minScore, maxScore);
    }

    @Override
    public Set<?> getForIndexRange(String key, long minIndex, long maxIndex) {
    	
        return getCacheTemplate().opsForZSet().range(key, minIndex, maxIndex);
    }

    @Override
    public void setWithScore(final String key, final Object value, final long score) {
        getCacheTemplate().opsForZSet().add(key, value, score);
    }

    @Override
    public void deleteInScoreRange(final String key, final long minScore, final long maxScore) {
        getCacheTemplate().opsForZSet().removeRangeByScore(key, minScore, maxScore);
    }
    
    @Override
    public Boolean setIfAbsent(String key, Object value) {
        return getCacheTemplate().opsForValue().setIfAbsent(key, value);
    }

    public void addToSet(String key, Object value){
        getCacheTemplate().opsForSet().add(key, value);

    }
    public void removeFromSet(String key, Object value){
        getCacheTemplate().opsForSet().remove(key, value);
    }

    public Set<?> fetchSetMembers(String key){
        return getCacheTemplate().opsForSet().members(key);
    }

    @Override
    public long getSetCardinality(String key) {
        return getCacheTemplate().opsForSet().size(key);
    }

    @Override
    public Object pop(String key) {
        return getCacheTemplate().opsForSet().pop(key);
    }

    public Object fetchSetMember(String key){
        return getCacheTemplate().opsForSet().randomMember(key);
    }

    @Override
    public Boolean expire(String key, long millis) {
        return getCacheTemplate().expire(key, millis, TimeUnit.MILLISECONDS);
    }

    protected RedisTemplate<String, Object> getCacheTemplate() {
        return this.redisCacheTemplate;
    }
    
    protected Long increment(String key) {
        return getCacheTemplate().opsForValue().increment(key, 1l);
    }
    
    protected Long increment(String key,Long value) {
        return getCacheTemplate().opsForValue().increment(key, value);
    }

    @Override
    public Boolean expireAt(String key, Date date) {
        return getCacheTemplate().expireAt(key, date);
    }
    
    protected Collection<String> getKeys(String pattern) {
        return getCacheTemplate().keys(pattern);
    }
    
    protected void deleteKeys(Collection<String> keysToDelete) {
        getCacheTemplate().delete(keysToDelete);
    }

    @Override
    public void delete(List<String> keys) {
        getCacheTemplate().delete(keys);
    }
}
