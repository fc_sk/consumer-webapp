package com.freecharge.common.cache;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.common.framework.properties.FCProperties;

@Component
public class CacheManager implements Cache {
    @Autowired
    private FCProperties fcProperties;

    @Autowired
    private RedisCacheManager redisCacheManager;

    @Autowired
    private MockCacheManager mockCacheManager;

    public CacheManager() {
        super();
    }

    public Cache getCurrentCachingStrategy() {
        if (!fcProperties.isProdMode() && fcProperties.isMockCacheEnabled()) {
            return mockCacheManager;
        }

        return redisCacheManager;
    }

    @Override
    public Object get(final String key) {
        return getCurrentCachingStrategy().get(key);
    }

    @Override
    public void set(final String key, final Object value) {
        getCurrentCachingStrategy().set(key, value);
    }

    @Override
    public void delete(final String key) {
        getCurrentCachingStrategy().delete(key);
    }

    @Override
    public void set(final String key, final Object value, final long timeout, final TimeUnit unit) {
        getCurrentCachingStrategy().set(key, value, timeout, unit);
    }

    @Override
    public Set<?> getForScoreRange(final String key, final long minScore, final long maxScore) {
        return getCurrentCachingStrategy().getForScoreRange(key, minScore, maxScore);
    }

    @Override
    public Set<?> getForIndexRange(String key, long minIndex, long maxIndex) {
    	
        return getCurrentCachingStrategy().getForIndexRange(key, minIndex, maxIndex);
    }

    @Override
    public void setWithScore(final String key, final Object value, final long score) {
        getCurrentCachingStrategy().setWithScore(key, value, score);
    }

    @Override
    public void deleteInScoreRange(final String key, final long minScore, final long maxScore) {
        getCurrentCachingStrategy().deleteInScoreRange(key, minScore, maxScore);
    }

    @Override
    public void addToSet(String key, Object value){
        getCurrentCachingStrategy().addToSet(key, value);
    }

    @Override
    public Set<?> fetchSetMembers(String key) {
        return getCurrentCachingStrategy().fetchSetMembers(key);
    }

    @Override
    public Object pop(String key) {
        return getCurrentCachingStrategy().pop(key);
    }

    @Override
    public long getSetCardinality(String key) {
        return  getCurrentCachingStrategy().getSetCardinality(key);
    }

    @Override
    public Object fetchSetMember(String key) {
        return getCurrentCachingStrategy().fetchSetMember(key);
    }

    @Override
    public void removeFromSet(String key, Object value){
        getCurrentCachingStrategy().removeFromSet(key, value);
    }

    @Override
    public Boolean setIfAbsent(String key, Object value) {
        return getCurrentCachingStrategy().setIfAbsent(key, value);
    }

    @Override
    public Boolean expire(String key, long millis) {
        return getCurrentCachingStrategy().expire(key, millis);
    }

    @Override
    public Boolean expireAt(String key, Date date) {
        return getCurrentCachingStrategy().expireAt(key, date);
    }

    @Override
    public void delete(List<String> keys) {
        getCurrentCachingStrategy().delete(keys);
    }
}
