package com.freecharge.common.cache;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: abhi
 * Date: 4/4/13
 * Time: 10:49 AM
 * To change this template use File | Settings | File Templates.
 */


/*
                      +--------------+
                      |   Cache      |
                      +--------------+
                             |
                             |
                             |
    +--------------------------------------------------------------+
    |                      CacheManager                            |
    | +----------------------+         +------------------------+  |
    | |    MockCacheManager  |         |     RedisCacheManager  |  |
    | +----------------------+         +------------------------+  |
    +--------------------------------------------------------------+

 */

public interface Cache {
    public Object get(String key);
    public void set(String key, Object value);
    public void delete(String key);
    public void set(String key, Object value,  long timeout, TimeUnit unit);
    /**
     * Return a range of members in a sorted set, by score
     * @param key
     * @param minScore - Min Score (Inclusive)
     * @param maxScore - Max Score (Inclusive)
     * @return
     */
    public Set<?> getForScoreRange(String key, long minScore, long maxScore);
    /**
     * Return a range of members in a sorted set, by Index
     * @param key
     * @param minIndex - Min Index (Inclusive)
     * @param maxIndex - Max Index (Inclusive)
     * @return
     */
    public Set<?> getForIndexRange(String key, long minIndex, long maxIndex);
    /**
     * Add one member to a sorted set, or update its score if it already exists
     * @param key
     * @param value - Value to be Set
     * @param score
     */
    public void setWithScore(String key, Object value, long score);
    /**
     * Remove all members in a sorted set within the given scores
     * @param key
     * @param minScore - Min (Inclusive)
     * @param maxScore - Max (Inclusive)
     */
    public void deleteInScoreRange(String key, long minScore, long maxScore);

    public void addToSet(String key, Object value);
    public void removeFromSet(String key, Object value);

    /**
     * Removes a random member from the set specified via key and returns it
     * @param key
     * @return
     */
    public Object pop(String key);
    public long getSetCardinality(String key);
    public Set<?> fetchSetMembers(String key);
    /**
     * Returns a random member from the set specified via key
     * @param key
     * @return
     */
    public Object fetchSetMember(String key);

    public Boolean setIfAbsent(String key, Object value);
    public Boolean expire(String key, long millis);
    /**
     * Sets the expiry of a key.
     * @param key
     * @param date - Expiry date
     * @return
     */
    public Boolean expireAt(String key, Date date);
    public void delete(List<String> keys);
}
