package com.freecharge.common.cache;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

/**
 * This class exists to run the code without a cache.
 */
@Component
public class MockCacheManager implements Cache {
    private static class CacheObject {
        private Object object;
        private Timestamp expiryTime;

        public CacheObject(final Object object, final Timestamp expiryTime) {
            this.object = object;
            this.expiryTime = expiryTime;
        }

        public CacheObject(final Object object) {
            this.object = object;
            this.expiryTime = null;
        }

        public Timestamp getExpiryTime() {
            return expiryTime;
        }

        public void setExpiryTime(Timestamp et) {
            this.expiryTime = et;
        }

        public Object getObject() {
            return object;
        }
    }

    private Map<String, CacheObject> mock = new ConcurrentHashMap<String, CacheObject>();

    @Override
    public Object get(final String key) {
        CacheObject cacheObject = mock.get(key);
        if (cacheObject != null) {
            if (cacheObject.getExpiryTime() == null) {
                return cacheObject.getObject();
            } else {
                if (cacheObject.getExpiryTime().before(new Timestamp(System.currentTimeMillis()))) {
                    mock.remove(key);
                    return null;
                } else {
                    return cacheObject.getObject();
                }
            }
        }
        return null;
    }

    @Override
    public void set(final String key, final Object value) {
        mock.put(key, new CacheObject(value));
    }

    @Override
    public void delete(final String key) {
        mock.remove(key);
    }

    @Override
    public void set(final String key, final Object value, final long timeout, final TimeUnit unit) {
        Timestamp expiryTime = new Timestamp(System.currentTimeMillis() + unit.toMillis(timeout));
        mock.put(key, new CacheObject(value, expiryTime));
    }

    @Override
    public Boolean setIfAbsent(String key, Object value) {
        Object o = get(key);
        if (o == null) {
            mock.put(key, new CacheObject(value));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Boolean expire(String key, long millis) {
        CacheObject cacheObject = mock.get(key);
        Timestamp expiryTime = new Timestamp(System.currentTimeMillis() + millis);
        cacheObject.setExpiryTime(expiryTime);
        return true;
    }

    @Override
    public Set<?> getForScoreRange(String key, long minScore, long maxScore) {
        // Do nothing here as it is a mock.
        return new HashSet<Object>();
    }

    @Override
    public Set<?> getForIndexRange(String key, long minIndex, long maxIndex) {
        // Do nothing here as it is a mock.
        return new HashSet<Object>();
    }

    @Override
    public void setWithScore(String key, Object value, long score) {
        // Do nothing here as it is a mock.
    }

    @Override
    public void deleteInScoreRange(String key, long minScore, long maxScore) {
     // Do nothing here as it is a mock.
    }

    @Override
    public void addToSet(String key, Object value) {
        // Do nothing here as it is a mock.
    }

    @Override
    public void removeFromSet(String key, Object value) {
            // Do nothing here as it is a mock.
    }

    @Override
    public Set<?> fetchSetMembers(String key) {
        return null;
    }

    @Override
    public Object pop(String key) {
        return null;
    }

    @Override
    public Object fetchSetMember(String key) {
        return null;
    }

    @Override
    public long getSetCardinality(String key) {
        return 0;
    }

    @Override
    public Boolean expireAt(String key, Date date) {
        CacheObject cacheObject = mock.get(key);
        cacheObject.setExpiryTime(new Timestamp(date.getTime()));
        return true;
    }

    @Override
    public void delete(final List<String> keys) {
        for (String k : keys){
            this.delete(k);
        }
    }
}
