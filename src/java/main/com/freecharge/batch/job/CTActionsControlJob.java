package com.freecharge.batch.job;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.admin.service.CTActionControlJobService;
import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;

public class CTActionsControlJob extends FreeChargeJob {
    private static final Logger LOGGER = LoggingFactory.getLogger(CTActionsControlJob.class);
    public static void main(final String[] args) throws IOException {

        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            Thread.currentThread().setName("CTActionsControlJob");
            CTActionControlJobService bean = (CTActionControlJobService) ctx
                    .getBean("ctActionControlJobService");
            
            //Write job functions
            LOGGER.info("Started executing CTActionsControlJob job " + dateFormat.format(new Date()));
            bean.updateCTActionControlJobMethod();
            LOGGER.info("Finished executing CTActionsControlJob job " + dateFormat.format(new Date()));
        } catch (Exception e) {
            LOGGER.error("Exception occured when executing CTActionsControlJob." + dateFormat.format(new Date()), e);
        } finally {
            BatchJobUtil.destroyBatchJobContext(ctx);
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
    }
}
