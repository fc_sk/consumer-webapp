package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.services.RechargeSchedulerService;

public class RechargeSchedulerJob extends FreeChargeJob {
    private static final Logger logger = LoggingFactory
			.getLogger(RechargeSchedulerJob.class);
	private RechargeSchedulerService handler;

	private void execute(Integer m1, Integer m2) throws Exception {
		logger.info("----------------------------STARTING RechargeSchedulerJob------------------------");
		getHandler().getRetryRechargeStatus(m1, m2);
		logger.info("----------------------------FINISHED RechargeSchedulerJob------------------------");
	}

	public static void main(String[] args) {
        Thread.currentThread().setName("RechargeSchedulerJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        RechargeSchedulerJob job = (RechargeSchedulerJob) ctx.getBean("rechargeSchedulerJob");

        try {
			// We will check status of recharges between (T-m1) -> (T-m2) -
			// where T = current time
			//Integer m1 = 15;
			//Integer m2 = 70;
			Integer m1 = 0;
            Integer m2 = 70;
			if (args.length == 2) {
				m1 = Integer.parseInt(args[0]);
				m2 = Integer.parseInt(args[1]);
			}

			job.execute(m1, m2);

		} catch (Exception e) {
			logger.error("ERROR occured in executing RechargeSchedulerJob.");
			logger.error(e.getCause(), e);
		} finally {
		    try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
		}
	}

	public RechargeSchedulerService getHandler() {
		return handler;
	}

	public void setHandler(RechargeSchedulerService handler) {
		this.handler = handler;
	}

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String...arguments) {
        Integer m1 = 15;
        Integer m2 = 70;
        if (arguments.length == 2) {
            m1 = Integer.parseInt(arguments[0]);
            m2 = Integer.parseInt(arguments[1]);
        }
        
        RechargeSchedulerService rechargeSchedulerService = jobContext.getBean(RechargeSchedulerService.class);
        rechargeSchedulerService.getRetryRechargeStatus(m1, m2);        
    }

}
