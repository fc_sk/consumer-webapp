package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.productdata.services.TopPlansIreffDataImportService;

public class TopPlansIreffIngestJob extends FreeChargeJob {
    private static final Logger LOGGER = LoggingFactory.getLogger(TopPlansIreffIngestJob.class);

    public static void main(final String[] args) throws IOException {
        Boolean isPopular = null;
        if (args.length != 0) {
            if (args[0].equals("popular")) {
                isPopular = true;
            } else if (args[0].equals("non-popular")) {
                isPopular = false;
            }
        }
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        try {
            Thread.currentThread().setName("TopPlansIreffIngestionJob");
            TopPlansIreffDataImportService bean = (TopPlansIreffDataImportService) ctx
                    .getBean("topPlansIreffIngestionJob");
            bean.fetchUpdate(isPopular);
        } catch (RuntimeException e) {
            LOGGER.error("RuntimeException occured when executing TopPlansIreffIngestJob.", e);
        } catch (IOException e) {
            LOGGER.error("IOException occured when executing TopPlansIreffIngestJob.", e);
        } catch (ParseException e) {
            LOGGER.error("ParseException occured when executing TopPlansIreffIngestJob.", e);
        } finally {
            BatchJobUtil.destroyBatchJobContext(ctx);
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        Boolean isPopular = null;
        if (arguments.length != 0) {
            if (arguments[0].equals("popular")) {
                isPopular = true;
            } else if (arguments[0].equals("non-popular")) {
                isPopular = false;
            }
        }

        TopPlansIreffDataImportService bean = (TopPlansIreffDataImportService) jobContext.getBean("topPlansIreffIngestionJob");
        
        bean.fetchUpdate(isPopular);
        
    }
}
