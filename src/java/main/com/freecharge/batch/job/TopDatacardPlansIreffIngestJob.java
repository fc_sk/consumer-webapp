package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.productdata.services.TopDatacardPlansIreffDataImportService;

public class TopDatacardPlansIreffIngestJob extends FreeChargeJob {
    private static final Logger LOGGER = LoggingFactory.getLogger(TopDatacardPlansIreffIngestJob.class);

    public static void main(final String[] args) throws IOException {

        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        try {
            Thread.currentThread().setName("TopDatacardPlansIreffIngestJob");
            TopDatacardPlansIreffDataImportService bean = (TopDatacardPlansIreffDataImportService) ctx
                    .getBean("topDatacardPlansIreffDataImportService");
            bean.fetchUpdate();
        } catch (RuntimeException e) {
            LOGGER.error("RuntimeException occured when executing TopPlansIreffIngestJob.", e);
        } catch (IOException e) {
            LOGGER.error("IOException occured when executing TopPlansIreffIngestJob.", e);
        } catch (ParseException e) {
            LOGGER.error("ParseException occured when executing TopPlansIreffIngestJob.", e);
        } finally {
            BatchJobUtil.destroyBatchJobContext(ctx);
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {

        TopDatacardPlansIreffDataImportService bean = (TopDatacardPlansIreffDataImportService) jobContext
                .getBean("topDatacardPlansIreffDataImportService");

        bean.fetchUpdate();

    }
}
