package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.api.coupon.service.web.model.BlockCouponRequest;
import com.freecharge.api.coupon.service.web.model.CouponOptinType;
import com.freecharge.app.handlingcharge.PricingService;
import com.freecharge.app.service.OrderService;
import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.OrderIdUtil;

public class BlockCouponJob extends FreeChargeJob {
	private static final Logger logger = LoggingFactory
			.getLogger(BlockCouponJob.class);
 
 	@Autowired
	@Qualifier("couponServiceProxy")
    private ICouponService handler;
 	
 	@Autowired
 	private PricingService pricingService;
 	
 	@Autowired
 	private OrderService orderService;
 	
	private void execute(int couponStoreId, int couponsCnt, boolean isMCoupon, boolean toggleMcouponStatus) throws Exception {
		logger.info("----------------------------STARTING BlockCouponJob------------------------");
		String orderId = OrderIdUtil.generateWebRedemptionOrderIdForBlockingCodes(couponStoreId);
		Integer userId = orderService.getUserIdForOrder(orderId);
		BlockCouponRequest blockCouponRequest = new BlockCouponRequest();
		blockCouponRequest.setOrderId(orderId);
		blockCouponRequest.setCouponStoreId(couponStoreId);
		blockCouponRequest.setQuantity(couponsCnt);
		CouponOptinType optinType = CouponOptinType.OPTIN;
		if (pricingService.hasPaidCoupons(orderId)){
			optinType = CouponOptinType.PRICED;
		}
		blockCouponRequest.setCouponOptinType(optinType);
 		handler.blockCouponForOrder(blockCouponRequest,userId);
		logger.info("----------------------------FINISHED BlockCouponJob------------------------");
	}

	public static void main(String[] args) {
        Thread.currentThread().setName("BlockCouponJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        BlockCouponJob job = (BlockCouponJob) ctx.getBean("blockCouponJob");
         try {

			if (args.length == 5) {
				int couponStoreId = Integer.parseInt(args[0]);
				int couponsCnt = Integer.parseInt(args[1]);
				boolean isMCoupon = Boolean.parseBoolean(args[2]);
				boolean toggleMcouponStatus = Boolean.parseBoolean(args[3]);
				int chunkSize = Integer.parseInt(args[4]);
				for(int count = 0 ; count < couponsCnt ; count += chunkSize){
					int blockCouponCnt = (couponsCnt - count) >= chunkSize ? chunkSize : (couponsCnt - count);
					job.execute(couponStoreId, blockCouponCnt, isMCoupon, toggleMcouponStatus);
					Thread.sleep(2000);
				}
			}

		} catch (Exception e) {
			logger.error("ERROR occured in executing BlockCouponJob.");
			logger.error(e.getCause(), e);
		} 
        finally {
		    try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } 
		    catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
		}
	}

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        BlockCouponJob job = (BlockCouponJob) jobContext.getBean("blockCouponJob");

       if (arguments.length == 5) {
           int couponStoreId = Integer.parseInt(arguments[0]);
           int couponsCnt = Integer.parseInt(arguments[1]);
           boolean isMCoupon = Boolean.parseBoolean(arguments[2]);
           boolean toggleMcouponStatus = Boolean.parseBoolean(arguments[3]);
           int chunkSize = Integer.parseInt(arguments[4]);
           for(int count = 0 ; count < couponsCnt ; count += chunkSize){
               int blockCouponCnt = (couponsCnt - count) >= chunkSize ? chunkSize : (couponsCnt - count);
               job.execute(couponStoreId, blockCouponCnt, isMCoupon, toggleMcouponStatus);
               Thread.sleep(2000);
           }
       }
    }
}
