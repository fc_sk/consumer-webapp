package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.productdata.services.DenomSuccessUpdaterService;

/**
 * A daily job to find the success rate of all the denominations across all the
 * operators and the circle.
 */

public class DenomSuccessUpdaterJob extends FreeChargeJob {

    private static final Logger logger = LoggingFactory.getLogger(DenomSuccessUpdaterJob.class);

    public static void main(final String[] args) throws IOException {
        try {
            ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("commonApplicationContext.xml");
            try {
                Thread.currentThread().setName("DenomSuccessUpdaterJob");
                DenomSuccessUpdaterService updaterService = (DenomSuccessUpdaterService) ctx
                        .getBean(DenomSuccessUpdaterService.class);
                updaterService.calculateDenomSuccessRates();
            } catch (RuntimeException e) {
                logger.error("RuntimeException occured when executing DenomSuccessUpdaterJob.", e);
            } finally {
                BatchJobUtil.destroyBatchJobContext(ctx);
            }
        } catch (Exception e) {
            logger.error(e);
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {

        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        DenomSuccessUpdaterService updaterService = (DenomSuccessUpdaterService) ctx
                .getBean(DenomSuccessUpdaterService.class);
        updaterService.calculateDenomSuccessRates();

    }
}
