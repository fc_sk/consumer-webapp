package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.app.service.FailedLoginService;
import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;

public class FailedLoginCleanUpJob extends FreeChargeJob {
    private static final Logger LOGGER = LoggingFactory.getLogger(FailedLoginCleanUpJob.class);

    public static void main(final String[] args) throws IOException {
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        try {
            Thread.currentThread().setName("FailedLoginCleanUpJob");
            FailedLoginService bean = (FailedLoginService) ctx.getBean("failedLoginCleanUpJob");
            bean.removeDataOlderThanAWeek();
        } catch (RuntimeException e) {
            LOGGER.error("RuntimeException occured when executing FailedLoginCleanUpJob.", e);
        } finally {
            BatchJobUtil.destroyBatchJobContext(ctx);
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        FailedLoginService bean = (FailedLoginService) jobContext.getBean("failedLoginCleanUpJob");
        bean.removeDataOlderThanAWeek();
    }
}
