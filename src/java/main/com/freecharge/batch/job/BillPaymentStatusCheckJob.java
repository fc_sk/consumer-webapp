package com.freecharge.batch.job;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freebill.common.BillPaymentStatusCheck;

public class BillPaymentStatusCheckJob extends FreeChargeJob {
    
    private static final Logger logger = LoggingFactory
            .getLogger(BillPaymentStatusCheckJob.class);

    public static void main(String[] args) {

        try {
            
            Integer m1 =1;
            Integer m2 = 100;
            if (args.length == 2) {
                m1 = Integer.parseInt(args[0]);
                m2 = Integer.parseInt(args[1]);
            }
            
            Date now = new Date();
            Date to = DateUtils.addMinutes(now, -m1); 
            Date from = DateUtils.addMinutes(to, -m2);
            
            logger.info(String.format("Doing bill payment status check for candidates from %s to %s", from,
                    to));
            doBillPaymentStatusCheck(from, to);
        } catch (Exception exception) {	
            logger.error("exception occured for bill payment status check for candidates from %s to %s", exception);
        }
    }

    private static void doBillPaymentStatusCheck(Date startDate, Date endDate) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml",
                "web-delegates.xml");
        BillPaymentStatusCheck billPaymentStatusCheck = (BillPaymentStatusCheck) ctx.getBean("billPaymentStatusCheck");
        try {
            billPaymentStatusCheck.doBillPaymentStatusCheck(startDate, endDate);
        } catch (Exception exception) {
            logger.error("Exception while doing Bill payment autorefund", exception);
        } finally {
            ctx.close();
            System.exit(0);
        }

    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        Integer m1 =30;
        Integer m2 = 100;
        if (arguments.length == 2) {
            m1 = Integer.parseInt(arguments[0]);
            m2 = Integer.parseInt(arguments[1]);
        }
        
        Date now = new Date();
        Date to = DateUtils.addMinutes(now, -m1); 
        Date from = DateUtils.addMinutes(to, -m2);
        
        logger.info(String.format("Doing bill payment status check for candidates from %s to %s", from, to));
        
        BillPaymentStatusCheck billPaymentStatusCheck = (BillPaymentStatusCheck) jobContext.getBean("billPaymentStatusCheck");
        
        billPaymentStatusCheck.doBillPaymentStatusCheck(from, to);
    }
}
