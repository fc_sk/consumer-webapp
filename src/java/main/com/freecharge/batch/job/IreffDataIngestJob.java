package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.productdata.services.IreffDataImportService;

public class IreffDataIngestJob extends FreeChargeJob {
    private static final Logger logger = LoggingFactory.getLogger(IreffDataIngestJob.class);

    public static void main(final String[] args) throws IOException {
        try {
            ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
            try {
                Thread.currentThread().setName("IreffDataIngestJob");
                IreffDataImportService ireffDataImportService = (IreffDataImportService) ctx
                        .getBean(IreffDataImportService.class);
                ireffDataImportService.updateIreffRepo();
            } catch (Exception e) {
                logger.error("ParseException occured when executing IreffDataIngestJob.", e);
            } finally {
                BatchJobUtil.destroyBatchJobContext(ctx);
            }
        } catch (Exception e) {
            logger.error("Exception occured when executing IreffDataIngestJob.", e);
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {

        IreffDataImportService ireffDataImportService = (IreffDataImportService) jobContext
                .getBean(IreffDataImportService.class);
        ireffDataImportService.updateIreffRepo();

    }
}
