package com.freecharge.batch.job;

import java.io.IOException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.joda.time.DateMidnight;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCProperties;
import com.freecharge.payment.autorefund.AutoRefund;
import com.freecharge.payment.autorefund.AutoRefundAction;

/**
 * Same as {@link AutoRefundJob} but that spans about a week, from T-2 to T-8.
 * @author arun
 *
 */
public class AutoRefundWeeklyCleanupJob extends FreeChargeJob {
    private static final Logger logger = LoggingFactory.getLogger(AutoRefundWeeklyCleanupJob.class);

    public static void main(String[] args) {
        Thread.currentThread().setName("AutoRefundWeeklyCleanupJob");

        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        AutoRefund autoRefund = ((AutoRefund) ctx.getBean("autoRefund"));
        FCProperties fcProperties = ctx.getBean(FCProperties.class);

        ForkJoinPool fjPool = new ForkJoinPool();
        
        DateMidnight endDate = (new DateMidnight()).minusDays(fcProperties
                .getIntProperty("autorefund.weekly.cron.scanningDelayDays"));
        
        DateMidnight startDate = endDate.minusDays(fcProperties
                .getIntProperty("autorefund.weekly.cron.scanningWindowDays"));
        Boolean async = false;
        if (args.length >= 1) {
            if (args[0].equals("async")) {
                async = true;
            }
        }
        
        try {
            fjPool.submit(new AutoRefundAction(startDate.toDate(), endDate.toDate(), autoRefund, async)).get();
        } catch (Exception e) {
            logger.error("ERROR occured in executing AutoRefundWeeklyCleanupJob.");
            logger.error(e.getCause(), e);
        } finally {
            fjPool.shutdown();
            
            try {
                fjPool.awaitTermination(1, TimeUnit.MINUTES);
            } catch (InterruptedException ie) {
                logger.info("Got interrupted while waiting fork-join pool going ahead with clean-up", ie);
            }

            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) {
        AutoRefund autoRefund = ((AutoRefund) jobContext.getBean("autoRefund"));
        FCProperties fcProperties = jobContext.getBean(FCProperties.class);

        DateMidnight endDate = (new DateMidnight()).minusDays(fcProperties
                .getIntProperty("autorefund.weekly.cron.scanningDelayDays"));
        
        DateMidnight startDate = endDate.minusDays(fcProperties
                .getIntProperty("autorefund.weekly.cron.scanningWindowDays"));

        Boolean async = false;
        if (arguments.length >= 1) {
            if (arguments[0].equals("async")) {
                async = true;
            }
        }

        ForkJoinPool fjPool = new ForkJoinPool();

        try {
            fjPool.submit(new AutoRefundAction(startDate.toDate(), endDate.toDate(), autoRefund, async)).get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            fjPool.shutdown();

            try {
                fjPool.awaitTermination(1, TimeUnit.MINUTES);
            } catch (InterruptedException ie) {
                logger.info("Got interrupted while waiting fork-join pool going ahead with clean-up", ie);
            }
        }
    }
}
