package com.freecharge.batch.job;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.app.domain.entity.jdbc.Users;
import com.freecharge.common.comm.sms.SMSBusinessDO;
import com.freecharge.common.comm.sms.SMSService;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCConstants;
import com.freecharge.common.util.SpringBeanLocator;
import com.freecharge.growthevent.service.UserServiceProxy;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.productdata.services.RechargePlanService;
import com.freecharge.recharge.services.RechargeReminderService;
import com.freecharge.rest.recharge.RemindRecharge;

public class RechargeReminderJob {
    private static final Logger logger = LoggingFactory.getLogger(RechargeReminderJob.class);

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml",
                "web-delegates.xml");
        SpringBeanLocator.getInstance().setApplicationContext(ctx);
        RechargeReminderService rechargeReminderService = (RechargeReminderService) ctx
                .getBean(RechargeReminderService.class);
        RechargePlanService rechargePlanService = (RechargePlanService) ctx.getBean(RechargePlanService.class);
        SMSService smsService = (SMSService) ctx.getBean(SMSService.class);

        UserServiceProxy userServiceProxy = (UserServiceProxy) ctx.getBean(UserServiceProxy.class);

        List<RemindRecharge> currentDayReminders = rechargeReminderService.getReminderForDay();

        for (RemindRecharge remindRecharge : currentDayReminders) {
            try {
                if (rechargeReminderService.isRechargeAlreadyDone(remindRecharge)) {
                    continue;
                }

                RechargePlan rechargePlan = rechargePlanService.getRechargePlan(remindRecharge.getPlanId());
                if (rechargePlan == null || !rechargePlan.getStatus()) {
                    continue;
                }
                Users user = userServiceProxy.getUserByUserId(remindRecharge.getUserId());
                SMSBusinessDO smsBusinessDO = new SMSBusinessDO();
                Map<String, String> params = new HashMap<String, String>();
                params.put("rechargeamount", String.valueOf(remindRecharge.getRechargeAmount()));
                params.put("mobileno", remindRecharge.getRechargeNo());
                smsBusinessDO.setReceiverNumber(remindRecharge.getRechargeNo());
                smsBusinessDO.setTemplateName(FCConstants.SMS_RECHARGE_REMINDER);
                smsBusinessDO.setVariableValues(params);
                smsBusinessDO.setRegisteredUserEmail(user.getEmail());
                smsService.sendSMS(smsBusinessDO);
                logger.info("Sent SMS reminder for " + user.getEmail() + ", mobNumber : "
                        + remindRecharge.getRechargeNo());
            } catch (Exception e) {
                logger.error("Exception on running job : ", e);
            } finally {
                remindRecharge.setActive(false);
                rechargeReminderService.updateRemindRecharge(remindRecharge);
            }
        }
        ctx.close();
    }
}
