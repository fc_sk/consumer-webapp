package com.freecharge.batch.job;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.cron.UploadCodesCron;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class UploadCodesJob extends FreeChargeJob{
    private static final Logger logger = LoggingFactory
            .getLogger(UploadCodesJob.class);

    private UploadCodesCron handler;

    private void execute() throws Exception {
        logger.info("----------------------------STARTING InsertCodesIntoMongoJob------------------------");
        getHandler().doUploadCodes();
        logger.info("----------------------------FINISHED InsertCodesIntoMongoJob------------------------");
    }

    public static void main(String[] args) {
        Thread.currentThread().setName("uploadCodesJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        UploadCodesJob uploadCodesJob = (UploadCodesJob) ctx.getBean("uploadCodesJob");

        try {
            uploadCodesJob.execute();
        } catch (Exception e) {
            logger.error("ERROR occured in executing insertCodesIntoMongoJob.");
            logger.error(e.getCause(), e);
        } finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
        }
    }

    public UploadCodesCron getHandler() {
        return handler;
    }

    public void setHandler(UploadCodesCron handler) {
        this.handler = handler;
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        UploadCodesCron delegate = (UploadCodesCron) jobContext.getBean("uploadCodesCronHandler");
        delegate.doUploadCodes();
    }
}
