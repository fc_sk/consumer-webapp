package com.freecharge.batch.job;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.payment.util.PaymentConstants;
import com.freecharge.recharge.services.UserTransactionHistoryService;

@Service
public class UPOrderUpdateService {
    private static Logger         logger = LoggingFactory.getLogger(UPOrderUpdateService.class);

    @Autowired
    UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    KestrelWrapper                kestrelWrapper;

    public void process(Integer m1, Integer m2) {
        Date now = new Date();
        Date to = DateUtils.addMinutes(now, -m1);
        Date from = DateUtils.addMinutes(to, -m2);
        logger.info("Checking pending orders from " + from + " to " + to);
        processPendingOrders(from, to);
    }

    private void processPendingOrders(Date from, Date to) {
        List<String> pendingOrders = userTransactionHistoryService.findPendingTransactionsBetween(from, to);

        if (pendingOrders == null) {
            logger.info("Could not find any pending orders between " + from + " and " + to);
            return;
        } else {
            logger.info("Processing pending orders. Count = " + pendingOrders.size());
        }

        for (String pendingOrder : pendingOrders) {
            kestrelWrapper.enqueueUPUpdate(pendingOrder);
        }

    }

}
