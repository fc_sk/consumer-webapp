package com.freecharge.batch.job;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.hibernate.envers.tools.Pair;

import com.freecharge.common.util.FCUtil;

public class RechargePlanDailyMapper {
	
	static List<String> AIRCEL_CIRCLE = Arrays.asList("Andhra Pradesh",
												"Chennai",
												"Delhi",
												"Karnataka",
												"Kolkata",
												"Mumbai",
												"Rajasthan",
												"Tamil Nadu",
												"Uttar Pradesh (E)"
			);
	
	static List<String> AIRTEL_CIRCLE = Arrays.asList("Andhra Pradesh",
												"Bihar",
												"Chennai",
												"Delhi",
												"Gujarat",
												"Haryana",
												"Karnataka",
												"Kerala",
												"Kolkata",
												"Madhya Pradesh",
												"Maharashtra",
												"Mumbai",
												"Orissa",
												"Punjab",
												"Rajasthan",
												"Tamil Nadu",
												"Uttar Pradesh (E)",
												"Uttar Pradesh (W)"
			);
	
	static List<String> BSNL_CIRCLE = Arrays.asList("Andhra Pradesh",
												"Bihar",
												"Gujarat",
												"Haryana",
												"Karnataka",
												"Kerala",
												"Madhya Pradesh",
												"Maharashtra",
												"Orissa",
												"Rajasthan",
												"Tamil Nadu",
												"Uttar Pradesh (E)",
												"Uttar Pradesh (W)"
			);
	
	static List<String> IDEA_CIRCLE = Arrays.asList("Andhra Pradesh",
												"Delhi",
												"Gujarat",
												"Haryana",
												"Karnataka",
												"Kerala",
												"Madhya Pradesh",
												"Maharashtra",
												"Mumbai",
												"Rajasthan",
												"Tamil Nadu",
												"Uttar Pradesh (E)",
												"Uttar Pradesh (W)"
			);
	
	static List<String> LOOP_CIRCLE = Arrays.asList("Mumbai");
	
	static List<String> RELIANCE_CDMA_CIRCLE = Arrays.asList("Delhi");
	
	static List<String> RELIANCE_GSM_CIRCLE = Arrays.asList("Andhra Pradesh",
												"Bihar",
												"Delhi",
												"Gujarat",
												"Karnataka",
												"Madhya Pradesh",
												"Maharashtra",
												"Mumbai",
												"Punjab",
												"Rajasthan"
			);
	
	static List<String> TATA_DOCOMO_GSM_CIRCLE = Arrays.asList("Andhra Pradesh",
												"Gujarat",
												"Haryana",
												"Karnataka",
												"Madhya Pradesh",
												"Maharashtra",
												"Mumbai",
												"Punjab",
												"Tamil Nadu",
												"Uttar Pradesh (E)",
												"Uttar Pradesh (W)"
			);
	
	static List<String> TATA_INDICOM_GSM_CIRCLE = Arrays.asList("Delhi");
	
	static List<String> UNINOR_CIRCLE = Arrays.asList("Andhra Pradesh",
												"Bihar",
												"Gujarat",
												"Maharashtra",
												"Uttar Pradesh (E)"
			);
	
	static List<String> VODAFONE_CIRCLE = Arrays.asList("Andhra Pradesh",
												"Bihar",
												"Chennai",
												"Delhi",
												"Gujarat",
												"Haryana",
												"Karnataka",
												"Kerala",
												"Kolkata",
												"Madhya Pradesh",
												"Maharashtra",
												"Mumbai",
												"Punjab",
												"Rajasthan",
												"Tamil Nadu",
												"Uttar Pradesh (E)",
												"Uttar Pradesh (W)"
		);


	
	// Operator circle map which needs to be run on a daily basis
	private static final Map<String, List<String>> DAILY_OPERATOR_CIRCLE_MAP = FCUtil.createMap(
            // Freecharge operator code, Freecharge Circle name
			"Aircel",   AIRCEL_CIRCLE,
			"Airtel",   AIRTEL_CIRCLE,
			"BSNL",   BSNL_CIRCLE,
			"Idea",   IDEA_CIRCLE,
			"Loop",   LOOP_CIRCLE,
			"Reliance-CDMA",   RELIANCE_CDMA_CIRCLE,
			"Reliance-GSM",   RELIANCE_GSM_CIRCLE,
			"Tata Docomo GSM",   TATA_DOCOMO_GSM_CIRCLE,
			"Tata Indicom",   TATA_INDICOM_GSM_CIRCLE,
			"Uninor",   UNINOR_CIRCLE,
			"Vodafone",   VODAFONE_CIRCLE
            );
	
	public static List<Pair<String,String>> getDailyRunOperatorCirclePair() throws Exception{
		try {
			List<Pair<String, String>> dailyList = new ArrayList<Pair<String, String>>();
			for (String fcOperator: DAILY_OPERATOR_CIRCLE_MAP.keySet()) {
				for(String fcCircle: DAILY_OPERATOR_CIRCLE_MAP.get(fcOperator)){
					Pair<String, String> pair = new Pair<String, String>(fcCircle, fcOperator);
					dailyList.add(pair);
				}
			}
			return dailyList;
		}catch (Exception e) {
			throw e;
		}
	}
	
}
