package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;

public class UPOrderUpdateJob extends FreeChargeJob {
    private static final Logger logger = LoggingFactory.getLogger(UPOrderUpdateJob.class);

    /*
     * This Job is to pick up all the orders which are pending ('08', '-1')
     * status and do the checks / refunds / fulfilment
     */

    public static void main(String[] args) {
        Thread.currentThread().setName("UPOrderUpdateJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        UPOrderUpdateJob job = (UPOrderUpdateJob) ctx.getBean("upOrderUpdateJob");

        try {
            job.execute(ctx, args);
        } catch (Exception e) {
            logger.error("ERROR occured in executing UPOrderUpdateJob.", e);
        } finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.", e);
            }
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) {
        Integer m1 = 90;
        Integer m2 = 240;
        if (arguments.length == 2) {
            m1 = Integer.parseInt(arguments[0]);
            m2 = Integer.parseInt(arguments[1]);
        }

        UPOrderUpdateService upOrderUpdateService = jobContext.getBean(UPOrderUpdateService.class);
        upOrderUpdateService.process(m1, m2);
    }

}
