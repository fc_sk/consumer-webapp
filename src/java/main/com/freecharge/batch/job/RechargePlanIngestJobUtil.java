package com.freecharge.batch.job;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.app.domain.entity.ProductMaster;
import com.freecharge.app.domain.entity.jdbc.CircleMaster;
import com.freecharge.app.domain.entity.jdbc.OperatorMaster;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.util.FCUtil;
import com.freecharge.common.util.HashMapIgnoreCase;
import com.freecharge.productdata.dao.jdbc.RechargePlanWriteDAO;
import com.freecharge.productdata.entity.DenominationType;
import com.freecharge.productdata.entity.RechargePlan;
import com.freecharge.productdata.entity.RechargePlanCategory;
import com.freecharge.productdata.services.RechargePlanModerationCausePair;
import com.freecharge.productdata.services.RechargePlanService;

@Component("rechargePlanIngestJobUtil")
public class RechargePlanIngestJobUtil {
    private static final Logger  LOGGER = LoggingFactory.getLogger(RechargePlanIngestJobUtil.class);
    private static final String  TOPUP                    = "Topup";
    private static final String  NAME                     = "name";
    private static final String  DISPLAYNAME              = "displayName";

    @Autowired
    private RechargePlanWriteDAO rechargePlanWriteDAO;
    @Autowired
    private RechargePlanService  rechargePlanService;

    private static final String  MODERATION_CAUSE_FULL_TT = "Full Talktime Plan.";
    private static final int     MOBILE_MASTER_ID         = 1;
    private static final int     DATACARD_MASTER_ID         = 3;
    private static final String  OTHER                    = "Other";
    private static final String  PLAN_NOT_IN_IREFF        = "Plan not found in Ireff.";
    private static final String  SPECIAL_PLAN             = "Special Plan. ";
    private static final String  TOPUP_PLAN               = "Topup Plan. ";

    // Checks if two talk times are the same
    public boolean sameTalktime(final RechargePlan rp1, final RechargePlan rp2) {
        BigDecimal tt1 = rp1.getTalktime();
        BigDecimal tt2 = rp1.getTalktime();
        if (tt1.setScale(0, RoundingMode.HALF_UP).compareTo(tt2.setScale(0, RoundingMode.HALF_UP)) != 0) {
            return false;
        }
        return true;
    }

    // Process and push recharge plans to the moderation queue
    public List<RechargePlan> processForModeration(final List<RechargePlanModerationCausePair> plansToBeModerated) {
        List<RechargePlan> rechargePlansAddedToDB = new ArrayList<>();
        RechargePlan rechargePlan = new RechargePlan();
        try {
            for (RechargePlanModerationCausePair rechargePlanModerationCausePair : plansToBeModerated) {
                rechargePlan = rechargePlanModerationCausePair.getRechargePlan();
                try {
                    assertValidRechargePlanID(rechargePlan);
                } catch (Exception e) {
                    if (!rechargePlanService.checkPlanExistsInModeration(rechargePlan)) {
                        rechargePlansAddedToDB.add(rechargePlan);
                        rechargePlanWriteDAO.insertechargePlan(rechargePlan);
                    } else {
                        LOGGER.info("Plan Exists in moderation queue : " + rechargePlan.getRechargePlanId());
                        continue;
                    }
                }

                rechargePlanService.addRechargePlanToModeration(rechargePlan,
                        rechargePlanModerationCausePair.getCause());
                if (rechargePlan.getStatus()) {
                    rechargePlan.setStatus(false);
                    rechargePlanWriteDAO.updateStatus(rechargePlan);
                }
            }

        } catch (Exception e) {
            LOGGER.info("Plan not added to moderation queue. It might aleady exist.", e);
        }
        return rechargePlansAddedToDB;
    }

    private void assertValidRechargePlanID(final RechargePlan rechargePlan) {
        if (rechargePlan == null || rechargePlan.getRechargePlanId() == 0) {
            throw new IllegalArgumentException("Invalid RechargePlan argument: " + rechargePlan == null ? "NULL"
                    : rechargePlan.toString());
        }
    }

    // Returns the length of the description stored in rechargePlan
    public int getDescriptionLength(final RechargePlan rechargePlan) throws Exception {
            return rechargePlan.getDescription().trim().length();
    }

    // Checks if a recharge plan rp exists in the Recharge Plan list result.
    public boolean checkIfDuplicate(final RechargePlan rp, final List<RechargePlan> result) {
        for (RechargePlan rechargePlan : result) {
            if (rechargePlan.getIreffID() != null) {
                if (rechargePlan.getIreffID().equals(rp.getIreffID()) && rechargePlan.getName().equals(rp.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    // Returns a list a recharge plans having id of invalidPlan from
    // rechargePlanList
    public List<RechargePlan> getAllInvalidPlansFromList(final RechargePlan invalidPlan,
            final List<RechargePlan> rechargePlanList) {
        List<RechargePlan> result = new ArrayList<>();
        for (RechargePlan rechargePlan : rechargePlanList) {
            if (rechargePlan.getIreffID() != null) {
                if (rechargePlan.getIreffID().equals(invalidPlan.getIreffID())) {
                    result.add(rechargePlan);
                }
            }
        }
        return result;
    }

    // Returns non sticky plans which got Full TT
    public List<RechargePlanModerationCausePair> getFullTTPlansToBeModerated(final List<RechargePlan> allPlansDB) {
        List<RechargePlanModerationCausePair> planFullTT = new ArrayList<>();
        for (RechargePlan rechargePlan : allPlansDB) {
            if (!rechargePlan.isSticky() && isFullTT(rechargePlan)) {
                planFullTT.add(new RechargePlanModerationCausePair(rechargePlan, MODERATION_CAUSE_FULL_TT));
            }
        }
        return planFullTT;
    }
    // Checks if the recharge plan is a full TT
    public boolean isFullTT(final RechargePlan rechargePlan) {
        if (rechargePlan.getTalktime().compareTo(rechargePlan.getAmount()) >= 0) {
            return true;
        }
        return false;
    }
    // Creates and returns a map between circle name and id
    public HashMapIgnoreCase<Integer> getCircleMap(final List<CircleMaster> circles) {
        HashMapIgnoreCase<Integer> circleMap = new HashMapIgnoreCase<>();
        for (CircleMaster each : circles) {
            circleMap.put(each.getName(), each.getCircleMasterId());
        }
        return circleMap;
    }

    // Creates and returns a map between operator name and id
    public HashMapIgnoreCase<Integer> getOperatorMap(final List<OperatorMaster> operators) {
        HashMapIgnoreCase<Integer> operatorMap = new HashMapIgnoreCase<>();
        for (OperatorMaster each : operators) {
            if (each.getFkProductMasterId() == MOBILE_MASTER_ID) {
                operatorMap.put(each.getOperatorCode(), each.getOperatorMasterId());
            }
        }
        return operatorMap;
    }
 // Creates and returns a map between operator name and id
    public HashMapIgnoreCase<Integer> getDatacardOperatorMap(final List<OperatorMaster> operators) {
        HashMapIgnoreCase<Integer> operatorMap = new HashMapIgnoreCase<>();
        for (OperatorMaster each : operators) {
            if (each.getFkProductMasterId() == DATACARD_MASTER_ID) {
                operatorMap.put(each.getOperatorCode(), each.getOperatorMasterId());
            }
        }
        return operatorMap;
    }
    // Function to merge operator to the description
    public String appendOperatorToDesc(final String operator, final String desc, final String category) {
        return (category.equals(OTHER)) ? desc : (operator + " " + desc);
    }

    // Gets all the plans for a particular amount
    public List<RechargePlan> getSimilarPlansInAPI(final List<RechargePlan> rechargePlans, final BigDecimal amount,
            final boolean isSpecial, final Integer operatorId) {
        List<RechargePlan> rechargePlansForAmount = new ArrayList<>();
        for (RechargePlan rechargePlan : rechargePlans) {
            if (rechargePlan.getAmount().equals(BigDecimal.valueOf(amount.intValue()))) {
                if (!rechargePlanService.SPECIAL_OPERATOR_IDS.contains(operatorId)) {
                    rechargePlansForAmount.add(rechargePlan);
                } else if (!isSpecial && rechargePlan.getName().equals(TOPUP)) {
                    rechargePlansForAmount.add(rechargePlan);
                } else if (isSpecial && !rechargePlan.getName().equals(TOPUP)) {
                    rechargePlansForAmount.add(rechargePlan);
                }
            }
        }
        return rechargePlansForAmount;
    }

 // Gets all the plans for a particular amount
    public List<RechargePlan> getSimilarDataPlansInAPI(final List<RechargePlan> rechargePlans, final BigDecimal amount,
            final boolean isSpecial, final Integer operatorId, final Integer circleId) {
        List<RechargePlan> rechargePlansForAmount = new ArrayList<>();
        for (RechargePlan rechargePlan : rechargePlans) {
            if (rechargePlan.getAmount().equals(BigDecimal.valueOf(amount.intValue()))) {
                
                //RE_WRITE THIS LOGIC TO FETCH PLANS OF AIRTEL, VODAFONE, IDEA & custome circles
                if (!rechargePlanService.SPECIAL_OPERATOR_IDS.contains(operatorId)) {
                    rechargePlansForAmount.add(rechargePlan);
                } else if (!isSpecial && rechargePlan.getName().equals(TOPUP)) {
                    rechargePlansForAmount.add(rechargePlan);
                } else if (isSpecial && !rechargePlan.getName().equals(TOPUP)) {
                    rechargePlansForAmount.add(rechargePlan);
                }
                
            }
        }
        return rechargePlansForAmount;
    }
    
    public RechargePlan createRechargePlanFromAmount(final int operatorMasterId, final int circleMasterId,
            final BigDecimal amount, boolean isRecomended) {
        RechargePlan rp = new RechargePlan();
        rp.setAmount(amount);
        rp.setCircleMasterId(circleMasterId);
        rp.setOperatorMasterId(operatorMasterId);
        rp.setStatus(false);
        rp.setCreatedTime(new Timestamp(new Date().getTime()));
        rp.setTalktime(BigDecimal.ZERO);
        rp.setIsRecommended(isRecomended);
        rp.setDenominationType(DenominationType.FIXED_DENOMINATION.getName());
        rp.setProductType(ProductMaster.ProductName.Mobile.getProductId());
        return rp;
    }

    public List<RechargePlanModerationCausePair> getRechargePlanModerationCausePairs(
            final List<RechargePlan> rechargePlans, final String cause) {
        List<RechargePlanModerationCausePair> rechargePlanModerationCausePairs = new ArrayList<>();
        for (RechargePlan rechargePlan : rechargePlans) {
            rechargePlanModerationCausePairs.add(new RechargePlanModerationCausePair(rechargePlan, cause));
        }
        return rechargePlanModerationCausePairs;
    }

    // Maps Data from Ireff Category API "name"=>"displayName"
    public Map<String, String> setRechargePlanCategoryMap(final String rechargePlanCategoryString) throws IOException,
            ParseException {
        Reader rechargePlanCategoryReader = new StringReader(rechargePlanCategoryString);
        Object deserializedRechargePlanCategory = new JSONParser().parse(rechargePlanCategoryReader);

        JSONArray rechargePlanCategoryArray = (JSONArray) deserializedRechargePlanCategory;
        List<RechargePlanCategory> rechargePlanCategoryList = new ArrayList<>(rechargePlanCategoryArray.size());
        for (Object each : rechargePlanCategoryArray) {
            JSONObject row = (JSONObject) each;
            RechargePlanCategory rpc = new RechargePlanCategory();
            rpc.setName((String) row.get(NAME));
            rpc.setDisplayName((String) row.get(DISPLAYNAME));
            rechargePlanCategoryList.add(rpc);
        }
        Map<String, String> rpcMap = new HashMap<>();
        if (rechargePlanCategoryList != null && rechargePlanCategoryList.size() > 0) {
            for (RechargePlanCategory rpc : rechargePlanCategoryList) {
                rpcMap.put(rpc.getName(), rpc.getDisplayName());
            }
        }
        return rpcMap;
    }

    public RechargePlanModerationCausePair getRechargePlanToBeModerated(final RechargePlan rechargePlan,
            final List<RechargePlan> allPlansDB, final boolean isSpecial) {
        List<RechargePlan> similarPlansInDB = getSimilarExistingPlans(rechargePlan, allPlansDB, isSpecial);
        if (FCUtil.isEmpty(similarPlansInDB)) {
            if (rechargePlanService.SPECIAL_OPERATOR_IDS.contains(rechargePlan.getOperatorMasterId())) {
                if (isSpecial) {
                    return new RechargePlanModerationCausePair(rechargePlan, SPECIAL_PLAN + PLAN_NOT_IN_IREFF);
                } else {
                    return new RechargePlanModerationCausePair(rechargePlan, TOPUP_PLAN + PLAN_NOT_IN_IREFF);
                }
            } else {
                return new RechargePlanModerationCausePair(rechargePlan, PLAN_NOT_IN_IREFF);
            }
        }
        return null;
    }

    public List<RechargePlan> getSimilarExistingPlans(final RechargePlan planAPI, final List<RechargePlan> allPlanDB,
            final boolean isSpecial) {
        List<RechargePlan> rechargePlans = new ArrayList<>();
        for (RechargePlan rechargePlan : allPlanDB) {
            final String name = rechargePlan.getName();
            if (rechargePlan.getProductType() == planAPI.getProductType()
                    && rechargePlan.getAmount().intValue() == planAPI.getAmount().intValue()) {
                if (!rechargePlanService.SPECIAL_OPERATOR_IDS.contains(planAPI.getOperatorMasterId())) {
                    rechargePlans.add(rechargePlan);
                } else if (FCUtil.isEmpty(name)) {
                    rechargePlans.add(rechargePlan);
                } else if (!isSpecial && name.equals(TOPUP)) {
                    rechargePlans.add(rechargePlan);
                } else if (isSpecial && !name.equals(TOPUP)) {
                    rechargePlans.add(rechargePlan);
                }
            }
        }
        return rechargePlans;
    }

    public void enablePlansNotInModeration(final List<RechargePlan> rechargePlans) {
        for (RechargePlan rechargePlan : rechargePlans) {
            if (!rechargePlan.getStatus()
                    && !rechargePlanService.isRechargePlanInModeration(rechargePlan.getRechargePlanId()) && !rechargePlan.isSticky()) {
                rechargePlan.setStatus(true);
                rechargePlanService.updateRechargePlanStatus(rechargePlan);
            }
        }
    }
    
    public void setRecomendedPlans(final List<RechargePlan> rechargePlans) {
        rechargePlanService.updateRechargePlanRecomended(rechargePlans);
    }
}
