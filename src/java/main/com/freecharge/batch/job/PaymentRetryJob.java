package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.services.PaymentRetryService;

/**
 * @author chirag, Feb 2016 This job enqueues failed payments in the last 30 min
 *         for payment retry notification
 */
public class PaymentRetryJob extends FreeChargeJob {

	private static final Logger logger = LoggingFactory.getLogger(PaymentRetryJob.class);

	@Override
	protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) {
		Integer m1 = 1;
		Integer m2 = 10;
		if (arguments.length == 2) {
			m1 = Integer.parseInt(arguments[0]);
			m2 = Integer.parseInt(arguments[1]);
		}

		PaymentRetryService paymentRetryService = jobContext.getBean(PaymentRetryService.class);
		logger.info("------------Started PaymentRetryJob------------");
		paymentRetryService.process(m1, m2);
		logger.info("------------Finished PaymentRetryJob-----------");
	}

	public static void main(String[] args) {
		Thread.currentThread().setName("PaymentRetryJob");
		ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
		PaymentRetryJob job = (PaymentRetryJob) ctx.getBean("paymentRetryJob");

		try {
			job.execute(ctx, args);
		} catch (Exception e) {
			logger.error("ERROR occured in executing PaymentRetryJob.", e);
		} finally {
			try {
				BatchJobUtil.destroyBatchJobContext(ctx);
			} catch (IOException e) {
				logger.error("ERROR while destroying batch job context.", e);
			}
		}
	}

}
