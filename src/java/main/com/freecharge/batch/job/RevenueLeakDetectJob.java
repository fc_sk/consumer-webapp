package com.freecharge.batch.job;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.revenueleak.DetectRevenueLeakService;
 
@Component
public class RevenueLeakDetectJob extends FreeChargeJob {
	private static final Logger logger = LoggingFactory
			.getLogger(RevenueLeakDetectJob.class);
 

    @Autowired
    private DetectRevenueLeakService detectRevenueLeakService;
	
	
	public DetectRevenueLeakService getDetectRevenueLeakService() {
		return detectRevenueLeakService;
	}

	public void setDetectRevenueLeakService(
			DetectRevenueLeakService detectRevenueLeakService) {
		this.detectRevenueLeakService = detectRevenueLeakService;
	}

	private void execute(Date from, Date to) throws Exception {
		logger.info("STARTING RevenueLeakDetectJob from : "+ from.toString() +" to "+ to.toString());
		detectRevenueLeakService.initiate(from, to);
  		logger.info("----------------------------FINISHED RevenueLeakDetectJob------------------------");
	}

	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Date getDate(String dateString) throws ParseException {
        try{
        	return new SimpleDateFormat(DATE_FORMAT).parse(dateString);
        }catch(Exception e){
        	logger.error("Invalid date " + dateString);
        	throw e;
        }
    }

	public static void main(String[] args) {
        Thread.currentThread().setName("revenueLeakDetectJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        RevenueLeakDetectJob revenueLeakDetectJob = (RevenueLeakDetectJob) ctx.getBean("revenueLeakDetectJob");

        try {
        	Date start = getDate(args[0]);
            Date end = getDate(args[1]);
            if(!start.before(end)){
            	throw new Exception("start date is not before end date");
            }else{
            	revenueLeakDetectJob.execute(start, end);	
            }
		} catch (Exception e) {
			logger.error("ERROR occured in executing RevenueLeakDetectJob.");
			logger.error(e.getCause(), e);
		} finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
		}
	}

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        Date start = getDate(arguments[0]);
        Date end = getDate(arguments[1]);
        if(!start.before(end)){
            throw new Exception("start date is not before end date");
        } else {
            DetectRevenueLeakService delegate = jobContext.getBean(DetectRevenueLeakService.class);
            delegate.initiate(start, end);
        }
    }
}

