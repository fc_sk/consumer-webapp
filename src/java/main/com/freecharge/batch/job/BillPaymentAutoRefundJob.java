package com.freecharge.batch.job;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freebill.autorefund.BillPaymentAutoRefund;

public class BillPaymentAutoRefundJob extends FreeChargeJob {

    private static final Logger logger = LoggingFactory
            .getLogger(BillPaymentAutoRefundJob.class);
    /**
     * @param args
     */
    public static void main(String[] args) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        Date endDate = calendar.getTime();
        calendar.add(Calendar.DATE, -7);
        Date startDate = calendar.getTime();
        logger.info(String.format("Doing bill payment auto refund for candidates from %s to %s", startDate, endDate));
        doBillPaymentAutoRefund(startDate, endDate);
    }

    private static void doBillPaymentAutoRefund(Date startDate, Date endDate) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml",
                "web-delegates.xml");
        BillPaymentAutoRefund billPaymentAutoRefund = (BillPaymentAutoRefund) ctx.getBean("billPaymentAutoRefund");
        try {
            billPaymentAutoRefund.doBillPaymentRefundForBillPaymentFailedTransactions(startDate, endDate);
        } catch (Exception exception) {
            logger.error("Exception while doing Bill payment autorefund", exception);
        } finally {
            ctx.close();
            System.exit(0);
        }

    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        Date endDate = calendar.getTime();
        calendar.add(Calendar.DATE, -7);
        Date startDate = calendar.getTime();
        logger.info(String.format("Doing bill payment auto refund for candidates from %s to %s", startDate, endDate));
        
        BillPaymentAutoRefund billPaymentAutoRefund = (BillPaymentAutoRefund) jobContext.getBean("billPaymentAutoRefund");
        billPaymentAutoRefund.doBillPaymentRefundForBillPaymentFailedTransactions(startDate, endDate);
    }
}
