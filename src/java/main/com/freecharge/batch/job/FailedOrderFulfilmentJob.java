package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;

public class FailedOrderFulfilmentJob extends FreeChargeJob {
    private static final Logger logger = LoggingFactory.getLogger(FailedOrderFulfilmentJob.class);

    /*
     * This Job is to pick up all the orders which has not reached the final
     * status and do the refund / fulfilment
     */

    public static void main(String[] args) {
        Thread.currentThread().setName("FailedOrderFulfilmentJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        FailedOrderFulfilmentJob job = (FailedOrderFulfilmentJob) ctx.getBean("failedOrderFulfilmentJob");

        try {
            job.execute(ctx, args);
        } catch (Exception e) {
            logger.error("ERROR occured in executing FailedOrderFulfilmentJob.", e);
        } finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.", e);
            }
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) {
        Integer m1 = 30;
        Integer m2 = 180;
        if (arguments.length == 2) {
            m1 = Integer.parseInt(arguments[0]);
            m2 = Integer.parseInt(arguments[1]);
        }

        FailedOrderFulfilmentService failedOrderFulfilmentService = jobContext
                .getBean(FailedOrderFulfilmentService.class);
        failedOrderFulfilmentService.process(m1, m2);
    }

}
