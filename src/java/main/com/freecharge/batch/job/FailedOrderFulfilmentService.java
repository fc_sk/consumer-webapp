package com.freecharge.batch.job;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.kestrel.KestrelWrapper;
import com.freecharge.recharge.services.UserTransactionHistoryService;

@Service
public class FailedOrderFulfilmentService {
    private static Logger         logger = LoggingFactory.getLogger(FailedOrderFulfilmentService.class);

    @Autowired
    UserTransactionHistoryService userTransactionHistoryService;

    @Autowired
    KestrelWrapper                kestrelWrapper;

    public void process(Integer m1, Integer m2) {
        Date now = new Date();
        Date to = DateUtils.addMinutes(now, -m1);
        Date from = DateUtils.addMinutes(to, -m2);
        logger.info("Checking failed orders from " + from + " to " + to);
        processFailedOrders(from, to);
    }

    private void processFailedOrders(Date from, Date to) {
        List<String> failedOrders = userTransactionHistoryService.findFailedTransactionsBetween(from, to);

        if (failedOrders == null) {
            logger.info("Could not find any failed orders between " + from + " and " + to);
            return;
        } else {
            logger.info("Processing failed orders. Count = " + failedOrders.size());
        }

        for (String failedOrder : failedOrders) {
            kestrelWrapper.enqueueForFailureFulfilment(failedOrder);
        }
    }

}
