package com.freecharge.batch.job;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.autorefund.RefundUtil;
import com.freecharge.payment.services.PaymentRefundService;

public class RefundSchedulerJob extends FreeChargeJob {
    private static final Logger  logger = LoggingFactory.getLogger(RefundSchedulerJob.class);
    private PaymentRefundService handler;

    private void execute(DateTime createdFrom, DateTime createdTo) throws Exception {
        logger.info("----------------------------STARTING RefundSchedulerJob------------------------");
        getHandler().triggerScheduledRefunds(createdFrom, createdTo);
        logger.info("----------------------------FINISHED RefundSchedulerJob------------------------");
    }

    public static void main(String[] args) {
        Thread.currentThread().setName("RefundSchedulerJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        RefundSchedulerJob job = (RefundSchedulerJob) ctx.getBean("refundSchedulerJob");

        try {
            Date date = new Date();
            DateTime createdFrom = new DateTime(date);
            createdFrom.minusDays(1);

            DateTime createdTo = new DateTime(date);

            if (args.length >= 1) {
                logger.info("FROM DATE : " + args[0]);
                date = RefundUtil.getDate(args[0]);
                createdFrom = new DateTime(date.getTime());
            }
            if (args.length >= 2) {
                logger.info("TO DATE : " + args[1]);
                date = RefundUtil.getDate(args[1]);
                createdTo = new DateTime(date.getTime());
            }

            logger.info("CreatedFrom : " + createdFrom + " and createdTo : " + createdTo);
            job.execute(createdFrom, createdTo);

        } catch (Exception e) {
            logger.error("ERROR occured in executing RechargeSchedulerJob.");
            logger.error(e.getCause(), e);
        } finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
        }
    }

    public PaymentRefundService getHandler() {
        return handler;
    }

    public void setHandler(PaymentRefundService handler) {
        this.handler = handler;
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws ParseException {
        Date date = new Date();
        DateTime createdFrom = new DateTime(date);
        createdFrom.minusDays(1);

        DateTime createdTo = new DateTime(date);

        if (arguments.length >= 1) {
            date = RefundUtil.getDate(arguments[0]);
            createdFrom = new DateTime(date.getTime());
        }
        if (arguments.length >= 2) {
            date = RefundUtil.getDate(arguments[1]);
            createdTo = new DateTime(date.getTime());
        }

        PaymentRefundService paymentRefundService = jobContext.getBean(PaymentRefundService.class);
        paymentRefundService.triggerScheduledRefunds(createdFrom, createdTo);
    }

}
