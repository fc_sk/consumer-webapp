package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.freebill.service.BillPaymentService;

public class RefundUnknownTxnJob  extends FreeChargeJob {
	private static final Logger LOGGER = LoggingFactory.getLogger(RefundUnknownTxnJob.class);

    public static void main(final String[] args) throws IOException {
//        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
//        try {
//            Thread.currentThread().setName("RefundUnknownTxnJob");
//            BillPaymentService bean = (BillPaymentService) ctx.getBean("refundUnknownTxnJob");
//            bean.excuteUPTxnclearing();
//        } catch (RuntimeException e) {
//            LOGGER.error("RuntimeException occured when executing RefundUnknownTxnJob.", e);
//        } finally {
//            BatchJobUtil.destroyBatchJobContext(ctx);
//        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
    	BillPaymentService bean = (BillPaymentService) jobContext.getBean("refundUnknownTxnJob");
        bean.excuteUPTxnclearing();
    }
}
