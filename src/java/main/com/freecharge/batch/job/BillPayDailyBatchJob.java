package com.freecharge.batch.job;

import java.io.IOException;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.infrastructure.billpay.api.IBillPayService;

public class BillPayDailyBatchJob extends FreeChargeJob {

    private static final Logger logger                      = LoggingFactory.getLogger(BillPayDailyBatchJob.class);

    private IBillPayService     handler;

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        Calendar processingDate = Calendar.getInstance();
        getHandler().triggerBillPayDailyBatchJob(processingDate);
    }

    public static void main(String[] args) {
        logger.info("Starting BillPayDailyBatchJob");
        Thread.currentThread().setName("BillPayDailyBatchJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        BillPayDailyBatchJob job = (BillPayDailyBatchJob) ctx.getBean("billPayDailyBatchJob");
        try {
            job.execute(ctx);
        } catch (Exception e) {
            logger.error("ERROR occured in executing BillPayDailyBatchJob.", e);
        } finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
        }
        logger.info("Completed BillPayDailyBatchJob");
    }

    public IBillPayService getHandler() {
        return handler;
    }

    public void setHandler(IBillPayService handler) {
        this.handler = handler;
    }

}
