package com.freecharge.batch.job.framework;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.platform.metrics.MetricsClient;

/**
 * Base class for all the background jobs of
 * FreeCharge system. This class provides
 * generic, framework level features such as
 * resilience against failure, metrics, 
 * monitoring capability and clean startup/shutdown.
 * @author arun
 *
 */
public abstract class FreeChargeJob {
    public final void executeJob(ClassPathXmlApplicationContext jobContext, String...arguments) {
        long startTime = System.currentTimeMillis();
        
        MetricsClient metrics = (MetricsClient) jobContext.getBean("metricsClient");
        
        try {
            this.execute(jobContext, arguments);
            metrics.recordEvent("FreeChargeJob", this.getJobName(), "Success");
        } catch (Exception e) {
            metrics.recordEvent("FreeChargeJob", this.getJobName(), "Failure");
        } finally {
            final long executionTime = System.currentTimeMillis() - startTime;
            metrics.recordLatency("FreeChargeJob", this.getJobName() + ".ExecutionTime", executionTime);
        }
    }
    
    protected String getJobName() {
        return this.getClass().getSimpleName();
    }
    
    protected abstract void execute(ClassPathXmlApplicationContext jobContext, String...arguments) throws Exception;
}