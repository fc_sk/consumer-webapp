package com.freecharge.batch.job.framework;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * A command executor that takes in job name,
 * its arguments and execute it. It handles
 * correct starting up and shutting down of
 * job context.
 * @author arun
 *
 */
public class FreeChargeJobExecutor {
    private static final Logger logger = LoggingFactory.getLogger(FreeChargeJobExecutor.class);
    
    public static void main(String[] args) {
        if (args.length < 1) {
            throw new IllegalArgumentException("No job to execute");
        }
        
        String jobName = args[0];
        
        ClassPathXmlApplicationContext jobContext = null;
        
        try {
            jobContext = initialize();
            
            if (!jobContext.containsBean(jobName)) {
                throw new IllegalArgumentException("Job not defined : " + jobName);
            }
            
            List<String> jobParameters = extractJobParameters(args);
            
            FreeChargeJob fcJob = (FreeChargeJob) jobContext.getBean(jobName);
            
            fcJob.executeJob(jobContext, jobParameters.toArray(new String[args.length - 1]));
            
        } catch (Exception e) {
            logger.error("Error while executing job : " + jobName, e);
        } finally {
            try {
                destroyBatchJobContext(jobContext);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context for job: " + jobName, e);
            }
        }
        
    }

    private static List<String> extractJobParameters(String[] args) {
        List<String> argList = new ArrayList<>();
        
        /*
         * args[0] is job name hence
         * should be skipped.
         */
        for (int i = 1; i < args.length; i++) {
            argList.add(args[i]);
        }
        
        return argList;
    }
    
    private static ClassPathXmlApplicationContext initialize() {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("batchContext.xml");
        return ctx;
    }

    private static final void destroyBatchJobContext(ClassPathXmlApplicationContext context) throws IOException {
        if (context != null) {
            context.close();
        }
    }
}
