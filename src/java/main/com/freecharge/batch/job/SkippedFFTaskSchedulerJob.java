package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.fulfilment.service.SkippedFFTaskSchedulerService;

public class SkippedFFTaskSchedulerJob extends FreeChargeJob {
    private static final Logger logger = LoggingFactory
			.getLogger(SkippedFFTaskSchedulerJob.class);

	
	public static void main(String[] args) {
        Thread.currentThread().setName("skippedFFTaskSchedulerJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        SkippedFFTaskSchedulerJob job = (SkippedFFTaskSchedulerJob) ctx.getBean("skippedFFTaskSchedulerJob");

        try {
			// We will check status of recharges between (T-m1) -> (T-m2) -
			// where T = current time
			//Integer m1 = 15;
			//Integer m2 = 70;
			Integer m1 = 0;
            Integer m2 = 70;
			if (args.length == 2) {
				m1 = Integer.parseInt(args[0]);
				m2 = Integer.parseInt(args[1]);
			}

			job.execute(ctx, args);

		} catch (Exception e) {
			logger.error("ERROR occured in executing SkippedFFTaskSchedulerJob.");
			logger.error(e.getCause(), e);
		} finally {
		    try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
		}
	}

	

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String...arguments) {
        Integer m1 = 15;
        Integer m2 = 70;
        if (arguments.length == 2) {
            m1 = Integer.parseInt(arguments[0]);
            m2 = Integer.parseInt(arguments[1]);
        }
        
        SkippedFFTaskSchedulerService skippedFFTaskSchedulerService = jobContext.getBean(SkippedFFTaskSchedulerService.class);
        skippedFFTaskSchedulerService.pushSkippedFFRecords(m1, m2);        
    }

}
