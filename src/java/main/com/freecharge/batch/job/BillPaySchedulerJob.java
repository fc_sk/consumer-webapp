package com.freecharge.batch.job;

import java.io.IOException;
import java.text.ParseException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.services.BillPaySchedulerService;

public class BillPaySchedulerJob extends FreeChargeJob {
    private static final Logger logger = LoggingFactory
            .getLogger(BillPaySchedulerJob.class);
    private BillPaySchedulerService handler;

    private void execute(Integer m1, Integer m2) throws Exception {
        logger.info("----------------------------STARTING BillPaySchedulerJob------------------------");
        getHandler().getRetryBillPayStatus(m1, m2);
        logger.info("----------------------------FINISHED BillPaySchedulerJob------------------------");
    }

    public static void main(String[] args) {
        Thread.currentThread().setName("BillPaySchedulerJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        BillPaySchedulerJob job = (BillPaySchedulerJob) ctx.getBean("billPaySchedulerJob");

        try {
            // We will check status of bill payments between (T-m1) -> (T-m2) -
            // where T = current timl
            Integer m1 = 15;
            Integer m2 = 70;
            if (args.length == 2) {
                m1 = Integer.parseInt(args[0]);
                m2 = Integer.parseInt(args[1]);
            }

            job.execute(m1, m2);

        } catch (Exception e) {
            logger.error("ERROR occured in executing BillPaySchedulerJob.");
            logger.error(e.getCause(), e);
        } finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
        }
    }

    public BillPaySchedulerService getHandler() {
        return handler;
    }

    public void setHandler(BillPaySchedulerService handler) {
        this.handler = handler;
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String...arguments) throws ParseException {
        Integer m1 = 15;
        Integer m2 = 70;
        if (arguments.length == 2) {
            m1 = Integer.parseInt(arguments[0]);
            m2 = Integer.parseInt(arguments[1]);
        }
        
        BillPaySchedulerService billPaySchedulerService = jobContext.getBean(BillPaySchedulerService.class);
        billPaySchedulerService.getRetryBillPayStatus(m1, m2);        
    }

}

