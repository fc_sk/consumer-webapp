package com.freecharge.batch.job;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.services.OperatorUptimeCommService;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Created by yashveer on 30/5/16.
 * Job to send Operator Uptime Notification to all user recharges impacted by an Operator Downtime.
 */
public class OperatorUptimeCommJob extends FreeChargeJob {

    private static final Logger logger = LoggingFactory.getLogger(OperatorUptimeCommJob.class);

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) {
        /*Integer m1 = 1;
        Integer m2 = 10;
        if (arguments.length == 2) {
            m1 = Integer.parseInt(arguments[0]);
            m2 = Integer.parseInt(arguments[1]);
        }*/

        OperatorUptimeCommService operatorUpNotificationService = jobContext.getBean(OperatorUptimeCommService.class);
        logger.info("------------Started OperatorUptimeCommunicationJob------------");
        operatorUpNotificationService.checkInactiveOperators();
        logger.info("------------Finished OperatorUptimeCommunicationJob-----------");
    }

    public static void main(String[] args) {
        Thread.currentThread().setName("OperatorUptimeCommJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        OperatorUptimeCommJob job = (OperatorUptimeCommJob) ctx.getBean("operatorUptimeCommJob");

        try {
            job.execute(ctx, args);
        } catch (Exception e) {
            logger.error("ERROR occured while executing OperatorUptimeCommJob.", e);
        } finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.", e);
            }
        }
    }
}
