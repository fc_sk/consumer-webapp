package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.freecharge.app.service.VoucherService;
import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;

@Component
public class RefreshCouponsJob extends FreeChargeJob {
	private static final Logger logger = LoggingFactory
			.getLogger(RefreshCouponsJob.class);
	
	@Autowired
	private VoucherService voucherService;

	private void execute() throws Exception {
		logger.info("----------------------------STARTING RefreshCouponsJob------------------------");
  		voucherService.refreshAllActiveCoupons();
		logger.info("----------------------------FINISHED RefreshCouponsJob------------------------");
	}

	public static void main(String[] args) {
        Thread.currentThread().setName("RefreshCouponsJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        RefreshCouponsJob refreshCouponsJob = (RefreshCouponsJob) ctx.getBean("refreshCouponsJob");

        try {
        	refreshCouponsJob.execute();
		} catch (Exception e) {
			logger.error("ERROR occured in executing RefreshCouponsJob.");
			logger.error(e.getCause(), e);
		} finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
		}
	}

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        VoucherService voucherDelegate = jobContext.getBean(VoucherService.class);
        voucherDelegate.refreshAllActiveCoupons();
    }
}
