package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.services.PlanValidatorCron;

public class PlanValidatorJob extends FreeChargeJob{

	private static final Logger logger = LoggingFactory
			.getLogger(PlanValidatorJob.class);
	private PlanValidatorCron handler;	
	
	private void execute() throws Exception {
		logger.info("----------------------------STARTING TataDocomoPlanValidatorJob------------------------");
		getHandler().populatePlanStatus();
		logger.info("----------------------------FINISHED TataDocomoPlanValidatorJob------------------------");
	}

	public static void main(String[] args) {
        Thread.currentThread().setName("PlanValidatorJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        PlanValidatorJob planValidatorJob = (PlanValidatorJob) ctx.getBean("planValidatorJob");
       
        try {
			planValidatorJob.execute();
		} catch (Exception e) {
			logger.error("ERROR occured in executing TataDocomoPlanValidatorJob.");
			logger.error(e.getCause(), e);
		} finally {
            try {
                logger.info("Start batch job context destroy.");
                BatchJobUtil.destroyBatchJobContext(ctx);
                logger.info("Completed batch job context destroy.");
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
		}
	}
	public PlanValidatorCron getHandler() {
		return handler;
	}

	public void setHandler(PlanValidatorCron handler) {
		this.handler = handler;
	}


	@Override
	protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
		// TODO Auto-generated method stub
		PlanValidatorCron delegate = (PlanValidatorCron) jobContext.getBean("planValidatorCron");
		delegate.populatePlanStatus();
	}

}
