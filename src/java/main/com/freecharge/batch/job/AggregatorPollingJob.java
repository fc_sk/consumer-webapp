package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.timeout.AggregatorPollingService;

public class AggregatorPollingJob extends FreeChargeJob {
    private static Logger logger = LoggingFactory.getLogger(AggregatorPollingJob.class);
   
    private void execute(AggregatorPollingService aggregatorPollingService) throws Exception {
        logger.info("----------------------------STARTING AggregatorPollingJob------------------------");
        aggregatorPollingService.pollForBalance();
        logger.info("----------------------------FINISHED AggregatorPollingJob------------------------");
    }

    public static void main(String[] args) {
        Thread.currentThread().setName("AggregatorPollingJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        AggregatorPollingJob job = (AggregatorPollingJob) ctx.getBean("aggregatorPollingJob");
        AggregatorPollingService aggregatorPollingService = (AggregatorPollingService) ctx.getBean("aggregatorPollingService");

        try {
           
            job.execute(aggregatorPollingService);

        } catch (Exception e) {
            logger.error("ERROR occured in executing AggregatorPollingJob.");
            logger.error(e.getCause(), e);
        } finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
                ctx.close();
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) {
        AggregatorPollingService aggregatorPollingService = (AggregatorPollingService) jobContext.getBean("aggregatorPollingService");
        aggregatorPollingService.pollForBalance();
    }
}
