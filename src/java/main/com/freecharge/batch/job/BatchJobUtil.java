package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.FCPropertyPlaceholderConfigurer;
import com.freecharge.common.util.FCConstants;

/**
 * Util class to be used for batch execution functionality. Provides init,
 * destroy and other util functions.
 * 
 * @author shirish
 * 
 */
public class BatchJobUtil {
	private static final Logger logger = LoggingFactory
			.getLogger(BatchJobUtil.class);

	/**
	 * Initialize the batch execution. example, attach console appender etc.
	 * 
	 * @return
	 */
	public static final ClassPathXmlApplicationContext initBatchJobContext() {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("batchContext.xml");
		// Loggers are now taken care off by -Dlog4j.configuration= argument
		//initBatchLogger();
		return ctx;
	}

	/**
	 * Does the cleaning up task. Stops daemon threads and closing context.
	 * 
	 * @param context
	 *            the application context, required to get hold of beans.
	 */
	public static final void destroyBatchJobContext(ClassPathXmlApplicationContext context) throws IOException {
		context.close();
	}

	/**
	 * Returns true if the current execution is configured to run in batch mode.
	 * 
	 * @return true if execution is in batch mode.
	 */
	public static final boolean isBatchMode() {
		String batchModeStr = FCPropertyPlaceholderConfigurer
				.getStringValueStatic(FCConstants.BATCH_MODE_PROPERTY);
		logger.debug("batchModeStr " + batchModeStr);
		if ("true".equals(batchModeStr)) {
			return true;
		}
		return false;
	}
	
	private static final boolean shouldWaitForFulfillment() {
        String waitForFulfillment = FCPropertyPlaceholderConfigurer.getStringValueStatic(FCConstants.WAIT_FOR_FULFILLMENT);

        logger.debug("waitForFulfillment " + waitForFulfillment);
        if ("true".equals(waitForFulfillment)) {
            return true;
        }
        return false;
	}

	/**
	 * If execution is in batch mode, serialize the thread execution by joining
	 * on it.
	 * 
	 * @param thread
	 *            the thread which has to be serialized in batch mode.
	 */
	public static void handleThread(Thread thread) {
		if (isBatchMode() && thread != null) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				logger.error("Error joining on thread: " + thread.getName());
				logger.error(e.getMessage());
			}
		}
	}

	/**
	 * Similar to handleThread except that it's exclusive to fulfillment service
	 * threads.
	 * @param thread
	 */
	public static void waitForFulfillmentCompletion(Thread thread) {
        if (shouldWaitForFulfillment() && thread != null) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                logger.error("Error joining on thread: " + thread.getName());
                logger.error(e.getMessage());
            }
        }
	}
}
