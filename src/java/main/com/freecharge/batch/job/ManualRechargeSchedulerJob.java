package com.freecharge.batch.job;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.services.RechargeSchedulerService;

public class ManualRechargeSchedulerJob extends FreeChargeJob {
	private static final Logger logger = LoggingFactory
			.getLogger(ManualRechargeSchedulerJob.class);
	private RechargeSchedulerService handler;

	private void execute() throws Exception {
		logger.info("----------------------------STARTING manualRechargeSchedulerJob------------------------");
		getHandler().rechargeStatusForTMinisSixMinusTwoDays();
		logger.info("----------------------------FINISHED manualRechargeSchedulerJob------------------------");
	}

	public static void main(String[] args) {
		try {
			Thread.currentThread().setName("ManualRechargeSchedulerJob");
			ClassPathXmlApplicationContext ctx = BatchJobUtil
					.initBatchJobContext();
			ManualRechargeSchedulerJob job = (ManualRechargeSchedulerJob) ctx
					.getBean("manualRechargeSchedulerJob");
			
			job.execute();
			BatchJobUtil.destroyBatchJobContext(ctx);
		} catch (Exception e) {
			logger.error("ERROR occured in executing ManualRechargeSchedulerJob.");
			logger.error(e.getCause(), e);
		}
	}

	public RechargeSchedulerService getHandler() {
		return handler;
	}

	public void setHandler(RechargeSchedulerService handler) {
		this.handler = handler;
	}

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        ManualRechargeSchedulerJob job = (ManualRechargeSchedulerJob) jobContext.getBean("manualRechargeSchedulerJob");
        
        job.execute();
        
    }

}
