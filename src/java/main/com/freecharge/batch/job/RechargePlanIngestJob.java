package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.productdata.services.IreffRechargeDataImportService;

public class RechargePlanIngestJob extends FreeChargeJob {

	private static final Logger logger = LoggingFactory
			.getLogger(RechargePlanIngestJob.class);

	public static void main(String[] args) throws IOException {
		Boolean isPopular = null;
		if(args.length != 0) {
			if(args[0].equals("popular"))
				isPopular = true;
			else if(args[0].equals("non-popular")) 
				isPopular=false;
		}
		ClassPathXmlApplicationContext ctx = BatchJobUtil
				.initBatchJobContext();
		try {
			Thread.currentThread().setName("RechargePlanIngestionJob");
			IreffRechargeDataImportService bean = (IreffRechargeDataImportService)
					ctx.getBean("rechargePlanIngestionJob");
			bean.fetchUpdate(isPopular);
		} catch(Exception e) {
			logger.error("ERROR occured when executing IreffRechargeDataImportService. " + e.getCause(),e);
		} finally {
			BatchJobUtil.destroyBatchJobContext(ctx);
		}
	}

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {

        Boolean isPopular = null;
        if(arguments.length != 0) {
            if(arguments[0].equals("popular"))
                isPopular = true;
            else if(arguments[0].equals("non-popular")) 
                isPopular=false;
        }

        IreffRechargeDataImportService bean = (IreffRechargeDataImportService) jobContext.getBean("rechargePlanIngestionJob");
        bean.fetchUpdate(isPopular);
        
    }

}
