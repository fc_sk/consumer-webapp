package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.payment.cron.AutoRefundCron;

public class AutoRefundJob extends FreeChargeJob {
	private static final Logger logger = LoggingFactory
			.getLogger(AutoRefundJob.class);
	private AutoRefundCron handler;

	private void execute(boolean async) throws Exception {
		logger.info("----------------------------STARTING AutoRefundJob------------------------");
		getHandler().doRefund(async);
		logger.info("----------------------------FINISHED AutoRefundJob------------------------");
	}

	public static void main(String[] args) {
        Thread.currentThread().setName("AutoRefundJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        AutoRefundJob autoRefundJob = (AutoRefundJob) ctx.getBean("autoRefundJob");
        boolean async = false;
        if (args.length >= 1) {
            if (args[0].equals("async")) {
                async = true;
            }
        }
        try {
			autoRefundJob.execute(async);
		} catch (Exception e) {
			logger.error("ERROR occured in executing AutoRefundJob.");
			logger.error(e.getCause(), e);
		} finally {
            try {
                logger.info("Start batch job context destroy.");
                BatchJobUtil.destroyBatchJobContext(ctx);
                logger.info("Completed batch job context destroy.");
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
		}
	}

	public AutoRefundCron getHandler() {
		return handler;
	}

	public void setHandler(AutoRefundCron handler) {
		this.handler = handler;
	}

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        AutoRefundCron delegate = (AutoRefundCron) jobContext.getBean("autoRefundCronHandler");
        delegate.doRefund(false);
    }

}
