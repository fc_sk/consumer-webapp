package com.freecharge.batch.job;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.infrastructure.billpay.api.IBillPayService;

public class BillPayJob extends FreeChargeJob {

    private static final Logger logger = LoggingFactory.getLogger(BillPayJob.class);

    private IBillPayService     handler;

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) throws Exception {
        Integer jobId = Integer.parseInt(arguments[0]);
        getHandler().triggerBillPayJob(jobId);
    }

    public static void main(String[] args) {
        logger.info("Starting BillpayJob");
        Thread.currentThread().setName("BillPayJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        BillPayJob job = (BillPayJob) ctx.getBean("billPayJob");
        try {
            job.execute(ctx, args);
        } catch (Exception e) {
            logger.error("ERROR occured in executing BillpayJob.", e);
        } finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.");
                logger.error(e.getCause(), e);
            }
        }
        logger.info("Completed BillpayJob");
    }

    public IBillPayService getHandler() {
        return handler;
    }

    public void setHandler(IBillPayService handler) {
        this.handler = handler;
    }

}
