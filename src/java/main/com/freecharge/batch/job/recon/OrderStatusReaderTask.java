package com.freecharge.batch.job.recon;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.recharge.businessdao.AggregatorInterface.AggregatorName;

public class OrderStatusReaderTask implements Runnable {

    private static final String AFFILIATE_TRANS_ID = "affiliateTransId";

    private static final Logger logger = LoggingFactory.getLogger(AggregatorStatusChangeReportService.class);

    private DataSource                      jdbcSlaveDataSource;
    private BlockingQueue<StatusChangeData> euronetMessageStore;
    private BlockingQueue<StatusChangeData> oxigenMessageStore;
    private final String                    billStatusSql     = "select affiliate_trans_id,gateway_reference_number,bill_payment_gateway,affiliate_trans_id,amount,is_success from bill_payment_status where affiliate_trans_id = :affiliateTransId";
    private final String                    rechargeStatusSql = "select req.request_id,aggr_trans_id,aggr_name,affiliate_trans_id,req.amount,in_resp_code from in_request req left join in_response resp on req.request_id = resp.request_id  where req.affiliate_trans_id = :affiliateTransId order by resp.updated_time desc limit 1";
    private final String                    refundAmountSql   = "select sum(refunded_amount) from payment_refund_transactions where fk_payment_txn_id in (select payment_txn_id from payment_txn where order_id = :affiliateTransId) and status != 'failure'";

    private StatusChangeData orderData;

    public StatusChangeData getOrderData() {
        return orderData;
    }

    public void setOrderData(StatusChangeData orderData) {
        this.orderData = orderData;
    }

    public void setJdbcSlaveDataSource(DataSource jdbcSlaveDataSource) {
        this.jdbcSlaveDataSource = jdbcSlaveDataSource;
    }

    public void setEuronetMessageStore(BlockingQueue<StatusChangeData> euronetMessageStore) {
        this.euronetMessageStore = euronetMessageStore;
    }

    public void setOxigenMessageStore(BlockingQueue<StatusChangeData> oxigenMessageStore) {
        this.oxigenMessageStore = oxigenMessageStore;
    }

    @Override
    public void run() {

        try {
            NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(jdbcSlaveDataSource);
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put(AFFILIATE_TRANS_ID, orderData.getOrderId());
            StatusChangeData rechargeStatusChangeData = getRechargeStatus(jdbcTemplate, paramMap);
            StatusChangeData billpayStatusChangeData = getBillPayStatus(jdbcTemplate, paramMap);
            StatusChangeData finalStatusChangeData = null;
            if (rechargeStatusChangeData != null && billpayStatusChangeData == null) {
                finalStatusChangeData = rechargeStatusChangeData;
            }
            if (rechargeStatusChangeData == null && billpayStatusChangeData != null) {
                finalStatusChangeData = billpayStatusChangeData;
            }
            if (rechargeStatusChangeData != null && billpayStatusChangeData != null) {
                // bill pay has higher precedence as we do postpaid after
                // prepaid incase of product retry
                finalStatusChangeData = billpayStatusChangeData;
            }
            if (rechargeStatusChangeData == null && billpayStatusChangeData == null) {
                return;
            }
            finalStatusChangeData.setOrderCreationDate(orderData.getOrderCreationDate());
            if (StringUtils.containsIgnoreCase(finalStatusChangeData.getAggrName(), AggregatorName.EURONET.getName())) {
                euronetMessageStore.put(finalStatusChangeData);
            }
            if (StringUtils.containsIgnoreCase(finalStatusChangeData.getAggrName(), AggregatorName.OXIGEN.getName())) {
                oxigenMessageStore.put(finalStatusChangeData);
            }
        } catch (Exception ex) {
            logger.error("Exception processing orderId:" + orderData.getOrderId());
        }
    }

    private StatusChangeData getBillPayStatus(NamedParameterJdbcTemplate jdbcTemplate, Map<String, Object> paramMap) {
        StatusChangeData statusChangeData = null;
        try {
            statusChangeData = jdbcTemplate.queryForObject(billStatusSql, paramMap, new StatusChangeDataMapper());
            statusChangeData.setRefundAmount(getRefundAmount(jdbcTemplate, paramMap));
        } catch (EmptyResultDataAccessException ex) {
            //
        } catch (Exception ex) {
            logger.error("Exception getting bill pay status for  orderId:" + paramMap.get(AFFILIATE_TRANS_ID), ex);
        }
        return statusChangeData;
    }

    private StatusChangeData getRechargeStatus(NamedParameterJdbcTemplate jdbcTemplate, Map<String, Object> paramMap) {
        StatusChangeData statusChangeData = null;
        try {
            statusChangeData = jdbcTemplate.queryForObject(rechargeStatusSql, paramMap, new StatusChangeDataMapper());
            statusChangeData.setRefundAmount(getRefundAmount(jdbcTemplate, paramMap));
        } catch (EmptyResultDataAccessException ex) {
            //
        } catch (Exception ex) {
            logger.error("Exception getting recharge status for  orderId:" + paramMap.get(AFFILIATE_TRANS_ID), ex);
        }
        return statusChangeData;
    }

    private String getRefundAmount(NamedParameterJdbcTemplate jdbcTemplate, Map<String, Object> paramMap) {
        String refundAmount = "0";
        try {
            refundAmount = jdbcTemplate.queryForObject(refundAmountSql, paramMap, String.class);
        } catch (EmptyResultDataAccessException ex) {
            //
        } catch (Exception ex) {
            logger.error("Exception getting refund amount for orderId:" + paramMap.get(AFFILIATE_TRANS_ID), ex);
        }
        return refundAmount;
    }

}
