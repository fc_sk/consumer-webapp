package com.freecharge.batch.job.recon;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.freecharge.common.framework.logging.LoggingFactory;

public class OrderStatusWriterTask implements Runnable {

    private static final String TMP_AGRECON_LOCATION = "/tmp/agrecon/";

    private static final Logger logger = LoggingFactory.getLogger(AggregatorStatusChangeReportService.class);

    private BlockingQueue<StatusChangeData> messageStore;
    private String                          fileName;
    private AmazonS3Client                  s3Client;
    private String                          agName;
    private Date                            reportDate;

    public OrderStatusWriterTask(AmazonS3Client s3Client, BlockingQueue<StatusChangeData> messageStore, String fileName,
            String agName, Date reportDate) {
        this.s3Client = s3Client;
        this.messageStore = messageStore;
        this.fileName = fileName;
        this.agName = agName;
        this.reportDate = reportDate;
    }

    @Override
    public void run() {
        FileWriter writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(TMP_AGRECON_LOCATION + this.fileName);
            bufferedWriter = new BufferedWriter(writer);
            StatusChangeData statusChangeData = null;
            while (true) {
                statusChangeData = messageStore.poll(2, TimeUnit.SECONDS);
                if (statusChangeData == null) {
                    break;
                }
                bufferedWriter.write(statusChangeData.toString());
                bufferedWriter.newLine();
            }
            uploadFileToS3();
        } catch (Exception e) {
            logger.error("Exception writing to AG status change report file.", e);
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                logger.error("Exception writing to AG status change report file.");
            }
        }
    }

    private void uploadFileToS3() {
        try {
            logger.info("Uploading a new object to S3 from a file.");
            File file = new File(TMP_AGRECON_LOCATION + fileName);
            String key = "payments/FC/recon/FC_AG_" + agName + "/FC_" + agName + "_STCHG/" + new LocalDate(reportDate).getYear()
                    + "/" + new LocalDate(reportDate).getMonthOfYear() + "/" + new LocalDate(reportDate).getDayOfMonth() + "/";
            s3Client.putObject(new PutObjectRequest("finance.freecharge.sg", key + fileName, file));
            logger.info("Uploading file:" + fileName + " to S3 is complete.");
        } catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, which " + "means your request made it "
                    + "to Amazon S3, but was rejected with an error response" + " for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            logger.error("Caught an AmazonClientException, which " + "means the client encountered "
                    + "an internal error while trying to " + "communicate with S3, "
                    + "such as not being able to access the network.");
            logger.error("Error Message: " + ace.getMessage());
        }

    }
}
