package com.freecharge.batch.job.recon;

public class StatusChangeData {

    private String fcTransId;
    private String agRefNo;
    private String orderId;
    private String amount;
    private String status;
    private String refundAmount;
    private String aggrName;
    private String orderCreationDate;

    public String getAggrName() {
        return aggrName;
    }

    public void setAggrName(String aggrName) {
        this.aggrName = aggrName;
    }

    public String getFcTransId() {
        return fcTransId;
    }

    public void setFcTransId(String fcTransId) {
        this.fcTransId = fcTransId;
    }

    public String getAgRefNo() {
        return agRefNo;
    }

    public void setAgRefNo(String agRefNo) {
        this.agRefNo = agRefNo;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(String refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getOrderCreationDate() {
        return orderCreationDate;
    }

    public void setOrderCreationDate(String orderCreationDate) {
        this.orderCreationDate = orderCreationDate;
    }

    @Override
    public String toString() {
        return fcTransId + "," + agRefNo + "," + orderId + "," + amount + "," + orderCreationDate + ","
                + ("00".equals(status) || "1".equals(status) ? "success" : "fail") + ","
                + (refundAmount == null ? "0" : refundAmount);
    }

}