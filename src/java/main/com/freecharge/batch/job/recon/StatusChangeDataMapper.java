package com.freecharge.batch.job.recon;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class StatusChangeDataMapper implements RowMapper<StatusChangeData> {

    @Override
    public StatusChangeData mapRow(ResultSet resultset, int rowNum) throws SQLException {
        StatusChangeData statusChangeData = new StatusChangeData();
        statusChangeData.setFcTransId(resultset.getString(1));
        statusChangeData.setAgRefNo(resultset.getString(2));
        statusChangeData.setAggrName(resultset.getString(3));
        statusChangeData.setOrderId(resultset.getString(4));
        statusChangeData.setAmount(resultset.getString(5));
        statusChangeData.setStatus(resultset.getString(6));
        return statusChangeData;

    }
}
