package com.freecharge.batch.job.recon;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.freecharge.batch.job.BatchJobUtil;
import com.freecharge.batch.job.framework.FreeChargeJob;
import com.freecharge.common.framework.logging.LoggingFactory;

/**
 * This job reports all orders which are more than 1 day old and whose status
 * has changed on a particular day.
 */
public class AggregatorStatusChangeReportJob extends FreeChargeJob {

    private static final Logger logger = LoggingFactory.getLogger(AggregatorStatusChangeReportJob.class);

    public static void main(String[] args) {
        Thread.currentThread().setName("AggregatorStatusChangeReportJob");
        ClassPathXmlApplicationContext ctx = BatchJobUtil.initBatchJobContext();
        AggregatorStatusChangeReportJob job = (AggregatorStatusChangeReportJob) ctx
                .getBean("aggregatorStatusChangeReportJob");

        try {
            job.execute(ctx, args);
        } catch (Exception e) {
            logger.error("ERROR occured in executing AggregatorStatusChangeReportJob.", e);
        } finally {
            try {
                BatchJobUtil.destroyBatchJobContext(ctx);
            } catch (IOException e) {
                logger.error("ERROR while destroying batch job context.", e);
            }
        }
    }

    @Override
    protected void execute(ClassPathXmlApplicationContext jobContext, String... arguments) {
        AggregatorStatusChangeReportService aggregatorStatusChangeReportService = jobContext
                .getBean(AggregatorStatusChangeReportService.class);
        Date reportDate = new DateTime().minusDays(2).toDate();
        if (arguments.length == 1) {
            try {
                reportDate = new SimpleDateFormat("dd-MM-yyyy").parse(arguments[0]);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        aggregatorStatusChangeReportService.process(reportDate);
    }

}
