package com.freecharge.batch.job.recon;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3Client;
import com.freecharge.app.domain.dao.UserTransactionHistoryDAO;
import com.freecharge.common.framework.logging.LoggingFactory;

@Service
public class AggregatorStatusChangeReportService {

    private static final Logger logger = LoggingFactory.getLogger(AggregatorStatusChangeReportService.class);

    @Autowired
    private UserTransactionHistoryDAO userTransactionHistoryDAO;

    @Autowired
    private DataSource jdbcSlaveDataSource;

    @Autowired
    private AmazonS3Client s3Client;

    public void process(Date reportDate) {
        logger.info("Fetching orders with status change.");
        List<StatusChangeData> orders = userTransactionHistoryDAO.findStatusChangeOrders(reportDate);
        logger.info("Total orders whose status changed on " + reportDate + " : " + orders.size());
        ExecutorService service = Executors.newFixedThreadPool(10);
        OrderStatusReaderTask orderStatusReaderTask = null;
        BlockingQueue<StatusChangeData> euronetMessageStore = new ArrayBlockingQueue<StatusChangeData>(50);
        BlockingQueue<StatusChangeData> oxigenMessageStore = new ArrayBlockingQueue<StatusChangeData>(50);
        for (StatusChangeData statusChangeData : orders) {
            orderStatusReaderTask = new OrderStatusReaderTask();
            orderStatusReaderTask.setJdbcSlaveDataSource(jdbcSlaveDataSource);
            orderStatusReaderTask.setOrderData(statusChangeData);
            orderStatusReaderTask.setEuronetMessageStore(euronetMessageStore);
            orderStatusReaderTask.setOxigenMessageStore(oxigenMessageStore);
            service.submit(orderStatusReaderTask);
        }
        logger.info("Submitted all the orders for getting status.");
        service.shutdown();
        try {
            String date = new SimpleDateFormat("yyyy_MM_dd").format(reportDate).toString();
            Thread euronetWriterTask = new Thread(new OrderStatusWriterTask(s3Client, euronetMessageStore,
                    "AG_STATUS_CHANGE_EURONET_" + date + ".csv", "EURONET", reportDate));
            Thread oxigenWriterTask = new Thread(new OrderStatusWriterTask(s3Client, oxigenMessageStore,
                    "AG_STATUS_CHANGE_OXIGEN_" + date + ".csv", "OXIGEN", reportDate));
            euronetWriterTask.start();
            oxigenWriterTask.start();
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            logger.error("Exception generating AG status change report.", e);
        } finally {
            service.shutdownNow();
            logger.info("Generation of AgStatusChangeReports complete.");
        }

    }

}