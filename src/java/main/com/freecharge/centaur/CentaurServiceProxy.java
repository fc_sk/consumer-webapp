package com.freecharge.centaur;

import java.util.Map;

import com.freecharge.api.coupon.common.api.ICouponService;
import com.freecharge.centaur.service.client.CentaurApiServiceClient;
import com.freecharge.centaur.service.common.ICentaurApiService;
import com.freecharge.centaur.service.common.RequestType;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.freecharge.common.framework.logging.LoggingFactory;
import com.freecharge.common.framework.properties.AppConfigService;


public class CentaurServiceProxy {

	private final Logger logger = Logger.getLogger(getClass());

	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("centaur-clientBeans.xml");
	CentaurApiServiceClient centaurService = (CentaurApiServiceClient) applicationContext.getBean("centaurApiServiceClient");
	
	public Integer getBucketId(RequestType type,Integer experimentId,String requestId){
		return centaurService.getBucketId(type, experimentId, requestId);
	}
	
	public void recordData(Integer experimentId, Integer bucketId, String requestId, Map<Integer, Double> data) {
		centaurService.recordData(experimentId, bucketId, requestId, data);
	}
}
