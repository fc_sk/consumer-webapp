-- liquibase formatted sql

-- changeset kopaul:billPayCommonData

CREATE TABLE IF NOT EXISTS `billpay_operator_master` (
  `operator_master_id` int(11) NOT NULL AUTO_INCREMENT,
  `operator_code` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `fk_category_master_id` int(11) NOT NULL DEFAULT '1',
  `fk_country_master_id` int(11) NOT NULL,
  `fk_product_master_id` int(11) NOT NULL,
  `is_registration_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `img_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`operator_master_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

insert into billpay_operator_master values(200, "TSSPDCL", "TSSPDCL Hyderabad (OLD CPDCL)", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/spdctl.png");
insert into billpay_operator_master values(201, "APDCL", "APDCL - Assam Power Distribution Co Ltd", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/apdcl.png");
insert into billpay_operator_master values(202, "BEST", "BEST Mumbai", 0, 2, 1, 313, 1, "images/circular-operator-logos/bills/bmestu.png");
insert into billpay_operator_master values(203, "BSES Rajdhani", "BSES Rajdhani Delhi NCR", 0, 2, 1, 313, 1, "images/circular-operator-logos/bills/bsesr.png");
insert into billpay_operator_master values(204, "BSES Yamuna", "BSES Yamuna Delhi NCR", 0, 2, 1, 313, 1, "images/circular-operator-logos/bills/bsesy.png");
insert into billpay_operator_master values(205, "CESC", "CESC Ltd", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/cescl.png");
insert into billpay_operator_master values(206, "CSEB", "CSEB - Chhattisgarh State Electricity Board", 1, 2, 1, 313, 0, "images/circular-operator-logos/bills/cseb.png");
insert into billpay_operator_master values(207, "DHBVN", "DHBVN - Dakshin Haryana Bijli Vitran Nigam", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/dhbvn.png");
insert into billpay_operator_master values(208, "JUSCO", "JUSCO - Jamshedpur Utilities & Services", 0, 2, 1, 313, 1, "images/circular-operator-logos/bills/juscl.png");
insert into billpay_operator_master values(209, "MPWZ Indore", "MP Paschim Kshetra Vidyut Vitaran Indore", 1, 2, 1, 313, 0, "images/circular-operator-logos/bills/mppkvvcl.png");
insert into billpay_operator_master values(210, "MSEDC", "MSEDC Ltd Maharashtra", 0, 2, 1, 313, 1, "images/circular-operator-logos/bills/msedcl.png");
insert into billpay_operator_master values(211, "NDPL TATA", "NDPL - Tata Power DDL Delhi", 0, 2, 1, 313, 1, "images/circular-operator-logos/bills/tpddl.png");
insert into billpay_operator_master values(212, "NPCL", "NPCL - Noida Power Co Ltd", 0, 2, 1, 313, 1, "images/circular-operator-logos/bills/npcl.png");
insert into billpay_operator_master values(213, "Odisha Discoms", "Odisha Discoms", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/odishad.png");
insert into billpay_operator_master values(214, "Reliance", "Reliance Energy Mumbai", 0, 2, 1, 313, 1, "images/circular-operator-logos/bills/relem.png");
insert into billpay_operator_master values(215, "JVVNL", "JVVNL - Jaipur Vidyut Vitran Nigam Ltd", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/javvnl.png");
insert into billpay_operator_master values(216, "APSPDCL", "APSPDCL - Southern Power AP", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/apspdcl.png");
insert into billpay_operator_master values(217, "Torrent", "Torrent Power", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/tpa.png");
insert into billpay_operator_master values(218, "TSECL", "TSECL - Tripura State Electricity Corp", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/tsecl.png");
insert into billpay_operator_master values(219, "WBSEDCL", "WBSEDCL - West Bengal Electricity", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/wbsedcl.png");
insert into billpay_operator_master values(220, "JDVVNL", "JDVVNL - Jodhpur Vidyut Vitran Nigam Ltd", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/jovvnl.png");
insert into billpay_operator_master values(221, "BESCOM", "BESCOM Bangalore", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/bescom.png");
insert into billpay_operator_master values(223, "MPEZ Jabalpur", "MP Poorv Kshetra Vidyut Vitaran Jabalpur", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/mppkvcl.png");
insert into billpay_operator_master values(224, "IPCL", "India Power Corporation Limited Bihar", 0, 2, 1, 313, 0, "images/circular-operator-logos/bills/ipcl.png");

insert into billpay_operator_master values(100, "MTNL", "MTNL DELHI", 0, 1, 1, 312, 0, "images/circular-operator-logos/bills/mtnldel.png");

insert into billpay_operator_master values(400, "Gujarat Gas", "GGL - Gujarat Gas Co Ltd", 0, 4, 1, 315, 0, "images/circular-operator-logos/bills/ggcl.png");
insert into billpay_operator_master values(401, "GSPC Gas", "GSPC Gas Co Ltd", 0, 4, 1, 315, 0, "images/circular-operator-logos/bills/gspcgcl.png");
insert into billpay_operator_master values(402, "Mahanagar Gas", "Mahanagar Gas Ltd", 0, 4, 1, 315, 1, "images/circular-operator-logos/bills/mgl.png");

CREATE TABLE IF NOT EXISTS `billpay_oppr_aggr_priority` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_operator_id` int(10) NOT NULL,
  `aggr_priority` varchar(160) NOT NULL,
  `update_time` datetime NOT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fk_operator_id` (`fk_operator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO billpay_oppr_aggr_priority (fk_operator_id, aggr_priority, update_time) VALUES(100,"bdk",NOW()),(200,"bdk",NOW()), (201,"bdk",NOW()), 
(202,"bdk",NOW()),(203,"bdk",NOW()),(204,"bdk",NOW()),(205,"bdk",NOW()),(206,"bdk",NOW()),(207,"bdk",NOW()),
(208,"bdk",NOW()),(209,"bdk",NOW()),(210,"bdk",NOW()),(211,"bdk",NOW()),(212,"bdk",NOW()),(213,"bdk",NOW()),(214,"bdk",NOW()),
(215,"bdk",NOW()),(216,"bdk",NOW()),(217,"bdk",NOW()),(218,"bdk",NOW()),(219,"bdk",NOW()),(220,"bdk",NOW()),(221,"bdk",NOW()),
(223,"bdk",NOW()),(224,"bdk",NOW()),(400,"bdk",NOW()),(401,"bdk",NOW()),(402,"bdk",NOW());


CREATE TABLE IF NOT EXISTS `billpay_circle_master` (
  circle_master_id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  is_active tinyint(1) DEFAULT '1',
  fk_country_master_id int(11) NOT NULL,
  PRIMARY KEY (circle_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `billpay_circle_master` VALUES (1,'Andhra Pradesh',1,1),(2,'Assam',1,1),(3,'Bihar',1,1),(4,'Chattisgarh',0,1),(5,'Chennai',1,1),(6,'Delhi',1,1),(7,'Gujarat',1,1),(8,'Haryana',1,1),
    (9,'Himachal Pradesh',1,1),(10,'Karnataka',1,1),(11,'Kerala',1,1),(12,'Kolkata',1,1),(13,'Madhya Pradesh',1,1),(14,'Maharashtra',1,1),(15,'Mumbai',1,1),(16,'North East',1,1),(17,'Orissa',1,1),
    (18,'Punjab',1,1),(19,'Rajasthan',1,1),(20,'Tamil Nadu',1,1),(21,'Uttar Pradesh',1,1),(23,'West Bengal',1,1),(32,'Uttar Pradesh',0,1),(34,'JK',1,1),
    (35,'UTTARANCHAL',1,1), (501,'Agra',1,1),(502,'Ahmedabad',1,1),(503,'Amritsar',1,1),(504,'Bangalore',0,1),(505,'Belgaum',1,1),(506,'Bhopal',1,1),(507,'Chandigarh',1,1),(508,'Chennai',1,1),
    (509,'Davangere',1,1),(511,'Faridabad',1,1),(512,'Ghaziabad',1,1),(513,'Goa',1,1),(515,'Gulbarga',1,1),(516,'Gurgaon',1,1),(517,'Guwahati',1,1),
    (518,'Hubli',1,1),(519,'Hyderabad',1,1),(520,'Indore',1,1),(521,'Jaipur',1,1),(522,'Jalndhar',1,1),(523,'Jamnagar',1,1),(532,'Kanpur',0,1),(533,'Kolhapur',1,1),(534,'Kolkata',1,1),
    (535,'Lucknow',1,1),(536,'Ludhiana',1,1),(537,'Buldhana',1,1),(538,'Mangalore',1,1),(539,'Kalyan',1,1),(540,'Mysore',1,1),(541,'Nagpur',1,1),(542,'Pune',1,1),(543,'Rajkot',1,1),(544,'Shimoga',1,1),
    (545,'Surat',1,1),(546,'Kozhikode',1,1),(547,'Trivandrum',1,1);
    
CREATE TABLE IF NOT EXISTS `billpay_category_master` (
  `category_master_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_code` varchar(32) NOT NULL,
  `category_name` varchar(128) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (category_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `billpay_category_master` VALUES (1, 'Landline', 'Landline', '0'), (2, 'Electricity', 'Electricity', '1'), (3, 'Insurance', 'Insurance' , '0'), (4, 'GAS', 'Gas', '0');

CREATE TABLE IF NOT EXISTS `request_counter` (
  `trace_id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`trace_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `billpay_aggr_opr_circle_map` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `aggr_name` varchar(128) NOT NULL,
  `fk_operator_id` int(11) DEFAULT NULL,
  `aggr_operator_code` varchar(255) DEFAULT NULL,
  `fk_circle_id` int(20) DEFAULT NULL,
  `aggr_circle_code` varchar(255) DEFAULT NULL,
  `recharge_plan_type` varchar(50) DEFAULT NULL,
  `should_timeout` tinyint(1) DEFAULT '0',
  `time_out_millis` int(20) DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `fk_circle_id` (`fk_circle_id`),
  KEY `fk_operator_id` (`fk_operator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",200,"APCPDCLOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",201,"ASPDCLOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",202,"BESTMUOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",203,"BSESRA",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",204,"BSESYA",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",205,"CESCOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",206,"CSEBOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",207,"DHBVNOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",208,"JUSCOOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",209,"MPPKVCLIOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",210,"MSEBOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",211,"NDPLDE",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",212,"NPCL",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",213,"ORDISCMOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",214,"RELENGOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",215,"RVVNLOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",216,"SPDCLOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",217,"TORRENTOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",218,"TSECLOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",219,"WBSEDCLOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",220,"RVVNLOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",221,"BESCOMOB",-1,1,600000);

insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",223,"MPPKVCLJOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",224,"IPCLOB",-1,1,600000);

insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",100,"MTNLDELOB",-1,1,600000);

insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",400,"GGCLOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",401,"GSPCGASOB",-1,1,600000);
insert into billpay_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,should_timeout,time_out_millis) values ("bdk",402,"MHGLMUOB",-1,1,600000);

CREATE TABLE IF NOT EXISTS `bill_transaction` (
  `transaction_id` bigint(20) NOT NULL,
  `bill_object` TEXT NOT NULL,
  `caller_id` varchar(64) NOT NULL,
  `caller_reference` varchar(64) NOT NULL,
  `last_transaction_ledger_id` varchar(64),
  `transaction_status` varchar(64) NOT NULL,
  `bill_pay_status` varchar(64),
  `created_on` datetime NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`transaction_id`, `created_on`),
  KEY `idx_bill_transaction_last_transaction_ledger_id` (`last_transaction_ledger_id`),
  KEY `idx_bill_transaction_transaction_status` (`transaction_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
PARTITION BY list (month(created_on))(
  PARTITION bt_p1 VALUES IN (1),
  PARTITION bt_p2 VALUES IN (2),
  PARTITION bt_p3 VALUES IN (3),
  PARTITION bt_p4 VALUES IN (4),
  PARTITION bt_p5 VALUES IN (5),
  PARTITION bt_p6 VALUES IN (6),
  PARTITION bt_p7 VALUES IN (7),
  PARTITION bt_p8 VALUES IN (8),
  PARTITION bt_p9 VALUES IN (9),
  PARTITION bt_p10 VALUES IN (10),
  PARTITION bt_p11 VALUES IN (11),
  PARTITION bt_p12 VALUES IN (12)
);

CREATE TABLE IF NOT EXISTS `bill_transaction_ledger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_type` varchar(64) NOT NULL,
  `ledger_id` varchar(64) NOT NULL,
  `fk_bill_transaction_id` varchar(64) NOT NULL,
  `aggregator` varchar(64) NOT NULL,
  `aggr_bill_txn_id` varchar(64) DEFAULT NULL,
  `oper_bill_txn_id` varchar(64) DEFAULT NULL,
  `transaction_status` varchar(64) NOT NULL,
  `aggregator_response_code` varchar(64) NOT NULL,
  `raw_request` text NOT NULL,
  `raw_response` text NOT NULL,
  `created_on` datetime NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `created_on`),
  KEY `idx_bill_transaction_ledger_ledger_id` (`ledger_id`),
  KEY `idx_bill_transaction_ledger_fk_bill_transaction_id` (`fk_bill_transaction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
PARTITION BY list (month(created_on))(
  PARTITION btl_p1 VALUES IN (1),
  PARTITION btl_p2 VALUES IN (2),
  PARTITION btl_p3 VALUES IN (3),
  PARTITION btl_p4 VALUES IN (4),
  PARTITION btl_p5 VALUES IN (5),
  PARTITION btl_p6 VALUES IN (6),
  PARTITION btl_p7 VALUES IN (7),
  PARTITION btl_p8 VALUES IN (8),
  PARTITION btl_p9 VALUES IN (9),
  PARTITION btl_p10 VALUES IN (10),
  PARTITION btl_p11 VALUES IN (11),
  PARTITION btl_p12 VALUES IN (12)
);

CREATE TABLE IF NOT EXISTS `user_biller` (
  `fc_biller_id` int(11) NOT NULL AUTO_INCREMENT,
  `biller_account_id` varchar(64) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(32) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `fk_operator_id` int(11) DEFAULT NULL,
  `fk_circle_id` int(11) DEFAULT NULL,
  `aggr_biller_id` varchar(32) NOT NULL,
  `authenticator` varchar(256),
  `aggregator` varchar(16),
  `is_active` tinyint(1) unsigned,
  `notification_enabled` tinyint(1) unsigned,
  `created_on` datetime NOT NULL,
  CONSTRAINT `idx_user_biller_fc_biller_id` PRIMARY KEY (`fc_biller_id`),
  KEY `idx_user_biller_biller_account_id` (`biller_account_id`),
  KEY `idx_user_biller_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_bill_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_fc_biller_id` int(11) NOT NULL,
  `fk_biller_account_id` varchar(64) DEFAULT NULL,
  `ag_bill_id` varchar(64),
  `bill_date` timestamp NULL,
  `bill_due_date` timestamp NULL,
  `bill_number` varchar(32),
  `bill_amount` decimal(10,2) NOT NULL,
  `bill_status` varchar(16),
  `generation_notified` tinyint(1) unsigned DEFAULT 0,
  `reminder_notified` tinyint(1) unsigned DEFAULT 0,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT `idx_user_bill_details_id` PRIMARY KEY (`id`),
  KEY `idx_user_bill_details_fk_biller_account_id` (`fk_biller_account_id`),
  KEY `idx_user_bill_details_fk_fc_biller_id` (`fk_fc_biller_id`),
  KEY `idx_user_bill_details_bill_due_date` (`bill_due_date`),
  KEY `idx_user_bill_details_bill_date` (`bill_date`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `billpay_validator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_operator_id` int(11) NOT NULL,
  `fk_circle_id` int(11) NOT NULL,
  `biller_type` int(2) NOT NULL,
  `is_partial_pay` tinyint(1) DEFAULT '1',
  `required_validators` int(2) DEFAULT NULL,
  `info1_name` varchar(128) NOT NULL,
  `info1_regex` varchar(4096) NOT NULL,
  `info1_msg` varchar(256) NOT NULL,
  `info2_name` varchar(128) DEFAULT NULL,
  `info2_regex` varchar(4096) DEFAULT NULL,
  `info2_msg` varchar(256) DEFAULT NULL,
  `info3_name` varchar(128) DEFAULT NULL,
  `info3_regex` varchar(4096) DEFAULT NULL,
  `info3_msg` varchar(256) DEFAULT NULL,
  `info4_name` varchar(128) DEFAULT NULL,
  `info4_regex` varchar(4096) DEFAULT NULL,
  `info4_msg` varchar(256) DEFAULT NULL,
  `info5_name` varchar(128) DEFAULT NULL,
  `info5_regex` varchar(4096) DEFAULT NULL,
  `info5_msg` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_billpay_validator_fk_operator_id` (`fk_operator_id`),
  CONSTRAINT chk_biller_type CHECK (biller_type in (0,1,2,3)),
  KEY `idx_billpay_validator_fk_circle_id` (`fk_circle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

insert into billpay_validator(fk_operator_id,fk_circle_id,biller_type,is_partial_pay,required_validators,info1_name,info1_regex,info1_msg,info2_name,info2_regex,info2_msg,info3_name,info3_regex,info3_msg,info4_name,info4_regex,info4_msg,info5_name,info5_regex,info5_msg) values(200,-1,3,0,1,"Unique Service Number","^[0-9]{9}$","Please enter valid fixed 9 digit Unique Service Number (eg 996747046)","Service Number","^[A-Za-z0-9\\s]{1,13}$","Please enter a valid Service Number having length minimum 1 and maximum 13 character long(eg Gaurav123)","Ero code","^[A-Za-z0-9]{1,11}$","Please enter a valid Ero Code having length minimum 1 and maximum 11 character long(eg abcd123)",NULL,NULL,NULL,NULL,NULL,NULL), (201,-1,3,0,1,"Consumer Number","^[0-9]{12,50}$","Please enter a valid Consumer Number (e.g. 009000018003)","Consumer Name","^[A-Za-z0-9\\#\\-\\_\\(\\.\\)\\s\\,\\&\\/\\]\\[]{1,50}$","Please enter a valid Consumer Name (e.g. Jayanta Prasad Sharma)","Sub Division Name","^[A-Za-z0-9\\#\\-\\_\\(\\.\\)\\s\\,\\&\\/\\]\\[]{1,50}$","Please enter a valid Sub Division Name (e.g. Basistha ESD)","Circle Code","^[0-9a-zA-Z]{3}$","Please enter a valid Circle Code (e.g. A02)",NULL,NULL,NULL), (202,-1,3,0,1,"Consumer Number","^[0-9\\,]{10}$","Please enter a valid 10 digit Consumer Number without any special characters except comma (,). For e.g. BEST Consumer Number 626-285-011*2, to be entered with comma i.e 6262850112,)","SCD","^[0-9\\,]{1,10}$","Please enter a valid SCD(e.g. 3)","Contract Account Number","^[0-9]{1,50}$","Please enter a valid Contract Account Number (e.g. 1222431)","Bill month","^[0-9]{1,10}$","Please enter a valid Bill month (Eg; 201506)",NULL,NULL,NULL), (203,-1,3,0,1,"CA Number","^[0-9]{9}$","Please enter a valid 9 digit CA Number (e.g. 102252117)",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), (204,-1,3,0,1,"CA Number","^[0-9]{9}$","Please enter a valid 9 digit valid CA (e.g. 124003293)",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), (205,-1,3,0,1,"Consumer Id","^[-0-9]{11}$","Please enter a valid Customer Id fixed 11 digits (e.g. 48093021005)","Identifier","^[0-9]{20}$","Please enter a valid Identifier fixed 20 digits (e.g. 00000000000000000015).","Payment Info","^[0-9]{40,60}$","Please enter a valid Payment Info fixed 43 digits (e.g. 4809302100547140214093600000000000000000015).","Accounting Month Year","^[0-9]{5}$","Please enter a valid Accounting Month Year fixed 5 digits long (e.g. 01144).","Consumer Name","^[A-Za-z0-9\\#\\-\\_\\.\\,\\&\\/\\]\\[\\s]{5,30}$","Please enter a valid Consumer Number (e.g. T.KUJUR)."), (206,-1,3,1,1,"Business Partner No","^100[0-9]{7}$","Please enter a valid Business Partner No (e.g. 1000319434)","DC Code","^[A-Za-z0-9]{1,50}$","Please enter a valid DC Code (e.g. 456611)",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), (207,-1,3,0,1,"K Number","^[0-9]{10}$","Please enter a valid 10 digit K Number (e.g. 3234234872)","Transaction ID","^[0-9]{1,12}$","Please enter a valid Transaction Id(e.g. 4597332).","Office Code","^[0-9A-Za-z]{1,5}$","Please enter a valid Office Code(e.g. H12).",NULL,NULL,NULL,NULL,NULL,NULL), (208,-1,3,0,2,"Business Partner Number","^[0-9]{6,10}$","Please enter a valid Business Partner Number having length minimum 6 and maximum 10 digit long.(e.g. 10028000)","Customer Name","^[A-Za-z0-9\\#\\-\\_\\(\\.\\)\\s\\,\\&\\/]{1,60}$","Please enter a valid Customer Partner Name (e.g. S Prasad)","Contract Account","^[A-Za-z0-9]{8,12}$","Please enter a valid Contract Account having length minimum 8 and maximum 12 digit long.(e.g. 20060022)","Circle Id","^[A-Za-z0-9]{1,10}$","Please enter a Circle Id having length minimum 1 and maximum 10 digit long. (e.g. 20060022)",NULL,NULL,NULL), (209,-1,3,0,1,"Customer Number","^[-0-9]{10}$","Please enter a valid Customer Number (e.g. 1000192782)","Order Id","^[0-9]{5,20}$","Please enter a valid Order Id(e.g. 10001021).","RAO Code","^[0-9a-zA-Z]{6,7}$","Please enter a valid RAO Code(e.g. 3420000).","DC_code(Location code)","^[0-9a-zA-Z]{4,25}$","Please enter a valid Location Code (e.g. 3424623).","Consumer Name","^[0-9a-zA-Z\\#\\-\\_\\(\\.\\)\\s\\,\\&\\/]{2,100}$","Please enter a valid Consumer Name (e.g. Praveen Tiwari)."), (210,-1,3,0,3,"Consumer Number","^[-0-9]{12}$","Please enter a valid Customer Number (e.g. 100019278202)","Billing Unit","0019~0027~0035~0043~0233~0235~0240~0251~0306~0311~0329~0337~0345~0353~0354~0356~0357~0358~0359~0360~0361~0363~0364~0365~0366~0383~0384~0385~0386~0388~0389~0390~0391~0392~0418~0426~0434~0442~0451~0467~0469~0477~0485~0493~0515~0523~0531~0540~0546~0558~0560~0561~0562~0565~0566~0574~0580~0582~0590~0591~0602~0612~0621~0639~0647~0655~0663~0671~0680~0698~0744~0787~0795~0817~0833~0841~0850~0868~0876~0884~0892~0914~0922~0931~0949~0957~0965~0973~0974~0981~0990~1121~1139~1147~1155~1163~1171~1180~1198~1228~1236~1244~1252~1261~1279~1287~1295~1325~1333~1341~1350~1368~1376~1384~1392~1406~1511~1520~1538~1546~1554~1562~1627~1635~1643~1651~1660~1678~1686~1694~1708~1716~1724~1732~1741~1759~1767~1813~1830~1848~1856~1864~1881~1911~1929~1937~1945~1953~2020~2119~2127~2160~2178~2186~2224~2232~2241~2267~2275~2313~2321~2330~2348~2356~2364~2372~2381~2399~2411~2429~2437~2445~2453~2461~2470~2488~2496~2518~2526~2534~2542~2551~2569~2577~2585~2593~2615~2623~2631~2640~2655~2658~2666~2674~2712~2721~2739~2747~2755~2763~2801~2810~2828~2836~2844~2852~2861~2879~2887~2895~2917~2925~2933~2941~2984~2992~3000~3041~3077~3085~3093~3107~3115~3123~3131~3140~3158~3166~3174~3191~3204~3222~3311~3312~3399~3554~3556~3557~3651~3652~3653~3654~3656~3657~3808~3905~3956~4016~4017~4018~4019~4021~4065~4066~4067~4068~4069~4073~4086~4087~4088~4089~4127~4129~4130~4131~4132~4133~4134~4135~4136~4137~4138~4139~4140~4141~4142~4143~4144~4145~4146~4147~4148~4151~4158~4159~4160~4161~4162~4163~4164~4165~4166~4167~4168~4169~4170~4171~4172~4173~4174~4175~4176~4177~4178~4179~4180~4181~4182~4183~4184~4185~4186~4187~4188~4189~4190~4191~4192~4193~4200~4204~4236~4237~4250~4251~4252~4253~4261~4265~4275~4295~4296~4297~4310~4316~4327~4328~4329~4330~4331~4332~4334~4335~4336~4337~4338~4339~4340~4341~4342~4343~4344~4345~4346~4359~4367~4371~4372~4373~4375~4377~4379~4380~4383~4391~4394~4395~4396~4405~4445~4448~4456~4464~4532~4540~4541~4542~4551~4570~4572~4577~4578~4579~4591~4592~4593~4594~4595~4596~4597~4598~4599~4600~4601~4602~4603~4604~4605~4606~4607~4608~4609~4610~4611~4612~4613~4614~4615~4635~4636~4637~4640~4641~4642~4643~4645~4646~4652~4655~4668~4669~4670~4671~4672~4673~4674~4675~4676~4677~4678~4679~4680~4681~4682~4683~4684~4685~4686~4687~4688~4689~4690~4691~4692~4696~4697~4698~4699~4700~4701~4702~4703~4704~4705~4706~4707~4708~4709~4710~4711~4713~4714~4715~4716~4717~4718~4719~4720~4721~4722~4723~4724~4726~4727~4728~4729~4730~4731~4732~4733~4734~4738~4739~4740~4741~4742~4743~4744~4745~4746~4747~4748~4749~4750~4751~4752~4753~4754~4755~4756~4757~4758~4759~4760~4761~4762~4763~4764~4765~4766~4767~4768~4769~4770~4783~4788~4789~4790~4791~4792~4793~4794~4795~4796~4797~4798~4799~4800~4801~4802~4803~4804~4805~4806~4807~4808~4809~4810~4811~4812~4813~4814~4815~4816~4817~4818~4819~4820~4821~4822~4823~4824~4825~4826~4827~4828~4829~4830~4831~4832~4833~4834~4835~4836~4837~4838~4839~4840~4841~4842~4843~4844~4845~4846~4847~4848~4849~4850~4851~4852~4853~4854~4855~4856~4857~4858~4859~4860~5118~5126~5134~5142~5151~5169~5177~5185~5193~5258~5266~5274~5282~5291~5401~5410~5428~5436~5444~5452~5461~5479~5487~5495~5517~5525~5533~5541~5550~5568~5576~5584~5606~5614~5622~5631~5649~5657~5665~5681~5690~5711~5720~5738~5746~5754~5762~5771~5789~5797~5801~5819~5827~5835~5843~5851~5860~5878~5886~5894~5916~5924~5932~5941~5959~5967~5975~5983~5991~6131~6149~6157~6165~6173~6181~6190~6211~6220~6238~6246~6254~6262~6271~6289~6319~6327~6335~6343~6351~6360~6416~6424~6441~6459~6718~6726~6734~6742~6912~7111~7129~7137~7145~7153~7218~7226~7234~7242~7323~7412~7421~7439~7943~7951~7960~7978~7986~8125~8133~8141~8150~8168~8176~8184~8192~9121~9148~9517~9525~9533~4861~4862~4863~4864~4865~4866~4869~4868~4867~4870~4871~4872~4873~4874","Please select a valid billing unit.","Processing Cycle","00~01~02~03~04~05~06~07~08","Please select a valid processing cycle.",NULL,NULL,NULL,NULL,NULL,NULL), (211,-1,3,0,1,"Contract Account Number","^6[0-9]{10,11}$|^06[0-9]{9,10}$","Please enter a valid minimum 11 and maximum 12 digit Contract Account Number starting with 6 or 06 (e.g. 61100131066)",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), (212,-1,3,0,1,"Consumer Number","^[0-9]{10}$","Please enter a valid 10 digit Consumer Number (e.g. 2000003234)","Order Number","^[0-9A-Za-z]{1,50}$","Please enter a valid minimum 1 and maximum 50 digit Order Number (e.g. 2000003234)","Contract Account Number","^[0-9A-Za-z]{1,50}$","Please enter a valid minimum 1 and maximum 50 digit Contract Account Number (e.g. 2000003234)","Address","^[A-Za-z0-9\\#\\-\\_\\(\\.\\)\\s\\,\\&\\/\\]\\[]{1,100}$","Please enter a valid Address having maximum 100 character length (e.g. Maharashtra, Mumbai)",NULL,NULL,NULL), (213,-1,3,0,1,"Consumer Number","^[0-9]{12}$","Please enter a valid Consumer Number (e.g. 675132846534)","Company code","^[0-9]{1,2}$","Please enter a valid Company Code (e.g. 1)","Bill Month","^[0-9]{6}$","Please enter a valid Bill  Month (eg - 201308 )",NULL,NULL,NULL,NULL,NULL,NULL), (214,-1,3,0,2,"Consumer number","^[0-9]{9}$","Please enter fixed 9 digit long consumer Number (eg 212345678)","Cycle Number","01~02~03~04~05~06~07~08~09~10~11~12~13~14~15~16~17~18~19~20~21~22~23~24~25~26~27~28~29~30~31~32~33","Please enter a valid Cycle Number having length minimum 1 and maximum 2 digit long(eg 23)",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), (215,-1,3,0,1,"K Number","^(2|3){1}[0-9A-Za-z]{11}$","Please enter a valid 12 digit K Number starting with 2 or 3(e.g. 210152017378)","Discom Code","^[0-9A-Za-z]{5,10}$","Please enter a valid Discom Name (e.g. AVVNL)","Binder Number","^[0-9]{1,4}$","Please enter a valid 4 digit Binder Number (e.g. 2112)","Account No","^[0-9A-Za-z]{4}$","Please enter a valid 4 digit Account Number (e.g. 0195)","Office Code","^[0-9]{1,7}$","Please enter a valid  Office Code (e.g. 1101520)"), (216,-1,3,0,1,"Service Number","^[0-9]{9,13}$","Please enter valid Service Numer having length minimum 9 and maximum 13 digit long (eg 1236547891)","Service Type","^[A-Za-z0-9]{2}$","Please enter a valid Service having length fixed 2 character long(eg A1)","Circle Name","^[0-9A-Za-z]{5,12}$","Please enter a valid Circle Name having length minimum 5 and maximum 12 Character long(eg Mumbai)","ERO CODE","^[0-9]{3}$","Please enter a valid ERO Code having fixed 3 digit length(eg 123)",NULL,NULL,NULL), (217,-1,3,0,2,"Service Number","^[0-9]{1,15}$","Please enter a valid numeric Service Number having length minimum 1 and maximum 15 digit(e.g. 1000192782)","City","Ahmedabad~Agra~Bhiwandi~Surat","Please enter valid City having length minimum 1 and maximum 30 character long(e.g. AHMEDABAD).","Customer Name","^[A-Za-z0-9\\/\\s\\.\\{\\}]{1,50}$","Please enter valid a valid Customer name having minimum 1 and maximum 50 character length (e.g. Amarendra Rao)",NULL,NULL,NULL,NULL,NULL,NULL), (218,-1,3,0,1,"Unique Ref_id","^[A-Za-z0-9\\:\\-]{1,40}$","Please enter valid Unique Ref_id having length minimum 1 and maximum 40 chracter long (eg SUB:654789-123)","Consumer Id","^[0-9]{1,12}$","Please enter a valid Consumer Id having minimum 1 and maximum 1 digit long(eg 9876543211)","Invoice ID","^[A-Za-z0-9]{1,20}$","Please enter a valid Invoice ID having length minimum 1 and maximum 20 character long(eg abcd123)","Consumer Name","^[A-Za-z0-9\\s\\_\\.\\,\\-]{1,42}$","Please enter a valid Consumer Name having length minimum 1 and maximum 40 characters long(eg Gaurav Singh)",NULL,NULL,NULL), (219,-1,3,0,2,"Installation Number","^[0-9]{1,10}$","Please enter a valid Installation Number (e.g. 1234567890)","Consumer ID","^[0-9]{1,12}$","Please enter a valid Consumer ID (e.g. 123456789012)","Mobile Number","^[0-9]{10}$","Please enter a valid Mobile Number (e.g. 9831198311)","Payment ID","^[0-9A-Za-z]{3,100}$","Please enter a valid Payment ID (e.g. AAA123456)","Customer Name","^[A-Za-z0-9\\#\\-\\_\\(\\.\\)\\s\\,\\&\\\\/\\]\\[\\;\\:\\@\\~]{3,160}$","Please enter a valid Customer Name (eg - Anuj Tripathi )"), (220,-1,3,0,1,"K Number","^(2|3){1}[0-9A-Za-z]{11}$","Please enter a valid 12 digit K Number starting with 2 or 3(e.g. 210152017378)","Discom Code","^[0-9A-Za-z]{5,10}$","Please enter a valid Discom Name (e.g. AVVNL)","Binder Number","^[0-9]{1,4}$","Please enter a valid 4 digit Binder Number (e.g. 2112)","Account No","^[0-9A-Za-z]{4}$","Please enter a valid 4 digit Account Number (e.g. 0195)","Office Code","^[0-9]{1,7}$","Please enter a valid  Office Code (e.g. 1101520)"), (400,-1,3,1,1,"CA Number","^[0-9]{9}$","Please enter a valid digit Customer Number(e.g. 111033289)",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), (401,-1,3,0,1,"Customer ID","^[-0-9]{1,15}$","Please enter a valid Customer ID (e.g. 500000106768)","City Code","^[0-9a-zA-Z]{4,10}$","Please enter a valid City Code(e.g. CA01).",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL), (402,-1,3,0,2,"CA Number","^21[0-9]{10}$","Please enter a valid 12 digit C.A. Number start with 21 (e.g. 210000253340) without spaces or any special characters","Bill Group Number","^[A-Za-z0-9\\#\\-\\_\\(\\.\\)\\s\\,\\&\\/\\]\\[]{1,8}$","Please enter a valid Bill Group Number (e.g. TH043022) as it appears on your bill",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL); 

insert into billpay_validator(fk_operator_id,fk_circle_id,biller_type,is_partial_pay,required_validators,info1_name,info1_regex,info1_msg,info2_name,info2_regex,info2_msg,info3_name,info3_regex,info3_msg,info4_name,info4_regex,info4_msg,info5_name,info5_regex,info5_msg) values(221,-1,3,0,1,"Account ID","^[0-9]{10}$","Please enter a valid Account ID (e.g. 6784513465)",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

insert into billpay_validator(fk_operator_id,fk_circle_id,biller_type,is_partial_pay,required_validators,info1_name,info1_regex,info1_msg,info2_name,info2_regex,info2_msg,info3_name,info3_regex,info3_msg,info4_name,info4_regex,info4_msg,info5_name,info5_regex,info5_msg) values("100", "-1", "3", "0", "2", "Telephone Number", "^2(?!(0|451|450|025|026|027|028|029))[0-9]{7}$", "Please enter a valid 8 digit Telephone Number start with 2 (eg 24183797)", "Customer A/C Number", "^[0-9]{10}$", "Please enter a valid 10 digit Customer A/C Number (eg 2071116390)", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"), ("223", "-1", "3", "0", "1", "Customer Number", "^[-0-9]{10}$", "Please enter a valid Customer Number (e.g. 1000192782)", "Order Id", "^[0-9]{5,20}$", "Please enter a valid Order Id(e.g. 10001021).", "RAO Code", "^[0-9a-zA-Z]{6,7}$", "Please enter a valid RAO Code(e.g. 3420000).", "DC_code(Location code)", "^[0-9a-zA-Z]{4,25}$", "Please enter a valid Location Code (e.g. 3424623).", "Consumer Name", "^[0-9a-zA-Z\\#\\-\\_\\(\\.\\)\\s\\,\\&\\/]{2,100}$", "Please enter a valid Consumer Name (e.g. Praveen Tiwari)."), ("224", "-1", "3", "0", "1", "Consumer Number", "^[A-Za-z0-9]{5,13}$", "Please enter a valid Consumer Number (e.g. 100000010002) ", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA");

CREATE TABLE IF NOT EXISTS `billpay_errorcode_map` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aggr_name` varchar(64) NOT NULL,
  `aggr_resp_code` varchar(64) NOT NULL,
  `fc_resp_code` varchar(64) NOT NULL,
  `is_retryable` tinyint(1) NOT NULL DEFAULT '1',
  `is_display_enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_billpay_errorcode_map_aggr_resp_code` (`aggr_resp_code`),
  KEY `idx_billpay_errorcode_map_aggr_name` (`aggr_name`),
  KEY `idx_billpay_errorcode_map_fc_resp_code` (`fc_resp_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO billpay_errorcode_map(aggr_name,aggr_resp_code,fc_resp_code) values
("bdk", "ERR_BILLNUMBER", "60000"), ("bdk", "PYMT_ERROR", "60008"), 
("bdk", "ERRBV0009", "60007"), ("bdk", "ERRBV0027", "60000"), 
("bdk", "INV_MSG_001", "60000"), ("bdk", "INV_MSG_002", "60000"), 
("bdk", "INV_MSG_003", "60000"), ("bdk", "INV_MSG_004", "60000"), 
("bdk", "INV_MSG_005", "60000"), ("bdk", "INV_MSG_006", "60000"), 
("bdk", "INV_MSG_007", "60000"), ("bdk", "INV_MSG_008", "60000"), 
("bdk", "INV_MSG_009", "60000"), ("bdk", "INV_MSG_010", "60000"), 
("bdk", "INV_MSG_011", "60000"), ("bdk", "INV_MSG_012", "60000"), 
("bdk", "INV_MSG_013", "60000"), ("bdk", "INV_MSG_021", "60000"), 
("bdk", "INV_MSG_022", "60000"), ("bdk", "INV_MSG_023", "60000"), 
("bdk", "INV_MSG_031", "60002"), ("bdk", "INV_MSG_041", "60000"), 
("bdk", "INV_MSG_051", "60000"), ("bdk", "INV_MSG_061", "60000"), 
("bdk", "INV_MSG_063", "60003"), ("bdk", "INV_MSG_064", "60002"), 
("bdk", "INV_MSG_065", "60004"), ("bdk", "INV_MSG_067", "60005"), 
("bdk", "INV_MSG_069", "60006"), ("bdk", "INV_MSG_070", "60002"), 
("bdk", "INV_MSG_071", "60002"), ("bdk", "INV_MSG_072", "60002"), 
("bdk", "INV_MSG_073", "60004"), ("bdk", "INV_MSG_074", "60004"), 
("bdk", "INV_MSG_075", "60007"), ("bdk", "INV_MSG_076", "60004"), 
("bdk", "INV_MSG_077", "60007"), ("bdk", "INV_MSG_078", "60000"), 
("bdk", "INV_MSG_079", "60004"), ("bdk", "INV_MSG_081", "60000"), 
("bdk", "INV_MSG_082", "60000"), ("bdk", "INV_MSG_083", "60000"), 
("bdk", "INV_MSG_084", "60000"), ("bdk", "INV_MSG_085", "60000"), 
("bdk", "INV_MSG_086", "60000"), ("bdk", "INV_MSG_087", "60000"), 
("bdk", "INV_MSG_091", "60000"), ("bdk", "INV_MSG_093", "60000"), 
("bdk", "INV_MSG_094", "60000"), ("bdk", "INV_MSG_095", "60004"), 
("bdk", "INV_MSG_096", "60004"), ("bdk", "INV_MSG_097", "60000"), 
("bdk", "INV_MSG_098", "60000"), ("bdk", "INV_MSG_099", "60000"), 
("bdk", "INV_MSG_100", "60000"), ("bdk", "INV_MSG_101", "60000"), 
("bdk", "INV_MSG_102", "60000"), ("bdk", "INV_MSG_103", "60000"), 
("bdk", "INV_MSG_107", "60000"), ("bdk", "INV_MSG_110", "60000"), 
("bdk", "INV_MSG_111", "60000"), ("bdk", "INV_MSG_112", "60000"), 
("bdk", "INV_MSG_113", "60000"), ("bdk", "INV_MSG_114", "60000"), 
("bdk", "INV_MSG_115", "60000"), ("bdk", "INV_MSG_116", "60000"), 
("bdk", "INV_MSG_117", "60000"), ("bdk", "INV_PYMT_STATUS", "60008");

INSERT INTO billpay_errorcode_map(aggr_name,aggr_resp_code,fc_resp_code,is_retryable) values("bdk", "0", "00", "0");

CREATE TABLE IF NOT EXISTS `billpay_fc_error_msg` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fc_resp_code` varchar(64) NOT NULL,
  `fc_error_msg` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_billpay_fc_error_msg_fc_resp_code` (`fc_resp_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO billpay_fc_error_msg(fc_resp_code,fc_error_msg) values
("00", ""),
("60000", "Oops! Something went wrong. Please try again later."), 
("60002", "Oops! No bills found. Please enter valid details. "), 
("60003", "Oops! We are unable to process your request. Please try again after sometime."), 
("60004", "Due date has passed or no bill is pending. Please try when next bill generates."), 
("60005", "Oops! We are unable to fetch bill information for this service provider."), 
("60006", "Oops! Amount more than Rs.4000 is not allowed for NDPL."), 
("60007", "Oops! Please try again with exact bill amount"), 
("60008", "Oops! Payment failed with biller. Please try again later.");

-- rollback DELETE FROM billpay_aggr_opr_circle_map, billpay_circle_master, billpay_operator_master

