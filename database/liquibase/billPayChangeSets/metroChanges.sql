-- liquibase formatted sql

-- changeset kopaul:metroChanges

insert into billpay_operator_master values(501, "Mumbai Metro", "Mumbai Metro", 1, 0, 1, 316, 0, "images/circular-operator-logos/bills/mmrda.png");

INSERT INTO billpay_oppr_aggr_priority (fk_operator_id, aggr_priority, update_time) VALUES(501,"mmrda",NOW());
-- rollback 