--liquibase formatted sql

--changeset abhi:bootStrapDb

DROP TABLE IF EXISTS admin_users;
CREATE TABLE admin_users (
  admin_user_id int(5) NOT NULL,
  admin_user_name varchar(50) NOT NULL,
  admin_user_password varchar(50) NOT NULL,
  admin_user_active_status tinyint(4) DEFAULT NULL,
  PRIMARY KEY (admin_user_id,admin_user_name)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS affiliate_profile;
CREATE TABLE affiliate_profile (
  id int(11) NOT NULL AUTO_INCREMENT,
  affliate_unique_id varchar(36) NOT NULL,
  company varchar(255) NOT NULL,
  contact_name varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  phone varchar(24) NOT NULL,
  ext varchar(8) DEFAULT NULL,
  fax varchar(24) NOT NULL,
  address1 varchar(255) NOT NULL,
  address2 varchar(255) DEFAULT NULL,
  city varchar(127) NOT NULL,
  state varchar(127) NOT NULL,
  postal_code varchar(16) NOT NULL,
  site_name varchar(127) NOT NULL,
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS audit_trail_filter;
CREATE TABLE audit_trail_filter (
  audit_trail_master_id int(11) NOT NULL AUTO_INCREMENT,
  url varchar(250) DEFAULT NULL,
  method_name varchar(250) DEFAULT NULL,
  is_active tinyint(1) DEFAULT NULL,
  calling_order tinyint(2) DEFAULT NULL,
  PRIMARY KEY (audit_trail_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS banks_grp_master;
CREATE TABLE banks_grp_master (
  banks_grp_master_id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  code_ui varchar(24) DEFAULT NULL,
  bank_code varchar(24) DEFAULT NULL,
  is_active tinyint(1) DEFAULT NULL,
  payment_option int(3) DEFAULT NULL,
  display_name varchar(255) DEFAULT NULL,
  PRIMARY KEY (banks_grp_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS binbased_offer;
CREATE TABLE binbased_offer (
  id int(11) NOT NULL AUTO_INCREMENT,
  bank varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  image_url varchar(255) DEFAULT NULL,
  bin varchar(255) DEFAULT NULL,
  offer_key varchar(255) DEFAULT NULL,
  offer_type varchar(255) DEFAULT NULL,
  discount_type varchar(255) DEFAULT NULL,
  discount decimal(10,2) DEFAULT NULL,
  min_recharge_amount decimal(10,2) DEFAULT NULL,
  coupon_factor int(10) NOT NULL DEFAULT '1',
  description varchar(2000) DEFAULT NULL,
  is_active tinyint(1) NOT NULL DEFAULT '1',
  valid_from datetime NOT NULL,
  valid_upto datetime NOT NULL,
  created_at datetime NOT NULL,
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS card_meta;
CREATE TABLE card_meta (
  card_meta_id int(11) NOT NULL AUTO_INCREMENT,
  card_ref varchar(255) NOT NULL,
  card_type varchar(10) DEFAULT NULL,
  card_brand varchar(10) DEFAULT NULL,
  card_issuer varchar(128) DEFAULT NULL,
  card_bin char(6) DEFAULT NULL,
  nickname_empty tinyint(1) DEFAULT NULL,
  created_ts datetime NOT NULL,
  PRIMARY KEY (card_meta_id),
  UNIQUE KEY card_ref (card_ref)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS card_storage;
CREATE TABLE card_storage (
  card_storage_id int(11) NOT NULL AUTO_INCREMENT,
  order_id varchar(20) NOT NULL,
  card_type varchar(16) DEFAULT NULL,
  card_brand varchar(16) DEFAULT NULL,
  card_issuer varchar(128) DEFAULT NULL,
  access_ts datetime NOT NULL,
  PRIMARY KEY (card_storage_id),
  UNIQUE KEY order_id (order_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cart;
CREATE TABLE cart (
  cart_id int(11) NOT NULL AUTO_INCREMENT,
  fk_order_id int(11) DEFAULT NULL,
  lookup_id varchar(64) DEFAULT NULL,
  fk_user_id int(11) DEFAULT NULL,
  is_open tinyint(11) DEFAULT NULL,
  created_on datetime DEFAULT NULL,
  expires_on datetime DEFAULT NULL,
  final_status int(2) DEFAULT NULL COMMENT 'abandon, deleted, bought',
  PRIMARY KEY (cart_id),
  KEY idx_cart_lookup_id (lookup_id),
  KEY idx_cart_order_id (fk_order_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS cart_items;
CREATE TABLE cart_items (
  cart_items_id int(11) NOT NULL AUTO_INCREMENT,
  fk_cart_id int(11) DEFAULT NULL,
  fk_item_id int(11) DEFAULT NULL,
  entity_id varchar(50) DEFAULT NULL,
  fl_product_master_id int(11) DEFAULT NULL,
  display_label varchar(255) DEFAULT NULL,
  price double(10,2) DEFAULT NULL,
  added_on datetime DEFAULT NULL,
  deleted_on datetime DEFAULT NULL,
  is_deleted tinyint(1) DEFAULT NULL,
  quantity int(11) DEFAULT NULL,
  PRIMARY KEY (cart_items_id),
  KEY idx_crtitems_cart_id (fk_cart_id),
  KEY idx_crtitems_added_on (added_on),
  KEY idx_crtitems_fk_item_id (fk_item_id),
  KEY idx_crtitems_product_id (fl_product_master_id),
  KEY idx_maf_cart_items (entity_id,fk_cart_id,fk_item_id),
  KEY idx_maf_cart_items_combo (is_deleted,entity_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS cash_back_campaign;
CREATE TABLE cash_back_campaign (
  id int(11) NOT NULL AUTO_INCREMENT,
  fk_user_id int(11) DEFAULT NULL,
  state enum('opted', 'applied') NOT NULL,
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (id),
  UNIQUE KEY fk_user_id (fk_user_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS circle_master;
CREATE TABLE circle_master (
  circle_master_id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  is_active tinyint(1) DEFAULT '1',
  fk_country_master_id int(11) NOT NULL,
  PRIMARY KEY (circle_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS circle_operator_mapping;
CREATE TABLE circle_operator_mapping (
  circle_operator_mapping_id int(11) NOT NULL AUTO_INCREMENT,
  fk_operator_master_id int(11) NOT NULL,
  fk_circle_master_id int(11) NOT NULL,
  PRIMARY KEY (circle_operator_mapping_id),
  KEY idx_com_circle_id (fk_circle_master_id),
  KEY idx_com_operator_id (fk_operator_master_id),
  KEY idx_com_circle_operator (fk_operator_master_id,fk_circle_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS citi_bank_offer;
CREATE TABLE citi_bank_offer (
  citi_bank_offer_id int(10) NOT NULL AUTO_INCREMENT,
  title varchar(3) DEFAULT NULL,
  first_name varchar(50) DEFAULT NULL,
  last_name varchar(50) DEFAULT NULL,
  city varchar(9) DEFAULT NULL,
  age varchar(12) DEFAULT NULL,
  mobile varchar(10) DEFAULT NULL,
  profession varchar(13) DEFAULT NULL,
  annual_salary varchar(14) DEFAULT NULL,
  fk_user_id int(10) DEFAULT NULL,
  look_up_id varchar(36) DEFAULT NULL,
  is_considered tinyint(1) DEFAULT NULL,
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  updated_date timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (citi_bank_offer_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS city_master;
CREATE TABLE city_master (
  city_master_id int(11) NOT NULL AUTO_INCREMENT,
  city_name varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  fk_state_master_id int(11) NOT NULL,
  fk_country_master_id int(11) NOT NULL,
  fk_zone_master_id int(11) NOT NULL,
  PRIMARY KEY (city_master_id),
  KEY index_cityname (city_name)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS country_master;
CREATE TABLE country_master (
  country_master_id int(11) NOT NULL AUTO_INCREMENT,
  country_code varchar(5) DEFAULT NULL,
  print_name varchar(25) DEFAULT NULL,
  country_name varchar(100) NOT NULL DEFAULT '',
  iso3 varchar(5) DEFAULT NULL,
  numcode varchar(5) DEFAULT NULL,
  is_active tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (country_master_id),
  KEY index_country_code (country_code),
  KEY index_print_name (print_name),
  KEY index_country_name (country_name)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS coupon_code;
CREATE TABLE coupon_code (
  coupon_code_id int(8) NOT NULL AUTO_INCREMENT,
  fk_coupon_inventory_id int(11) NOT NULL,
  coupon_code varchar(150) NOT NULL,
  created_at datetime DEFAULT NULL,
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  is_blocked tinyint(1) NOT NULL DEFAULT '0',
  block_time datetime DEFAULT NULL,
  order_id varchar(20) DEFAULT NULL,
  PRIMARY KEY (coupon_code_id),
  KEY idx_coupon_inventory_id (fk_coupon_inventory_id),
  KEY idx_order_id (order_id),
  KEY idx_fk_coupon_maf (fk_coupon_inventory_id,coupon_code),
  KEY idx_is_blocked_maf (fk_coupon_inventory_id,is_blocked)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS coupon_inventory;
CREATE TABLE coupon_inventory (
  coupon_inventory_id int(11) NOT NULL AUTO_INCREMENT,
  fk_coupon_store_id mediumint(5) NOT NULL,
  coupon_count int(255) NOT NULL,
  threshold_count int(11) NOT NULL DEFAULT '0',
  sent_coupon_count int(11) NOT NULL DEFAULT '0',
  daily_complimentary_limit int(11) NOT NULL DEFAULT '0',
  daily_complimentary_sent int(11) NOT NULL DEFAULT '0',
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (coupon_inventory_id),
  KEY idx_coupon_store_id (fk_coupon_store_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS coupon_merchant;
CREATE TABLE coupon_merchant (
  id int(11) NOT NULL AUTO_INCREMENT,
  merchant_title varchar(255) NOT NULL,
  merchant_path varchar(255) NOT NULL,
  merchant_desc text NOT NULL,
  merchant_url varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  added_on datetime NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS coupon_store;
CREATE TABLE coupon_store (
  coupon_store_id int(11) NOT NULL AUTO_INCREMENT,
  fk_merchant_id int(11) DEFAULT NULL,
  fk_voucher_cat_id int(11) DEFAULT NULL,
  campaign_name varchar(255) DEFAULT NULL,
  coupon_value int(11) NOT NULL,
  face_value int(11) NOT NULL,
  min_recharge_amt int(11) DEFAULT NULL,
  min_redem_amt int(11) DEFAULT NULL,
  zone_master_id int(11) DEFAULT NULL,
  city_id_list varchar(255) DEFAULT NULL,
  terms_conditions text,
  date_added timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  date_updated timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  coupon_type char(3) DEFAULT 'P',
  coupon_nature varchar(45) DEFAULT NULL,
  coupon_image_path varchar(255) NOT NULL DEFAULT 'NA',
  coupon_position int(11) NOT NULL DEFAULT '0',
  is_active tinyint(4) NOT NULL DEFAULT '1',
  is_complimentary tinyint(1) NOT NULL DEFAULT '0',
  validity_date timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  is_unique tinyint(1) NOT NULL DEFAULT '0',
  coupon_hyperlink varchar(1000) DEFAULT NULL,
  coupon_short_desc varchar(1024) DEFAULT NULL,
  fk_crosssell_id int(11) DEFAULT NULL,
  PRIMARY KEY (coupon_store_id),
  KEY idx_coupon_merchant_id (fk_merchant_id),
  KEY idx_voucher_cat_id (fk_voucher_cat_id),
  KEY idx_campaign_name (campaign_name),
  KEY idx_coupon_type (coupon_type),
  KEY isx_crosssell_id (fk_crosssell_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS courier_master;
CREATE TABLE courier_master (
  courier_master_id int(8) NOT NULL AUTO_INCREMENT,
  courier_code varchar(25) NOT NULL,
  courier_name varchar(50) NOT NULL,
  courier_url varchar(50) NOT NULL,
  courier_contact_no varchar(50) NOT NULL,
  PRIMARY KEY (courier_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS crosssell;
CREATE TABLE crosssell (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  `xml` text,
  label varchar(250) DEFAULT NULL,
  category varchar(250) DEFAULT NULL,
  created_date timestamp NULL DEFAULT NULL,
  created_by varchar(250) DEFAULT NULL,
  modified_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by varchar(250) DEFAULT NULL,
  effective_from date NOT NULL,
  valid_upto date NOT NULL,
  count_uses int(20) NOT NULL DEFAULT '0',
  price int(20) DEFAULT NULL,
  is_active tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS diwali_codes;
CREATE TABLE diwali_codes (
  diwali_code_id int(10) NOT NULL AUTO_INCREMENT,
  phone_no varchar(10) DEFAULT NULL,
  fk_user_id int(10) DEFAULT NULL,
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (diwali_code_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS ecoupon_code;
CREATE TABLE ecoupon_code (
  id int(8) NOT NULL AUTO_INCREMENT,
  coupon_id mediumint(5) NOT NULL,
  coupon_code varchar(150) NOT NULL,
  date_added timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  date_sent timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` mediumint(5) NOT NULL DEFAULT '1',
  order_id varchar(50) NOT NULL,
  validity_date datetime DEFAULT NULL,
  PRIMARY KEY (id),
  KEY coupon_id (coupon_id),
  KEY coupon_code (coupon_code),
  KEY order_id (order_id),
  KEY status_coupon_id (`status`,coupon_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS enqueue_failure;
CREATE TABLE enqueue_failure (
  enqueue_failure_id int(11) NOT NULL AUTO_INCREMENT,
  queue_name varchar(30) NOT NULL,
  payload varchar(512) NOT NULL,
  created_ts datetime NOT NULL,
  PRIMARY KEY (enqueue_failure_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS freefund_class;
CREATE TABLE freefund_class (
  id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  description varchar(2000) DEFAULT NULL,
  is_active tinyint(1) NOT NULL DEFAULT '1',
  valid_from datetime NOT NULL,
  valid_upto datetime NOT NULL,
  created_at datetime NOT NULL,
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS freefund_coupon;
CREATE TABLE freefund_coupon (
  freefund_coupon_id int(11) NOT NULL AUTO_INCREMENT,
  freefund_class_id int(11) DEFAULT NULL,
  freefund_code varchar(128) DEFAULT NULL,
  generated_date datetime DEFAULT NULL,
  used_date datetime DEFAULT NULL,
  used_email_id varchar(256) DEFAULT NULL,
  used_service_number varchar(64) DEFAULT NULL,
  freefund_value decimal(10,2) DEFAULT NULL,
  min_recharge_value decimal(10,2) DEFAULT NULL,
  promo_entity varchar(40) DEFAULT NULL,
  PRIMARY KEY (freefund_coupon_id),
  KEY idx_fcode (freefund_code),
  KEY idx_maf_freefund_coupon (freefund_class_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS homepage_txn_details;
CREATE TABLE homepage_txn_details (
  homepage_txn_details_id int(11) NOT NULL AUTO_INCREMENT,
  session_unique_id char(36) DEFAULT NULL,
  lookup_id varchar(255) DEFAULT NULL,
  product_service_number varchar(64) DEFAULT NULL,
  email varchar(128) DEFAULT NULL,
  coupon varchar(128) DEFAULT NULL,
  created_on datetime DEFAULT NULL,
  modified_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  is_loggedin tinyint(1) DEFAULT NULL,
  myaccount_accessed tinyint(1) DEFAULT NULL,
  alias_used varchar(128) DEFAULT NULL,
  amount decimal(10,2) DEFAULT NULL,
  product_selected int(11) DEFAULT NULL,
  operator varchar(36) DEFAULT NULL,
  circle varchar(36) DEFAULT NULL,
  PRIMARY KEY (homepage_txn_details_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS in_aggr_opr_circle_map;
CREATE TABLE in_aggr_opr_circle_map (
  id int(20) NOT NULL AUTO_INCREMENT,
  aggr_name varchar(255) NOT NULL,
  fk_operator_id int(20) DEFAULT NULL,
  aggr_operator_code varchar(255) DEFAULT NULL,
  fk_circle_id int(20) DEFAULT NULL,
  aggr_circle_code varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY fk_circle_id (fk_circle_id),
  KEY fk_operator_id (fk_operator_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS in_errorcode_map;
CREATE TABLE in_errorcode_map (
  id int(10) NOT NULL AUTO_INCREMENT,
  aggr_name varchar(255) NOT NULL,
  aggr_resp_code varchar(255) NOT NULL,
  in_resp_code varchar(255) NOT NULL,
  in_error_msg varchar(255) NOT NULL,
  is_retryable tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS in_oppr_aggr_priority;
CREATE TABLE in_oppr_aggr_priority (
  id int(10) NOT NULL AUTO_INCREMENT,
  operator_name varchar(255) DEFAULT NULL,
  circle_name varchar(255) DEFAULT NULL,
  aggr_priority varchar(255) NOT NULL,
  update_time datetime NOT NULL,
  updated_by varchar(255) NOT NULL,
  fk_circle_operator_mapping_id int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS in_request;
CREATE TABLE in_request (
  request_id bigint(20) NOT NULL AUTO_INCREMENT,
  affiliate_id int(10) NOT NULL,
  affiliate_trans_id varchar(50) NOT NULL,
  product_type varchar(50) NOT NULL,
  subscriber_number varchar(50) NOT NULL,
  operator varchar(50) NOT NULL,
  circle varchar(250) DEFAULT NULL,
  amount float(10,2) NOT NULL,
  created_time datetime NOT NULL,
  updated_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  raw_request varchar(2000) NOT NULL,
  PRIMARY KEY (request_id),
  UNIQUE KEY idx_inrq_order_id (affiliate_trans_id,affiliate_id),
  KEY idx_inrq_affiliate_id (affiliate_trans_id),
  KEY idx_inrq_operator (operator),
  KEY idx_inrq_circle (circle),
  KEY idx_maf_in_request (created_time)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS in_response;
CREATE TABLE in_response (
  response_id bigint(20) NOT NULL AUTO_INCREMENT,
  request_id bigint(20) NOT NULL,
  raw_request varchar(2000) NOT NULL,
  raw_response varchar(2000) NOT NULL,
  aggr_trans_id varchar(20) DEFAULT NULL,
  opr_trans_id varchar(20) DEFAULT NULL,
  aggr_name varchar(20) NOT NULL,
  aggr_resp_code varchar(20) NOT NULL,
  aggr_resp_msg varchar(2000) DEFAULT NULL,
  in_resp_code varchar(20) NOT NULL,
  in_resp_msg varchar(1000) NOT NULL,
  retry_number int(10) NOT NULL,
  created_time datetime NOT NULL,
  updated_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (response_id),
  UNIQUE KEY idx_inrs_request_id (request_id),
  KEY idx_inrs_aggregator_name (aggr_name),
  KEY idx_inrs_resp_code (in_resp_code),
  KEY idx_maf_in_response (created_time),
  KEY idx_upresp (in_resp_code,updated_time)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS in_transactions;
CREATE TABLE in_transactions (
  transaction_id bigint(20) NOT NULL AUTO_INCREMENT,
  fk_request_id bigint(20) NOT NULL,
  aggr_name varchar(255) NOT NULL,
  aggr_response_code varchar(255) DEFAULT NULL,
  aggr_response_message varchar(1000) DEFAULT NULL,
  aggr_reference_no varchar(255) DEFAULT NULL,
  aggr_opr_refernce_no varchar(255) DEFAULT NULL,
  raw_request varchar(1000) DEFAULT NULL,
  raw_response varchar(1000) DEFAULT NULL,
  request_time datetime DEFAULT NULL,
  response_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  transaction_status varchar(255) DEFAULT NULL,
  request_process_time bigint(20) DEFAULT '0',
  PRIMARY KEY (transaction_id),
  KEY idx_intxn_request_id (fk_request_id),
  KEY idx_intxn_aggregator_name (aggr_name),
  KEY idx_intxn_trans_status (transaction_status)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS operator_master;
CREATE TABLE operator_master (
  operator_master_id int(11) NOT NULL AUTO_INCREMENT,
  operator_code varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  is_active tinyint(1) DEFAULT '1',
  fk_country_master_id int(11) NOT NULL,
  fk_product_master_id int(11) NOT NULL,
  img_url varchar(255) DEFAULT NULL,
  PRIMARY KEY (operator_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS order_fulfillment_details;
CREATE TABLE order_fulfillment_details (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  fk_txn_fulfillment_id bigint(20) NOT NULL,
  order_id varchar(20) NOT NULL,
  daily_seq_id bigint(20) NOT NULL,
  courier_master_id varchar(255) NOT NULL,
  awb_num varchar(255) NOT NULL,
  fulfillment_status varchar(255) NOT NULL,
  courier_remarks varchar(255) DEFAULT NULL,
  date_added datetime NOT NULL,
  date_printed datetime DEFAULT NULL,
  date_sent datetime DEFAULT NULL,
  date_delivered datetime DEFAULT NULL,
  date_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  is_first_order tinyint(1) DEFAULT '0',
  ceoletter_print_date datetime DEFAULT NULL,
  PRIMARY KEY (id),
  KEY idx_ordfull (order_id,fulfillment_status),
  KEY idx_fstatodid (fulfillment_status,order_id),
  KEY idx_maf_order_fulfillment_details (date_added),
  KEY idx_fxtxfuid (fk_txn_fulfillment_id),
  KEY idx_maf_order_fulfillment (date_printed,is_first_order)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS order_id;
CREATE TABLE order_id (
  pk_order_id int(11) NOT NULL AUTO_INCREMENT,
  lookup_id varchar(36) NOT NULL,
  session_id varchar(36) NOT NULL,
  order_id varchar(20) NOT NULL,
  order_id_inc int(11) NOT NULL,
  fk_product_master_id int(11) NOT NULL,
  fk_affiliate_profile_id int(11) NOT NULL,
  channel_id int(11) NOT NULL,
  created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (pk_order_id),
  KEY idx_oid_session_id (session_id),
  KEY id_oid_lookup_id (lookup_id),
  KEY idx_oid_create_time (created_on),
  KEY idx_oid_order_id (order_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS payment_gateway_master;
CREATE TABLE payment_gateway_master (
  payment_gateway_master_id int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `code` varchar(16) NOT NULL,
  is_active tinyint(1) NOT NULL DEFAULT '1',
  custom_property_1 varchar(64) DEFAULT NULL,
  custom_property_2 varchar(64) DEFAULT NULL,
  custom_property_3 varchar(64) DEFAULT NULL,
  PRIMARY KEY (payment_gateway_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS payment_plan;
CREATE TABLE payment_plan (
  PLAN_ID int(16) NOT NULL AUTO_INCREMENT,
  PLAN_REFERENCE varchar(64) NOT NULL,
  PG_AMOUNT decimal(10,2) NOT NULL,
  WALLET_AMOUNT decimal(10,2) NOT NULL,
  CREATED_TS datetime NOT NULL,
  UPDATED_TS timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (PLAN_ID),
  UNIQUE KEY PLAN_REFERENCE (PLAN_REFERENCE)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS payment_refund_transactions;
CREATE TABLE payment_refund_transactions (
  payment_refund_transaction_id int(11) NOT NULL AUTO_INCREMENT,
  pg_transaction_reference_no varchar(255) DEFAULT NULL,
  fk_payment_txn_id int(11) NOT NULL,
  sent_to_pg datetime NOT NULL,
  received_from_pg datetime DEFAULT NULL,
  refunded_amount decimal(10,2) DEFAULT NULL,
  raw_pg_request text NOT NULL,
  raw_pg_response text,
  pg_response_code varchar(255) DEFAULT NULL,
  pg_response_description varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  pg_status varchar(255) DEFAULT NULL,
  refund_to varchar(60) DEFAULT NULL,
  PRIMARY KEY (payment_refund_transaction_id),
  KEY idx_payRfdTxn_fk_pay_txn_id (fk_payment_txn_id),
  KEY idx_payRfdTxn_status (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS payment_rq;
CREATE TABLE payment_rq (
  payment_rq_id int(11) unsigned NOT NULL AUTO_INCREMENT,
  order_id varchar(64) NOT NULL,
  request_to_pp text CHARACTER SET latin1 NOT NULL,
  amount decimal(10,2) DEFAULT NULL,
  payment_mode varchar(16) NOT NULL,
  payment_gateway varchar(16) DEFAULT NULL,
  payment_type varchar(16) DEFAULT NULL,
  mtxn_id varchar(64) DEFAULT NULL,
  request_to_gateway text CHARACTER SET latin1,
  affiliate_code varchar(8) DEFAULT NULL,
  created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  fk_banks_grp_master_id varchar(255) DEFAULT NULL,
  PRIMARY KEY (payment_rq_id),
  KEY idx_prq_order_id (order_id),
  KEY idx_prq_mtxn_id (mtxn_id),
  KEY idx_prq_gateway (payment_gateway),
  KEY idx_prq_pay_mode (payment_mode),
  KEY idx_prq_amount (amount),
  KEY idx_prq_created_on (created_on),
  KEY idx_mtx_date (created_on)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS payment_rs;
CREATE TABLE payment_rs (
  payment_rs_id int(11) unsigned NOT NULL AUTO_INCREMENT,
  is_successful tinyint(1) unsigned NOT NULL,
  pg_transaction_id varchar(64) DEFAULT NULL,
  order_id varchar(64) DEFAULT NULL,
  mtxn_id varchar(64) DEFAULT NULL,
  response_data text CHARACTER SET latin1,
  amount decimal(10,2) NOT NULL,
  payment_gateway varchar(16) NOT NULL,
  pg_message text CHARACTER SET latin1,
  rrn_no varchar(64) DEFAULT NULL,
  auth_code_no varchar(64) DEFAULT NULL,
  avs_code varchar(16) DEFAULT NULL,
  eci varchar(16) DEFAULT NULL,
  csc_response_code varchar(16) DEFAULT NULL,
  response_from_pp text CHARACTER SET latin1,
  pg_transaction_time timestamp NULL DEFAULT NULL,
  created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (payment_rs_id),
  KEY idx_prs_order_id (order_id),
  KEY idx_prs_pg_trans_id (pg_transaction_id),
  KEY idx_prs_mtxn_id (mtxn_id),
  KEY idx_prs_is_successful (is_successful),
  KEY idx_prs_gateway (payment_gateway),
  KEY idx_prs_amount (amount),
  KEY idx_prs_created_on (created_on),
  KEY idx_mtx_date1 (created_on)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS payment_txn;
CREATE TABLE payment_txn (
  payment_txn_id int(11) unsigned NOT NULL AUTO_INCREMENT,
  pg_transaction_id varchar(64) DEFAULT NULL,
  order_id varchar(64) NOT NULL,
  sent_to_pg datetime DEFAULT NULL,
  recieved_from_pg datetime DEFAULT NULL,
  amount decimal(10,2) DEFAULT NULL,
  mtxn_id varchar(64) DEFAULT NULL,
  is_successful tinyint(1) unsigned DEFAULT NULL,
  payment_gateway varchar(16) DEFAULT NULL,
  payment_mode varchar(16) DEFAULT NULL,
  pg_message text CHARACTER SET latin1,
  fk_payment_rq_id int(11) DEFAULT NULL,
  fk_payment_rs_id int(11) DEFAULT NULL,
  payment_type int(3) DEFAULT NULL,
  payment_option varchar(24) DEFAULT NULL,
  pg_transaction_time timestamp NULL DEFAULT NULL,
  PRIMARY KEY (payment_txn_id),
  KEY idx_ptxn_mtxn_id (mtxn_id),
  KEY idx_ptxn_order_id (order_id),
  KEY idx_ptxn_pg_trans_id (pg_transaction_id),
  KEY idx_ptxn_is_successful (is_successful),
  KEY idx_ptxn_amount (amount),
  KEY idx_ptxn_gateway (payment_gateway),
  KEY idx_ptxn_prq_id (fk_payment_rq_id),
  KEY idx_ptxn_prs_id (fk_payment_rs_id),
  KEY idx_ptxn_pay_type (payment_type),
  KEY idx_ptxn_pay_option (payment_option),
  KEY idx_ptxn_sent_to_pg (sent_to_pg),
  KEY idx_ptxn_received_time (recieved_from_pg)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS payment_type_master;
CREATE TABLE payment_type_master (
  payment_type_master_id int(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) DEFAULT NULL,
  display_data varchar(128) DEFAULT NULL,
  PRIMARY KEY (payment_type_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS payment_type_options;
CREATE TABLE payment_type_options (
  payment_type_options_id int(11) NOT NULL AUTO_INCREMENT,
  fk_payment_type_relation_id int(11) DEFAULT NULL,
  display_name varchar(64) DEFAULT NULL,
  display_img varchar(128) DEFAULT NULL,
  img_url varchar(128) DEFAULT NULL,
  `code` varchar(8) DEFAULT NULL,
  is_active tinyint(1) NOT NULL DEFAULT '1',
  order_by int(2) DEFAULT NULL,
  PRIMARY KEY (payment_type_options_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS payment_type_relation;
CREATE TABLE payment_type_relation (
  payment_type_relation_id int(11) NOT NULL AUTO_INCREMENT,
  fk_product_master_id int(11) DEFAULT NULL,
  fk_affliate_unique_id varchar(36) DEFAULT NULL,
  fk_payment_type_id int(11) DEFAULT NULL,
  is_active tinyint(1) NOT NULL DEFAULT '1',
  order_by int(2) DEFAULT NULL,
  PRIMARY KEY (payment_type_relation_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS pg_mis_user;
CREATE TABLE pg_mis_user (
  pg_user_id int(5) NOT NULL,
  pg_user_name varchar(50) NOT NULL,
  pg_user_password varchar(50) NOT NULL,
  pg_user_active_status tinyint(4) DEFAULT NULL,
  PRIMARY KEY (pg_user_id,pg_user_name)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS pg_relation;
CREATE TABLE pg_relation (
  pg_relation_id int(11) NOT NULL AUTO_INCREMENT,
  fk_circle_master_id int(11) DEFAULT NULL,
  fk_in_master int(11) DEFAULT NULL,
  fk_product_master_id int(11) DEFAULT NULL,
  fk_affiliate_profile_id int(11) DEFAULT NULL,
  fk_banks_grp_master_id int(11) DEFAULT NULL,
  is_active tinyint(1) DEFAULT '1',
  primary_gateway int(2) DEFAULT NULL,
  secondary_gateway int(2) DEFAULT NULL,
  PRIMARY KEY (pg_relation_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS pincode_city_courier_zone;
CREATE TABLE pincode_city_courier_zone (
  id int(11) NOT NULL AUTO_INCREMENT,
  pincode int(6) DEFAULT NULL,
  courier_id int(11) DEFAULT NULL,
  city_id int(11) DEFAULT NULL,
  zone_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY idx_city_id (city_id),
  KEY idx_courier_id (courier_id),
  KEY idx_pincode (pincode),
  KEY idx_zone_id (zone_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS product_master;
CREATE TABLE product_master (
  product_master_id int(11) NOT NULL AUTO_INCREMENT,
  product_name varchar(255) DEFAULT NULL,
  img_url varchar(255) DEFAULT NULL,
  fk_affiliate_profile_id int(11) DEFAULT '1',
  is_active tinyint(1) DEFAULT NULL,
  product_type varchar(10) DEFAULT NULL,
  is_refundable tinyint(1) DEFAULT NULL,
  PRIMARY KEY (product_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS promocode;
CREATE TABLE promocode (
  promocode_id int(10) unsigned NOT NULL AUTO_INCREMENT,
  promo_title varchar(250) NOT NULL,
  promo_code varchar(250) NOT NULL,
  success_msg varchar(1250) NOT NULL,
  failure_msg varchar(1250) NOT NULL,
  valid_from timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  valid_upto timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (promocode_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS promocode_campaign;
CREATE TABLE promocode_campaign (
  promocode_campaign_id int(10) NOT NULL AUTO_INCREMENT,
  fk_promocode_id int(10) unsigned NOT NULL,
  phone_no varchar(10) DEFAULT NULL,
  fk_user_id int(10) DEFAULT NULL,
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (promocode_campaign_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS recharge_plans;
CREATE TABLE recharge_plans (
  recharge_plan_id int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  amount decimal(10,2) DEFAULT NULL,
  min_amount decimal(10,2) DEFAULT NULL,
  max_amount decimal(10,2) DEFAULT NULL,
  talktime decimal(10,2) DEFAULT NULL,
  validity int(10) DEFAULT NULL,
  fk_product_master_id int(10) DEFAULT NULL,
  denomination_type varchar(255) DEFAULT NULL,
  description varchar(2000) DEFAULT NULL,
  fk_operator_master_id int(10) DEFAULT NULL,
  fk_circle_master_id int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (recharge_plan_id),
  KEY fk_operator_master_id (fk_operator_master_id),
  KEY fk_circle_master_id (fk_circle_master_id),
  KEY fk_product_master_id (fk_product_master_id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS reissued_coupons;
CREATE TABLE reissued_coupons (
  id int(11) NOT NULL AUTO_INCREMENT,
  order_id varchar(50) DEFAULT NULL,
  reissued_coupon_id int(11) NOT NULL,
  reissued_coupon_quantity int(11) NOT NULL,
  fc_ticket_id int(11) NOT NULL,
  remarks varchar(500) DEFAULT NULL,
  is_deleted tinyint(4) DEFAULT '0',
  created_on datetime DEFAULT NULL,
  updated_on datetime DEFAULT NULL,
  created_by varchar(50) DEFAULT NULL,
  deleted_by varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS replaced_coupons;
CREATE TABLE replaced_coupons (
  id int(11) NOT NULL AUTO_INCREMENT,
  order_id varchar(50) DEFAULT NULL,
  replaced_coupon_id int(11) NOT NULL,
  replaced_coupon_quantity int(11) NOT NULL,
  fc_ticket_id int(11) NOT NULL,
  created_on datetime DEFAULT NULL,
  updated_on datetime DEFAULT NULL,
  created_by varchar(50) DEFAULT NULL,
  updated_by varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS review_address_details;
CREATE TABLE review_address_details (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  user_id bigint(20) NOT NULL,
  delivery_name varchar(255) NOT NULL,
  delivery_add varchar(255) NOT NULL,
  delivery_city varchar(255) NOT NULL,
  delivery_fk_state_master_id int(11) NOT NULL,
  delivery_fk_country_master_id int(11) NOT NULL,
  delivery_landmark varchar(255) NOT NULL,
  delivery_pincode varchar(255) NOT NULL,
  order_id varchar(45) NOT NULL,
  date_added datetime NOT NULL,
  review_status int(11) NOT NULL DEFAULT '0' COMMENT '''0''=>not reviewed,''1''=>reviewed',
  email_sent varchar(60) DEFAULT NULL,
  addr_updated varchar(60) DEFAULT NULL,
  email_sent_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  addr_updated_date timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  addr_review_date datetime NOT NULL,
  sent_for_reprint set('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (id),
  KEY user_id (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS session_details;
CREATE TABLE session_details (
  session_details_id int(11) NOT NULL AUTO_INCREMENT,
  session_unique_id char(36) DEFAULT NULL,
  ip_address varchar(255) DEFAULT NULL,
  last_access timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  create_time timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  expiry_time timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (session_details_id),
  KEY idx_session_id (session_unique_id),
  KEY idx_ipaddr (ip_address)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS session_master;
CREATE TABLE session_master (
  session_master_id int(11) NOT NULL AUTO_INCREMENT,
  session_unique_id char(36) DEFAULT NULL,
  session_data longblob,
  updated_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (session_master_id),
  KEY idx_session_master_session_id (session_unique_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS site_autosuggest_prefix;
CREATE TABLE site_autosuggest_prefix (
  site_autosuggest_prefix_id int(11) NOT NULL AUTO_INCREMENT,
  prefix varchar(64) DEFAULT NULL,
  fk_circle_operator_mapping_id int(11) DEFAULT NULL,
  fk_product_master_id int(11) DEFAULT NULL,
  is_active tinyint(1) DEFAULT NULL,
  PRIMARY KEY (site_autosuggest_prefix_id),
  UNIQUE KEY idx_prefix (prefix),
  KEY idx_prefix_circle_operator_mapping_id (fk_circle_operator_mapping_id),
  KEY idx_prefix_product_id (fk_product_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS state_master;
CREATE TABLE state_master (
  state_master_id int(11) NOT NULL AUTO_INCREMENT,
  state_name varchar(100) NOT NULL,
  is_active tinyint(1) NOT NULL DEFAULT '1',
  fk_zone_master_id int(11) NOT NULL,
  fk_country_master_id int(11) NOT NULL,
  PRIMARY KEY (state_master_id),
  KEY index_state_name (state_name)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS txn_cross_sell;
CREATE TABLE txn_cross_sell (
  id int(11) NOT NULL AUTO_INCREMENT,
  txn_home_page_id int(11) DEFAULT NULL,
  coupon_id int(11) DEFAULT NULL,
  corss_sell_id int(11) DEFAULT NULL,
  is_complimentary tinyint(1) NOT NULL DEFAULT '0',
  is_deleted tinyint(1) DEFAULT NULL,
  created_on timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  quantity int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY idx_txn_home_page_id (txn_home_page_id),
  KEY idx_feeee (txn_home_page_id,coupon_id),
  KEY idx_cupid (coupon_id),
  KEY idx_isdcip (is_deleted,coupon_id),
  KEY idx_hpgcid (txn_home_page_id,coupon_id),
  KEY idx_txn_pgciddel (txn_home_page_id,coupon_id,is_deleted)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS txn_fulfilment;
CREATE TABLE txn_fulfilment (
  id int(11) NOT NULL AUTO_INCREMENT,
  txn_home_page_id int(11) DEFAULT NULL,
  email varchar(255) DEFAULT NULL,
  address_mobile varchar(12) DEFAULT NULL,
  first_name varchar(255) DEFAULT NULL,
  is_guest tinyint(1) DEFAULT NULL,
  delivery_street varchar(255) DEFAULT NULL,
  delivery_area varchar(255) DEFAULT NULL,
  delivery_landmark varchar(255) DEFAULT NULL,
  delivery_add varchar(255) DEFAULT NULL,
  delivery_city varchar(255) DEFAULT NULL,
  delivery_fk_state_master_id int(11) DEFAULT NULL,
  delivery_fk_country_master_id int(11) DEFAULT NULL,
  delivery_country varchar(255) DEFAULT NULL,
  delivery_pincode varchar(255) DEFAULT NULL,
  delivery_name varchar(255) DEFAULT NULL,
  sms_sent datetime DEFAULT NULL,
  email_sent datetime DEFAULT NULL,
  transaction_status tinyint(4) DEFAULT NULL,
  order_id varchar(45) DEFAULT NULL,
  fk_user_profile_id int(11) DEFAULT NULL,
  created_at datetime DEFAULT NULL,
  updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  KEY idx_txnful_home_page_id (txn_home_page_id),
  KEY idx_txnful_email (email),
  KEY idx_txnful_email_sent (email_sent),
  KEY idx_txnful_sms_sent (sms_sent),
  KEY idx_txnful_txn_status (transaction_status),
  KEY idx_txnful_order_id (order_id),
  KEY idx_txnful_profile_id (fk_user_profile_id),
  KEY idx_txnfulfill_created_at (created_at),
  KEY idx_freech (order_id,updated_at),
  KEY idx_upat (updated_at),
  KEY idx_maf_txn_fulfilment (transaction_status,delivery_fk_state_master_id,delivery_city)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS txn_home_page;
CREATE TABLE txn_home_page (
  id int(11) NOT NULL AUTO_INCREMENT,
  service_number varchar(45) DEFAULT NULL,
  operator_name varchar(45) DEFAULT NULL,
  circle_name varchar(255) DEFAULT NULL,
  amount float DEFAULT NULL,
  product_type varchar(45) DEFAULT NULL,
  session_id varchar(45) DEFAULT NULL,
  lookup_id varchar(45) DEFAULT NULL,
  creatred_on timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  fk_product_master_id int(11) DEFAULT NULL,
  fk_circle_master_id int(11) DEFAULT NULL,
  fk_operator_master_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY idx_lookup_id (lookup_id),
  KEY idx_session_id (session_id),
  KEY idx_thp_create_time (creatred_on),
  KEY idx_maf_txn_home_page (service_number)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS user_card_meta;
CREATE TABLE user_card_meta (
  user_card_meta_id int(11) NOT NULL AUTO_INCREMENT,
  fk_user_id int(11) NOT NULL,
  fk_card_meta_id int(11) NOT NULL,
  created_ts datetime NOT NULL,
  PRIMARY KEY (user_card_meta_id),
  UNIQUE KEY user_card_meta_unique (fk_user_id,fk_card_meta_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS user_card_storage_choice_preference;
CREATE TABLE user_card_storage_choice_preference (
  id int(11) NOT NULL AUTO_INCREMENT,
  fk_users_id int(11) DEFAULT NULL,
  preference varchar(20) DEFAULT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS user_invitee_list;
CREATE TABLE user_invitee_list (
  user_rechargee_list_id int(11) NOT NULL AUTO_INCREMENT,
  fk_user_id int(11) NOT NULL,
  first_name varchar(64) DEFAULT NULL,
  last_name varchar(64) DEFAULT NULL,
  nick_name varchar(64) DEFAULT NULL,
  created_date datetime NOT NULL,
  is_invited tinyint(1) DEFAULT NULL,
  invitation_accepted datetime DEFAULT NULL,
  inviation_date datetime DEFAULT NULL,
  invitation_source varchar(255) DEFAULT NULL,
  invitation_code varchar(64) DEFAULT NULL,
  PRIMARY KEY (user_rechargee_list_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS user_profile;
CREATE TABLE user_profile (
  user_profile_id int(11) NOT NULL AUTO_INCREMENT,
  fk_user_id int(11) DEFAULT '1',
  fk_user_recharge_contact_id int(11) DEFAULT NULL,
  nick_alias varchar(64) DEFAULT NULL COMMENT 'this is to indentify type of user like website or backend user',
  address1 varchar(255) DEFAULT NULL,
  landmark varchar(255) DEFAULT NULL,
  city varchar(100) DEFAULT '7',
  postal_code varchar(50) DEFAULT NULL,
  fk_state_master_id int(11) DEFAULT NULL,
  fk_country_master_id int(11) DEFAULT NULL,
  is_active tinyint(1) DEFAULT NULL,
  is_default tinyint(1) DEFAULT NULL,
  created_on datetime DEFAULT NULL,
  last_update timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  first_name varchar(255) DEFAULT NULL,
  last_name varchar(255) DEFAULT NULL,
  title varchar(5) DEFAULT NULL,
  area varchar(100) DEFAULT NULL,
  street varchar(100) DEFAULT NULL,
  PRIMARY KEY (user_profile_id),
  KEY idx_fk_user_id (fk_user_id),
  KEY idx_com (fk_user_recharge_contact_id),
  KEY ind_fk_country_master_id (fk_country_master_id),
  KEY ind_contact_id_first_name (fk_user_recharge_contact_id,first_name),
  KEY ind_fk_country_master_id_last_update (fk_country_master_id,last_update),
  KEY idx_profile_recharge_contact_id (fk_user_recharge_contact_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS user_recharge_contact;
CREATE TABLE user_recharge_contact (
  user_recharge_contact_id int(11) NOT NULL AUTO_INCREMENT,
  fk_users_id int(11) DEFAULT NULL,
  service_number varchar(128) DEFAULT NULL,
  fk_product_master_id int(11) DEFAULT NULL,
  fk_circle_master_id int(11) DEFAULT NULL,
  fk_operator_master_id int(11) DEFAULT NULL,
  nick_alias varchar(255) DEFAULT NULL,
  last_recharge_amount decimal(10,2) DEFAULT NULL,
  is_active tinyint(1) DEFAULT NULL,
  created_on datetime DEFAULT NULL,
  updated_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (user_recharge_contact_id),
  KEY idx_com (fk_users_id,service_number),
  KEY ind_1 (service_number,fk_users_id),
  KEY ind_fk_users_id_user_recharge_contact_id (fk_users_id,user_recharge_contact_id),
  KEY ind_fk_users_id (fk_users_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS user_recharge_history;
CREATE TABLE user_recharge_history (
  user_recharge_history_id int(11) NOT NULL AUTO_INCREMENT,
  fk_users_id int(11) DEFAULT NULL,
  fk_user_recharge_contact int(11) DEFAULT NULL,
  txn_amount decimal(10,2) DEFAULT NULL,
  payment_status enum('0','1','2') DEFAULT NULL COMMENT 'success, failed, pending',
  recharge_status enum('0','1','2') DEFAULT NULL COMMENT 'failes, success, pending',
  fk_in_master_id int(11) DEFAULT NULL,
  fk_payment_gateway_id int(11) DEFAULT NULL,
  created_on datetime DEFAULT NULL,
  updated_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  fk_payment_txn_id int(11) DEFAULT NULL,
  fk_recharge_txn_id int(11) DEFAULT NULL,
  order_id varchar(20) DEFAULT NULL,
  PRIMARY KEY (user_recharge_history_id),
  KEY idx_history_user_id (fk_users_id),
  KEY idx_history_user_recharge_contact_id (fk_user_recharge_contact),
  KEY idx_history_payment_txn_id (fk_payment_txn_id),
  KEY idx_history_recharge_txn_id (fk_recharge_txn_id),
  KEY idx_history_order_id (order_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  user_id int(11) NOT NULL AUTO_INCREMENT,
  fk_affiliate_profile_id int(11) DEFAULT '1',
  `type` int(2) DEFAULT NULL COMMENT 'this is to indentify type of user like website or backend user',
  email varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  mobile_no varchar(20) NOT NULL,
  dob date DEFAULT NULL,
  is_active tinyint(1) DEFAULT NULL,
  date_added datetime DEFAULT NULL,
  last_loggedin datetime DEFAULT NULL,
  is_loggedin tinyint(1) DEFAULT '0',
  morf varchar(5) DEFAULT NULL,
  last_update timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  forgot_password_key varchar(128) DEFAULT NULL,
  forgot_password_expiry date DEFAULT NULL,
  fk_refferred_by_user_id int(11) DEFAULT NULL,
  PRIMARY KEY (user_id),
  KEY idx_email (email),
  KEY idx_users_mobile (mobile_no),
  KEY idx_usrs (date_added)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS voucher_cat;
CREATE TABLE voucher_cat (
  voucher_cat_id int(11) NOT NULL AUTO_INCREMENT,
  cat_name varchar(50) DEFAULT NULL,
  cat_detail varchar(200) DEFAULT NULL,
  cat_image varchar(200) DEFAULT NULL,
  cat_flag tinyint(1) DEFAULT NULL,
  PRIMARY KEY (voucher_cat_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS wallet_account;
CREATE TABLE wallet_account (
  wallet_account_id int(11) NOT NULL AUTO_INCREMENT,
  fk_users_id int(11) DEFAULT NULL,
  fk_affiliate_profile_id int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  current_balance decimal(10,2) DEFAULT NULL,
  is_active tinyint(1) DEFAULT NULL,
  last_used datetime DEFAULT NULL,
  created_on datetime DEFAULT NULL,
  PRIMARY KEY (wallet_account_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS wallet_items;
CREATE TABLE wallet_items (
  wallet_items_id int(11) NOT NULL AUTO_INCREMENT,
  fk_wallet_transaction_id int(11) DEFAULT NULL,
  fk_item_id int(11) DEFAULT NULL,
  entity_id varchar(50) DEFAULT NULL,
  added_on datetime DEFAULT NULL,
  PRIMARY KEY (wallet_items_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS wallet_status;
CREATE TABLE wallet_status (
  WALLET_STATUS_ID int(10) NOT NULL AUTO_INCREMENT,
  FK_USER_ID int(10) NOT NULL,
  BALANCE decimal(10,2) NOT NULL,
  CREATED_TS datetime NOT NULL,
  UPDATED_TS datetime NOT NULL,
  PRIMARY KEY (WALLET_STATUS_ID),
  UNIQUE KEY FK_USER_ID (FK_USER_ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS wallet_transaction;
CREATE TABLE wallet_transaction (
  WALLET_TRANSACTION_ID int(10) NOT NULL AUTO_INCREMENT,
  FK_USER_ID int(10) NOT NULL,
  ORDER_ID varchar(20) NOT NULL,
  TXN_AMOUNT decimal(10,2) NOT NULL,
  IS_DEBIT tinyint(1) NOT NULL,
  OLD_BALANCE decimal(10,2) NOT NULL,
  NEW_BALANCE decimal(10,2) NOT NULL,
  CREATED_TS datetime NOT NULL,
  caller_refrence varchar(128) DEFAULT NULL,
  fund_source varchar(20) DEFAULT NULL,
  fund_destination varchar(20) DEFAULT NULL,
  metadata varchar(256) DEFAULT NULL,
  transaction_type varchar(20) DEFAULT NULL,
  fk_wallet_id int(10) DEFAULT NULL,
  transaction_date datetime DEFAULT NULL,
  PRIMARY KEY (WALLET_TRANSACTION_ID),
  KEY idx_caller_reference (caller_refrence),
  KEY wallet_transaction_fk_user_i (FK_USER_ID),
  KEY idx_maf_wallet_transaction (ORDER_ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS zone_master;
CREATE TABLE zone_master (
  zone_master_id int(11) NOT NULL AUTO_INCREMENT,
  zone_name varchar(15) DEFAULT NULL,
  fk_country_master_id int(11) NOT NULL,
  PRIMARY KEY (zone_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
