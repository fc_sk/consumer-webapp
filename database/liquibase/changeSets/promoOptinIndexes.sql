-- liquibase formatted sql

-- changeset shwetanka:promoOptinIndexes

create index promo_action_id on ar_promo_opt_in_activity (fk_ar_action_id);

create index promo_user_id on ar_promo_opt_in_activity(fk_user_id);

create index promo_session_id on ar_promo_opt_in_activity (session_id);

-- rollback drop index promo_user_id on ar_promo_opt_in_activity;
-- rollback drop index promo_action_id on ar_promo_opt_in_activity
-- rollback drop index promo_session_id on ar_promo_opt_in_activity
