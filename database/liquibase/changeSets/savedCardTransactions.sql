-- liquibase formatted sql

-- changeset chirag:savedCardTransactions

CREATE TABLE IF NOT EXISTS saved_card_transactions (
  id int(11) not null auto_increment
, order_id varchar(20)
, merchant_transaction_id varchar(20)
, ts timestamp default current_timestamp on update current_timestamp
, primary key(id)
, INDEX(order_id)
, INDEX(merchant_transaction_id));
