-- liquibase formatted sql

-- changeset priyanalang:alterBinbasedOffer-datamap

alter table binbased_offer add column datamap TEXT NULL DEFAULT NULL after description;

-- rollback ALTER TABLE binbased_offer DROP COLUMN datamap;