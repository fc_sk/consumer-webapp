-- liquibase formatted sql

-- changeset kopaul:updateOneCheckToFCWallet

update payment_type_master set name="FreeCharge Wallet", display_data="FreeCharge Wallet" where payment_type_master_id=9;
update payment_type_options set display_name="FreeCharge Wallet" where fk_payment_type_relation_id=59;

-- rollback DELETE from payment_type_master where payment_type_master_id=9
