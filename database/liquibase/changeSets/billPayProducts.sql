-- liquibase formatted sql

-- changeset kopaul:billPayProducts

insert into product_master values(312,"Landline", NULL, 1, 1,1,1), (313,"Electricity", NULL, 1, 1,1,1), (314,"Insurance", NULL, 1, 1,1,1), (315,"Gas", NULL, 1, 1,1,1);

-- rollback DELETE from product_master where product_master_id in (312,313,314,315)