-- liquibase formatted sql

-- changeset shwetanka:arActionEmailTemplate

alter table ar_action add column email_template text default null;

-- rollback alter table ar_action drop column email_template