--liquibase formatted sql

--changeset amaresh:freefundchannelcondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('userconnectedviaprovidercondition', 'userconnectedviaprovidercondition', now(), now(), 'Params: provider . Values : facebook or google');

--rollback delete from ar_condition_type where class_ident = 'freefundchannelcondition'