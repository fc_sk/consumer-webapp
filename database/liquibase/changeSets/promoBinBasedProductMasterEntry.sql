--liquibase formatted sql

--changeset chetanvaity:promoBinBasedProductMasterEntry

insert into product_master (product_master_id, product_name, fk_affiliate_profile_id, is_active, product_type, is_refundable) values (81, 'FreeFund', 1, 1, 4, 0);

--rollback delete from product_master where product_master_id=81;