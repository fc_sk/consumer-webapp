-- liquibase formatted sql

-- changeset ujjwala:alterCreditEncashBankDetail

alter table fc_credit_encash_bank_detail add reference_id varchar(200);
alter table fc_credit_encash_bank_detail add amount decimal(10,2);

-- rollback ALTER TABLE fc_credit_encash_bank_detail DROP COLUMN reference_id;
-- rollback ALTER TABLE fc_credit_encash_bank_detail DROP COLUMN amount;
