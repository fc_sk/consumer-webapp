-- liquibase formatted sql

-- changeset jyothi:maxRedemptionPerEventCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('Max Redemption Per Event Condition', 'maxRedemptionPerEventCondition', now(), now(), 'Params required : maxRedemptionPerNumber(1..n)');

-- rollback delete from ar_condition_type where class_ident = 'maxRedemptionPerEventCondition';
