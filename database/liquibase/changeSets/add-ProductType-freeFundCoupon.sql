-- liquibase formatted sql

-- changeset Asmita:add-product-type-in-freefund-coupon

INSERT INTO ar_condition_type
	(name, class_ident, created_on, updated_on, description) 
	VALUES ('FreefundProductCondition', 'freefundProductCondition', NOW(), NOW(), 'Params required are products (comma 
	seperated list - Mobile, DTH, DataCard)');


-- rollback DELETE FROM ar_condition_type
-- rollback   WHERE name = 'FreefundProductCondition' AND class_ident = 'freefundProductCondition';

