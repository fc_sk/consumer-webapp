-- liquibase formatted sql

-- changeset Vipin:userTransactionHistoryService

CREATE TABLE `user_transaction_history` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(20) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `transaction_amount` decimal(10,2) NOT NULL,
  `subscriber_identification_number` varchar(128) NOT NULL,
  `product_type` varchar(128) NOT NULL,
  `service_provider` varchar(128) NOT NULL,
  `service_region` varchar(128) NOT NULL,
  `transaction_status` varchar(64) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_updated` datetime NOT NULL,
  `metadata` TEXT DEFAULT NULL,
  PRIMARY KEY (`transaction_id`,`last_updated`),
  KEY `idx_user_transaction_history_order_id` (`order_id`),
  KEY `idx_user_transaction_history_fk_user_id` (`fk_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
PARTITION BY list (month(last_updated))(
  PARTITION uth_p1 VALUES IN (1),
  PARTITION uth_p2 VALUES IN (2),
  PARTITION uth_p3 VALUES IN (3),
  PARTITION uth_p4 VALUES IN (4),
  PARTITION uth_p5 VALUES IN (5),
  PARTITION uth_p6 VALUES IN (6),
  PARTITION uth_p7 VALUES IN (7),
  PARTITION uth_p8 VALUES IN (8),
  PARTITION uth_p9 VALUES IN (9),
  PARTITION uth_p10 VALUES IN (10),
  PARTITION uth_p11 VALUES IN (11),
  PARTITION uth_p12 VALUES IN (12));
    
-- rollback DROP TABLE IF EXISTS `user_transaction_history`;
