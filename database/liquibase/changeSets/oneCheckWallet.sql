-- liquibase formatted sql

-- changeset kopaul:oneCheckWallet

insert into payment_type_master values (9, "OneCheck Wallet", "OCW", "OneCheck Wallet");
insert into payment_type_relation(payment_type_relation_id, fk_product_master_id, fk_affliate_unique_id, fk_payment_type_id,is_active,order_by) values(59, 1,1,9,1,6);
insert into payment_type_options(fk_payment_type_relation_id ,display_name, display_img, img_url, code, is_active, order_by) values(59,"OneCheck Wallet", NULL, NULL, "OCW", 1,0);

-- rollback DELETE from payment_type_master where payment_type_master_id=9
-- rollback DELETE from payment_type_relation where fk_payment_type_id=9
