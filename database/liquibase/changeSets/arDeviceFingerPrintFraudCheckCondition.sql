-- liquibase formatted sql

-- changeset priyanalang:arDeviceFingerPrintFraudCheckCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('DeviceFingerPrintFraudCheckCondition', 'deviceFingerPrintFraudCheckCondition', now(), now(), 'ONLY USE ON APPLY. Checks if Max Redemptions for FingerPrint and User have been reached. Params are : maxRedemptionFingerPrint=-1...n, maxRedemptionFPPerUser=-1..n. -1 means unlimited redemptions');

-- rollback delete from ar_condition_type where class_ident = 'deviceFingerPrintFraudCheckCondition';