-- liquibase formatted sql

-- changeset arun:inAggrOprCircleMap

ALTER TABLE in_aggr_opr_circle_map ADD COLUMN should_timeout TINYINT(1) DEFAULT 0;
ALTER TABLE in_aggr_opr_circle_map ADD COLUMN time_out_millis INT(20) DEFAULT -1;

-- rollback ALTER TABLE in_aggr_opr_circle_map DROP should_timeout;
-- rollback ALTER TABLE in_aggr_opr_circle_map DROP time_out_millis;
