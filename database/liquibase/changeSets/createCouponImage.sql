-- liquibase formatted sql

-- changeset divyansh:couponImage

CREATE TABLE coupon_image (
id int(11) NOT NULL AUTO_INCREMENT,
coupon_store_id int(11) NOT NULL, 
channel varchar(20) NOT NULL,
image_name varchar(50) NOT NULL,
resolution varchar(10) NOT NULL,
version varchar(10) NOT NULL,
date_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
primary key (id) , UNIQUE KEY `coupon_store_image_key` (`coupon_store_id`,`channel`,`resolution`,`version`))
ENGINE=InnoDB AUTO_INCREMENT=1, DEFAULT CHARSET=utf8;

CREATE INDEX idx_coupon_image_coupon_store_id ON coupon_image(coupon_store_id) USING BTREE;

-- rollback DROP TABLE coupon_image;

 
 
 
 
