--liquibase formatted sql

-- changeset korathpaul:dataweave_id
ALTER TABLE recharge_plans ADD COLUMN dataweave_id varchar(255) DEFAULT NULL;
ALTER TABLE recharge_plans ADD KEY idx_recharge_plans_dataweave_id USING BTREE (dataweave_id);

-- rollback ALTER TABLE recharge_plans DROP COLUMN dataweave_id;