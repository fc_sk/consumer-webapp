-- liquibase formatted sql

-- changeset priyanalang:growthEvent

CREATE TABLE `growth_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `event_type` varchar(50) NOT NULL,
  `fk_reward_id` int(11) NOT NULL,
  `fk_condition_id` int(11) NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_upto` datetime NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `channel` tinyint(3) NOT NULL,
  `is_privilege_user_cond` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `growth_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_growth_event_id` int(11) NOT NULL,
  `event_type` varchar(50) NOT NULL,
  `fk_user_id` int(11) DEFAULT NULL,
  `session_id` varchar(36) DEFAULT NULL,
  `lookup_id` varchar(36) DEFAULT NULL,
  `thread_id` int(11) DEFAULT NULL,
  `activity` varchar(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `growth_reward_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_user_id` int(11) NOT NULL,
  `fk_growth_event_id` int(11) NOT NULL,
  `fk_ar_reward_id` int(11) NOT NULL,
  `fk_ar_reward_type_id` int(11) NOT NULL,
  `reward_value` varchar(50) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollback DROP TABLE if exists growth_event;
-- rollback DROP TABLE if exists growth_activity;
-- rollback DROP TABLE if exists growth_reward_history;
