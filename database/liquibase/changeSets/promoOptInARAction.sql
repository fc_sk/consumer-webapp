-- liquibase formatted sql

-- changeset anderavi:promoOptInARAction

insert into ar_action_type(name, class_ident, created_on) values('PromoOptIn', 'promoOptInARAction', now());

-- rollback DELETE FROM ar_action_type where class_ident='promoOptInARAction';
