-- liquibase formatted sql

-- changeset sreeram:add-product-fraud-check-tables
CREATE TABLE duration_master (
  duration_id int(2) NOT NULL,
  description varchar(30) NOT NULL,
  number_of_days int(11) NOT NULL,
  number_of_hours int(11) NOT NULL,
  number_of_minutes int(11) NOT NULL,
  PRIMARY KEY (`duration_id`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

  

CREATE TABLE product_fraud_limit (
  fraud_limit_id int(2) NOT NULL AUTO_INCREMENT,
  fk_product_master_id  int(11) NOT NULL,
  fk_rolling_window_id int(2) NOT NULL,
  fk_fc_acct_age_id int(2) NULL,
  max_amount int(11) NOT NULL,
  max_txn_count int(11) NULL,
  active_in char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (fraud_limit_id),
  FOREIGN KEY (fk_product_master_id)
      REFERENCES product_master(product_master_id),
  FOREIGN KEY (fk_rolling_window_id)
      REFERENCES duration_master(duration_id),
  FOREIGN KEY (fk_fc_acct_age_id)
      REFERENCES duration_master(duration_id)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


