-- liquibase formatted sql

-- changeset abhishekanand:createInvalidPlanDenominationAttempts

CREATE TABLE IF NOT EXISTS `invalid_plan_denomination_attempts` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `product_id` int(5) NOT NULL,
    `fk_operator_id` int(10) NOT NULL,
    `fk_circle_id` int(10) NOT NULL,
    `amount` decimal(4,2) DEFAULT NULL,
    `mobile_number` VARCHAR(15) DEFAULT NULL,
    `user_id` VARCHAR(50) DEFAULT NULL,
    `created_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `plan_type` VARCHAR(10) DEFAULT NULL,
    `n_last_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `n_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `idx_userid` (`user_id`),
    KEY `idx_operatorid_circleid` (`fk_operator_id`, `fk_circle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- rollback drop table invalid_plan_denomination_attempts;