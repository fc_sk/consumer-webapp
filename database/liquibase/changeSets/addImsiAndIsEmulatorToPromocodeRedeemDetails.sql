-- liquibase formatted sql

-- changeset kamal:addImsiAndIsEmulatorToPromocodeRedeemDetails

alter table promocode_redeem_details 
 add column imsi varchar(50), 
 add column is_emulator boolean;

-- rollback alter table promocode_redeem_details drop column imsi_number, drop column is_emulator;
