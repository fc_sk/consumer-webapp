-- liquibase formatted sql

-- changeset korathpaul:createUserWhitelistTable

CREATE TABLE IF NOT EXISTS `users_whitelist` (
    `user_id` int(11) unsigned NOT NULL,
    `whitelisted` tinyint(1) unsigned DEFAULT NULL,
    PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- rollback drop table users_whitelist;
