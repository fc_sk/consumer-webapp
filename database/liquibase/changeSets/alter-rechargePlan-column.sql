--liquibase formatted sql

--changeset GoyalVipin:002-alter-rechargeplan-table-column

ALTER TABLE recharge_plans ADD COLUMN created_time datetime;

--rollback ALTER TABLE recharge_plans ADD COLUMN created_time datetime;

