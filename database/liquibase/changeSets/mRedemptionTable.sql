-- liquibase formatted sql

-- changeset arun:mRedemptionTable

CREATE TABLE `m_coupon` (
  `redemption_id` int(10) NOT NULL AUTO_INCREMENT,
  `coupon_code` varchar(64) NOT NULL,
  `fk_coupon_store_id` int(11) NOT NULL,
  `merchant_id` varchar(128) NOT NULL,
  `redeemption_state` varchar(64) NOT NULL,
  `redeemed_request_id` varchar(128) DEFAULT NULL,
  `redeemed_location` varchar(128) DEFAULT NULL,
  `redeemed_city` varchar(128) DEFAULT NULL,
  `redeemed_state` varchar(128) DEFAULT NULL,
  `metadata` varchar(256) DEFAULT NULL,
  `updated_ts` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creation_ts` timestamp NULL, 
  `redemption_ts` timestamp NULL,
  `expiration_ts` timestamp NULL, 
  PRIMARY KEY (`redemption_id`),
  KEY `idx_m_coupon_code` (`coupon_code`, `merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE coupon_store ADD COLUMN is_m_coupon TINYINT(1) DEFAULT 0;
ALTER TABLE coupon_store ADD COLUMN short_terms_conditions text DEFAULT NULL;
ALTER TABLE coupon_merchant ADD COLUMN merchant_reference varchar(128) DEFAULT NULL;

-- rollback DROP TABLE IF EXISTS `m_coupon`;
-- rollback ALTER TABLE coupon_store DROP is_m_coupon;
-- rollback ALTER TABLE coupon_store DROP short_terms_conditions;
-- rollback ALTER TABLE coupon_merchant DROP merchant_reference;
