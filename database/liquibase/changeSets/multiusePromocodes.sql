-- liquibase formatted sql

-- changeset ujjwala:multiusePromocodes

-- Entity changes related to multi use promocodes

alter table promocode add column max_redemptions int(11) null default null;

drop table if exists multiuse_promocode;

create table multiuse_promocode (
id int(11) auto_increment primary key,
promocode varchar(128),
user_id int(11),
campaign_id int(11) null default null,
order_id varchar(20) null default null,
status varchar(10),
used_date timestamp,
created_date timestamp,
updated_at timestamp,
foreign key (user_id) references users(user_id),
foreign key (campaign_id) references freefund_class(id),
key idx_order_id (order_id),
key idx_promocode (promocode))
ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollBack alter table promocode drop column max_redemptions;

-- rollback drop table multiuse_promocode;