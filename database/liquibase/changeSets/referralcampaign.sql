-- liquibase formatted sql

-- changeset amaresh:referralcampaign

create table referral_campaign(
  sender_user_id int(11) not null,
  provider varchar(20),
  sender_provider_id varchar(128),
  recipient_provider_id varchar(128),
  referral_campaign_id varchar(100),
  sender_channel_id varchar(30),
  last_updated timestamp,
  notification_sent_time timestamp,
  primary key (sender_provider_id, recipient_provider_id));

-- rollback drop table referral_campaign