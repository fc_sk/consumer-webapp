-- liquibase formatted sql

-- changeset shwetanka:arActivityThread

alter table ar_form_activity add column thread_id int(11) default null after lookup_id;
alter table ar_promo_opt_in_activity add column thread_id int(11) default null after lookup_id;
alter table ar_voucher_opt_in_activity add column thread_id int(11) default null after lookup_id;
alter table ar_url_activity add column thread_id int(11) default null after lookup_id;
alter table ar_social_share_activity add column thread_id int(11) default null after object_id;

create index ar_form_thread_id on ar_form_activity (thread_id);
create index ar_promo_thread_id on ar_promo_opt_in_activity (thread_id);
create index ar_voucher_thread_id on ar_voucher_opt_in_activity (thread_id);
create index ar_url_thread_id on ar_url_activity (thread_id);
create index ar_social_thread_id on ar_social_share_activity (thread_id);

-- rollback alter table ar_form_activity drop column thread_id;
-- rollback alter table ar_promo_opt_in_activity drop column thread_id;
-- rollback alter table ar_voucher_opt_in_activity drop column thread_id;
-- rollback alter table ar_url_activity drop column thread_id;
-- rollback alter table ar_social_share_activity drop column thread_id;

