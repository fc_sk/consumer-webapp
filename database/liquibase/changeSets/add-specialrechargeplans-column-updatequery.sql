-- liquibase formatted sql

-- changeset vipin:add-specialrechargeplans-column-updatequery

alter table in_request add column recharge_plan varchar(50); 

alter table in_aggr_opr_circle_map add column recharge_plan_type varchar(50);

update in_aggr_opr_circle_map set recharge_plan_type='SPECIAL' where fk_operator_id=43 and aggr_name='euronet';

update in_aggr_opr_circle_map set recharge_plan_type='SPECIAL' where fk_operator_id=30 and aggr_name='euronet';

update in_aggr_opr_circle_map set recharge_plan_type='SPECIAL' where fk_operator_id=24 and aggr_name='euronet';

update in_aggr_opr_circle_map set recharge_plan_type='SPECIAL' where fk_operator_id=13 and aggr_name='euronet';

update in_aggr_opr_circle_map set recharge_plan_type='SPECIAL' where fk_operator_id=5 and aggr_name='euronet';

update in_aggr_opr_circle_map set recharge_plan_type='RECHARGE' where fk_operator_id=3 and aggr_name='euronet';

update in_aggr_opr_circle_map set recharge_plan_type='RECHARGE' where fk_operator_id=9 and aggr_name='euronet';

update in_aggr_opr_circle_map set recharge_plan_type='ValBSNL' where fk_operator_id=3 and aggr_name='oxigen';

update in_aggr_opr_circle_map set recharge_plan_type='ValMTNL' where fk_operator_id=9 and aggr_name='oxigen';

update in_aggr_opr_circle_map set recharge_plan_type='ETOPSPDOCOMO' where fk_operator_id=5 and aggr_name='oxigen';

update in_aggr_opr_circle_map set recharge_plan_type='ETOPT24' where fk_operator_id=43 and aggr_name='oxigen';

update in_aggr_opr_circle_map set recharge_plan_type='ETOPSPUNIN' where fk_operator_id=13 and aggr_name='oxigen';

update in_aggr_opr_circle_map set recharge_plan_type='ETOPSPVIDGSM' where fk_operator_id=24 and aggr_name='oxigen';



-- rollBack alter table in_aggr_opr_circle_map drop column recharge_plan_type;

-- rollBack alter table in_request drop column recharge_plan;



