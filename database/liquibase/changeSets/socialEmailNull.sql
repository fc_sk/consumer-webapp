-- liquibase formatted sql

-- changeset shwetanka:socialEmailNull

alter table social_user change column email email varchar(150) default null;

-- rollback alter table social_user change column email email varchar(150) not null;