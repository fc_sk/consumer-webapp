--liquibase formatted sql

--changeset amaresh:freefundchannelcondition

update ar_condition_type set description='channels=Web or Mobile or App or IOS or Windows or any combination of these. like channels:{"Mobile","App"}(Case independent)' where class_ident='freefundchannelcondition';

--rollback update ar_condition_type set description='channels=Web or Mobile or App or any combination of these. like channels:{"Mobile","App"}(Case independent)' where class_ident='freefundchannelcondition';