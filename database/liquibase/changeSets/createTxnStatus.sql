--liquibase formatted sql

-- changeset abhishek:createTxnStatus

CREATE TABLE IF NOT EXISTS transaction_status (
	status_id BIGINT(20) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	status VARCHAR(64) NOT NULL,
	description VARCHAR(100) NOT NULL,
	index `index_status` (`status`)
);

INSERT INTO transaction_status (status, description) VALUES ('01','Payment Initiated');
INSERT INTO transaction_status (status, description) VALUES ('02','Payment redirected to bank');
INSERT INTO transaction_status (status, description) VALUES ('03','Payment response received from bank');
INSERT INTO transaction_status (status, description) VALUES ('04','Payment successful');
INSERT INTO transaction_status (status, description) VALUES ('05','Payment failed');
-- rollback DROP TABLE transaction_status;