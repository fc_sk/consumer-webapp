-- liquibase formatted sql

-- changeset kopaul:createRequestLookupMap

CREATE TABLE IF NOT EXISTS request_lookup_map
(
   merchant_id int(5) NOT NULL,
   request_id varchar(64) NOT NULL,
   lookup_id varchar(64) NOT NULL,
   n_last_updated datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   n_created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`merchant_id` , `request_id`)
) ENGINE=InnoDB CHARSET=utf8;

-- rollback DROP TABLE request_lookup_map;
