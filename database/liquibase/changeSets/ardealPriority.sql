-- liquibase formatted sql

-- changeset shwetanka:ardealPriority

alter table ar_deal add column priority mediumint (8) default null after theme;

-- rollback alter table ar_deal drop column priority;