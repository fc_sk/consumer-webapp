-- liquibase formatted sql

-- changeset Narasimha Murthi:insertPaymentOptionMaster.sql

INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('1', 'NB', 'SBI', 'SBIN', 'statbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('2', 'NB', 'HDFC Bank', 'HDFC', 'hdfcbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('3', 'NB', 'ICICI Bank', 'ICIC', 'icicibank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('4', 'NB', 'CITI Bank', 'CITA', 'citibank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('5', 'NB', 'AXIS Bank', 'AXIS', 'axisbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('6', 'NB', 'Punjab National Bank', 'PJRB', 'punjabNationalBank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('7', 'NB', 'KOTAK Bank', 'KOBK', 'kotakbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('8', 'NB', 'YES Bank', 'YEBK', 'statbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('9', 'NB', 'Bank of India', 'BOI', 'bankofindia.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('10', 'NB', 'Indian Overseas Bank', 'IOB', 'indianoverseasbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('11', 'NB', 'Andhra Bank', 'ANDB', 'andharabank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('12', 'NB', 'Allahabad Bank', 'ALLB', 'allahabadbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('13', 'NB', 'Bank of Bahrain and Kuwait', 'BAHK', 'bahrainbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('14', 'NB', 'Bank of Baroda - Coporate', 'BOBC', 'barodacorporate.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('15', 'NB', 'Bank of Baroda - Retail', 'BOBR', 'barodaretail.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('16', 'NB', 'Bank of Maharastra', 'MAHB', 'maharashtrabank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('17', 'NB', 'Canara Bank', 'CANB', 'canarabank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('18', 'NB', 'City Union Bank', 'CITU', 'cityunion.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('19', 'NB', 'Corporation Bank', 'CORB', 'corporationbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('20', 'NB', 'Catholic Syrian Bank', 'CSYB', 'syrianbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('21', 'NB', 'Cosmos Bank', 'COB', '');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('22', 'NB', 'Dena bank', 'DENB', 'denabank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('23', 'NB', 'Deutche Bank', 'DEUB', 'deutschebank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('24', 'NB', 'Development Credit Bank', 'DECB', 'developmentbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('25', 'NB', 'Federal Bank', 'FEDB', 'federalbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('26', 'NB', 'Indian Bank', 'INDB', 'indianbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('27', 'NB', 'IDBI Bank', 'IDBI', 'idbibank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('28', 'NB', 'IndusInd Bank', 'INIB', 'indusbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('29', 'NB', 'ING Vyasa Bank', 'INVB', 'ingbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('30', 'NB', 'Jammu and Kashmir Bank', 'JNKB', 'jnkbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('31', 'NB', 'Karnataka Bank Ltd', 'KRTB', 'karnatakabank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('32', 'NB', 'Karur Vyasa Bank', 'KARB', 'karurbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('33', 'NB', 'Laxmi Vilas Bank - Corporate Net Banking', 'LXCB', 'laxmicorporate.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('34', 'NB', 'Laxmi Vilas Bank - Retail Net Banking', 'LXRB', 'laxmiretail.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('35', 'NB', 'Oriental Bank of Commerce', 'ORTB', 'orientalbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('36', 'NB', 'Punjab and Sind Bank', 'PJSB', 'punjabsindh.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('37', 'NB', 'Punjab National Bank - Retail Banking', 'PJRB', 'punjabretail.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('38', 'NB', 'Ratnakar Bank', 'RATB', 'ratnakarbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('39', 'NB', 'Shamrao Vittal Cooperative Bank', 'SHVB', 'shamraobank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('40', 'NB', 'Saraswat Bank', 'SARB', '');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('41', 'NB', 'South Indian Bank', 'SINB', 'southbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('42', 'NB', 'Standard Chartered Bank', 'STCB', 'stancbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('43', 'NB', 'State Bank of Bikaner and Jaipur', 'SBJB', 'stbikaner.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('44', 'NB', 'State Bank of Hyderabad', 'SHYB', 'hyderabadbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('45', 'NB', 'State Bank of Mysore', 'SMYB', 'mysorebank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('46', 'NB', 'State Bank of Patiala', 'SPTB', 'patialabank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('47', 'NB', 'State Bank of Travancore', 'STVB', 'travancorebank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('48', 'NB', 'Syndicate Bank', 'SYNB', 'syndicatebank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('49', 'NB', 'Tamilnadu Mercentile Bank', 'TMEB', 'tamilbank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('50', 'NB', 'Union Bank of India', 'UBI', 'unionbankofindia.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('51', 'NB', 'United Bank of India', 'UNIB', 'unitedbankofindia.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('52', 'NB', 'Vijaya Bank', 'VIJB', 'vijayabank.png');
INSERT INTO payment_option_master (id, payment_type, payment_option_display_name, payment_option_code, image_url) VALUES('53', 'NB', 'UCO Bank', 'UCOB', 'ucobank.png');
	   
-- rollback ;