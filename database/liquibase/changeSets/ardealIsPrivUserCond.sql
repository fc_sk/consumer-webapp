-- liquibase formatted sql

-- changeset ashwini:ardealIsPrivUserCond

alter table ar_deal add column is_privilege_user_cond boolean default false;

-- rollback alter table ar_deal drop column is_privilege_user_cond;
