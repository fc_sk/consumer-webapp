-- liquibase formatted sql

-- changeset shwetanka:paymentTypeARCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('PaymentTypeARCondition', 'paymentTypeARCondition', now(), now(), 'Params accepted is - paymentTypes = 1(Credit Card), 2(Debit Card), 3(Net Banking), 4(Cash Card), 5(ATM Card), 6(FC Credits), 7(HDFC Debit Card), 8(Zero Pay)');

-- rollback delete from ar_condition_type where class_ident = 'paymentTypeARCondition';