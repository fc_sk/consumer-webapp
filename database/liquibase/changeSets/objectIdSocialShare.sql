-- liquibase formatted sql

-- changeset shwetanka:objectIdSocialShare

alter table ar_social_share_activity add column object_id varchar(150) default null after session_id;

-- rollback alter table ar_social_share_activity drop column object_id;