-- liquibase formatted sql

-- changeset anderavi:alterOptInActivityTables

alter table ar_voucher_opt_in_activity add column lookup_id varchar(36) NULL DEFAULT NULL after session_id ;
alter table ar_promo_opt_in_activity add column lookup_id varchar(36) NULL DEFAULT NULL after session_id ;

-- rollback ALTER TABLE ar_voucher_opt_in_activity DROP COLUMN lookup_id;
-- rollback ALTER TABLE ar_promo_opt_in_activity DROP COLUMN lookup_id;