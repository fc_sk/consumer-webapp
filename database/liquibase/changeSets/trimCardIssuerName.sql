-- liquibase formatted sql

-- changeset abhi:trimCardIssuerName

UPDATE card_issuer set card_issuer_name = TRIM(card_issuer_name);