-- liquibase formatted sql

-- changeset priyatb:product-seperation

insert into ar_condition_type (name, class_ident, description) values ('MinRechargePrepaidCondition', 'minRechargePrepaidCondition', 'Params required are: minRechargeAmount');

insert into ar_condition_type (name, class_ident, description) values ('MinRechargePostpaidCondition', 'minRechargePostpaidCondition', 'Params required are: minRechargeAmount');

--rollback
--delete from ar_condition_type where name = 'MinRechargePrepaidCondition';
--delete from ar_condition_type where name = 'MinRechargePostpaidCondition';