-- liquibase formatted sql

-- changeset Vipin Goyal:add-city-sirsi-karnataka

 CREATE TABLE `aggregator_preference` (
  `operator_id` int(2) DEFAULT NULL,
  `operator_name` varchar(30) DEFAULT NULL,
  `ag_euronet` varchar(20) DEFAULT 0,
  `ag_oxigen` varchar(20) DEFAULT 0,
  `aggregator_txn_ratio` varchar(40) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `is_enabled` smallint(6) DEFAULT 0,
  UNIQUE KEY `operator_id` (`operator_id`));
  
insert into app_config values('ag.preference.enabled',0,null);

insert into aggregator_preference values(1,'Aircel',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(2,'Airtel',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(3,'BSNL',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(4,'Dish Tv',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(5,'Tata Docomo GSM',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(6,'Idea',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(7,'Indicom-Walky',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(8,'Loop',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(9,'MTNL Delhi',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(10,'Reliance-CDMA',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(11,'Reliance-GSM',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(12,'Tata Indicom',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(13,'Uninor',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(14,'Vodafone',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);

insert into aggregator_preference values(16,'Big Tv',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(17,'Airtel Tv',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(18,'Sun Tv',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(19,'Tata Sky',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(20,'Videocon Tv',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(21,'Tata Photon Plus',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(22,'MTS',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(23,'Reliance Netconnect',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(24,'Videocon Mobile',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);

insert into aggregator_preference values(30,'Virgin GSM',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(31,'Virgin CDMA',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);

insert into aggregator_preference values(40,'MTS MBrowse',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(41,'MTS Mblaze',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(42,'Tata Docomo CDMA',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(43,'T24',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(44,'Airtel DataCard',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(45,'BSNL DataCard',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(46,'MTNL DataCard Delhi',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(47,'Vodafone DataCard',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(48,'Aircel DataCard',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(49,'Idea DataCard',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(50,'Reliance-GSM DataCard',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(51,'Tata Indicom DataCard',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(52,'Tata Docomo GSM DataCard',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(53,'T24 DataCard',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(54,'MTNL Mumbai',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);
insert into aggregator_preference values(55,'MTNL Datacard Mumbai',0,0,'oxigen-0~euronet-0','2014-11-19 14:00:00',0);


-- rollback drop table aggregator_preference;

-- rollback delete from app_config where app_key='ag.preference.enabled';
