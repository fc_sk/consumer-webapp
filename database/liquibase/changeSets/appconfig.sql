-- liquibase formatted sql

-- changeset shwetanka:appconfig

create table app_config (
  app_key varchar (100) unique not null,
  app_value varchar (255) null,
  primary key (`app_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- rollback drop table app_config;