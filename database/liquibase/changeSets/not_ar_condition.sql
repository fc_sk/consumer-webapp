-- liquibase formatted sql

-- changeset priyatb:not_condition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('NotARCondition', 'notOperatorARCondition', now(), now(), 'Params accepted are -  Condition id as text');

-- rollback delete from ar_condition_type where class_ident = 'notOperatorARCondition';