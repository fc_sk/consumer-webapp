-- liquibase formatted sql

-- changeset arun:bpAlter

alter table bill_payment_status change column affiliate_trans_id affiliate_trans_id  varchar(64);

-- rollback alter table bill_payment_status change column affiliate_trans_id affiliate_trans_id varchar(50);
