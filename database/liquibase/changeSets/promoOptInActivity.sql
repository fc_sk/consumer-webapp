-- liquibase formatted sql

-- changeset anderavi:promoOptInActivity

CREATE TABLE ar_promo_opt_in_activity (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_ar_action_id` int(11) NOT NULL,
  `fk_user_id` int(11) NULL DEFAULT NULL,
  `session_id` varchar(36) NULL DEFAULT NULL, 
  `activity` varchar(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollback DROP TABLE ar_promo_opt_in_activity;
