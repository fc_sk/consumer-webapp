-- liquibase formatted sql

-- changeset sayak:add-postpaid-merchantcode-billdesk

UPDATE bill_postpaid_merchants SET billdesk_merchant_id = "AIRMOBOB" WHERE fk_operator_master_id = 56;
UPDATE bill_postpaid_merchants SET billdesk_merchant_id = "BSNLMOB" WHERE fk_operator_master_id = 58;
UPDATE bill_postpaid_merchants SET billdesk_merchant_id = "DOCOMOOB" WHERE fk_operator_master_id = 59;
UPDATE bill_postpaid_merchants SET billdesk_merchant_id = "IDEAOB" WHERE fk_operator_master_id = 60;
UPDATE bill_postpaid_merchants SET billdesk_merchant_id = "TTSLOB" WHERE fk_operator_master_id = 61;
UPDATE bill_postpaid_merchants SET billdesk_merchant_id = "VODAOB" WHERE fk_operator_master_id = 62;
UPDATE bill_postpaid_merchants SET billdesk_merchant_id = "AIRCELOB" WHERE fk_operator_master_id = 65;


