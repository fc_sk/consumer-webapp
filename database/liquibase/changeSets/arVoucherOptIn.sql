-- liquibase formatted sql

-- changeset chetanvaity:arVoucherOptIn
DROP TABLE IF EXISTS ar_voucher_code;
CREATE TABLE ar_voucher_code (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_ar_action_id` int(11) NOT NULL,
  `voucher_code` varchar(36) NOT NULL, 
  `status` tinyint(1) DEFAULT 0,
  `fk_user_id` int(11) NULL DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT 0,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE ar_deal ADD COLUMN params TEXT NULL AFTER theme;
  
CREATE INDEX ar_voucher_code_fk_ar_action_id_idx ON ar_voucher_code (fk_ar_action_id);
CREATE INDEX ar_voucher_code_fk_user_id_idx ON ar_voucher_code (fk_user_id);

CREATE INDEX ar_voucher_opt_in_activity_fk_ar_action_id_idx ON ar_voucher_opt_in_activity (fk_ar_action_id);
CREATE INDEX ar_voucher_opt_in_activity_fk_user_id_idx ON ar_voucher_opt_in_activity (fk_user_id);
CREATE INDEX ar_voucher_opt_in_activity_session_id_idx ON ar_voucher_opt_in_activity (session_id);

INSERT INTO ar_action_type SET name='VoucherOptIn', class_ident='voucherOptInARAction', created_on=NOW();
SET @voiActionTypeId = LAST_INSERT_ID();
INSERT INTO ar_reward_type SET name='Empty', class_ident='emptyARReward', created_on=NOW();
SET @emptyRewardTypeId = LAST_INSERT_ID();
INSERT INTO ar_condition_type SET name='Number of times shown', class_ident='numTimesARCondition', created_on=NOW();
SET @numTimesConditionTypeId = LAST_INSERT_ID();
INSERT INTO ar_condition_type SET name='Registered since', class_ident='registeredSinceARCondition', created_on=NOW();
SET @regSinceConditionTypeId = LAST_INSERT_ID();
INSERT INTO ar_condition_type SET name='Number of times action done', class_ident='numTimesActionDoneARCondition', created_on=NOW();
SET @numTimesActionDoneConditionTypeId = LAST_INSERT_ID();
INSERT INTO ar_condition_type SET name='OR operator', class_ident='orOperatorARCondition', created_on=NOW();
SET @orOperartorConditionTypeId = LAST_INSERT_ID();
INSERT INTO ar_condition_type SET name='AND operator', class_ident='andOperatorARCondition', created_on=NOW();
SET @andOperatorConditionTypeId = LAST_INSERT_ID();
INSERT INTO ar_condition_type SET name='Dummy True', class_ident='dummyTrueARCondition', created_on=NOW();
INSERT INTO ar_condition_type SET name='Dummy False', class_ident='dummyFalseARCondition', created_on=NOW();

-- rollback DROP TABLE ar_voucher_code;
-- rollback ALTER TABLE ar_deal DROP COLUMN params; 
-- rollback DROP INDEX ar_voucher_opt_in_activity_fk_ar_action_id_idx ON ar_voucher_opt_in_activity;
-- rollback DROP INDEX ar_voucher_opt_in_activity_fk_user_id_idx ON ar_voucher_opt_in_activity;
-- rollback DROP INDEX ar_voucher_opt_in_activity_session_id_idx ON ar_voucher_opt_in_activity;
-- rollback DELETE FROM ar_action_type;
-- rollback DELETE FROM ar_reward_type;
-- rollback DELETE FROM ar_condition_type;

