-- liquibase formatted sql

-- changeset kopaul:recharge_retry_data


ALTER TABLE recharge_retry_data MODIFY aggr_name varchar(64) DEFAULT NULL;

CREATE INDEX `idx_recharge_retry_data_order_id` ON recharge_retry_data (order_id) USING BTREE;

-- rollback DROP TABLE recharge_retry_data;
