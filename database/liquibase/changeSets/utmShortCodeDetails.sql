-- liquibase formatted sql

-- changeset amaresh.vijayakumar:utmShortcodeDetails

CREATE TABLE utm_shortcode_details (
utm_shortcode_id INT(11) AUTO_INCREMENT PRIMARY KEY,
utm_short_code varchar(50),
utm_campaign_id int(11),
start_date date ,
end_date date ,
web_url varchar(500),
mobweb_url varchar(500),
android_app_url varchar(500),
ios_app_url varchar(500),
windows_app_url varchar(500),
fc_freefund_class_id int(11),
fc_ardeal_id int(11),
created_at timestamp,
created_by varchar (100),
updated_at timestamp,
updated_by varchar (100)
);

-- rollback DROP TABLE utm_shortcode_details;