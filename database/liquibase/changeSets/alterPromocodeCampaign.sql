-- liquibase formatted sql

-- changeset raviande:alterPromocodeCampaign

ALTER TABLE promocode_campaign add column status varchar(10) NULL DEFAULT NULL;
 
-- rollback ALTER TABLE promocode_campaign DROP COLUMN status;
