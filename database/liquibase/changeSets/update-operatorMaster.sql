-- liquibase formatted sql

-- changeset Rajani:update-operator_master table to make loop mobile inactive

update operator_master set is_active=0 where operator_master_id=8;
update operator_master set is_active=0 where operator_master_id=57;