-- liquibase formatted sql

-- changeset vivek:alterCouponQualityData

alter table coupon_quality_data modify no_of_pick bigint(11) default 0;

-- rollback alter table coupon_quality_data modify no_of_pick bigint(11) NOT NULL;