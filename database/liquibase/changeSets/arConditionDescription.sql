-- liquibase formatted sql

-- changeset shwetanka:arConditionDescription

alter table ar_condition_type add column description text default null;

update ar_condition_type set description = 'Params required are: numTimes' where class_ident = 'numTimesARCondition';
update ar_condition_type set description = 'Params required are: conditionIds' where class_ident = 'andOperatorARCondition';
update ar_condition_type set description = 'Params required are: cities (Pipe separated list)' where class_ident = 'cityARCondition';
update ar_condition_type set description = 'Params required are: gender' where class_ident = 'genderARCondition';
update ar_condition_type set description = 'Params required are: numTimes' where class_ident = 'numTimesActionDoneARCondition';
update ar_condition_type set description = 'Params required are: operator' where class_ident = 'operatorARCondition';
update ar_condition_type set description = 'Params required are: conditionIds' where class_ident = 'orOperatorARCondition';
update ar_condition_type set description = 'Params required are: minRechargeAmount and products (comma seperated list - V=1, D=2, C=3)' where class_ident = 'productARCondition';
update ar_condition_type set description = 'Params required are: minDays' where class_ident = 'registeredSinceARCondition';
update ar_condition_type set description = 'Params required are: direction (smaller, greater, equals) and age' where class_ident = 'ageARCondition';

-- rollback alter table ar_condition_type drop column description;
