-- liquibase formatted sql

-- changeset Vipin:billPayment

CREATE TABLE `prepaid_postpaid_operator_mapping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `prepaid_operator_master_id` int(10) DEFAULT NULL,
  `postpaid_operator_master_id` int(10) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `prepaid_operator_master_id` (`prepaid_operator_master_id`));

INSERT INTO `prepaid_postpaid_operator_mapping` VALUES (1,2,56,1),(2,8,57,1),(3,3,58,1),(4,5,59,1),(5,6,60,1),(6,12,61,1),(7,14,62,1),(8,10,63,1),(9,11,63,1);

-- rollback DROP TABLE IF EXISTS prepaid_postpaid_operator_mapping;

