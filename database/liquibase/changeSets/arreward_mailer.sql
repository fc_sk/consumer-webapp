--liquibase formatted sql
--changeset priyatb:arreward_mailer

alter table ar_reward add email_template text;
alter table ar_reward add email_subject varchar(200);

--rollback alter table ar_reward drop column email_template;
--rollback alter table ar_reward drop column email_subject;