-- liquibase formatted sql

-- changeset ujjwala.srivastava:kpayPaymentMeta

create table if not exists kpay_payment_meta (
id int(11) auto_increment primary key,
mtxn_id varchar(20) not null,
pg_used varchar(20) not null,
created_on timestamp default current_timestamp,
key idx_mtxn_id (mtxn_id),
key idx_pg_used (pg_used)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollback DROP TABLE IF EXISTS kpay_payment_meta;