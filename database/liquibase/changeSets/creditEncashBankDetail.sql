-- liquibase formatted sql

-- changeset ujjwala:fcCreditEncashBankDetail

create table if not exists fc_credit_encash_bank_detail (
id int(11) auto_increment primary key,
user_id int(11) not null,
bank_account_no varchar(50) not null,
ifsc_code varchar(50) not null,
beneficiary_name varchar(250) not null,
email_id varchar(250) not null,
key idx_user_id (user_id),
key idx_bank_account_no (bank_account_no)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

