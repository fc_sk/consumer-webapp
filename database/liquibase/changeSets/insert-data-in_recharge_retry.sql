-- liquibase formatted sql

-- changeset amitgoel:insert-data-in_recharge_retry

INSERT INTO in_recharge_retry (id,aggr_name,fk_operator_master_id,aggr_resp_code,is_retryable,fk_retry_operator_master_id,retry_priority) VALUES (1,'euronet',14,'ECI',1,62,'1');
INSERT INTO in_recharge_retry (id,aggr_name,fk_operator_master_id,aggr_resp_code,is_retryable,fk_retry_operator_master_id,retry_priority) VALUES (2,'euronet',14,'CI',1,62,'1');
INSERT INTO in_recharge_retry (id,aggr_name,fk_operator_master_id,aggr_resp_code,is_retryable,fk_retry_operator_master_id,retry_priority) VALUES (3,'euronet',14,'ECI',1,2,'2');
INSERT INTO in_recharge_retry (id,aggr_name,fk_operator_master_id,aggr_resp_code,is_retryable,fk_retry_operator_master_id,retry_priority) VALUES (4,'euronet',14,'CI',1,2,'2');
INSERT INTO in_recharge_retry (id,aggr_name,fk_operator_master_id,aggr_resp_code,is_retryable,fk_retry_operator_master_id,retry_priority) VALUES (5,'euronet',14,'ECI',1,3,'3');
INSERT INTO in_recharge_retry (id,aggr_name,fk_operator_master_id,aggr_resp_code,is_retryable,fk_retry_operator_master_id,retry_priority) VALUES (6,'euronet',14,'CI',1,3,'3');

-- rollback DELETE FROM in_recharge_retry WHERE id in (1,2,3,4,5,6)
