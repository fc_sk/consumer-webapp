-- liquibase formatted sql

-- changeset shwetanka:dealClients

create table deal_clients (
  id int (11) unique not null auto_increment,
  name varchar (50) not null,
  deal_id int(11) not null,
  created_on timestamp not null default current_timestamp,
  primary key (id)
) engine InnoDB DEFAULT CHARSET=utf8;


-- rollback drop table deal_clients;
