-- liquibase formatted sql

-- changeset priyanalang:arReferralCashbackReward
insert into ar_reward_type(name, class_ident, created_on) values('Referral Cashback Reward', 'referralCashbackARReward', now());
-- rollback DELETE FROM ar_reward_type where class_ident='referralCashbackARReward';