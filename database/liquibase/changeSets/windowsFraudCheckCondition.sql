-- liquibase formatted sql

-- changeset amaresh:windowsFraudCheckCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('WindowsFraudCheckCondition', 'WindowsFraudCheckCondition', now(), now(), 'WORKS FOR WINDOWS APP ONLY. Checks if max redemption per deviceUniqueId has been reached. Params : maxRedemptionsPerUniqueIdCount- -1....n(-1 is unlimited)');

-- rollback delete from ar_condition_type where class_ident = 'WindowsFraudCheckCondition';