-- liquibase formatted sql

-- changeset shirish:alterCouponOptinHistory_cid
alter table coupon_optin_history add coupon_store_id int(11); 
alter table coupon_optin_history add voucher_cat_id int(11); 
alter table crosssell_optin_history add coupon_store_id int(11); 
alter table crosssell_optin_history add voucher_cat_id int(11);

CREATE INDEX idx_coupon_optin_history_coupon_store_id ON coupon_optin_history(coupon_store_id) USING BTREE;
CREATE INDEX idx_crosssell_optin_history_coupon_store_id ON crosssell_optin_history(coupon_store_id) USING BTREE;

CREATE INDEX idx_coupon_optin_history_voucher_cat_id ON coupon_optin_history(voucher_cat_id) USING BTREE;
CREATE INDEX idx_crosssell_optin_history_voucher_cat_id ON crosssell_optin_history(voucher_cat_id) USING BTREE;

-- rollback alter table coupon_optin_history drop column coupon_store_id;
-- rollback alter table coupon_optin_history drop column voucher_cat_id; 
-- rollback alter table crosssell_optin_history drop column coupon_store_id;
-- rollback alter table crosssell_optin_history drop column voucher_cat_id;   
