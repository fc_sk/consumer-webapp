-- liquibase formatted sql

-- changeset shirish:rechargeRetry
create table recharge_retry_config (
id int(3) unsigned NOT NULL AUTO_INCREMENT,
error_code varchar(10), 
max_retry_count tinyint,
retry_interval int(4) unsigned,
max_retry_duration int(4) unsigned,
is_active tinyint(1),
primary key (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


create table recharge_retry_status (
id int(11) unsigned NOT NULL AUTO_INCREMENT,
order_id varchar(20),
error_code varchar(10),
retry_count_remaining int(1),
is_retry_complete tinyint(1),
created_on datetime,
updated_on  timestamp default current_timestamp on update current_timestamp,
primary key (id, created_on)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
PARTITION BY list (month(created_on))(
  PARTITION bt_p1 VALUES IN (1),
  PARTITION bt_p2 VALUES IN (2),
  PARTITION bt_p3 VALUES IN (3),
  PARTITION bt_p4 VALUES IN (4),
  PARTITION bt_p5 VALUES IN (5),
  PARTITION bt_p6 VALUES IN (6),
  PARTITION bt_p7 VALUES IN (7),
  PARTITION bt_p8 VALUES IN (8),
  PARTITION bt_p9 VALUES IN (9),
  PARTITION bt_p10 VALUES IN (10),
  PARTITION bt_p11 VALUES IN (11),
  PARTITION bt_p12 VALUES IN (12)
);

insert into app_config values('recharge.retry.enabled',0,null);

CREATE INDEX idx_recharge_retry_status_order_id ON recharge_retry_status(order_id) USING BTREE;

CREATE INDEX idx_recharge_retry_status_is_retry_complete ON recharge_retry_status(is_retry_complete) USING BTREE;

CREATE INDEX idx_recharge_retry_status_created_on ON recharge_retry_status(created_on) USING BTREE;

CREATE INDEX idx_recharge_retry_config_error_code ON recharge_retry_config(error_code) USING BTREE;

-- rollback DROP TABLE IF EXISTS recharge_retry_config;
-- rollback DROP TABLE IF EXISTS recharge_retry_status;