-- liquibase formatted sql

-- changeset shirish:alterCouponHistoryFulfilmentType

alter table coupon_optin_history add optin_type varchar(20) not null default "optin"; 

CREATE INDEX idx_coupon_optin_history_fulfilment_type ON coupon_optin_history(optin_type) USING BTREE;

-- rollback alter table coupon_optin_history drop column optin_type