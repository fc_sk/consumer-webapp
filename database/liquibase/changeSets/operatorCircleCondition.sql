-- liquibase formatted sql

-- changeset ashwini:operatorCircleCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('operator circle condition ', 'operatorCircleCondition', now(), now(), 'operator circle condition, param:operatorCircleList');

