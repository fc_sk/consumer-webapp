-- liquibase formatted sql

-- changeset priyanalang:arMinSocialInvitesCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('MinSocialInvitesCondition', 'minSocialInvitesCondition', now(), now(), 'Params required are: numTimes-min invitations to be sent by user, referralCampaignId - ARdeal CampaignId');

-- rollback delete from ar_condition_type where class_ident = 'minSocialInvitesCondition';