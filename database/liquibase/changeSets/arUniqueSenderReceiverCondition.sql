-- liquibase formatted sql

-- changeset priyanalang:arUniqueSenderReceiverCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('UniqueSenderReceiverCondition', 'uniqueSenderReceiverCondition', now(), now(), 'No Params Required');

-- rollback delete from ar_condition_type where class_ident = 'uniqueSenderReceiverCondition';