-- liquibase formatted sql

-- changeset shirish:couponMission

create table coupon_mission (
id int(11) NOT NULL AUTO_INCREMENT,
coupon_store_id int(11) NOT NULL,
mission_id int(11) NOT NULL,
created_on timestamp default current_timestamp,
primary key (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE INDEX idx_coupon_mission_coupon_store_id ON coupon_mission(coupon_store_id) USING BTREE;

-- rollback DROP TABLE IF EXISTS coupon_mission;

