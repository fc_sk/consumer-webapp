-- liquibase formatted sql

-- changeset priyatb:viral_mint

 CREATE TABLE ar_viralmint_activity (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  action_id int(11) NOT NULL,
  activity varchar(20) NOT NULL,
  session_id varchar(40) DEFAULT NULL,
  lookup_id varchar(40) DEFAULT NULL,
  thread_id int(11) DEFAULT NULL,
  vm_id varchar(40) default null,
  created_on timestamp NOT NULL DEFAULT now(),
  PRIMARY KEY (id),
  KEY idx_ar_viralmint_activity_user_id (user_id),
  KEY idx_ar_viralmint_activity_action_id (action_id),
  KEY idx_ar_viralmint_activity_activity (activity),
  KEY ar_viralmint_thread_id (thread_id)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;


insert into ar_action_type (name, class_ident, created_on, updated_on) values ('VMARAction', 'vmARAction', now(), now());

-- rollback drop table ar_viralmint_activity;
-- rollback delete from ar_action_type where name = 'VMARAction' and class_ident='vmARAction';
