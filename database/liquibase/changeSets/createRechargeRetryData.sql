-- liquibase formatted sql

-- changeset amitgoel:recharge_retry_data

CREATE TABLE recharge_retry_data
(
   retry_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
   order_id varchar(64) NOT NULL,
   product_type varchar(50) NOT NULL,
   retry_Operator_Name varchar(64),
   final_retry bit,
   created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
   updated_at timestamp,
   aggr_trans_id varchar(64),
   opr_trans_id varchar(20),
   aggr_name varchar(20),
   aggr_resp_code varchar(20),
   aggr_resp_msg longtext,
   in_resp_code varchar(20),
   in_resp_msg longtext
);



-- rollback DROP TABLE recharge_retry_data;

 
 
 
 
