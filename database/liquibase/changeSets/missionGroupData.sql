-- liquibase formatted sql

-- changeset Ankur:gaptun missions

CREATE TABLE mission_group_data (
  id int(11) NOT NULL AUTO_INCREMENT,
  mission_group_id int(11) DEFAULT NULL,
  mission_id int(11) DEFAULT NULL,
  sequence int(4) NOT NULL,
  PRIMARY KEY (id)
) engine InnoDB;
