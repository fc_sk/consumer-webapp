-- liquibase formatted sql

-- changeset amaresh:cfAlter

alter table freefund_class add column isNewCampaign boolean default 0;

-- rollback alter table freefund_class drop column isNewCampaign;