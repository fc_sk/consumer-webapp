-- liquibase formatted sql

-- changeset shirish:couponFiltering

CREATE TABLE coupon_channel_filter (
id int(11) NOT NULL AUTO_INCREMENT,
coupon_store_id int(11) NOT NULL, 
channel_id int(2) NOT NULL,
primary key (id)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE INDEX idx_coupon_channel_filter_channel_id ON coupon_channel_filter(channel_id) USING BTREE;
CREATE INDEX idx_coupon_channel_filter_coupon_store_id ON coupon_channel_filter(coupon_store_id) USING BTREE;

-- rollback DROP TABLE coupon_channel_filter;
