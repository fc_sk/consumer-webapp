-- liquibase formatted sql

-- changeset shwetanka:arDayTimeCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('DayTimeARCondition', 'dayTimeARCondition', now(), now(), 'Params accepted are - start = [0-23] start hour of the day in 24 hour format, end = [0-23] end hour of the day in 24 hour format, days = list of week days [1-7] and 8 for all days.');

-- rollback delete from ar_condition_type where class_ident = 'dayTimeARCondition';