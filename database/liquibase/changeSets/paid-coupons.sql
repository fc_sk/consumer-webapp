--liquibase formatted sql

--changeset Shirish:paid-coupons

alter table coupon_store add column price int(11) default 0;

alter table coupon_optin_history add column price int(11) default null;

alter table coupon_optin_history add optin_type varchar(20) not null default "optin"; 

alter table coupon_optin_history add column metadata text default null;

CREATE INDEX idx_coupon_optin_history_fulfilment_type ON coupon_optin_history(optin_type) USING BTREE;

insert into product_master(product_master_id, product_name, img_url, fk_affiliate_profile_id, is_active, product_type, is_refundable) values(15, 'Price Coupon', NULL, 1, 1, 2, 1);

--rollback ALTER TABLE coupon_store DROP COLUMN price;
--rollback ALTER TABLE coupon_optin_history DROP COLUMN price;
--rollback ALTER TABLE coupon_optin_history DROP COLUMN optin_type;
--rollback delete from product_master where product_master_id = 15;

