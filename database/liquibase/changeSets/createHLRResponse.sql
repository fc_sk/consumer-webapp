-- liquibase formatted sql

-- changeset sayak:createHLRResponse

CREATE TABLE IF NOT EXISTS hlr_response (
id int(11) NOT NULL auto_increment,
request_id VARCHAR(128),
order_id VARCHAR(20) NOT NULL,
serviceNumber VARCHAR(12) NOT NULL,
hlr_service TINYINT NOT NULL,
input_operator_id INT(10),
input_circle_id INT(10),
hlr_operator VARCHAR(128),
hlr_circle VARCHAR(128),
status TINYINT,
reason VARCHAR(128),
request_time timestamp NOT NULL,
PRIMARY KEY(id),
index(order_id));