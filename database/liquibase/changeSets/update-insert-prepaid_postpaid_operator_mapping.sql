-- liquibase formatted sql

-- changeset Vipin:insert-prepaid_postpaid_operator_mapping

update prepaid_postpaid_operator_mapping set postpaid_operator_master_id=64 where id=9;

update bill_postpaid_merchants set fk_operator_master_id=64 where postpaid_merchant_id=9;

insert into operator_master values(64,'Reliance Postpaid','Reliance Postpaid',1,1,201,'images/operator-logos/reliance_mobile_gsm_service.png');

update operator_master set operator_code='Reliance Postpaid-CDMA',name='Reliance Postpaid-CDMA' where operator_master_id=63;

update operator_master set operator_code='Reliance Postpaid-GSM',name='Reliance Postpaid-GSM' where operator_master_id=64;

update operator_master set img_url='images/operator-logos/reliance_mobile.png' where operator_master_id=63;

-- rollback update prepaid_postpaid_operator_mapping set postpaid_operator_master_id=63 where id=9;

-- rollback update bill_postpaid_merchants set fk_operator_master_id=11 where postpaid_merchant_id=9;

-- rollback update bill_postpaid_merchants set fk_operator_master_id=11 where postpaid_merchant_id=9;

-- rollback delete from operator_master where operator_master_id=64;

-- rollback update operator_master set operator_code='Reliance Postpaid',name='Reliance Postpaid' where operator_master_id=63;

-- rollback update operator_master set img_url='images/operator-logos/reliance_mobile_gsm_service.png' where operator_master_id=63;