-- liquibase formatted sql

-- changeset divyansh:alter-couponStore-add-printable

 alter table coupon_store add column printable tinyint(1) NOT NULL DEFAULT '0';

-- rollback ALTER TABLE coupon_store DROP COLUMN printable;
