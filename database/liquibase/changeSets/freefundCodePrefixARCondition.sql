-- liquibase formatted sql

-- changeset ashwini:freefundCodePrefixARCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('Freefund code prefix match', 'freefundCodePrefixARCondition', now(), now(), 'freefundCode should start with TX');

