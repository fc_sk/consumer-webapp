-- liquibase formatted sql

-- changeset vivek:firstSuccessfullRechargePerEventCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('First Success Recharge Per Event', 'firstSuccessfullRechargePerEventCondition', now(), now(), '');

-- rollback delete from ar_condition_type where class_ident = 'firstSuccessfullRechargePerEventCondition';
