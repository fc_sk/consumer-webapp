--liquibase formatted sql

--changeset shantanu:binBasedProductEntry

insert into product_master (product_master_id, product_name, img_url, fk_affiliate_profile_id, is_active, product_type, is_refundable) values (11, 'BinOffer', null, 1, 1, 4, 0);

--rollback INSERT product_master
--rollback (product_master_id, product_name, img_url, fk_affiliate_profile_id, is_active, product_type, is_refundable)
--rollback VALUES (11, 'BinOffer', null, 1, 1, 4, 0);
