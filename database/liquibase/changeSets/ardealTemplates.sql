-- liquibase formatted sql

-- changeset shwetanka:ardealTemplates

create table ardeal_templates (
  id int (11) unique not null auto_increment,
  name varchar (25) not null,
  content text not null,
  created_on timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  primary key (id)
) engine InnoDB DEFAULT CHARSET=utf8;

alter table ar_deal add column fk_template_id int (11) default null;
alter table ar_deal change column theme theme varchar(20) default null;

-- rollback drop table ardeal_templates;
-- rollback alter table ar_deal drop column template_id;