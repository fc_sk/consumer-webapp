-- liquibase formatted sql

-- changeset amaresh:additionalReward

 alter table additional_reward drop validFrom, drop validUpto;
 alter table additional_reward drop column freefundClassId;

-- rollback alter table additional_reward add validFrom datetime, add validUpto datetime;alter table additional_reward add column freefundClassId int(11);