-- liquibase formatted sql

-- changeset ujjwala:alterFreefundClass6

alter table freefund_class add column affiliate_id varchar(200) NULL DEFAULT NULL;

-- rollback ALTER TABLE freefund_class DROP COLUMN affiliate_id;