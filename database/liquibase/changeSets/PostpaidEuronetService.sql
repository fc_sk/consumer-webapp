-- liquibase formatted sql

-- changeset Vipin:postpaidEuronetService


alter table bill_payment_gateway_priority add column fk_operator_id int(11);
alter table bill_payment_gateway_priority add UNIQUE (fk_operator_id);
alter table bill_postpaid_merchants add column euronet_merchant_id varchar(30);
update bill_postpaid_merchants set euronet_merchant_id='REL' where postpaid_merchant_id=8;
update bill_postpaid_merchants set euronet_merchant_id='REG' where postpaid_merchant_id=9;
update bill_postpaid_merchants set euronet_merchant_id='IDE' where postpaid_merchant_id=5;
alter table bill_payment_errorcode_map add column is_display_enabled tinyint(1);
alter table bill_payment_gateway_priority drop index bill_type;
truncate table bill_payment_gateway_priority;
alter table bill_payment_gateway_priority modify column bill_provider varchar(160);
insert into bill_payment_gateway_priority values(1,'postpaid','billPaymentOxigenClient,billPaymentEuronetClient',1,56);
insert into bill_payment_gateway_priority values(2,'postpaid','billPaymentOxigenClient,billPaymentEuronetClient',1,57);
insert into bill_payment_gateway_priority values(3,'postpaid','billPaymentOxigenClient,billPaymentEuronetClient',1,58);
insert into bill_payment_gateway_priority values(4,'postpaid','billPaymentOxigenClient,billPaymentEuronetClient',1,59);
insert into bill_payment_gateway_priority values(5,'postpaid','billPaymentOxigenClient,billPaymentEuronetClient',1,60);
insert into bill_payment_gateway_priority values(6,'postpaid','billPaymentOxigenClient,billPaymentEuronetClient',1,61);
insert into bill_payment_gateway_priority values(7,'postpaid','billPaymentOxigenClient,billPaymentEuronetClient',1,62);
insert into bill_payment_gateway_priority values(8,'postpaid','billPaymentOxigenClient,billPaymentEuronetClient',1,63);
insert into bill_payment_gateway_priority values(9,'postpaid','billPaymentOxigenClient,billPaymentEuronetClient',1,64);
 
INSERT INTO `bill_payment_errorcode_map` VALUES 
(2,'vipin','1','07','',0,0),(3,'euronet','00','00','Transaction Successful.',0,0),
(4,'euronet','AB','07','Amount temporarily barred for Recharge.',1,0),
(5,'euronet','AI','06','Invalid Amount.',0,0),
(6,'euronet','CB','07','Mobile No temporarily barred from Refill, please c',0,0),
(7,'euronet','CI','07','Invalid Mobile Number',0,0),
(8,'euronet','EAI','06','Invalid Amount',1,0),
(9,'euronet','EBO','07','Merchant Exceeded available Balance',1,0),
(10,'euronet','EBT','07','Transaction amount greater than available Merchant',1,0),
(11,'euronet','ECI','07','Invalid Mobile Number',1,0),
(12,'euronet','ECL','07','Customer exceeded daily limit',1,0),
(13,'euronet','ECP','07','Please change the password',1,0),
(14,'euronet','ECR','07','Commission rule is not set',1,0),
(15,'euronet','EDT','07','Duplicate Transaction ID received from Merchant',1,0),
(16,'euronet','EF','07','Successful Refill present in the system',1,0),
(17,'euronet','EG','07','Operator System General Error',1,0),
(18,'euronet','EI','07','Operator System Internal Error',1,0),
(19,'euronet','EIC','07','Invalid Channel sent by Merchant',1,0),
(20,'euronet','EIE','07','Euronet Internal Error',1,0),
(21,'euronet','EIM','07','Invalid Merchant Account sent by Merchant',1,0),
(22,'euronet','EIP','07','Invalid Payment Mode sent by Merchant',1,0),
(23,'euronet','EIU','07','Invalid User Name sent by Merchant',1,0),
(24,'euronet','EMI','07','Invalid Message sent by Merchant',1,0),
(25,'euronet','EML','07','Merchant exceeded daily limit',1,0),
(26,'euronet','ERC','07','Invalid Response code configuration',1,0),
(27,'euronet','ES','07','Operator System Not Available',1,0),
(28,'euronet','ESM','07','Service provider or Service not available for Merc',1,0),
(29,'euronet','ESP','07','Invalid Service Provider or Service Provider is no',1,0),
(30,'euronet','ESS','07','Invalid Sub Service Provider or Sub Service Provid',1,0),
(31,'euronet','ETC','07','Term and condition flag is off',1,0),
(32,'euronet','ETI','07','Transaction not found for the Transaction ID sent',1,0),
(33,'euronet','SC','07','Service Class Change not allowed, please contact O',1,0),
(34,'euronet','SI','07','Service Class Issue, please contact Operator',1,0),
(35,'euronet','UP','08','Transaction Under Process',0,0),
(36,'euronet','US','08','Transaction Status is Unknown',0,0),
(37,'euronet','E05','07','Euronet Internal Error',1,0),
(38,'euronet','E18','07','Invalid Merchant',1,0),
(39,'euronet','E19','07','Invalid Payment mode',1,0),
(40,'euronet','E20','07','Invalid Channel',1,0),
(41,'euronet','E21','07','Crossed Daily Limit for the day',1,0),
(42,'euronet','E22','07','Invalid User',1,0),
(43,'euronet','E23','07','Please change the password',1,0),
(44,'euronet','E24','07','Credit Balance over',1,0),
(45,'euronet','E25','07','Transaction amount bigger than available credit ba',1,0),
(46,'euronet','IN_SE','08','Socket Exception',0,0),
(47,'euronet','IN_CE','08','Connect Exception',0,0),
(48,'euronet','IN_CTE','08','Connection Time Out',0,0);

update bill_payment_errorcode_map set bill_payment_gateway='billPaymentEuronetClient' where bill_payment_gateway='euronet';

-- rollback DROP TABLE IF EXISTS `bill_transaction`, `bill_transaction_ledger`;
