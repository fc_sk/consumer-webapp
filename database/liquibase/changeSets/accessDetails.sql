--liquibase formatted sql

-- changeset narasimhamurthy:accessDetails

CREATE TABLE access_details (
  id int(11) NOT NULL AUTO_INCREMENT,
  access_key varchar(64) NOT NULL,
  api varchar(100) NOT NULL,
  is_active smallint(2) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY access_key_api (access_key,api)
);

-- rollback DROP TABLE access_details;