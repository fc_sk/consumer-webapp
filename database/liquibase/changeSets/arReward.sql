-- liquibase formatted sql

-- changeset priyatb:arreward-enum

alter table ar_reward add reward_process_type varchar(40);
create index promo_created_on on ar_promo_opt_in_activity (created_on);

-- rollback alter table ar_reward drop reward_process_type;
-- drop index promo_created_on on ar_promo_opt_in_activity;