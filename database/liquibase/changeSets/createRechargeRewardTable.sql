-- liquibase formatted sql

-- changeset skasam:createRechargeRewardTable
CREATE TABLE `reward_recharge` (
  `reward_recharge_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_number` varchar(20) NOT NULL,
  `fk_operator_id` int(11) NOT NULL,
  `fk_circle_id` int(11) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `source_order_id` varchar(35) NOT NULL,
  `order_id` varchar(35) NOT NULL,
  `user_id` int(11) NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`reward_recharge_id`),
  KEY `idx_reward_recharge_id` (`reward_recharge_id`),
  KEY `idx_reward_recharge_svcno` (`service_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- rollback DROP TABLE reward_recharge;
