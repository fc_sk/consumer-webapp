-- liquibase formatted sql

-- changeset jyothi:minRechargeForProductCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('Min Recharge For Product Condition', 'minRechargeForProductCondition', now(), now(), 'Params required are products (comma separated list of product codes - Mobile(V), DTH(D), DataCard(C), MobilePostpaid(M) eg. V,M) and minRechargeAmount');

-- rollback delete from ar_condition_type where class_ident = 'minRechargeForProductCondition';
