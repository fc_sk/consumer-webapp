-- liquibase formatted sql

-- changeset vinaym:inRechargePlans

DROP TABLE IF EXISTS in_recharge_plans;
CREATE TABLE `in_recharge_plans` (
  `recharge_plan_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `min_amount` decimal(10,2) DEFAULT NULL,
  `max_amount` decimal(10,2) DEFAULT NULL,
  `talktime` decimal(10,2) DEFAULT NULL,
  `validity` varchar(255) DEFAULT NULL,
  `fk_product_master_id` int(10) DEFAULT NULL,
  `denomination_type` varchar(255) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `fk_operator_master_id` int(10) DEFAULT NULL,
  `fk_circle_master_id` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_sticky` tinyint(1) DEFAULT NULL,
  `ireff_id` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `dataweave_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`recharge_plan_id`),
  KEY `idx_fk_operator_master_id` (`fk_operator_master_id`),
  KEY `idx_fk_circle_master_id` (`fk_circle_master_id`),
  KEY `idx_fk_product_master_id` (`fk_product_master_id`),
  KEY `idx_ireff_id` (`ireff_id`) USING BTREE,
  KEY `idx_recharge_plans_dataweave_id` (`dataweave_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8000000 DEFAULT CHARSET=utf8;

-- rollback DROP TABLE IF EXISTS in_recharge_plans;
