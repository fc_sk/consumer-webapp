-- liquibase formatted sql

-- changeset vipin:arDealtagId

CREATE TABLE `ardeal_tagid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_deal_id` int(11) DEFAULT NULL,
  `fk_user_id` int(11) DEFAULT NULL,
  `tag_id` varchar(32) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));

-- rollback drop table ardeal_tagid;