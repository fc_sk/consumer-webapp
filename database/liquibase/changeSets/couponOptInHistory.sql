-- liquibase formatted sql

-- changeset shirish:couponOptInHistory

create table coupon_optin_history (
id int(11) NOT NULL AUTO_INCREMENT,
campaign_name varchar(255), 
coupon_value int(11),
face_value int(11),
zone_name varchar(15),
city_name_list text,
terms_and_conditions text,
coupon_type char(3),
coupon_nature varchar(45),
coupon_image_path varchar(255),
coupon_position int(11),
is_complimentary tinyint(1),
validity_date timestamp null default null,
is_unique tinyint(1),
is_m_coupon tinyint(1),
short_description varchar(1024),
coupon_hyperlink varchar(1000),
category_name varchar(50),
coupon_code varchar(150),
block_time datetime null default null,
merchant_title varchar(255),
merchant_image_path varchar(255),
merchant_url varchar(255),
order_id varchar(20),
created_on timestamp default current_timestamp,
user_id int(11),
primary key (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE INDEX idx_coupon_history_user_id ON coupon_optin_history(user_id) USING BTREE;

CREATE INDEX idx_coupon_history_order_id ON coupon_optin_history(order_id) USING BTREE;

CREATE INDEX idx_coupon_history_created_on ON coupon_optin_history(created_on) USING BTREE;

-- rollback DROP TABLE IF EXISTS coupon_optin_history;