-- liquibase formatted sql

-- changeset shwetanka:alterLocationGroup

alter table location_groups change column locality locality varchar(50) default null, change address address text, change latitude latitude decimal(10,6) not null, change longitude longitude decimal(10,6) not null;

-- rollback alter table location_groups change column latitude latitude float(10,6)