--liquibase formatted sql

-- changeset abhishek:createTxnCardData

CREATE TABLE IF NOT EXISTS txn_card_data (
	mtxnid varchar(64) NOT NULL,
	card_bin varchar(25) NOT NULL,
	card_hash varchar(100) NOT NULL,
	created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	KEY `index_order_id` (`mtxnid`),
	index `index_card_bin` (`card_bin`),
	index `index_created_on` (`created_on`)
);

-- rollback DROP TABLE txn_card_data;