--liquibase formatted sql

-- changeset narasimhamurthy:accessInfo

CREATE TABLE access_info (
  id int(11) NOT NULL AUTO_INCREMENT,
  access_key varchar(64) NOT NULL,
  secret_key varchar(64) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY access_key (access_key),
  KEY access_key_2 (access_key)
)

-- rollback DROP TABLE access_info;