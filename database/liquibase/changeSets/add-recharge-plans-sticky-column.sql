-- liquibase formatted sql

-- changeset shantanu:add-recharge-plans-sticky-column

ALTER TABLE recharge_plans
  ADD COLUMN is_sticky BOOLEAN,
  ADD COLUMN ireff_id VARCHAR(255);
CREATE INDEX ireff_id_index ON recharge_plans (ireff_id) USING BTREE;

-- rollback DROP INDEX ireff_id_index ON recharge_plans;
-- rollback ALTER TABLE recharge_plans DROP COLUMN is_sticky, DROP COLUMN ireff_id;

