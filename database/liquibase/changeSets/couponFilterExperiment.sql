-- liquibase formatted sql

-- changeset shwetanka:couponFilterExperiment

create table coupon_experiments (
  id int (11) unique not null auto_increment,
  uid varchar (50) not null,
  imei varchar (50) not null,
  measure_id int (11) not null,
  lookup_id varchar(40) default null,
  created_on timestamp DEFAULT current_timestamp,
  primary key (id)
) engine InnoDB DEFAULT CHARSET=utf8;

create index idx_coupon_experiment_imei on coupon_experiments (imei);
create index idx_coupon_experiment_uid on coupon_experiments (uid);

-- rollback drop table coupon_experiments
