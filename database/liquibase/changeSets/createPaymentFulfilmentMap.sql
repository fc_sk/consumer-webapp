-- liquibase formatted sql

-- changeset kopaul:createPaymentFulfilmentMap

CREATE TABLE IF NOT EXISTS payment_fulfilment_map
(
   order_id varchar(26) PRIMARY KEY NOT NULL,
   mtxn_id varchar(30) NOT NULL,
   n_last_updated datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   n_created datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
   key idx_payment_fulfilment_map_mtxn_id (mtxn_id)
) ENGINE=InnoDB CHARSET=utf8;

-- rollback DROP TABLE payment_fulfilment_map;