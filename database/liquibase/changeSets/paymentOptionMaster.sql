--liquibase formatted sql

-- changeset narasimhamurthy:paymentOptionMaster

CREATE TABLE `payment_option_master` (
  `id` int(11) NOT NULL,
  `payment_type` varchar(45) DEFAULT NULL,
  `payment_option_display_name` varchar(100) DEFAULT NULL,
  `payment_option_code` varchar(45) DEFAULT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
)

-- rollback DROP TABLE payment_option_master;