-- liquibase formatted sql

-- changeset arun:binRange

CREATE TABLE `card_bin_range` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bin_range_id` varchar(64) NOT NULL,
  `bin_head` varchar(64) NOT NULL,
  `bin_tail` varchar(64) NOT NULL,
  `card_type` varchar(64) NOT NULL,
  `card_nature` varchar(64) NOT NULL,
  `fk_card_issuer_id` varchar(64) NOT NULL,
  `status` varchar(64) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_card_bin_range_bin_head` (`bin_head`),
  KEY `idx_card_bin_range_bin_tail` (`bin_tail`),
  KEY `idx_card_bin_range_fk_card_issuer_id` (`fk_card_issuer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `card_issuer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_issuer_id` varchar(64) NOT NULL,
  `card_issuer_name` varchar(64) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_card_issuer_card_issuer_id` (`card_issuer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollback DROP TABLE IF EXISTS `card_bin_range`, `card_issuer`;
