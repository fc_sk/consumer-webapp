--liquibase formatted sql

--changeset amaresh:freefundchannelcondition


insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values
('freefundpaymenttypecondition', 'freefundpaymenttypecondition', now(), now(), 'checks if payment type for a order is one of the types set. Use paymentTypes as a param with values as a list of: Credit Card, Debit Card, Net Banking, Cash Card, ATM Card, FC Balance, ZeroPay.');

--rollback delete from ar_condition_type where class_ident = 'freefundpaymenttypecondition'