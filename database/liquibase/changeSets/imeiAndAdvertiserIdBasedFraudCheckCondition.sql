-- liquibase formatted sql

-- changeset amaresh:imeiAndAdvertiserIdBasedFraudCheckCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('IMEIAndAdvIdCondition', 'imeiAndAdvertiserIdBasedFraudCheckCondition', now(), now(), 'WORKS FOR ANDROID APP ONLY. Checks if max redemption per imei and advertiser id has been reached. Params : maxImeiNumberCount - max redemptions allowed per imei for a campaign. maxAdvertisementIdCount - max redemptions allowed per advertisementId for a imei and campaign');

-- rollback delete from ar_condition_type where class_ident = 'IMEIAndAdvIdCondition';