-- liquibase formatted sql

-- changeset shwetanka:inEmailListCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('In Email List Condition', 'inEmailListARCondition', now(), now(), 'In Email List Condition, param: customKey');

-- rollback delete from ar_condition_type where class_ident = 'inEmailListARCondition';