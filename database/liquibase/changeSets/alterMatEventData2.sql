-- liquibase formatted sql

alter table mat_event_data add bundle_id varchar(50) DEFAULT NULL after revenue,
                           add assigned_click_id varchar(50) DEFAULT NULL after bundle_id,
                           add creative_id varchar(50) DEFAULT NULL after assigned_click_id,
                           add google_aid varchar(50) DEFAULT NULL after creative_id,
                           add sha1_android_id varchar(200) DEFAULT NULL after google_aid,
                           add sha1_android_advertising_id varchar(200) DEFAULT NULL after sha1_android_id,
                           add md5_android_advertising_id varchar(200) DEFAULT NULL after sha1_android_advertising_id,
                           add apsalar_internal_device_id varchar(50) DEFAULT NULL after md5_android_advertising_id,
                           add fb_campaign_id varchar(50) DEFAULT NULL after apsalar_internal_device_id,
                           add fb_campaign_name varchar(200) DEFAULT NULL after fb_campaign_id,
                           add fb_ad_set_id varchar(50) DEFAULT NULL after fb_campaign_name,
                           add fb_ad_set_name varchar(200) DEFAULT NULL after fb_ad_set_id,
                           add fb_ad_id varchar(50) DEFAULT NULL after fb_ad_set_name,
                           add fb_ad_name varchar(200) DEFAULT NULL after fb_ad_id,
                           add twitter_campaign_name varchar(200) DEFAULT NULL after fb_ad_name,
                           add twitter_campaign_id varchar(50) DEFAULT NULL after twitter_campaign_name,
                           add twitter_line_item_id varchar(50) DEFAULT NULL after twitter_campaign_id,
                           add tweet_id varchar(50) DEFAULT NULL after twitter_line_item_id;

-- rollback ALTER TABLE mat_event_data drop bundle_id, drop assigned_click_id, drop creative_id, drop google_aid, drop sha1_android_id, drop  sha1_android_advertising_id, drop md5_android_advertising_id, drop apsalar_internal_device_id, drop fb_campaign_id, drop fb_campaign_name, drop fb_ad_set_id, drop fb_ad_set_name, drop fb_ad_id, drop fb_ad_name, drop twitter_campaign_name, drop twitter_campaign_id, drop  twitter_line_item_id, drop tweet_id;

