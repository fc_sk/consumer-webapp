-- liquibase formatted sql

-- changeset shwetanka:appVersionAndImeiCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description, params) values ('MinAppVersionCondition', 'minAppVersionCondition', now(), now(), 'param is: version', '{"condKeys": {"version": {"type": "simple"}}}');
insert into ar_condition_type (name, class_ident, created_on, updated_on, description, params) values ('ClosedImeiGroupCondition', 'closedImeiGroupCondition', now(), now(), '', '');

-- rollback delete from ar_condition_type where class_ident = 'minAppVersionCondition';
-- rollback delete from ar_condition_type where class_ident = 'closedImeiGroupCondition';