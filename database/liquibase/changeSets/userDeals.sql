-- liquibase formatted sql

-- changeset ashwini:user_deals

create table user_deals (
   fk_user_id int(11) NOT NULL,
   fk_deal_id int(11) NOT NULL,
   PRIMARY KEY (fk_user_id, fk_deal_id)
);
    

-- rollback drop table user_deals;
