-- liquibase formatted sql

-- changeset vinaym:ireffPlans

DROP TABLE IF EXISTS ireff_plans;
CREATE TABLE `ireff_plans` (
  `plan_id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `talktime` decimal(10,2) DEFAULT NULL,
  `validity` varchar(255) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `fk_operator_master_id` int(10) DEFAULT NULL,
  `fk_circle_master_id` int(10) DEFAULT NULL,
  `ireff_id` varchar(255) DEFAULT NULL,
  `source_uri` varchar(500) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`plan_id`),
  KEY `idx_fk_operator_master_id` (`fk_operator_master_id`),
  KEY `idx_fk_circle_master_id` (`fk_circle_master_id`),
  KEY `idx_ireff_id` (`ireff_id`) USING BTREE,
  KEY `idx_amount` (`amount`),
  KEY `idx_created_time` (`created_time`),
  KEY `idx_is_active` (`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- rollback DROP TABLE IF EXISTS ireff_plans;
