-- liquibase formatted sql

-- changeset amitgoel:in_recharge_retry

CREATE TABLE in_recharge_retry
(
   id int PRIMARY KEY NOT NULL,
   aggr_name varchar(255) NOT NULL,
   fk_operator_master_id int NOT NULL,
   aggr_resp_code varchar(255) NOT NULL,
   is_retryable bit DEFAULT 0 NOT NULL,
   fk_retry_operator_master_id int,
   retry_priority varchar(2)
);


-- rollback DROP TABLE in_recharge_retry;

 
 
 
 
