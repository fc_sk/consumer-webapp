-- liquibase formatted sql

-- changeset abhishekanand:createOperatorCircleBlockTable

CREATE TABLE IF NOT EXISTS `operator_circle_block` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `operator_id` int(10) NOT NULL,
    `circle_id` int(10) NOT NULL,
    `is_blocked` tinyint(1) DEFAULT '0',
    `user_email` VARCHAR(150),
    PRIMARY KEY (`id`),
    UNIQUE KEY (`operator_id`,`circle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX operator_circle_block_idx ON operator_circle_block (operator_id, circle_id) USING BTREE;

-- rollback drop table operator_circle_block;