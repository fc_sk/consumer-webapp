-- liquibase formatted sql

-- changeset shantanu:alter-validity-column-type

ALTER TABLE recharge_plans MODIFY validity VARCHAR(255);
UPDATE recharge_plans SET validity = CONCAT(validity, ' days');

-- rollback ALTER TABLE recharge_plans MODIFY validity INT;

