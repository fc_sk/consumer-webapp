-- liquibase formatted sql

-- changeset amaresh:freefundCondition

alter table freefund_class modify column datamap text;

-- rollback alter table freefund_class drop column data_map;