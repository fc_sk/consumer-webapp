-- liquibase formatted sql

-- changeset shwetanka:customAppConfigProp

alter table app_config add column prop_type varchar(10) default null;

-- rollback alter table app_config drop column prop_type;