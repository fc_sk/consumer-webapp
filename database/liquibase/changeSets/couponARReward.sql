-- liquibase formatted sql

-- changeset jyothi:couponARReward
insert into ar_reward_type(name, class_ident, created_on) values('Coupon Reward', 'couponARReward', now());
-- rollback DELETE FROM ar_reward_type where class_ident='couponARReward';
