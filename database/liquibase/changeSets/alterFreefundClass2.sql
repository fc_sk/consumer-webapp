-- liquibase formatted sql

-- changeset anderavi:alterFreefundClass2

alter table freefund_class add column min_recharge_value decimal(10,2) DEFAULT NULL ;
alter table freefund_class add column freefund_value decimal(10,2) DEFAULT NULL ;
alter table freefund_class add column promo_entity varchar(40) DEFAULT NULL ;

-- rollback ALTER TABLE freefund_class DROP COLUMN min_recharge_value;
-- rollback ALTER TABLE freefund_class DROP COLUMN freefund_value;
-- rollback ALTER TABLE freefund_class DROP COLUMN promo_entity;
