-- liquibase formatted sql

-- changeset priyanalang:alterFreefundClass3

ALTER TABLE freefund_class ADD COLUMN discount_type VARCHAR(45) NULL DEFAULT 'FLAT';
ALTER TABLE freefund_class ADD COLUMN max_discount DECIMAL(10,2) NULL DEFAULT NULL;

-- rollback ALTER TABLE freefund_class DROP COLUMN discount_type;
-- rollback ALTER TABLE freefund_class DROP COLUMN max_discount;