-- liquibase formatted sql

-- changeset anderavi:arProductCondition

INSERT INTO ar_condition_type SET name='ProductARCondition', class_ident='productARCondition', created_on=NOW();

-- rollback DELETE FROM ar_condition_type where class_ident='productARCondition';
