-- liquibase formatted sql

-- changeset ujjwala:promocodeApiEnhancements

-- Entity to store promocode related attributes

CREATE TABLE promocode_affiliate_type (
 id int(11) AUTO_INCREMENT PRIMARY KEY,
 affiliate_type varchar(50),
 configurable_properties text,
 KEY idx_promocode_affiliate_type_affiliate_type (affiliate_type))
 ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

 -- rollback drop table promocode_affiliate_type;
