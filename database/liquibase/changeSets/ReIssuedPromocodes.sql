-- liquibase formatted sql

-- changeset amaresh:reIssuedPromocodes

CREATE TABLE reissued_promocodes (
  id int(11) NOT NULL AUTO_INCREMENT,
  order_id varchar(50),
  reissued_promocode_id int(11),
  parent_promocode_id int(11),
  parent_freefund_class_id int(11) ,
  created_at datetime,
  updated_at datetime,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- rollback drop table reissued_promocodes