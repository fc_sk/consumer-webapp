-- liquibase formatted sql
-- changeset Vipin:billPaymentOperatorRename

update operator_master set operator_code='Docomo Postpaid-GSM',name='Docomo Postpaid-GSM' where operator_master_id=59;

update operator_master set operator_code='Docomo Postpaid-CDMA',name='Docomo Postpaid-CDMA'where operator_master_id=61;

-- rollback update operator_master set operator_code='Docomo Postpaid',name='Docomo Postpaid' where operator_master_id=59;

-- rollback update operator_master set operator_code='Indicom Postpaid',name='Indicom Postpaid'where operator_master_id=61;

