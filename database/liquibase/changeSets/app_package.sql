-- liquibase formatted sql

-- changeset Ankur:Play store package of Apps

create table app_package(
  id int(11) PRIMARY KEY AUTO_INCREMENT,
  deal_id int(11),
  package_name varchar(100)
);

-- insert into app_package (deal_id, package_name) values (185, 'in.amazon.mShop.android.shopping');
-- insert into app_package (deal_id, package_name) values (186, 'com.myntra.android');
