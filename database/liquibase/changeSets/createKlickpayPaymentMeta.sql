-- liquibase formatted sql

-- changeset vivektejwani:klickpay_payment_meta

DROP TABLE IF EXISTS klickpay_payment_meta;
CREATE TABLE `klickpay_payment_meta` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`mtxn_id` varchar(64) NOT NULL,
`pg_used` varchar(20) NOT NULL,
`reference_id` varchar(50),
`metadata` text,
`created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`n_last_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`n_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT payment_meta_pk PRIMARY KEY (`id`),
KEY `payment_meta_idx_mtxn_id` (`mtxn_id`),
KEY `payment_meta_idx_pg_used` (`pg_used`)
) ENGINE=InnoDB;

-- rollback DROP TABLE IF EXISTS klickpay_payment_meta;