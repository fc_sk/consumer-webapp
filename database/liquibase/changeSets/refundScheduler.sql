-- liquibase formatted sql

-- changeset kopaul:refundScheduler

DROP TABLE IF EXISTS refund_scheduler;
CREATE TABLE refund_scheduler (
  mtxn_id varchar(30) NOT NULL,
  status tinyint(1) NOT NULL,
  expiry_time datetime NOT NULL,
  schedule_count int(1) NOT NULL,
  created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  last_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  n_created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  n_last_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  primary key (mtxn_id),
  KEY idx_refund_scheduler_expiry_time (expiry_time),
  KEY idx_refund_scheduler_status (status),
  KEY idx_refund_scheduler_created_on (created_on)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- rollback DROP TABLE IF EXISTS refund_scheduler;