-- liquibase formatted sql

-- changeset anderavi:alterFreefundClass

alter table freefund_class add column max_redemptions int(11) NULL DEFAULT NULL after is_active ;

-- rollback ALTER TABLE freefund_class DROP COLUMN max_redemptions;
