-- liquibase formatted sql

-- changeset priya:urlARAction

insert into ar_action_type set name="UrlARAction", class_ident="urlARAction", created_on = now(), updated_on = now();

create table ar_url_activity (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  action_id int(11) NOT NULL,
  activity varchar(40) DEFAULT NULL,
  session_id varchar(40) DEFAULT NULL,
  lookup_id varchar(40) DEFAULT NULL,
  unique_id varchar(200) DEFAULT NULL,
  created_on timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (id),
  KEY idx_ar_url_activity_user_id (user_id),
  KEY idx_ar_url_activity_action_id (action_id),
  KEY idx_ar_url_activity (activity),
  KEY idx_ar_url_unique_id (unique_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- rollback delete from ar_action_type where class_ident = 'urlARAction';
-- rollback drop table if exists ar_url_activity;