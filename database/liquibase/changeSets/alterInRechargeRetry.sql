-- liquibase formatted sql

-- changeset kopaul:in_recharge_retry


ALTER TABLE in_recharge_retry MODIFY id int NOT NULL AUTO_INCREMENT;

-- rollback DROP TABLE in_recharge_retry;