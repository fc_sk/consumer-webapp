-- liquibase formatted sql

-- changeset raviande:alter-promocode-campaign

alter table promocode_campaign add column orderid varchar(20) DEFAULT NULL after fk_user_id;

-- rollback ALTER TABLE promocode_campaign DROP COLUMN orderid;