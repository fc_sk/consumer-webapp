-- liquibase formatted sql

-- changeset jyothi:alterGrowthEvent

alter table growth_event change column channel channels varchar(50) not null;

-- rollback alter table growth_event change column channels channel tinyint(3) not null;
