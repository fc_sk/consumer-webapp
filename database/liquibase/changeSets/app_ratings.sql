-- liquibase formatted sql

-- changeset Ankur:Play store ratings of Apps

create table app_ratings(
  deal_id int(11) PRIMARY KEY,
  rating double
);

