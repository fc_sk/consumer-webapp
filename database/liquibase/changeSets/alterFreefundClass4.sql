-- liquibase formatted sql

-- changeset amaresh:freefundCondition

alter table freefund_class add datamap varchar(100);

-- rollback alter table freefund_class drop column data_map;