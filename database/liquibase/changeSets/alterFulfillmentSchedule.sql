-- liquibase formatted sql

-- changeset arun:alterFulfillmentSchedule

ALTER TABLE `recharge_fulfillment_schedule` MODIFY `trigger_event` SMALLINT;

-- rollback ALTER TABLE `recharge_fulfillment_schedule` MODIFY `trigger_event` TINYINT;
