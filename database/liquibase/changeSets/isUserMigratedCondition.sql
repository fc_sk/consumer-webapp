-- liquibase formatted sql

-- changeset amaresh:isUserMigratedCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description, params) values ('IsMigratedUserCondition', 'isMigratedUserCondition', now(), now(), 'Is User Migraated condition', 'No params needed');

-- rollback delete from ar_condition_type where class_ident = 'isMigratedUserCondition';
