-- liquibase formatted sql

-- changeset vivek:alterCouponStore

alter table coupon_store add is_out_txn tinyint(1) not null default 0,add is_in_txn tinyint(1) not null default 1;

-- rollback alter table coupon_store drop is_out_txn,drop is_in_txn;