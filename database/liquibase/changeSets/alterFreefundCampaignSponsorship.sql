-- liquibase formatted sql

-- changeset priyanalang:alterFreefundCampaignSponsorship

ALTER TABLE `freefund_class` 
ADD COLUMN `campaign_sponsorship` VARCHAR(40) NULL AFTER `datamap`,
ADD COLUMN `campaign_description` VARCHAR(2000) NULL AFTER `campaign_sponsorship`;

-- rollback ALTER TABLE freefund_class DROP COLUMN campaign_sponsorship, DROP COLUMN campaign_description;