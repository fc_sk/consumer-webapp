-- liquibase formatted sql

-- changeset abhishekanand:googleCreditsNewCategory

INSERT INTO product_master(product_master_id, product_name, img_url, fk_affiliate_profile_id, 
						is_active , product_type , is_refundable ) 
					VALUES (21, 'GoogleCredits', NULL, 1, 1, 1, 1);

INSERT INTO operator_master(operator_master_id , operator_code , name, is_active , 
						fk_country_master_id , fk_product_master_id , img_url ) 
					VALUES (89,'Google','Google',1,1,21,'images/circular-operator-logos/google_credits.png');

INSERT INTO circle_operator_mapping (circle_operator_mapping_id, fk_operator_master_id , fk_circle_master_id , aggr_priority ) 
					VALUES (1392,89,33,'euronet');
					
INSERT INTO in_oppr_aggr_priority (aggr_priority ,update_time, updated_by, fk_circle_operator_mapping_id ) 
					VALUES ('euronet', NOW(), 'abhishek.anand@freecharge.com' 1392);

INSERT INTO in_aggr_opr_circle_map(aggr_name, fk_operator_id, aggr_operator_code, 
						fk_circle_id, aggr_circle_code, should_timeout, time_out_millis) 
					VALUES ('euronet', 89, 'ETR', 33, 'ETRDV', 1, 180000);
					
					
-- rollback DELETE FROM in_aggr_opr_circle_map WHERE aggr_name = 'euronet' AND fk_operator_id = 89 AND fk_circle_id = 33;
-- rollback DELETE FROM in_oppr_aggr_priority WHERE fk_circle_operator_mapping_id = 1392;					
-- rollback DELETE FROM circle_operator_mapping WHERE fk_operator_master_id = 89 and fk_circle_master_id = 33;
-- rollback DELETE FROM operator_master WHERE operator_master_id = 89;
-- rollback DELETE FROM product_master WHERE product_master_id = 21;
