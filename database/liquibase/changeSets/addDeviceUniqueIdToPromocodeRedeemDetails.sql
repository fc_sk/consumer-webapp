-- liquibase formatted sql

-- changeset amaresh.vijayakumar:addDeviceUniqueIdToPromocodeRedeemDetails

alter table promocode_redeem_details add column windows_device_unique_id varchar(100);

-- rollback alter table promocode_redeem_details drop column windows_device_unique_id;