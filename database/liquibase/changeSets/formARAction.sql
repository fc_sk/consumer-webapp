-- liquibase formatted sql

-- changeset shwetanka:formARAction

insert into ar_action_type set name="FormARAction", class_ident="formARAction", created_on = now(), updated_on = now();

create table ar_form_activity(
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) not null,
  action_id int(11) not null,
  activity varchar (20) not null,
  session_id varchar (40),
  lookup_id varchar (40),
  extra_data text,
  created_on timestamp NOT NULL DEFAULT 0,
  primary key (id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

create index idx_ar_form_activity_user_id on ar_form_activity (user_id);
create index idx_ar_form_activity_action_id on ar_form_activity (action_id);
create index idx_ar_form_activity_activity on ar_form_activity (activity);

-- rollback delete from ar_action_type where class_ident = 'formARAction';
-- rollback drop table if exists ar_form_activity;