-- liquibase formatted sql

-- changeset rachitaChoudhary:arIPFraudCheckCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('IPFraudCheckCondition', 'ipAddressFraudCheckCondition', now(), now(), 'ONLY USE ON APPLY. Checks if Max Redemptions per IP Address have been reached. Params are : maxRedemptionIP=-1...n -1 means unlimited redemptions');

-- rollback delete from ar_condition_type where class_ident = 'ipAddressFraudCheckCondition';