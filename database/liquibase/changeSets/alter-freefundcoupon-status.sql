-- liquibase formatted sql

-- changeset raviande:alter-freefundcoupon-status

alter table freefund_coupon add column status varchar(10) NULL DEFAULT NULL;

-- rollback ALTER TABLE freefund_coupon DROP COLUMN status ;

