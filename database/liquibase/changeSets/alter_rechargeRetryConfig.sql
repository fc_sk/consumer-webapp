-- liquibase formatted sql

-- changeset vinay:rechargeRetryConf

alter table recharge_retry_config add column error_msg varchar(300);

insert into recharge_retry_config ( error_code, max_retry_count, retry_interval, max_retry_duration, is_active, error_msg) values ('OSU', 3, 900, 1800, 1, "Service Unavailable");
insert into recharge_retry_config ( error_code, max_retry_count, retry_interval, max_retry_duration, is_active, error_msg) values ('OD', 3, 900, 1800, 1, "service is currently unavailable");
insert into recharge_retry_config ( error_code, max_retry_count, retry_interval, max_retry_duration, is_active, error_msg) values ('OST', 3, 900, 1800, 1, "please try after some time");

