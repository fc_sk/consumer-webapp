-- liquibase formatted sql

-- changeset raviande:freefundMobileCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) 
values ('freefundMobileCondition', 'freefundMobileCondition', now(), now(), 'This condition takes user recharging number, finds the promocode assigned to it. A comparison is made with his applied promocode to validate this condition, hence, no param configuration required');

-- rollback delete from ar_condition_type where class_ident = 'freefundMobileCondition'
