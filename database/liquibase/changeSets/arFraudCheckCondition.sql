-- liquibase formatted sql

-- changeset shwetanka:arFraudCheckCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('FraudCheckCondition', 'fraudCheckCondition', now(), now(), 'Params accepted are - {cookie: {count: <int>, days: <int>}, ip: {count: <int>, days: <int>}, mobileNumber: {count: <int>, days: <int>}}');

-- rollback delete from ar_condition_type where class_ident = 'fraudCheckCondition';