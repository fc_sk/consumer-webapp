-- liquibase formatted sql

-- changeset shwetanka:initialAppConfig

insert into app_config (app_key, app_value) values
    ("async.recharge.enabled", "1"),
    ("under.maintenance.header.banner", "0"),
    ("payment.under.maintenance", "0"),
    ("site.under.maintenance", "0"),
    ("cash.back.campaign.enalbed", "0"),
    ("ardeal.enabled", "1"),
    ("web.site.analytics.enabled", "0"),
    ("mobile.site.analytics.enabled", "0"),
    ("final.page.coupons.enabled", "1"),
    ("tracking.enabled", "0"),
    ("recaptcha.enabled", "1"),
    ("promocode.enabled", "1"),
    ("new.registration.enabled", "1"),
    ("recharge.retry.block.enabled", "0"),
    ("zedo.ad.enabled", "1"),
    ("pepsi.campaign.enabled", "1"),
    ("antifraud.fraudfresh.enabled", "1"),
    ("antifraud.fraud3000.enabled", "1"),
    ("antifraud.fraud5000.enabled", "1"),
    ("antifraud.fraudregistertotxn.enabled", "1"),
    ("antifraud.fraudticketsize.enabled", "1");

-- rollback delete from app_config