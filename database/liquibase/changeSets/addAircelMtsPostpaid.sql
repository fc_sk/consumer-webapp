-- liquibase formatted sql

-- changeset sreeram:add-aircel-mts-postpaid
INSERT INTO operator_master(`operator_master_id`,`operator_code`,`name`,`is_active`,`fk_country_master_id`,`fk_product_master_id`,`img_url`) VALUES(65,'Aircel Postpaid','Aircel Postpaid',1,1,201,'images/circular-operator-logos/aircel.png');
INSERT INTO operator_master(`operator_master_id`,`operator_code`,`name`,`is_active`,`fk_country_master_id`,`fk_product_master_id`,`img_url`) VALUES(66,'MTS Postpaid','MTS Postpaid',1,1,201,'images/circular-operator-logos/mts.png');



INSERT INTO bill_postpaid_merchants(`postpaid_merchant_id`,`fk_operator_master_id`,`billdesk_merchant_id`,`oxigen_merchant_id`,`euronet_merchant_id`) VALUES(10,65,'AIRCEL','AIRC',null);
INSERT INTO bill_postpaid_merchants(`postpaid_merchant_id`,`fk_operator_master_id`,`billdesk_merchant_id`,`oxigen_merchant_id`,`euronet_merchant_id`) VALUES(11,66,'MTS','MTS',null);

INSERT INTO bill_payment_gateway_priority(`bill_type`,`bill_provider`,`is_active`,`fk_operator_id`) VALUES('postpaid','billPaymentOxigenClient',1,65);
INSERT INTO bill_payment_gateway_priority(`bill_type`,`bill_provider`,`is_active`,`fk_operator_id`) VALUES('postpaid','billPaymentOxigenClient',1,66);

update bill_payment_gateway_priority set bill_provider = 'billPaymentOxigenClient' where fk_operator_id = 58;



-- rollback DELETE FROM TABLE operator_master where operator_master_id = 65;
-- rollback DELETE FROM TABLE operator_master where operator_master_id = 66;
-- rollback DELETE FROM TABLE bill_postpaid_merchants where postpaid_merchant_id = 10;
-- rollback DELETE FROM TABLE bill_postpaid_merchants where fk_operator_id = 11;
-- rollback DELETE FROM TABLE bill_payment_gateway_priority where fk_operator_id = 65;
-- rollback DELETE FROM TABLE bill_payment_gateway_priority where fk_operator_id = 66;