--liquibase formatted sql

--changeset kopaul:alterRechargeReminder

ALTER TABLE user_recharge_reminder ADD COLUMN plan_id int(11) DEFAULT -1;

--rollback ALTER TABLE user_recharge_reminder DROP COLUMN plan_id;

