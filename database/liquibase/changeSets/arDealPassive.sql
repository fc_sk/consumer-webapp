-- liquibase formatted sql

-- changeset shwetanka:arDealPassive

alter table ar_deal add column passive tinyint default 0 after params;

-- rollback alter table ar_deal drop column passive;