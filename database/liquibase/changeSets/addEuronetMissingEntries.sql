-- liquibase formatted sql

-- changeset sayak:addEuronetMissingEntries

INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','23','REG','5','REG',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','22','MTS','7','MTS',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','3','BSP','23','BSP',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','23','REG','10','REG',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','23','REG','15','REG',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','23','REG','18','REG',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','23','REG','14','REG',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','22','MTS','23','MTS',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','19','TDH','33','TDH',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','23','REG','21','REG',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','3','BSP','14','BSP',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','22','MTS','11','MTS',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','14','VOD','15','VOD',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','14','VOD','6','VOD',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','2','ART','20','ART',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','22','MTS','22','MTS',NULL,'1','180000');
INSERT INTO in_aggr_opr_circle_map (aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values ('euronet','14','VOD','1','VOD',NULL,'1','180000');