-- liquibase formatted sql

-- changeset shwetanka:ardealChannel

alter table ar_deal add column channel tinyint(3) not null;
update ar_deal set channel = 1;

-- rollback alter table ar_deal drop column channel;