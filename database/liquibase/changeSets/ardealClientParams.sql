-- liquibase formatted sql

-- changeset ashwini:ardealClientParams

create table ardeal_client_params (
  id int(11) NOT NULL AUTO_INCREMENT,
  fk_thread_id int(11) DEFAULT NULL,
  activity_type varchar(20) NOT NULL,
  params varchar(200) NOT NULL,
  primary key(id)
) engine InnoDB AUTO_INCREMENT=1;

-- rollback DROP TABLE IF EXISTS ardeal_client_params;
