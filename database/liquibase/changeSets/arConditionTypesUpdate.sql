-- liquibase formatted sql

-- changeset shwetanka:arConditionTypesUpdate

alter table ar_condition_type add column params text default null;

update ar_condition_type set params = '{"condKeys": {"numTimes": {"type": "simple"}}}' where class_ident = 'numTimesActionDoneARCondition';
update ar_condition_type set params = '{"condKeys": {"conditionIds": {"type": "list"}}}' where class_ident = 'orOperatorARCondition';
update ar_condition_type set params = '{"condKeys": {"conditionIds": {"type": "list"}}}' where class_ident = 'andOperatorARCondition';
update ar_condition_type set params = '{"condKeys": {"operator": {"type": "simple"}}}' where class_ident = 'operatorARCondition';
update ar_condition_type set params = '{"condKeys": {"cities": {"type": "list"}}}' where class_ident = 'cityARCondition';
update ar_condition_type set params = '{"condKeys": {"paymentTypes": {"type": "list"}}}' where class_ident = 'paymentTypeARCondition';
update ar_condition_type set params = '{"condKeys": {"minRechargeAmount": {"type": "simple"}}}' where class_ident = 'minRechargePrepaidCondition';
update ar_condition_type set params = '{"condKeys": {"minRechargeAmount": {"type": "simple"}}}' where class_ident = 'minRechargePostpaidCondition';
update ar_condition_type set params = '{"condKeys": {"percent": {"type": "simple"}}}' where class_ident = 'percentageARCondition';
update ar_condition_type set params = '{"condKeys": {"conditionId": {"type": "simple"}}}' where class_ident = 'notOperatorARCondition';
update ar_condition_type set params = '{"condKeys": {"operatorCircleList": {"type": "list"}}}' where class_ident = 'operatorCircleCondition';

-- rollback alter table ar_condition_type drop column params;