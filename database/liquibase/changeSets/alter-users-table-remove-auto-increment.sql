-- liquibase formatted sql

-- changeset sateesh:remove-auto-increment-property-from-users-table
alter table users change user_id user_id int(11) NOT NULL;

create table user_id (id int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY(id));

ALTER TABLE user_id AUTO_INCREMENT=28868200;