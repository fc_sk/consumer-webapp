-- liquibase formatted sql

-- changeset vivek:coupon_channel_filter

alter table coupon_channel_filter add min_app_version int(5) not null;

create index idx_coupon_channel_filter_min_app_version on coupon_channel_filter (min_app_version) using btree;

-- rollback alter table coupon_channel_filter drop min_app_version;