-- liquibase formatted sql

-- changeset amaresh.vijayakumar:utmRedirectionTrackingDetails

CREATE TABLE utm_redirection_tracking_details (
utm_redirection_id INT(11) AUTO_INCREMENT PRIMARY KEY,
utm_short_code varchar (50),
user_id int(11),
channel_id int(11),
redirected_url varchar (500),
created_at timestamp);

-- rollback DROP TABLE utm_redirection_tracking_details;