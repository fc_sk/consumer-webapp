--liquibase formatted sql

-- changeset sayak:createFetchBillValidator

CREATE TABLE IF NOT EXISTS `fetchbill_validator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_operator_id` int(11) NOT NULL,
  `fk_circle_id` int(11) NOT NULL,
  `biller_type` int(2) NOT NULL,
  `is_partial_pay` tinyint(1) DEFAULT '1',
  `required_validators` int(2) DEFAULT NULL,
  `info1_name` varchar(128) NOT NULL,
  `info1_regex` TEXT DEFAULT NULL,
  `info1_msg` varchar(256) DEFAULT NULL,
  `info2_name` varchar(128) DEFAULT NULL,
  `info2_regex` TEXT DEFAULT NULL,
  `info2_msg` varchar(256) DEFAULT NULL,
  `info3_name` varchar(128) DEFAULT NULL,
  `info3_regex` TEXT DEFAULT NULL,
  `info3_msg` varchar(256) DEFAULT NULL,
  `info4_name` varchar(128) DEFAULT NULL,
  `info4_regex` TEXT DEFAULT NULL,
  `info4_msg` varchar(256) DEFAULT NULL,
  `info5_name` varchar(128) DEFAULT NULL,
  `info5_regex` TEXT DEFAULT NULL,
  `info5_msg` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_fetchbill_validator_fk_operator_id` (`fk_operator_id`)
) engine=InnoDB charset=UTF8;

-- rollback DROP TABLE fetchbill_validator;