-- liquibase formatted sql

-- changeset shirish:createCouponMetricsAndCouponRank
CREATE TABLE coupon_rank (
  id INT NOT NULL AUTO_INCREMENT,
  fk_coupon_store_id INT(11),
  optin_count INT UNSIGNED DEFAULT 0,
  available_hours INT UNSIGNED DEFAULT 0,
  inferred_rank INT(11) DEFAULT 0,
  manual_rank INT(11) DEFAULT 0,
  updated_on TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);

CREATE INDEX idx_coupon_rank_coupon_store_id ON coupon_rank(fk_coupon_store_id) USING BTREE;

-- rollback drop table coupon_rank;
