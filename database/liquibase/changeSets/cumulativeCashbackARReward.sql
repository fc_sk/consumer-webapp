-- liquibase formatted sql

-- changeset jyothi:cumulativeCashbackARReward
insert into ar_reward_type(name, class_ident, created_on) values('Cumulative Cashback Reward', 'cumulativeCashbackARReward', now());
-- rollback DELETE FROM ar_reward_type where class_ident='cumulativeCashbackARReward';
