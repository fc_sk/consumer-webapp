-- liquibase formatted sql

-- changeset ashwini:numImpsShownCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ("Number of Imps Shown", "numImpsShownCondition", now(), now(),"Params required are: numImps");
