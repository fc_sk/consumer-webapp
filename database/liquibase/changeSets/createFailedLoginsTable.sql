--liquibase formatted sql

-- changeset korathpaul:createFailedLoginsTable
CREATE TABLE failed_logins (
    id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(150) NOT NULL,
    ip_address VARCHAR(16) NOT NULL,
    attempted DATETIME NOT NULL,
    INDEX `idx_failed_logins_attempted` (`attempted`),
    INDEX `idx_failed_logins_username` (`username`),
    INDEX `idx_failed_logins_ip_address` (`ip_address`)
    ) engine=InnoDB charset=UTF8;

-- rollback DROP TABLE failed_logins;
