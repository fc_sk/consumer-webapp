-- liquibase formatted sql

-- changeset ashwini:userDealsCondition

insert into ar_condition_type(name, class_ident, created_on, updated_on, description) values ("User Deals Condition", "userDealsCondition", now(), now(),"No Params required");

-- rollback delete from ar_condition_type where class_ident = 'userDealsCondition'
