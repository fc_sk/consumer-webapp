-- liquibase formatted sql

-- changeset jyothi:promocodeARReward
insert into ar_reward_type(name, class_ident, created_on) values('Promocode Reward', 'promocodeARReward', now());
-- rollback DELETE FROM ar_reward_type where class_ident='promocodeARReward';
