-- liquibase formatted sql

-- changeset anderavi:alterBinBasedOffer

alter table binbased_offer add column short_description varchar(500) NULL DEFAULT NULL after coupon_factor ;
alter table binbased_offer add column conditions varchar(500) NULL DEFAULT NULL after description ;

-- rollback ALTER TABLE binbased_offer DROP COLUMN short_description;
-- rollback ALTER TABLE binbased_offer DROP COLUMN conditions;
