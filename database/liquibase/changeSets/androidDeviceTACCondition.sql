-- liquibase formatted sql

-- changeset amaresh:androidDeviceTACCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('androidDeviceTACCondition', 'androidDeviceTACCondition', now(), now(), 'WORKS FOR ANDROID APP ONLY. USE ONLY ON APPLY CONDITION. Checks if device TAC passed from app is valid. Parameters: allowedTacs (comma separated)');

-- rollback delete from ar_condition_type where class_ident = 'androidDeviceTACCondition';