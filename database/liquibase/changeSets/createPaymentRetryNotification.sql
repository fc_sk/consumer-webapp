-- liquibase formatted sql

-- changeset sayak:createPaymentRetryNotification

CREATE TABLE IF NOT EXISTS payment_retry_notification(
notification_id BIGINT not NULL AUTO_INCREMENT,
order_id VARCHAR(30) NOT NULL,
email_id VARCHAR(128) NOT NULL,
fire_time DATETIME,
PRIMARY KEY(notification_id),
INDEX(order_id),
INDEX(email_id),
INDEX(fire_time)
);

-- rollback DROP TABLE payment_retry_notification;