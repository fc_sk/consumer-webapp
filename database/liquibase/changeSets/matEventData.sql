-- liquibase formatted sql

-- changeset amaresh:matEventData

CREATE TABLE mat_event_data (
`id` INT(11) AUTO_INCREMENT PRIMARY KEY,
`partner` varchar (200),
`app_name` varchar (200),
`device_id` varchar (100),
`device_ip` varchar (45),
`device_carrier` varchar (100),
`device_type` varchar (100),
`campaign_name` varchar (200),
`app_version` varchar (20),
`event_name` varchar (100),
`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`event_time` timestamp,
`updated_at` timestamp);

-- rollback DROP TABLE mat_event_data;