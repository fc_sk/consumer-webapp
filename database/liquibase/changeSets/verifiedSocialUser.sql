-- liquibase formatted sql

-- changeset shwetanka:verifiedSocialUser

alter table social_user add column verified tinyint(1) default 0 after image_url;

-- rollback alter table social_user drop column verified;