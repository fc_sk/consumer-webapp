-- liquibase formatted sql

-- changeset vipin:billPayment

ALTER TABLE txn_home_page ADD COLUMN fk_bill_txn_home_page_id BIGINT;

CREATE TABLE bill_payment_attempts (
  attempt_id   BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  fk_status_id BIGINT NOT NULL,
  raw_request  VARCHAR(2000) DEFAULT NULL,
  raw_response VARCHAR(2000) DEFAULT NULL,
  is_success   BOOLEAN       DEFAULT NULL,
  auth_status  VARCHAR(10)   DEFAULT NULL,
  auth_detail  VARCHAR(200),
  created_at   DATETIME      DEFAULT NULL);
  
  CREATE TABLE `bill_payment_errorcode_map` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `bill_payment_gateway` varchar(30) DEFAULT NULL,
  `bill_payment_gateway_response_code` varchar(30) DEFAULT NULL,
  `bill_payment_internal_response_code` varchar(30) DEFAULT NULL,
  `bill_payment_internal_message` varchar(200) DEFAULT NULL,
  `is_retryable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`));
  
  CREATE TABLE `bill_payment_gateway_priority` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `bill_type` varchar(255) DEFAULT NULL,
  `bill_provider` varchar(30) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bill_type` (`bill_type`));
  
  CREATE TABLE `bill_payment_status` (
  `status_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `affiliate_id` int(11) NOT NULL,
  `affiliate_trans_id` varchar(50) NOT NULL,
  `product_type` varchar(50) NOT NULL,
  `postpaid_number` varchar(10) DEFAULT NULL,
  `postpaid_ac_number` varchar(50) DEFAULT NULL,
  `amount` decimal(19,2) NOT NULL,
  `is_success` tinyint(1) DEFAULT NULL,
  `auth_status` varchar(10) DEFAULT NULL,
  `auth_detail` varchar(200) DEFAULT NULL,
  `retry_number` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `bill_payment_gateway` varchar(30) DEFAULT NULL,
  `gateway_reference_number` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`status_id`),
  UNIQUE KEY `uc_affiliate_trans_id` (`affiliate_id`,`affiliate_trans_id`));
  
CREATE TABLE `bill_txn_home_page` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `postpaid_phone_number` varchar(10) DEFAULT NULL,
  `postpaid_account_number` varchar(20) DEFAULT NULL,
  `postpaid_merchant_id` varchar(20) DEFAULT NULL,
  `postpaid_merchant_name` varchar(40) DEFAULT NULL,
  `bill_amount` decimal(19,4) DEFAULT NULL,
  `bill_due_date` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `bill_provider` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`));
    
  CREATE TABLE `bill_postpaid_merchants` (
  `postpaid_merchant_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_operator_master_id` int(11) NOT NULL,
  `billdesk_merchant_id` varchar(30) NOT NULL,
  `oxigen_merchant_id` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`postpaid_merchant_id`),
  UNIQUE KEY `fk_operator_master_id` (`fk_operator_master_id`));

-- rollback DELETE FROM product_master WHERE product_master_id = 201;
-- rollback DELETE FROM payment_type_relation WHERE payment_type_relation_id >= 41 AND payment_type_relation_id <= 48;
-- rollback DELETE FROM payment_type_options  WHERE payment_type_options_id >= 402 AND payment_type_options_id <= 477;
-- rollback ALTER TABLE txn_home_page DROP COLUMN fk_bill_txn_home_page_id;
-- rollback DROP TABLE IF EXISTS bill_postpaid_merchants;
-- rollback DROP TABLE IF EXISTS bill_txn_home_page;
-- rollback DROP TABLE IF EXISTS bill_payment_status;
-- rollback DROP TABLE IF EXISTS bill_payment_attempts;
