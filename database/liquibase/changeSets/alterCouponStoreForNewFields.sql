-- liquibase formatted sql

-- changeset vivek:alterCouponStoreForNewFields

alter table coupon_store add merchant_payout int(11);
alter table coupon_store add tags varchar(500);
alter table coupon_store add offer_highlights text;
alter table coupon_store add how_redeem_offer text;
alter table coupon_store add owner_of_deal varchar(50);

--- rollback alter table coupon_store drop merchant_payout;
--- rollback alter table coupon_store drop tags;
--- rollback alter table coupon_store drop offer_highlights;
--- rollback alter table coupon_store drop how_redeem_offer;
--- rollback alter table coupon_store drop owner_of_deal;