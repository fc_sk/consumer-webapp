-- liquibase formatted sql

-- changeset ujjwala:promocodeDistribution

-- Entity to store promocode related attributes
DROP TABLE IF EXISTS promocode;
CREATE TABLE promocode (
 promocode_id int(11) AUTO_INCREMENT PRIMARY KEY,
 promocode varchar(128),
 promo_type varchar(40),
 campaign_id int(11),
 promo_sponsorship	varchar(40),
 current_status varchar(128),
 prefix varchar(20),
 code_length int(11),
 discount_type varchar(45),
 discount_value decimal(10,2),
 max_discount_value decimal(10,2),
 min_recharge_value decimal(10,2),
 apply_condition_id int(11),
 payment_condition_id int(11),
 redeem_condition_id	int(11),
 success_message_apply varchar(255),
 success_message_recharge varchar(255),
 description	varchar(2000),
 has_unique_mapping tinyint(1),
 valid_from datetime,
 valid_upto datetime,
 created_at timestamp,
 created_by varchar(100),
 updated_at timestamp,
 updated_by varchar(100),
 foreign key (campaign_id) references freefund_class(id),
 KEY idx_promocode (promocode),
 KEY idx_campaign_id (campaign_id))
 ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Entity to store every request for promocode generation along with respective response
CREATE TABLE promocode_generation_request_response (
 id int(11) AUTO_INCREMENT PRIMARY KEY,
 promocode_id int(11),
 request_id varchar(128),
 request_data text,
 response_id varchar(128),
 response_data text,
 created_at timestamp,
 KEY idx_request_id (request_id))
 ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Entity to store unique key value mapping for a promocode
CREATE TABLE promocode_key_value_mapping (
 id int(11) AUTO_INCREMENT PRIMARY KEY ,
 promocode_id int(11),
 mapping_key varchar(128),
 key_value varchar(128),
 foreign key (promocode_id) references promocode (promocode_id))
 ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Entity to store affiliates for promocode generation
CREATE TABLE promocode_affiliates (
 id INT(11) AUTO_INCREMENT PRIMARY KEY,
 affiliate_id varchar (200),
 affiliate_name varchar (500),
 affiliate_type varchar (100),
 created_at timestamp,
 created_by varchar (100),
 updated_at timestamp,
 updated_by varchar (100),
 KEY idx_affiliate_id (affiliate_id))
 ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Entity to store every request for promocode status along with the response
CREATE TABLE promocode_status_request_response (
 id INT(11) AUTO_INCREMENT PRIMARY KEY,
 request_id varchar (20),
 request text,
 response text,
 created_at timestamp,
 created_by varchar (100),
 updated_at timestamp,
 updated_by varchar (100),
 KEY idx_request_id (request_id)
);

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('promocodeKeyValueMappingCheckCondition', 'promocodeKeyValueMappingCheckCondition', now(), now(), 'No params needed. Condition to verify if promocode key value mapping holds for the current user.');

-- rollback drop tables promocode,campaign_programme,promocode_generation_request_response, promocode_key_value_mapping, promocode_affiliates, promocode_status_request_response;delete from ar_condition_type where class_ident = 'promocodeKeyValueMappingCheckCondition';
