-- liquibase formatted sql

-- changeset amaresh.vijayakumar:utmTrackerCampaignDetails

CREATE TABLE utm_campaign_details (
utm_campaign_id INT(11) AUTO_INCREMENT PRIMARY KEY,
fc_master varchar(50),
utm_campaign_name varchar(100),
utm_medium varchar(50),
utm_source varchar(50),
utm_keyword varchar(50),
utm_url varchar(100),
utm_content varchar(50),
forward_url varchar(200),
fc_freefund_class_id int(11),
fc_ardeal_id int(11),
created_at timestamp,
created_by varchar (100),
updated_at timestamp,
updated_by varchar (100)
);

-- rollback DROP TABLE utm_campaign_details;