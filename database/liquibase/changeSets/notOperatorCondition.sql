-- liquibase formatted sql

-- changeset jyothi:notOperatorCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('Not Operator Condition', 'notOperatorCondition', now(), now(), 'Param required is operator. For promocode campaigns, this condition can be set only on promocode application');

-- rollback delete from ar_condition_type where class_ident = 'notOperatorCondition';
