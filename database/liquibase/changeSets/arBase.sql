-- liquibase formatted sql

-- changeset chetanvaity:arBase

CREATE TABLE `ar_deal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `fk_action_id` int(11) NOT NULL,
  `fk_reward_id` int(11) NOT NULL,
  `fk_condition_id` int(11) NOT NULL,
  `is_enabled` boolean NOT NULL,
  `hookpoint` varchar(20) NOT NULL,
  `theme` varchar(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT 0,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE ar_action_type (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `class_ident` varchar(200) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT 0,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE ar_action (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `fk_ar_action_type_id` int(11) NOT NULL,
  `params` TEXT,
  `created_on` timestamp NOT NULL DEFAULT 0,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE ar_reward_type (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `class_ident` varchar(200) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT 0,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE ar_reward (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `fk_ar_reward_type_id` int(11) NOT NULL,
  `params` TEXT NULL,
  `created_on` timestamp NOT NULL DEFAULT 0,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE ar_condition_type (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `class_ident` varchar(200) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT 0,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE ar_condition (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `fk_ar_condition_type_id` int(11) NOT NULL,
  `params` TEXT NULL,
  `created_on` timestamp NOT NULL DEFAULT 0,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- VoucherOptIn action related
CREATE TABLE ar_voucher_opt_in_activity (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_ar_action_id` int(11) NOT NULL,
  `fk_user_id` int(11) NULL DEFAULT NULL,
  `session_id` varchar(36) NULL DEFAULT NULL, 
  `activity` varchar(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE ar_voucher_code (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_ar_action_id` int(11) NOT NULL,
  `voucher_code` varchar(36) NULL DEFAULT NULL, 
  `status` tinyint(1) DEFAULT 0,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollback DROP TABLE IF EXISTS `ar_deal`, `ar_action_type`, `ar_action`, `ar_reward_type`, `ar_reward`, `ar_condition_type`, `ar_condition`, `ar_voucher_opt_in_activity`, `ar_voucher_code`;