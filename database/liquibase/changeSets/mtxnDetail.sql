-- liquibase formatted sql

-- changeset vivektejwani:mtxn_detail

DROP TABLE IF EXISTS mtxn_detail;
CREATE TABLE mtxn_detail (
  mtxn_id varchar(30) NOT NULL,
  order_id varchar(26) NOT NULL,
  payment_status varchar(20) NOT NULL,
  fulfilment_status varchar(20) NOT NULL,
  refund_status varchar(20) NOT NULL,
  pay_status_last_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  fulfilment_status_last_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  refund_status_last_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  state varchar(20) NOT NULL,
  primary key (mtxn_id),
  KEY idx_mtxn_detail_order_id (order_id),
  KEY idx_mtxn_detail_state (state),
  KEY idx_mtxn_detail_pay_status (payment_status),
  KEY idx_mtxn_detail_fulfill_status (fulfilment_status),
  KEY idx_mtxn_detail_refund_status (refund_status),
  KEY idx_mtxn_detail_refund_status_last_updated (refund_status_last_updated)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- rollback DROP TABLE IF EXISTS mtxn_detail;