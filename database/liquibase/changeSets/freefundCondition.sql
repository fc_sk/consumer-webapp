-- liquibase formatted sql

-- changeset shirish:freefundCondition
alter table freefund_class add apply_condition_id  int(11);
alter table freefund_class add payment_condition_id  int(11);


insert into ar_condition_type (name, class_ident, created_on, updated_on, description) 
values ('FreefundRedemptionsCondition', 'freefundClassRedemptionCondition', now(), now(), 'maxRedemptionEmail = -1..n, maxRedemptionNumber = -1..n, maxRedemptionProfileNumber = -1..n ');


insert into ar_condition_type (name, class_ident, created_on, updated_on, description) 
values ('FreefundCookieCondition', 'freefundClassCookieCondition', now(), now(), 'maxApplyCount = -1..n');

-- rollback alter table freefund_class drop column apply_condition_id; 
-- rollback alter table freefund_class drop column payment_condition_id;
-- rollback delete from ar_condition_type where name = 'FreefundRedemptionsCondition'
-- rollback delete from ar_condition_type where name = 'FreefundApplyCondition'