--liquibase formatted sql

--changeset shantanu:001-delete-duplicate-entry

DELETE FROM circle_operator_mapping WHERE circle_operator_mapping_id = 136;

--rollback INSERT circle_operator_mapping
--rollback (circle_operator_mapping_id, fk_operator_master_id, fk_circle_master_id)
--rollback VALUES (136, 12, 1);

