-- liquibase formatted sql

-- changeset raviande:freefundFirstOrderCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) 
values ('freefundFirstOrderCondition', 'freefundFirstOrderCondition', now(), now(), 'No param configuration required');

-- rollback delete from ar_condition_type where class_ident = 'freefundFirstOrderCondition'
