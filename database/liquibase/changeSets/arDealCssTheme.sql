-- liquibase formatted sql

-- changeset shwetanka:arDealCssTheme

alter table ar_deal add column css_theme varchar(40) default 'Default';

-- rollback alter table ar_deal drop column css_theme;