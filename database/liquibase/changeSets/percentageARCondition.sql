-- liquibase formatted sql

-- changeset shwetanka:percentageARCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('PercentageARCondition', 'percentageARCondition', now(), now(), 'Params accepted is - percent (long value)');

-- rollback delete from ar_condition_type where class_ident = 'percentageARCondition';