--liquibase formatted sql

--changeset amaresh:freefundchannelcondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('freefundchannelcondition', 'freefundchannelcondition', now(), now(), 'channels=Web or Mobile or App or any combination of these. like channels:{"Mobile","App"}(Case independent)');

--rollback delete from ar_condition_type where class_ident = 'freefundchannelcondition'