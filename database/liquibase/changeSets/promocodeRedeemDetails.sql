-- liquibase formatted sql

-- changeset amaresh.vijayakumar:promocodeRedeemDetails

CREATE TABLE promocode_redeem_details (
`order_id` varchar (50) PRIMARY KEY ,
`campaign_name` varchar (200),
`campaign_id` int(11),
`discount_type` varchar (45),
`discount_value` decimal(10,2),
`promo_entity` varchar (40),
`freefund_value` decimal (10, 2),
`freefund_code` varchar(128),
`user_id` int(11),
`operator_name` varchar(128),
`circle_name` varchar (128),
`recharge_amount` decimal (10,2),
`service_number` varchar (15),
`channel_id` int (11),
`product_type` varchar (50),
`is_coupon_opted` tinyint(1),
`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`updated_at` timestamp);

-- rollback DROP TABLE promocode_redeem_details;