-- liquibase formatted sql

-- changeset shwetanka:arRewardMultiple

alter table ar_reward add column multiple tinyint(1) default 0 after fk_ar_reward_type_id;

-- rollback alter table ar_reward drop column multiple;
