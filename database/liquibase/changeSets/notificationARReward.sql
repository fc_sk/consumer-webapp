-- liquibase formatted sql

-- changeset jyothi:notificationARReward
insert into ar_reward_type(name, class_ident, created_on) values('Notification Reward', 'notificationARReward', now());
-- rollback DELETE FROM ar_reward_type where class_ident='notificationARReward';
