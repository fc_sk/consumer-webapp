-- liquibase formatted sql

-- changeset arun:rechargeFulfillmentSchedule

CREATE TABLE `recharge_fulfillment_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(20) NOT NULL,
  `trigger_event` tinyint NOT NULL,
  `trigger_expiry` datetime NOT NULL,
  `created_by` tinyint NOT NULL,
  `schedule_reason` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`trigger_expiry`),
  KEY `idx_recharge_fulfillment_schedule_status` (`status`),
  KEY `idx_recharge_fulfillment_schedule_order_id` (`order_id`),
  KEY `idx_recharge_fulfillment_schedule_status_schedule_reason` (`status`, `schedule_reason`),
  KEY `idx_recharge_fulfillment_schedule_status_trigger_event` (`status`, `trigger_event`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
PARTITION BY list (month(trigger_expiry))(
  PARTITION rfs_p1 VALUES IN (1),
  PARTITION rfs_p2 VALUES IN (2),
  PARTITION rfs_p3 VALUES IN (3),
  PARTITION rfs_p4 VALUES IN (4),
  PARTITION rfs_p5 VALUES IN (5),
  PARTITION rfs_p6 VALUES IN (6),
  PARTITION rfs_p7 VALUES IN (7),
  PARTITION rfs_p8 VALUES IN (8),
  PARTITION rfs_p9 VALUES IN (9),
  PARTITION rfs_p10 VALUES IN (10),
  PARTITION rfs_p11 VALUES IN (11),
  PARTITION rfs_p12 VALUES IN (12));
    
-- rollback DROP TABLE IF EXISTS `recharge_fulfillment_schedule`;
