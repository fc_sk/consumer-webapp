-- liquibase formatted sql

-- changeset jyothi:alterFreefundClass7

alter table freefund_class add column creation_limit int(11) NULL DEFAULT NULL;
alter table freefund_class add column redemption_limit int(11) NULL DEFAULT NULL ;

-- rollback ALTER TABLE freefund_class DROP COLUMN creation_limit;
-- rollback ALTER TABLE freefund_class DROP COLUMN redemption_limit;