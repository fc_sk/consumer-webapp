-- liquibase formatted sql

-- changeset arun:alterRechargeRetryStatus

ALTER TABLE recharge_retry_status CHANGE is_retry_complete retry_status tinyint

-- rollback ALTER TABLE recharge_retry_status CHANGE retry_status is_retry_complete tinyint
