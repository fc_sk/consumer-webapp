-- liquibase formatted sql

-- changeset shirish:voucherCatSequence.sql

alter table voucher_cat add column sequence int(4) default 1000;

-- rollback alter table voucher_cat drop column sequence ;
