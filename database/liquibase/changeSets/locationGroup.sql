-- liquibase formatted sql

-- changeset shwetanka:locationGroup

create table location_groups (
id int(11) NOT NULL AUTO_INCREMENT,
group_id int(11) NOT NULL,
latitude float (10, 6) NOT NULL,
longitude float (10, 6) NOT NULL,
locality varchar(20) default null ,
address varchar(50) default null,
city varchar(30) default null ,
state varchar(30) default null ,
pincode int(8) default null ,
created_on timestamp default current_timestamp,
updated_on timestamp,
primary key (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

create index idx_location_group_group_id on location_groups (group_id);

create table coupon_location_group (
id int(11) not null auto_increment,
fk_coupon_id int(11) not null,
fk_group_id int(11) not null,
created_on timestamp default current_timestamp,
updated_on timestamp,
primary key (id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

create index idx_coupon_location_group_coupon_id on coupon_location_group (fk_coupon_id);
create index idx_coupon_location_group_group_id on coupon_location_group (fk_group_id);

-- rollback drop table location_groups
-- rollback drop table coupon_location_group
