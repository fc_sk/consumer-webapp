--liquibase formatted sql

-- changeset korathpaul:createAppVersionOrder
CREATE TABLE IF NOT EXISTS `app_version_order` (
  `app_version_order_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(64) NOT NULL,
  `app_version` int(3) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`app_version_order_id`),
  KEY `idx_app_ver_order_id` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollback DROP TABLE app_version_order;
