-- liquibase formatted sql

-- changeset sayak:addPostPaidBillDeskErrorCode

INSERT into bill_payment_errorcode_map (bill_payment_gateway,bill_payment_gateway_response_code,bill_payment_internal_response_code,bill_payment_internal_message,is_retryable,is_display_enabled) values ('billdeskClient','00','1','Transaction Successful',0,0);