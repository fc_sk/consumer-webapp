-- liquibase formatted sql

-- changeset shwetanka:smsARActivity

insert into ar_action_type set name="RechargeSuccessSms", class_ident="rechargeSuccessSmsAction", created_on = now(), updated_on = now();

create table ar_sms_activity(
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) not null,
  ar_action_id int(11) not null,
  order_id varchar (100) default null,
  result varchar (20) not null,
  event varchar (10) not null,
  number varchar (15) not null,
  created_on timestamp NOT NULL DEFAULT 0,
  primary key (id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollback delete from ar_action_type where class_ident = 'rechargeSuccessSmsAction';
-- rollback DROP TABLE IF EXISTS ar_sms_activity;
