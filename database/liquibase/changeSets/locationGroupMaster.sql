-- liquibase formatted sql

-- changeset shirish:locationGroupMaster

create table location_group_master (
id int(11) not null auto_increment,
name varchar(255) not null,
created_on timestamp default current_timestamp,
updated_on timestamp,
primary key (id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollback drop table location_group_master

