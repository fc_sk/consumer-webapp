--liquibase formatted sql

--changeset amaresh:freefundchannelcondition


insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('freefundbinbasedoffercondition', 'freefundbinbasedoffercondition', now(), now(), 'checks if a card no is eligible for a bin based offer. OfferKey to be passsed as param');

--rollback delete from ar_condition_type where class_ident = 'freefundchannelcondition'