--liquibase formatted sql

--changeset anderavi:002-alter-cashback-campaign-table-state-column-to-take-pending-state-also

ALTER TABLE cash_back_campaign  MODIFY state enum('opted','pending','applied') NOT NULL;

--rollback ALTER TABLE cash_back_campaign  MODIFY state enum('opted','applied') NOT NULL;

