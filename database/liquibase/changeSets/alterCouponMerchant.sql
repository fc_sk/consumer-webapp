-- liquibase formatted sql

-- changeset vivek:alterCouponMerchant

alter table coupon_merchant add merchant_email varchar(255);

-- rollback alter table coupon_merchant drop merchant_email;