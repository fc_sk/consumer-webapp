-- liquibase formatted sql

-- changeset amitgoel:alter_bill_payment_status

ALTER TABLE bill_payment_status ADD COLUMN
(
 operator_name varchar(45) NULL,
 circle_name varchar(255) NULL,
 fk_product_master_id int NULL,
 fk_operator_master_id int NULL
) ;

-- rollback DROP COLUMN operator_name, circle_name,fk_product_master_id, fk_operator_master_id from bill_payment_status;