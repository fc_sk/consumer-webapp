-- liquibase formatted sql

-- changeset abhi:alterCardIssuerWithUniqueIndex

ALTER TABLE card_issuer ADD UNIQUE index idx_card_issuer_card_issuer_name (card_issuer_name);

-- rollback DROP INDEX idx_card_issuer_card_issuer_name ON card_issuer;
