-- liquibase formatted sql

-- changeset abhi:fbLocationMasterRedux

DROP TABLE IF EXISTS fb_location_master;

CREATE TABLE fb_location_master (
  id int(11) NOT NULL AUTO_INCREMENT,
  fb_id VARCHAR(30) NOT NULL,
  location_name VARCHAR(100) NOT NULL,
  raw_location_name TEXT NOT NULL,
  status tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (id),
  UNIQUE (fb_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

create index idx_fb_location_master_fb_id on fb_location_master (fb_id);

-- rollback DROP TABLE IF EXISTS fb_location_master;
