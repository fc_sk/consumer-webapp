-- liquibase formatted sql

-- changeset priyanalang:arFBFraudCheckCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('FBFraudCheckCondition', 'fbFraudCheckCondition', now(), now(), 'ONLY USE ON APPLY. Checks if Min Post Count / Last Post Date Difference conditions have been satisfied by the User. Params are : fbMinPostCount=-1...25, fbLastPostDayDiff=-1...n. -1 means condition not applicable');

-- rollback delete from ar_condition_type where class_ident = 'fbFraudCheckCondition';