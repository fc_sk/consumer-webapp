-- liquibase formatted sql

-- changeset arun:cardCategory

CREATE TABLE `card_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_category_id` varchar(64) NOT NULL,
  `card_category_name` varchar(64) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_card_category_card_category_id` (`card_category_id`),
  KEY `idx_card_category_card_category_name` (`card_category_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO card_category(card_category_id, card_category_name) VALUES ('default-unknown-id', 'UNKNOWN');

ALTER TABLE card_bin_range ADD COLUMN fk_card_category_id varchar(64) DEFAULT 'default-unknown-id';

ALTER TABLE card_bin_range ADD index idx_card_bin_range_fk_card_category_id (fk_card_category_id);

-- rollback ;
