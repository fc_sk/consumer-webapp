-- liquibase formatted sql

-- changeset sateesh:add-ims-migrated-users-table
CREATE TABLE ims_migrated_users (
	id int(11) NOT NULL AUTO_INCREMENT,
	user_id int(11) NOT NULL,
	fc_wallet_id varchar(127) NOT NULL,
	email varchar(150) NOT NULL,
	mobile_number varchar(16) NOT NULL,
	created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id)
);

-- add unique constraints to ims_migrated_users
ALTER TABLE ims_migrated_users ADD UNIQUE (user_id);
ALTER TABLE ims_migrated_users ADD UNIQUE (fc_wallet_id);