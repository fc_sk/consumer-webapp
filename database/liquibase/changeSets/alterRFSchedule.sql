-- liquibase formatted sql

-- changeset arun:alterRFSchedule

alter table recharge_fulfillment_schedule modify created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;

-- rollback
