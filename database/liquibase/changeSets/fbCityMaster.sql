-- liquibase formatted sql

-- changeset abhi:fbLocationMaster

CREATE TABLE fb_location_master (
  id int(11) NOT NULL AUTO_INCREMENT,
  location_name varchar(100) NOT NULL,
  raw_location_name TEXT NOT NULL,
  status tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

create index idx_fb_location_master_location_name on fb_location_master (location_name);

-- rollback DROP TABLE IF EXISTS fb_location_master;
