-- liquibase formatted sql

-- changeset sateesh:insertTrackerSqsAppConfig

insert into app_config values ("tracking.sqs.enabled", "1", NULL);

-- rollback delete from app_config where app_key = "tracking.sqs.enabled";
