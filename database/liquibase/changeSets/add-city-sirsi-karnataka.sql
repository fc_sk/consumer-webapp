-- liquibase formatted sql

-- changeset shantanu:add-city-sirsi-karnataka

INSERT INTO city_master
  (city_name, status, fk_state_master_id, fk_country_master_id, fk_zone_master_id)
  VALUES ('Sirsi', 1, 18, 99, 5);

-- rollback DELETE FROM city_master
-- rollback   WHERE city_name = 'Sirsi' AND fk_state_master_id = 18
-- rollback     AND fk_country_master_id = 99 AND fk_zone_master_id = 5;

