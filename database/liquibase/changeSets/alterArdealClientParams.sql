-- liquibase formatted sql

-- changeset shwetanka:alterArdealClientParams

alter table ardeal_client_params change column fk_thread_id fk_thread_id int(11) not null;
alter table ardeal_client_params change column params params text default null;

-- rollback alter table ardeal_client_params drop column fk_thread_id;
-- rollback alter table ardeal_client_params drop column ;