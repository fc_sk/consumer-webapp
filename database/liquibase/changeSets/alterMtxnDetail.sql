--liquibase formatted sql

--changeset vivektejwani:alterMtxnDetail

alter table mtxn_detail add column refund_enqueue_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP after fulfilment_status_last_updated,
                        add column refund_initiated_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP after refund_enqueue_time,
                        modify pay_status_last_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        modify column fulfilment_status_last_updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP;

-- rollback alter table mtxn_detail drop refund_enqueue_time, drop refund_initiated_time;