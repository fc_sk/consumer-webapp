-- liquibase formatted sql

-- changeset korath:billpay oxigen integration

insert into operator_master values(80, "BSESRajdhani", "BSES Rajdhani-Delhi", 1, 1, 311, "images/operator-logos/bses_rajdhani.png");
insert into operator_master values(81, "BSESYamuna", "BSES Yamuna-Delhi", 1, 1, 311, "images/operator-logos/bses_yamuna.png");
insert into operator_master values(82, "NDPLDelhi", "North Delhi Power Ltd", 1, 1, 311, "images/operator-logos/ndpl.png");
insert into operator_master values(83, "MahanagarGasMumbai", "Mahanagar Gas-Mumbai", 1, 1, 311, "images/operator-logos/mahanagar_gas.png");
insert into operator_master values(84, "TollDelhiGurgaon", "Toll Delhi-Gurgaon", 1, 1, 311, "images/operator-logos/toll_delhi.png");
insert into operator_master values(85, "RelianceEnergyMumbai", "Reliance Energy-Mumbai", 1, 1, 311, "images/operator-logos/rel_energy_mumbai.png");
insert into operator_master values(86, "AirtelLandLine", "Airtel Landline", 1, 1, 311, "images/operator-logos/airtel_ll.png");
insert into operator_master values(87, "TataLandLine", "Tata Teleservices", 1, 1, 311, "images/operator-logos/tata_ll.png");
insert into operator_master values(88, "MTNLLandLine", "MTNL Landline-Delhi", 1, 1, 311, "images/operator-logos/mtnl_ll.png");



-- Dumping data for table `in_aggr_opr_circle_map`

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 80, 'BSESR', 6, 'DEL', NULL, '1', 600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 81, 'BSESY', 6, 'DEL', NULL, '1', 600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 82, 'NDPL', 6, 'DEL', NULL, '1', 600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 83, 'MGL', 15, 'MUM', NULL, '1', 600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 84, 'TOLL', 6, 'DEL', NULL, '1', 600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 85, 'RELENG', 15, 'MUM', NULL, '1', 600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 1, 'AP', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 2, 'ASM', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 3, 'BIH', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 4, 'CH', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 5, 'CHE', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 6, 'DEL', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 7, 'GUJ', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 8, 'HAR', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 9, 'HP', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 10, 'KK', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 11, 'KER', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 12, 'KOL', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 13, 'MP', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 14, 'MAH', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 15, 'MUM', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 16, 'NE', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 17, 'ORI', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 18, 'PUN', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 19, 'RAJ', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 20, 'TN', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 21, 'UPE', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 22, 'UPW', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 23, 'WB', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 33, 'ALL', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 34, 'JK', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 35, 'UTC', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 36, 'UTC', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 86, 'AIRTPHONE', 40, 'WB', NULL, '1', 600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 1, 'AP', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 2, 'ASM', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 3, 'BIH', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 4, 'CH', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 5, 'CHE', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 6, 'DEL', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 7, 'GUJ', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 8, 'HAR', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 9, 'HP', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 10, 'KK', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 11, 'KER', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 12, 'KOL', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 13, 'MP', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 14, 'MAH', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 15, 'MUM', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 16, 'NE', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 17, 'ORI', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 18, 'PUN', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 19, 'RAJ', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 20, 'TN', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 21, 'UPE', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 22, 'UPW', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 23, 'WB', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 33, 'ALL', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 34, 'JK', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 35, 'UTC', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 36, 'UTC', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 87, 'INDI', 40, 'WB', NULL, '1', 600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 1, 'AP', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 2, 'ASM', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 3, 'BIH', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 4, 'CH', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 5, 'CHE', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 6, 'DEL', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 7, 'GUJ', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 8, 'HAR', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 9, 'HP', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 10, 'KK', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 11, 'KER', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 12, 'KOL', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 13, 'MP', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 14, 'MAH', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 15, 'MUM', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 16, 'NE', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 17, 'ORI', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 18, 'PUN', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 19, 'RAJ', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 20, 'TN', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 21, 'UPE', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 22, 'UPW', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 23, 'WB', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 33, 'ALL', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 34, 'JK', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 35, 'UTC', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 36, 'UTC', NULL, '1', 600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('oxigen', 88, 'MTNL', 40, 'WB', NULL, '1', 600000);


-- rollback delete from in_aggr_opr_circle_map where aggr_name='oxigen' and fk_operator_id in (80,81,82,83,84,85,86,87,88);
-- rollback delete from operator_master where operator_master_id in (80,81,82,83,84,85,86,87,88);

