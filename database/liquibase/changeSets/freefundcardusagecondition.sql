--liquibase formatted sql

--changeset amaresh:freefundchannelcondition


insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('freefundcardusagecondition', 'freefundcardusagecondition', now(), now(), 'checks if a card has exceeded the number of times it can avail an offer. Max number of times is the param, like numberOfTimes=3.');

--rollback delete from ar_condition_type where class_ident = 'freefundchannelcondition'