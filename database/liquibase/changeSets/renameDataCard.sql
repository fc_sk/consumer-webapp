-- liquibase formatted sql

-- changeset Vipin:rename Data Card

update operator_master set name='Tata Indicom (All Products)' where operator_master_id=51;

update operator_master set name='Tata Docomo (All Products)' where operator_master_id=52;

 update operator_master set is_active=0 where operator_master_id=21;
 
 update operator_master set operator_code='Tata Indicom DataCard' where operator_master_id=51;

-- rollback update operator_master set name='Tata Photon Whiz' where operator_master_id=51;
 
-- rollback update operator_master set name='Tata Docomo GSM' where operator_master_id=52; 
 
-- rollback update operator_master set is_active=1 where operator_master_id=21;

-- rollback update operator_master set operator_code='Tata Photon Whiz DataCard' where operator_master_id=51;