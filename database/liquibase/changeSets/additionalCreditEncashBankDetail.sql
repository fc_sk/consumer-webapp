-- liquibase formatted sql

-- changeset umakanth:additionalCreditEncashBankDetail

alter table fc_credit_encash_bank_detail add transfer_status varchar(20);
alter table fc_credit_encash_bank_detail add transfer_date timestamp NULL;
alter table fc_credit_encash_bank_detail add failure_reason varchar(250);

-- rollback ALTER TABLE fc_credit_encash_bank_detail DROP COLUMN transfer_status;
-- rollback ALTER TABLE fc_credit_encash_bank_detail DROP COLUMN transfer_date;
-- rollback ALTER TABLE fc_credit_encash_bank_detail DROP COLUMN failure_reason;
