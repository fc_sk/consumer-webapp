-- liquibase formatted sql

-- changeset raviande:freefundZeropayCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) 
values ('freefundZeropayCondition', 'freefundZeropayCondition', now(), now(), 'No param configuration required');

-- rollback delete from ar_condition_type where class_ident = 'freefundZeropayCondition'
