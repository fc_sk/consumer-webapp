-- liquibase formatted sql

-- changeset shwetanka:socialUser

create table social_user (
  user_id int(11) not null,
  email varchar (150) not null,
  provider_id varchar (100) not null,
  provider varchar (20) not null,
  access_token varchar (254) not null,
  expiry long default null,
  secret varchar (254) default null,
  display_name varchar (50) default null,
  image_url varchar (254) default null,
  created_on timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  updated_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  primary key (user_id, provider_id, provider)
) engine InnoDB;

create index idx_user_id on social_user (user_id);
create index idx_provider on social_user (provider);
create index idx_provider_id on social_user (provider_id);

-- rollback DROP TABLE IF EXISTS social_user;