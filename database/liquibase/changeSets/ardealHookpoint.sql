-- liquibase formatted sql

-- changeset shwetanka:ardealHookpoint

alter table ar_deal change hookpoint hookpoint varchar(255) default null;

-- rollback