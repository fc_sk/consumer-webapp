-- liquibase formatted sql

-- changeset shwetanka:userAgentARCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description, params) values ('User Agent Condition', 'userAgentCondition', now(), now(), 'Param accepted is - agents = list of user agents(chrome|firefox|ie)', '{"condKeys": {"agents": {"type": "list"}}}');

-- rollback delete from ar_condition_type where class_ident = 'userAgentCondition';
