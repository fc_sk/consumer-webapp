-- liquibase formatted sql

-- changeset anderavi:arCashbackReward
insert into ar_reward_type(name, class_ident, created_on) values('Cashback Reward', 'cashbackARReward', now());
-- rollback DELETE FROM ar_reward_type where class_ident='cashbackARReward';