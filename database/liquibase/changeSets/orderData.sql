CREATE TABLE order_data (
 id int(11) NOT NULL AUTO_INCREMENT,
 order_id varchar(20) NOT NULL,
 meta_data text NOT NULL,
 created_at datetime NOT NULL,
 updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (id),
 KEY idx_order_data_order_id (order_id)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 -- rollback DROP TABLE IF EXISTS order_data;