-- liquibase formatted sql

-- changeset divyansh:alter-m_coupon-add-fc_request_id

 alter table m_coupon add column fc_request_id   varchar(36)  default NULL after redeemed_request_id;


-- rollback ALTER TABLE m_coupon DROP COLUMN fc_request_id ;

