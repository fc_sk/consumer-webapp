--liquibase formatted sql

--changeset amaresh:freefundchannelcondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('facebooktotalnotificationscondition', 'facebooktotalnotificationscondition', now(), now(), 'params : totalNotificationsLimit. This is a per day Limit. ');

--rollback delete from ar_condition_type where class_ident = 'freefundchannelcondition'