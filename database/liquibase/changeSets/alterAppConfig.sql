-- liquibase formatted sql

-- changeset arun:alterAppConfig

ALTER TABLE `app_config` CHANGE COLUMN `app_value` `app_value` VARCHAR(2048);

-- rollback ALTER TABLE `app_config` CHANGE COLUMN `app_value` `app_value` VARCHAR(255);
