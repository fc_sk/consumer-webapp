-- liquibase formatted sql

-- changeset Shirish:myntra-condition

INSERT INTO ar_condition_type
	(name, class_ident, created_on, updated_on, description) 
	VALUES ('MyntraNewUserCondition', 'myntraNewUserCondition', NOW(), NOW(), 'Retruns true if email id passed is not registered with Myntra.');

insert into ar_condition (name, fk_ar_condition_type_id, params, created_on, updated_on, campaign_context)
values ('myntraNewUser', 15, '', now(), now(), 'ARDEAL')


-- rollback DELETE FROM ar_condition_type WHERE name = 'MyntraNewUserCondition' AND class_ident = 'myntraNewUserCondition';
-- rollback DELETE FROM ar_condition WHERE name = 'myntraNewUser';


