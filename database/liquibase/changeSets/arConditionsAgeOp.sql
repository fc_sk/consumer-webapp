-- liquibase formatted sql

-- changeset shwetanka:arConditionsAgeOp

INSERT INTO ar_condition_type SET name='AgeCondition', class_ident='ageARCondition', created_on=NOW();
INSERT INTO ar_condition_type SET name='OperatorCondition', class_ident='operatorARCondition', created_on=NOW();

-- rollback DELETE FROM ar_condition_type where name='AgeCondition' or name = 'OperatorCondition';
