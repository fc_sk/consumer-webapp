-- liquibase formatted sql

-- changeset GoyalVipin:site_autosuggest_prefix 

alter table site_autosuggest_prefix drop index idx_prefix;

-- rollback ALTER TABLE site_autosuggest_prefix ADD CONSTRAINT idx_prefix UNIQUE (prefix);
