-- liquibase formatted sql

-- changeset raviande:alter-opr-circle-mapping

alter table circle_operator_mapping add column aggr_priority varchar(255) default null;

-- rollback alter table circle_operator_mapping drop column aggr_priority

