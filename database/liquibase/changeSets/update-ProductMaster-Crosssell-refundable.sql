-- liquibase formatted sql

-- changeset shirish:update-ProductMaster-Crosssell-refundable

update product_master set is_refundable = 1 where product_name = 'Crosssell';

-- rollback update product_master set is_refundable = 0 where product_name = 'Crosssell';

