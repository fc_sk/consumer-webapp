-- liquibase formatted sql

-- changeset raviande:freefundTimeboundCondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) 
values ('FreefundTimeboundCondition', 'freefundTimeboundCondition', now(), now(), 'redemptions = 1..n, inhours = 1..n. In non-prod environment, inhours initialized to 30min');

-- rollback delete from ar_condition_type where class_ident = 'freefundTimeboundCondition'
