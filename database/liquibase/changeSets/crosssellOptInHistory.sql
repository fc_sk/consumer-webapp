-- liquibase formatted sql

-- changeset shirish:crosssellOptInHistory

create table crosssell_optin_history (
id int(11) NOT NULL AUTO_INCREMENT,
campaign_name varchar(255),
cross_sell_data text,  
cross_sell_label varchar(250),
cross_sell_price int(20),
cross_sell_valid_from date null default null,
cross_sell_valid_upto date null default null,
city_name_list text,
coupon_code varchar(150),
coupon_type varchar(3),
block_time datetime null default null,
is_complimentary tinyint(1),
is_unique tinyint(1),
is_m_coupon tinyint(1),
coupon_store_validity_date timestamp null default null,
order_id varchar(20),
created_on timestamp default current_timestamp,
user_id int(11),
primary key (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE INDEX idx_crosssell_history_user_id ON crosssell_optin_history(user_id) USING BTREE;

CREATE INDEX idx_crosssell_history_order_id ON crosssell_optin_history(order_id) USING BTREE;

CREATE INDEX idx_crosssell_history_created_on ON crosssell_optin_history(created_on) USING BTREE;

-- rollback DROP TABLE IF EXISTS crosssell_optin_history;