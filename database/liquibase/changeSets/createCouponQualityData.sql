-- liquibase formatted sql

-- changeset shwetanka:createCouponQualityData

CREATE TABLE `coupon_quality_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_store_id` int(11) NOT NULL,
  `rating` float(3,1) NOT NULL,
  `no_of_pick` bigint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

create index idx_coupon_store_id on coupon_quality_data (coupon_store_id);

-- rollback drop table coupon_quality_data