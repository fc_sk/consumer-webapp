-- liquibase formatted sql

-- changeset shirish:couponChannelCategory

CREATE TABLE coupon_category (
id int(11) NOT NULL AUTO_INCREMENT,
name varchar(50) NOT NULL,
display_title varchar(100) DEFAULT NULL, 
description varchar(500) DEFAULT NULL,
is_active tinyint(1) NOT NULL DEFAULT 0,
is_derived tinyint(1) NOT NULL DEFAULT 0,
image varchar(100) DEFAULT NULL,
is_custom_sequence tinyint(1) NOT NULL DEFAULT 0,
updated_on timestamp default current_timestamp on update current_timestamp,
primary key (id)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE coupon_category_data (
id int(11) NOT NULL AUTO_INCREMENT,
fk_coupon_category_id int(11) NOT NULL, 
fk_coupon_store_id int(11) NOT NULL,
sequence int(5) NOT NULL DEFAULT 0,
updated_on timestamp default current_timestamp on update current_timestamp,
primary key (id)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE coupon_channel_category (
id int(11) NOT NULL AUTO_INCREMENT,
channel_id int(5) NOT NULL,
affiliate_id int(5) NOT NULL,
fk_coupon_category_id int(11) NOT NULL, 
sequence int(5) NOT NULL DEFAULT 0,
updated_on timestamp default current_timestamp on update current_timestamp,
primary key (id)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE INDEX idx_coupon_category_data_fk_coupon_category_id ON coupon_category_data(fk_coupon_category_id) USING BTREE;
CREATE INDEX idx_coupon_category_data_sequence ON coupon_category_data(sequence) USING BTREE;

CREATE INDEX idx_coupon_channel_category_fk_channel_id ON coupon_channel_category(channel_id) USING BTREE;
CREATE INDEX idx_coupon_channel_category_fk_coupon_category_id ON coupon_channel_category(fk_coupon_category_id) USING BTREE;
CREATE INDEX idx_coupon_channel_category_sequence ON coupon_channel_category(sequence) USING BTREE;

-- rollback DROP TABLE coupon_category;
-- rollback DROP TABLE coupon_category_data;
-- rollback DROP TABLE coupon_channel_category;
