--liquibase formatted sql

-- changeset korathpaul:createRechargeReminder

drop table IF EXISTS user_recharge_reminder;

create table user_recharge_reminder (
	reminder_id BIGINT NOT NULL AUTO_INCREMENT,
	user_id INT,
	service_no VARCHAR(50),
	order_id varchar(30) NOT NULL,
	operator_name VARCHAR(100),
	reminder_date DATETIME,
	amount DOUBLE,
	action VARCHAR(20),
	pattern INT NOT NULL,
	is_active tinyint(1) unsigned,
	inserted_on DATETIME,
	PRIMARY KEY (`reminder_id`),
	UNIQUE (`order_id`),
  	KEY `idx_service_no` (`service_no`),
  	KEY `idx_reminder_date` (`reminder_date`),
  	KEY `idx_amount` (`amount`),
  	KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollback DROP TABLE user_recharge_reminder;