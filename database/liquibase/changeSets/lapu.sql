-- liquibase formatted sql

-- changeset vipin:lapu integration

insert into in_errorcode_map (aggr_name,aggr_resp_code,in_resp_code,in_error_msg,is_retryable,is_display_enabled) values('lapu','FAILED','07','Transaction Failed',0,0);
insert into in_errorcode_map (aggr_name,aggr_resp_code,in_resp_code,in_error_msg,is_retryable,is_display_enabled) values('lapu','SUCCESS','00','Transaction Successful',0,0);
insert into in_errorcode_map (aggr_name,aggr_resp_code,in_resp_code,in_error_msg,is_retryable,is_display_enabled) values('lapu','PENDING','08','Transaction UnderProcess',0,0);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',1,'4',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',2,'43',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',6,'3',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',5,'1',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',11,'7',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',14,'9',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',35,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',3,'10',23,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',10,'20',35,'*','topup','1',600000);



insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',13,'26',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',22,'13',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',44,'14',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',18,'19',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',16,'21',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',19,'22',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',20,'23',35,'*','topup','1',600000);


insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',1,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',2,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',3,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',4,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',5,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',6,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',7,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',8,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',9,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',10,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',11,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',12,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',13,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',14,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',15,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',16,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',17,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',18,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',19,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',20,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',21,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',22,'*','topup','1',600000);

insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',23,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',32,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',33,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',34,'*','topup','1',600000);
insert into in_aggr_opr_circle_map(aggr_name,fk_operator_id,aggr_operator_code,fk_circle_id,aggr_circle_code,recharge_plan_type,should_timeout,time_out_millis) values('lapu',4,'29',35,'*','topup','1',600000);




-- rollback delete from in_aggr_opr_circle_map where aggr_name='lapu' ;

