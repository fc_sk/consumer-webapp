-- liquibase formatted sql

alter table mat_event_data change column assigned_click_id assigned_click_id varchar(100)  default null;

-- rollback alter table mat_event_data change column assigned_click_id assigned_click_id varchar(50) default null;
