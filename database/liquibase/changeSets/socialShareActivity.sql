-- liquibase formatted sql

-- changeset shwetanka:socialShareActivity

insert into ar_action_type set name="FacebookShare", class_ident="facebookShareARAction", created_on = now(), updated_on = now();

create table ar_social_share_activity(
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) not null,
  ar_action_id int(11) not null,
  provider varchar (40) not null,
  activity varchar (20) not null,
  session_id varchar (40) not null,
  created_on timestamp NOT NULL DEFAULT 0,
  primary key (id)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollback delete from ar_action_type where class_ident = 'facebookShareARAction';
-- rollback DROP TABLE IF EXISTS ar_social_share_activity;
