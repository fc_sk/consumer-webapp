-- liquibase formatted sql

-- changeset vinaym:walletTxn

DROP TABLE IF EXISTS wallet_txn;
CREATE TABLE wallet_txn (
  WALLET_TRANSACTION_ID int(10) NOT NULL AUTO_INCREMENT,
  FK_USER_ID int(10) NOT NULL,
  ORDER_ID varchar(64) NOT NULL,
  TXN_AMOUNT decimal(10,2) NOT NULL,
  IS_DEBIT tinyint(1) NOT NULL,
  OLD_BALANCE decimal(10,2) NOT NULL,
  NEW_BALANCE decimal(10,2) NOT NULL,
  CREATED_TS datetime NOT NULL,
  caller_refrence varchar(128) DEFAULT NULL,
  fund_source varchar(20) DEFAULT NULL,
  fund_destination varchar(20) DEFAULT NULL,
  metadata varchar(256) DEFAULT NULL,
  transaction_type varchar(20) DEFAULT NULL,
  fk_wallet_id int(10) DEFAULT NULL,
  transaction_date datetime DEFAULT NULL,
  primary key(WALLET_TRANSACTION_ID),
  KEY idx_caller_reference (caller_refrence),
  KEY wallet_transaction_fk_user_i (FK_USER_ID),
  KEY idx_maf_wallet_transaction (ORDER_ID),
  KEY idx1_wall (CREATED_TS)
) ENGINE=InnoDB AUTO_INCREMENT=300000000, DEFAULT CHARSET=utf8;

-- rollback DROP TABLE IF EXISTS wallet_txn;
