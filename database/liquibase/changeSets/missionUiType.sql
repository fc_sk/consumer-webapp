-- liquibase formatted sql

-- changeset shwetanka:missionUiType

alter table ar_deal add column ui_type tinyint(2) default null;

-- rollback alter table ar_deal drop column ui_type;