--liquibase formatted sql

--changeset amaresh:freefundregisteredsincecondition

insert into ar_condition_type (name, class_ident, created_on, updated_on, description) values ('freefundregisteredsincecondition', 'freefundregisteredsincecondition', now(), now(), 'Params: minDays(user has to be registered before/after that day), flag(True for user has to be registered after the min days/ False for user has to be registered before)');

--rollback delete from ar_condition_type where class_ident = 'freefundchannelregisteredsincecondition'