-- liquibase formatted sql

-- changeset arun:inErrorCodeMap

alter table in_errorcode_map add is_display_enabled tinyint(1) not null default 0;

-- rollback alter table in_errorcode_map drop is_display_enabled;
