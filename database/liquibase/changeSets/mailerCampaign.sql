-- liquibase formatted sql

-- changeset priyatb:mailer_campaign

create table mailer_campaign  (
  id int(11) NOT NULL AUTO_INCREMENT,
  camp_name varchar(50) NOT NULL,
  fk_ar_condition_id int(11) NOT NULL,
  camp_start_date datetime NOT NULL,
  camp_end_date datetime,
  mailer_type varchar(40) NOT NULL,
  camp_priority mediumint(8) DEFAULT NULL,
  is_enabled tinyint(1) NOT NULL,
  camp_params text,
  created_on datetime not null,
  updated_on timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  KEY mailer_camp_start_date (camp_start_date),
  KEY mailer_fk_ar_condition_id (fk_ar_condition_id)
); 


alter table mailer_campaign change created_on created_on timestamp not null default '0000-00-00 00:00:00' ;

alter table ar_condition add campaign_context varchar(20);
update ar_condition set campaign_context = 'ARDEAL';

CREATE TABLE  mailer_campaign_activity (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  fk_mailer_campaign_id int(11) NOT NULL,
  lookup_id varchar(40) DEFAULT NULL,
  created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  KEY idx_mailer_campaign_user_id (user_id),
  KEY idx_mailer_campaign_fk_campaign_id (fk_mailer_campaign_id)
);

-- rollback drop table mailer_campaign;
-- rollback alter table ar_condition drop column campaign_context;
-- rollback drop table mailer_campaign_activity;