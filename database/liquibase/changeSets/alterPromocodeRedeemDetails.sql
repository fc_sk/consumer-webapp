-- liquibase formatted sql

-- changeset amaresh:addImeiNumberAndAdvertisementIdColumns

alter table promocode_redeem_details add column imei_number varchar(50) , add column advertisement_id varchar(50);

-- rollback alter table promocode_redeem_details drop column imei_number, drop column advertisement_id