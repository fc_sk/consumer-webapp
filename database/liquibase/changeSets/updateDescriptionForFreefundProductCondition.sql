-- liquibase formatted sql

-- changeset amaresh:updateDescriptionForFreefundProductCondition

update ar_condition_type set description='Params required are products (comma separated list - Mobile, DTH, DataCard, MobilePostpaid)'
where class_ident='freefundProductCondition';

-- rollback update ar_condition_type set description='Params required are products (comma separated list - Mobile, DTH, DataCard)' where class_ident='freefundProductCondition';
