-- liquibase formatted sql

-- changeset amaresh:additionalReward

create table additional_reward(
  id int(11) PRIMARY KEY AUTO_INCREMENT,
  rewardName varchar (200),
  freefundClassId int(11),
  rewardValue decimal(10, 2),
  rewardMaxDiscount decimal(10, 2),
  rewardConditionId int(11),
  rewardDiscountType varchar(20),
  isEnabled boolean,
  validFrom datetime,
  validUpto datetime
);

-- rollback drop table additional_reward;
