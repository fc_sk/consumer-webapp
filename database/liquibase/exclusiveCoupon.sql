-- liquibase formatted sql

-- changeset shirish:exclusiveCoupon

create table exclusive_coupon (
id int(11) NOT NULL AUTO_INCREMENT,
coupon_store_id int(11),
condition_id int(11), 
created_on timestamp default current_timestamp,
primary key (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- rollback DROP TABLE IF EXISTS exclusive_coupon;
