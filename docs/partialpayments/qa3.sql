| wallet_transaction | CREATE TABLE `wallet_transaction` (
  `caller_refrence` varchar(128) DEFAULT NULL,
  `fund_source` varchar(20) DEFAULT NULL,
  `fund_destination` varchar(20) DEFAULT NULL,
  `metadata` varchar(20) DEFAULT NULL,
  `transaction_type` varchar(20) DEFAULT NULL,
  `fk_wallet_id` int(10) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  PRIMARY KEY (`WALLET_TRANSACTION_ID`),
  KEY `wallet_transaction_fk_user_i` (`FK_USER_ID`),
  KEY `idx_caller_reference` (`caller_refrence`)
