| wallet_transaction | CREATE TABLE `wallet_transaction` (
  `WALLET_TRANSACTION_ID` int(10) NOT NULL AUTO_INCREMENT,
  `FK_USER_ID` int(10) NOT NULL,
  `ORDER_ID` varchar(20) NOT NULL,
  `TXN_AMOUNT` decimal(10,2) NOT NULL,
  `IS_DEBIT` tinyint(1) NOT NULL,
  `OLD_BALANCE` decimal(10,2) NOT NULL,
  `NEW_BALANCE` decimal(10,2) NOT NULL,
  `CREATED_TS` datetime NOT NULL,
  PRIMARY KEY (`WALLET_TRANSACTION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=211148 DEFAULT CHARSET=latin1 |
