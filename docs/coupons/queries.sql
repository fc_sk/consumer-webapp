select count(coupon_code_id), oid.created_on as  cnt, oid.order_id , fk_coupon_inventory_id from order_id oid join coupon_code cc on oid.order_id = cc.order_id where is_blocked = 0 and block_time is not null and oid.created_on >='2013-05-13' group by oid.order_id , fk_coupon_inventory_id having cnt >8;


select oid.order_id, ci.fk_item_id, ci.quantity, oid.created_on from cart c join cart_items ci on c.cart_id = ci.fk_cart_id join order_id oid on c.lookup_id = oid.lookup_id where ci.entity_id = 'site_merchant' and is_deleted = 0 and oid.created_on >='2013-05-13' and quantity > 8; 


SELECT 

cs.campaign_name as couponName, 
sum(tcs.quantity) as count, 
cs.coupon_value as couponValue, 
cs.face_value as faceValue, 
cs.coupon_type as couponType, 
cs.coupon_nature as couponNature,
cs.coupon_store_id as couponStoreId, 
cs.coupon_image_path as couponImagePath, 
cs.validity_date as expireDate, 
cs.terms_conditions as termsConditions, 
tcs.is_complimentary as isComplimentary, 
cs.coupon_hyperlink as couponhyperlink, 
cs.is_unique as isUnique 

FROM
 
txn_home_page thp LEFT JOIN txn_cross_sell tcs on tcs.txn_home_page_id = thp.id LEFT JOIN 
coupon_store cs on cs.coupon_store_Id = tcs.coupon_id LEFT JOIN 
coupon_inventory ci on ci.fk_coupon_store_id = cs.coupon_Store_Id 

WHERE 

thp.lookup_id = :lookupId and 
tcs.coupon_id <> 0 and 
tcs.is_deleted = 0 

group by couponName, couponValue, faceValue, couponType, couponNature, couponStoreId, couponImagePath, expireDate, termsConditions, isComplimentary, couponhyperlink, isUnique


select distinct oid.order_id, cs.coupon_store_id, cs.campaign_name, coi.coupon_inventory_id, cs.coupon_type, cs.is_unique from 


order_id oid join cart on oid.lookup_id=cart.lookup_id join 
cart_items ci on cart.cart_id = ci.fk_cart_id join 
coupon_store cs on ci.fk_item_id = cs.coupon_store_id join 
coupon_inventory coi on coi.fk_coupon_store_id = cs.coupon_store_id join 
coupon_code cc on cc.fk_coupon_inventory_id = coi.coupon_inventory_id join 
payment_rs prs on oid.order_id = prs.order_id join 
in_request inr on inr.affiliate_trans_id=oid.order_id join 
in_response inrs on inrs.request_id = inr.request_id 



where ci.fl_product_master_id = 4 and cs.is_unique=1 and prs.is_successful=1 and inrs.in_resp_code='00' and oid.order_id=:orderId




SELECT cs.campaign_name as couponName, sum(tcs.quantity) as count, cs.coupon_value as couponValue, cs.face_value as faceValue, cs.coupon_type as couponType, cs.coupon_nature as couponNature,cs.coupon_store_id as couponStoreId, cs.coupon_image_path as couponImagePath, cs.validity_date as expireDate, cs.terms_conditions as termsConditions, tcs.is_complimentary as isComplimentary, cs.coupon_hyperlink as couponhyperlink, cs.is_unique as isUnique FROM txn_home_page thp LEFT JOIN txn_cross_sell tcs on tcs.txn_home_page_id = thp.id LEFT JOIN coupon_store cs on cs.coupon_store_Id = tcs.coupon_id LEFT JOIN coupon_inventory ci on ci.fk_coupon_store_id = cs.coupon_Store_Id WHERE thp.lookup_id = :lookupId and tcs.coupon_id <> 0 and tcs.is_deleted = 0 group by couponName, couponValue, faceValue, couponType, couponNature, couponStoreId, couponImagePath, expireDate, termsConditions, isComplimentary, couponhyperlink, isUnique;
