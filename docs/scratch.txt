mysql> select pto.* from payment_type_options pto, payment_type_master ptm, payment_type_relation ptr where pto.fk_payment_type_relation_id = ptr.payment_type_relation_id  and ptr.fk_payment_type_id = ptm.payment_type_master_id and ( ptm.code = 'DC');
+-------------------------+-----------------------------+------------------+-------------+---------------------+------+-----------+----------+
| payment_type_options_id | fk_payment_type_relation_id | display_name     | display_img | img_url             | code | is_active | order_by |
+-------------------------+-----------------------------+------------------+-------------+---------------------+------+-----------+----------+
|                       4 |                           2 | Master Card      | NULL        | mastercard.png      | DMC  |         1 |        1 |
|                       5 |                           2 | Visa card        | NULL        | visacard_lrg.png    | DVI  |         1 |        2 |
|                      41 |                           5 | Master Card      | NULL        | mastercard.png      | DMC  |         1 |        1 |
|                      42 |                           5 | Visa card        | NULL        | visacard_lrg.png    | DVI  |         1 |        2 |
|                      57 |                           8 | Master Card      | NULL        | mastercard.png      | DMC  |         1 |        1 |
|                      58 |                           8 | Visa card        | NULL        | visacard_lrg.png    | DVI  |         1 |        2 |
|                     264 |                           2 | SBI Maestro      | NULL        | sbiMaestro.png      | DSME |         1 |        3 |
|                     265 |                           2 | Citibank Maestro | NULL        | citibankMaestro.png | DCME |         1 |        4 |
|                     266 |                           5 | SBI Maestro      | NULL        | sbiMaestro.png      | DSME |         1 |        3 |
|                     267 |                           5 | Citibank Maestro | NULL        | citibankMaestro.png | DCME |         1 |        4 |
|                     268 |                           8 | SBI Maestro      | NULL        | sbiMaestro.png      | DSME |         1 |        3 |
|                     269 |                           8 | Citibank Maestro | NULL        | citibankMaestro.png | DCME |         1 |        4 |
+-------------------------+-----------------------------+------------------+-------------+---------------------+------+-----------+----------+
12 rows in set (0.03 sec)

mysql> select pto.* from payment_type_options pto, payment_type_master ptm, payment_type_relation ptr where pto.fk_payment_type_relation_id = ptr.payment_type_relation_id  and ptr.fk_payment_type_id = ptm.payment_type_master_id and ( ptm.code = 'CC');
+-------------------------+-----------------------------+------------------+-------------+-------------------------+------+-----------+----------+
| payment_type_options_id | fk_payment_type_relation_id | display_name     | display_img | img_url                 | code | is_active | order_by |
+-------------------------+-----------------------------+------------------+-------------+-------------------------+------+-----------+----------+
|                       1 |                           1 | Master Card      | NULL        | mastercard.png          | MC   |         1 |        1 |
|                       2 |                           1 | Visa             | NULL        | visacard_lrg.png        | VI   |         1 |        2 |
|                       3 |                           1 | American Express | NULL        | americanexpresscard.png | AX   |         0 |        3 |
|                      38 |                           4 | Master Card      | NULL        | mastercard.png          | MC   |         1 |        1 |
|                      39 |                           4 | Visa             | NULL        | visacard_lrg.png        | VI   |         1 |        2 |
|                      40 |                           4 | American Express | NULL        | americanexpresscard.png | AX   |         1 |        3 |
|                      54 |                           7 | Master Card      | NULL        | mastercard.png          | MC   |         1 |        1 |
|                      55 |                           7 | Visa             | NULL        | visacard_lrg.png        | VI   |         1 |        2 |
|                      56 |                           7 | American Express | NULL        | americanexpresscard.png | AX   |         1 |        3 |
+-------------------------+-----------------------------+------------------+-------------+-------------------------+------+-----------+----------+

