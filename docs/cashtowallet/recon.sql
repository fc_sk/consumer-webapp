SELECT
  (select CONCAT(0, '_', request_id) from in_request where affiliate_trans_id = ord.order_id limit 1) as 'In Txn Id',
  ord.created_on as 'Date and Time',
  ord.order_id as 'order Id',
  txn_home.service_number as 'Service No',
  (select payment_gateway from payment_txn p where p.order_id = ord.order_id and p.pg_transaction_id is not NULL order by sent_to_pg desc limit 1) as payment_gateway1,
  (select mtxn_id from payment_txn p where p.order_id = ord.order_id order by sent_to_pg desc limit 1) as merchant_transaction_id,
  (select pg_transaction_id from payment_txn p where p.order_id = ord.order_id and p.pg_transaction_id is not NULL order by sent_to_pg desc limit 1) as pg_transaction_id,
  (select po.display_name from payment_txn p, payment_type_options po where p.order_id = ord.order_id  and p.payment_option = po.code order by sent_to_pg desc limit 1) as payment_option,
  (select pg_amount from payment_plan p where p.plan_reference = ord.order_id order by created_ts desc limit 1) as pg1_amount,
  (select payment_gateway from payment_txn p where p.order_id = ord.order_id and p.pg_transaction_id is NULL order by sent_to_pg desc limit 1) as payment_gateway2,
  (select wallet_amount from payment_plan p,payment_txn pt where p.plan_reference = ord.order_id and pt.order_id=p.plan_reference and pt.is_successful=1 order by created_ts desc limit 1) as pg2_amount,
  (select fund_source from wallet_transaction wt, payment_txn pt where pt.order_id = ord.order_id and pt.mtxn_id = wt.order_id and pt.payment_gateway <> 'fw' and pt.is_successful = 1 and wt.transaction_type = 'deposit' and fund_source <> 'REFUND' and wt.order_id <> 'resetAmountToWallet' and fund_source  <> 'CASHBACK' limit 1) as fund_source,
  (select amount from payment_txn p where p.order_id = ord.order_id order by sent_to_pg desc limit 1) as Total_payment_amount,
  (select is_successful from payment_txn p where p.order_id = ord.order_id order by sent_to_pg desc limit 1) as payment_status,
  (select recieved_from_pg from payment_txn p where p.order_id = ord.order_id order by sent_to_pg desc limit 1) as Payment_received,
  (select aggr_name from in_response irsp,in_request irt where irt.affiliate_trans_id = ord.order_id and irsp.request_id = irt.request_id order by irsp.created_time desc limit 1) as AG,
  (select aggr_trans_id from in_response irsp,in_request irt where irt.affiliate_trans_id = ord.order_id and irsp.request_id = irt.request_id order by irsp.created_time desc limit 1) as AG_Ref_No,
  (select operator from in_response irsp,in_request irt where irt.affiliate_trans_id = ord.order_id and irsp.request_id = irt.request_id order by irsp.created_time desc limit  1) as operator,
  (select circle from in_response irsp,in_request irt where irt.affiliate_trans_id = ord.order_id and irsp.request_id = irt.request_id order by irsp.created_time desc limit 1) as circle,
  (select ir.amount from in_response irsp,in_request irt where irt.affiliate_trans_id = ord.order_id and irsp.request_id = irt.request_id order by irsp.created_time desc limit 1) as AG_amount,
  (select irsp.in_resp_code from in_response irsp,in_request irt where irt.affiliate_trans_id = ord.order_id and irsp.request_id = irt.request_id order by irsp.created_time desc limit 1) as In_resp_code,
  (select irsp.aggr_resp_msg from in_response irsp,in_request irt where irt.affiliate_trans_id = ord.order_id and irsp.request_id = irt.request_id order by irsp.created_time desc limit 1) as aggr_message,
  (select irsp.in_resp_msg from in_response irsp,in_request irt where irt.affiliate_trans_id = ord.order_id and irsp.request_id = irt.request_id order by irsp.created_time desc limit 1) as in_resp_msg


 FROM

   order_id ord left join txn_home_page txn_home on ord.lookup_id = txn_home.lookup_id left join  in_request ir on ord.order_id = ir.affiliate_trans_id left join in_response irs on ir.request_id = irs.request_id where txn_home.creatred_on > '2013-04-04 00:00:00' and txn_home.creatred_on < '2013-04-08 23:59:59';
