#!/bin/bash
function log_error(){
    echo -e "### ERROR: $@"
    exit 127
}

cd ..
ant makeConsumerJar makeAdminConsumerJar
if [ $? -ne 0 ]; then
    log_error "### ERROR: Build Failed"
fi
cd -
echo "Consumer build done."
