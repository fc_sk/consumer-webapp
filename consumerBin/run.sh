#!/bin/sh

if [ -e "/etc/freecharge_env" ]; then
    SPRING_ENV=`cat /etc/freecharge_env`
fi

if [ -z "$SPRING_ENV" ]; then
    SPRING_ENV=dev
fi

if [[ "qa" == "$SPRING_ENV" ]]; then
   JAVA_OPTIONS=' -XX:+DoEscapeAnalysis -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=256m -Xms1024m -Xmx1024m '
fi

if [[ "prod" == "$SPRING_ENV" ]]; then
  JAVA_OPTIONS=' -Dhttps.protocols=TLSv1.2,SSLv3,TLSv1,TLSv1.1  -XX:+DoEscapeAnalysis -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=256m -Xms4096m -Xmx4096m '
fi

if [[ "qa" == "$SPRING_ENV" ]]; then
  JAVA_OPTIONS=' -XX:+DoEscapeAnalysis -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=256m -Xms1024m -Xmx1024m '
fi

if [[ "preProd" == "$SPRING_ENV" ]]; then
  JAVA_OPTIONS=' -XX:+DoEscapeAnalysis -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=256m -Xms1024m -Xmx1024m '
fi

echo ""
echo "SPRING_ENV: $SPRING_ENV"
echo ""

launch_daemon() {
  
  (java -Dconsumers.pidfile=consumers.pid -DLog4jContextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector -Dspring.profiles.active=${SPRING_ENV} -server ${JAVA_OPTIONS} -cp "../consumerJar/consumer.jar:../target/fcwebapp-1.0.jar:../webapp/WEB-INF/lib/*" com.freecharge.dop.framework.ConsumerFramework <& - &
  pid=$!
  echo ${pid})
}

daemon_pid=`launch_daemon`
if ps -p "${daemon_pid}" > /dev/null 2>&1
then
  #Daemon is running.
  echo ${daemon_pid} > consumers.pid
else
  echo "Daemon did not start."
fi

