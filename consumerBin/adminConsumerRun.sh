#!/bin/sh

launch_daemon() {
  #Do not get rid of the braces, this is done so that the process launces in a new sub shell
  (java -Dconsumers.pidfile=adminConsumer.pid -cp "../consumerJar/adminConsumer.jar:../consumerJar/consumer.jar:../target/fcwebapp-1.0.jar:../webapp/WEB-INF/lib/*" com.freecharge.dop.framework.ConsumerFramework <& - &
  pid=$!
  echo ${pid})
}

daemon_pid=`launch_daemon`
if ps -p "${daemon_pid}" > /dev/null 2>&1
then
  #Daemon is running.
  echo ${daemon_pid} > adminConsumer.pid
else
  echo "Daemon did not start."
fi

