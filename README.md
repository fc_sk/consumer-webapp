# webapp

## Setup

After cloning this Git repository, do the following

1. Create a log directory with appropriate permissions

```bash
$ sudo mkdir -p /var/data/logs
$ sudo chown -R $USER:$USER /var/data/logs
```

2. Install MySQL, Redis and MongoDB as appropriate and add properties to
   `conf/local.properties` to override properties from `conf/dev.properties`.

3. Clone the `images` repo

```bash
$ mkdir -p webapp/content
$ cd webapp/content
$ git@bitbucket.org:freecharge/images.git
```

4. Clone the `webassets` repo parallel to `webapp` repo and build the assets.
   Follow the instructions at `README.md` file in `webassets` repo.

```bash
$ git clone git@bitbucket.org:freecharge/webassets.git
$ vim gulp-conf.json  # set "assetsPath" to "/path/to/webapp-repo/webapp/content/"
$ gulp 
```

5. Create a file `$CATALINA_HOME/conf/Catalina/localhost/ROOT.xml` and populate
   the following content

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Context docBase="/absolute-path/to/webapp-repo/webapp" path="/">
  <Loader checkInterval="3" classname="org.apache.catalina.loader.StandardLoader"/>
</Context>
```

## Running

This is what you would be doing most of the time to run the webapp

```bash
$ $CATALINA_HOME/bin/shutdown.sh
$ $CATALINA_HOME/bin/startup.sh
$ ant build  # now go open URL http://localhost:8080 in browser
```

## License

Copyright (C) 2014, Accelyst Systems Private limited

This software is a sole property of ASPL (Accelyst Systems private Limited). No
part of this software may be directly/indirectly stored or transmitted in any
form unless explicitly authorized by ASPL.
