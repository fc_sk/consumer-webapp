#!/bin/bash

# Upload freefund coupons to db
#
#

ffclassid=105
ffvalue=20
minrechargevalue=50
promoent=all-payment-options

tstart=`date +%s`

RCol='\e[0m'
Gre='\e[0;32m'
Red='\e[0;31m'
Blu='\e[0;34m'

#run mysql
for f in `ls $1/$2*.csv`
do
    lst=`date +%s`
    echo -e "Loading - ${Red} $f"
    mysql -uroot -proot freecharge_test -e "load data infile '$f' into table freefund_coupon (@code) set freefund_class_id = 177 , freefund_code = @code , freefund_value = 20 , min_recharge_value = 10 , promo_entity = 'all-payment-options' , generated_date = CURRENT_TIMESTAMP;"
    len=`date +%s`
    echo -e "Time file: ${Blu} $f  ${Gre} `expr $len - $lst` ${RCol}"
done


tend=`date +%s`

echo "Total time: `expr $tend - $tstart`"