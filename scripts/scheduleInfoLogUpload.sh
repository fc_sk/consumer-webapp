#!/bin/bash
#/log -- the qa credentials
#fc.gw.rechargeplan.qa.retry -- the qa bucket
LOG_FOLDER=/var/log/fc-consumer-applog
BUCKET_NAME=fc.gw.recharge.schedule.status
BUCKET_REGION=ap-south-1
APP_USER=fc-consumer
HOST_NAME=$HOSTNAME
FILE_PREFIX=scheduleDetailsStatus.log

echo ' '
echo '--------------------------------------------------'
echo 'Archiving logs to S3. Current time: '$(date)
find $LOG_FOLDER -type f | grep "$FILE_PREFIX*$(date -d "-1 days" +"%Y-%m-%d")" | while read x; do
        echo ' '
        BUCKET_URL='s3://'$BUCKET_NAME'/'$APP_USER'/'$(date -d "-1 days" +"%Y/%m/%d")'/'$HOST_NAME'/'
        echo 'archiving '$x' to '$BUCKET_URL' --region '$BUCKET_REGION
        aws s3 cp "$x" $BUCKET_URL --region $BUCKET_REGION
        echo ' '
done
echo 'Done archiving logs to S3'
