#!/bin/bash
#
# Usage - ./minifyjs <js directory> <yui jar path>
#
#

red='\e[0;31m'
NC='\e[0m' # No Color

for f in `ls $1/*.js`
do
    java -jar $2 $f -o $f

    if [ $? -ne 0 ]; then
        echo -e "${red}Failed minification $f ${NC}"
    else
        echo "File $f minified"
    fi
done