__author__ = 'shwetanka'

'''
Sample usage - python freecharge.py -hlocalhost -ufreecharge -pfreecharge -eshwetanka.srivastava@freecharge.com
This will add/update user with given email with 'ALL' admin privileges.
'''

import sys
import getopt
try:
    import pymongo
except ImportError:
    print("Requires pymongo - sudo pip install pymongo")
    raise ImportError


host = 'localhost'
user = 'freecharge'
psw = 'freecharge'
email = 'shwetanka.srivastava@freecharge.com'

args = sys.argv

if len(args) > 1:
    args = args[1:]
    options, args = getopt.getopt(args, 'h:u:p:e:')

    for (opt, val) in options:
        if opt == '-h':
            host = val
        elif opt == '-u':
            user = val
        elif opt == '-p':
            psw = val
        elif opt == '-e':
            email = val

print("host: %s :: user: %s :: psw: %s :: email: %s" % (host, user, psw, email))

client = pymongo.MongoClient(host, 27017)
db = client.freecharge
db.authenticate(user, password=psw)
col = db.admin_user

col.update({'email': email.strip()}, {'$set': {'components': ['ALL']}}, upsert=True)
print("Done!")
