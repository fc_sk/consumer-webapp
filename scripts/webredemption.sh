#!/bin/sh
#exec 2>> /tmp/webredemption.log
#set -x::

alias connection='mysql -uroot -proot freecharge'
coupon_store_id=693
couponsToBlock=5
now=$(date +"%s")
orderId="test_${now}"
echo $orderId

merchant_id=$(connection -N -e "select fk_merchant_id from coupon_store where coupon_store_id = $coupon_store_id")
coupon_inventory_id=$(connection -N -e "select coupon_inventory_id from coupon_inventory where fk_coupon_store_id = $coupon_store_id")
sent_coupon_count=$(connection -N -e "select sent_coupon_count from coupon_inventory where fk_coupon_store_id = $coupon_store_id")

final_sent_coupon_count=`expr $sent_coupon_count + $couponsToBlock`

$(connection -e "update coupon_inventory set sent_coupon_count = $final_sent_coupon_count where coupon_inventory_id = $coupon_inventory_id")
$(connection -e "update coupon_code set is_blocked = 1, block_time = now(), order_id = '$orderId' where fk_coupon_inventory_id = $coupon_inventory_id and is_blocked = 0 limit $couponsToBlock")

BLOCKED_CODES_FILE="${coupon_store_id}_blocked_codes.txt"
COUPON_CODE_INSERTS="${coupon_store_id}_m_coupon.sql"

test -f $BLOCKED_CODES_FILE && rm -f $BLOCKED_CODES_FILE
test -f $COUPON_CODE_INSERTS && rm -f $COUPON_CODE_INSERTS

blockedCodes=$(connection -N -e "select coupon_code from coupon_code where fk_coupon_inventory_id = $coupon_inventory_id and order_id = '$orderId' and is_blocked = 1") 
test -n "$blockedCodes" &&  echo $blockedCodes  >> $BLOCKED_CODES_FILE

if [ -f $BLOCKED_CODES_FILE ]
 then 
	for code in `cat $BLOCKED_CODES_FILE` 
	   do echo "insert into m_coupon(coupon_code, fk_coupon_store_id, merchant_id, redeemption_state) values ('$code',$coupon_store_id,'$merchant_id','AVAILABLE');" >> $COUPON_CODE_INSERTS
	done
 else 
	echo $BLOCKED_CODES_FILE does not exist 
fi

if [ -f $COUPON_CODE_INSERTS ]
 then 
	connection < $COUPON_CODE_INSERTS
 else 
	echo  $COUPON_CODE_INSERTS does not exist 
fi

