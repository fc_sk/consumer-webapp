#!/bin/bash
# Must be bash - not any other shell

#
# Usage instructions
#
usage() {
    cat << ENDUSAGE

    Usage: $0 -d logdir -p logfile-prefix [-h]
	-d : path of log directory (eg: /var/data/logs)
        -p : prefix of the log file without the date string appended after rolling (eg: freecharge.log)
	-h : show me this help

    Moves log files into monthly directories. Also compresses the files.
    Assumes that daily rolling has been employed - and the rolled files are of the form freecharge.log-2012-02-28
ENDUSAGE
    exit 0
}

###################
# The main script
###################

LOGDIR=
FILEPREFIX=

while getopts "d:p:h" options; do
  case $options in
    d ) LOGDIR=$OPTARG;;
    p ) FILEPREFIX=$OPTARG;;
    h ) usage;;
  esac
done

if [ "X$LOGDIR" == "X" ]; then
    echo "The log dir must be specified using the -d parameter."
    usage
    exit 1
fi

if [ "X$FILEPREFIX" == "X" ]; then
    echo "The file prefix must be specified using the -p parameter."
    usage
    exit 1
fi

# Check if the log dir exists
if [ -e $LOGDIR ]; then
    echo "$LOGDIR exists. Good."
else
    echo "ERROR: $LOGDIR does not exist."
    exit 1
fi

# Now the real action begins
YEARMONTH=`date +%Y-%m`

ARCHIVEDIR=$LOGDIR/$YEARMONTH
mkdir -p $ARCHIVEDIR
if [ $? -ne 0 ]; then
    echo "Failed creating directory: $ARCHIVEDIR"
    exit 1
fi

shopt -s nullglob
for f in $LOGDIR/$FILEPREFIX.$YEARMONTH*
do
    gzip $f
    if [ $? -ne 0 ]; then
	echo "Failed compressing log file: $f"
	exit 1
    fi

    mv $f.gz $ARCHIVEDIR
    if [ $? -ne 0 ]; then
	echo "Failed moving log file: $f.gz to archive directory: $ARCHIVEDIR"
	exit 1
    fi
    echo "Compressed and moved log file: $f to archive directory: $ARCHIVEDIR"
done
