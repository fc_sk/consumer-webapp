#!/bin/bash

# Create a file with contents like this:
#
# build.branch=wallet-v2
# build.revision=7739
#

PROPFILE=conf/build.properties
CBFILE=conf/cacheBusting.properties

BRANCH=$(git branch | cut -d ' ' -f2)
REVISION=$(git log | head -1 | cut -f 2 -d ' ')
BUILDNODE=$(hostname)
BUILDDATE=$(date "+%I:%M %p, %d %b %Y")
CBHASH=$(git rev-parse HEAD)

rm -f $PROPFILE
echo build.branch=$BRANCH GIT > $PROPFILE
echo build.revision=$REVISION >> $PROPFILE
echo build.node=$BUILDNODE >> $PROPFILE
echo build.time=$BUILDDATE >> $PROPFILE
rm -f $CBFILE
echo version.no=$CBHASH > $CBFILE
