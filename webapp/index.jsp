<%@ page import="com.freecharge.common.framework.session.FreechargeContextDirectory" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- Redirected because we can't set the welcome page to a virtual URL. --%>
<jsp:forward page="app/home.htm" />
<%--<c:redirect url="app/home.htm" />--%>
