<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="row" >
	<div class="sixteen columns tabContainer" >
		<h1><spring:message code="my.account" text="#my.account#"/></h1>
        <ul class="tabTitle clearfix">
            <li><a href="mycredits.htm"><spring:message code="fc.money" text="#fc.money#"/></a></li>
            <li><a href="myrecharges.htm"><spring:message code="previous.recharges" text="#previous.recharges#"/></a></li>
            <li class="active"><a href="mycontacts.htm"><spring:message code="contacts" text="#contacts#"/></a></li>
            <li><a href="myprofile.htm"><spring:message code="profile" text="#profile#"/></a></li>
        </ul>
        <div class="content">
            <table class="zebra" id="basicContactWrap">
                <thead>
                    <tr>
                        <th width="75%"><spring:message code="all.contacts" text="#all.contacts#"/>  <br />
                            <p id="msgdiv" style="display: none;"></p>
                        </th>
                        <th width="25%"></th>
                    </tr>
                </thead>
                <tbody>
                    <c:if test="${empty userconts.contactslist}">
                        <tr>
                            <td colspan="2">
                                <p class="noContentMsg"><spring:message code="sorry.contactsnotfound.msg" text="#sorry.contactsnotfound.msg#"/></p>
                            </td>
                        </tr>
                    </c:if>
                    <c:forEach items="${userconts.contactslist}" var="contactslist" varStatus='total'>
                        <tr id="basicContactWrap_${total.count}">
                            <td>
                                <p class="accountpage-myContact-staticMode">
                                    <span id='staticname${total.count}' class="contactUnit">${contactslist.name}</span>
                                    <span class="color-lighter">|</span>
                                    <strong class="contactUnit-Number ">+91 <span id="staticno${total.count}">${contactslist.serviceno}</span></strong>
                                    <span class="color-lighter">|</span>
                                    <span id="staticopr${total.count}" class="contactUnit-Operator">${contactslist.operatorname}</span>
                                </p>
                                <div class="accountpage-myContact-editMode" style="display: none;">
                                    <form:form action="#" id="form${total.count}" modelAttribute="usercontact">
                                        <spring:bind path="name">
                                            <form:input path="name" id="contactname${total.count}"
                                                class="validate[required,funcCall[firstName],custom[onlyLetterSp]]  lf "
                                                value="${contactslist.name}" />
                                        </spring:bind>
                                        <input type='hidden' name='contactid' id='cid${total.count}' value='${contactslist.contactid}' />
                                        <span class="mobCD lf mobCDdisalbed">+91 <spring:bind path="serviceno">
                                            <form:input path="serviceno" id="contactno${total.count}"
                                                value="${contactslist.serviceno}"
                                                readonly="true"
                                                class="text-input  mobText "
                                                maxlength="10" />
                                        </spring:bind></span>
                                        <spring:bind path="operator">
                                            <form:select path="operator" class=" lf " id="selectoperator${total.count}">
                                                <c:forEach var='operators' items='${contactslist.operatorlist}'>
                                                    <c:choose>
                                                        <c:when test="${operators.operatorMasterId eq  contactslist.operator}">
                                                            <option selected="selected" value="${operators.operatorMasterId}">${operators.operatorCode}</option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="${operators.operatorMasterId}">${operators.operatorCode}</option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </form:select>
                                        </spring:bind>
                                        <spring:bind path="product">
                                            <form:input path="product" id="product${total.count}" value='1' type="hidden" />
                                        </spring:bind>
                                    </form:form>
                                </div>
                            </td>
                            <td>
                                <%--<span class="editOn"><a href="javascript:void(0)"  onClick="editContact(this, '${total.count}');">Edit</a>  <span class="color-lighter"> | </span> </span>
                                    <span class="editOff" style="display: none;"> <a href="javascript:void(0)" onClick="updateContact(this,'${total.count}')">Save</a>
                                    <span class="color-lighter"> | </span> <a href="javascript:void(0)" onClick="saveContact(this, '${total.count}');">Cancel</a>
                                    <span class="color-lighter"> | </span> </span> <a href="home.htm">Recharge Again</a>--%>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
   </div>
</div>