<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if gt IE 9 ]><html class="ie ie9plus" lang="en"> <![endif]-->
<!--[if !(IE)]><!-->
<%@page import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<html lang="en">
	<!--<![endif]-->
	<head>
		<style type="text/css">
			dev{display: none !important;}
		</style>
		<%@page pageEncoding="UTF-8" %>
		<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
		<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
		<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
		<%@page import="com.freecharge.common.util.FCConstants"%>
		<%@page import="com.freecharge.common.util.FCSessionUtil"%>
		<%@ page import="com.freecharge.web.util.WebConstants" %>
		<%@ page import="java.util.Map" %>
		<%@ page import ="org.apache.commons.lang.WordUtils" %>
		<%@ page import="com.freecharge.tracker.TrackerUtil" %>
		<%@ page contentType="text/html;charset=UTF-8" language="java" %>

		<title>Wallet</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
    	<meta name="msapplication-tap-highlight" content="no"/>
		<meta name="fragment" content="!">
		<link href="https://www.freecharge.in"/>
		<link rel="shortcut icon" href="${mt:keyValue("imgprefix2")}/images/favicon-new.ico?v=${mt:keyValue("version.no")}"/>

		<tiles:insertAttribute name="css"/>
		<!-- Schema markup for social links to show up in Google Knowledge Graph (v1.0) -->
		<script type="application/ld+json">
			{
				"@context" : "http://schema.org",
				"@type" : "Organization",
				"name" : "FreeCharge: recharge nahi freecharge",
				"url" : "https://www.freecharge.in",
				"logo" : "https://www.dropbox.com/s/mqxtm37m74l8nbz/Logo-150-x-150.png?dl=0",
				"sameAs" : [ "http://www.facebook.com/freecharge",
				"http://www.twitter.com/freecharge",
				"http://plus.google.com/+freecharge",
				"https://www.facebook.com/freecharge"]
			}
		</script>
		<base href="/app/wallet/" >
	</head>


    <c:if test="${mt:keyValue('system.ctx.mode') == 'prod'}">
	   <script type="text/javascript">
	    window.location.href = "../WEB-INF/views/errorpages/404.jsp";
	  </script>
    </c:if>
    
    <body class="">
		<div id="main"></div>
		<div id="wrapper">
			<div class="mobile-header">
				<div class="mobile-logo">
					Freecharge<span>Account</span>
				</div>
			</div>
			<div id="menuOverlay"></div>
			<div class="menubar-bg"></div>
			<div class="hamburger"></div>
			<div class="menubar">
				<div class="logo">
					<div class="short"></div>
					<div class="full">Freecharge<span>Account</span></div>
				</div>
				<ul class="main-nav">
					<li class="fgo" data-page="virtualCard">
						<div class="tip">Freecharge Go</div>
					</li>
					<li class="transactions" data-page="transactions">
						<div class="tip">Transactions</div>
					</li>
					<li class="cash" data-page="cash">
						<div class="tip">Cash</div>
					</li>
					<li class="cards" data-page="storedCards">
						<div class="tip">Saved Cards</div>
					</li>
					<li class="addressbook" data-page="addressbook">
						<div class="tip">Address Book</div>
					</li>
					<li class="banks" data-page="banks">
						<div class="tip">Banks</div>
					</li>
					<li class="statement" data-page="statement">
						<div class="tip">Download 6 months Statement</div>
					</li>
					<li class="profile" data-page="profile">
						<div class="tip">Profile</div>
					</li>
				</ul>

				<!-- <ul class="footer-nav">
					<li class="feedback">
						<div class="tip">Feedback</div>
					</li>
					<li class="about">
						<div class="tip">About</div>
					</li>
				</ul> -->
			</div>
			<div class="stage">
				<div class="dashboard" class="clearfix">
					<div class="shortView">
						<!-- Render Page here -->
					</div>
					<div id="fullView" class="full-view">
						<div class="widget banks full-size">
							<header>
								Bank Accounts
							</header>

							<ul>
								<li class="bank-logo bank-hdfc">
									<div class="tag">Hdfc0canscb</div>
									<h3 class="bank-name">HDFC Bank</h3>
									<small class="acc-no">03672938568</small>
									<small class="acc-name">Harish SB Account</small>
									<div class="actions">
										<span class="edit">Edit</span>
										<span class="delete">Delete</span>
									</div>
								</li>
								<li class="bank-logo bank-axis">
									<div class="tag">Axis00045</div>
									<h3 class="bank-name">AXIS Bank</h3>
									<small class="acc-no">03672938568</small>
									<small class="acc-name">Ravdeep SB Account</small>
									<div class="actions">
										<span class="edit">Edit</span>
										<span class="delete">Delete</span>
									</div>
								</li>
							</ul>
							<footer>
								Add account
							</footer>
						</div>
					</div>
				</div>
			</div>
			<div id="overlay" class="overlay">
				<span class="close-overlay cancel">&#xd7;</span>
				<div class="overlay-wrap">
					<!-- Overlay content will be rendered here -->
				</div>
			</div>
			<div id="notification">
				<div class="msg"></div>
				<div class="close"></div>

			</div>
		</div>
		<%
			FreechargeSession freechargesession = FreechargeContextDirectory.get().getFreechargeSession();
			Object login = false;
			String name = "";
			String email = "";
			if (freechargesession != null) {
				Map<String, Object> sessionData = freechargesession.getSessionData();
				if (sessionData != null)
					login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
				name = (String) sessionData.get(WebConstants.SESSION_USER_FIRSTNAME);
				if (name != null && !name.isEmpty())
					name = WordUtils.capitalize(name);
				email = (String) sessionData.get(WebConstants.SESSION_USER_EMAIL_ID);
			}
		%>

		<c:set  value="<%=login%>" var="login" />
		<c:set  value="<%=name%>" var="name" />
		<c:set  value="<%=email%>" var="email" />

		<%-- Show build information like branch, mode etc --%>
		<jsp:include page="../views/developer/build-information.jsp" />
		<script>
			var config = {
				imgPrefix1: '${mt:keyValue("imgprefix1")}',
				imgPrefix2: '${mt:keyValue("imgprefix2")}',
				version: '${mt:keyValue("version.no")}',
				templatePrefix: '${mt:keyValue("templateprefix")}',
				scriptUrl: '${mt:keyValue("jsprefix2")}',

				googleClientId: '${googleClientID}',
				googleRedirectUri: '${googleRedirectURI}',

				// login status
				loginStatus: '${login}',
				// build info
				releaseToken: '${releaseType}',
				// CSRF
				csrfToken: '${mt:csrfToken()}',
				//tracker related
				trackerVisitApi: '${trackerVisitApi}',
				userDetailsMap: JSON.parse('${userDetailsMap}'),
				walletAPIEndPoint: '${walletAPIEndPoint}',
				vCardAPIEndPoint:'${vCardAPIEndPoint}'
			};
		</script>
		<c:choose>
			<c:when test="${notLoggedIn}">
                <script>
				document.body.style.color = "#fff";
                if(window.location.hostname === "account.freecharge.in"){
                    window.location = "https://www.freecharge.in/desktop/login?from=accountui";
                }else {
                    window.location = "/desktop/login?from=accountui";
                }
                </script>
				<%--<div class="not-logged-container" style="position: fixed;top: 0;left: 0;width: 100%;height: 100%;z-index: 100;background-color: rgba(0,0,0,0.4);">--%>
					<%--<div class="modal-cont" style="width: 60%;min-width: 320px;margin: 60px auto;background-color: #fff;padding: 24px;border-radius: 6px;font-size: 18px;text-align: center;color: #454545;">--%>
						<%--<p class="text">You are not logged in currently, please log in and refresh this page.</p>--%>
					<%--</div>--%>
				<%--</div>--%>
			</c:when>
			<c:otherwise>
				<div class="not-logged-container" style="position: fixed;top: 0;left: 0;width: 100%;height: 100%;z-index: 100;background-color: rgba(0,0,0,0.4); display: none;">
					<div class="modal-cont" style="width: 60%;min-width: 320px;margin: 60px auto;background-color: #fff;padding: 24px;border-radius: 6px;font-size: 18px;text-align: center;color: #454545;">
						<p class="text"></p>
					</div>
				</div>
				<tiles:insertAttribute name="js"/>
			</c:otherwise>
		</c:choose>
		<!-- <c:if test="${notLoggedIn}">
		</c:if> -->
		<div class="ghost" style="display: none;">
			<form class="ghost-form" method="post">
				<input class="dummy">
			</form>
		</div>
			<c:if test = "${mt:keyValue('system.ctx.mode') == 'prod'}">
				<script>
					var chType = helpers.getParameterByName('fcChannelType');
					var source;
					var subsource = 'NA';
					if(chType != ""){
						source = 'FC';
						if(chType === '5'){
							subsource = 'ios';
						} else if(chType === '6'){
							subsource = 'windows';
						} else if(chType === '11'){
							subsource = 'web';
						} else if(chType === '12'){
							subsource = 'mweb';
						} else if(chType === '13'){
							subsource = 'android';
						}
					} else{
						source = 'SD';
					}
					if(!window._fcTrackerEventQueue){
			            window._fcTrackerEventQueue = []
			        }
			        window._fcTrackerEventQueue.push({eventName: 'oc_account', val0: source, val1: subsource});
					
					<%-- This data is pushed to tracker controller in trackerjs.jsp --%>
					var TRACKER_CLIENT_DATA = (function() {
						var userAgent = navigator.userAgent;
						var referrer = document.referrer;
						var params = location.search;
	
						return {
							"<%=TrackerUtil.CLIENT_DETAILS_PARAM_USER_AGENT%>": userAgent,
							"<%=TrackerUtil.CLIENT_DETAILS_PARAM_REFERRER%>": referrer,
							"<%=TrackerUtil.CLIENT_DETAILS_PARAM_PARAMS%>": params
						};
					})();
	
					(function(){
						var headID = document.getElementsByTagName("head")[0];
				      var newScript = document.createElement('script');
				      newScript.type = 'text/javascript';
				      // remove script after load
				      newScript.src = 'https://tracker.freecharge.in/api/1/tracker/?v=1&channelId=2';
				      headID.appendChild(newScript);
					})();
				</script>
			</c:if>
			<script type="text/javascript">
				
			</script>
	</body>
</html>
