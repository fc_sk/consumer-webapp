<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
	<%@ page import="com.freecharge.tracker.TrackerUtil" %>
<style type="text/css">
	*{
		margin: 0;
		padding: 0;
	}
	body{
		background-color: #efefef;
		font-family: verdana;
	}
	.center{
	    position: absolute;
	    top: 50%;
	    left: 50%;
	    width: 300px;
	    margin-left: -150px;
	    margin-top: -150px;
	}
	.left{
		float: left;
	}
	.msg{
		text-align: center;
	}
	.heading{
		font-size: 72px;
		line-height: .8;
		margin-bottom: 10px;
		color : #666;
	}
	.smalls{
		display: inline-block;
		width: 100%;
		font-size: 14px;
		text-align: center;
		margin: 12px auto;
	}
	a{
		color : #65C3DF;
		text-decoration: none;
	}
	.icon{
		height: 70px;
		width: 70px;
		background: transparent url('/content/images/tick3.png') no-repeat center center;
		margin: 0 auto;
	}
</style>
</head>
<body>
	
<div class="center">
	<div class="icon"></div>
	<div class="msg left">
		<div class="heading">success</div>
		<div class="smalls">your transaction was successful, click <a href="/app/wallet?reload=true">here</a> to go back</div>
	</div>
</div>
<c:if test = "${mt:keyValue('system.ctx.mode') == 'prod'}">
	<script>
		if(!window._fcTrackerEventQueue){
		    window._fcTrackerEventQueue = []
		}
		window._fcTrackerEventQueue.push({eventName : 'oc_txn_status', val0: 'success'});
		<%-- This data is pushed to tracker controller in trackerjs.jsp --%>
		var TRACKER_CLIENT_DATA = (function() {
			var userAgent = navigator.userAgent;
			var referrer = document.referrer;
			var params = location.search;
		
			return {
				"<%=TrackerUtil.CLIENT_DETAILS_PARAM_USER_AGENT%>": userAgent,
				"<%=TrackerUtil.CLIENT_DETAILS_PARAM_REFERRER%>": referrer,
				"<%=TrackerUtil.CLIENT_DETAILS_PARAM_PARAMS%>": params
			};
		})();
		
		(function(){
			var headID = document.getElementsByTagName("head")[0];
			var newScript = document.createElement('script');
			newScript.type = 'text/javascript';
			// remove script after load
			newScript.src = 'https://tracker.freecharge.in/api/1/tracker/?v=1&channelId=2';
			headID.appendChild(newScript);

			window.location = "https://www.freecharge.in/accounts/walletsuccess";
		})();			
	</script>
</c:if>
</body>
</html>