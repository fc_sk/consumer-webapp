<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:choose>
    <c:when test = "${ mt:keyValue('system.ctx.mode') == 'prod' or mt:keyValue('system.ctx.mode') == 'preProd' or mt:keyValue('system.ctx.mode') == 'qa' or mt:keyValue('system.ctx.mode') == 'e2e'}" >
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/dist/js/all.libs.js"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/dist/templates/wallet-templates.js"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/dist/templates/wallet-partials.js"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/dist/js/all.min.js"></script>
    </c:when>

    <c:otherwise>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/libs/pace.min.js"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/libs/jquery-1.11.3.min.js"></script>
    <!-- <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/libs/mockajax.js"></script> -->
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/libs/page.js"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/libs/stapes.min.js"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/libs/handlebars.runtime-v1.3.0.js"></script>

    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/dist/templates/wallet-templates.js"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/dist/templates/wallet-partials.js"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/View.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/Model.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/routes.js?v=${mt:keyValue("version.no")}"></script>

    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/SideNav.js?v=${mt:keyValue("version.no")}"></script>


    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/app.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/render.js?v=${mt:keyValue("version.no")}"></script>



    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/TransactionsViewScript.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/CardsViewScript.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/BanksViewScript.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/ContactViewScript.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/CashViewScript.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/ProfileViewScript.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/VirtualCardViewScript.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/libs/validate.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/Controller.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/walletcheck.js?v=${mt:keyValue("version.no")}"></script>
    </c:otherwise>

</c:choose>
