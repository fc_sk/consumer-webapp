<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/whyupgrade/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/whyupgrade/skrollr.min.js"></script>
<script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/whyupgrade/owl.carousel.min.js"></script>
<script type="text/javascript" src="${mt:keyValue("walletprefix")}/js/whyupgrade/main.js"></script>