<!DOCTYPE html>

<%@page import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<html lang="en">
    <head>
        <style type="text/css">
            dev{display: none !important;}
        </style>
        <%@page pageEncoding="UTF-8" %>
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
        <%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
        <%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
        <%@page import="com.freecharge.common.util.FCConstants"%>
        <%@page import="com.freecharge.common.util.FCSessionUtil"%>
        <%@ page import="com.freecharge.web.util.WebConstants" %>
        <%@ page import="java.util.Map" %>
        <%@ page import ="org.apache.commons.lang.WordUtils" %>
        <%@ page import="com.freecharge.tracker.TrackerUtil" %>
        <%@ page contentType="text/html;charset=UTF-8" language="java" %>

        <title>Upgrade Account</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
        <meta name="msapplication-tap-highlight" content="no"/>
        <meta name="fragment" content="!">
        <link rel="shortcut icon" href="${mt:keyValue("imgprefix2")}/images/favicon-new.ico?v=${mt:keyValue("version.no")}"/>

        <tiles:insertAttribute name="css"/>

        <base href="/app/whyupgrade" >
    </head>

    <body>
            <div id="container" class="desktop">
        <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/logo.png?v=${mt:keyValue("version.no")}" class="logo" 
        data-0="-webkit-transform: translate(-50%,-100%) scale(0.5);
                -o-transform: translate(-50%,-100%) scale(0.5);
                -moz-transform: translate(-50%,-100%) scale(0.5);
                -ms-transform: translate(-50%,-100%) scale(0.5);
                transform: translate(-50%,-100%) scale(0.5);
                left: 10%; top: 15%;"

        data-1500="-webkit-transform: translate(-50%,-100%) scale(0.5);
                    -moz-transform: translate(-50%,-100%) scale(0.5);
                    -o-transform: translate(-50%,-100%) scale(0.5);
                    -ms-transform: translate(-50%,-100%) scale(0.5);
                    transform: translate(-50%,-100%) scale(0.5);
                     left: 10%; top: 15%;">

        <div class="scene scene0">
            <!-- <h1><img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/logo.png?v=${mt:keyValue("version.no")}"> <span>Account</span></h1> -->
            <h1>#futureofmoney</h1>
            <div id="scroll">
                <h4>Scroll Down</h4>
                <span>
                    <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/caret_down.png?v=${mt:keyValue("version.no")}">
                </span>
            </div>
        </div>

    

        <!-- Scene 1  =  Access Anywhere -->
        <div class="scene scene1"  
            data-0="-webkit-transform: translateY(100%);
                    -moz-transform: translateY(100%);
                    -o-transform: translateY(100%);
                    -ms-transform: translateY(100%);
                    transform: translateY(100%);" 
            data-500="-webkit-transform: translateY(100%);
                        -moz-transform: translateY(100%);
                        -o-transform: translateY(100%);
                        -ms-transform: translateY(100%);
                        transform: translateY(100%);"
            data-1500="-webkit-transform: translateY(0%);
                        -moz-transform: translateY(0%);
                        -o-transform: translateY(0%);
                        -ms-transform: translateY(0%);
                        transform: translateY(0%);" 
            data-3000="-webkit-transform: translateY(0%);
                        -moz-transform: translateY(0%);
                        -o-transform: translateY(0%);
                        -ms-transform: translateY(0%);
                        transform: translateY(0%);"
            data-4000="-webkit-transform: translateY(-40%);
                        -moz-transform: translateY(-40%);
                        -o-transform: translateY(-40%);
                        -ms-transform: translateY(-40%);
                        transform: translateY(-40%);" >
            <div class="bg">
                <div></div>
                <div></div>
                <div></div>
            </div>
            <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene1/shop1.png?v=${mt:keyValue("version.no")}" class="bg-store"
                data-1500="opacity: 0;"
                data-1800="opacity: 0.08;">
            <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene1/shop2.png?v=${mt:keyValue("version.no")}" class="fg-store"
                data-1300="-webkit-transform: translateX(-800px);
                            -moz-transform: translateX(-800px);
                            -o-transform: translateX(-800px);
                            -ms-transform: translateX(-800px);
                            transform: translateX(-800px);"
                data-1700="-webkit-transform: translateX(0px);
                            -moz-transform: translateX(0px);
                            -o-transform: translateX(0px);
                            -ms-transform: translateX(0px);
                            transform: translateX(0px);">
            <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene1/girl.png?v=${mt:keyValue("version.no")}" class="girl"
                data-1300="-webkit-transform: translateX(-800px);
                            -moz-transform: translateX(-800px);
                            -o-transform: translateX(-800px);
                            -ms-transform: translateX(-800px);
                            transform: translateX(-800px);"

                data-1700="-webkit-transform: translateX(0px);
                            -moz-transform: translateX(0px);
                            -o-transform: translateX(0px);
                            -ms-transform: translateX(0px);
                            transform: translateX(0px);">
            <div class="content"
                data-1800="-webkit-transform: translateX(800px);
                            -moz-transform: translateX(800px);
                            -o-transform: translateX(800px);
                            -ms-transform: translateX(800px);
                            transform: translateX(800px);"
                data-2000="-webkit-transform: translateX(0px);
                            -moz-transform: translateX(0px);
                            -o-transform: translateX(0px);
                            -ms-transform: translateX(0px);
                            transform: translateX(0px);">
                <h3>Access Anywhere</h3>
                <p>
                    Experience freedom from cash. Now with one account, pay at Snapdeal, FreeCharge and an ever growing list of places
                </p>
            </div>
        </div>



        <!-- Scene 2  =  Save time -->  
        <div class="scene scene2"
            data-0="-webkit-transform: translateY(100%);
                    -moz-transform: translateY(100%);
                    -o-transform: translateY(100%);
                    -ms-transform: translateY(100%);
                    transform: translateY(100%);" 
            data-3000="-webkit-transform: translateY(100%);
                        -moz-transform: translateY(100%);
                        -o-transform: translateY(100%);
                        -ms-transform: translateY(100%);
                        transform: translateY(100%);"
            data-4000="-webkit-transform: translateY(0%);
                        -moz-transform: translateY(0%);
                        -o-transform: translateY(0%);
                        -ms-transform: translateY(0%);
                        transform: translateY(0%);" 
            data-7500="-webkit-transform: translateY(0%);
                        -moz-transform: translateY(0%);
                        -o-transform: translateY(0%);
                        -ms-transform: translateY(0%);
                        transform: translateY(0%);" 
            data-8500="-webkit-transform: translateY(-40%);
                        -moz-transform: translateY(-40%);
                        -o-transform: translateY(-40%);
                        -ms-transform: translateY(-40%);
                        transform: translateY(-40%);" >

            <div class="bg">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>

            <div class="buildings"
                data-3000="-webkit-transform: translateX(0%);
                            -moz-transform: translateX(0%);
                            -o-transform: translateX(0%);
                            -ms-transform: translateX(0%);
                            transform: translateX(0%);
                            opacity: 0; "
                data-4000="-webkit-transform: translateX(0%);
                            -moz-transform: translateX(0%);
                            -o-transform: translateX(0%);
                            -ms-transform: translateX(0%);
                            transform: translateX(0%); opacity: 0;"
                data-6000="-webkit-transform: translateX(-5%);
                            -moz-transform: translateX(-5%);
                            -o-transform: translateX(-5%);
                            -ms-transform: translateX(-5%);
                            transform: translateX(-5%); opacity: 0.2;">
            </div>

            <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene2/bus.png?v=${mt:keyValue("version.no")}" class="bus"
                data-5000="-webkit-transform: translateX(500px);
                            -moz-transform: translateX(500px);
                            -o-transform: translateX(500px);
                            -ms-transform: translateX(500px);
                            transform: translateX(500px);"
                data-6000="-webkit-transform: translateX(0px);
                            -moz-transform: translateX(0px);
                            -o-transform: translateX(0px);
                            -ms-transform: translateX(0px);
                            transform: translateX(0px);"
                data-7000="-webkit-transform: translateX(0px);
                            -moz-transform: translateX(0px);
                            -o-transform: translateX(0px);
                            -ms-transform: translateX(0px);
                            transform: translateX(0px);"
                data-7600="-webkit-transform: translateX(500px);
                            -moz-transform: translateX(500px);
                            -o-transform: translateX(500px);
                            -ms-transform: translateX(500px);
                            transform: translateX(500px);">

            <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene2/tree.png?v=${mt:keyValue("version.no")}" class="tree"
                data-4500="-webkit-transform: translateX(1000px);
                            -moz-transform: translateX(1000px);
                            -o-transform: translateX(1000px);
                            -ms-transform: translateX(1000px);
                            transform: translateX(1000px);"
                data-6000="-webkit-transform: translateX(0px);
                            -moz-transform: translateX(0px);
                            -o-transform: translateX(0px);
                            -ms-transform: translateX(0px);
                            transform: translateX(0px);">

            <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene2/man.png?v=${mt:keyValue("version.no")}" class="man"
                data-4500="-webkit-transform: translateX(1000px);
                            -moz-transform: translateX(1000px);
                            -o-transform: translateX(1000px);
                            -ms-transform: translateX(1000px);
                            transform: translateX(1000px);"
                data-6000="-webkit-transform: translateX(0px);
                            -moz-transform: translateX(0px);
                            -o-transform: translateX(0px);
                            -ms-transform: translateX(0px);
                            transform: translateX(0px);">

            <div class="content"
                data-6000="opacity: 0;"
                data-6500="opacity: 1;">
                <h3>Save Time</h3>
                <p>
                    Zip through checkout queues in under 10 seconds with pre-filled addresses and card details to focus on what you love
                </p>
            </div>
        </div>


        <!-- Scene 3  =  Earn Rewards -->
        <div class="scene scene3"
            data-0="-webkit-transform: translateY(100%);
                    -moz-transform: translateY(100%);
                    -o-transform: translateY(100%);
                    -ms-transform: translateY(100%);
                    transform: translateY(100%);" 
            data-7500="-webkit-transform: translateY(100%);
                        -moz-transform: translateY(100%);
                        -o-transform: translateY(100%);
                        -ms-transform: translateY(100%);
                        transform: translateY(100%);"
            data-8500="-webkit-transform: translateY(0%);
                        -moz-transform: translateY(0%);
                        -o-transform: translateY(0%);
                        -ms-transform: translateY(0%);
                        transform: translateY(0%);"
            data-10000="-webkit-transform: translateY(0%);
                        -moz-transform: translateY(0%);
                        -o-transform: translateY(0%);
                        -ms-transform: translateY(0%);
                        transform: translateY(0%);"
            data-11000="-webkit-transform: translateY(-40%);
                        -moz-transform: translateY(-40%);
                        -o-transform: translateY(-40%);
                        -ms-transform: translateY(-40%);
                        transform: translateY(-40%);">
            <div class="bg">
                <div></div>
                <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene3/gifts.png?v=${mt:keyValue("version.no")}" class="gifts"
                    data-7500="-webkit-transform: translateY(500px)
                                -moz-transform: translateY(500px)
                                -o-transform: translateY(500px)
                                -ms-transform: translateY(500px)
                                transform: translateY(500px);"
                    data-8200="-webkit-transform: translateY(0px);
                                -moz-transform: translateY(0px);
                                -o-transform: translateY(0px);
                                -ms-transform: translateY(0px);
                                transform: translateY(0px);">
                <div></div>
            </div>

            <div class="wall"
                data-8000="opacity: 0;"
                data-8500="opacity: 0.2;">
        </div>

            
            

            <div class="light" 
                data-7200="-webkit-transform: translateX(-1000px);
                            -moz-transform: translateX(-1000px);
                            -o-transform: translateX(-1000px);
                            -ms-transform: translateX(-1000px);
                            transform: translateX(-1000px);"
                data-8000="-webkit-transform: translateX(0px);
                            -moz-transform: translateX(0px);
                            -o-transform: translateX(0px);
                            -ms-transform: translateX(0px);
                            transform: translateX(0px);">

                <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene3/ray.png?v=${mt:keyValue("version.no")}" class="ray"
                    data-8900="opacity: 0;"
                    data-9000="opacity: 1;">

                <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene3/light.png?v=${mt:keyValue("version.no")}" class="bulb">
                
            </div>

            <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene3/man.png?v=${mt:keyValue("version.no")}" class="man"
                data-8000="-webkit-transform: translateX(-1000px);
                            -moz-transform: translateX(-1000px);
                            -o-transform: translateX(-1000px);
                            -ms-transform: translateX(-1000px);
                            transform: translateX(-1000px);"
                data-9000="-webkit-transform: translateX(0px);
                            -moz-transform: translateX(0px);
                            -o-transform: translateX(0px);
                            -ms-transform: translateX(0px);
                            transform: translateX(0px);">
            <div class="content"
                data-8500="-webkit-transform: translateX(500px);
                            -moz-transform: translateX(500px);
                            -o-transform: translateX(500px);
                            -ms-transform: translateX(500px);
                            transform: translateX(500px);"
                data-9500="-webkit-transform: translateX(0px);
                            -moz-transform: translateX(0px);
                            -o-transform: translateX(0px);
                            -ms-transform: translateX(0px);
                            transform: translateX(0px);">
                <h3>Earn Rewards</h3>
                <p>
                    Access exclusive goodies from SnapDeal, FreeCharge and our amazing partner ecosystem for making FreeCharge Payments
                </p>
            </div>
        </div>

        <!-- Scene 4  =  Sleep Easy -->
        <div class="scene scene4"
            data-0="-webkit-transform: translateY(100%);
                    -moz-transform: translateY(100%);
                    -o-transform: translateY(100%);
                    -ms-transform: translateY(100%);
                    transform: translateY(100%);" 
            data-10000="-webkit-transform: translateY(100%);
                        -moz-transform: translateY(100%);
                        -o-transform: translateY(100%);
                        -ms-transform: translateY(100%);
                        transform: translateY(100%);"
            data-11000="-webkit-transform: translateY(0%);
                        -moz-transform: translateY(0%);
                        -o-transform: translateY(0%);
                        -ms-transform: translateY(0%);
                        transform: translateY(0%);" >
            <div class="bg">
                <div></div>
            </div>

            
            <div class="cot">
                <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene4/bed.png?v=${mt:keyValue("version.no")}" class="bed">
                <div class="table"
                    data-10500="-webkit-transform: translateY(-500px);
                                -moz-transform: translateY(-500px);
                                -o-transform: translateY(-500px);
                                -ms-transform: translateY(-500px);
                                transform: translateY(-500px);"
                    data-11500="-webkit-transform: translateY(0px);
                                -moz-transform: translateY(0px);
                                -o-transform: translateY(0px);
                                -ms-transform: translateY(0px);
                                transform: translateY(0px);">


                    <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene4/laptop.png?v=${mt:keyValue("version.no")}" class="laptop"
                        data-11300="-webkit-transform: translateY(-300px);
                                    -moz-transform: translateY(-300px);
                                    -o-transform: translateY(-300px);
                                    -ms-transform: translateY(-300px);
                                    transform: translateY(-300px);"
                        data-11800="-webkit-transform: translateY(0px);
                                    -moz-transform: translateY(0px);
                                    -o-transform: translateY(0px);
                                    -ms-transform: translateY(0px);
                                    transform: translateY(0px);">


                    <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene4/wallet.png?v=${mt:keyValue("version.no")}" class="wallet"
                        data-11200="-webkit-transform: translateY(-300px);
                                    -moz-transform: translateY(-300px);
                                    -o-transform: translateY(-300px);
                                    -ms-transform: translateY(-300px);
                                    transform: translateY(-300px);"
                        data-11700="-webkit-transform: translateY(0px);
                                    -moz-transform: translateY(0px);
                                    -o-transform: translateY(0px);
                                    -ms-transform: translateY(0px);
                                    transform: translateY(0px);">


                    <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene4/cup.png?v=${mt:keyValue("version.no")}" class="cup"
                        data-11150="-webkit-transform: translateY(-300px);
                                    -moz-transform: translateY(-300px);
                                    -o-transform: translateY(-300px);
                                    -ms-transform: translateY(-300px);
                                    transform: translateY(-300px);"
                        data-11650="-webkit-transform: translateY(0px);
                                    -moz-transform: translateY(0px);
                                    -o-transform: translateY(0px);
                                    -ms-transform: translateY(0px);
                                    transform: translateY(0px);">


                    <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene4/coins.png?v=${mt:keyValue("version.no")}" class="coins"
                        data-11100="-webkit-transform: translateY(-300px);
                                    -moz-transform: translateY(-300px);
                                    -o-transform: translateY(-300px);
                                    -ms-transform: translateY(-300px);
                                    transform: translateY(-300px);"
                        data-11600="-webkit-transform: translateY(0px);
                                    -moz-transform: translateY(0px);
                                    -o-transform: translateY(0px);
                                    -ms-transform: translateY(0px);
                                    transform: translateY(0px);">


                    <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene4/mobile.png?v=${mt:keyValue("version.no")}" class="mobile"
                        data-11200="-webkit-transform: translateY(-300px);
                                    -moz-transform: translateY(-300px);
                                    -o-transform: translateY(-300px);
                                    -ms-transform: translateY(-300px);
                                    transform: translateY(-300px);"
                        data-11700="-webkit-transform: translateY(0px);
                                    -moz-transform: translateY(0px);
                                    -o-transform: translateY(0px);
                                    -ms-transform: translateY(0px);
                                    transform: translateY(0px);">


                </div>
                <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene4/blanket.png?v=${mt:keyValue("version.no")}" class="blanket"
                    data-10500="-webkit-transform: translateY(100px);
                                -moz-transform: translateY(100px);
                                -o-transform: translateY(100px);
                                -ms-transform: translateY(100px);
                                transform: translateY(100px);"
                    data-11400="-webkit-transform: translateY(0px);
                                -moz-transform: translateY(0px);
                                -o-transform: translateY(0px);
                                -ms-transform: translateY(0px);
                                transform: translateY(0px);">
            </div>

            <!-- <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene3/ray.png?v=${mt:keyValue("version.no")}" class="ray"> -->
            <!-- <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene3/gifts.png?v=${mt:keyValue("version.no")}" class="gifts"> -->
            <!-- <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene3/light.png?v=${mt:keyValue("version.no")}" class="light"> -->
            <!-- <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/scene3/man.png?v=${mt:keyValue("version.no")}" class="man"> -->
            <div class="content"
                data-12000="opacity: 0;"
                data-12500="opacity: 1;">
                <h3>Sleep Easy</h3>
                <p>
                    Join more than 85 Million happy users on a PCI DSS compliant platform that ensures your financial information is always well protected
                </p>
            </div>
        </div>
    </div>
    <div id="loadMask">
        <div class="f-bolt"></div>
    </div>
    <div id="wrapper" class="mobile">
        <div id="mobileCarousel">
            <div class="fold fold0">
                <div class="content">
                    <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/logo.png?v=${mt:keyValue("version.no")}">
                    <h3>#futureofmoney</h3>
                </div>
            </div>
            <div class="fold fold1">
                <!-- <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/mobile/access.png?v=${mt:keyValue("version.no")}"> -->
                <div class="content">
                    <h3>Access Anywhere</h3>
                    <p>
                        Pay with a common account on FreeCharge, Snapdeal and more
                    </p>
                </div>
            </div>
            <div class="fold fold2">
                <!-- <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/mobile/save.png?v=${mt:keyValue("version.no")}"> -->
                <div class="content">
                    <h3>Save Time</h3>
                    <p>
                        Checkout in under 10 seconds with saved cards and addresses
                    </p>
                </div>
            </div>
            <div class="fold fold3">
                <!-- <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/mobile/rewards.png?v=${mt:keyValue("version.no")}"> -->
                <div class="content">
                    <h3>Earn Rewards</h3>
                    <p>
                        Get goodies for the payments you make via FreeCharge
                    </p>
                </div>
            </div>
            <div class="fold fold4">
                <!-- <img src="${mt:keyValue("imgprefix2")}/images/whyupgrade/mobile/sleep.png?v=${mt:keyValue("version.no")}"> -->
                <div class="content">
                    <h3>Sleep Easy</h3>
                    <p>
                        Protect your personal data with an RBI approved, PCI DSS compliant platform
                    </p>
                </div>
            </div>
        </div>

    </div>
    
    <tiles:insertAttribute name="js"/>
    </body>
</html>
