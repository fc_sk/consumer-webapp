<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<%@page import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<%@ page import ="org.apache.commons.lang.WordUtils" %>
<html lang="en" style="background-color: #fafafa;">
<!--<![endif]-->
<%
    FreechargeSession freechargesession = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    String name = "";
    String email = "";
    if (freechargesession != null) {
        Map<String, Object> sessionData = freechargesession.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
        name = (String) sessionData.get(WebConstants.SESSION_USER_FIRSTNAME);
        if (name != null && !name.isEmpty())
            name = WordUtils.capitalize(name);
        email = (String) sessionData.get(WebConstants.SESSION_USER_EMAIL_ID);
    }

    String lookupid = request.getParameter("lookupID");
    String lid = request.getParameter("lid");
    if (lookupid == null && lid != null) {
        lookupid = lid;
    }
    String productType = "";
    productType = FCSessionUtil.getRechargeType(lookupid, freechargesession);
    Integer productId = FCConstants.productMasterMapReverse.get(productType);
%>
<head>
<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@page import="com.freecharge.common.framework.session.FreechargeSession"%>
<%@page import="com.freecharge.common.util.FCConstants"%>
<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@page import="com.freecharge.web.util.WebConstants"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html;charset=UTF-8" language="java"%>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="fragment" content="!">
<link href="https://www.freecharge.in" />

<tiles:insertAttribute name="css" />

<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

<title>FREE Online Recharge - Prepaid Mobile, DTH &amp; Data Card Recharge</title>

<c:set value="${riwd.rechargeResponse}" var="rechargeResponse"></c:set>



<%-- ALL THIRD PARTY SCRIPTS WHICH WILL WORK FOR PROD SHOULD GO IN HERE --%>
<c:if test="${mt:keyValue('system.ctx.mode') == 'prod'}">
	<%-- ERRORCEPTION SNIPPET STARTS --%>
	<script>
            (function(_,e,rr,s){_errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
                c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
                c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
                _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)})
                    (window,document,"script","51df8e9c6fdb191b3e000003");
        </script>


	<%-- ERRORCEPTION SNIPPET ENDS --%>

	<%-- Google A/B experiments code STARTS --%>
	<script
		src="//www.google-analytics.com/cx/api.js?experiment=8N906rX_SgePpDK54wOP5Q"></script>
	<%-- Google A/B experiments code ENDS --%>



</c:if>

<!--[if lt IE 9]>
    <script src='${mt:keyValue("jsprefix2")}/singlepage/lib/html5shim.js?v=${mt:keyValue("version.no")}'></script>
    <![endif]-->

</head>

<body style="background-color: #FAFAFA;">
    <noscript>
        <div class="no-js">
            <p>JavaScript is disabled in your browser! FreeCharge requires javascript.</p>
        </div>
    </noscript>
    <!-- This is to load the Facebook API -->
    <div id="fb-root"></div>


<%-- YOptima pixel - start --%>

<c:if test="${yoptimaEnabled}">

  <%-- Product-type events --%>

  <c:if test="${riwd.productType == 'V'}">
    <script src="https://secure.adnxs.com/seg?add=1651625&t=1" type="text/javascript"></script>
  </c:if>
  <c:if test="${riwd.productType == 'C'}">
    <script src="https://secure.adnxs.com/seg?add=1664316&t=1" type="text/javascript"></script>
  </c:if>
  <c:if test="${riwd.productType == 'D'}">
    <script src="https://secure.adnxs.com/seg?add=1664315&t=1" type="text/javascript"></script>
  </c:if>
  <c:if test="${riwd.productType == 'M'}">
    <script src="https://secure.adnxs.com/seg?add=1651627&t=1" type="text/javascript"></script>
  </c:if>

  <%-- Conversion event --%>

  <c:set value="${riwd.rechargeResponse}" var = "rechargeResponse"></c:set>
  <c:if test="${rechargeResponse == '0' || rechargeResponse == '00'}">
    <script src="https://secure.adnxs.com/px?id=182104&seg=1651628&t=1" type="text/javascript"></script>
  </c:if>

</c:if>

<%-- YOptima pixel - end --%>

<c:set  value="<%=login%>" var="login" />
<c:set  value="<%=name%>" var="name" />
<c:set  value="<%=email%>" var="email" />
<c:set  value="<%=lookupid%>" var="lookupid" />
<c:set  value="${orderId}" var="orderId" scope="request"/>
<c:if test="${empty lookupid}">
    <c:set  value="${lookupId}" var="lookupid" />
</c:if>


<div class="wrapper" id="freechargeApp">


    <header class="header custom-grid" id="user-account"></header>

    <!-- The content of the page goes here -->
    <section class="content-body custom-grid" id="main">
        <div class="page final-page">
            <div class="wrap">
                <div id="rechargeStatus" class="recharge-status-block clearfix">
                    
                </div>
                <!-- Grid for the final page : START -->
                <div class="recharge-block clearfix">
                    <div class="recharge-left-block" id="successLeft"></div>
                    <div class="recharge-right-block">
                        <c:if test="${isZedoEnabled}">
                            <div id="ad-container">
                                <script type="text/javascript" language="JavaScript">
                                    var zflag_nid="2427"; var zflag_cid="11"; var zflag_sid="2"; var zflag_width="300"; var zflag_height="250"; var zflag_sz="9";
                                </script>
                                <script src="https://saxp.zedo.com/jsc/sxp2/fo.js"> </script>
                            </div>
                        </c:if>
                    </div>
                </div>

                <c:if test="${rewardsEnabled}">
                    <div class="rewards-block" id="rewards-block"></div>
                </c:if>

            </div>
        </div>
    </section>

    <%--components--%>
    <!--modal-->
    <div id="fcModal" class=""> <!-- Add/Remove Show class appropriately -->
        <div class="modal-wrapper">
            <div class="modal-close"></div>
            <div class="content">
                <!-- Render whatever content you want here -->
            </div>
        </div>
    </div>

    <div id="overlay"></div>
</div>


    <script>
        var appConfig = {
            csrfToken: '${mt:csrfToken()}',
            imgPrefix1: '${mt:keyValue("imgprefix1")}',
            imgPrefix2: '${mt:keyValue("imgprefix2")}',
            version: '${mt:keyValue("version.no")}',
            templatePrefix: '${mt:keyValue("templateprefix")}',
            loginStatus: '${login}',
            lookupId: '${lookupid}',
            email: '${email}',
            name: '${name}',
            orderId: '${orderId}',
            ext: '${ext}',
            extType: '${extType}',
            extName: '${extName}',
            productId: '${displayProductId}',
            operatorName: '${displayTabOperatorName}',
            redirect: false,
            hasSpecialDeal: "${hasSpecialDeal}",
            rewardsEnabled: ${rewardsEnabled}
        }
    </script>


<tiles:insertAttribute name="js"/>

<!-- Wiz rocket login script starts -->
    <script type="text/javascript">
        var wizrocket = {event:[], profile:[], account:[]};
        wizrocket.account.push({"id":"KKK-K49-59KZ"});
        (function() {
            var wzrk = document.createElement('script'); wzrk.type = 'text/javascript'; wzrk.async = true;
            wzrk.src = ('https:' == document.location.protocol ? 'https://d2r1yp2w7bby2u.cloudfront.net' : 'http://static.wizrocket.com') + '/js/a.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wzrk, s);
        })();
    </script>
<!-- Wiz rocket login script ends -->
    
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/successPage.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript">
    window.appController = new AppController();
    
    $(document).ready(function() {
        <c:set var="channelId" value="<%=FCConstants.CHANNEL_ID_WEB%>"/>
        $.getScript('${mt:trackerUrl(channelId)}');
    });

    window.appController.view.set('viewName', 'rechargeSuccessPage');

    /* Google Analytics Code */
     var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-12939857-1']);
    _gaq.push(['_setDomainName', 'freecharge.in']);
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

    <c:if test="${rechargeResponse == '0' || rechargeResponse == '00'}">
        <c:if test="${googleCodeEnabled}">
            <!-- Google Code for Freecharge Display - Transaction -A/c1 Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 973722500;
                var google_conversion_language = "en";
                var google_conversion_format = "2";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "GH1PCKSQ1wkQhKen0AM";
                var google_conversion_value = 1.000000;
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript"
                    src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display: inline;">
                    <img height="1" width="1" style="border-style: none;" alt=""
                         src="//www.googleadservices.com/pagead/conversion/973722500/?value=1.000000&amp;label=GH1PCKSQ1wkQhKen0AM&amp;guid=ON&amp;script=0" />
                </div>
            </noscript>
            <!-- Google Code for Transaction- A/c 2 Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 968143684;
                var google_conversion_language = "en";
                var google_conversion_format = "2";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "DFPlCNybggoQxObSzQM";
                var google_conversion_value = 1.000000;
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript"
                    src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display: inline;">
                    <img height="1" width="1" style="border-style: none;" alt=""
                         src="//www.googleadservices.com/pagead/conversion/968143684/?value=1.000000&amp;label=DFPlCNybggoQxObSzQM&amp;guid=ON&amp;script=0" />
                </div>
            </noscript>


            <!-- Google Code for Transaction -A/c3 Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 968143084;
                var google_conversion_language = "en";
                var google_conversion_format = "2";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "HlNCCNScggoQ7OHSzQM";
                var google_conversion_value = 1.000000;
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript"
                    src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display: inline;">
                    <img height="1" width="1" style="border-style: none;" alt=""
                         src="//www.googleadservices.com/pagead/conversion/968143084/?value=1.000000&amp;label=HlNCCNScggoQ7OHSzQM&amp;guid=ON&amp;script=0" />
                </div>
            </noscript>
            <!-- Google Code for Transaction -A/c4 Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 978663220;
                var google_conversion_language = "en";
                var google_conversion_format = "2";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "9vz5CJyc0goQtO7U0gM";
                var google_conversion_value = 1.000000;
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript"
                    src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display: inline;">
                    <img height="1" width="1" style="border-style: none;" alt=""
                         src="//www.googleadservices.com/pagead/conversion/978663220/?value=1.000000&amp;label=9vz5CJyc0goQtO7U0gM&amp;guid=ON&amp;script=0" />
                </div>
            </noscript>
        </c:if>
        <c:if test="${facebookEnabled}">
            <!-- Facebook Marketing Account Conversion Code for Transactions -->
            <script>(function() {
                var _fbq = window._fbq || (window._fbq = []);
                if (!_fbq.loaded) {
                    var fbds = document.createElement('script');
                    fbds.async = true;
                    fbds.src = '//connect.facebook.net/en_US/fbds.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(fbds, s);
                    _fbq.loaded = true;
                }
            })();
            window._fbq = window._fbq || [];
            window._fbq.push(['track', '6016453207727', {'value':'0.01','currency':'INR'}]);
            </script>
            <noscript>
                <img height="1" width="1" alt="" style="display: none"
                     src="https://www.facebook.com/tr?ev=6016453207727&amp;cd[value]=0.01&amp;cd[currency]=INR&amp;noscript=1" />
            </noscript>


            <!-- Facebook Freecharge Account Conversion Code for Transactions -->
            <script>(function() {
                var _fbq = window._fbq || (window._fbq = []);
                if (!_fbq.loaded) {
                    var fbds = document.createElement('script');
                    fbds.async = true;
                    fbds.src = '//connect.facebook.net/en_US/fbds.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(fbds, s);
                    _fbq.loaded = true;
                }
            })();
            window._fbq = window._fbq || [];
            window._fbq.push(['track', '6019689328114', {'value':'0.01','currency':'INR'}]);
            </script>
            <noscript>
                <img height="1" width="1" alt="" style="display: none"
                     src="https://www.facebook.com/tr?ev=6019689328114&amp;cd[value]=0.01&amp;cd[currency]=INR&amp;noscript=1" />
            </noscript>
        </c:if>
    </c:if>


<!-- Code to load social media API's -->
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>


<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>

<!-- Code to load social media API's : END-->



<%-- ALL THIRD PARTY SCRIPTS WHICH WILL WORK FOR PROD SHOULD GO IN HERE --%>
<c:if test = "${mt:keyValue('system.ctx.mode') == 'prod'}">

    <%@include file="../layouts/googleCode.jsp" %>




    <!-- Google pixel code -->
    <!-- Google Code for Transaction Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1002754145;
        var google_conversion_language = "en";
        var google_conversion_format = "2";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "Ru_MCMf3nQMQ4aCT3gM";
        var google_conversion_value = 0;
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript"
            src="//www.googleadservices.com/pagead/conversion.js"></script>

    <noscript>
        <div style="display: none;">
            <img height="1" width="1" style="border-style: none;" alt=""
                 src="//www.googleadservices.com/pagead/conversion/1002754145/?value=0&amp;label=Ru_MCMf3nQMQ4aCT3gM&amp;guid=ON&amp;script=0" />
        </div>
    </noscript>
    <!-- End of Google code -->

    <!-- Yahoo code STARTS -->
    <script type="text/javascript"> if (!window.mstag) mstag = {loadTag : function(){},time : (new Date()).getTime()};</script>
    <script id="mstag_tops" type="text/javascript"
            src="//flex.msn.com/mstag/site/2c445f4e-9aba-412f-b4ee-f752dcde9c27/mstag.js"></script>
    <script type="text/javascript"> mstag.loadTag("analytics", {dedup:"1",domainId:"2022221",type:"1",revenue:"",actionid:"104703"})</script>
    <noscript>
        <iframe
                src="//flex.msn.com/mstag/tag/2c445f4e-9aba-412f-b4ee-f752dcde9c27/analytics.html?dedup=1&domainId=2022221&type=1&revenue=&actionid=104703"
                frameborder="0" scrolling="no" width="1" height="1"
                style="visibility: hidden; display: none"> </iframe>
    </noscript>
    <!-- Yahoo code ENDS-->

    <!-- Facebook Ads ecommerce code STARTS -->
    <script type="text/javascript">
        var fb_param = {};
        fb_param.pixel_id = '6008985739714';
        fb_param.value = '0.00';
        fb_param.currency = 'INR';
        (function(){
            var fpw = document.createElement('script');
            fpw.async = true;
            fpw.src = '//connect.facebook.net/en_US/fp.js';
            var ref = document.getElementsByTagName('script')[0];
            ref.parentNode.insertBefore(fpw, ref);
        })();
    </script>
    <noscript>
        <img height="1" width="1" alt="" style="display: none" src="https://www.facebook.com/offsite_event.php?id=6008985739714&amp;value=0&amp;currency=INR" />
    </noscript>
    <!-- Facebook Ads ecommerce code ENDS -->


    <!-- Gamooga Second Screen -->
    <%@include file="../layouts/pixels/gamoogaPixel.jsp" %>

    <!-- Add_roll visitor pixel starts -->
    <script type="text/javascript">
        adroll_adv_id = "5EOGYDTTQVBELHWLCHCHSF";
        adroll_pix_id = "5K7XUYWONFFNVDEKAGI4IU";
        (function () {
            var oldonload = window.onload;
            window.onload = function(){
                __adroll_loaded=true;
                var scr = document.createElement("script");
                var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
                scr.setAttribute('async', 'true');
                scr.type = "text/javascript";
                scr.src = host + "/j/roundtrip.js";
                ((document.getElementsByTagName('head') || [null])[0] ||
                        document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
                if(oldonload){oldonload()}};
        }());
    </script>
    <!-- Add_roll visitor pixel ends -->

    <!-- FreeCharge_Visitor_Segment_Secure script starts -->
    <script src="https://secure.adnxs.com/seg?add=1651624&t=1" type="text/javascript"></script>
    <!-- FreeCharge_Visitor_Segment_Secure script ends -->
</c:if>
<!-- re-marketing script starts -->
  <script type="text/javascript">
  /* <![CDATA[ */
  var google_conversion_id = 973722500;
  var google_custom_params = window.google_tag_params;
  var google_remarketing_only = true;
  /* ]]> */
</script>
   <script type="text/javascript"
       src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display: inline;">
      <img height="1" width="1" style="border-style: none;" alt=""
          src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/973722500/?value=0&amp;guid=ON&amp;script=0" />
     </div>  
   </noscript>
<!--  re-marketing script ends -->
</body>
</html>
