<%@ page import="com.freecharge.tracker.TrackerUtil" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:choose>
    <c:when test = "${ mt:keyValue('system.ctx.mode') == 'prod' or mt:keyValue('system.ctx.mode') == 'preProd' or mt:keyValue('system.ctx.mode') == 'qa' or mt:keyValue('system.ctx.mode') == 'e2e'}" >
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all.lib.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all-web-templates.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all-web-partials.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all.min.js?v=${mt:keyValue("version.no")}"></script>
      <%-- For Google+ login --%>
      <script type="text/javascript" src="//apis.google.com/js/client:platform.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/thirdparty/audiencemanager.js?v=${mt:keyValue("version.no")}"></script>

    </c:when>

    <c:otherwise>

      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/jquery.2.1.0.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/stapes.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/path.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/Pace.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/bootstrap.3.0.3.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/jquery-ui-1.10.3.custom.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/jquery.selectBoxIt.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/jquery.knob.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/underscore.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/jquery.imageScroll.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/parsley.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/parsley.custom.validation.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/jquery.nouislider.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/jquery.isotope.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/fingerprint.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/bootstrap-datepicker.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/placeholders.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/handlebars.runtime-v1.3.0.js"></script>
      
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all-web-partials.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all-web-templates.js?v=${mt:keyValue("version.no")}"></script>

      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/custom/AppView.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/custom/AppModel.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/custom/AppUtil.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/custom/AppRoutes.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/custom/AppController.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="//apis.google.com/js/client:platform.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/thirdparty/audiencemanager.js?v=${mt:keyValue("version.no")}"></script>
    </c:otherwise>

</c:choose>
