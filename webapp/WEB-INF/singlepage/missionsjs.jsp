<%@ page import="com.freecharge.tracker.TrackerUtil" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/lib/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/lib/bootstrap.3.0.3.min.js"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/lib/jquery.barrating.min.js"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/singlepage/lib/bootstrap-datepicker.js"></script>
