<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if gt IE 9 ]><html class="ie ie9plus" lang="en"> <![endif]-->
<!--[if !(IE)]><!-->
<%@page import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<html lang="en">
<!--<![endif]-->
<head>

<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
<%@page import="com.freecharge.common.util.FCConstants"%>
<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@ page import="com.freecharge.tracker.TrackerUtil" %>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ page import ="org.apache.commons.lang.WordUtils" %>
<%@ page import="java.util.Map" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- 
${mt:hostNamePrefix()}
-->
    <title>${seoAttributes.pageTitle}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
    <meta name="fragment" content="!">
    <meta name="keywords" content="${seoAttributes.metaKeywords}"/>
    <meta name="description" content="${seoAttributes.metaDescription}"/>
    <link href="https://www.freecharge.in"/>

    <tiles:insertAttribute name="css"/>

<%-- ALL THIRD PARTY SCRIPTS WHICH WILL WORK FOR PROD SHOULD GO IN HERE --%>
    <c:if test = "${mt:keyValue('system.ctx.mode') == 'prod'}">
        <%-- ERRORCEPTION SNIPPET STARTS --%>
        <script>
            (function(_,e,rr,s){_errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
                c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
                c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
                _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)})
                    (window,document,"script","51df8e9c6fdb191b3e000003");
        </script>
        <%-- ERRORCEPTION SNIPPET ENDS --%>

    </c:if>

    <%-- Google A/B experiments code STARTS --%>
    <%----%>
    <%--<script src="//www.google-analytics.com/cx/api.js?experiment=XtM5CFKwR-O06PegCow6eA"></script>--%>

    <script>
        /*
         * Coupon page experiment :- decrease maxCouponOptin count to 1 ( from 4 )
         */

        // Variation setup.
        // chooseVariation() method should be fired before GA code kicks in

        variation = 0; // default - no variations
        if ( window.cxApi && window.cxApi != undefined && window.cxApi != null  ) {
            variation = window.cxApi.chooseVariation();
        }

    </script>
    <%-- Google A/B experiments code ENDS --%>


    <!--[if lt IE 9]>
    <script type="text/javascript" src='${mt:keyValue("jsprefix2")}/singlepage/lib/html5shim.js?v=${mt:keyValue("version.no")}'></script>
    <script type="text/javascript" src='${mt:keyValue("jsprefix2")}/singlepage/lib/respond.min.js?v=${mt:keyValue("version.no")}'></script>
    <![endif]-->

    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
</head>

<body>
<noscript>
    <div class="no-js">
        <p>JavaScript is disabled in your browser! FreeCharge requires javascript.</p>
    </div>
</noscript>

<%
    FreechargeSession freechargesession = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    String name = "";
    String email = "";
    if (freechargesession != null) {
        Map<String, Object> sessionData = freechargesession.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
        name = (String) sessionData.get(WebConstants.SESSION_USER_FIRSTNAME);
        if (name != null && !name.isEmpty())
             name = WordUtils.capitalize(name);
            email = (String) sessionData.get(WebConstants.SESSION_USER_EMAIL_ID);
    }

    String lookupid = request.getParameter("lookupID");
    String lid = request.getParameter("lid");
    if (lookupid == null && lid != null) {
        lookupid = lid;
    }
    String productType = "";
    productType = FCSessionUtil.getRechargeType(lookupid, freechargesession);
    Integer productId = FCConstants.productMasterMapReverse.get(productType);

    String gosf = request.getParameter("gosf");
    String ext = request.getParameter("ext");
    String extType = request.getParameter("extType");
    String extName = request.getParameter("extName");
    String extHeading = request.getParameter("extHeading");
    String extDesc = request.getParameter("extDesc");
%>

<c:set  value="<%=login%>" var="login" />
<c:set  value="<%=name%>" var="name" />
<c:set  value="<%=email%>" var="email" />
<c:set  value="<%=lookupid%>" var="lookupid" />
<c:set  value="${orderId}" var="orderId" scope="request"/>
<c:set  value="<%=ext%>" var="ext" />
<c:set  value="<%=extName%>" var="extName" />
<c:set  value="<%=extType%>" var="extType" />
<c:set  value="<%=extHeading%>" var="extHeading" />
<c:set  value="<%=extDesc%>" var="extDesc" />

<c:choose >
    <c:when test="${seoAttributes.productId != null}" >
        <c:set var="displayProductId" value="${seoAttributes.productId}" />
        <c:set var="displayTabOperatorName" value="${seoAttributes.operatorName}" />
        <c:set var="displayTabOperatorId" value="${seoAttributes.operatorId}" />
    </c:when>
    <c:otherwise>
        <c:set var="displayProductId" value="${homeWebDo.tabId}" />
        <c:set var="displayTabOperatorName" value="${homeWebDo.displayOperatorName}" />
    </c:otherwise>
</c:choose>

<%-- Show build information like branch, mode etc --%>
<jsp:include page="../views/developer/build-information.jsp" />


<div class="wrapper" id="freechargeApp">
    <!-- Just a basic load masker by AMAN remove it if you find something better for Login page -->
    <div id="loading" class='load-mask-wrapper'>
        <div class='load-mask'></div>
        <img src="${mt:keyValue("imgprefix2")}/images/singlepage/load-mask.gif" width="80" height="80" align="top" class='load-mask-image'/>
    </div>
    
    <div class="la-anim-10"></div>
    
    <!-- Header block -->
    <header class="header custom-grid" id="user-account"></header>
    
    <!-- page body -->
    <section class="content-body custom-grid" id="main"></section>
    
    <!-- Footer block -->
    <jsp:include page="newFooter.jsp" />

    <%--components--%>
    <!--modal-->
    <div id="fcModal" class=""> <!-- Add/Remove Show class appropriately -->
        <div class="modal-wrapper">
            <div class="modal-close"></div>
            <div class="content">
                <!-- Render whatever content you want here -->
            </div>
        </div>
    </div>

    <div id="overlay"></div>

    <!--sidebar-->
    <div class="sidebar" id="sidebar"></div>
</div>



<script>
    var appConfig = {

        // lookupId
        lookupId: '${lookupid}',

        // user details
        email: '${email}',
        name: '${name}',
        orderId: '${orderId}',

        // Campaign details
        ext: '${ext}',
        extType: '${extType}',
        extName: '${extName}',
        extHeading: '${extHeading}',
        extDesc: '${extDesc}',

        // Univercell campaign flag
        isUnivercellHomePage: '${isUnivercellHomePage}',

        // url paths for resources
        imgPrefix1: '${mt:keyValue("imgprefix1")}',
        imgPrefix2: '${mt:keyValue("imgprefix2")}',
        version: '${mt:keyValue("version.no")}',
        templatePrefix: '${mt:keyValue("templateprefix")}',
        scriptUrl: '${mt:keyValue("jsprefix2")}',

        // 3rd party login widget flag
        facebookLoginEnabled: ${facebookLoginEnabled},
        googleLoginEnabled: ${googleLoginEnabled},
        googleClientId: '${googleClientID}',
        googleRedirectUri: '${googleRedirectURI}',

        // login status
        loginStatus: '${login}',

        // Personalization
        loginCustomMessage: '${loginCustomMessage}',
        homePageCustomMessage: '${homePageCustomMessage}',

        // A/B experiment name
        experimentName: '${experimentName}',

        // RuPay flag
        isRupayEnabled: ${isRupayEnabled},

        // coupon flags
        exclusiveCouponsEnabled: ${exclusiveCouponsEnabled},
        couponsRecoEnabled: ${recoCouponsEnabled},

        // build info
        releaseToken: '${releaseType}',

        // CSRF
        csrfToken: '${mt:csrfToken()}',

        // Testimonial version
        testimonialVersionNo: '${testimonialVersionNo}',

        // Redirect Flag
        redirect: false,

        // Fingerprint flag
        fingerPrintingEnabled: ${fingerPrintingEnabled},

        // product info
        productId: ${displayProductId},
        operatorName: '${displayTabOperatorName}',
        operatorId: '${displayTabOperatorId}',

        // Rewards enabler
        showMissionsTab: ${isMissionsTabEnabled},

        //tracker related
        trackerVisitApi: '${trackerVisitApi}',

        //showRechargePlans-is used in operator landing pages to decide whether to show plans or banner
        showRechargePlans: '${showRechargePlans}'
    };
  </script>

<!-- Wiz rocket login script starts -->
	<script type="text/javascript">
       var wizrocket = {event:[], profile:[], account:[]};
       wizrocket.account.push({"id":"KKK-K49-59KZ"});
      (function() {
        var wzrk = document.createElement('script'); wzrk.type = 'text/javascript'; wzrk.async = true;
        wzrk.src = ('https:' == document.location.protocol ? 'https://d2r1yp2w7bby2u.cloudfront.net' : 'http://static.wizrocket.com') + '/js/a.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wzrk, s);
    })();
  </script>
<!-- Wiz rocket login script ends -->

<tiles:insertAttribute name="js"/>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/custom/facebook.js?v=${mt:keyValue("version.no")}"></script>

<script>

    // event for app load
    fcTrack({'eventName' : 'w_homepage_load'});

            window.appController = new AppController();
            window.appController.view.set('affiliateId', "${affiliateIdWeb}");
            
           	//event for app load for logged in user
           	if(window.appController.view.getAll().isLoggedIn()){
           		wizrocket.event.push('ATTEMPT_IN',{
                  	'Type': 'Auto',
                  	'Channel': 'FC'
                });
           	}
            initAppRoutes();
            window.FB = new Facebook({showAlert: true});

            if('undefined' !== typeof InitAudienceManager){
              InitAudienceManager();
            }


        </script>






    <script type="text/javascript" src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
    <!-- Wrapping the Recaptcha create method in a javascript function -->
    <script>
    function showRecaptcha(element) {
        var recaptcha_public_key="6LfExOASAAAAADfMsFelOr5AGJIZ0fqHritB70bG";
            Recaptcha.create(recaptcha_public_key, element, {
                theme: "clean"
            });
        }
    function getRecaptchaChallenge() {
            return Recaptcha.get_challenge();
        }
    function getRecaptchaResponse() {
            return Recaptcha.get_response();
        }
    </script>





    <script type="text/javascript">

        <%-- This data is pushed to tracker controller in trackerjs.jsp --%>
        var TRACKER_CLIENT_DATA = (function() {
            var userAgent = navigator.userAgent;
            var referrer = document.referrer;
            var params = location.search;

            return {
                "<%=TrackerUtil.CLIENT_DETAILS_PARAM_USER_AGENT%>": userAgent,
                "<%=TrackerUtil.CLIENT_DETAILS_PARAM_REFERRER%>": referrer,
                "<%=TrackerUtil.CLIENT_DETAILS_PARAM_PARAMS%>": params
            };
        })();

        $(document).ready(function() {
            <c:set var="channelId" value="<%=FCConstants.CHANNEL_ID_WEB%>"/>
            $.getScript('${mt:trackerUrl(channelId)}');
        });
    </script>

    <!--<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/custom/AppRoutes.js?v=${mt:keyValue("version.no")}"></script> -->
    
    <!-- Google analytics snippet START -->
    <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-12939857-1']);
    _gaq.push(['_setDomainName', 'freecharge.in']);
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
    </script>
    <script>
            var paceOptions = {
              ajax: true, // disabled
              document: true, // disabled
              eventLag: true, // disabled
              elements: {
                selectors: ['.recharge', '.coupon']
              }
    };
    </script>
    <!-- Google analytics snippet END -->

   <!-- Add_roll visitor pixel starts -->
    <script type="text/javascript">
      adroll_adv_id = "5EOGYDTTQVBELHWLCHCHSF";
      adroll_pix_id = "5K7XUYWONFFNVDEKAGI4IU";
     (function () {
      var oldonload = window.onload;
      window.onload = function(){
      __adroll_loaded=true;
      var scr = document.createElement("script");
      var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
      scr.setAttribute('async', 'true');
      scr.type = "text/javascript";
      scr.src = host + "/j/roundtrip.js";
      ((document.getElementsByTagName('head') || [null])[0] ||
       document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
       if(oldonload){oldonload()}};
    }());
   </script>
    <!-- Add_roll visitor pixel ends -->
    
    <!-- FreeCharge_Visitor_Segment_Secure script starts -->
    <script src="https://secure.adnxs.com/seg?add=1651624&t=1" type="text/javascript"></script>
    <!-- FreeCharge_Visitor_Segment_Secure script ends -->
    

	<!-- re-marketing script starts -->
	<script type="text/javascript">
  /* <![CDATA[ */
  var google_conversion_id = 973722500;
  var google_custom_params = window.google_tag_params;
  var google_remarketing_only = true;
  /* ]]> */
</script>
    <script type="text/javascript"
		src="//www.googleadservices.com/pagead/conversion.js">
    </script>
	<noscript>
		<div style="display: inline;">
         <img height="1" width="1" style="border-style: none;" alt=""
              src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/973722500/?value=0&amp;guid=ON&amp;script=0" />
        </div>
    </noscript>
    <!--  re-marketing script ends -->

    <c:if test = "${mt:keyValue('system.ctx.mode') == 'prod'}">
        <!-- Start Visual Website Optimizer Asynchronous Code -->
        <%--
        <script type='text/javascript'>
            var _vwo_code=(function(){
            var account_id=53504,
            settings_tolerance=2000,
            library_tolerance=2500,
            use_existing_jquery=false,
            // DO NOT EDIT BELOW THIS LINE
            f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
        </script>
        --%>
        <!-- End Visual Website Optimizer Asynchronous Code -->
    </c:if>
</body>
</html>
