<%-- used for 404 page --%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<header class="header custom-grid" id="user-account">
    <div class="main-head">
        <div class="container">
            <a href="/" id="freecharge" class="logo freeCharge">
                <img src="${mt:keyValue("imgprefix1")}/images/singlepage/freecharge_white.png" alt="freecharge.in">
            </a>
        </div>
    </div>
</header>

