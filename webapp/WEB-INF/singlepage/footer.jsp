<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
	<c:when test="${ not empty seoAttributes.footerTextParaOne}">
		<div class="seo-text custom-grid">
			<div class="container">
				<p>${seoAttributes.footerTextParaOne}</p>
				<p>${seoAttributes.footerTextParaTwo}</p>
			</div>
		</div>
	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>
<footer class="footer custom-grid">
    <div class="container">
        <div class="footer-block">
            <div class="footer-one">
                <ul class="footer-links">
                    <li><a href="/app/aboutus.htm" target="_blank">About Us</a></li>
                    <li><a href="/app/termsandconditions.htm" target="_blank">T &amp; C</a></li>
                    <!-- <li><a href="/app/faq.htm" target="_blank">FAQ</a></li> -->
                    <li><a href="http://support.freecharge.in" target="_blank">Support</a></li>
                    <li><a href="/app/contactus.htm" target="_blank">Contact Us</a></li>
                    <li><a href="http://geekery.freecharge.in" target="_blank">Blog</a></li>
                    <li><a href="/app/privacypolicy.htm" target="_blank">Privacy Policy</a></li>
                    <li><a href="/app/sitemap.htm" target="_blank">Sitemap</a></li>
                </ul>
                <p class="copyright">&copy; Accelyst Solutions Pvt. Ltd. All Rights Reserved.</p>
            </div>
            <div class="footer-two">
                <ul>
                    <li><a href="/airtel-prepaid-mobile-recharge">Airtel</a></li>
                    <li><a href="/aircel-prepaid-mobile-recharge">Aircel</a></li>
                    <li><a href="/vodafone-prepaid-mobile-recharge">Vodafone</a></li>
                    <li><a href="/bsnl-prepaid-mobile-recharge">BSNL</a></li>
                    <li><a href="/tata-docomo-cdma-prepaid-mobile-recharge">Tata Docomo CDMA</a></li>
                    <li><a href="/tata-docomo-gsm-prepaid-mobile-recharge">Tata Docomo GSM</a></li>
                    <li><a href="/idea-prepaid-mobile-recharge">Idea</a></li>
                    <li><a href="/tata-indicom-walky-prepaid-mobile-recharge">Tata Walky</a></li>
                    <li><a href="/mtnl-trump-prepaid-mobile-recharge">MTNL Trump</a></li>
                    <li><a href="/reliance-cdma-prepaid-mobile-recharge">Reliance CDMA</a></li>
                    <li><a href="/reliance-gsm-prepaid-mobile-recharge">Reliance GSM</a></li>
                    <li><a href="/tata-indicom-prepaid-mobile-recharge">Tata Indicom</a></li>
                    <li><a href="/uninor-prepaid-mobile-recharge">Uninor</a></li>
                    <li><a href="/mts-prepaid-mobile-recharge">MTS</a></li>
                    <li><a href="/videocon-prepaid-mobile-recharge">Videocon</a></li>
                    <li><a href="/virgin-cdma-prepaid-mobile-recharge">Virgin CDMA</a></li>
                    <li><a href="/virgin-gsm-prepaid-mobile-recharge">Virgin GSM</a></li>
                    <li><a href="/online-airtel-dth-recharge">Airtel Digital</a></li>
                    <li><a href="/online-big-tv-recharge">BIG TV</a></li>
                    <li><a href="/online-dish-tv-recharge">Dish TV</a></li>
                    <li><a href="/online-tata-sky-recharge">Tata Sky</a></li>
                    <li><a href="/online-sun-direct-recharge">Sun Direct</a></li>
                    <li><a href="/videocon-d2h-online-recharge">Videocon D2H</a></li>
                    <li><a href="/tata-photon-online-recharge">Tata Photon</a></li>
                    <li><a href="/mts-mblaze-recharge">MTS MBlaze</a></li>
                    <li><a href="/mts-mbrowse-datacard-recharge">MTS MBrowse</a></li>
                    <li><a href="/reliance-netconnect-datacard-recharge">Reliance NetConnect</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>