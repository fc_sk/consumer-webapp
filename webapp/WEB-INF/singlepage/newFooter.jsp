<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<footer class="footer custom-grid">
    <div class="container">
        <div class="footer-block">
            <div class="footer-two">
              <div class="footercolumn">
                <h5 class="newfooterheading"><a href="/online-mobile-recharge">MOBILE RECHARGE</a></h5>
                  <ul>
                      <li><a href="/airtel-prepaid-mobile-recharge">Airtel&nbsp;&nbsp;</a>(<a href="/airtel-prepaid-plans">Plans</a>)</li><br>
                      <li><a href="/aircel-prepaid-mobile-recharge">Aircel&nbsp;&nbsp;</a>(<a href="/aircel-prepaid-plans">Plans</a>)</li><br>
                      <li><a href="/vodafone-prepaid-mobile-recharge">Vodafone&nbsp;&nbsp;</a>(<a href="/vodafone-prepaid-mobile-plans">Plans</a>)</li><br>
                      <li><a href="/bsnl-prepaid-mobile-recharge">BSNL&nbsp;&nbsp;</a>(<a href="/bsnl-prepaid-mobile-plans">Plans</a>)</li><br>
                      <li><a href="/tata-docomo-gsm-prepaid-mobile-recharge">Tata Docomo GSM&nbsp;&nbsp;</a>(<a href="/tata-docomo-gsm-Prepaid-Plans">Plans</a>)</li><br>
                      <li><a href="/idea-prepaid-mobile-recharge">Idea&nbsp;&nbsp;</a>(<a href="/idea-Prepaid-Mobile-Plans">Plans</a>)</li><br>
                      <li><a href="/tata-indicom-walky-prepaid-mobile-recharge">Indicom Walky&nbsp;&nbsp;</a>(<a href="/indicom-walky-Prepaid-Plans">Plans</a>)</li><br>
                      <li><a href="/mtnl-trump-prepaid-mobile-recharge">MTNL Delhi&nbsp;&nbsp;</a>(<a href="/mtnl-delhi-Prepaid-Plans">Plans</a>)</li><br>
                      <li><a href="/reliance-cdma-prepaid-mobile-recharge">Reliance CDMA&nbsp;&nbsp;</a>(<a href="/reliance-cdma-plans">Plans</a>)</li><br>
                      <li><a href="/reliance-gsm-prepaid-mobile-recharge">Reliance GSM&nbsp;&nbsp;</a>(<a href="/reliance-Prepaid-Mobile-Plans">Plans</a>)</li><br>
                      <li><a href="/tata-indicom-prepaid-mobile-recharge">Tata Indicom&nbsp;&nbsp;</a>(<a href="/tata-indicom-Prepaid-Plans">Plans</a>)</li><br>
                      <li><a href="/uninor-prepaid-mobile-recharge">Uninor&nbsp;&nbsp;</a>(<a href="/uninor-Prepaid-Mobile-Plans">Plans</a>)</li><br>
                      <li><a href="/mts-prepaid-mobile-recharge">MTS&nbsp;&nbsp;</a>(<a href="/mts-Prepaid-Mobile-Plans">Plans</a>)</li><br>
                      <li><a href="/videocon-prepaid-mobile-recharge">Videocon&nbsp;&nbsp;</a>(<a href="/videocon-Prepaid-Mobile-Plans">Plans</a>)</li><br>
                      <li><a href="/virgin-cdma-prepaid-mobile-recharge">Virgin CDMA&nbsp;&nbsp;</a>(<a href="/virgin-cdma-Prepaid-Mobile-Plans">Plans</a>)</li><br>
                      <li><a href="/virgin-gsm-prepaid-mobile-recharge">Virgin GSM&nbsp;&nbsp;</a>(<a href="/virgin-gsm-Prepaid-Mobile-Plans">Plans</a>)</li><br>
                      <li><a href="/tata-docomo-cdma-prepaid-mobile-recharge">Tata Docomo CDMA&nbsp;&nbsp;</a>(<a href="/tata-docomo-cdma-Prepaid-Plans">Plans</a>)</li><br>
                </ul>
              </div>
             <div class="footercolumn">
                <div id="datacarddiv">
                  <h5 class="newfooterheading"><a href="/online-datacard-recharge">DATA CARD RECHARGE</a></h5>
                    <ul>
                      <li><a href="/tata-photon-plus-datacard-recharge">Tata Photon Plus</a></li><br>
                      <li><a href="/tata-photon-whiz-datacard-recharge">Tata Photon Whiz</a></li><br>
                      <li><a href="/mts-mblaze-recharge">MTS MBlaze</a></li><br>
                      <li><a href="/mts-mbrowse-datacard-recharge">MTS MBrowse</a></li><br>
                      <li><a href="/reliance-netconnect-datacard-recharge">Reliance NetConnect</a></li><br>
                      <li><a href="/airtel-datacard-recharge">Airtel</a></li><br>
                      <li><a href="/bsnl-datacard-recharge">BSNL</a></li><br>
                      <li><a href="/aircel-datacard-recharge">Aircel</a></li><br>
                      <li><a href="/mtnl-delhi-datacard-online-recharge">MTNL Delhi</a></li><br>
                      <li><a href="/vodafone-datacard-online-recharge">Vodafone</a></li><br>
                      <li><a href="/idea-datacard-recharge">Idea</a></li><br>
                      <li><a href="/mtnl-mumbai-datacard-recharge">MTNL Mumbai</a></li><br>
                   </ul>
                </div>
                <div id="dthdiv" class="headinggap">
                   <h5 class="newfooterheading"><a href="/online-dth-recharge">DTH(TV) RECHARGE</a></h5>
                     <ul>
                       <li><a href="/online-airtel-dth-recharge">Airtel Digital TV</a></li><br>
                       <li><a href="/online-big-tv-recharge">Reliance Digital TV </a></li><br>
                       <li><a href="/online-dish-tv-recharge">Dish TV </a></li><br>
                       <li><a href="/online-tata-sky-recharge">Tata Sky </a></li><br>
                       <li><a href="/online-sun-direct-recharge">Sun Direct </a></li><br>
                       <li><a href="/videocon-d2h-online-recharge">Videocon D2H </a></li><br>
                   </ul>
                </div>
             </div>
             <div class="footercolumn">
                <div id="postpaiddiv" class="clearfix" >
                 <h5 class="newfooterheading">POSTPAID</h5>
                   <ul>
                    <li><a href="/airtel-mobile-postpaid-bill-payment"> Airtel Bill Payment</a></li><br>
                    <li><a href="/bsnl-mobile-postpaid-bill-payment">BSNL Bill Payment</a></li><br>
                    <li><a href="/tata-docomo-gsm-mobile-postpaid-bill-payment"> Tata Docomo GSM Bill Payment</a></li><br>
                    <li><a href="/tata-docomo-cdma-mobile-postpaid-bill-payment"> Tata Docomo CDMA Bill Payment</a></li><br>
                    <li><a href="/idea-mobile-postpaid-bill-payment"> Idea Bill Payment</a></li><br>
                    <li><a href="/vodafone-mobile-postpaid-bill-payment"> Vodafone Bill Payment</a></li><br>
                    <li><a href="/reliance-gsm-mobile-postpaid-bill-payment"> Reliance GSM Bill Payment</a></li><br>
                    <li><a href="/reliance-cdma-mobile-postpaid-bill-payment"> Reliance CDMA Bill Payment</a></li><br>
                  </ul>
                 </div>
                <div id="freechargediv" class="clearfix headinggap" >
                  <h5 class="newfooterheading">FREECHARGE</h5>
                    <ul class="listcoloumn">
                       <li><a rel="noreferrer" href="/app/aboutus.htm" target="_blank">About Us</a></li><br>
                        <li><a rel="noreferrer" href="https://freecharge.ripplehire.com/ripplehire/careers" target="_blank">Careers</a></li><br>
                       <!-- <li><a rel="noreferrer" href="/app/faq.htm" target="_blank">FAQ</a></li><br> -->
                       <li><a rel="noreferrer" href="http://support.freecharge.in" target="_blank">Support</a></li><br>
                       <li><a rel="noreferrer" href="/app/contactus.htm" target="_blank">Contact Us</a></li><br>
                       <!--<li><a rel="noreferrer" href="/app/privacypolicy.htm" target="_blank">Privacy Policy</a></li>--><br>
                    </ul>
                    <ul class="listcoloumn">
                       <li><a rel="noreferrer" href="/app/sitemap.htm" target="_blank">Sitemap</a></li><br>
                       <li><a rel="noreferrer" href="http://geekery.freecharge.in" target="_blank">Geekery</a></li><br>
                       <li><a rel="noreferrer" href="/app/termsandconditions.htm" target="_blank">T &amp; C</a></li><br>
                       <li><a rel="noreferrer" href="http://blog.freecharge.in/" target="_blank">Blog</a></li><br>
                       <!-- <li><a rel="noreferrer" href="/app/wallet" target="_blank">Balance</a></li><br> -->
                       <!--<li><a rel="noreferrer" href="/security/disclosure.htm" target="_blank">Security Policy</a></li>--><br>
                    </ul>
                </div>
                <div id="mobilediv" class="headinggap">
                  <h5 class="newfooterheading">MOBILE</h5>
                    <ul class="listcoloumn">
                      <li><a href="https://play.google.com/store/apps/details?id=com.freecharge.android">Android App</a></li><br>
                      <li><a href="http://www.windowsphone.com/en-in/store/app/freecharge-recharge-mobile-dth-data-card/353ad694-42a4-4b60-bfe6-c250a6526f17">Windows</a>
                    </ul>
                    <ul class="listcolumn">
                       <li><a href="https://www.freecharge.in/m">Mobile Site</a></li><br>
                       <li><a href="https://itunes.apple.com/us/app/recharge-mobile-dth-online/id877495926">iOS</a></li>
                    </ul>
                </div>
             </div>
             <div class="footercolumn">
                <h5 class="newfooterheading">SECURED</h5>
                <img src='${mt:keyValue("imgprefix1")}/images/footer/visa-mastercard.png' alt="Visa-mastercard verification"/><br>
                 <br>
                  <div class="secure-scan-seal">
                    <a href="https://sisainfosec.com/site/certificate/56584876565666219279" target="_blank">
                     <img src='${mt:keyValue("imgprefix1")}/images/pci-logo.png' oncontextmenu='return false;' border='0'></a>
                  </div>
                  <br>
                  <div class="security-information">
                   <div class="geotrust">
                      <script language="javascript" type="text/javascript" src="//smarticon.geotrust.com/si.js"></script>
                   </div>
                  </div>
                </div>
            </div>
            <div class="footercolumn">
              <h5 class="footerheadingextendedcolumn">SUPPORT</h5>
                For help, send email to <a href="mailto:care@freecharge.com" style="color:#3a7dc8;">care@freecharge.com</a>
              <div id="joinusdiv" style="margin-top:50px;">
               <h5 class="footerheadingextendedcolumn">JOIN US ON</h5>
               <div class="clearfix">
                  <a href="https://www.facebook.com/freecharge"><div class="fbimage"></div></a>
                  <a href="https://twitter.com/freecharge"><div class="twitterimage"></div></a>
                  <a href="https://plus.google.com/+freecharge" rel="publisher"><div class="googleplusimage"></div></a>
                </div>
            </div>
            <div style="margin-top:50px;">
              <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Ffreecharge&amp;width&amp;height=260&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:290px;" allowTransparency="true"></iframe>
           </div>
           </div>
          </div>
        </div>
        <div class="footer-one">
            <p class="copyright" id="so">&copy; Accelyst Solutions Pvt. Ltd. All Rights Reserved.</p>
        </div>
   </footer>
