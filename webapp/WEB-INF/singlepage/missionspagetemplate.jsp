<%@page import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
<%@page import="com.freecharge.common.util.FCConstants"%>
<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ page import ="org.apache.commons.lang.WordUtils" %>
<%@ page import="java.util.Map" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <title>Awesome Missions with Awesome Rewards!</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="fragment" content="!">
    <link href="https://www.freecharge.in"/>

    <tiles:insertAttribute name="css"/>

    <%-- ALL THIRD PARTY SCRIPTS WHICH WILL WORK FOR PROD SHOULD GO IN HERE --%>
    <c:if test = "${mt:keyValue('system.ctx.mode') == 'prod'}">
        <%-- ERRORCEPTION SNIPPET STARTS --%>
        <script>
            (function(_,e,rr,s){_errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
                c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
                c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
                _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)})
                    (window,document,"script","51df8e9c6fdb191b3e000003");
        </script>
        <%-- ERRORCEPTION SNIPPET ENDS --%>

        <!-- Add Scripts Only if you need it and are tracking them -->
        <!-- Pingdom RUM START-->
        <!--<script>
        var _prum = [['id', '52657701abe53dc439000000'],
        ['mark', 'firstbyte', (new Date()).getTime()]];
        (function() {
        var s = document.getElementsByTagName('script')[0]
        , p = document.createElement('script');
        p.async = 'async';
        p.src = '//rum-static.pingdom.net/prum.min.js';
        s.parentNode.insertBefore(p, s);
        })();
        </script> -->
        <!-- End Pingdom RUM -->

    </c:if>


    <!--[if lt IE 9]>
    <script type="text/javascript" src='${mt:keyValue("jsprefix2")}/singlepage/lib/html5shim.js?v=${mt:keyValue("version.no")}'></script>
    <script type="text/javascript" src='${mt:keyValue("jsprefix2")}/singlepage/lib/respond.min.js?v=${mt:keyValue("version.no")}'></script>
    <![endif]-->

    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
</head>

<body>
<noscript>
    <div class="no-js">
        <p>JavaScript is disabled in your browser! FreeCharge requires javascript.</p>
    </div>
</noscript>

<%
    FreechargeSession freechargesession = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    String name = "";
    String email = "";
    if (freechargesession != null) {
        Map<String, Object> sessionData = freechargesession.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
        name = (String) sessionData.get(WebConstants.SESSION_USER_FIRSTNAME);
        if (name != null && !name.isEmpty())
            name = WordUtils.capitalize(name);
        email = (String) sessionData.get(WebConstants.SESSION_USER_EMAIL_ID);
    }

    String lookupid = request.getParameter("lookupID");
    String lid = request.getParameter("lid");
    if (lookupid == null && lid != null) {
        lookupid = lid;
    }
    String productType = "";
    productType = FCSessionUtil.getRechargeType(lookupid, freechargesession);
    Integer productId = FCConstants.productMasterMapReverse.get(productType);

    String gosf = request.getParameter("gosf");
    String ext = request.getParameter("ext");
    String extType = request.getParameter("extType");
    String extName = request.getParameter("extName");
    String extHeading = request.getParameter("extHeading");
    String extDesc = request.getParameter("extDesc");
%>

<c:set  value="<%=login%>" var="login" />
<c:set  value="<%=name%>" var="name" />
<c:set  value="<%=email%>" var="email" />
<c:set  value="<%=lookupid%>" var="lookupid" />
<c:set  value="${orderId}" var="orderId" scope="request"/>
<c:set  value="<%=ext%>" var="ext" />
<c:set  value="<%=extName%>" var="extName" />
<c:set  value="<%=extType%>" var="extType" />
<c:set  value="<%=extHeading%>" var="extHeading" />
<c:set  value="<%=extDesc%>" var="extDesc" />

<c:choose >
    <c:when test="${seoAttributes.productId != null}" >
        <c:set var="displayProductId" value="${seoAttributes.productId}" />
        <c:set var="displayTabOperatorName" value="${seoAttributes.operatorName}" />
    </c:when>
    <c:otherwise>
        <c:set var="displayProductId" value="${homeWebDo.tabId}" />
        <c:set var="displayTabOperatorName" value="${homeWebDo.displayOperatorName}" />
    </c:otherwise>
</c:choose>

<%-- Show build information like branch, mode etc --%>
<jsp:include page="../views/developer/build-information.jsp" />

<%--<button class="btn" id="test" style="position: absolute; z-index: 999; top: 0px; opacity:0.1">Auto fill</button>--%>

<div class="wrapper" id="freechargeApp"
     data-release-token='${releaseType}'
     data-csrf-token='${mt:csrfToken()}'
     data-img-prefix1='${mt:keyValue("imgprefix1")}'
     data-img-prefix2='${mt:keyValue("imgprefix2")}'
     data-version = '${mt:keyValue("version.no")}'
     data-template-prefix = '${mt:keyValue("templateprefix")}'
     data-login-status='${login}'
     data-name='${name}'
     data-email='${email}'
     data-lookup-id='${lookupid}'
     data-order-id='${orderId}'
     data-ext='${ext}'
     data-ext-type='${extType}'
     data-ext-name='${extName}'
     data-ext-heading='${extHeading}'
     data-ext-desc='${extDesc}'
     data-product-id='${displayProductId}'
     data-operator-name="${displayTabOperatorName}"
     data-redirect="false"
     data-is-rupay-enabled='${isRupayEnabled}'>

    <%--notification bar--%>
    <div class="notification-bar-primary">
        <p>Change is good! You are viewing the new version of Freecharge. Please share your experience &amp; feedback with us on <a href="mailto:care@freecharge.com">care@freecharge.com</a></p>
        <a onclick="$('.notification-bar-primary').slideUp(200); $(this).hide()" class="notification-close"></a>
    </div>

    <!-- Aman removed the JSP, this should save us one call to fetch the JSP  -->
    <header class="navbar navbar-default header custom-grid" id="user-account">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/" id="freecharge" class="freeCharge">
                    <img src="${mt:keyValue("imgprefix2")}/images/singlepage/freecharge_white.png" alt="freecharge.in" />
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <nav class="collapse navbar-collapse fc-fadeIn" id="bs-example-navbar-collapse-1" role="navigation">
                <ul class="nav navbar-nav navbar-right user-account">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="navbarAccountBtn">${name} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href='/#!/credits' id="creditsPage">
                                    <i class="icon-credits"></i>
                                    <span class='user-account-text'>Credits</span>
                                </a>
                            </li>
                            <li>
                                <a href='/#!/profile' id="profilePage">
                                    <i class='icon-profile'></i>
                                    <span class='user-account-text'>Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href='/#!/transactions' id="transactionPage">
                                    <i class="icon-transaction"></i>
                                    <span class='user-account-text'>Transaction History</span>
                                </a>
                            </li>
                            <li>
                                <a href='/#!/myCoupons' id="myCouponsPage">
                                    <i class="icon-mycoupons"></i>
                                    <span class='user-account-text'>My Coupons</span>
                                </a>
                            </li>
                            <li>
                                <a href='/#!/logout'>
                                    <i class='icon-logout'></i>
                                    <span class='user-account-text'>Logout</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav><!-- /.navbar-collapse -->
        </div>
    </header>

    <!-- The content of the page goes here -->
    <section class="content-body custom-grid" id="main">
        <tiles:insertAttribute name="body"/>
    </section>

    <!-- The footer section -->
    <jsp:include page="footer.jsp" />

</div>

<tiles:insertAttribute name="js"/>

<!-- Custom Scripts should go into this section -->
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/custom/ardeal.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">

    var data = {
        'eventName': 'missionspage'
    };

    var csrfToken = $('#freechargeApp').data('csrfToken');
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('csrfRequestIdentifier', csrfToken);
        }
    });

    $(document).ready(function(ev){
        $('#missions-list').ardeals({
            hookPoint: 'dp'
        });

        <c:set var="channelId" value="<%=FCConstants.CHANNEL_ID_WEB%>"/>
        $.getScript('${mt:trackerUrl(channelId)}');
    });

    var _fcTrackerEventQueue = _fcTrackerEventQueue || [];
    <%--
    //This function gets redefined by the call to TrackingController default method. Take a look at trackerJs.jsp to see the redefinition.
    //The logic here is that, until we get a response from TrackingController default method, we queue events up. As soon as the response is loaded,
    //we redefine fcTrack to send the events directly to the server. We do this dance because we want the tracker requests to the server to
    //have the cookies sent in the TrackingController call but we do not want to block the page load until we get a response from TrackingController.
    --%>
    function fcTrack(params) {
        _fcTrackerEventQueue.push(params);
    }
    fcTrack(data);
</script>

<!-- Google analytics snippet START -->
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-12939857-1']);
    _gaq.push(['_setDomainName', 'freecharge.in']);
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<!-- Google analytics snippet END -->

</body>
</html>
