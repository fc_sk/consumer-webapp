<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if gt IE 9 ]><html class="ie ie9plus" lang="en"> <![endif]-->
<!--[if !(IE)]><!-->
<%@page import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<html lang="en">
<!--<![endif]-->
<head>

<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
<%@page import="com.freecharge.common.util.FCConstants"%>
<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ page import="java.util.Map" %>
<%@ page import ="org.apache.commons.lang.WordUtils" %>
<%@ page import="com.freecharge.tracker.TrackerUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- 
${mt:hostNamePrefix()}
-->
    <title>${seoAttributes.pageTitle}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
    <meta name="fragment" content="!">
    <meta name="keywords" content="${seoAttributes.metaKeywords}"/>
    <meta name="description" content="${seoAttributes.metaDescription}"/>
    <link href="https://www.freecharge.in"/>
    <link rel="shortcut icon" href="${mt:keyValue("imgprefix2")}/images/favicon-new.ico?v=${mt:keyValue("version.no")}"/>
    <link rel="alternate" media="handheld" href="http://www.freecharge.in/mobile"/>
    <link rel="alternate" href="android-app://com.freecharge.android/freechargeapp/freecharge" />
    <tiles:insertAttribute name="css"/>
    
<!-- Schema markup for social links to show up in Google Knowledge Graph (v1.0) -->
<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "FreeCharge: recharge nahi freecharge",
  "url" : "https://www.freecharge.in",
  "logo" : "https://www.dropbox.com/s/mqxtm37m74l8nbz/Logo-150-x-150.png?dl=0",
  "sameAs" : [ "http://www.facebook.com/freecharge",
    "http://www.twitter.com/freecharge",
    "http://plus.google.com/+freecharge",
    "https://www.facebook.com/freecharge"] 
}
</script>

    <!-- Conditional linking and unlinking of missions CSS -->



    <%-- Google A/B experiments code STARTS --%>
    <%----%>
    <%--<script src="//www.google-analytics.com/cx/api.js?experiment=XtM5CFKwR-O06PegCow6eA"></script>--%>

    <script>
        /*
         * Coupon page experiment :- decrease maxCouponOptin count to 1 ( from 4 )
         */

        // Variation setup.
        // chooseVariation() method should be fired before GA code kicks in

        variation = 0; // default - no variations
        if ( window.cxApi && window.cxApi != undefined && window.cxApi != null  ) {
            variation = window.cxApi.chooseVariation();
        }

    </script>
    <%-- Google A/B experiments code ENDS --%>


    <!--[if lt IE 9]>
    <script type="text/javascript" src='${mt:keyValue("jsprefix2")}/singlepage/lib/html5shim.js?v=${mt:keyValue("version.no")}'></script>
    <script type="text/javascript" src='${mt:keyValue("jsprefix2")}/singlepage/lib/respond.min.js?v=${mt:keyValue("version.no")}'></script>
    <![endif]-->

    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
</head>

<body class="">
	<script>
      window.fbAsyncInit = function() {
        FB.init({
        	appId      : '267804853344286',
            cookie     : true,  // enable cookies to allow the server to access
            xfbml      : true,  // parse social plugins on this page
            version    : 'v2.2' // use version 2.0
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
<noscript>
    <div class="no-js">
        <p>JavaScript is disabled in your browser! FreeCharge requires javascript.</p>
    </div>
</noscript>

<%
    FreechargeSession freechargesession = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    String name = "";
    String email = "";
    if (freechargesession != null) {
        Map<String, Object> sessionData = freechargesession.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
        name = (String) sessionData.get(WebConstants.SESSION_USER_FIRSTNAME);
        if (name != null && !name.isEmpty())
             name = WordUtils.capitalize(name);
            email = (String) sessionData.get(WebConstants.SESSION_USER_EMAIL_ID);
    }

    String lookupid = request.getParameter("lookupID");
    String lid = request.getParameter("lid");
    if (lookupid == null && lid != null) {
        lookupid = lid;
    }
    String productType = "";
    productType = FCSessionUtil.getRechargeType(lookupid, freechargesession);
    Integer productId = FCConstants.productMasterMapReverse.get(productType);

    String gosf = request.getParameter("gosf");
    String ext = request.getParameter("ext");
    String extType = request.getParameter("extType");
    String extName = request.getParameter("extName");
    String extHeading = request.getParameter("extHeading");
    String extDesc = request.getParameter("extDesc");
%>

<c:set  value="<%=login%>" var="login" />
<c:set  value="<%=name%>" var="name" />
<c:set  value="<%=email%>" var="email" />
<c:set  value="<%=lookupid%>" var="lookupid" />
<c:set  value="${orderId}" var="orderId" scope="request"/>
<c:set  value="<%=ext%>" var="ext" />
<c:set  value="<%=extName%>" var="extName" />
<c:set  value="<%=extType%>" var="extType" />
<c:set  value="<%=extHeading%>" var="extHeading" />
<c:set  value="<%=extDesc%>" var="extDesc" />

<c:choose >
    <c:when test="${seoAttributes.productId != null}" >
        <c:set var="displayProductId" value="${seoAttributes.productId}" />
        <c:set var="displayTabOperatorName" value="${seoAttributes.operatorName}" />
        <c:set var="displayTabOperatorId" value="${seoAttributes.operatorId}" />
    </c:when>
    <c:otherwise>
        <c:set var="displayProductId" value="${homeWebDo.tabId}" />
        <c:set var="displayTabOperatorName" value="${homeWebDo.displayOperatorName}" />
    </c:otherwise>
</c:choose>

<%-- Show build information like branch, mode etc --%>
<jsp:include page="../views/developer/build-information.jsp" />

<div class="wrapper" id="freechargeApp">
    <!-- Just a basic load masker by AMAN remove it if you find something better for Login page -->

<!--     <div id="loading" class='load-mask-wrapper'>
        <div class='load-mask'></div>
        <img src="${mt:keyValue("imgprefix2")}/images/singlepage/load-mask.gif" width="80" height="80" align="top" class='load-mask-image'/>
    </div> -->
    
    <div class="la-anim-10"></div>
    
    <!-- Header block -->
    <!-- <header class="header custom-grid" id="user-account"></header> -->

    
    <!-- NEW HEADER -->

    <header class="" id="mainHeader">
        
    </header>
    <!-- The content of the page goes here -->
    <section class="content-body custom-grid" id="main"></section>

    <!-- Overlays and other html that is independent of main content -->

    <div id="overlay"></div>

    <div id="fcModal" class="hide">
        <div class="maxHeight"></div>
        <div class="modal-wrapper">
            <div class="modal-close"></div>
            <div class="fc-modal-content" id="modal-content">
                <!-- Render whatever content you want here -->
            </div>
        </div>
    </div>


    <div id="scrollTopBtn"></div>

    <!-- The footer section -->
    <!-- page body -->
    <!-- <section class="content-body custom-grid" id="main"></section> -->
    
    <!-- Footer block -->
    <jsp:include page="newFooter.jsp" />

    <%--components--%>
    <!--modal
    <div id="fcModal" class=""> 
        <div class="modal-wrapper">
            <div class="modal-close"></div>
            <div class="content">
               
            </div>
        </div>
    </div>-->

    <!-- <div id="overlay"></div> -->

    <!--sidebar-->
    <div class="sidebar" id="sidebar"></div>

    <div id="loadMask">
        <div class="f-bolt"></div>
    </div>
</div>

<script type="text/javascript">
    var tagsToReplace = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;'
    };
    function replaceTag(tag) {
        return tagsToReplace[tag] || tag;
    }
    function safe_tags_replace(str) {
        return str.replace(/[&<>]/g, replaceTag);
    }
</script>

<script>
    var displayProductId = "${displayProductId}";
    var appConfig = {

        // lookupId
        lookupId: '${lookupid}',

        // user details
        email: '${email}',
        name: '${name}',
        orderId: '${orderId}',

        // Campaign details
        ext: safe_tags_replace('${ext}'),
        extType: safe_tags_replace('${extType}'),
        extName: safe_tags_replace('${extName}'),
        extHeading: safe_tags_replace('${extHeading}'),
        extDesc: safe_tags_replace('${extDesc}'),

        // Univercell campaign flag
        isUnivercellHomePage: '${isUnivercellHomePage}',

        // url paths for resources
        imgPrefix1: '${mt:keyValue("imgprefix1")}',
        imgPrefix2: '${mt:keyValue("imgprefix2")}',
        version: '${mt:keyValue("version.no")}',
        templatePrefix: '${mt:keyValue("templateprefix")}',
        scriptUrl: '${mt:keyValue("jsprefix2")}',

        // 3rd party login widget flag
        facebookLoginEnabled: ${facebookLoginEnabled},
        googleLoginEnabled: ${googleLoginEnabled},
        googleClientId: '${googleClientID}',
        googleRedirectUri: '${googleRedirectURI}',

        // login status
        loginStatus: '${login}',

        // Personalization
        loginCustomMessage: '${loginCustomMessage}',
        homePageCustomMessage: '${homePageCustomMessage}',

        // A/B experiment name
        experimentName: '${experimentName}',

        // RuPay flag
        isRupayEnabled: ${isRupayEnabled},

        // coupon flags
        exclusiveCouponsEnabled: ${exclusiveCouponsEnabled},
        couponsRecoEnabled: ${recoCouponsEnabled},

        // build info
        releaseToken: '${releaseType}',

        // CSRF
        csrfToken: '${mt:csrfToken()}',

        // Testimonial version
        testimonialVersionNo: '${testimonialVersionNo}',

        // Redirect Flag
        redirect: false,

        // Fingerprint flag
        fingerPrintingEnabled: ${fingerPrintingEnabled},

        // product info
        productId: displayProductId ? parseInt(displayProductId) : '',
        operatorName: '${displayTabOperatorName}',
        operatorId: '${displayTabOperatorId}',

        // Rewards enabler
        showMissionsTab: ${isMissionsTabEnabled},

        //tracker related
        trackerVisitApi: '${trackerVisitApi}',

        //showRechargePlans-is used in operator landing pages to decide whether to show plans or banner
        showRechargePlans: '${showRechargePlans}',
        walletRedirectUrl: '${walletRedirectUrl}'
    };
  </script>



<tiles:insertAttribute name="js"/>

<script>

    // event for app load
    fcTrack({'eventName' : 'fc_n_load'});
    window.appController = new AppController();
    /* Not the best thing to do....shouldn't defined another Global Variable
     * Try to include this in AppController.
     */
    /*window.FB = new Facebook({showAlert: true});*/
    /* This is assuming this JSP will be used by Desktop website only and not by Mobile website */
    window.appController.view.set('affiliateId', "${affiliateIdWeb}");
    initAppRoutes();
    <%-- This data is pushed to tracker controller in trackerjs.jsp --%>
    var TRACKER_CLIENT_DATA = (function() {
        var userAgent = navigator.userAgent;
        var referrer = document.referrer;
        var params = location.search;

        return {
            "<%=TrackerUtil.CLIENT_DETAILS_PARAM_USER_AGENT%>": userAgent,
            "<%=TrackerUtil.CLIENT_DETAILS_PARAM_REFERRER%>": referrer,
            "<%=TrackerUtil.CLIENT_DETAILS_PARAM_PARAMS%>": params
        };
    })();

    $(document).ready(function() {
        <c:set var="channelId" value="<%=FCConstants.CHANNEL_ID_WEB%>"/>
        $.getScript('${mt:trackerUrl(channelId)}');
        if (console && console.log) {
            console.log("\n\n\n       .d8888888\n      d888888P\"\n     y888\"\n    88888\n 888888888888\n 888888888888\n   .d888P\"\n  .d88P\"\n .d88P\"\n.d888888P\n    d8P\"\n  .d8P\"\n .dP\"\n P\"\n\n\n\n");
        }
    });
</script>

    <script type="text/javascript" src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
    <!-- Wrapping the Recaptcha create method in a javascript function -->
    <script>
    function showRecaptcha(element) {
        var recaptcha_public_key="6LfExOASAAAAADfMsFelOr5AGJIZ0fqHritB70bG";
            Recaptcha.create(recaptcha_public_key, element, {
                theme: "clean"
            });
        }
    function getRecaptchaChallenge() {
            return Recaptcha.get_challenge();
        }
    function getRecaptchaResponse() {
            return Recaptcha.get_response();
        }
    </script>


    <!--<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/custom/AppRoutes.js?v=${mt:keyValue("version.no")}"></script> -->
    <c:if test = "${ mt:keyValue('system.ctx.mode') == 'prod' or mt:keyValue('system.ctx.mode') == 'qa' }" >
        <!-- Google analytics snippet START -->
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-12939857-1']);
        _gaq.push(['_setDomainName', 'freecharge.in']);
        _gaq.push(['_trackPageview']);
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
    </c:if>
    <script>
            var paceOptions = {
              ajax: true, // disabled
              document: true, // disabled
              eventLag: true, // disabled
              elements: {
                selectors: ['.recharge', '.coupon']
              }
    };
    </script>
    <!-- Google analytics snippet END -->

    <c:if test = "${ mt:keyValue('system.ctx.mode') == 'prod' or mt:keyValue('system.ctx.mode') == 'qa' }" >
       <!-- Add_roll visitor pixel starts -->
        <script type="text/javascript">
          adroll_adv_id = "5EOGYDTTQVBELHWLCHCHSF";
          adroll_pix_id = "5K7XUYWONFFNVDEKAGI4IU";
         (function () {
          var oldonload = window.onload;
          window.onload = function(){
          __adroll_loaded=true;
          var scr = document.createElement("script");
          var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
          scr.setAttribute('async', 'true');
          scr.type = "text/javascript";
          scr.src = host + "/j/roundtrip.js";
          ((document.getElementsByTagName('head') || [null])[0] ||
           document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
           if(oldonload){oldonload()}};
        }());
       </script>
        <!-- Add_roll visitor pixel ends -->
    </c:if>
    
    <c:if test = "${ mt:keyValue('system.ctx.mode') == 'prod' or mt:keyValue('system.ctx.mode') == 'qa' }" >
        <!-- FreeCharge_Visitor_Segment_Secure script starts -->
        <script src="https://secure.adnxs.com/seg?add=1651624&t=1" type="text/javascript"></script>
        <!-- FreeCharge_Visitor_Segment_Secure script ends -->
    </c:if>
    <c:if test = "${ mt:keyValue('system.ctx.mode') == 'prod' or mt:keyValue('system.ctx.mode') == 'qa' }" >
        <!-- re-marketing script starts -->
        <script type="text/javascript">
          /* <![CDATA[ */
          var google_conversion_id = 973722500;
          var google_custom_params = window.google_tag_params;
          var google_remarketing_only = true;
          /* ]]> */
        </script>
        <script type="text/javascript"
            src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
            <div style="display: inline;">
             <img height="1" width="1" style="border-style: none;" alt=""
                  src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/973722500/?value=0&amp;guid=ON&amp;script=0" />
            </div>
        </noscript>
        <!--  re-marketing script ends -->
    </c:if>

    <c:if test = "${mt:keyValue('system.ctx.mode') == 'prod'}">
        <!-- Start Visual Website Optimizer Asynchronous Code -->
        <%--
        <script type='text/javascript'>
            var _vwo_code=(function(){
            var account_id=53504,
            settings_tolerance=2000,
            library_tolerance=2500,
            use_existing_jquery=false,
            // DO NOT EDIT BELOW THIS LINE
            f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
        </script>
        --%>
        <!-- End Visual Website Optimizer Asynchronous Code -->
    </c:if>

    <!--- 
    * ###### IMPORTANT. PLEASE DO NOT EDIT ###### 
    * Handling conditional CSS loading for delights 

    * The following code checks for the delights route change 
    * and conditionally adds and removes the css section required
    * for delights to function correctly.

    -->
</body>
</html>