<%@ page import="com.freecharge.tracker.TrackerUtil" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- IMPORTANT: KEEP THIRD-PARTY LIBRARIES IN lib/
     AND DO NOT VERSION JS FILES IN lib/ (?v=XXX)
--%>
<c:choose>
    <c:when test = "${ mt:keyValue('system.ctx.mode') == 'prod' or mt:keyValue('system.ctx.mode') == 'preProd' or mt:keyValue('system.ctx.mode') == 'qa' or mt:keyValue('system.ctx.mode') == 'e2e'}" >
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all.new.lib.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all-new-web-templates.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all-new-web-partials.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all.new.min.js?v=${mt:keyValue("version.no")}"></script>
      <!-- <script type="text/javascript" src="//apis.google.com/js/client:platform.js"></script> -->
    </c:when>
    <c:otherwise>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/jquery.2.1.0.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/stapes.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/path.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/Pace.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/jquery.knob.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/underscore.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/sweet-alert.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/carousel.min.js"></script>
      <!-- <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/lib/jquery.imageScroll.min.js"></script> -->
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/parsley.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/parsley.custom.validation.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/fingerprint.min.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/handlebars.runtime-v1.3.0.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/awesome-select.min.js"></script>
      <!-- <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/fcTip.js"></script> -->
      <!--<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/selectordie.js"></script> -->
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all-new-web-partials.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/dist/all-new-web-templates.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/microlibs.js"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/TurboView.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/HomeView.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/PlansView.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/CouponsView.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/PaymentView.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/SigninView.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/AppView.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/AppModel.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/AppController.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/AppUtil.js?v=${mt:keyValue("version.no")}"></script>
      <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/experiment/custom/AppRoutes.js?v=${mt:keyValue("version.no")}"></script>
      <!-- <script type="text/javascript" src="//apis.google.com/js/client:platform.js"></script> -->
    </c:otherwise>
</c:choose>

<%-- For Google+ login --%>
<script type="text/javascript" src="//apis.google.com/js/platform.js"></script>
