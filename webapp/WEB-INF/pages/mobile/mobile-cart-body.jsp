<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="cart-page" data-lookupid="${lookupId}">
    <h2 class="page-title">My Cart</h2>

    <%--<div class="wrapper">
        <button onclick="history.back()" class="btn full-width">Back</button>
    </div>--%>

    <div class="carts-table">
        <table id="cart-contents">
            <tbody>

            </tbody>
        </table>
    </div>


    <h2 class="page-title inverse">Coupons selected</h2>
    <div class="carts-table">
        <table id="cart-coupons" class="carts-table">
            <tbody>

            </tbody>
        </table>
    </div>

    <div class="wrapper">
        <form name="couponForm" id="checkoutData" method="POST" action="/m/save-coupons">
            <input type="hidden" name="coupons" id="coupons1" value="" />
            <input type="hidden" name="lookupId" id="lookupId" value="${lookupId}" />
            <input type="hidden" name="csrfRequestIdentifier" value="${mt:csrfToken()}" />
            <button type="submit" class='btn btn-primary full-width' id="cart-continue"> Continue </button>
        </form>
    </div>

</div>
