<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>

<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeContextDirectory" %>
<%@page import="com.freecharge.common.framework.session.FreechargeSession"%>
<%@page import="com.freecharge.common.util.FCConstants"%>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ page import ="org.apache.commons.lang.WordUtils" %>
<%@ page import="com.freecharge.web.webdo.CommonSessionWebDo" %>
<%@ page import="java.util.Map" %>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>


    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="fragment" content="!">
    <link href="https://www.freecharge.in"/>
    <tiles:insertAttribute name="css"/>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <title>FREE Online Recharge - Prepaid Mobile, DTH &amp; Data Card Recharge</title>
     <%-- ALL THIRD PARTY SCRIPTS WHICH WILL WORK FOR PROD SHOULD GO IN HERE --%>
    <c:if test = "${mt:keyValue('system.ctx.mode') == 'prod'}">
        <%-- ERRORCEPTION SNIPPET STARTS --%>
        <script>
            (function(_,e,rr,s){_errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
                c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
                c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
                _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)})
                    (window,document,"script","51df8e9c6fdb191b3e000003");
        </script>
        <%-- ERRORCEPTION SNIPPET ENDS --%>

        <%-- Google A/B experiments code STARTS --%>
        <script src="//www.google-analytics.com/cx/api.js?experiment=8N906rX_SgePpDK54wOP5Q"></script>
        <%-- Google A/B experiments code ENDS --%>

        <!-- Google pixel code -->
        <!-- Google Code for Transaction Conversion Page -->
        <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 1002754145;
            var google_conversion_language = "en";
            var google_conversion_format = "2";
            var google_conversion_color = "ffffff";
            var google_conversion_label = "Ru_MCMf3nQMQ4aCT3gM";
            var google_conversion_value = 0;
            var google_remarketing_only = false;
            /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
        
        <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1002754145/?value=0&amp;label=Ru_MCMf3nQMQ4aCT3gM&amp;guid=ON&amp;script=0"/>
            </div>
        </noscript>
        <!-- End of Google code -->

        <!-- Yahoo code STARTS -->
        <script type="text/javascript"> if (!window.mstag) mstag = {loadTag : function(){},time : (new Date()).getTime()};</script> <script id="mstag_tops" type="text/javascript" src="//flex.msn.com/mstag/site/2c445f4e-9aba-412f-b4ee-f752dcde9c27/mstag.js"></script> <script type="text/javascript"> mstag.loadTag("analytics", {dedup:"1",domainId:"2022221",type:"1",revenue:"",actionid:"104703"})</script> <noscript> <iframe src="//flex.msn.com/mstag/tag/2c445f4e-9aba-412f-b4ee-f752dcde9c27/analytics.html?dedup=1&domainId=2022221&type=1&revenue=&actionid=104703" frameborder="0" scrolling="no" width="1" height="1" style="visibility:hidden;display:none"> </iframe> </noscript>
        <!-- Yahoo code ENDS-->

        <!-- Facebook Ads ecommerce code STARTS -->
        <script type="text/javascript">
        var fb_param = {};
        fb_param.pixel_id = '6008985739714';
        fb_param.value = '0.00';
        fb_param.currency = 'INR';
        (function(){
          var fpw = document.createElement('script');
          fpw.async = true;
          fpw.src = '//connect.facebook.net/en_US/fp.js';
          var ref = document.getElementsByTagName('script')[0];
          ref.parentNode.insertBefore(fpw, ref);
        })();
        </script>
        <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6008985739714&amp;value=0&amp;currency=INR" /></noscript>
        <!-- Facebook Ads ecommerce code ENDS -->

    </c:if>
    
    <!--[if lt IE 9]>
    <script src='${mt:keyValue("jsprefix2")}/singlepage/lib/html5shim.js?v=${mt:keyValue("version.no")}'></script>
    <![endif]-->

</head>

<body>
<noscript>
    <div class="no-js">
        <p>JavaScript is disabled in your browser! FreeCharge requires javascript.</p>
    </div>
</noscript>

<%
    FreechargeSession freechargesession = FreechargeContextDirectory.get().getFreechargeSession();
    Object login=false;
    String name="";
    String email="";
    if(freechargesession!=null){
        Map<String,Object> sessionData=freechargesession.getSessionData();
        if(sessionData!=null)
            login=sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
        name = (String)sessionData.get(WebConstants.SESSION_USER_FIRSTNAME);
        if(name!=null && !name.isEmpty())
            name =  WordUtils.capitalize(name);
        email = (String)sessionData.get(WebConstants.SESSION_USER_EMAIL_ID);
    }

    String lookupid = request.getParameter("lookupID");
    String lid= request.getParameter("lid");
    if(lookupid == null && lid != null){
        lookupid = lid;
    }
    CommonSessionWebDo commonSessionWebDo = (CommonSessionWebDo) freechargesession.getSessionData().get(lookupid+"_CommonSessionPojo");
    String productType = "";
    if( commonSessionWebDo != null ){
        productType = commonSessionWebDo.getType();
    }
    Integer productId = FCConstants.productMasterMapReverse.get(productType);

%>

<c:set  value="<%=login%>" var="login" />
<c:set  value="<%=name%>" var="name" />
<c:set  value="<%=email%>" var="email" />
<c:set  value="<%=lookupid%>" var="lookupid" />
<c:set  value="${orderId}" var="orderId" scope="request"/>
<c:if test="${empty lookupid}">
    <c:set  value="${lookupId}" var="lookupid" />
</c:if>


<div class="wrapper" id="freechargeApp" 
    data-csrf-token='${mt:csrfToken()}'
    data-img-prefix1='${mt:keyValue("imgprefix1")}'
    data-img-prefix2='${mt:keyValue("imgprefix2")}'
    data-version = '${mt:keyValue("version.no")}'
    data-template-prefix = '${mt:keyValue("templateprefix")}'
    data-login-status='${login}' 
    data-name='${name}' 
    data-email='${email}' 
    data-lookup-id='${lookupid}' 
    data-order-id='${orderId}' 
    data-ext='${ext}' 
    data-ext-type='${extType}' 
    data-ext-name='${extName}' 
    data-product-id='${displayProductId}'
    data-operator-name="${displayTabOperatorName}" 
    data-redirect="false"
    data-zedo-ad-enabled="${isZedoEnabled}">

    
    <!-- Aman removed the JSP, this should save us one call to fetch the JSP  -->
    <header class="navbar navbar-default header custom-grid" id="user-account"></header>
    
    <!-- The content of the page goes here -->
    <section class="content-body custom-grid" id="main"></section>
    <!-- <section class="content-body">
        <div class="container">
            <div id="main"></div>
        </div>
    </section> -->
</div>
<c:if test="${isZedoEnabled}">
    <div id="zedoAd" style="display:none;">
        <!-- Javascript tag  -->
        <!-- begin ZEDO for channel:  Post Success Page 72890 , publisher: freecharge.in , Ad Dimension: Super Banner - 728 x 90 -->
        <script language="JavaScript">
            var zflag_nid="2427"; var zflag_cid="10"; var zflag_sid="2"; var zflag_width="728"; var zflag_height="90"; var zflag_sz="14";
        </script>
        <script language="JavaScript" src="https://saxp.zedo.com/jsc/sxp2/fo.js"></script>
        <!-- end ZEDO for channel:  Post Success Page 72890 , publisher: freecharge.in , Ad Dimension: Super Banner - 728 x 90 -->
    </div>
</c:if>

<tiles:insertAttribute name="js"/>

<%--needed for ardeal--%>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/singlepage/lib/jquery.barrating.min.js"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/singlepage/lib/bootstrap-datepicker.js"></script>
<%--ends--%>

<script type="text/javascript">
    window.appController = new AppController();
    
    $(document).ready(function() {
        <c:set var="channelId" value="<%=FCConstants.CHANNEL_ID_MOBILE%>"/>
        $.getScript('${mt:trackerUrl(channelId)}');
    });

    window.appController.view.set('viewName', 'rechargeSuccessPage');

    var fcUserState = $('#freechargeApp');
    var page = appController.view.get('viewName');
    var hookPoint = "";
    var csrfToken = '${mt:csrfToken()}';
    if(page == 'rechargeSuccessPage'){
        hookPoint = "page5";
    }else if(page == 'mobilePage' || page == 'dthPage' || page == 'datacardPage'){
        hookPoint = "page1";
    }//Add rest of the pages later

    $('body').on('domloaded', function(){

        if(hookPoint != ""){
            $.ajax({
                url: "${mt:keyValue("jsprefix2")}/singlepage/custom/ardeal.js?v=${mt:keyValue("version.no")}",
                dataType: 'script',
                success: function(response){
                    $('#final-page-deals').ardeals({
                        hookPoint: 'page5',
                        lookupId: fcUserState.data('lookupId')
                    });
                }
            });
        }
    });

     var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-12939857-1']);
    _gaq.push(['_setDomainName', 'freecharge.in']);
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<c:if test="${isSokratiPixelEnabled}">
    <%@include file="../layouts/pixels/sokratiPixel.jsp" %>
</c:if>

<!-- Gamooga Second Screen -->
<c:if test = "${mt:keyValue('system.ctx.mode') == 'prod'}">
  <%@include file="../layouts/pixels/gamoogaPixel.jsp" %>
</c:if>

</body>
</html>
