<%@page import="com.freecharge.util.*;"%>
<!doctype html>
<html lang="en">
<head>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200'
	rel='stylesheet' type='text/css'>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport"
	content="width=100%, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
<title>App Install Campaign</title>
<style>
header {
	padding: 10px;
	text-align: center;
	margin-top: 1em;
}

body {
	background-color: #356BC1;
	font-family: 'Source Sans Pro', ​sans-serif;
	font-size: 18px;
	line-height: 1.4;
	margin: 0;
	color: #FFFFFF;
}

#content,#terms-wrapper {
	width: 94%;
	margin: 0 auto;
}

.redirect-btn {
	color: #356BC1;
	text-decoration: none;
	font-size: 1.6em;
	font-weight: bold;
	display: block;
	width: 100%;
	padding: 10px;
	margin: 1em auto;
	text-align: center;
	background-color: #FFFFFF;
	border-color: #999999;
}

p {
	text-align: center;
	margin: 0;
	font-size: 1.2em;
}

.cashback-msg {
	font-weight: bold;
}

#terms-wrapper {
	border-top: 1px solid #FFFFFF;
	font-size: 1em;
	margin: 2em auto;
}

#terms-heading {
	text-align: center;
	margin: 1em;
}

.terms-mark {
	vertical-align: text-top;
}

#banner {
	text-align: center;
	margin-bottom: 1em;
}

#banner #cb-msg {
	background-color: #F7D251;
	box-shadow: 0px 2px 4px 0px #999999;
	border-radius: 2%;
	display: inline-block;
	font-size: 2em;
	font-weight: bold;
	color: #000000;
	padding: 0.2em 0;
	width: 90%
}
</style>
<script type="text/javascript">
		 window.switchTemplate = function() {
			var operatingSystem = document.getElementById('OS').value,
				browser = document.getElementById('browser').value,
				useragent = document.getElementById('useragent').value, i = 0,
				appTemplate = document.getElementsByClassName('app-template'),
				siteTemplate = document.getElementsByClassName('site-template');

			if( hasAndroid(operatingSystem) || hasAndroid(browser) || hasAndroid(useragent) ) {
				// Hide mobile site template for Android devices
				for(i=0; i<siteTemplate.length; i++) {
					siteTemplate[i].style.display = 'none';
				}

			} else {
				// Hide App Template for Non Android devices
				for(i=0; i<appTemplate.length; i++) {
					appTemplate[i].style.display = 'none';
				}
			}
		}
		window.hasAndroid = function(strToCheck) {
			return (strToCheck.toLowerCase().indexOf('android') !== -1);
		}
		
        function getEmail() {
            // Split into key/value pair
            var queryString = window.location.search.substring(1);
            var query = queryString.split("=");
            return query[1];
        };

		trackClick = function() {
		    function handler() {
		        if (oReq.readyState == 4 /* complete */ ) {
		            if (oReq.status == 200) {
		            	console.log(this.responseText);
		                var json = JSON.parse(this.responseText);
		            	window.location="https://play.google.com/store/apps/details?id=com.snapdeal.main&referrer=utm_source%3Dfreecharge%26utm_medium%3Dmobileapp" +
		            			"%26utm_campaign%3Dfc-appinstall%26deviceid%3DABCD";
		            }
		            else {
		            	window.location="/m?utm_source=freecharge&utm_medium=redirect&utm_campaign=fc-appinstall";
		            }
		        }
		    }

		    var oReq = new window.XMLHttpRequest;
		    if (oReq != null) {
		    	var data = JSON.stringify({"um":getEmail(),"pnm":"snapdeal", "uuid": "ABCD"});
		    	console.log(data);
		    	
		    	oReq.open("POST", "http://mobileapps.freecharge.com/clicktracking");
		        oReq.setRequestHeader('Content-Type', "application/json;charset=UTF-8");
		        oReq.setRequestHeader("Content-Length", data.length);
		        oReq.onreadystatechange = handler;
		        oReq.send(data);
		    } else {
		        window.alert("AJAX (XMLHTTP) not supported.");
		    }
		}
	</script>
</head>
<body onload="switchTemplate();">

	<!-- Header -->
	<header>
		<!-- Logo -->
		<img src="../../content/images/m/logo-large.png" alt="freecharge.in">
	</header>

	<div id="content">
		<div id="banner">
			<span id="cb-msg">10% CASHBACK<span class="terms-mark">*</span></span>
			<p>See T&amp;C below</p>
		</div>
		<div class="cashback-msg">
			<p>Recharge your prepaid mobile and get upto Rs.50 cashback
				today!</p>
			<p>(valid only on first recharge)</p>
		</div>
		<button type="button" class="site-template redirect-btn"
			onclick="trackClick(this);">Install SnapDeal App</button>
	</div>

	<div id="terms-wrapper">
		<h4 id="terms-heading">
			<span class="terms-mark">*</span>Terms &amp; Conditions
		</h4>
		<ul id="terms">
			<li>You can get 10% on the value of recharge/bill payment or a
				maximum of Rs.50 as cash back , whichever is lower</li>
			<li>This offer is only valid on one recharge/bill payment per
				user/device</li>
			<li>This offer is only valid on March 10th 2014</li>
			<li>Existing FreeCharge users can login with their existing
				FreeCharge username andpassword</li>
			<li>If you are new to FreeCharge, register for a new account</li>
			<li>The cash back will be added to your FreeCharge account
				within 2-3 business days</li>
			<li>Valid only on mobile site and Android app</li>
		</ul>
	</div>

	<input type="hidden" id="OS"
		value="<% out.println(RequestUtil.getOSFromUserAgent(request.getHeader("User-Agent"))); %>">

	<input type="hidden" id="browser"
		value="<% out.println(RequestUtil.getBrowserFromUserAgent(request.getHeader("User-Agent"))); %>">

	<input type="hidden" id="useragent"
		value="<% out.println(request.getHeader("User-Agent")); %>">

</body>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12939857-1']);
  _gaq.push(['_setDomainName', 'freecharge.in']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</html>