<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
	<head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="alexaVerifyID" content="_ld1uhM6KiPm6ru7cNPKE2MvpTg">
        <title>McDonalds gift festival</title>
		<meta name="description" content="Easy &amp; FREE Recharge service for prepaid mobile, DTH and data card. Instant Recharge for all Mobile and DTH operators using credit card, debit card, net banking and cash card. Get FREE COUPONS &amp; offers of equal and higher value.">
		<meta name="keywords" content="Online Recharge,Mobile Recharge,Prepaid Recharge,Airtel,Vodafone,BSNL,Reliance,Docomo,Idea,Aircel,Free Online mobile recharge, free refill, free talk time, free prepaid mobile recharge,Free E-recharge, easy &amp; instant mobile recharge, online recharge vouchers, online top up &amp; Recharge airtel, free vodafone prepaid mobile recharge, reliance top up online, idea online top up, tata indicom prepaid top up, bsnl mobile recharge, docomo, aircel, videocon, loop, uninor, mts, s tel recharge &amp; top up services">
		<link href="https://www.freecharge.in">
	    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no">
        <link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/pepsi/m-style.min.css?v=${mt:keyValue("version.no")}" >	
    </head>
	<body>
	<div class="pepsipromotion">
		<div class="banner" style="background-color: #640012;">
			<img src="${mt:keyValue('imgprefix1')}/images/banner/mcdGiftFestival-small.png">
			<h2>Rs.400 Crores&apos; worth of Assured Gifts</h2>
		</div>
		<div class="sub-header">
			<h4>How to Redeem</h4>
			<p>and get Rs.20 talktime using your unique code.</p>
			<hr>
			<ol class="steps">
				<li>Select &lsquo;Mobile&rsquo;</li>
				<li>Enter your prepaid mobile details to recharge your phone.</li>
				<li>Pick coupons for the value of your recharge. <em>(This is optional and a handling charge of Rs.10 is
				 applicable. You could CONTINUE with or without coupons.)</em></li>
				<li>If you are already a FreeCharge user, just enter your log in details.</li>
				<li>If you are new to FreeCharge, please sign up.</li>
				<li>Enter the unique 11-digit code mentioned under the label.</li>
				<li>After you successfully apply your promocode, click &lsquo;Continue&rsquo;.</li>
				<li>Recharging for more than Rs.20? Just pay the remaining amount.</li>
				<li>That&#39;s it. Your Recharge is done!</li>
			</ol>
			<div class="cta">
				<p>Okay, I understand. Take me to FreeCharge.</p>
				<a class="button" href="/m">go</a>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		mixpanel.track("Startup", {
			Campaign : "Mc Donalds"
		});
	</script>
	</body>
</html>
