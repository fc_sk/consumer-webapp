<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ page import="com.freecharge.payment.util.PaymentConstants" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%
    List<String> allowablePGList = (List<String>) request.getAttribute("allowablePGList");
    if (allowablePGList == null)
        allowablePGList = new ArrayList<String>();
%>
<script type="text/javascript">
    FB_CONNECT = false;
</script>
<div class="payment">
    <h2 class="page-title">Review & pay</h2>
  
<c:if test="${isDeletedFreefund == true}">
  <p class="failureMsgBig"  id="GAcall" >Damn! Your Promo code has been reset. Don&#39;t want to miss your discount? Re-apply and retry!</p>
</c:if>

    <c:if test="${checkoutData.isPaymentSuccessful != null && !checkoutData.isPaymentSuccessful}">
        <div><spring:message code="m.payment.error.msg" text="#m.payment.error.msg#"/></div>
    </c:if>

    <div id='total-payable-amount' class='payable-amount' data-total-payable-amount="${payableTotalAmount}">
        <p>
            <spring:message code="m.total.payable.amount" text="#m.total.payable.amount#"/> <span class='amount fc-selected-coupon-count-badge'><span class="webRupee "><spring:message code="m.rs" text="#m.rs#"/>.</span> <c:out value="${payableTotalAmount}" /></span>
        </p>
        <c:if test="${not showFBConnect}">
            <p class='helper-text'>Pay for the transaction using Credits or any of the other payment options</p>
        </c:if>
    </div>

    <!-- Partial payment starts -->
    <c:if test="${checkoutData.paymentOptions.usePartialPayment}">
        <div class="wrapper partial-payment" id="partial-payment-set" data-is-partial-payment-used="${checkoutData.paymentOptions.usePartialPayment}">
            <c:if test="${checkoutData.paymentOptions.walletEligibility.eligibleForPartialPayment && checkoutData.productType!= 'T'}">
                <input type="checkbox" id="mobileUsePartialWalletPaymentCheck"  name="mobilePartialEnabled"/>
                <label for="mobileUsePartialWalletPaymentCheck"><spring:message code="m.use.balance" text="#m.use.balance#"/><span class='help'></span></label>

                <div id="mobileWalletPayableAmountArea">
                    <p class="info">
                        <spring:message code="m.balance.used" text="#m.balance.used#"/>
                        <label class='amount'> - <spring:message code="m.rs" text="#m.rs#"/>. <c:out value="${checkoutData.paymentOptions.paymentBreakup.payableWalletAmount}" /></label>
                    </p>
                </div>
                <div id="mobileOutstandingPayAmountArea">
                    <p class="info">
                        <spring:message code="m.outstanding.amount.payable" text="#m.outstanding.amount.payable#"/>
                        <label class='amount'> - <spring:message code="m.rs" text="#m.rs#"/>. <c:out value="${checkoutData.paymentOptions.paymentBreakup.payablePGAmount}" /></label>
                    </p>
                </div>

            </c:if>
        </div>
    </c:if>
    
    
<%
   if (allowablePGList.contains(PaymentConstants.ZEROPAY_PAYMENT_OPTION)) {%>
    <c:if test="${showFBConnect}">
        <p>Authenticate using facebook to redeem your promocode.</p>
        <a href="#" id="fb-connect"><img src="${mt:keyValue("imgprefix1")}/images/social/fb-connect.png"></a>
        <script type="text/javascript">
            FB_CONNECT = true;
            _jsPrefix = '${mt:keyValue("jsprefix1")}';
            _version = '${mt:keyValue("version.no")}';
            _imgPrefix = '${mt:keyValue("imgprefix1")}/images';
        </script>
    </c:if>
    <div id="zeroPaymentOptionsBlock" > <%--Call refreshAmountToPay(lid) to enable this --%>
        <form:form commandName="checkoutData" action="/m/payment/request/do-zero-payment-pay" method="POST"
                   name="zeroPaymentForm" id="zeroPaymentForm" class="form">
	        <form:hidden path="productType" />
	        <form:hidden path="lookupId" />
	        <form:hidden path="totalAmount" />
	        <form:hidden path="paymentOptions.usePartialPayment" id="PartialPayment"/>
        </form:form>
   </div> 
<%
     } else {
 %>

 <%
     if (allowablePGList.contains(PaymentConstants.ALL)) {
 %>    
    <c:if test="${checkoutData.productType!= 'T' && !checkoutData.paymentOptions.walletEligibility.eligibleForPartialPayment}">
        <div class="accordionButton credits-option" data-credits-pay="true">
            <h3><spring:message code="fc.money" text="#fc.money#"/> <span class="credit-amount-badge">
                	Rs. <c:out value="${checkoutData.paymentOptions.wallet.balance.amount}"></c:out>
                </span>
                
            </h3>
        </div>
        <div class="accordionContent">
            <jsp:include page="mobile-fc-balance-form.jsp"></jsp:include>
        </div>
    </c:if>
    
   

    <div class="accordionButton saved-cards-option" data-collapsed='<%--<c:out value="${!checkoutData.paymentOptions.debitCard.selected}"/>--%>' data-payment-type="saved_cards">
        <h3><spring:message code="m.savedcard.label" text="#m.savedcard.label#"/></h3>
    </div>
    <div class="accordionContent">
        <jsp:include page="mobile-saved-card-form.jsp"></jsp:include>
    </div>
    <div class="accordionButton" data-collapsed='<c:out value="${!checkoutData.paymentOptions.debitCard.selected}"/>' data-payment-type="debit">
        <h3><spring:message code="m.debitcard.label" text="#m.debitcard.label#"/></h3>
    </div>
    <div class="accordionContent">
        <jsp:include page="mobile-debit-card-form.jsp"></jsp:include>
    </div>
    <div class="accordionButton" data-collapsed='<c:out value="${!checkoutData.paymentOptions.creditCard.selected}"/>' data-payment-type="credit">
        <h3><spring:message code="m.creditcard.label" text="#m.creditcard.label#"/></h3>
    </div>
    <div class="accordionContent">
        <jsp:include page="mobile-credit-card-form.jsp"></jsp:include>
    </div>
    <div class="accordionButton" data-collapsed='<c:out value="${!checkoutData.paymentOptions.netBanking.selected}"/>' data-payment-type="netbanking">
        <h3><spring:message code="m.netbanking.label" text="#m.netbanking.label#"/></h3>
    </div>
    <div class="accordionContent">
        <jsp:include page="mobile-net-banking-form.jsp"></jsp:include>
    </div>
    
    
    
    <% } else if (allowablePGList.contains(PaymentConstants.ICICNETBANKING_PAYMENT_OPTION)) { %>
    
	
		
	<form:form commandName="checkoutData"
	action="/m/payment/request/do-net-banking-pay" method="POST"
	name="netBankingForm" id="netBankingForm" class="form">
    <div class="field-unit">
        <form:select path="paymentOptions.netBanking.bankCode" cssClass="input-field input-field-select-small">
            <form:option value="<%=PaymentConstants.ICICNETBANKING_PAYMENT_OPTION %>"><spring:message code="m.select.icici.net.banking" text="#m.select.icici.net.banking#"/></form:option>
        </form:select>
        <form:errors element="div" cssClass="error" path="paymentOptions.netBanking.bankCode"></form:errors>
    </div>

	<form:hidden path="productType" />
	<form:hidden path="lookupId" />
	<form:hidden path="totalAmount" />
	<form:hidden path="paymentOptions.usePartialPayment" id="netBankingPartialPayment"/>
    <button type="submit" class="btn btn-primary full-width">
        <spring:message code="m.pay.label" text="#m.pay.label#"/>
    </button>
</form:form>

<div>
  Don't Want to use Promo Code ?<a href="/m">Click Here</a>
</div>
	
	<%
	    } else if (allowablePGList.contains(PaymentConstants.HDFCDEBITCARD_PAYMENT_OPTION)) {
	%>
               <form:form commandName="checkoutData"
	action="/m/payment/request/do-hdfc-debit-pay" method="POST"
	name="netBankingForm" id="netBankingForm" class="form">
    <div class="field-unit">
        <form:select path="" cssClass="input-field input-field-select-small">
            <form:option value="<%=PaymentConstants.PAYMENT_TYPE_HDFC_DEBIT_CARD%>"><spring:message code="m.select.hdfc.debit.card" text="#m.select.hdfc.debit.card#"/></form:option>
        </form:select>
        <form:errors element="div" cssClass="error" path="paymentOptions.netBanking.bankCode"></form:errors>
    </div>

	<form:hidden path="productType" />
	<form:hidden path="lookupId" />
	<form:hidden path="totalAmount" />
	<form:hidden path="paymentOptions.usePartialPayment" id="netBankingPartialPayment"/>
    <button type="submit" class="btn btn-primary full-width">
        <spring:message code="m.pay.label" text="#m.pay.label#"/>
    </button>
</form:form>

<div>
  Don't Want to use Promo Code ?<a href="/m">Click Here</a>
</div>
			
<%
    }
%>
<%
    }
%>
</div>




</script>
