<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.freecharge.common.util.FCConstants"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>

<c:set var="walletDisplayName"
	value="<%=FCConstants.WALLET_DISPLAY_NAME%>"></c:set>
	
	

<div class="account">
    <h2 class="page-title">My Credits</h2>
    <c:if test="${paymentStatus eq true}">
        <div class="wrapper">
            <p class="message-success-small">You successfully deposited cash in your Balance.</p>
        </div>
    </c:if>

    <c:choose>
        <c:when test="true">
            <div class="wrapper">
                <div class="credits-text">
                   <!--   <img src="${mt:keyValue("imgprefix1")}/images/m/rupee-lock.png" />-->
                    <p>You have <strong><span class="webRupee"><spring:message code="rupee" text="#rupee#" />.</span> ${balance}</strong> available.</p>
                    <p class="helper-text">Credits can be used to recharge any time in the future. Credits also make the recharging process 10x faster!</p>
                </div>

                <%--<p><strong>Last activity: </strong>
                    <c:choose>
                        <c:when test="${not empty lastEntry}">
                            <c:choose>
                                <c:when test="${lastEntry.IS_DEBIT}">
                                    <spring:message code="payment.using" text="#payment.using#" />
                                    <span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span> ${lastEntry.txn_amount}
                                </c:when>
                                <c:otherwise>
                                    <span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span> ${lastEntry.txn_amount} <spring:message code="deposited.fc" text="#deposited.fc#" />
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            None
                        </c:otherwise>
                    </c:choose>
                </p>
                <p><strong>Order ID: </strong> ${lastEntry.order_id}</p>
                <p><strong>Date:</strong> ${lastEntry.created_ts}</p>--%>
            </div>
            
            <div class="account">

        <ul class="menu menu-icons">
            <li class="option"><a href="/m/addcash">
                <img src="${mt:keyValue("imgprefix1")}/images/m/account/acc-add-credit.png">
                Add Cash to Credits
                <img src="${mt:keyValue("imgprefix1")}/images/m/chevron.png" class="menu-indicator"/>
            </a></li>
            <li class="option"><a href="/m/freefund">
                <img src="${mt:keyValue("imgprefix1")}/images/m/account/acc-freefund.png">
                Redeem FreeFund code
                <img src="${mt:keyValue("imgprefix1")}/images/m/chevron.png" class="menu-indicator"/>
            </a></li>
            <li class="option"><a href="/m/credits-activity">
                <img src="${mt:keyValue("imgprefix1")}/images/m/account/acc-credit-activity.png">
                Credits Activity
                <img src="${mt:keyValue("imgprefix1")}/images/m/chevron.png" class="menu-indicator"/>
            </a></li>
        </ul>

</div>
            
         
     
        </c:when>
        <c:otherwise>
            <p><strong>Currently ${walletDisplayName} is under maintenance.</strong></p>
        </c:otherwise>
    </c:choose>
   
</div>