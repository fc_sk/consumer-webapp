<%@page pageEncoding="UTF-8"%>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="mobile-common-includes.jsp" />
</head>
<body>
    <div id="mobile-coupons-select-page" class="coupons-page" data-lookupid="${checkoutData.lookupId}">
        <jsp:include page="mobile-header.jsp" />

        <!-- <h2 class="page-title">Pick your free coupons</h2> -->
        
        <div class="couponnext boxmodel">
          <a class="fc-silent-button pull-left couponcount" id="cartBtn" href='javascript:void(0)'><span class='coupon-number'>0</span> picked</a>
          <div class="pull-right">
            <form:form  commandName="checkoutData" action="/m/save-coupons" method="POST"  name='couponForm'>
              <form:hidden path="rechargeAmount"/>
              <form:hidden path="coupons" id="coupons1"/>
              <form:hidden path="lookupId"/>
              <button type="submit" class='btn-primary btn-coupon' id="coupon-continue-bottom"> Continue </button>
            </form:form>
          </div>
          
        </div>
        
        <div class ="fc-error-message-bar mobile-dialog">
            <p class="fc-message-text mobile-dialog-text"></p>
        </div>
        
        <div class="pagehead textcenter">
	      You can pick coupons worth ₹<span id="coupon-available-amount"></span>
	    </div>
	    
	    
        
        <div class="content">
            <div class="fc-menu-container">
                <div class="coupons-menu clearfix">
                    <select id="filterBtn" class="pull-right coupon-categories"></select>
                </div>
            </div>

            <div class="fc-coupons">
                <div id="wrapper" class="fc-coupon-wrapper">

                </div>
            </div>
        </div>

        <jsp:include page="mobile-footer.jsp" />


        <jsp:include page="mobile-bottom-scripts.jsp" />

        <script src="${mt:keyValue("jsprefix1")}/amplify.min.js"></script>
        <script src="${mt:keyValue("jsprefix1")}/mustache.min.js"></script>
        <script src="${mt:keyValue("jsprefix1")}/mobile/mobile.coupons.js?v=${mt:keyValue("version.no")}"></script>

    </div>

</body>
</html>
