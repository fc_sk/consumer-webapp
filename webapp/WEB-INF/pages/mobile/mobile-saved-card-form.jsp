<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div id='stored-cards-layout'>
	<form:form commandName="checkoutData" method="POST" name="saved-card-form" id="saved-card-form" autocomplete="off" class="form" data-ajax="false">
		<div class="saved-cards"></div>
		<%--DEBIT CARD FIELD --%>
		<form:hidden path="paymentOptions.debitCard.cvvNumber" maxlength="3" autocomplete="off" id='saved-debitcard-cvv-number'/>
		<form:hidden path="paymentOptions.cardStorage.debitCardToken"  id='saved-debit-card-token'/>
		<form:hidden path="paymentOptions.cardStorage.cardType" id='saved-debitcard-type'/>  
		
		<%--CREDIT Cards field --%>
		<form:hidden path="paymentOptions.creditCard.cvvNumber" id='saved-creditcard-cvv-number'/>
		<form:hidden path="paymentOptions.cardStorage.creditCardToken" id='saved-credit-card-token'/>
		<form:hidden path="paymentOptions.cardStorage.cardReference" id='saved-credit-card-reference'/>
		
		<%-- Common fields, need not set them --%>
		<form:hidden path="lookupId" id='look-up-id'/>
		<form:hidden path="totalAmount" id='total-amount'/>
		<form:hidden path="productType" id='product-type'/>
		<form:hidden path="paymentOptions.usePartialPayment" id="cardStoragePartialPayment"/>
	</form:form>   
	
	<div class="stored-card-pay">
        <button type='submit' class="btn btn-primary full-width paymentTypeOption" data-type="Stored Cards">
            <spring:message code="m.pay.label" text="#m.pay.label#"/>
        </button>
	</div>

</div>
<div id='no-stored-cards'>
	<p class="info"><spring:message code="m.nostoredcard.label" text="#m.nostoredcard.label#"/></p>
</div>