<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="rememberLabel" scope="request"><spring:message code="m.remember.label" text="#m.remember.label#"/></c:set>

<div class="recharge" data-product="Mobile">
	<h2 class="page-title"><spring:message code="m.prepaid.mobile.recharge" text="#m.prepaid.mobile.recharge#"/></h2>

        <form:form commandName="mobileRecharge"
            action="/m/do-mobile-recharge" method="POST" name="mobileForm"
            id="mobileForm" class="form">

            <div class="form-wrap">
                <div id="mobileNumberWrap" class="field-unit">
                    <label for="mobileNumber"><spring:message code="m.mobile.number" text="#m.mobile.number#"/></label>
                    <form:input path="mobileNumber" type="tel" maxlength="10" cssClass="input-field" />
                    
                    <c:choose>
                        <c:when test="${retryBlock eq true}">
                          <p class="error">Please try recharging this number after 10 minutes.</p>
                        </c:when>
                        
                        <c:when test="${denominationFailure eq true}">
                          <p class="error">You have entered an invalid denomination, please check for valid denominations.</p>
                        </c:when>
                        
                        <c:when test="${permanentFailure eq true}">
                          <p class="error">Please select the right operator or choose different service (DTH, Data Card or Postpaid)</p>
                        </c:when>
                        
                        <c:when test="${egFailure eq true}">
                          <p class="error">Please select the right amount, operator or choose different service (DTH, Data Card or Postpaid)</p>
                        </c:when>
                        
                        <c:when test="${operatorFailure eq true}">
                          <p class="error">We are observing a high failure rate for this operator in the last few minutes, please try after some time</p>
                        </c:when>
                        
                        <c:otherwise></c:otherwise>
                    </c:choose>
                    
                    <p class="error" style="display: none"></p>
                    <form:errors path="mobileNumber" element="div" cssClass="error"></form:errors>
                </div>

                <div id="mobileOperatorWrap" class="field-unit field-unit-crippled">
                    <label for="operator"><spring:message code="m.mobile.operator" text="#m.mobile.operator#"/></label>
                    <form:select path="operator" cssClass="input-field">
                        <form:option value=""><spring:message code="m.select.operator" text="#m.select.operator#"/></form:option>
                        <form:options items="${mobileRecharge.masterData.mobileOperators}"
                                      itemLabel="name" itemValue="operatorMasterId" />
                    </form:select>
                    <p class="error" style="display: none"></p>
                    <form:errors path="operator" element="div" cssClass="error"></form:errors>
                </div>

                <div id="mobileCircleWrap" class="field-unit field-unit-crippled">
                    <label for="circle"><spring:message code="m.mobile.circle" text="#m.mobile.circle#"/></label>
                    <form:select path="circle" cssClass="input-field">
                        <form:option value=""><spring:message code="m.select.circle" text="#m.select.circle#"/></form:option>
                        <form:options items="${mobileRecharge.masterData.mobileCircles}"
                                      itemLabel="name" itemValue="circleId" />
                    </form:select>
                    <p class="error" style="display: none"></p>
                    <form:errors path="circle" element="div" cssClass="error"></form:errors>
                </div>

                <div id="mobileRechargeTypeWrap" class="field-unit-crippled rechargeType" style="display: none;">
               <label class="radio-inline"> <input
                   type="radio" name="rechargetype" value="topup"> Top Up
               </label> <label class="radio-inline"> <input type="radio"
                   name="rechargetype" value="special"> Special
                   (2G,3G,SMS,Voice)
               </label>
               <p class="error" style="display: none"></p>
              </div>

           <div id="mobileAmountWrap" class="field-unit field-unit-stroke">
                    <label for="amount"><spring:message code="m.mobile.recharge.amount" text="#m.mobile.recharge.amount#"/></label>
                    <form:input path="amount" type="tel" cssClass="input-field" maxlength="4" />
                    <p class="error" style="display: none"></p>
                    <form:errors path="amount" element="div" cssClass="error"></form:errors>
                </div>
            </div>
            <div class="field-unit-button">
               <button id="mobile-recharge-submit" type="submit" class="btn btn-primary full-width"><spring:message code="m.continue.label" text="#m.continue.label#"/></button>
            </div>
        </form:form>

	
</div>