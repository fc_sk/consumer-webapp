<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div data-role="content">
 	<h2 class="page-title"><spring:message code="m.oops.pg.overloaded" text="#m.oops.pg.overloaded#"/>.</h2>
     <p><spring:message code="m.we.are.sorry" text="#m.we.are.sorry#"/> !!!</p>
     <p><spring:message code="m.pg.overloaded.msg" text="#m.pg.overloaded.msg#"/>.</p>
     <p><spring:message code="m.or.you.may.go.back" text="#m.or.you.may.go.back#"/> <a href="/m" rel="external"><spring:message code="m.homepage" text="#m.homepage#"/></a>.</p>     
</div>
     



