<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ page
	import="com.freecharge.common.framework.session.FreechargeSession"%>
<%@ page
	import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<%@ page import="java.util.Map"%>
<%@ page import="com.freecharge.payment.util.PaymentConstants"%>
<%@ page import="com.freecharge.web.util.WebConstants"%>
<%@ page import="com.freecharge.common.util.FCConstants"%>
<%@ page import="com.freecharge.web.webdo.CommonSessionWebDo"%>

<%
    FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    if (fs != null) {
        Map<String, Object> sessionData = fs.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
    }
    String prodType = FCSessionUtil.getRechargeType(request.getAttribute("lookupId"), fs);
    Integer prodId = FCConstants.productMasterMapReverse.get(prodType);
%>
<c:set value="<%=login%>" var="login" />

<c:set var="cartItems" value="${cart.itemsList}" scope="request" />
<c:set var="cartId" value="${cart.id}" scope="request" />
<c:set var="primaryProductType"
	value='<%=FCConstants.PRIMARY_PRODUCT_TYPE%>' scope="application" />
<c:set var="secondaryProductType"
	value='<%=FCConstants.SECONDARY_PRODUCT_TYPE%>' scope="application" />
<c:set var="chargesProductType"
	value='<%=FCConstants.CHARGES_PRODUCT_TYPE%>' scope="application" />
<c:set var="crosssellEntityName"
	value='<%=FCConstants.ENTITY_NAME_FOR_CROSSSELL%>' scope="application" />
<c:set var="couponEntityName" value='<%=FCConstants.COUPON_ENTITY_ID%>'
	scope="application" />
<c:set var="couponCounter" value='0' />
<c:set var="crossSellCounter" value='0' />
<c:set var="totalCouponPrice" value='0' />
<c:set var="totalCrossSellPrice" value='0' />
<c:set var="charges" value='0' />
<c:set var="lid" value='${lookupId}' />

<c:set var="lookupID" value="${lookupId}"
	scope="request" />
<c:set var="walletDisplayName"
	value="<%=FCConstants.WALLET_DISPLAY_NAME%>"></c:set>

<div data-role="content">
	<%-- Payment options page --%>
	<c:forEach items="${paymentTypes}" var="pts" varStatus="summaryCtr">
		<c:forEach items="${pts.options}" var="opts" varStatus="summaryCtr1">
			<c:out value="${pts.name}"/>
			<c:choose>
				<c:when test="${pts.name eq walletDisplayName}">
					<c:choose>
						<c:when test="${isWalletDataDisplayEnabled}">
							<c:choose>
								<c:when test="${cart.totalCartPrice le walletBalance}">
									<c:set var="enabledWalletDisplay" value="block"></c:set>
									<c:set var="disabledWalletDisplay" value="none"></c:set>
								</c:when>
								<c:otherwise>
									<c:set var="enabledWalletDisplay" value="none"></c:set>
									<c:set var="disabledWalletDisplay" value="block"></c:set>
								</c:otherwise>
							</c:choose>

							<div class="disabledWallet"
								style="display: ${disabledWalletDisplay}">
								<p>
									D'oh! Your current balance of <strong><span
										class="webRupee">Rs.</span> ${walletBalance}</strong> is not sufficient
									for this transaction. Please choose another payment option.
								</p>
							</div>

							<div class="enabledWallet"
								style="display: ${enabledWalletDisplay}">
								<p>
									Woohoo! Pay using Credits. Your Credits are <strong>Rs.
										${walletBalance}</strong>.
								</p>
							</div>

							<c:choose>
								<c:when test="${isWalletPayEnabled}">
									<div>
										<div class="payment-button-main clearfix">
											<p>
												Total payable amount: <strong><span
													class="webRupee">Rs.</span><span
													class="total-amount-to-pay">${cart.totalCartPrice}</span></strong>
											</p>
											<input type="hidden" id="6-paymenttype"
												value="<%=PaymentConstants.PAYMENT_TYPE_WALLET%>" /> <input
												type="hidden" id="6-productId"
												value="<%=prodId%>" /> <input type="hidden"
												id="6-affiliateid"
												value="<%=FCConstants.DEFAULT_AFFILIATE_ID%>" /> <input
												type="hidden" id="6-lookupID" value="${lid}" />
											<input type="hidden" id="6-cartId"
												value="${cartId}" />
											<button type="button" class="button buttonThree rf"
												onclick="doWalletPayment(${pts.id}, '${opts.code}');">Make
												payment &rarr;</button>
										</div>
									</div>
								</c:when>
								<c:otherwise>
									<p>
										<strong>Currently ${walletDisplayName} is under
											maintenance. Please select another Payment Option.</strong>
									</p>
								</c:otherwise>
							</c:choose>

						</c:when>
						<c:otherwise>
							<p>
								<strong>Currently ${walletDisplayName} is under
									maintenance. Please select another Payment Option.</strong>
							</p>
						</c:otherwise>
					</c:choose>
				</c:when>
			</c:choose>
		</c:forEach>
	</c:forEach>
	<div id="paymentwaitPageid">
	</div>
</div>
<!-- /content -->

<script type="text/javascript">
function doWalletPayment(paymentTypeId, paymentTypeCode) {
    var formData = createPaymentSubmitData(paymentTypeId, paymentTypeCode);
    submitPaymentRequest(formData);
}

function createPaymentSubmitData(paymenttypeid, paymentOptionValue) {
    var paymenttype = '#'+paymenttypeid+'-paymenttype';
    var productId = '#'+paymenttypeid+'-productId';
    var affiliateid = '#'+paymenttypeid+'-affiliateid';
    var lookupID = '#'+paymenttypeid+'-lookupID';
    var cartId = '#'+paymenttypeid+'-cartId';
    var couponCode = $('#couponCodeId').val();
    var productType = "<%=prodType%>";
var ccNum = $('#ccNum').val();
var ccHolderName = $('#ccHolderName').val();
var ccExpMon = $('#ccExpMon').val();
var ccExpYear = $('#ccExpYear').val();
var ccCvv = $('#ccCvv').val();
var dbNum = $('#dbNum').val();
var dbHolderName = $('#dbHolderName').val();
var dbExpMon = $('#dbExpMon').val();
var dbExpYear = $('#dbExpYear').val();
var dbCvv = $('#dbCvv').val();

    var formData = 'paymenttype='+$(paymenttype).val()+
            '&productId='+$(productId).val()+
            '&affiliateid='+$(affiliateid).val()+
            '&lookupID='+$(lookupID).val()+
            '&cartId='+$(cartId).val()+
            '&paymentoption='+paymentOptionValue+
            '&couponCode='+couponCode+
            '&productType='+productType+
            '&ccHolderName='+ccHolderName+
            '&ccNum='+ccNum+
            '&ccExpMon='+ccExpMon+
            '&ccExpYear='+ccExpYear+
            '&ccCvv='+ccCvv+
            '&dbHolderName='+dbHolderName+
            '&dbNum='+dbNum+
            '&dbExpMon='+dbExpMon+
            '&dbExpYear='+dbExpYear+
            '&dbCvv='+dbCvv;

    return formData;
}


function submitPaymentRequest(formData) {
    var url = '<%=com.freecharge.mobile.web.controller.MobileProductPaymentController.DO_PAYMENT_ACTION%>';
    $.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: "html",
        cache: false,
        timeout:1800000,
        success: function (responseText) {
            var code = responseText.charAt(0);
            if(code == 1){
                responseText = responseText.substring(1);
                $(".mainContainer").html(responseText);
                $.unblockUI();
            }else{
                $("#paymentwaitPageid").html(responseText);
                var isloggedIn = $("#isloggedin").val();
                var hostprefix = $("#hostPrefix").val();
                /* if(isloggedIn != 'true'){
                    alert("Your session is expired,Please login again.");
                    window.location.href = hostprefix;
                    return false;
                }  */
                document.paymentRequest.submit();
            }
        },
        error:function (e){
            $.unblockUI();
            alert("Technical error occured. Please try after some time or contact our customer care.");
        }
    });
}
</script>
