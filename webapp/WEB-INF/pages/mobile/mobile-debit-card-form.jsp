<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<p class="helper-text">For <strong>Maestro cards</strong>, expiry date and CVV are optional fields. ( Enter these values only if its present on card )</p>


<form:form commandName="checkoutData"
        action="/m/payment/request/do-debit-card-pay" method="POST"
        name="debitCardForm" id="new-debit-card-form" autocomplete="off" class="form">
        <div class="field-unit" >
            <label for="paymentOptions.debitCard.cardType"><spring:message code="m.debitcard.type" text="#m.debitcard.type#"/></label>
        <form:select path="paymentOptions.debitCard.cardType" class="paymentOptionsdebitCardcardType" cssClass="card-details-select">
            <form:option value=""><spring:message code="m.select.card.type" text="#m.select.card.type#"/></form:option>
            <form:options items="${checkoutData.paymentOptions.debitCard.cardTypeList}"  itemLabel="name" itemValue="code" />
        </form:select>
        <form:errors path="paymentOptions.debitCard.cardType" cssClass="error" element="div"></form:errors>
        </div>

        <div class="field-unit">
            <label for="paymentOptions.debitCard.cardNumber"><spring:message code="m.debit.card.number" text="#m.debit.card.number#"/></label>
            <form:input path="paymentOptions.debitCard.cardNumber" type="tel" cssClass="input-field" maxlength="19"/>
            <form:errors  cssClass="error" element="div" path="paymentOptions.debitCard.cardNumber"></form:errors>
        </div>

        <div class="field-unit" id ="dcExpiryDiv">
            <label><spring:message code="m.expiry.label" text="#m.expiry.label#"/></label>
            <form:select path="paymentOptions.debitCard.expiryMonth" id='debit-card-expiry-month' cssClass="card-details-select">
                    <form:option value=""><spring:message code="m.expiry.month" text="#m.expiry.month#"/></form:option>
                    <form:option value="01">01</form:option>
                    <form:option value="02">02</form:option>
                    <form:option value="03">03</form:option>
                    <form:option value="04">04</form:option>
                    <form:option value="05">05</form:option>
                    <form:option value="06">06</form:option>
                    <form:option value="07">07</form:option>
                    <form:option value="08">08</form:option>
                    <form:option value="09">09</form:option>
                    <form:option value="10">10</form:option>
                    <form:option value="11">11</form:option>
                    <form:option value="12">12</form:option>
            </form:select>
            <form:select path="paymentOptions.debitCard.expiryYear" id='debit-card-expiry-year' cssClass="card-details-select">
                    <form:option value=""><spring:message code="m.expiry.year" text="#m.expiry.year#"/></form:option>
                    <form:options items="${checkoutData.paymentOptions.debitCard.yearList}" />
            </form:select>
            <form:errors cssClass="error" element="div" path="paymentOptions.debitCard.expiryMonth"></form:errors>
            <form:errors cssClass="error" element="div" path="paymentOptions.debitCard.expiryYear"></form:errors>
        </div>

        <div class="field-unit">
                <label for="paymentOptions.debitCard.name"><spring:message code="m.name.on.card" text="#m.name.on.card#"/></label>
                <form:input path="paymentOptions.debitCard.name" cssClass="input-field"/>
                <form:errors cssClass="error" element="div" path="paymentOptions.debitCard.name"></form:errors>
        </div>


        <div class="field-unit" id="dcCvvDiv">
                <label for="paymentOptions.debitCard.cvvNumber"><spring:message code="m.cvv.number" text="#m.cvv.number#"/></label>
                <form:password path="paymentOptions.debitCard.cvvNumber" maxlength="3" autocomplete="off" cssClass="input-field input-field-small cvv-field" />
                <p class="error" style="display: none"></p>
                <form:errors cssClass="error" element="div" path="paymentOptions.debitCard.cvvNumber"></form:errors>
        </div>

        <div class="field-unit field-unit-checkbox save-card">
            <form:checkbox path="paymentOptions.cardStorage.saveCardSelected" id="save-debit-card" />
            <label for="save-debit-card">Remember this card</label>
            <p class="helper-text">(Note: we do not save or store your CVV2/CVC2 number)</p>
        </div>
        <div class="field-unit">
            <label>Enter a name for this card</label>
            <form:input path="paymentOptions.cardStorage.saveCardName" cssClass="input-field" />
        </div>

        <form:hidden path="lookupId" />
        <form:hidden path="totalAmount" />
        <form:hidden path="productType" />
        <form:hidden path="paymentOptions.usePartialPayment" id="debitCardPartialPayment"/>
        <button type="submit" class="btn btn-primary full-width card-submit card-submit-debit paymentTypeOption" data-type="Debit">
            <spring:message code="m.pay.label" text="#m.pay.label#"/>
        </button>
</form:form>
