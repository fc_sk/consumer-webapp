<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<form:form commandName="checkoutData"
	action="/m/payment/request/do-credit-card-pay" method="POST"
	name="creditCardForm" id="creditCardForm" autocomplete="off" class="form">
	<div class="field-unit">
		<label for="paymentOptions.creditCard.cardNumber"><spring:message code="m.credit.card.number" text="#m.credit.card.number#"/></label>
		<form:input path="paymentOptions.creditCard.cardNumber" type="tel" cssClass="input-field" maxlength="19" />
		<form:errors cssClass="error" element="div" path="paymentOptions.creditCard.cardNumber"></form:errors>
	</div>
	<div class="field-unit">
		<label><spring:message code="m.expiry.label" text="#m.expiry.label#"/></label>
        <form:select path="paymentOptions.creditCard.expiryMonth" cssClass="card-details-select" >
            <form:option value=""><spring:message code="m.expiry.month" text="#m.expiry.month#"/></form:option>
            <form:option value="01">01</form:option>
            <form:option value="02">02</form:option>
            <form:option value="03">03</form:option>
            <form:option value="04">04</form:option>
            <form:option value="05">05</form:option>
            <form:option value="06">06</form:option>
            <form:option value="07">07</form:option>
            <form:option value="08">08</form:option>
            <form:option value="09">09</form:option>
            <form:option value="10">10</form:option>
            <form:option value="11">11</form:option>
            <form:option value="12">12</form:option>
        </form:select>
        <form:select path="paymentOptions.creditCard.expiryYear" cssClass="card-details-select">
            <form:option value=""><spring:message code="m.expiry.year" text="#m.expiry.year#"/></form:option>
            <form:options items="${checkoutData.paymentOptions.creditCard.yearList}" />
        </form:select>
		<form:errors cssClass="error" element="div" path="paymentOptions.creditCard.expiryMonth"></form:errors>
		<form:errors cssClass="error" element="div" path="paymentOptions.creditCard.expiryYear"></form:errors>
	</div>
	<div class="field-unit">
		<label for="paymentOptions.creditCard.name"><spring:message code="m.name.on.card" text="#m.name.on.card#"/></label>
		<form:input path="paymentOptions.creditCard.name" cssClass="input-field" />
		<form:errors cssClass="error" element="div" path="paymentOptions.creditCard.name"></form:errors>
	</div>
	<div class="field-unit">
		<label for="paymentOptions.creditCard.cvvNumber"><spring:message code="m.cvv.number" text="#m.cvv.number#"/></label>
		<form:password path="paymentOptions.creditCard.cvvNumber"  maxlength="3"  autocomplete="off" cssClass="input-field input-field-small cvv-field"/>
        <p class="error" style="display: none"></p>
		<form:errors cssClass="error" element="div" path="paymentOptions.creditCard.cvvNumber"></form:errors>
	</div>
	<form:hidden path="lookupId" />
	<form:hidden path="totalAmount" />
	<form:hidden path="productType" />
	<form:hidden path="paymentOptions.usePartialPayment" id="crediCardPartialPayment"/>
	<div class="field-unit field-unit-checkbox save-card">
        <form:checkbox path="paymentOptions.cardStorage.saveCardSelected" id="save-credit-card"/>
        <label for="save-credit-card">Remember this card</label>
        <p class="helper-text">(Note: we do not save or store your CVV2/CVC2 number)</p>
    </div>
    <div class="field-unit">
        <label>Enter a name for this card</label>
        <form:input path="paymentOptions.cardStorage.saveCardName" cssClass="input-field" />
    </div>
    <button type="submit" class="btn btn-primary full-width card-submit paymentTypeOption" data-type="Credit">
        <spring:message code="m.pay.label" text="#m.pay.label#"/>
    </button>                

</form:form>                                                    
