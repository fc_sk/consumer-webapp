<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="auth">
    <h2 class="page-title">Sign Up</h2>

    <div class="form-wrap">
    <form:form commandName="user" action="/m/adduser" method="POST" name="userForm" id="userForm" class="form" autocomplete="off">
   	    <form:errors path="" cssClass="error" element="div"></form:errors>
        <div class="field-unit">
            <label for="title"><spring:message code="m.title.label" text="#m.title.label#"/></label>
            <form:select path="title" cssClass="input-field">
                <option value="">
                    <spring:message code="m.select.title" text="#m.select.title#"/> 
                </option>
                <form:option value="Mr">
                    <spring:message code="m.mr.label" text="#m.mr.label#"/>.
                </form:option>
                <form:option value="Ms">
                    <spring:message code="m.ms.label" text="#m.ms.label#"/>.
                </form:option>
                <form:option value="Mrs">
                   <spring:message code="m.mrs.label" text="#m.mrs.label#"/>.
                </form:option>
            </form:select>
            <form:errors path="title" element="div" cssClass="error"></form:errors>
        </div>
        <div class="field-unit">
            <label for="firstName"><spring:message code="m.fullname.label" text="#m.fullname.label#"/></label>
            <form:input path="firstName" cssClass="input-field" />
            <form:errors path="firstName" cssClass="error" element="div"></form:errors>
        </div>
        <div class="field-unit">
            <label for="mobileNo"><spring:message code="m.mobile.no.label" text="#m.mobile.no.label#"/></label>
            <form:input path="mobileNo" type="tel" cssClass="input-field" maxlength="10" />
            <p class="error" style="display: none"></p>
            <form:errors path="mobileNo" cssClass="error" element="div"></form:errors>
        </div>
        <div class="field-unit">
            <label for="email"><spring:message code="m.email.label" text="#m.email.label#"/></label>
            <form:input path="email" type="email" cssClass="input-field"/>
            <form:errors path="email" cssClass="error" element="div"></form:errors>
        </div>
        <div class="field-unit">
            <label for="password"><spring:message code="m.password.label" text="#m.password.label#"/></label>
            <form:password path="password" cssClass="input-field" maxlength="20" />
            <form:errors path="password" cssClass="error" element="div"></form:errors>
        </div>
        <div class="field-unit">
            <label for="confirmPassword"><spring:message code="m.confirm.password.label" text="#m.confirm.password.label#"/></label>
            <form:password path="confirmPassword" cssClass="input-field" maxlength="20" />
            <form:errors path="confirmPassword" cssClass="error" element="div"></form:errors>
        </div>
        <div class="field-unit">
            <label for="address1"><spring:message code="m.address.label" text="#m.address.label#"/></label>
            <form:input path="address1" cssClass="input-field"/>
            <form:errors path="address1" cssClass="error" element="div"></form:errors>
        </div>
        <div class="field-unit">
            <label for="area"><spring:message code="m.area.label" text="#m.area.label#"/></label>
            <form:input path="area" cssClass="input-field" maxlength="50" />
            <form:errors path="area" cssClass="error" element="div"></form:errors>
        </div>
       
        <div class="field-unit">
            <label for="landmark"><spring:message code="m.landmark.label" text="#m.landmark.label#"/></label>
            <form:input path="landmark" cssClass="input-field" maxlength="50" />
            <form:errors path="landmark" cssClass="error" element="div"></form:errors>
        </div>
        <div class="field-unit">
        <label for="stateId"><spring:message code="m.state.label" text="#m.state.label#"/></label>
            <form:select path="stateId" cssClass="input-field">
            <option value="">
                    <spring:message code="m.select.state" text="#m.select.state#"/>
                </option>
            <form:options items="${user.masterData.stateMasterList}" itemLabel="stateName" itemValue="stateMasterId" />
            </form:select>
            <form:errors path="stateId" cssClass="error" element="div"></form:errors>
        </div>
        <div class="field-unit">
        <label for="cityId"><spring:message code="m.city.label" text="#m.city.label#"/></label>
            <form:select path="cityId" cssClass="input-field">
                <option value="">
                    <spring:message code="m.select.city" text="#m.select.city#"/>
                </option>
                <form:options items="${user.cityList}" itemLabel="cityName" itemValue="cityMasterId" />
            </form:select>
            <form:errors path="cityId" cssClass="error" element="div"></form:errors>
        </div>
        
        <div class="field-unit">
            <label for="postalCode"><spring:message code="m.pincode.label" text="#m.pincode.label#"/></label>
            <form:input path="postalCode" type="tel" cssClass="input-field" maxlength="6" />
            <form:errors path="postalCode" cssClass="error" element="div"></form:errors>
        </div>

		<form:hidden path="signupFrom" />
		<c:forEach items="${user.fromParamMap}" var="entry">
			<form:hidden path="fromParamMap[${entry.key}]" />
		</c:forEach>
		<c:forEach items="${user.fromParamMap}" var="entry">
			<input type="hidden" name="<c:out value="${entry.key}"/>"
				value="<c:out value="${entry.value[0]}"/>" />
		</c:forEach>
			
        <div class="field-unit">
            <button id="register" type="submit" class="btn btn-primary full-width">
                <spring:message code="m.signup.label" text="#m.signup.label#"/>
            </button>
        </div>

    </form:form>


        <a href="/m/login" class="btn block"><spring:message code="m.login.label" text="#m.login.label#"/></a>
    </div>

</div>
