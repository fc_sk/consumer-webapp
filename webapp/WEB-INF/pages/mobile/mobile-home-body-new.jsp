<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set  value="${param.ext}" var="ext" />
<c:set  value="${param.extName}" var="extName" />

<c:set var="rememberLabel" scope="request">
	<spring:message code="m.remember.label" text="#m.remember.label#" />
</c:set>

<%
    Integer[] sunDirectDenominationList = { 5, 12, 15, 20, 32, 35, 65, 100, 155, 185, 190, 199, 200, 210, 220,
            260, 270, 300, 310, 400, 440, 500, 525, 560, 600, 740, 845, 850, 880, 950, 999, 1001, 1100, 1150,
            1210, 1270, 1430, 1600, 1650, 1800, 1900, 2050, 2450, 3200, 4700 };
    request.setAttribute("sunDirectDenominationList", sunDirectDenominationList);
%>

<div id="new">
<div class="home">

	<c:choose>
		<c:when test="${stripType == 'did4'}">
			<a class="pepsi-banner" style="background-color: #0056a7;"
				href="/m/didpromotion" target="_blank"
				title="Dance India Dance free talktime"><img
				src="${mt:keyValue("imgprefix1")}/images/m/campaigns/DID4_2.png" alt="Dance India Dance free talktime"></a>
		</c:when>
		<c:when test="${stripType == 'mcd'}">
			<a class="pepsi-banner" style="background-color: #87171c;"
				href="/m/mcdpromotion" target="_blank"
				title="McDonalds gift festival">
				<img src="${mt:keyValue("imgprefix1")}/images/m/campaigns/mcd.png" alt="McDonalds gift festival"></a>
		</c:when>
		<c:when test="${stripType == 'amazon'}">
			<a class="pepsi-banner" style="background-color: #0056a7;"
				href="https://d32vr05tkg9faf.cloudfront.net/content/campaigns/Amazon/m/index.html" target="_blank"
				title="Amazon gift festival">
				<img src="https://d32vr05tkg9faf.cloudfront.net/content/campaigns/Amazon/m/images/Amazon.png" alt="Amazon gift festival"></a>
		</c:when>
		<c:when test="${ext == '1' && extName != ''}">
			<a class="pepsi-banner" style="background-color: #0056a7;"
				href="https://d32vr05tkg9faf.cloudfront.net/content/campaigns/${extName}/m/index.html" target="_blank"
				title="${extName}">
				<img src="https://d32vr05tkg9faf.cloudfront.net/content/campaigns/${extName}/m/images/${extName}.png" title="${extName}"></a>
		</c:when>
		<c:otherwise>
	        <a class="pepsi-banner" style="background-color: #10263B" href="https://play.google.com/store/apps/details?id=com.freecharge.android&referrer=utm_source%3Dmobilesite%26utm_medium%3Dbanner%26utm_campaign%3Dmobilesitetoandroid" target="_blank" title="Install the FreeCharge android app now" ><img src="${mt:keyValue("imgprefix1")}/images/m/campaigns/androidapp.png" alt="Install the FreeCharge android app now" />
		</c:otherwise>
	</c:choose>

	<div class="menu">
		<ul id="product-type">
			<li id="product-type-prepaid-mobile" class="option-selected boxmodel">
				<spring:message code="m.mobile.label" text="#m.mobile.label#" />
			</li>
			<li id="product-type-dth" class="option boxmodel"><spring:message
					code="m.dth.label" text="#m.dth.label#" /></li>
			<li id="product-type-datacard" class="option boxmodel"><spring:message
					code="m.datacard.label" text="#m.datacard.label#" /></li>
		</ul>
	</div>

</div>


<div id="prepaid-mobile-recharge" class="recharge">

	<form:form commandName="mobileRecharge" action="/m/do-mobile-recharge"
		method="POST" name="mobileForm" id="mobileForm" class="form">

		<div class="pagehead">The fastest way to recharge your prepaid mobile</div>

		<div class="form-wrap">
			<div id="mobileNumberWrap" class="field-unit">
				<label for="mobileNumber"><spring:message
						code="m.mobile.number" text="#m.mobile.number#" /></label>
				<form:input path="mobileNumber" type="tel"
					placeholder="Enter mobile number" maxlength="10"
					cssClass="input-field" />
				<p class="error" style="display: none"></p>
				<form:errors path="mobileNumber" element="div" cssClass="error"></form:errors>
			</div>

			<div id="mobileOperatorWrap" class="field-unit">
				<%-- <label for="operator"><spring:message
						code="m.mobile.operator" text="#m.mobile.operator#" /></label> --%>
				<form:select path="operator" cssClass="dropbox">
					<form:option value="">
						<spring:message code="m.select.operator"
							text="#m.select.operator#" />
					</form:option>
					<form:options items="${mobileRecharge.masterData.mobileOperators}"
						itemLabel="name" itemValue="operatorMasterId" />
				</form:select>
				<p class="error" style="display: none"></p>
				<form:errors path="operator" element="div" cssClass="error"></form:errors>
			</div>

			<div id="mobileCircleWrap" class="field-unit" style="display: none;">
				<label for="circle"><spring:message code="m.mobile.circle"
						text="#m.mobile.circle#" /></label>
				<form:select path="circle" cssClass="dropbox">
					<form:option value="">
						<spring:message code="m.select.circle" text="#m.select.circle#" />
					</form:option>
					<form:options items="${mobileRecharge.masterData.mobileCircles}"
						itemLabel="name" itemValue="circleId" />
				</form:select>
				<p class="error" style="display: none"></p>
				<form:errors path="circle" element="div" cssClass="error"></form:errors>
			</div>

			<div id="mobileAmountWrap" class="field-unit field-unit-stroke">
				<label for="amount"><spring:message
						code="m.mobile.recharge.amount" text="#m.mobile.recharge.amount#" /></label>
				<form:input path="amount" type="tel" placeholder="Recharge amount"
					cssClass="input-field" maxlength="4" />
				<p class="error" style="display: none"></p>
				<form:errors path="amount" element="div" cssClass="error"></form:errors>
			</div>
		</div>
		<div class="field-unit-button">
			<button id="mobile-recharge-submit" type="submit"
				class="btn btn-primary full-width">
				<spring:message code="m.continue.label" text="#m.continue.label#" />
			</button>
		</div>
	</form:form>
</div>

<div id="dth-recharge" class="recharge" style="display: none;">

	<form:form commandName="dthRecharge" action="/m/do-dth-recharge"
		method="POST" name="dthForm" id="dthForm" class="form"
		data-ajax="false">

		<div class="pagehead">The fastest way to recharge your DTH TV connection</div>

		<div class="form-wrap">

			<div class="field-unit" id="dthOperatorWrap">
				<label for="dthOperator"><spring:message
						code="m.dth.operator" text="#m.dth.operator#" /></label>
				<form:select path="dthOperator" cssClass="dropbox">
					<form:option value="">
						<spring:message code="m.select.operator"
							text="#m.select.operator#" />
					</form:option>
					<form:options items="${dthRecharge.masterData.dthOperators}"
						itemLabel="name" itemValue="operatorMasterId" />
				</form:select>
				<p class="error" style="display: none"></p>
				<form:errors path="dthOperator" element="div" cssClass="error"></form:errors>
			</div>

			<div class="helper-text" style="display: none"></div>

			<div class="field-unit" id="dthNumberWrap">
				<label for="dthNumber" id="dthNumberLabel"><spring:message
						code="m.dth.subscriber.number" text="m.#dth.subscriber.number#" /></label>
				<form:input path="dthNumber" type="tel" maxlength="14"
					cssClass="input-field" />
				<p class="error" style="display: none"></p>
				<form:errors path="dthNumber" element="div" cssClass="error"></form:errors>
			</div>

			<div class="field-unit field-unit-stroke" id="dthAmountWrap">
				<label for="dthAmount"><spring:message
						code="m.dth.recharge.amount" text="#m.dth.recharge.amount#" /></label>
				<form:input path="dthAmount" type="tel" maxlength="4"
					cssClass="input-field" />
				<select id="amount-dth-suntv" class="input-field">
					<c:forEach var="dthAmountItem" items="${sunDirectDenominationList}">
						<option value="${dthAmountItem}"
							<c:if test="${dthAmountItem eq fn:escapeXml(homeWebDo.dthAmount)}">selected='selected'</c:if>>${dthAmountItem}</option>
					</c:forEach>
				</select>
				<p class="error" style="display: none"></p>
				<form:errors path="dthAmount" element="div" cssClass="error"></form:errors>
			</div>
		</div>

		<div class="field-unit-button">
			<button id="dth-recharge-submit" type="submit"
				class="btn btn-primary full-width">
				<spring:message code="m.continue.label" text="#m.continue.label#" />
			</button>
		</div>
	</form:form>
</div>

<div id="datacard-recharge" class="recharge" style="display: none;">

	<form:form commandName="dataCardRecharge"
		action="/m/do-datacard-recharge" method="POST" name="datacardForm"
		id="datacardForm" class="form" data-ajax="false">

		<div class="pagehead">The fastest way to recharge your internet Data Card</div>

		<div class="form-wrap">
			<div class="field-unit" id="datacardNumberWrap">
				<label for="datacardNumber"><spring:message
						code="m.datacard.number" text="#m.datacard.number#" /></label>
				<form:input path="datacardNumber" placeholder="Data card number"
					type="tel" maxlength="10" cssClass="input-field" />
				<p class="error" style="display: none"></p>
				<form:errors path="datacardNumber" element="div" cssClass="error"></form:errors>
			</div>

			<div class="field-unit" id="datacardOperatorWrap">
				<%-- <label for="datacardOperator"><spring:message
						code="m.datacard.operator" text="#m.datacard.operator#" /></label> --%>
				<form:select path="datacardOperator" cssClass="dropbox">
					<form:option value="">
						<spring:message code="m.select.operator"
							text="#m.select.operator#" />
					</form:option>
					<form:options
						items="${dataCardRecharge.masterData.dataCardOperators}"
						itemLabel="name" itemValue="operatorMasterId" />
				</form:select>
				<p class="error" style="display: none"></p>
				<form:errors path="datacardOperator" element="div" cssClass="error"></form:errors>
			</div>

			<div class="field-unit field-unit-crippled circle-wrap"
				id="datacardCircleWrap" style="display: none;">
				<label for="datacardCircle"><spring:message
						code="m.datacard.circle" text="#m.datacard.circle#" /></label>
				<form:select path="datacardCircle" cssClass="dropbox">
					<form:option value="">
						<spring:message code="m.select.circle" text="#m.select.circle#" />
					</form:option>
					<form:options items="${dataCardRecharge.masterData.mobileCircles}"
						itemLabel="name" itemValue="circleId" />
				</form:select>
				<p class="error" style="display: none"></p>
				<form:errors path="datacardCircle" element="div" cssClass="error"></form:errors>
			</div>

			<div class="field-unit field-unit-stroke" id="datacardAmountWrap">
				<label for="datacardAmount"><spring:message
						code="m.datacard.recharge.amount"
						text="#m.datacard.recharge.amount#" /></label>
				<form:input path="datacardAmount" placeholder="Recharge amount"
					type="tel" maxlength="4" cssClass="input-field" />
				<p class="error" style="display: none"></p>
				<form:errors path="datacardAmount" element="div" cssClass="error"></form:errors>
			</div>
		</div>
		<div class="field-unit-button">
			<button id="datacard-recharge-submit"
				class="btn btn-primary full-width">
				<spring:message code="m.continue.label" text="#m.continue.label#" />
			</button>
		</div>
	</form:form>
</div>

<%--scripts - to be loaded after content--%>
<script src="${mt:keyValue("jsprefix1")}/mobile/mobile.recharge.js?v=${mt:keyValue("version.no")}"></script>
</div>
