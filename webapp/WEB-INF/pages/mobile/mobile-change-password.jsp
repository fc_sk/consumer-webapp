<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="account">
    <h2 class="page-title">Change password</h2>
    <div class="form-wrap">
        <c:choose>
            <c:when test="${updpaswstatus}">
                <div class="message-success mobile-dialog" id="passwordupmsg">
                    <p class='mobile-dialog-text'><c:out value="${message}"/></p>
                </div>
            </c:when>
            <c:otherwise>
                <div class="message-failure mobile-dialog" id="passworderrormsg">
                    <p class='mobile-dialog-text'><c:out value="${errmessage}"/></p>
                </div>
            </c:otherwise>
        </c:choose>

        <form:form action="/m/mobile-editpassword" method="post" name="passwordupdateform" id="passwordupdateform" modelAttribute="editPassword" cssClass="form">
            <div class="field-unit first-field" >
                <label><spring:message code="m.old.password" text="#m.old.password#"/></label>
                <spring:bind path="editPassword.oldpassword">
                    <form:input path="oldpassword" type="password" maxlength="20" cssClass="input-field" />
                </spring:bind>
                <p class="error" style="display: none"></p>
                <form:errors path="oldpassword" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit" >
                <label><spring:message code="new.password" text="#new.password#"/></label>
                <spring:bind path="editPassword.newpassword">
                    <form:input path="newpassword" type="password" maxlength="20" cssClass="input-field" />
                </spring:bind>
                <p class="error" style="display: none"></p>
                <form:errors path="newpassword" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit" >
                <label><spring:message code="confirm.password" text="#confirm.password#"/></label>
                <spring:bind path="editPassword.confirmpassword">
                    <form:input path="confirmpassword" type="password" maxlength="20" cssClass="input-field" />
                </spring:bind>
                <p class="error" style="display: none"></p>
                <form:errors path="confirmpassword" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit">
                <button id="mobile-password-change" class="btn btn-primary full-width"><spring:message code="m.svae.changes" text="#m.svae.changes#"/></button>
            </div>
        </form:form>
    </div>
</div>
	

   

