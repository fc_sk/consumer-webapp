<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="mobile-common-includes.jsp"/>
</head>
<body>
	<tiles:insertAttribute name="header" />
    <tiles:insertAttribute name="body" />
    <tiles:insertAttribute name="footer" />

    <%--scripts - to be loaded after content--%>
    <jsp:include page="mobile-bottom-scripts.jsp"/>
</body>
</html>
