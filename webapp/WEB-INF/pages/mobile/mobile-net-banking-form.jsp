<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<form:form commandName="checkoutData"
	action="/m/payment/request/do-net-banking-pay" method="POST"
	name="netBankingForm" id="netBankingForm" class="form">
    <div class="field-unit">
        <form:select path="paymentOptions.netBanking.bankCode" cssClass="input-field input-field-select-small">
            <form:option value=""><spring:message code="m.select.bank" text="#m.select.bank#"/></form:option>
            <form:options
                    items="${checkoutData.paymentOptions.netBanking.bankList}"
                    itemLabel="bankName" itemValue="bankCode" />
        </form:select>
        <form:errors element="div" cssClass="error" path="paymentOptions.netBanking.bankCode"></form:errors>
    </div>

	<form:hidden path="productType" />
	<form:hidden path="lookupId" />
	<form:hidden path="totalAmount" />
	<form:hidden path="paymentOptions.usePartialPayment" id="netBankingPartialPayment"/>
    <button type="submit" class="btn btn-primary full-width paymentTypeOption" data-type="Net Banking">
        <spring:message code="m.pay.label" text="#m.pay.label#"/>
    </button>
</form:form>
