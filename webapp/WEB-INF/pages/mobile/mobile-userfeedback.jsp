<!doctype html>
<html lang="en">
<head>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200' rel='stylesheet' type='text/css'>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=100%, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
    <title>Feedback</title>
    <link href="https://play.google.com/store/apps/details?id=com.freecharge.android" />
    <style>
        html {
            font-size: 18px;
        }
        header {
            padding: 10px;
            text-align: center;
            margin-top: 1em;
        }
        body {
            -webkit-user-select: none;  /* Chrome all / Safari all */
            -moz-user-select: none;     /* Firefox all */
            -ms-user-select: none;      /* IE 10+ */
            -o-user-select: none;
            user-select: none;
            background-color: #356BC1;
            font-family: 'Source Sans Pro',​sans-serif;
            font-size: 1em;
            line-height: 1.4;
            margin: 0;
            color: #FFFFFF;
        }
        #content {
            width: 82%;
            margin: 0 auto;
            text-align: center;
            position: relative;
        }
        
        .floatleft{
            float:left;
        }
        [class^="smiley-"]{
            width: 3em;
            height: 3em;
            border-radius:50%;
            margin: 0 auto;
        }
        .smiley-green{
            background:#a9db7a;
            border:5px solid #92c563;
            position: relative;
            display: none;
        }
        .smiley-green .left-eye{
            width:18%;
            height:18%;
            background:#84b458;
            position:relative;
            top:29%;
            left:22%;
            border-radius:50%;
            float: left;
        }
        .smiley-green .right-eye{
            width:18%;
            height:18%;
            border-radius:50%;
            position:relative;
            background:#84b458;
            top: 29%;
            right: 22%;
            float: right;
        }
        .smiley-green .smile{
            position: absolute;
            top:67%;
            left:16.5%;
            width:70%;
            height:20%;
            overflow: hidden;
        }
         .smiley-green .smile:after, .smiley-green .smile:before{
            content:"";
            position:absolute;
            top:-50%;
            left:0%;
            border-radius:50%;
            background:#84b458;
            height:100%;
            width:97%;
         }
         .smiley-green .smile:after{
            background: #84b458;
            height: 80%;
            top: -40%;
            left: 0%;
        }

        .smiley-red{
            background:#ee9295;
            border:5px solid #e27378;
            position: relative;
            display: none;
        }
        .smiley-red .left-eye{
            width:18%;
            height:18%;
            background:#d96065;
            position:relative;
            top:29%;
            left:22%;
            border-radius:50%;
            float: left;
        }
        .smiley-red .right-eye{
            width:18%;
            height:18%;
            border-radius:50%;
            position:relative;
            background:#d96065;
            top: 29%;
            right: 22%;
            float: right;
        }
        .smiley-red .smile{
            position: absolute;
            top:57%;
            left:16.5%;
            width:70%;
            height:20%;
            overflow: hidden;
        }
         .smiley-red .smile:after, .smiley-red .smile:before{
            content:"";
            position:absolute;
            top:50%;
            left:0%;
            border-radius:50%;
            background:#d96065;
            height:100%;
            width:97%;
         }
         .smiley-red .smile:after{
            background: #d96065;
            height: 80%;
            top: 60%;
            left: 0%;
        }
        .emotion {
            width: 40%;
            overflow: hidden;
            display: inline-block;
        }
        .redirect-btn {
            color: #FFFFFF;
            text-decoration: none;
            font-size: 1.2em;
            font-weight: bold;
            border: 1px solid #FFFFFF;
            display: block;
            padding: 10px;
            margin-top: 1em;
        }
        .cta-wrapper {
            transition: left 0.5s;
            -webkit-transition: left 0.5s;
            position: absolute;
            left: -1000px;
            width: 100%;
        }
        #feedback-wrapper {
            transition: top 0.5s;
            -webkit-transition: top 0.5s;
            top: 0;
        }
        #happy {
            margin-right: 1em;
        }
        .smiley-fallback {
            background-image: url('../../content/images/m/smileys.png');
            background-color: transparent;
            background-repeat: no-repeat;
            width: 68px;
            height: 68px;
            display: none;
        }
        .smiley-fallback.happy {
            background-position: 0 0;
        }
        .smiley-fallback.sad {
            background-position: -71px 0;
        }
    </style>
    <script type="text/javascript">

        function userActionCB(response) {
            var feedbackWrapper = document.getElementById('feedback-wrapper');
            if(response === 'happy') {
                feedbackWrapper.style.position = 'absolute';
                feedbackWrapper.style.top = '-1000px';
                document.getElementById('happy-msg').style.left = '0';
            } else {
                feedbackWrapper.style.position = 'absolute';
                feedbackWrapper.style.top = '-1000px';
                document.getElementById('sad-msg').style.left = '0';
            }
        }

        function track(eventName) {
            var httpReq;
            
            if(window.XMLHttpRequest) {
                httpReq = new XMLHttpRequest();
                httpReq.open('GET', $('#freechargeApp').attr('data-trackerVisitApi'));
                httpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');    
                httpReq.send('eventName=' + eventName);
            }
        }

        function loadSmileys() {
            var IS_CSS_BORDER_RADIUS_SUPPORTED = (function() {
                var docEl = document.documentElement, s;
                    if (docEl && (s = docEl.style)) {
                        return typeof s.borderRadius == "string"
                    }
                return null;
            })();

            var smileyDefault = document.getElementsByClassName('smiley-default'),
                smileyFallback = document.getElementsByClassName('smiley-fallback');

            if(IS_CSS_BORDER_RADIUS_SUPPORTED) {
                smileyDefault[0].style.display = 'block';
                smileyDefault[1].style.display = 'block';
            }else {
                smileyFallback[0].style.display = 'inline-block';
                smileyFallback[1].style.display = 'inline-block';
            }
        }

    </script>
</head>
<body onload="loadSmileys();">
    <!-- Header -->
    <header>
    <!-- Logo -->
        <img src="../../content/images/m/logo-large.png" alt="freecharge.in">
    </header>
        
    <!-- Content -->
    <div id="content">
        <div class="cta-wrapper" id="happy-msg">
            <p>That's great news! <br> Rate us on the play store and spread the word</p>
            <a id="link-to-play-store" class="redirect-btn" href="https://play.google.com/store/apps/details?id=com.freecharge.android" target="_blank" onclick="track('rateOnPlayStore')">Take me to play store</a>
        </div>

        <div class="cta-wrapper" id="sad-msg">
            <p>We're sorry about that. Please take a moment to send us your feedback. We assure you a better experience in the future!</p>
            <a id="send-feedback-link" class="redirect-btn" href="mailto:mobile@freecharge.com" target="_blank" onclick="track('sendFeedback')">Send Feedback</a>
        </div>

        <div id="feedback-wrapper">
            <h3>Rate your FreeCharge app experience</h3>
            <div id="smiley-wrapper">
                <div id="happy" class="emotion" onclick="userActionCB('happy');">
                    <div class='smiley-green smiley-default'>
                        <div class='left-eye'></div>
                        <div class='right-eye'></div>
                        <div class='smile'></div>
                    </div>
                    <span class="smiley-fallback happy"></span>
                    <p>That was great!</p>      
                </div>
              
              <div id="sad" class="emotion" onclick="userActionCB('sad');">
                  <div class='smiley-red smiley-default'>
                      <div class='left-eye'></div>
                      <div class='right-eye'></div>
                      <div class='smile'></div>
                  </div>
                  <span class="smiley-fallback sad"></span>
                  <p>Nah! Not really</p>
              </div>
            </div>
        </div>

    </div>    
    
    <script type="text/javascript">
        window.___gcfg = {
            lang: 'en-US',
            parsetags: 'onload'
        };
        (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
</script>
</body>
</html>