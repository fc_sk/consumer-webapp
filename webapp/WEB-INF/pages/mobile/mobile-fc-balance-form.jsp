<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<form:form commandName="checkoutData"
	action="/m/payment/request/do-fc-balance-pay" method="POST"
	name="debitCardForm" id="debitCardForm">
	<form:hidden path="lookupId" />
	<form:hidden path="totalAmount" />
	<form:hidden path="productType" />
	<c:if test="${checkoutData.paymentOptions.wallet.balance.amount >= checkoutData.totalAmount}">

        <p class="helper-text">You have enough Credits to pay for this transaction. You can choose to skip Credits and use another payment option.</p>

        <button type="submit" id="fcPayButton" class="btn btn-primary full-width" >PAY</button>
	</c:if>
	<c:if test="${checkoutData.paymentOptions.wallet.balance.amount < checkoutData.totalAmount}">
		<spring:message code="m.sorry.no.fc.balance" text="#m.sorry.no.fc.balance#"/>.
	</c:if>
</form:form>
