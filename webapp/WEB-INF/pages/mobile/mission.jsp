<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>

<!DOCTYPE html>
<html>
<head>

    <title>Freecharge Missions</title>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
	
	<style>
		 
		body {
			 font-family: Lato,Arial,sans-serif;
			 font-size: 1rem;
			 color: #34495e;
			 margin: 0;
		}
		.app-install-holder{
			background-color: #FFFFFF;
			width: 100%;
		}
		div.app-install-holder img{
			width: 100%;
			margin: 0;
		}
		div.app-install-holder .image_child_two{
			width: 95%;
			margin: 0 2em;
		}
		div.instructions{
			font-size: 1rem;
		}
		div.instructions ul{
			font-size: 1rem;
			margin: 1.5em 1.5em 0 0;
		}
		div.instructions ul li{
			margin: 0.5rem 0;
		}
		div.instructions ul li span{
			position: relative;
		}
		div.instructions .p-instruction-header{
			text-align: center;
			margin: 0 1.2em;
			font-size: 1.4rem;
		}
		div.instructions .p-instruction-footer{
			margin: 0rem 2em 0.5em;
			position: relative;
			text-align: justify;
			left: 10px;
		}
		div.termsConditions{
			font-size: 1rem;
		}
		div.termsConditions .p-terms-headerone{
			text-align: center;
			font-size: 0.9rem;
			color: #FB9554;
		}
		div.termsConditions .p-terms-headertwo{
			margin: 1rem 0.4em;
			text-align: justify;
		}
		div.termsConditions ul{
			margin: 0 0.5em 0 0;
			font-size: 1rem;
			text-align: justify;
		}
		div.termsConditions ul li{
			margin: 0.5em 0;	
		}
		div.termsConditions ul li span{
			position: relative;
		}
		div.instructions .p-instruction-header .p_color_one{
			color: #FB9554;
		}
		div.instructions .p-instruction-header .p_color_two{
			color:#4D82CB
		}
		div.app-install-button .btn-install{
			color: #FFFFFF;
			background-color: #F39300;
			border-color: #F39300;
			text-transform: uppercase;
			width: 95%;
			margin: 0 0.4em;
			font-size: 1.5rem;
			text-align: center;
			line-height: 50px;
			font-family: inherit;
		}
		div.app-install-button .btn-color-one{
			background-color: #F39300;
			border-color: #F39300;
		}
	</style>
</head>
<body>
<div id="view-template"></div>
<%--template --%>
<script id="missionAppInstall" type="x-handlebars-template">
	<div class="app-install-holder">
		<img src="{{template.bannerImageUrl}}">
		<div class="instructions">
			<p class="p-instruction-header">
				Install the app and earn <span class="p_color_one">&#8377; {{rewardAmount}}</span> in <span class="p_color_two">Freecharge</span> credits
			</p>
			<ul>
				{{#each description}}
					<li>{{this}}</li>
				{{/each}}
			</ul>
			<p class="p-instruction-footer">
				&#8377; {{rewardAmount}} will be credited to your account in 3 days
			</p>
		</div>
		<div class="app-install-button">
			<button id="buttonToInstall" class="btn-install btn-color-one" onclick="submitActivity();">
				 {{btnText}}
			</button>
		</div>
		<div class="termsConditions">
			<p class="p-terms-headerone">TERMS AND CONDITIONS</p>
			<%--<p class="p-terms-headertwo">You shall receive $ {{rewardAmount}} worth of Freecharge credits on installing the {{title}} App via this notification within 3 days</p>--%>
				<ul>
					{{#each tnc}}
					<li><span>{{this}}</span></li>
					{{/each}}
				</ul>
		</div>
	</div>
	
	
</script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/lib/handlebars-v1.3.0.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript" >
var mission = ${mission};
    window.onload = function() {
        var source = document.getElementById('missionAppInstall').innerHTML;
        var template = Handlebars.compile(source);
        view = document.getElementById('view-template');
        view.innerHTML = template(mission);
    };
    
    function submitActivity(){
        var xhr = new XMLHttpRequest();
        xhr.addEventListener('readystatechange', onreadystatechangeHandler, false);
        xhr.open('POST', '/app/ardeal/action/promooptin/moptin', true);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

        var formData = new FormData();
        formData.append('uid', '${mission.uid}');
        formData.append('activity', 'step2');
        // Fire!
        xhr.send(formData);
    }

    function onreadystatechangeHandler(evt){
        var status = evt.target.status;
        if(evt.target.responseText){
            var res = JSON.parse(evt.target.responseText);
            //console.log(res);
            if(status == '200'){
                window.location.replace(res.redirectUrl);
            }
        }
    }
</script>
</body>
</html>