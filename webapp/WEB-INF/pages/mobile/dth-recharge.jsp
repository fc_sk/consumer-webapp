<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="rememberLabel" scope="request"><spring:message code="m.remember.label" text="#m.remember.label#"/></c:set>
<%
    Integer[] sunDirectDenominationList = {5, 12, 15, 20, 32, 35, 65, 100, 155, 185, 190, 199, 200, 210, 220, 260, 270, 300, 310, 400, 440, 500, 525, 560, 600, 740, 845, 850, 880, 950, 999, 1001, 1100, 1150, 1210, 1270, 1430, 1600, 1650, 1800, 1900, 2050, 2450, 3200, 4700};
    request.setAttribute("sunDirectDenominationList", sunDirectDenominationList);
%>

<div class="recharge" data-product="DTH">
    <h2 class="page-title"><span>01</span><spring:message code="m.dth.recharge.label" text="#m.dth.recharge.label#"/></h2>

    <form:form commandName="dthRecharge" action="/m/do-dth-recharge" method="POST" name="dthForm" id="dthForm" class="form"  data-ajax="false">

    <div class="form-wrap">

        <div class="field-unit field-unit-crippled" id="dthOperatorWrap">
            <label for="dthOperator"><spring:message code="m.dth.operator" text="#m.dth.operator#"/></label>
            <form:select path="dthOperator" cssClass="input-field">
                <form:option value=""><spring:message code="m.select.operator" text="#m.select.operator#"/></form:option>
                <form:options items="${dthRecharge.masterData.dthOperators}"
                              itemLabel="name" itemValue="operatorMasterId" />
            </form:select>

            <c:choose>
                <c:when test="${retryBlock eq true}">
                  <p class="error">Please try recharging this number after 10 minutes.</p>
                </c:when>
                
                <c:when test="${denominationFailure eq true}">
                  <p class="error">You have entered an invalid denomination, please check for valid denominations.</p>
                </c:when>
                
                <c:when test="${permanentFailure eq true}">
                  <p class="error">Please select the right operator or choose different service (Prepaid, Data Card or Postpaid)</p>
                </c:when>
                
                <c:when test="${operatorFailure eq true}">
                  <p class="error">We are observing a high failure rate for this operator in the last few minutes, please try after some time</p>
                </c:when>
                
                <c:otherwise></c:otherwise>
            </c:choose>

            <p class="error" style="display: none"></p>
            <form:errors path="dthOperator" element="div" cssClass="error"></form:errors>
        </div>

        <div class="helper-text" style="display: none"></div>

        <div class="field-unit" id="dthNumberWrap">
			<label for="dthNumber" id="dthNumberLabel"><spring:message code="m.dth.subscriber.number" text="m.#dth.subscriber.number#"/></label>
			<form:input path="dthNumber" type="tel" maxlength="14" cssClass="input-field" />
            <c:if test="${retryBlock eq true}">
              <p class="error" >Please try recharging this number after 10 minutes.</p>
            </c:if>
            <p class="error" style="display: none"></p>
            <form:errors path="dthNumber" element="div" cssClass="error"></form:errors>
		</div>

		<div class="field-unit field-unit-stroke" id="dthAmountWrap">
			<label for="dthAmount"><spring:message code="m.dth.recharge.amount" text="#m.dth.recharge.amount#"/></label>
			<form:input path="dthAmount" type="tel" maxlength="4" cssClass="input-field" />
            <select id="amount-dth-suntv" class="input-field">
                <c:forEach var="dthAmountItem" items="${sunDirectDenominationList}">
                    <option value="${dthAmountItem}"
                            <c:if test="${dthAmountItem eq fn:escapeXml(homeWebDo.dthAmount)}">selected='selected'</c:if>>${dthAmountItem}</option>
                </c:forEach>
            </select>
            <p class="error" style="display: none"></p>
            <form:errors path="dthAmount" element="div" cssClass="error"></form:errors>
		</div>		
    </div>

        <div class="field-unit-button">
            <button id="dth-recharge-submit" type="submit" class="btn btn-primary full-width"><spring:message code="m.continue.label" text="#m.continue.label#"/></button>
        </div>
	</form:form>


</div>