<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<jsp:include page="mobile-common-includes.jsp"/>
</head>
<body>
    <jsp:include page="mobile-header.jsp" />
    <jsp:include page="dth-recharge.jsp" />
    <jsp:include page="mobile-footer.jsp" />

    <%--scripts - to be loaded after content--%>
    <jsp:include page="mobile-bottom-scripts.jsp"/>
    <script src="${mt:keyValue("jsprefix1")}/mobile/mobile.recharge.js?v=${mt:keyValue("version.no")}"></script>
</body>
</html>

