<%@page import="com.freecharge.util.*;"%>
<!doctype html>
<html lang="en">
<head>
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200' rel='stylesheet' type='text/css'>
	<meta charset="UTF-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=100%, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
	<title>Freecharge - Cashback Success</title>
	<style>
		header {
            padding: 10px;
            text-align: center;
            margin-top: 1em;
        }
        body {
            background-color: #356BC1;
            font-family: 'Source Sans Pro',​sans-serif;
            font-size: 18px;
            line-height: 1.4;
            margin: 0;
            color: #FFFFFF;
        }
        #content {
            width: 94%;
            margin: 0 auto;
        }
        p {
        	text-align: center;
        	margin: 1em 0;
        	font-size: 1.2em;
        }
        #success-msg {
            font-size: 1.4em;
            font-weight: bold;
        }
	</style>
	<script type="text/javascript">
		
	</script>
</head>
<body>

	<!-- Header -->
    <header>
    <!-- Logo -->
        <img src="../../content/images/m/logo-large.png" alt="freecharge.in">
    </header>

    <div id="content">
        <p id="success-msg">
            Hey! your 10% cashback has been added to your FreeCharge Credits. You can see credits balance in the app.
        </p>
        <p>Use these credits for your next recharge or bill payment.</p>
    </div>
	
</body>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12939857-1']);
  _gaq.push(['_setDomainName', 'freecharge.in']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</html>