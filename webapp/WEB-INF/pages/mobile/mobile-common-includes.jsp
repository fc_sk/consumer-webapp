<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>

<!-- Start Visual Website Optimizer Code -->
<script type='text/javascript'>
var _vis_opt_account_id = 62392;
var _vis_opt_protocol = (('https:' == document.location.protocol) ? 'https://' : 'http://');
document.write('<s' + 'cript src="' + _vis_opt_protocol + 
'dev.visualwebsiteoptimizer.com/deploy/js_visitor_settings.php?v=1&a='+_vis_opt_account_id+'&url='
+encodeURIComponent(document.URL)+'&random='+Math.random()+'" type="text/javascript">' + '<\/s' + 'cript>');
</script>

<script type='text/javascript'>
if(typeof(_vis_opt_settings_loaded) == "boolean") { document.write('<s' + 'cript src="' + _vis_opt_protocol + 
'd5phz18u4wuww.cloudfront.net/vis_opt.js" type="text/javascript">' + '<\/s' + 'cript>'); }
// if your site already has jQuery 1.4.2, replace vis_opt.js with vis_opt_no_jquery.js above
</script>

<script type='text/javascript'>
if(typeof(_vis_opt_settings_loaded) == "boolean" && typeof(_vis_opt_top_initialize) == "function") {
        _vis_opt_top_initialize(); vwo_$(document).ready(function() { _vis_opt_bottom_initialize(); });
}
</script>
<!-- End Visual Website Optimizer Code -->

<%--<meta name="viewport" content="width=device-width, initial-scale=1">--%>
<%-- Fix for position:fixed problem // disables zoom ( which is good ) --%>
<meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no" />


<jsp:include page="mobile-html-header-includes.jsp"/>


<%--<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/freecharge.min.css?v=${mt:keyValue("version.no")}" />--%>
<%--<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/jquery.mobile.structure-1.3.1.min.css?v=${mt:keyValue("version.no")}" />--%>
<%--<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/mobile.custom.css?v=${mt:keyValue("version.no")}" />--%>

<link href='http://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
<%--<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/m/normalize.css" />--%>
<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/m/main.css?v=${mt:keyValue("version.no")}" />
<%--Loading jQuery first to make sure page load events can be tracked--%>
<script src="${mt:keyValue("jsprefix1")}/jquery-1.9.1.min.js"></script>
<%--Loading mix panel script--%>
<!-- start Mixpanel -->
<script type="text/javascript">
(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f);b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2}})(document,window.mixpanel||[]);
mixpanel.init("5e383124e41bb39e4f7384a2dcc244ed");
</script>
<!-- end Mixpanel -->
<script type="text/javascript">
    var _fcTrackerEventQueue = _fcTrackerEventQueue || [];
    <%--
    //This function gets redefined by the call to TrackingController default method. Take a look at trackerJs.jsp to see the redefinition.
    //The logic here is that, until we get a response from TrackingController default method, we queue events up. As soon as the response is loaded,
    //we redefine fcTrack to send the events directly to the server. We do this dance because we want the tracker requests to the server to
    //have the cookies sent in the TrackingController call but we do not want to block the page load until we get a response from TrackingController.
    --%>
    function fcTrack(params) {
        _fcTrackerEventQueue.push(params);
    }
</script>
