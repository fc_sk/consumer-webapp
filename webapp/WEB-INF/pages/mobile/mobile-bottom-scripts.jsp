<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ page import="com.freecharge.common.framework.util.CSRFTokenManager" %>
<%@ page import="com.freecharge.tracker.TrackerUtil" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="${mt:keyValue("jsprefix1")}/mobile/mobile.custom.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript">
    $(document).ready(function(ev){
        <c:set var="channelId" value="<%=FCConstants.CHANNEL_ID_MOBILE%>"/>
        $.getScript('${mt:trackerUrl(channelId)}');
    });

    $.ajaxSetup({
        beforeSend:function(xhr){
            xhr.setRequestHeader('<%=CSRFTokenManager.CSRF_REQUEST_IDENTIFIER%>', '${mt:csrfToken()}');
        }
    });
    //fcTrack({eventName: 'm_old_site_ld'});
</script>

<jsp:include page="mobile-google.jsp" />
