<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="payment" data-payment-request="true">
    <div class="wrapper">
        <p>
            Please Wait While We Redirect To Payment Gateway For Processing... <br>
            Please Do Not Press STOP, BACK or REFRESH buttons or CLOSE this
            window...
        </p>

        <form name="paymentRequest" id="paymentRequest" action="${actionUrl}" method="post">
            <c:forEach items="${resultMap.resultMap}" var="mapEntry">
                <input type="hidden" name="${mapEntry.key}" value="${mapEntry.value}"/>
            </c:forEach>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        fcTrack({eventName: 'm_o_payment_call'});
    });
</script>