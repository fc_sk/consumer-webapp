<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>


<div class="final">
    <div class="wrapper">
        <p class='payment-response-heading'><span>Thanks!</span><br>We got your payment and you'll get your recharge soon..</p>

        <table class="table">
            <tbody>
                <tr>
                    <th>Order ID</th>
                    <td>${checkoutData.orderId}</td>
                </tr>
                <tr>
                    <th>Recharge Amount</th>
                    <td><span class="webRupee">Rs.</span> ${checkoutData.rechargeAmount}</td>
                </tr>
                <tr>
                    <th><spring:message code="m.number" text="#m.number#"/></th>
                    <td>${checkoutData.number}</td>
                </tr>
            </tbody>
        </table>

        <p class="helper-text">You can check the status of your recharge <a href="/m/myrecharges">here</a> and  even if your recharge doesn't succeed, your money is always safe with us in your <a href="/m/mycredits?isAddCash=true">FreeCharge Credits</a>!</p>
        <!--  	<c:choose>
		<c:when test="${checkoutData.rechargeResponse=='00'}">
			<spring:message code="m.recharge.successful" text="#m.recharge.successful#"/>
		</c:when>
		<c:otherwise>
			<spring:message code="m.recharge.under.process" text="#m.recharge.under.process#"/>
		</c:otherwise>
	</c:choose> -->

        <p class="credits-msg">Speed up your recharging by 10 times! <a href="/m/mycredits?isAddCash=true">Find out how, here</a>!</p>

        <a href="/m" class="btn block"><spring:message code="m.do.another.recharge" text="#m.do.another.recharge#"/></a>

    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            mixpanel.track('Recharge Status');
        });
    </script>
</div>

