<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="formEditModeOnLoad" value="${(empty shippingDetails.miscCheckoutDetailsMap['isEdit']) || (shippingDetails.miscCheckoutDetailsMap['isEdit'] eq 'null') || (shippingDetails.miscCheckoutDetailsMap['isEdit'] eq 'no')}" />

<div class="account">
    <h2 class="page-title"><spring:message code="m.shippingdetails.heading" text="#m.shippingdetails.heading#"/></h2>
    <div class="form-wrap">
        <br>
        <form:form commandName="shippingDetails" action="/m/saveAddress" method="POST" name="userShippingDetails" id="userShippingDetails" data-form-edit="${formEditModeOnLoad}">
            <form:errors path="" cssClass="error" element="div"></form:errors>
            <div class="field-unit">
                <label for="title"><spring:message code="m.title.label" text="#m.title.label#"/></label>
                <form:select path="title" cssClass="input-field input-field-select-small">
                    <form:option value="Mr">
                        <spring:message code="m.mr.label" text="#m.mr.label#"/>.
                    </form:option>
                    <form:option value="Ms">
                        <spring:message code="m.ms.label" text="#m.ms.label#"/>.
                    </form:option>
                    <form:option value="Mrs">
                        <spring:message code="m.mrs.label" text="#m.mrs.label#"/>.
                    </form:option>
                </form:select>
                <form:errors path="title" element="div" cssClass="error"></form:errors>
            </div>
            <div class="field-unit">
                <label for="firstName"><spring:message code="m.fullname.label" text="#m.fullname.label#"/></label>
                <form:input path="firstName" cssClass="input-field"/>
                <form:errors path="firstName" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit">
                <label for="address1"><spring:message code="m.address.label" text="#m.address.label#"/></label>
                <form:input path="address1" cssClass="input-field"/>
                <form:errors path="address1" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit">
                <label for="area"><spring:message code="m.area.label" text="#m.area.label#"/></label>
                <form:input path="area" cssClass="input-field"/>
                <form:errors path="area" cssClass="error" element="div"></form:errors>
            </div>

            <div class="field-unit">
                <label for="landmark"><spring:message code="m.landmark.label" text="#m.landmark.label#"/></label>
                <form:input path="landmark" cssClass="input-field"/>
                <form:errors path="landmark" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit">
                <label for="state"><spring:message code="m.state.label" text="#m.state.label#"/></label>
                <form:select path="stateId" id="state" cssClass="input-field" onchange="getCityList();">
                    <option value="">
                        <spring:message code="m.select.state" text="#m.select.state#"/>
                    </option>
                    <form:options items="${shippingDetails.masterData.stateMasterList}" itemLabel="stateName" itemValue="stateMasterId" />
                </form:select>
                <form:errors path="stateId" cssClass="error" element="div"></form:errors>
            </div>

            <div class="field-unit">
                <label for="city"><spring:message code="m.city.label" text="#m.city.label#"/></label>
                <form:select path="cityId" id="city" cssClass="input-field">
                    <option value="">
                        <spring:message code="m.select.city" text="#m.select.city#"/>
                    </option>
                    <form:options items="${shippingDetails.cityList}" itemLabel="cityName" itemValue="cityMasterId" />
                </form:select>
                <form:errors path="cityId" cssClass="error" element="div"></form:errors>
            </div>

            <div class="field-unit">
                <label for="postalCode"><spring:message code="m.pincode.label" text="#m.pincode.label#"/></label>
                <form:input path="postalCode" type="number" cssClass="input-field input-field-small"/>
                <form:errors path="postalCode" cssClass="error" element="div"></form:errors>
            </div>

            <c:forEach items="${shippingDetails.miscCheckoutDetailsMap}" var="entry">
                <input type="hidden" id="${entry.key}" name="<c:out value="miscCheckoutDetailsMap['${entry.key}']"/>" value="<c:out value="${entry.value}"/>" />
            </c:forEach>


            <c:if test="${shippingDetails.miscCheckoutDetailsMap['isEdit'] == null}">
                <input type="hidden" id="isEdit" name="miscCheckoutDetailsMap['isEdit']" value="no" />

                <input type="hidden" name="miscCheckoutDetailsMap['actionParam']" value="saveorupdate" />
            </c:if>



            <div id="defaultActions" <c:if test="${!formEditModeOnLoad}" >style="display:none;" </c:if> >
                <div class="field-unit">
                    <input id="edit" type="button"  value='<spring:message code="m.edit.shippingdetails" text="#m.edit.shippingdetails#"/>' class="btn full-width">
                </div>
                <div class="field-unit">
                    <button id="continue" type="submit" class="btn btn-primary full-width" >
                        <spring:message code="m.continue.shippingdetails" text="#m.continue.shippingdetails#"/>
                    </button>
                </div>
            </div>

            <div id="onEditActions" <c:if test="${formEditModeOnLoad}" >style="display:none;" </c:if> >
                <div class="field-unit">
                    <input id="cancel" type="reset"  value='<spring:message code="m.cancel.shippingdetails" text="#m.cancel.shippingdetails#"/>' class="btn full-width">
                </div>
                <div class="field-unit">
                    <button id="save" type="button" class="btn btn-primary full-width">
                        <spring:message code="m.save.shippingdetails" text="#m.save.shippingdetails#"/>
                    </button>
                </div>
            </div>

        </form:form>    
    </div>
</div>
