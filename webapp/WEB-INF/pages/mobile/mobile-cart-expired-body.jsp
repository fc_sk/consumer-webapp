<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div data-role="content" class="home" data-url="/m">
		<script type="text/javascript">
			$( document ).ready(function(){
			  $.mobile.loader.prototype.options.text = "loading";
			  $.mobile.loader.prototype.options.textVisible = true;
			  $.mobile.loader.prototype.options.theme = "a";
			  $.mobile.loader.prototype.options.html = "";
			  $.mobile.loading( 'show', {
					text: 'Sorry your cart has expired. Redirecting to home page.',
					textVisible: true,
					theme: 'c',
					html: ""
				});
			  setTimeout( function() {
				  $.mobile.changePage( "/m" );
			  },2000);
			});
		</script>
</div>

