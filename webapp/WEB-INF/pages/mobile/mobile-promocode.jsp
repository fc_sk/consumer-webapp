<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>


<div class="account">
    <!-- <h2 class="page-title">Got a PROMO/FREEFUND code?</h2> -->
    
    <div class="pagehead">
	  Got a PROMO/FREEFUND code?
	</div>
    
    <div id="mobileFreefundApplyHolder" class="wrapper freefund">

        <p class="helper-text hideit">Promo codes are typically distributed to customers through our partners, and can be used to avail special discounts on your transaction. 
                               A FreeFund code is like a gift voucher code. The amount is fixed and it can be deposited instantly in your FreeCharge Credits. </p>
        <div class="field-unit">
            <label for="freefundCode">Enter your code</label> 
            <input type="text" value="" placeholder="Enter your code here" name="freefundCode" id="mobileFreefundCodeId" class="input-field" />
            <input id="is-recaptcha-enabled" type="hidden" value="${forceCaptcha}" />
    		<div id="captcha-placement-td"  style="<c:if test="${not forceCaptcha}"> display:none; </c:if>" >
	    		<div id="captcha-placement-id">
 	    		<div id="captcha-draw-id" style="border: 1px dotted #dddddd;">
 				 	<c:if test="${forceCaptcha}"> <jsp:include page="/WEB-INF/views/captcha-draw.jsp"></jsp:include>  </c:if>
 				</div> 
 				<a href="javascript:void(0)" id="redraw-captcha-button" >Refresh</a>
 				<br/>
	    			<input name="captcha" type="text" autocomplete="off" placeholder="Enter text here"  class="englishOnlyField required" />
	    		 </div>
    		</div>
            <div id="mobileFreefundApplyMsgHolder" class="error" style="display:none;"></div>
        </div>
        <div class="field-unit">
            <button id="addMobilePromoCode" class="btn btn-primary full-width" data-lookup-id="${checkoutData.lookupId}" data-product-type="${checkoutData.productType}">
                <spring:message code="button.redeem"/>
            </button>
        </div>
        
        <div class="pagehead2 textcenter">
	      If you don't have a code just continue
	    </div>
        
       <form:form  commandName="checkoutData" action="/m/paymentPage" method="POST"  name='couponForm'>
                    <form:hidden path="rechargeAmount" value = "${checkoutData.rechargeAmount}"/>
                    <form:hidden path="lookupId" value = "${checkoutData.lookupId}"/>
                    <form:hidden path="totalAmount" value = "${checkoutData.totalAmount}"/>
                    <form:hidden path="number" value = "${checkoutData.number}"/>
                    <form:hidden path="productType" value = "${checkoutData.productType}"/>
                    <div >
                        <button type="submit" class='btn full-width' id="promocode-continue-bottom">Continue to payment</button>
                    </div>
                </form:form>

    </div>
</div>
