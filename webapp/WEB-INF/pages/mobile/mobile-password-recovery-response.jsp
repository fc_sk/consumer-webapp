<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="auth">
    <div class="wrapper">
        <p><strong><spring:message code="m.we.have.sent.email.msg" text="#m.we.have.sent.email.msg#"/> ${emailId}</strong></p>
        <p><spring:message code="m.email.contain.instruction.for.reset.msg" text="#m.email.contain.instruction.for.reset.msg#"/>.<spring:message code="m.dont.recieve.mail.check.junk.msg" text="#m.dont.recieve.mail.check.junk.msg#"/> <a href="mailto:care@freecharge.com"><spring:message code="m.cust.care.address" text="#m.cust.care.address#"/></a> <spring:message code="m.further.assistance" text="#m.further.assistance#"/>.</p>
    </div>
</div>
