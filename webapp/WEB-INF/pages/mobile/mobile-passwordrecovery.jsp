<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="account">
	<h2 class="page-title"><spring:message code="m.reset.password.header" text="#m.reset.password.header#"/></h2>


    <form:form commandName="passwordRecovery" action="/m/mobile-passwordrecovery" method="POST" name="passwordRecoveryWebDOForm" id="passwordRecoveryWebDOForm" cssClass="form">
        <form:hidden path="email" />
	    <form:hidden path="userExist" />
        <form:hidden path="userId" />
        <form:hidden path="status" />
        <form:hidden path="encryptValue" />
        <form:hidden path="encryptEmail" />
	<p>
       	<label><strong><spring:message code="m.email" text="#m.email#"/> : </strong></label>
       	<span><c:out value="${passwordRecovery.email}"/></span>
    </p>

    <div class="form-wrap">
        <p class="error">
            <c:out value="${errorMsg}"/>
        </p>
        <div class="field-unit">
           <label for="newPassword"><spring:message code="m.reset.password" text="#m.reset.password#"/></label>
           <form:password path="newPassword" maxlength="30" cssClass="input-field" />
       </div>
       <div class="field-unit">
           <label for="confirmPassword"><spring:message code="m.confirm.password" text="#m.confirm.password#"/></label>
           <form:password path="confirmPassword" maxlength="30" cssClass="input-field" />
        </div>

      <div class="field-unit">
           <button id="password-reset-button" type="submit" class="btn btn-primary full-width"><spring:message code="m.reset.password.button" text="#m.reset.password.button#"/></button>
       </div>
    </div>
</form:form>
</div>
