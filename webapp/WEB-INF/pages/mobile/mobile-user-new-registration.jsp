<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="auth">
    <!-- <h2 class="page-title">Sign Up</h2> -->
    
    <div class="pagehead2">
	  Create a new FreeCharge account in a minute
	</div>

    <div class="form-wrap">
        <form:form commandName="user" action="/m/adduser" method="POST" name="userForm" id="userForm" class="form" autocomplete="off">
            <form:errors path="" cssClass="error" element="div"></form:errors>
            <div class="field-unit">
                <label for="firstName"><spring:message code="m.fullname.label" text="#m.fullname.label#"/></label>
                <form:input path="firstName" placeholder="Full name" cssClass="input-field" />
                <form:errors path="firstName" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit">
                <label for="mobileNo"><spring:message code="m.mobile.no.label" text="#m.mobile.no.label#"/></label>
                <form:input path="mobileNo" type="tel" Placeholder="Mobile" cssClass="input-field" maxlength="10" />
                <p class="error" style="display: none"></p>
                <form:errors path="mobileNo" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit">
                <label for="email"><spring:message code="m.email.label" text="#m.email.label#"/></label>
                <form:input path="email" type="email" placeholder="Email" cssClass="input-field"/>
                <form:errors path="email" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit">
                <label for="password"><spring:message code="m.password.label" text="#m.password.label#"/></label>
                <form:password path="password" placeholder="Password" cssClass="input-field" maxlength="20" />
                <form:errors path="password" cssClass="error" element="div"></form:errors>
            </div>

            <form:hidden path="signupFrom" />
            <c:forEach items="${user.fromParamMap}" var="entry">
                <form:hidden path="fromParamMap[${entry.key}]" />
            </c:forEach>
            <c:forEach items="${user.fromParamMap}" var="entry">
                <input type="hidden" name="<c:out value="${entry.key}"/>"
                       value="<c:out value="${entry.value[0]}"/>" />
            </c:forEach>

            <div class="field-unit">
                <button id="register" type="submit" class="btn btn-primary full-width">
                    <spring:message code="m.signup.label" text="#m.signup.label#"/>
                </button>
            </div>

        </form:form>
        
        <div class="pagehead2 textcenter">
	      Already have a FreeCharge account?
	    </div>

        <a href="/m/login" class="btn block"><spring:message code="m.login.label" text="#m.login.label#"/></a>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function(){
             if(_gaq){
                 _gaq.push(['_trackEvent', 'create_account', 'focus', "new_signup"]);
             }
        }, 10);
    });
</script>
