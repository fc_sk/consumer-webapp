<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession"%>
<%@ page import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<%@ page import="com.freecharge.web.util.WebConstants"%>
<%@ page import="java.util.Map"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class="auth">

    <!-- <h2 class="page-title"><spring:message code="m.login.label" text="#m.login.label#" /></h2> -->
    
    <div class="pagehead2">
	  Login into your FreeCharge account
	</div>

    <div class="form-wrap">
	<form:form commandName="mobileLogin" action="/m/do-mobile-login"
		method="POST" name="mobileForm" id="mobileForm" class="form">

        <form:label path="loginError" cssClass="error">${mobileLogin.loginError}</form:label>

        <div class="field-unit">
			<label for="loginEmail"><spring:message code="m.email.label"
					text="#m.email.label#" /></label>
			<form:input path="loginEmail" placeholder="Email address" type="email" cssClass="input-field" />
			<form:errors path="loginEmail" element="div" cssClass="error"></form:errors>
		</div>
		<div class="field-unit field-unit-password">
			<label for="loginPassword"><spring:message
					code="m.password.label" text="#m.password.label#" /></label>
			<form:password path="loginPassword" placeholder="Password" cssClass="input-field"/>
			<form:errors path="loginPassword" element="div" cssClass="error"></form:errors>
		</div>
        <p class="forgot-password"><a class="link" href="/m/mobile-changepasswordpage"><spring:message code="m.forgot.password.label" text="#m.forgot.password.label#"/></a></p>


        <form:hidden path="loginFrom" />
		<c:forEach items="${mobileLogin.fromParamMap}" var="entry">
			<form:hidden path="fromParamMap[${entry.key}]" />
		</c:forEach>
		<c:forEach items="${mobileLogin.fromParamMap}" var="entry">
			<input type="hidden" name="<c:out value="${entry.key}"/>"
				value="<c:out value="${entry.value[0]}"/>" />
		</c:forEach>

		<div class="field-unit">
			<button id="login" type="submit" class="btn btn-primary full-width">
				<spring:message code="m.login.label" text="#m.login.label#" />
			</button>
		</div>
		
		<div class="pagehead2 textcenter">
	      Don't have a FreeCharge account?
	    </div>
            
	</form:form>
        <form:form commandName="user" action="/m/signup"
                   method="POST" name="mobileSignupCallForm" id="mobileSignupCallForm" class="form" data-ajax="false">
            <form:hidden path="signupFrom" />
            <c:forEach items="${user.fromParamMap}" var="entry">
                <form:hidden path="fromParamMap[${entry.key}]" />
            </c:forEach>
            <c:forEach items="${user.fromParamMap}" var="entry">
                <input type="hidden" name="<c:out value="${entry.key}"/>"
                       value="<c:out value="${entry.value[0]}"/>" />
            </c:forEach>
            <a  href="javascript:void(0)" id="signup" class="btn block">Create new account</a>
        </form:form>
    </div>
    <script type="text/javascript">
    	$(document).ready(function() {
    		$('#login').on('click', function(event) {
    			event.preventDefault();
            	mixpanel.track("Login Attempt");
            	
            	//tracker
        		fcTrack({eventName: 'm_o_Signin_eml_att'});

            	$('#mobileForm').submit();
        	});
    		//mixpanel.track_links('#login', 'Login Attempt');
    	});
    </script>
</div>
