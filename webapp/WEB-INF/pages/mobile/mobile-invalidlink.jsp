<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div data-role="content">
     <h2 class="page-title"><spring:message code="m.oops.invalid.link" text="#m.oops.invalid.link#"/></h2>
     <p><spring:message code="m.we.are.sorry" text="#m.we.are.sorry#"/> !!!</p>
     <p><spring:message code="m.link.is.invalid" text="#m.link.is.invalid#"/>.</p>
     <p><spring:message code="m.you.may.go.back" text="#m.you.may.go.back#"/> <a href="/m" rel="external"><spring:message code="m.homepage" text="#m.homepage#"/></a>.</p>
</div>
