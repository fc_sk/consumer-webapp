<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="com.freecharge.common.framework.util.CSRFTokenManager"%>


<div class="account">
    <h2 class="page-title">My Account</h2>

    <ul class="menu menu-icons">
        <li class="option"><a href="/m/myrecharges" >
            <img src="${mt:keyValue("imgprefix1")}/images/m/account/acc-recharges.png">
            <spring:message code="m.myrecharge.label" text="#m.myrecharge.labels#" />
            <img src="${mt:keyValue("imgprefix1")}/images/m/chevron.png" class="menu-indicator"/>
        </a></li>
        <li class="option"><a href="/m/mycredits" >
            <img src="${mt:keyValue("imgprefix1")}/images/m/account/acc-credits.png">
            My Credits
            <img src="${mt:keyValue("imgprefix1")}/images/m/chevron.png" class="menu-indicator"/>
        </a></li>
        <li class="option"><a href="/m/editprofile" >
            <img src="${mt:keyValue("imgprefix1")}/images/m/account/acc-profile.png">
            <spring:message code="m.edit.profile" text="#m.edit.profile#"/>
            <img src="${mt:keyValue("imgprefix1")}/images/m/chevron.png" class="menu-indicator"/>
        </a></li>
        <li class="option"><a href="/m/change-password" >
            <img src="${mt:keyValue("imgprefix1")}/images/m/account/acc-password.png">
            <spring:message code="m.change.password" text="#m.change.password#"/>
            <img src="${mt:keyValue("imgprefix1")}/images/m/chevron.png" class="menu-indicator"/>
        </a></li>
        <%--<li class="option"><a href="/m/myprofile" ><spring:message code="m.profile.label" text="#m.profile.label#"/></a></li>--%>
        <li class="option"><a href="/m/logout?<%=CSRFTokenManager.CSRF_REQUEST_IDENTIFIER%>=${mt:csrfToken()}" >
            <img src="${mt:keyValue("imgprefix1")}/images/m/account/acc-logout.png">
            <spring:message code="m.logout.label" text="#m.logout.label#"/>
            <img src="${mt:keyValue("imgprefix1")}/images/m/chevron.png" class="menu-indicator"/>
        </a></li>
    </ul>

</div>