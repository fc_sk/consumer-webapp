<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%
    String[] months = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
    request.setAttribute("months", months);
%>


<div class="account">
    <h2 class="page-title no-margin"><spring:message code="my.recharges" text="#my.recharges#"/></h2>
    <c:choose>
        <c:when test="${fn:length(rechargeHistoryList) gt 0 }">
            <table class="table">
                <c:forEach items="${rechargeHistoryList}" var="rechargeHistoryList">
                    <c:choose>
                        <c:when test="${rechargeHistoryList.rechargeStatus == '00'}">
                            <c:set var="statusColor" value="success" />
                        </c:when>
                        <c:when test="${rechargeHistoryList.rechargeStatus == '08' || rechargeHistoryList.rechargeStatus == '-1'}">
                            <c:set var="statusColor" value="under-process" />
                        </c:when>
                        <c:otherwise>
                            <c:set var="statusColor" value="failed" />
                        </c:otherwise>
                    </c:choose>

                    <c:set var="date" value="${rechargeHistoryList.createdOn}" />
                    <c:set var="dt" value="${fn:split(date, '-')}" />
                    <fmt:parseNumber var="i" integerOnly="true" type="number" value="${dt[1]}" />

                    <tr>
                        <td>
                            <table class="date-table">
                                <tr>
                                    <td align="center">
                                        <div class="fc-history-day">${dt[2]}</div>
                                    </td>
                                    <td>
                                        <div class="fc-history-month">${months[i-1]}</div>
                                        <div class="fc-history-year">${dt[0]}</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <div class="fc-history-details">
                                <p class="fc-history-info"><strong class="${statusColor}">${rechargeHistoryList.serviceNumber}</strong></p>
                                <%--<p class="fc-history-info-more">${rechargeHistoryList.productName}/${rechargeHistoryList.operatorCode}/${rechargeHistoryList.circleName}</p>--%>
                                <p class="fc-history-id">${rechargeHistoryList.orderId}</p>
                            </div>
                        </td>
                        <td align="right">
                            <span class="amount"><span class="webRupee">Rs.</span> ${rechargeHistoryList.txnAmount}</span>
                        </td>
                    </tr>

                    <%--<form name="" id="${rechargeHistoryList.orderId}-form" method="post" action="/app/rechargeagain.htm">
                        <input type="hidden" name="rechargeHistoryId" id="${rechargeHistoryList.orderId}-amount" value="${rechargeHistoryList.userRechargeHistoryId}" />

                    </form>--%>
                </c:forEach>
            </table>
        </c:when>
        <c:otherwise>
            <div class="wrapper">
                <br>
                <p class="helper-text"><spring:message code="recharge.historymsg" text="#recharge.historymsg#"/>.</p>
                <br>
            </div>
        </c:otherwise>
    </c:choose>
</div>

<%--
<script type="text/javascript">
    $(document).ready(function(){
        // Split date for recharge history
        $(".fc-history-date").each(function(){
            var $this = $(this),
                dt = $(this).text(),
                arr = dt.split('-'[0]);

            switch(arr[1]){
                case 01 : arr[1] = "JAN",
                case 02 : arr[1] = "FEB",
            }

            $this.html('<span class="day">'+arr[2]+'</span><span class="month">'+arr[1]+'-'+arr[0]+'</span> ')


        });

    });

</script>--%>
