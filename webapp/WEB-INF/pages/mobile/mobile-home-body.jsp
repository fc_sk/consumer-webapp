<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ page import="com.freecharge.tracker.TrackerUtil" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${isValidQueryString}">
   <c:set  value="${param.ext}" var="ext" />
   <c:set  value="${param.extName}" var="extName" />
</c:if>

<div class="home" >
	<div class="home-banner">
        <p>
        	<span class='home-heading'>Welcome to Freecharge</span><br>
        	<span class='home-subheading'>Enjoy a hassle free recharge experience</span>
        </p>
	</div>

    <c:choose>
    <c:when test="${stripType == 'did4'}">
      <a class="pepsi-banner" style="background-color: #0056a7;" href="/m/didpromotion" target="_blank" title="Dance India Dance free talktime" ><img src="${mt:keyValue("imgprefix1")}/images/m/campaigns/DID4_2.png" alt="Dance India Dance free talktime"  ></a>
    </c:when>
	<c:when test="${stripType == 'mcd'}">
      <a class="pepsi-banner" style="background-color: #87171c;" href="/m/mcdpromotion" target="_blank" title="McDonalds gift festival" ><img src="${mt:keyValue("imgprefix1")}/images/m/campaigns/mcd.png" alt="McDonalds gift festival"></a>
    </c:when>
	<c:when test="${stripType == 'amazon'}">
      <a class="pepsi-banner" style="background-color: #0056a7;" href="https://d32vr05tkg9faf.cloudfront.net/content/campaigns/Amazon/m/index.html" target="_blank" title="Amazon gift festival" ><img src="https://d32vr05tkg9faf.cloudfront.net/content/campaigns/Amazon/m/images/Amazon.png" alt="Amazon gift festival"  ></a>
    </c:when>
    <c:when test="${ext == '1' && extName != ''}">
      <a class="pepsi-banner" style="background-color: #0056a7;" href="https://d32vr05tkg9faf.cloudfront.net/content/campaigns/${extName}/m/index.html" target="_blank" title="${extName}" ><img src="https://d32vr05tkg9faf.cloudfront.net/content/campaigns/${extName}/m/images/${extName}.png" alt="${extName}"  ></a>
    </c:when>
    <c:otherwise>
    <!--
      <a class="pepsi-banner" style="background-color: #10263B" href="https://play.google.com/store/apps/details?id=com.freecharge.android&referrer=utm_source%3Dmobilesite%26utm_medium%3Dbanner%26utm_campaign%3Dmobilesitetoandroid" target="_blank" title="Install the FreeCharge android app now" ><img src="${mt:keyValue("imgprefix1")}/images/m/campaigns/androidapp.png" alt="Install the FreeCharge android app now"/>
      -->

      <a class="pepsi-banner" style="background-color: #10263B;" href="http://offers.freecharge.com/bourbon/index.html" title="Bourbon offer" >
      <img src="${mt:keyValue("imgprefix1")}/images/m/campaigns/bourbon.png" alt="Bourbon offer" style="" style="min-width:100%;height:126px;" />
    </c:otherwise>
    </c:choose>

    <ul class="menu">
		<li class="option">
            <a href="/m/mobile-recharge" id="prepaid-mobile-recharge">
                <img src="${mt:keyValue("imgprefix1")}/images/m/mobileNew.png" class="home-menu-icon">
				<h2><spring:message code="m.mobile.label" text="#m.mobile.label#" /></h2>
				<p><spring:message code="m.recharge.prepaid.mobile" text="#m.recharge.prepaid.mobile#" /></p>
				<img src="${mt:keyValue("imgprefix1")}/images/m/chevron-right.png" class="home-menu-chevron">
		    </a>
            
        </li>
		<li class="option">
            <a href="/m/dth-recharge" id="dth-recharge">
                <img src="${mt:keyValue("imgprefix1")}/images/m/dthNew.png" class="home-menu-icon">
				<h2><spring:message code="m.dth.label" text="#m.dth.label#" /></h2>
				<p><spring:message code="m.recharge.dth" text="#m.recharge.dth#" /></p>
				<img src="${mt:keyValue("imgprefix1")}/images/m/chevron-right.png" class="home-menu-chevron">
		    </a>
            
        </li>
		<li class="option">
            <a href="/m/datacard-recharge" id="datacard-recharge">
                <img src="${mt:keyValue("imgprefix1")}/images/m/datacardNew.png" class="home-menu-icon">
				<h2><spring:message code="m.datacard.label" text="#m.datacard.label#" /></h2>
				<p><spring:message code="m.recharge.datacard" text="#m.recharge.datacard#" /></p>
				<img src="${mt:keyValue("imgprefix1")}/images/m/chevron-right.png" class="home-menu-chevron">
		    </a>
            
        </li>
	</ul>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        /*mixpanel.register({
            state: ($('#freechargeMApp').data('isLogged') ? 'Logged In' : 'Not Logged In')
        });*/
        mixpanel.track("Startup", {
            'Login State': ($('#freechargeMApp').data('isLogged') ? 'Logged In' : 'Not Logged In')
        });

        //tracking events
        $("#prepaid-mobile-recharge").click(function(){
            fcTrack({eventName: 'm_old_prepaid'});
        });
        $("#dth-recharge").click(function(){
            fcTrack({eventName: 'm_old_dth'});
        });
        $("#datacard-recharge").click(function(){
            fcTrack({eventName: 'm_old_datacard'});
        });

    });
    fcTrack({eventName: 'm_old_site_loaded'});

    <%-- This data is pushed to tracker controller in trackerjs.jsp --%>
    <%-- Wrapped in a try catch block to prevent some mobile browser from crapping out --%>
    try {
        var TRACKER_CLIENT_DATA = (function() {
            var userAgent = navigator.userAgent;
            var referrer = document.referrer;
            var params = location.search;

            return {
                "<%=TrackerUtil.CLIENT_DETAILS_PARAM_USER_AGENT%>": userAgent,
                "<%=TrackerUtil.CLIENT_DETAILS_PARAM_REFERRER%>": referrer,
                "<%=TrackerUtil.CLIENT_DETAILS_PARAM_PARAMS%>": params
            };
        })();
    } catch (e) {
    }
</script>

