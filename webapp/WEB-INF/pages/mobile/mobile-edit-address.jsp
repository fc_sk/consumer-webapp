<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class="account">
    <h2 class="page-title">Edit Address</h2>
    <div class="form-wrap">
        <c:choose>
             <c:when test="${updaddressstatus}">
                <div class="message-success mobile-dialog" id="addressupmsg">
                    <p class='mobile-dialog-text'><c:out value="${message}"/></p>
                </div>
            </c:when>
            <c:otherwise>
                <div class="message-failure mobile-dialog" id="addresserrormsg">
                    <p class='mobile-dialog-text'><c:out value="${errmessage}"/></p>
                </div>
            </c:otherwise>
        </c:choose>

        <%-- Profile address form start  --%>
        <form:form action="/m/mobile-updateaddress" method="post" name="addressupdateform" id="addressupdateform" modelAttribute="editAddress"  cssClass="form">
            <div class="field-unit first-field">
                <label><spring:message code="address" text="#address#"/></label>
                <spring:bind path="editAddress.address">
                    <form:input id="address" path="address" maxlength="50" cssClass="input-field" />
                </spring:bind>
                <p class="error" style="display: none"></p>
                <form:errors path="address" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit">
                <label><spring:message code="area" text="#area#"/></label>
                <spring:bind path="editAddress.area">
                    <form:input id="area"  path="area" maxlength="50" cssClass="input-field"  />
                </spring:bind>
                <p class="error" style="display: none"></p>
                <form:errors path="area" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit">
                <label><spring:message code="landmark" text="#landmark#"/></label>
                <spring:bind path="editAddress.landmar">
                    <form:input path="landmar" id="landmark"  maxlength="50"  cssClass="input-field" />
                </spring:bind>
            </div>
            <div class="field-unit">
                <label><spring:message code="state" text="#state#"/></label>
                <spring:bind path="editAddress.state">
                    <form:select path="state" id="state" cssClass="input-field" onchange="getCityList()">
                        <c:forEach var="stateMaster" items='${editAddress.statelist}'>
                            <c:out value="${stateMaster.stateMasterId}"/>
                            <c:choose>
                                <c:when test="${stateMaster.stateMasterId eq editAddress.state}">
                                    <option value="${stateMaster.stateMasterId}" selected="selected">${stateMaster.stateName}</option>
                                </c:when>
                                <c:otherwise><option value="${stateMaster.stateMasterId}">${stateMaster.stateName}</option></c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </form:select>
                </spring:bind>
            </div>
            <div class="field-unit">
                <label for="city"><spring:message code="m.city.label" text="#m.city.label#"/></label>
                <form:select path="city" cssClass="input-field">
                    <option value="">
                        <spring:message code="m.select.city" text="#m.select.city#"/>
                    </option>
                    <form:options items="${editAddress.citylist}" itemLabel="cityName" itemValue="cityMasterId" />
                </form:select>
                <p class="error" style="display: none"></p>
                <form:errors path="city" cssClass="error" element="div"></form:errors>
            </div>

            <div class="field-unit">
                <label><spring:message code="pin.code" text="#pin.code#"/>:</label>
                <spring:bind path="editAddress.pincode">
                    <form:input id="pincode"  path="pincode" maxlength="6" cssClass="input-field input-field-small" />
                </spring:bind>
                <p class="error" style="display: none"></p>
                <form:errors path="pincode" cssClass="error" element="div"></form:errors>
            </div>
            <div class="field-unit">
                <button id="mobile-address-edit" class="btn btn-primary full-width"><spring:message code="m.svae.changes" text="#m.svae.changes#"/></button>
            </div>
        </form:form>
    </div>
</div>


    





