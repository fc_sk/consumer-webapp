<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
    String[] months = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
    request.setAttribute("months", months);
%>

<div class="account">
    <h2 class="page-title no-margin">Credits Activity</h2>
    <c:choose>
    <c:when test="${rows == null}">
        <br />
        <p class="helper-text">Sorry! we've not found any history of your credits activity.</p>
        <br />
    </c:when>
    <c:otherwise>
        <table class="credits-table">

            <c:forEach items="${rows}" var="each">
                <tr>
                    <td>
                        <c:set var="date" value="${each.created_ts}" />
                        <c:set var="dt" value="${fn:split(date, '/')}" />
                        <fmt:parseNumber var="i" integerOnly="true" type="number" value="${dt[1]}" />

                        <table class="date-table">
                            <tr>
                                <td align="center">
                                    <div class="fc-history-day">${dt[0]}</div>
                                </td>
                                <td>
                                    <div class="fc-history-month">${months[i-1]}</div>
                                    <div class="fc-history-year">${dt[2]}</div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${each.IS_DEBIT eq 'true'}">
                                <p class="fc-credit-status text-color-3">Withdrawal</p>
                            </c:when>
                            <c:otherwise>
                                <p class="fc-credit-status text-color-2">Deposit</p>
                            </c:otherwise>
                        </c:choose>
                        <p class="fc-credit-id">${each.order_id}</p>
                            <%--<c:if test="${each.operator != NULL}">
                                <p>
                                ${each.description}
                                <c:choose>
                                    <c:when test="${each.fund_source eq 'RECHARGE_FUND'}">
                                    </c:when>
                                    <c:otherwise>
                                        <p>${each.subscriber_number} | ${each.operator} | ${each.circle}</p>
                                    </c:otherwise>
                                </c:choose>
                                </p>
                            </c:if>--%>
                            <%--<p><strong>Balance</strong> = <span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span> ${each.NEW_BALANCE}</p>--%>
                    </td>
                    <td align="right">
                        <c:choose>
                            <c:when test="${each.IS_DEBIT eq 'true'}">
                                <p class="amount">
                                    <span class="webRupee"><spring:message code="rupee" text="#rupee#" />.</span> ${each.txn_amount}
                                </p>
                            </c:when>
                            <c:otherwise>
                                <p class="amount">
                                    <span class="webRupee"><spring:message code="rupee" text="#rupee#" />.</span> ${each.txn_amount}
                                </p>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>

        </table>
    </c:otherwise>
    </c:choose>



</div>