<!doctype html>
<html lang="en">
<head>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200' rel='stylesheet' type='text/css'>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=100%, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
    <title>Plus One Freecharge on Google+</title>
    <link href="https://play.google.com/store/apps/details?id=com.freecharge.android" />
    <style>
        header {
            padding: 10px;
            text-align: center;
            margin-top: 1em;
        }
        body {
            -webkit-user-select: none;  /* Chrome all / Safari all */
            -moz-user-select: none;     /* Firefox all */
            -ms-user-select: none;      /* IE 10+ */
            -o-user-select: none;
            user-select: none;
            background-color: #356BC1;
            font-family: 'Source Sans Pro',​sans-serif;
            font-size: 1em;
            line-height: 1.4;
            margin: 0;
            color: #FFFFFF;
        }
        #content {
            width: 82%;
            margin: 0 auto;
        }
        #request-plus-one-msg {
            margin-top : 1em;
        }
        #request-plus-one-msg div {
            text-align: center;
        }
        #req-msg,#thank-msg {
            font-size: 1.5em;
            font-weight: bold;
        }
        #plus-msg {
            font-size: 2.2em;
            font-weight: bold;
        }
        #thank-plus-one-msg {
            display: none;
            text-align: center;
            margin-top: 4em;
        }
        #customPlusWrap {
            opacity: 0;
            position: absolute;
            left: 14px;
            top: 8px;
            text-align: center;
        }
        #plusoneWrap {
            position: relative;
            margin-top: 3em;
        }
        #dummyWrap {
            text-align: center;
        }
        #link-to-play-store {
            color: #FFFFFF;
            text-decoration: none;
            font-size: 1.2em;
            font-weight: bold;
            border: 1px solid #FFFFFF;
            display: block;
            padding: 10px;
            margin-top: 3em;
        }
    </style>
    <script type="text/javascript">
    // This callback function will be called after the user hits +1 button with JSON response that holds the url of the page liked and state(on or off).
    function userActionCB(response) {
        var requestMsg = document.getElementById('request-plus-one-msg'),
            thankMsg = document.getElementById('thank-plus-one-msg'),
            customWrap = document.getElementById('customPlusWrap'),
            dummyWrap = document.getElementById('dummyWrap');

        // if the user has +1'd the app, display thanks message
        if (response.state === 'on') {
            // track user +1 event
            track('plusOneAndroidApp');
            requestMsg.style.display = 'none';
            thankMsg.style.display = 'block';
            customWrap.style.opacity = 1;
            dummyWrap.style.display = "none";
            customWrap.style.position = "static";

        } else {  // else display message requesting him to +1 freecharge app
            requestMsg.style.display = 'block';
            thankMsg.style.display = 'none';
            customWrap.style.opacity = 0;
            dummyWrap.style.display = "block";
            customWrap.style.position = "absolute";
        }
    }

    function track(eventName) {
        var httpReq;
        
        if(window.XMLHttpRequest) {
            httpReq = new XMLHttpRequest();
            httpReq.open('GET', $('#freechargeApp').attr('data-trackerVisitApi'));
            httpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');    
            httpReq.send('eventName=' + eventName);
        }
    }

    //position custom gplus button on top of google's default button.
    function positionButton() {
        document.getElementById('customPlusWrap').style.left = (document.getElementById('dummyImg').offsetLeft + 16) + 'px';
    }
    </script>
</head>
<body onload="positionButton();">
    <!-- Header -->
    <header>
    <!-- Logo -->
        <img src="../../content/images/m/logo-large.png" alt="freecharge.in">
    </header>
        
    <!-- Content -->
    <div id="content">
      <!-- Request to +1 app, message -->
      <div id="request-plus-one-msg">
          <div id="req-msg">If you like our app give us a </div>
          <div id="plus-msg">+1</div>
      </div>
      
      <div id="plusoneWrap">
          <!-- +1 button -->
          <div id="customPlusWrap">
              <div class="g-plusone" data-width="120" data-size="standard" data-annotation="inline" data-callback="userActionCB"></div>
          </div>

          <div id="dummyWrap">
              <img id="dummyImg" src="../../content/images/m/gbutton.gif" alt="Plus one freecharge App">
          </div>
      </div>

      <!-- Thank for +1 app, message -->
      <div id="thank-plus-one-msg">
          <h3 id="thank-msg">Thanks for giving us a +1 <br> You can also rate our app on the play store</h3>
          <a id="link-to-play-store" href="https://play.google.com/store/apps/details?id=com.freecharge.android&referrer=utm_source%3Dmobilesite%26utm_medium%3Dpushnotification%26utm_campaign%3Dplusonerating" target="_blank" onclick="track('rateOnPlayStore')">Take me to play store</a>
      </div>
    </div>    
    
    <script type="text/javascript">
        window.___gcfg = {
            lang: 'en-US',
            parsetags: 'onload'
        };
        (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
</script>
</body>
</html>