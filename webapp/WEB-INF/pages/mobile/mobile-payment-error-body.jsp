<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div data-role="collapsible-set" data-theme="c" data-content-theme="d">
	<spring:message code="m.error.occured.in.payment" text="#m.error.occured.in.payment#"/>
</div>
