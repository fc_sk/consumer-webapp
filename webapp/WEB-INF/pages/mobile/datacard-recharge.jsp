<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="rememberLabel" scope="request"><spring:message code="m.remember.label" text="#m.remember.label#"/></c:set>

<div id="datacard-recharge" class="recharge" data-product="Data Card">
    <h2 class="page-title"><span>01</span><spring:message code="m.datacard.recharge.label" text="#m.datacard.recharge.label#"/></h2>

    <form:form commandName="dataCardRecharge"
		action="/m/do-datacard-recharge" method="POST"
		name="datacardForm" id="datacardForm" class="form" data-ajax="false">

    <div class="form-wrap">
        <div class="field-unit" id="datacardNumberWrap">
			<label for="datacardNumber"><spring:message code="m.datacard.number" text="#m.datacard.number#"/></label>
			<form:input path="datacardNumber" type="tel" maxlength="10" cssClass="input-field" />
			
            <c:choose>
                <c:when test="${retryBlock eq true}">
                  <p class="error">Please try recharging this number after 10 minutes.</p>
                </c:when>
                
                <c:when test="${denominationFailure eq true}">
                  <p class="error">You have entered an invalid denomination, please check for valid denominations.</p>
                </c:when>
                
                <c:when test="${permanentFailure eq true}">
                  <p class="error">Please select the right operator or choose different service (Prepaid, DTH, or Postpaid)</p>
                </c:when>
                
                <c:when test="${operatorFailure eq true}">
                  <p class="error">We are observing a high failure rate for this operator in the last few minutes, please try after some time</p>
                </c:when>
                
                <c:otherwise></c:otherwise>
            </c:choose>
            
            <p class="error" style="display: none"></p>
            <form:errors path="datacardNumber" element="div" cssClass="error"></form:errors>
		</div>

        <div class="field-unit field-unit-crippled operator-wrap" id="datacardOperatorWrap">
            <label for="datacardOperator"><spring:message code="m.datacard.operator" text="#m.datacard.operator#"/></label>
            <form:select path="datacardOperator" cssClass="input-field">
                <form:option value=""><spring:message code="m.select.operator" text="#m.select.operator#"/></form:option>
                <form:options
                        items="${dataCardRecharge.masterData.dataCardOperators}"
                        itemLabel="name" itemValue="operatorMasterId" />
            </form:select>
            <p class="error" style="display: none"></p>
            <form:errors path="datacardOperator" element="div" cssClass="error"></form:errors>
        </div>

        <div class="field-unit field-unit-crippled circle-wrap" id="datacardCircleWrap">
            <label for="datacardCircle"><spring:message code="m.datacard.circle" text="#m.datacard.circle#"/></label>
            <form:select path="datacardCircle" cssClass="input-field">
                <form:option value=""><spring:message code="m.select.circle" text="#m.select.circle#"/></form:option>
                <form:options items="${dataCardRecharge.masterData.mobileCircles}"
                              itemLabel="name" itemValue="circleId" />
            </form:select>
            <p class="error" style="display: none"></p>
            <form:errors path="datacardCircle" element="div" cssClass="error"></form:errors>
        </div>

        <div class="field-unit field-unit-stroke" id="datacardAmountWrap">
            <label for="datacardAmount"><spring:message code="m.datacard.recharge.amount" text="#m.datacard.recharge.amount#"/></label>
            <form:input path="datacardAmount" type="tel" maxlength="4" cssClass="input-field" />
            <p class="error" style="display: none"></p>
            <form:errors path="datacardAmount" element="div" cssClass="error"></form:errors>
        </div>
    </div>

		<div class="field-unit-button">
            <button id="datacard-recharge-submit" class="btn btn-primary full-width"><spring:message code="m.continue.label" text="#m.continue.label#"/></button>
        </div>
	</form:form>

</div>
