 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 
 <%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>

 <c:set var="status" scope="request">"${status}"</c:set>

 <div class="successmsg" id="passwordRecoverySuccess">
  <p><spring:message code="m.password.reset.successfully" text="#m.password.reset.successfully#"/></p>
 </div>

 <script>
     $(document).ready(function(){
         if(status == "success"){
             $('#passwordRecoverySuccess').show();
         }
     });

 </script>
