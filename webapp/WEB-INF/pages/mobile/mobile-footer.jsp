<%@page import="com.freecharge.common.util.FCConstants"%>
<%@page import="com.freecharge.mobile.constant.MobileConstants"%>
<%@page import="com.freecharge.mobile.web.util.MobileUtil"%>
<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<footer class="footer">
    <ul class="links">
        <li class="link customer-care-link">
        	<span class='customer-care-label'>Customer care : </span>
        	<span class='customer-care-email'><a href="mailto:<spring:message code="m.support.id" text="#m.support.id#"/>"><spring:message code="m.support.id" text="#m.support.id#"/></a></span>
        </li>
        <%if(!MobileUtil.isMobileAppChannel(request)){ %>
        <li class="link desktop-site-link">
        	<a href="/?desktop=true" rel="external" id="deskTopVersionLink"><spring:message code="m.desktop.version.label" text="#m.desktop.version.label#"/> &#187</a>
        </li>
        <%}%>
    </ul>
    <p class="copyright">&copy; <spring:message code="m.company.name" text="#m.company.name#"/></p>
    <%-- <img src="<%= GoogleAnalyticsUtil.googleAnalyticsGetImageUrl(request) %>"  height="1" width="1"/> --%>
</footer>