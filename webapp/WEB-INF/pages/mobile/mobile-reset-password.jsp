<%@page import="com.freecharge.mobile.web.util.GoogleAnalyticsUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="auth">
	<h2 class="page-title"><spring:message code="m.create.new.pasw" text="#m.create.new.pasw#"/></h2>

    <div class="form-wrap">
        <p><spring:message code="m.will.email.link.msg" text="#m.will.email.link.msg#"/>.</p>

        <form:form commandName="passwordRecoveryEmail"
            action="/m/mobile-changepassword" method="POST" name="passwordRecoveryForm"
            id="passwordRecoveryForm" class="form">

            <div class="field-unit">
                <label for="password-recovery-email"><spring:message code="m.email.address" text="#m.email.address#"/></label>
                <form:input path="email" id="password-recovery-email" type="email" maxlength="50" cssClass="input-field"/>
                <p class="error"><c:out value="${errorMsg}"/></p>
            </div>

            <div>
               <button id="password-reset-submit" type="submit" class="btn btn-primary full-width"><spring:message code="m.send.link" text="#m.send.link#"/></button>
            </div>
        </form:form>

    </div>
</div>
