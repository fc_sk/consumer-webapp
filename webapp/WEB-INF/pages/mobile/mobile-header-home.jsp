<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeContextDirectory" %>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ page import ="org.apache.commons.lang.WordUtils" %>
<%@ page import="java.util.Map" %>
<%@page import="com.freecharge.mobile.constant.MobileURLConstants"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
    Object login=false;
    Object emailId="";
    String firstName="";
    if(fs!=null){
        Map<String,Object> sessionData=fs.getSessionData();
        if(sessionData!=null)
            login=sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
        emailId=sessionData.get(WebConstants.SESSION_USER_EMAIL_ID);
        firstName = (String)sessionData.get(WebConstants.SESSION_USER_FIRSTNAME);
        if(firstName!=null && !firstName.isEmpty())
            firstName = WordUtils.capitalize(firstName);
    }
%>
<c:set  value="<%=login%>" var="login" />
<header class="header header-home">

    <c:if test="${mobileLogin == null && user == null}">
        <c:choose>
            <c:when test="${!login}">
                <a class="header-link" href="/m/login"><img src="${mt:keyValue("imgprefix1")}/images/m/icon-login.png" alt="Login"></a>
            </c:when>
            <c:otherwise>
                <a class="header-link" href="/m/myaccount"><img src="${mt:keyValue("imgprefix1")}/images/m/icon-account.png" alt="My Account"></a>
            </c:otherwise>
        </c:choose>
    </c:if>
</header>