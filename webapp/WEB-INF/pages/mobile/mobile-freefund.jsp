<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>


<div class="account">
    <h2 class="page-title">FreeFund code</h2>
    <div id="mobileFreefundApplyHolder" class="wrapper freefund">

        <p class="helper-text">What's that? FreeFund code is like a gift voucher code with a fixed amount that can be deposited in Credits.</p>
        <div class="field-unit">
            <label>Enter your FreeFund code</label>
            <input type="text" value="" name="freefundCode"  id="mobileFreefundCodeId" class="input-field" />
            <br/><br/> 
            <input id="is-recaptcha-enabled" type="hidden" value="${forceCaptcha}" />
            <div id="captcha-placement-td"  style="<c:if test="${not forceCaptcha}"> display:none; </c:if>" >
    			<div id="captcha-placement-id">
	 	    		<div id="captcha-draw-id" style="border: 1px dotted #dddddd;">
	 				  <c:if test="${forceCaptcha}"> <jsp:include page="/WEB-INF/views/captcha-draw.jsp"></jsp:include> </c:if>
	 				</div> 
	 				<a href="javascript:void(0)" id="redraw-captcha-button" >Refresh</a>
	 				<br/>
		    		<input name="captcha" type="text" autocomplete="off" placeholder="Please enter the words shown in image"  class="englishOnlyField required" />
	    		</div>
    		</div>
            <div id="mobileFreefundApplyMsgHolder" style="display:none;"></div>
        </div>
        <div class="field-unit">
            <button id="addMobileFreefund" class="btn btn-primary full-width" data-lookup-id="${param.lid}">
                <spring:message code="button.redeem"/>
            </button>
        </div>
    </div>
</div>
