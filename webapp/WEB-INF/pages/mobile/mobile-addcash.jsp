<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.freecharge.common.util.FCConstants"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="account">
    <h2 class="page-title">Add Cash to Credits</h2>
     <div class="wrapper">
        <div id="addCashToWallet" class="add-cash">
            <p class="helper-text">Why? Depositing money in Credits will speed up recharging.</p>
            <form:form id="addCashForm" action="/m/addcashproducttocart" method="post" modelAttribute="addCashWebDo" cssClass="form">
                <c:choose>
                    <c:when test="${errorMessage!=null}">
                        <p class="message-success" id="passwordupmsg"><c:out value="${errorMessage}"/></p>
                    </c:when>
                </c:choose>
                <div class="field-unit">
                    <label>Enter Amount</label>
                    <form:input path="addCashAmount" type="tel" cssClass="input-field" maxlength="4" />
                    <p class="error" style="display: none"></p>
                </div>
                <div class="field-unit">
                    <button type="submit" id="add-cash-button" class="btn btn-primary full-width">Deposit</button>
                </div>
            </form:form>
        </div>
    </div>
</div>