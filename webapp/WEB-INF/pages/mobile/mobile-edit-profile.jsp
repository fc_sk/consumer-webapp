<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class="account">
    <h2 class="page-title">Edit profile</h2>
    <div class="form-wrap">
        <c:choose>
            <c:when test="${updprofilestatus}">
                <div class="message-success mobile-dialog" id="profileupmsg">
                    <p class='mobile-dialog-text'><c:out value="${message}"/></p>
                </div>
            </c:when>
            <c:otherwise>
                <div class="message-failure mobile-dialog" id="profileerrormsg">
                    <p class='mobile-dialog-text'><c:out value="${errmessage}"/></p>
                </div>
            </c:otherwise>
        </c:choose>

        <form:form action="/m/mobile-updateprofile" class="form" method="post" name="profileupdateform" id="profileupdateform" commandName="editProfile" modelAttribute="editProfile" cssClass="form">
            <div class="field-unit first-field" >
                <label><spring:message code="email" text="#email#"></spring:message></label>
                <input type="text" class="input-field" value="<c:out value='${editProfile.email}'/>" disabled="disabled">
                <spring:bind path="editProfile.email">
                    <form:input id="email" path="email" type="hidden" readonly="readonly"/>
                </spring:bind>
            </div>

            <div class="field-unit">
                <label>Title</label>
                <spring:bind path="editProfile.title">
                    <form:select path="title" cssClass="input-field input-field-select-small">
                        <c:forEach var='title' items='${usertitle}'>
                            <c:choose>
                                <c:when test="${title eq editProfile.title}">
                                    <option value="${title}" selected="selected" >${title}.</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${title}">${title}.</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </form:select>
                </spring:bind>
            </div>

            <div class="field-unit" >
                <label><spring:message code="name" text="#name#"/></label>
                <spring:bind path="editProfile.name">
                    <form:input id="name" path="name" cssClass="input-field" maxlength="25" />
                </spring:bind>
                <p class="error" style="display: none"></p>
                <form:errors path="name" cssClass="error" element="div"></form:errors>
            </div>

            <div class="field-unit">
                <label><spring:message code="mobile" text="#mobile#"/></label>
                <spring:bind path="editProfile.mobileNo">
                    <form:input id="mobileNo" path="mobileNo" maxlength='10' cssClass="input-field"/>
                </spring:bind>
                <p class="error" style="display: none"></p>
                <form:errors path="mobileNo" cssClass="error" element="div"></form:errors>
            </div>

            <div class="field-unit">
                <button id="mobile-profile-edit" class="btn btn-primary full-width"><spring:message code="m.svae.changes" text="#m.svae.changes#"/></button>
            </div>
        </form:form>
    </div>
</div>
    
