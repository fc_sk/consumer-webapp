<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--Google analytics ecommerce for mobile site--%>
<c:choose>
    <%--for thank you page - adword code + ga ecommerce --%>
    <c:when test="${finalResultSet}">
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push([ '_setAccount', 'UA-12939857-1' ]);
	_gaq.push([ '_trackPageview']);
	_gaq.push([ '_addTrans', '${checkoutData.orderId}', // order ID - required
	'Freecharge', // affiliation or store name
	'${checkoutData.totalAmount}', // total - required
	'0.00', // tax
	'${checkoutData.orderDetails.shippingCharges}', // shipping
	'${checkoutData.deliveryCity}', // city
	'${checkoutData.deliveryState}', // state or province
	'${checkoutData.deliveryCountry}' // country
	]);
	// add item might be called for every item in the shopping cart
	// where your ecommerce engine loops through each item in the cart and
	// prints out _addItem for each
	<c:forEach var="item" items="${checkoutData.orderDetails.cartItemList}">
	_gaq.push([ '_addItem', '${checkoutData.orderId}', // '1234',           // order ID - required
	'${item.stockKeepingUnit}', // 'DD44',           // SKU/code - required
	'${item.productName}', // 'T-Shirt',        // product name
	'${item.displayLabel}', // 'Green Medium',  // category or variation
	'${item.unitPrice}', // unit price - required
	'${item.quantity}' // quantity - required
	]);
	</c:forEach>
	_gaq.push([ '_trackTrans' ]); //submits transaction to the Analytics servers
	(function() {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();
</script>
</c:when>
  <c:otherwise>
        <%--normal GA code--%>
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-12939857-1']);
            _gaq.push(['_setDomainName', 'freecharge.in']);
            _gaq.push(['_trackPageview']);
            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
    </c:otherwise>
</c:choose>