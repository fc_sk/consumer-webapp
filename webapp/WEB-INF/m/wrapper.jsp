<!DOCTYPE html>
<html lang="en">

<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page import="com.freecharge.common.util.ABTestingUtil" %>
<%@page import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<%@page import="com.freecharge.common.framework.session.FreechargeSession"%>
<%@ page import="java.util.Map" %>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ page import="com.freecharge.web.webdo.CommonSessionWebDo" %>
<%@ page import="com.freecharge.tracker.TrackerUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import ="org.apache.commons.lang.WordUtils" %>
<%
    FreechargeSession freechargesession = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    String name = "";
    String email = "";
    if (freechargesession != null) {
        Map<String, Object> sessionData = freechargesession.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
        name = (String) sessionData.get(WebConstants.SESSION_USER_FIRSTNAME);
        if (name != null && !name.isEmpty())
            name = WordUtils.capitalize(name);
        email = (String) sessionData.get(WebConstants.SESSION_USER_EMAIL_ID);
    }

    String lookupid = request.getParameter("lookupID");
    String lid = request.getParameter("lid");
    if (lookupid == null && lid != null) {
        lookupid = lid;
    }
    String productType = "";
    productType = FCSessionUtil.getRechargeType(lookupid, freechargesession);
    Integer productId = FCConstants.productMasterMapReverse.get(productType);

    String gosf = request.getParameter("gosf");
    String ext = request.getParameter("ext");
    String extType = request.getParameter("extType");
    String extName = request.getParameter("extName");
    String extHeading = request.getParameter("extHeading");
    String extDesc = request.getParameter("extDesc");
%>

<c:set  value="<%=login%>" var="login" />
<c:set  value="<%=name%>" var="name" />
<c:set  value="<%=email%>" var="email" />
<c:set  value="<%=lookupid%>" var="lookupid" />
<c:set  value="${orderId}" var="orderId" scope="request"/>
<c:set  value="<%=ext%>" var="ext" />
<c:set  value="<%=extName%>" var="extName" />
<c:set  value="<%=extType%>" var="extType" />
<c:set  value="<%=extHeading%>" var="extHeading" />
<c:set  value="<%=extDesc%>" var="extDesc" />

<c:choose >
    <c:when test="${seoAttributes.productId != null}" >
        <c:set var="displayProductId" value="${seoAttributes.productId}" />
        <c:set var="displayTabOperatorName" value="${seoAttributes.operatorName}" />
        <c:set var="displayTabOperatorId" value="${seoAttributes.operatorId}" />
    </c:when>
    <c:otherwise>
        <c:set var="displayProductId" value="${homeWebDo.tabId}" />
        <c:set var="displayTabOperatorName" value="${homeWebDo.displayOperatorName}" />
    </c:otherwise>
</c:choose>


<head>
    <%--<title>${seoAttributes.pageTitle}</title>--%>
    <title>FREE Online Recharge - Prepaid Mobile, DTH &amp; Data Card Recharge</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <%--<meta name="viewport" content="width=device-width, initial-scale=1.0">--%>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
    <meta name="msapplication-tap-highlight" content="no"/>
    <meta name="fragment" content="!">
    <meta name="keywords" content="${seoAttributes.metaKeywords}"/>
    <meta name="description" content="${seoAttributes.metaDescription}"/>
    <link rel="shortcut icon" href="${mt:keyValue("imgprefix2")}/images/favicon-new.ico?v=${mt:keyValue("version.no")}"/>


    <!-- Google Analytics Content Experiment code
    <script>function utmx_section(){}function utmx(){}(function(){var
    k='71845873-31',d=document,l=d.location,c=d.cookie;
    if(l.search.indexOf('utm_expid='+k)>0)return;
    function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
    indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
    length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
    '<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
    '://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
    '&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
    valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
    '" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
    </script><script>utmx('url','A/B');</script>-->
    <!-- End of Google Analytics Content Experiment code -->

    <link rel="canonical" href="https://www.freecharge.in"/>

    <link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/m/dist/all.css?v=${mt:keyValue("version.no")}" />
    <link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/lib/jquery.mmenu.css?v=${mt:keyValue("version.no")}" />
    <link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/lib/jquery.mmenu.positioning.css?v=${mt:keyValue("version.no")}" />
</head>

<body>
    
    <div id="main-nav">
        <div id="scroller">
            <ul>
                <div id="mainNavClose"></div>
                <li class="drawer-profile">
                    <h4 id="profileName"></h4>
                    <span id="profileEmail"></span>
                </li>
                <li>
                    <a href="#!/prepaid" class="icon-prepaid">
                        <span>Mobile</span>
                    </a>
                </li>
                <li>
                    <a href="#!/dth" class="icon-dth">
                        <span>dth</span>
                    </a>
                </li>
                <li>
                    <a href="#!/datacard" class="icon-datacard">
                        <span>data card</span>
                    </a>
                </li>
                <!--<li>
                    <a href="#!/bills" class="icon-orders">
                        <span>bills</span>
                    </a>
                </li> -->
<!--                 <li class="main-delights">
                    <a href="#!/delights" class="icon-credits">
                        <span>Delights</span>
                    </a>
                </li> -->
                <li class="account-lbl"> My Account </li>
                 <li>
                    <a href="#!/credits" class="icon-credits">
                        <span>FC Credits</span>
                        <span class="credits-badge">&#8377;0</span>
                    </a>
                </li>
                <li>
                    <a href="#!/transactionhistory" class="icon-orders">
                        <span>My Orders</span>
                    </a>
                </li>
                <li>
                    <a href="#!/mycoupons" class="icon-mycoupons">
                        <span>My Coupons</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)" id="logoutBtn" class="icon-logout">
                        <span>Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <nav id="cart-nav" class="">
    <div id="cart-wrapper" class="">
        
        
        <ul id="selectedCoupons">
            <li class="clearfix cart-drawer-header">
                ORDER SUMMARY
                <a href="javascript:void(0);" id="cartClose"></a>
            </li>
            <li class="cart-info">
                <a href="javascript:void(0);">
                    <div class="cart-summary clearfix">
                        <div class="number">
                            <h1 id="cart-service-num"></h1>
                            <p id="cart-operator"></p>
                        </div>
                        <div class="amount">
                            <h1 class="rupee"><span id="cart-amount"></span></h1>
                            <p>Amount</p>
                        </div>
                    </div>
                </a>
            </li>

            <div class="cart-label">Coupons picked</div>
            <div class="no-coupon">You have not picked any coupons</div>
            <div class="coupons-total">
                <p>Total Order</p>
                <span class="cart-total" id="cart-total">0</span>
            </div>
        </ul>
            
    </div>
</nav>

<noscript>
    <div class="no-js">
        <p>JavaScript is disabled in your browser! <br/>FreeCharge requires javascript.</p>
    </div>
</noscript>
<%-- Show build information like branch, mode etc --%>
<jsp:include page="../views/developer/build-information.jsp" />
<div class="wrapper" id="freechargeApp"
     data-release-token='${releaseType}'
     data-facebook-login-enabled='${facebookLoginEnabled}'
     data-google-login-enabled='${googleLoginEnabled}'
     data-google-client-id='${googleClientID}'
     data-google-redirect-uri='${googleRedirectURI}'
     data-csrf-token='${mt:csrfToken()}'
     data-img-prefix1='${mt:keyValue("imgprefix1")}'
     data-img-prefix2='${mt:keyValue("imgprefix2")}'
     data-version = '${mt:keyValue("version.no")}'
     data-template-prefix = '${mt:keyValue("templateprefix")}'
     data-login-status='${login}'
     data-name='<c:out value="${name}"/>'
     data-email='${email}'
     data-lookup-id='${lookupid}'
     data-order-id='${orderId}'
     data-ext='${ext}'
     data-ext-type='${extType}'
     data-ext-name='${extName}'
     data-ext-heading='${extHeading}'
     data-ext-desc='${extDesc}'
     data-product-id='${displayProductId}'
     data-operator-name="${displayTabOperatorName}"
     data-operator-id="${displayTabOperatorId}"
     data-redirect="false"
     data-sky-url='${mt:keyValue("sky.server.url")}'
     data-sky-db-enabled='${skyDbEnabled}'
     data-is-rupay-enabled='${isRupayEnabled}'
     data-finger-printing-enabled='${fingerPrintingEnabled}'>


    <a href="javascript:void(0);" class="cart-link hidden">
        <span class="items-in-cart">
            <div class="items-count">
                <div class="t"></div>
                <div class="m">0</div>
                <div class="b"></div>
            </div>
        </span>
    </a>
    <header class="header">

        <a href="javascript:void(0)" class="menu-link"></a>
        <div id="backBtn"></div>
        <div class="header-wrapper">
            <a href="javascript:void(0)" class="login-btn">LOGIN</a>
            <a class="logo" href="/">
                <!-- <img width="160" height="36" src="${mt:keyValue("imgprefix1")}/images/fc-mobile-logo.png" alt="freecharge.in"> -->
                <span class="img"></span>
                <div class="catchphrase">the<span class="img"></span>recharge experience</div>
            </a>
            <div class="turbo-phrase" hidden>Turbocharge your transaction</div>

        </div>
        <div id="pageHeader">FREECHARGE</div>
        <nav id="rechargeNav">
            <ul class="clearfix">
                <li class="prepaid active">
                    <a href="javascript:void(0)" data-link="#!/prepaid">
                        <span>Mobile</span>
                    </a>
                </li>
                <li class="dth">
                    <a href="javascript:void(0)" data-link="#!/dth">
                        <span>dth</span>
                    </a>
                </li>
                <li class="datacard">
                    <a href="javascript:void(0)" data-link="#!/datacard">
                        <span>datacard</span>
                    </a>
                </li>
                <!--<li class="bills">
                    <a href="javascript:void(0)" data-link="#!/bills">
                        <span>bills</span>
                    </a>
                </li>-->
<!--                 <li class="delights">
                    <a href="javascript:void(0)" data-link="#!/delights">
                        <span>delights</span>
                    </a>
                </li> -->
            </ul>
            <span class="arrow"></span>
        </nav>
    </header>
     <div class="notification" id="genericError"> <span class="msg"> Error Message </span> <span class="close">x</span> </div>
     <div class="loggedout-message"> You have been successfully logged out </div>

    <div id="appcontainer">
        <ul></ul>
    </div>
    <div id="loadMask" class="throbber">
        <span></span>
        <span></span>
        <span></span>
    </div>
    <footer>
        &copy; Accelyst Solutions Pvt. Ltd. All Rights Reserved.
    </footer>
</div>

<script type="text/javascript">
    window.DTHOperators = '{"operators":[{"operatorMasterId":4,"operatorCode":"Dish Tv","name":"Dish TV","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":2,"imgUrl":"images/circular-operator-logos/dish.png"},{"operatorMasterId":16,"operatorCode":"Big Tv","name":"Reliance Digital TV","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":2,"imgUrl":"images/circular-operator-logos/big_tv.png"},{"operatorMasterId":17,"operatorCode":"Airtel Tv","name":"Airtel Digital TV","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":2,"imgUrl":"images/circular-operator-logos/airtel-digital.png"},{"operatorMasterId":18,"operatorCode":"Sun Tv","name":"Sun Direct","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":2,"imgUrl":"images/circular-operator-logos/sun.png"},{"operatorMasterId":19,"operatorCode":"Tata Sky","name":"Tata Sky","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":2,"imgUrl":"images/circular-operator-logos/sky.png"},{"operatorMasterId":20,"operatorCode":"Videocon Tv","name":"Videocon D2H","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":2,"imgUrl":"images/circular-operator-logos/videocon-d2h.png"}]}';
    window.MobileOperators = '{"operators":[{"operatorMasterId":1,"operatorCode":"Aircel","name":"Aircel","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/aircel.png"},{"operatorMasterId":2,"operatorCode":"Airtel","name":"Airtel","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/airtel.png"},{"operatorMasterId":3,"operatorCode":"BSNL","name":"BSNL","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/bsnl.png"},{"operatorMasterId":5,"operatorCode":"Tata Docomo GSM","name":"Tata Docomo GSM","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/docomo-gsm.png"},{"operatorMasterId":6,"operatorCode":"Idea","name":"Idea","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/idea.png"},{"operatorMasterId":7,"operatorCode":"Indicom-Walky","name":"Indicom Walky","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/indicom.png"},{"operatorMasterId":9,"operatorCode":"MTNL Delhi","name":"MTNL Delhi","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/mtnl.png"},{"operatorMasterId":10,"operatorCode":"Reliance-CDMA","name":"Reliance CDMA","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/reliance-cdma.png"},{"operatorMasterId":11,"operatorCode":"Reliance-GSM","name":"Reliance GSM","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/reliance-gsm.png"},{"operatorMasterId":12,"operatorCode":"Tata Indicom","name":"Tata Indicom","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/indicom.png"},{"operatorMasterId":13,"operatorCode":"Uninor","name":"Uninor","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/uninor.png"},{"operatorMasterId":14,"operatorCode":"Vodafone","name":"Vodafone","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/vodafone.png"},{"operatorMasterId":22,"operatorCode":"MTS","name":"MTS","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/mts.png"},{"operatorMasterId":24,"operatorCode":"Videocon Mobile","name":"Videocon Mobile","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/videocon.png"},{"operatorMasterId":30,"operatorCode":"Virgin GSM","name":"Virgin GSM","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/virgin-gsm.png"},{"operatorMasterId":31,"operatorCode":"Virgin CDMA","name":"Virgin CDMA","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/virgin-cdma.png"},{"operatorMasterId":42,"operatorCode":"Tata Docomo CDMA","name":"Tata Docomo CDMA","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/docomo-cdma.png"},{"operatorMasterId":43,"operatorCode":"T24","name":"T24","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/t24.png"},{"operatorMasterId":54,"operatorCode":"MTNL Mumbai","name":"MTNL Mumbai","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":1,"imgUrl":"images/circular-operator-logos/mtnl.png"}]}';
    window.DataCardOperators = '{"operators":[{"operatorMasterId":21,"operatorCode":"Tata Photon Plus","name":"Tata Photon Plus","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/photon.png"},{"operatorMasterId":23,"operatorCode":"Reliance Netconnect","name":"Reliance Netconnect","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/netconnect.png"},{"operatorMasterId":40,"operatorCode":"MTS MBrowse","name":"MTS MBrowse","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/mts.png"},{"operatorMasterId":41,"operatorCode":"MTS Mblaze","name":"MTS MBlaze","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/mts.png"},{"operatorMasterId":44,"operatorCode":"Airtel DataCard","name":"Airtel","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/airtel.png"},{"operatorMasterId":45,"operatorCode":"BSNL DataCard","name":"BSNL","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/bsnl.png"},{"operatorMasterId":46,"operatorCode":"MTNL DataCard Delhi","name":"MTNL Delhi","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/mtnl.png"},{"operatorMasterId":47,"operatorCode":"Vodafone DataCard","name":"Vodafone","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/vodafone.png"},{"operatorMasterId":48,"operatorCode":"Aircel DataCard","name":"Aircel","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/aircel.png"},{"operatorMasterId":49,"operatorCode":"Idea DataCard","name":"Idea","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/idea.png"},{"operatorMasterId":50,"operatorCode":"Reliance-GSM DataCard","name":"Reliance-GSM","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/reliance-gsm.png"},{"operatorMasterId":51,"operatorCode":"Tata Photon Whiz DataCard","name":"Tata Photon Whiz","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/photon.png"},{"operatorMasterId":52,"operatorCode":"Tata Docomo GSM DataCard","name":"Tata Docomo GSM","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/docomo-gsm.png"},{"operatorMasterId":53,"operatorCode":"T24 DataCard","name":"T24","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/t24.png"},{"operatorMasterId":55,"operatorCode":"MTNL Datacard Mumbai","name":"MTNL Mumbai","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":3,"imgUrl":"images/circular-operator-logos/mtnl.png"}]}';
    window.circles = '{"circles":[{"circleMasterId":1,"name":"Andhra Pradesh","isActive":true,"fkCountryMasterId":1},{"circleMasterId":2,"name":"Assam","isActive":true,"fkCountryMasterId":1},{"circleMasterId":3,"name":"Bihar","isActive":true,"fkCountryMasterId":1},{"circleMasterId":5,"name":"Chennai","isActive":true,"fkCountryMasterId":1},{"circleMasterId":6,"name":"Delhi","isActive":true,"fkCountryMasterId":1},{"circleMasterId":7,"name":"Gujarat","isActive":true,"fkCountryMasterId":1},{"circleMasterId":8,"name":"Haryana","isActive":true,"fkCountryMasterId":1},{"circleMasterId":9,"name":"Himachal Pradesh","isActive":true,"fkCountryMasterId":1},{"circleMasterId":10,"name":"Karnataka","isActive":true,"fkCountryMasterId":1},{"circleMasterId":11,"name":"Kerala","isActive":true,"fkCountryMasterId":1},{"circleMasterId":12,"name":"Kolkata","isActive":true,"fkCountryMasterId":1},{"circleMasterId":13,"name":"Madhya Pradesh","isActive":true,"fkCountryMasterId":1},{"circleMasterId":14,"name":"Maharashtra","isActive":true,"fkCountryMasterId":1},{"circleMasterId":15,"name":"Mumbai","isActive":true,"fkCountryMasterId":1},{"circleMasterId":16,"name":"North East","isActive":true,"fkCountryMasterId":1},{"circleMasterId":17,"name":"Orissa","isActive":true,"fkCountryMasterId":1},{"circleMasterId":18,"name":"Punjab","isActive":true,"fkCountryMasterId":1},{"circleMasterId":19,"name":"Rajasthan","isActive":true,"fkCountryMasterId":1},{"circleMasterId":20,"name":"Tamil Nadu","isActive":true,"fkCountryMasterId":1},{"circleMasterId":21,"name":"Uttar Pradesh (E)","isActive":true,"fkCountryMasterId":1},{"circleMasterId":22,"name":"Uttar Pradesh (W)","isActive":true,"fkCountryMasterId":1},{"circleMasterId":23,"name":"West Bengal","isActive":true,"fkCountryMasterId":1},{"circleMasterId":33,"name":"ALL","isActive":true,"fkCountryMasterId":1},{"circleMasterId":34,"name":"JK","isActive":true,"fkCountryMasterId":1},{"circleMasterId":35,"name":"UTTARANCHAL","isActive":true,"fkCountryMasterId":1}]}';

    window.MobilePostpaidOperators = '{"operators":[{"operatorMasterId":56,"operatorCode":"Airtel Postpaid","name":"Airtel Postpaid","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":201,"imgUrl":"images/circular-operator-logos/airtel.png"},{"operatorMasterId":58,"operatorCode":"Bsnl Postpaid","name":"Bsnl Postpaid","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":201,"imgUrl":"images/circular-operator-logos/bsnl.png"},{"operatorMasterId":59,"operatorCode":"Docomo Postpaid-GSM","name":"Docomo Postpaid-GSM","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":201,"imgUrl":"images/circular-operator-logos/docomo-gsm.png"},{"operatorMasterId":60,"operatorCode":"Idea Postpaid","name":"Idea Postpaid","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":201,"imgUrl":"images/circular-operator-logos/idea.png"},{"operatorMasterId":61,"operatorCode":"Docomo Postpaid-CDMA","name":"Docomo Postpaid-CDMA","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":201,"imgUrl":"images/circular-operator-logos/indicom.png"},{"operatorMasterId":62,"operatorCode":"Vodafone Postpaid","name":"Vodafone Postpaid","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":201,"imgUrl":"images/circular-operator-logos/vodafone.png"},{"operatorMasterId":63,"operatorCode":"Reliance Postpaid-CDMA","name":"Reliance Postpaid-CDMA","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":201,"imgUrl":"images/operator-logos/reliance_mobile.png"},{"operatorMasterId":64,"operatorCode":"Reliance Postpaid-GSM","name":"Reliance Postpaid-GSM","isActive":true,"fkCountryMasterId":1,"fkProductMasterId":201,"imgUrl":"images/operator-logos/reliance_mobile_gsm_service.png"}]}';
</script>
<c:choose >
    <c:when test = "${ mt:keyValue('system.ctx.mode') == 'prod' or mt:keyValue('system.ctx.mode') == 'preProd' or mt:keyValue('system.ctx.mode') == 'qa' or mt:keyValue('system.ctx.mode') == 'e2e' }" >
        <script src="${mt:keyValue("jsprefix2")}/m/dist/all.lib.min.js?v=${mt:keyValue("version.no")}"></script>
        <script src="${mt:keyValue("jsprefix2")}/m/dist/all.templates.min.js?v=${mt:keyValue("version.no")}"></script>
        <script src="${mt:keyValue("jsprefix2")}/m/dist/m.min.js?v=${mt:keyValue("version.no")}"></script>
    </c:when>
    <c:otherwise>
        <script src="${mt:keyValue("jsprefix2")}/lib/jquery.2.1.0.js"></script>
        <script src="${mt:keyValue("jsprefix2")}/lib/iscroll.js"></script>
        <script src="${mt:keyValue("jsprefix2")}/lib/parsley.custom.validation.js"></script>
        <script src="${mt:keyValue("jsprefix2")}/lib/handlebars.runtime-v1.3.0.js"></script>
        <script src="${mt:keyValue("jsprefix2")}/m/dist/all.templates.min.js?v=${mt:keyValue("version.no")}"></script>
        <script src="${mt:keyValue("jsprefix2")}/lib/require.js" data-main="${mt:keyValue("jsprefix2")}/m/m.js?v=${mt:keyValue("version.no")}"></script>
        <!-- <script src="${mt:keyValue("jsprefix2")}/m/dist/m.min.js?v=${mt:keyValue("version.no")}"></script> -->
    </c:otherwise>
</c:choose>
<input type="hidden" data-host-url="${mt:keyValue("jsprefix2")}" id="hostUrl">
<!-- Comment comment out below js for normal require fetch -->
<!-- <script src="${mt:keyValue("jsprefix2")}/lib/require.js" data-main="${mt:keyValue("jsprefix2")}/m/m.js?v=${mt:keyValue("version.no")}"></script> -->
<!-- Comment comment out below js for normal minified fetch -->
<script type="text/javascript">
    /*
    var cacheVersion = $('#freechargeApp').attr('data-version');

    (function(){
        var hostUrl      = $('#hostUrl').attr('data-host-url'),
            templatesUrl = hostUrl + "/m/dist/all.templates.min.js",
            scriptUrl    = hostUrl + "/m/dist/m.min.js?v=" + cacheVersion;

        scriptsinlocal.require('fctemplates',templatesUrl,cacheVersion,function(){
            $.getScript(scriptUrl);
        });
    })();
    */

</script>
<!-- <script src="${mt:keyValue("jsprefix2")}/m/dist/m.min.js"></script> -->

<script>
$( document ).ready( function( e )
{
    fcTrack({eventName: 'm_new_site_loaded', val0: ""});
    var hash = window.location.hash;
    var lid, fcChannel;
    if( hash.indexOf("paycomplete") != -1 && hash.indexOf("lid") != -1 )
    {

        lid = hash.split("lid=")[1].split("&")[0];
        window.paycompletelookupId = lid;
        window.location.hash = "#!/paymentresponse";
    }
    else
    {
        // window.location.hash = "#!/home";
        //window.location.reload();
    }
    $("#freechargeApp").attr("data-signin-trigger", false);
    $("#freechargeApp").attr("data-signin-recharge", false);

    var profileName = $("#freechargeApp").data("name");
    var profileEmail =  $("#freechargeApp").data("email");

    if( profileEmail !== "")
    {
        $("#profileName").text( profileName || profileEmail );
        $("#profileEmail").text( profileEmail );
    }

   var isVibrateSupported = "vibrate" in navigator;
   window.isVibrateSupported = isVibrateSupported;

   if( hash.indexOf("fcChannel") != -1)
   {
        fcChannel = hash.split("fcChannel")[1].split("=")[1].split("&")[0];
        console.log('Channel', fcChannel );
        window.fcChannel = fcChannel;
   }


// var cont = document.getElementById('wrapper');

// console.log(cont//cont.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
var tempScroll;

var cartScroll;

// document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);




// Hide Delights feature for IEMobile

// $('body').addClass('no-delights');

var userAgent = window.navigator.userAgent;

var browser = userAgent.indexOf('IEMobile'),
    engine = userAgent.indexOf('Trident');


if(browser >= 0 || engine >= 0){
    $('body').addClass('no-delights');
}

$('.menu-link').on('click', function(){
    // $('#main-nav-wrapper').removeClass('hide')
    // setTimeout(function(){
    var scroll = $(window).scrollTop();
    $('#main-nav').height($(window).height()+'px');
    $('#main-nav').css('top',scroll+'px');
    tempScroll = new IScroll('#main-nav', { 
        mouseWheel: true,
        click: true
    });
    $('#main-nav').addClass('show');
    // $('#mainNavClose').fadeIn(200);
    // },100);
});

$('#mainNavClose, #main-nav li a').on('click', function(){
   $('#main-nav').removeClass('show');
   setTimeout(function(){
    $('#main-nav').css('top', '0px');
   }, 300);
   // $('#mainNavClose').fadeOut(200);
});

window.addEventListener('orientationchange', function(){
    $('#main-nav').height($(window).height()+'px');
    $('#cart-nav').height($(window).height()+'px');
});

// $('#main-nav-wrapper').on('webkitTransitionEnd transitionend', function(){
//     // console.log(this);
//     if(!$(this).hasClass('show')){
//         $(this).addClass('hide');
//     }
// });


// $('#main-nav-wrapper').addClass('show');

// set height of cart drawer

// $('#main-nav-wrapper').height(window.screen.height+'px');


$('.cart-link').on('click', function(){
    // $('#cart-nav').removeClass('hide')
    // setTimeout(function(){
    $('#cart-nav').height($(window).height()+'px');
    var scroll = $(window).scrollTop();
    $('#cart-nav').css('top',scroll+'px');
    
    $('#cart-nav').addClass('show');
    cartScroll = new IScroll('#cart-nav', { 
        mouseWheel: true,
        click: true
    });
    // },100);
});

$('#cartClose').on('click', function(){
   $('#cart-nav').removeClass('show');
   setTimeout(function(){
    $('#cart-nav').css('top', '0px');
   }, 300);
});


$("#genericError .close").on("click", function(){
  $(this).parents("#genericError").hide();
});

});



// console.log(window.screen.height);

$("#main-nav a").on("click", function(){
    // $("#loadMask").show();
    // alert("Buzzinga");
    window.location.hash = $(this).attr("href");
});

window.navControl = function(){
    var $activeMenu = $('#rechargeNav li.active a');
    var menuPos = $activeMenu.position();
    var widthOffset = $activeMenu.width()/2;
    var paddingoffset = $activeMenu[0].style.paddingLeft;
    var arrowPos = menuPos.left + widthOffset - 6;

    // console.log("left",menuPos.left,"width/2", widthOffset,"paddingoffset", paddingoffset,"total",arrowPos);
    if(window.Modernizr.csstransforms3d){
        $activeMenu.closest('#rechargeNav').find('.arrow').css({
            'transform' : 'translate3d('+arrowPos+'px, 0, 0)',
            'transition' : 'transform .3s ease',
            '-webkit-transform' : 'translate3d('+arrowPos+'px, 0, 0)',
            '-webkit-transition' : '-webkit-transform .3s ease',
            '-moz-transform' : 'translate3d('+arrowPos+'px, 0, 0)',
            '-moz-transition' : '-moz-transform .3s ease',
        });
    } else{
        $activeMenu.closest('#rechargeNav').find('.arrow').css({
            'left' : arrowPos+'px'
        });
    }
};

$('#rechargeNav a').off("").on('click', function(e){
    e.preventDefault();

    var view = window.viewManager.getAll();
    if(window.location.hash === "#!/home" && ($("#freechargeApp").data("loginStatus") == true)){
        // console.log(window.viewManager.get("turboProduct"));
        if($(this).data("link") === "#!/prepaid"){
            window.viewManager.set("turboProduct", "Mobile");
            window.viewManager.get('currentActiveView').render( view );
            // $(".menu-item").removeClass("active");
            // that.setActiveState();
            fcTrack({eventName: 'm_new_prepaid', val0: "prepaid click"});
        } else if($(this).data("link") === "#!/dth"){
            window.viewManager.set("turboProduct", "DTH");
            window.viewManager.get('currentActiveView').render( view );
            // $(".menu-item").removeClass("active");
            // that.setActiveState();
            fcTrack({eventName: 'm_new_prepaid', val0: "prepaid click"});
        } else if($(this).data("link") === "#!/datacard"){
            window.viewManager.set("turboProduct", "DataCard");
            window.viewManager.get('currentActiveView').render( view );
            // $(".menu-item").removeClass("active");
            // that.setActiveState();
            fcTrack({eventName: 'm_new_prepaid', val0: "prepaid click"});
        } else if($(this).data("link") === "#!/delights"){

            window.location.hash = "#!/delights";
            
        } else if($(this).data("link") === "#!/bills"){
            /*window.viewManager.set("turboProduct", "Bill");
            window.viewManager.get('currentActiveView').render( view );
            // $(".menu-item").removeClass("active");
            // that.setActiveState();
            fcTrack({eventName: 'm_new_prepaid', val0: "prepaid click"});*/
        }
    } else{
        window.location.hash = $(this).data("link");
    }
})


$(".login-btn").off("").on( "click" , function()
{
    $("#freechargeApp").attr("data-signin-trigger", true);
    window.location.hash = "#!/signin";


});

$("#logoutBtn").off("").on( "click" , function( )
{

    var doLogoutXHR = postLogoutRequest();
    $.when( doLogoutXHR ).always( function( doLogoutXHR)
    {
        // console.log("Logged out!", doLogoutXHR)
        try{
            if(window.localStorage){
                window.localStorage.clear();
                window.viewManager.set('isLocalStoreLogin', false);
            }
        }
        catch(error){
            console.log("We got error in localStorage");
        }
        // window.location.hash = "!#/prepaid"
        $(".loggedout-message").show();
        var intr = setInterval( function()
        {
            window.location.reload();
            clearInterval( intr );
        }, 500);
    });

});

$('#backBtn').on('click', function(){

    console.log('back btn clicked!');
    if(window.location.hash === "#!/rewardtnc"){ // Some problem with delights back btn will do proper fix later
        window.location.hash = "#!/delights";
    } else{
        window.history.back();
    }
    $('#backBtn, .menu-link').fadeToggle(100);
    
    fcTrack({eventName: 'm_new_coupon_tncbck', val0: "Coupon tnc back"});
    
});



// $('.menu-link').on('click', function(){
//     $('#main-nav-wrapper').removeClass('hide')
//     setTimeout(function(){
//         $('#main-nav-wrapper').addClass('show');
//     },100);
// });

// $('#mainNavClose, #main-nav li a').on('click', function(){
//    $('#main-nav-wrapper').removeClass('show');
// });

// $('#main-nav-wrapper').on('webkitTransitionEnd transitionend', function(){
//     // console.log(this);
//     if(!$(this).hasClass('show')){
//         $(this).addClass('hide');
//     }
// });

postLogoutRequest =  function()
{
    return $.ajax({
        url: '/app/logout.htm',
        type: 'GET',
        dataType: 'JSON',
        cache: false
    });
}




</script>
<!-- <script src="${mt:keyValue("jsprefix2")}/lib/parsley.custom.validation.js"></script> -->
<!-- <script src="${mt:keyValue("jsprefix2")}/m/m.js"></script> -->
<!--${mt:hostNamePrefix()}-->
<!--${mt:hostNamePrefix()}-->
<!-- ga code -->
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-12939857-1']);
    _gaq.push(['_setDomainName', 'freecharge.in']);
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<script type="text/javascript">
    <%-- This data is pushed to tracker controller in trackerjs.jsp --%>
    var TRACKER_CLIENT_DATA = (function() {
        var userAgent = navigator.userAgent;
        var referrer = document.referrer;
        var params = location.search;

        return {
            "<%=TrackerUtil.CLIENT_DETAILS_PARAM_USER_AGENT%>": userAgent,
            "<%=TrackerUtil.CLIENT_DETAILS_PARAM_REFERRER%>": referrer,
            "<%=TrackerUtil.CLIENT_DETAILS_PARAM_PARAMS%>": params
        };
    })();

    $(document).ready(function(ev){
        <c:set var="channelId" value="<%=FCConstants.CHANNEL_ID_MOBILE%>"/>
        $.getScript('${mt:trackerUrl(channelId)}');
    });

    var _fcTrackerEventQueue = _fcTrackerEventQueue || [];

    function fcTrack(params) {
        _fcTrackerEventQueue.push(params);
    }
</script>

<script>

window.fbAsyncInit = function()
{
    FB.init(
    {
       //appId      : '1425073241090824',//267804853344286
        appId      : '267804853344286',
        cookie     : true,  // enable cookies to allow the server to access
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.0' // use version 2.0
    });
};


  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));


</script>

    <c:if test = "${mt:keyValue('system.ctx.mode') == 'prod'}">
        <!-- Start Visual Website Optimizer Asynchronous Code -->
        <script type='text/javascript'>
            var _vwo_code=(function(){
            var account_id=53504,
            settings_tolerance=2000,
            library_tolerance=2500,
            use_existing_jquery=false,
            // DO NOT EDIT BELOW THIS LINE
            f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
        </script>
        <!-- End Visual Website Optimizer Asynchronous Code -->
    </c:if>
</body>
</html>
