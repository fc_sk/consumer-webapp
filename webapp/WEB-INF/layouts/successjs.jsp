<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery-1.8.2.min.js?v=${mt:keyValue("version.no")}"></script>
<!-- tabEffect jquery -->
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tabEffect.js?v=${mt:keyValue("version.no")}"></script>

<!-- Tooltip related jquery -->
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tooltip.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.fancybox.pack.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/freecharge_popup.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.mousewheel.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/common.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/JScrollPane.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.validationEngine.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.validationEngine-en.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery-uiblock.js?v=${mt:keyValue("version.no")}"></script>
