<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>

<div style="height: 0; overflow: hidden">
    <c:choose>
        <c:when test="${finalResultPage}">

            <%-- Google Adwords Code for Transaction Conversion Page --%>
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 1002754145;
                var google_conversion_language = "en";
                var google_conversion_format = "2";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "Ru_MCMf3nQMQ4aCT3gM";
                var google_conversion_value = 0;
                /* ]]> */
            </script>
            <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1002754145/?value=0&a
    mp;label=Ru_MCMf3nQMQ4aCT3gM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>

            <%--Google code for remarketing(transacting customers) - only for final page--%>
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 1002754145;
                var google_conversion_label = "b-cmCNfxpAUQ4aCT3gM";
                var google_custom_params = window.google_tag_params;
                var google_remarketing_only = true;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt=""
                         src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1002754145/?value=0&amp;label=b-cmCNfxpAUQ4aCT3gM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>


            <%--yahoo tracking stuff--%>
            <script type="text/javascript"> if (!window.mstag) mstag = {loadTag:function () {
            }, time:(new Date()).getTime()};</script>
            <script id="mstag_tops" type="text/javascript" src="//flex.msn.com/mstag/site/2c445f4e-9aba-412f-b4ee-f752dcde9c27/mstag.js"></script>
            <script type="text/javascript"> mstag.loadTag("analytics", {dedup:"1", domainId:"2022221", type:"1", revenue:"", actionid:"104703"})</script>
            <noscript>
                <iframe src="//flex.msn.com/mstag/tag/2c445f4e-9aba-412f-b4ee-f752dcde9c27/analytics.html?dedup=1&domainId=2022221&type=1&revenue=&actionid=104703"
                        frameborder="0" scrolling="no" width="1" height="1" style="visibility:hidden;display:none"></iframe>
            </noscript>

            <%-- Freecharge facebook tracking --%>
            <script>(function() {
		    	var _fbq = window._fbq || (window._fbq = []);
				if (!_fbq.loaded) {
				var fbds = document.createElement('script');
				fbds.async = true;
				fbds.src = '//connect.facebook.net/en_US/fbds.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(fbds, s);
				_fbq.loaded = true;
				}
				_fbq.push(['addPixelId', '1562688680653971']);
				})();
				window._fbq = window._fbq || [];
				window._fbq.push(['track', 'PixelInitialized', {}]);
			</script>
			<noscript>
				<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1562688680653971&amp;ev=PixelInitialized" />
			</noscript>
        </c:when>
    </c:choose>


    <%--Google remarketing pixel for all pages--%>
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1002754145;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt=""
                 src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1002754145/?value=0&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>

</div>

