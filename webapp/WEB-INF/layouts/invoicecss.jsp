<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<link href="${mt:keyValue("cssprefix1")}/layout.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
<link href="${mt:keyValue("cssprefix2")}/invoice.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>

<style type="text/css">
    .jsdisabled { border: 1px solid #C0E0E0;float: left;margin: 0 auto;padding: 8px;position: relative;width: 100%;text-align: center;color: red;}
</style>
<!--[if IE 7]>
	<style type="text/css" >
          </style>
<![endif]-->

<!--[if lt IE 9]>
	 <style type="text/css" >
              input[ type ="text" ], input[ type ="password" ], input.pwd , input.inputTxt,  textarea {
                  line-height:20px;
              }
              input.primaryInput {
                  line-height:30px;
              }
             .mobNumber .mobNum , .rchrgForm .rchrgAmt {
                   line-height:26px;
              }
          </style>
<![endif]-->

<!--[if lt IE 7]>
	<link rel="stylesheet" media="all" href="${mt:keyValue("cssprefix1")}/ie6.css?v=${mt:keyValue("version.no")}"/>
<![endif]-->

<!--[if lt IE 8]>
	 
<![endif]-->

<!--[if gte IE 9]>
  <style type="text/css">
    button, a, input, div {
       filter: none!important;
    }
  </style>
<![endif]-->
<link rel="apple-touch-icon" href="${mt:keyValue("imgprefix1")}/images/favicon.ico"/>
<link rel="icon" href="${mt:keyValue("imgprefix1")}/images/favicon.ico" type="image/x-icon">      
<link rel="shortcut icon" href="${mt:keyValue("imgprefix1")}/images/favicon.ico" type="image/x-icon"> 
<meta name="viewport" content="width=device-width, initial-scale=1">



