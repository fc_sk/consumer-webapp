<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--<c:set var="lookupID" value='<%=request.getParameter(FCConstants.LOOKUPID)%>' />--%>

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/spinners.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tipped.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">

    $(document).ready(function() {

        /*tooltips*/
        Tipped.create('#fcb-recharge-under-process-tip', 'fcb-recharge-under-process-tooltip', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });
        Tipped.create('#fcb-recharge-fail-tip', 'fcb-recharge-fail-tooltip', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });
    });

    /* google plus */
    (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();


</script>