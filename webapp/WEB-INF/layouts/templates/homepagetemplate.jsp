<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page import="com.freecharge.common.util.ABTestingUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <!-- Google Analytics Content Experiment code -->
    <script>function utmx_section(){}function utmx(){}(function(){var
    k='26220012-10',d=document,l=d.location,c=d.cookie;
    if(l.search.indexOf('utm_expid='+k)>0)return;
    function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
    indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
    length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
    '<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
    '://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
    '&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
    valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
    '" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
    </script><script>utmx('url','A/B');</script>
    <!-- End of Google Analytics Content Experiment code -->
	<jsp:include page="../visualWebOptimiser.jsp"/>
    <jsp:include page="../googleCode.jsp"/>
    <!--

     _______  _______  _______  _______  _______           _______  _______  _______  _______
    (  ____ \(  ____ )(  ____ \(  ____ \(  ____ \|\     /|(  ___  )(  ____ )(  ____ \(  ____ \
    | (    \/| (    )|| (    \/| (    \/| (    \/| )   ( || (   ) || (    )|| (    \/| (    \/
    | (__    | (____)|| (__    | (__    | |      | (___) || (___) || (____)|| |      | (__
    |  __)   |     __)|  __)   |  __)   | |      |  ___  ||  ___  ||     __)| | ____ |  __)
    | (      | (\ (   | (      | (      | |      | (   ) || (   ) || (\ (   | | \_  )| (
    | )      | ) \ \__| (____/\| (____/\| (____/\| )   ( || )   ( || ) \ \__| (___) || (____/\
    |/       |/   \__/(_______/(_______/(_______/|/     \||/     \||/   \__/(_______)(_______/


    -->

    <jsp:include page="../htmlHeadIncludes.jsp" />
    <tiles:insertAttribute name="css"/>
    <%--common scripts(plugins) to appear in <head>--%>
    <jsp:include page="../commonScripts.jsp"/>
</head>

<body class="home">
<jsp:include page="../../views/developer/build-information.jsp"/>
<jsp:include page="../openBodyTag.jsp"/>
<noscript>
    <div class="no-js">
        <p>JavaScript is disabled in your browser! FreeCharge requires javascript.</p>
    </div>
</noscript>
<jsp:include page="../../views/fc/header-message.jsp"/>
<tiles:insertAttribute name="header"/>
<div class="container">
    <tiles:insertAttribute name="body"/>
</div>
<tiles:insertAttribute name="footer"/>
<tiles:insertAttribute name="js"/>

<%--campaign tracking codes--%>
<jsp:include page="../campaignTracking.jsp" />



</body>
</html>
