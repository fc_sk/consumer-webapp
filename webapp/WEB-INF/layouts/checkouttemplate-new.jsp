<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <jsp:include page="htmlHeadPayments.jsp" />
    <title>
        <tiles:getAsString name="title"/>
    </title>

    <%-- ALL THIRD PARTY SCRIPTS WHICH WILL WORK FOR PROD SHOULD GO IN HERE --%>
    <c:if test = "${mt:keyValue('system.ctx.mode') == 'prod'}">
        <%-- ERRORCEPTION SNIPPET STARTS --%>
        <script>
            (function(_,e,rr,s){_errs=[s];var c=_.onerror;_.onerror=function(){var a=arguments;_errs.push(a);
                c&&c.apply(this,a)};var b=function(){var c=e.createElement(rr),b=e.getElementsByTagName(rr)[0];
                c.src="//beacon.errorception.com/"+s+".js";c.async=!0;b.parentNode.insertBefore(c,b)};
                _.addEventListener?_.addEventListener("load",b,!1):_.attachEvent("onload",b)})
                    (window,document,"script","51df8e9c6fdb191b3e000003");
        </script>
        <%-- ERRORCEPTION SNIPPET ENDS --%>
    </c:if>

    <tiles:insertAttribute name="css"/>

    <%--common scripts(plugins) to appear in <head>--%>
    <jsp:include page="commonScripts.jsp"/>

</head>

<body class="payment payment-new">
<jsp:include page="openBodyTag.jsp"/>
<noscript>
    <div class="no-js">
        <p>JavaScript is disabled in your browser! FreeCharge requires javascript</p>
    </div>
</noscript>

<tiles:insertAttribute name="header"/>
<div class="container window">
    <tiles:insertAttribute name="body"/>
</div>
<tiles:insertAttribute name="footer"/>

<tiles:insertAttribute name="js"/>

<%--campaign tracking codes--%>
<jsp:include page="campaignTracking.jsp" />
<jsp:include page="googleCode.jsp"/>
</body>
</html>