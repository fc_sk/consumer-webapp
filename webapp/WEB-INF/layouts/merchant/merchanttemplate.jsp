<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="${mt:keyValue("imgprefix1")}/images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="${mt:keyValue("imgprefix1")}/images/favicon.ico" type="image/x-icon">
    <title>
        <tiles:getAsString name="title"/>
    </title>
    <tiles:insertAttribute name="css"/>
</head>
<body>

<header>
	<div class="container">
		  <div class="logo">
                <a href="/" data-edit-status="0" data-cart-id="" data-product-type="" data-txn-id="" data-lookup-id="null" data-login-status="true" id="freeCharge">
                    <img alt="freecharge.in" src="${mt:keyValue("imgprefix1")}/images/logo/freecharge.png" class="freecharge">
                </a>
            </div>
	</div>
    <div class="container">
    	<c:if test="${not isLoginPage}">
        	<a href="/rest/merchant/logout">Logout</a>
        </c:if>
    </div>
</header>

<section class="wrapper">
    <tiles:insertAttribute name="header"/>
    <div class="container main">
        <tiles:insertAttribute name="body"/>
    </div>
</section>
<tiles:insertAttribute name="js"/>
</body>
</html>
