<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="${mt:keyValue("cssprefix1")}/bootstrap.css?v=${mt:keyValue("version.no")}" >
	<link type="text/css" rel="stylesheet" href="${mt:keyValue("cssprefix1")}/merchant-style.css?v=${mt:keyValue("version.no")}" >	
	<!--[if lt IE 9]><script src="js/html5.js"></script><![endif]-->
</head>