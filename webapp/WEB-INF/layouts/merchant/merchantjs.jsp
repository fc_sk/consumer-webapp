<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<!-- Scripts -->
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery-1.8.2.min.js?v=${mt:keyValue("version.no")}"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/bootstrap.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/custom.js?v=${mt:keyValue("version.no")}"></script>
