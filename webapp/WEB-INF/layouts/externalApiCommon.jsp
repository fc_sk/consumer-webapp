<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<div id="fb-root"></div>
<script>
 window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({
      appId      : '499793956715601', // App ID from the App Dashboard
      channelUrl : '${mt:keyValue("jsprefix2")}/channel.html', // Channel File for x-domain communication
      status     : true, // check the login status upon init?
      cookie     : true, // set sessions cookies to allow your server to access the session?
      xfbml      : true  // parse XFBML tags on this page?
    });
    // Additional initialization code such as adding Event Listeners goes here
  };
  (function(d){
     var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     d.getElementsByTagName('head')[0].appendChild(js);
   }(document));  
</script>