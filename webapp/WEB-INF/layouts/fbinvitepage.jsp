<%--
  Created by IntelliJ IDEA.
  User: amaresh
  Date: 27/3/14
  Time: 1:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<html>
<head>
    <title>Invite facebook friends!</title>
</head>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.0.js"></script>

<script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script>

<script type="text/javascript">
    FB.init({
        appId:'168516546564123',
        cookie:true,
        status:true,
        xfbml:true
    });

    var excludeIds;

    function FacebookInviteFriends()
    {
        FB.ui({
            method: 'apprequests',
            message: 'Invite your friends to use freecharge!',
            filters: ['app_non_users'],
            max_recipients : '5',
            exclude_ids: excludeIds,
            title : 'Invite your friends to join freecharge!'
        }, requestCallback);
    }

    function requestCallback(res){

        if(res.to){
            $.ajax({
                url: '/social/share/fb/saveUserAndRecipients',
                type: 'POST',
                data: { requestObject : res.request, recipients : res.to.join(),  sender_channel_id : 1,
                    provider : 'facebook', referral_campaign_id: 'facebook_notifications',
                    csrfRequestIdentifier: $('#fbInviteDiv').data('csrf')},
                dataType: 'JSON'
            }).done(function(response) {
                getExcludeIds();
            });
        }
    }

    $(document).ready(function() {
        $('input[type="submit"]').attr('disabled','disabled');
        getExcludeIds();

    });

    function getExcludeIds(){
        $.get('/social/share/fb/getExcludeIds', function (data) {
            excludeIds = data.recipients;
            $('input[type="submit"]').removeAttr('disabled');
        });
    }
</script>

<body>
<div id="fb-root"></div>
<div id="fbInviteDiv" data-csrf="${mt:csrfToken()}" onload="disableAndEnable();">
    <input type="submit"  title="Submit" value="Invite" class="btn btn-primary" id="btn-social btn-fb" name="addbut"
           onclick="FacebookInviteFriends();">
</div>

</body>
</html>
