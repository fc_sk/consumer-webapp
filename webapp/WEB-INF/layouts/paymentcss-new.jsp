<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link href="${mt:keyValue("cssprefix1")}/bootstrap.3.0.3.min.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css" >
<link href="${mt:keyValue("cssprefix1")}/base.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css" >
<link href="${mt:keyValue("cssprefix1")}/layoutNew.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css" >
<link href="${mt:keyValue("cssprefix1")}/custom.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css" >
<link href="${mt:keyValue("cssprefix1")}/payment-new.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css" >
<link href="${mt:keyValue("cssprefix1")}/jquery.fancybox.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css" >
<link href="${mt:keyValue("cssprefix1")}/tipped.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css" >