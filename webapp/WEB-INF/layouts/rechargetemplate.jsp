<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">


<html lang="en">

<head>
    <title>
        ${title}
    </title>
    <title>
        <tiles:getAsString name="title"/>
    </title>
</head>

<body>
<tiles:insertAttribute name="css"/>

<div id>
    <div>
        <tiles:insertAttribute name="header"/>
    </div>

    <div>
        <div>
            <tiles:insertAttribute name="body"/>
        </div>
    </div>

    <div>
        <tiles:insertAttribute name="globalFooter"/>
    </div>
</div>

</body>
</html>
