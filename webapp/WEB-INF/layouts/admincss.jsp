<%-- FC Admin pages --%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="icon" href="${mt:keyValue("imgprefix1")}/images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="${mt:keyValue("imgprefix1")}/images/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="${mt:keyValue("imgprefix1")}/images/apple-touch-icon.png">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="${mt:keyValue("imgprefix1")}/images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="${mt:keyValue("imgprefix1")}/images/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="${mt:keyValue("imgprefix1")}/images/apple-touch-icon.png">
<meta name ="robots" content = "index,no-follow" />

<link href="${mt:keyValue("cssprefix1")}/bootstrap.min.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
<link href="${mt:keyValue("cssprefix1")}/bootstrap-datetimepicker.min.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
<link href="${mt:keyValue("cssprefix1")}/admin.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
<link href="${mt:keyValue("cssprefix1")}/admin/filtergrid.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
<%-- ## Fonts --%>
<%--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600|Open+Sans+Condensed:700|Courgette' rel='stylesheet' type='text/css'>--%>
<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css?v=${mt:keyValue("version.no")}" type="text/css" media="all" />

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery-1.8.2.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery-ui-1.9.0.full.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/bootstrap.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery-uiblock.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/admin/actb.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/admin/tablefilter.js?v=${mt:keyValue("version.no")}"></script>