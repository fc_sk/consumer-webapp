<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/coupon.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/JScrollPane.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.mousewheel.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.DOM.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.validationEngine-en.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.validationEngine.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/common.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript">

			$(document).ready(function()
				{
				$('.coupon').hover(function()
				{
					$(".couponsForm .coupon").css("z-index" , "1");
					$(this).find('.pad').addClass("shadow").closest(".coupon").css("z-index" , "2");
				   $(this).find('.couponDesc').addClass("shadowTop").stop(true, true).slideDown();

				},
				function()
				{
				   $(this).find('.pad').removeClass("shadow");
				   $(this).find('.couponDesc').removeClass("shadowTop").stop(true, true).slideUp();
				});

				$('.couponSelectedAll').jScrollPane();
			});


		</script>
