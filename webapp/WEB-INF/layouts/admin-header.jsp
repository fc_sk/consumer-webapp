<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


	<div class="system-info">
        <div>
            <span>mode = ${mt:keyValue("system.ctx.mode")}</span>
            <span>branch = ${mt:keyValue("build.branch")}</span>
            <span>revision = ${mt:keyValue("build.revision")}</span>
            <span>node = ${mt:keyValue("build.node")}</span>
            <span>build-time = ${mt:keyValue("build.time")}</span>
        </div>
        <a class="close" onclick="$(this).parent('div').slideUp(200);">x</a>
    </div>



<div class="navbar navbar-inverse">
    <div class="navbar-inner" style="border-radius: 0">
        <div class="container">
            <a href="/admin" class="brand"><img src="${mt:keyValue("imgprefix1")}/images/logo/freecharge.png" alt="FreeCharge"/> </a>
            <ul class="nav" style="color: #fff; top: 0px; font-family: Arial; font-size: 20px; font-style: bold; margin-left: 30%">
                <li><h3><c:if test="${not empty curUser}">${pageName}</c:if></h3></li>
            </ul>
            <ul class="nav pull-right" style="color: #fff; top: 7px;">
                <li><c:if test="${not empty curUser}"><a href="/admin/logout">Logout</a></c:if></li>
            </ul>
            <ul class="nav pull-right" style="color: #fff; top:16px; margin-left: 20px">
                <li class="active"><c:if test="${not empty curUser}">${curUser}</c:if></li>
            </ul>
        </div>
    </div>
</div>

 



