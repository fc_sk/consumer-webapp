<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>

<%--spinner(optional-for ajax loaded content) & tipped = canvas based tooltip--%>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/spinners.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tipped.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.fancybox.pack.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery-uiblock.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.validationEngine.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.validationEngine-en.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/common.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript">
    function getData(orderId){
     	   var formId=orderId+"-form";
        	$('#'+formId+'').submit();

    }
function viewInvoice(orderId){
	window.location='invoice.htm?orderId='+orderId;
}
/***  profile related script started   *****/
	var useremail='${userprofile.email}'; 
	// changePwd function starts here *******************************	
	function changePwd(arg){
		saveShippingAdd(this,'01', 'submit');
		$('#updatemsg').hide();
		$("#pswderromsg").html("");
		if(arg)
			{
				$(".pwdStatic").hide();
				$(".pwdDynamic").show();
				$("#passwordlabel").html("Old Password:");
			}
		else
			{
			    $('#profileupdateform').validationEngine('hide') ;
				$(".pwdStatic").show();
				$(".pwdDynamic").hide();
				$("#passwordlabel").html("Password:");
				$('#profileupdateform')[0].reset();
				$('#addressupdateform')[0].reset();
				//$("#profileupdateform").reset();
			}
	}
	// changePwd function ends here
	function updateProfile(){
		var flag=$("#profileupdateform").validationEngine('validate'); 
		if(!flag)
			 return false;
		
		blockUI();
		$.ajax({
			type : "POST",
			url:"updateprofile.htm",
			data:$('#profileupdateform').serialize(),
			cache: false,
			success : function(data) {
				$("#profilecontentdiv").html(data); 
				var upstatus=$("#profileupdatestatus").val();
				if(upstatus == 'true'){
					$("#namediv").html($("#name").val());
				}
				$("#updatemsg").show();
				$.unblockUI();
				//$("#profilecontentdiv").html(data);
			},
			error:function(xhr,ajaxOptions, thrownError){
			$.unblockUI();
			}
		})
		
		
	}
	function updateProfileAddress(){
		//saveShippingAdd(this,'01', 'submit');
		var flag=$("#addressupdateform").validationEngine('validate');
		if(!flag)
			 return false;
		blockUI();
		$.ajax({
			type : "POST",
			url:"updateaddress.htm",
			data:$('#addressupdateform').serialize(),
			//dataType: "json",
	        cache: false,
			success : function(data) { 
				document.getElementById('profilecontentdiv').innerHTML=data;
				$("#updatemsg").show();
				$.unblockUI();
				//$("#profilecontentdiv").html(response);
				
				
			},
			error:function(xhr,ajaxOptions, thrownError){
				$("#updatemsg").hide();
			$.unblockUI();
			}
		})
	}
	function blockUI(){
		$.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } }); 
	}
// editShippingAdd function *******************************
var addressEditMode = 0;
	function editShippingAdd(obj, relId, btn){
		changePwd();
		$('#updatemsg').hide();
		$("#profileupdateformdiv").hide();
		var myParent = $("#myShippingAdd_" +  relId);
		$(".address-static", myParent).hide();
		$(".addressEditForm" , myParent).css("display", "inline")
		$(btn).attr("disabled","disabled").addClass("disable");
		$("#name").attr("disabled",true);
		$("#mobileNo").attr("disabled",true);
		$("#title").attr("disabled",true);
		$("#pswderromsg").hide();
		addressEditMode++;
	}	
	// saveShippingAdd function *******************************
	function saveShippingAdd(obj, relId,  btn){
		 $('#profileupdateform').validationEngine('hide') ;
		 $('#addressupdateform').validationEngine('hide') ;
		var myParent = $("#myShippingAdd_" +  relId);
		$(".address-static", myParent).show();
		$(".addressEditForm", myParent).hide();
		if( --addressEditMode == 0)
			$(btn).removeAttr("disabled").removeClass("disable");
			$("#profileupdateformdiv").show();
			$("#name").attr("disabled",false);
			$("#mobileNo").attr("disabled",false);
			$("#title").attr("disabled",false);
			$("#pswderromsg").hide();
	}	
function validateOldpassword(obj){ 
	var password=$("#oldpassword").val();
	if(password.length < 6){
		return false;
	}
	$('#updatemsg').hide();
	$('#updateprofilebtn').attr("disabled", true);
	$('#profileformreset').attr("disabled",true);
	var data="password="+password+"&email="+useremail; 
	$.ajax({
		type : "POST",
		url:"checkoldpswd.htm",
		data:data,
		success : function(responseText) {
			var loginstatus=responseText.loginstatus;
			var pwdstatus=responseText.oldpswdstatus;
			if(!loginstatus){
				window.location.reload(true);
				return false;
		    }
			 if(!pwdstatus){
				 $("#oldpassword").val("").addClass("formErrorInput");
				 $("#pswderromsg").show();
				 $("#pswderromsg").html("Damn! This doesn&#39;t match your old password.");
				 $('#updateprofilebtn').attr("disabled", false);
				 $('#profileformreset').attr("disabled",false);
			 }else{
				 $("#oldpassword").removeClass("formErrorInput");
				 $("#pswderromsg").html("");
				 $("#pswderromsg").hide();
				 $('#updateprofilebtn').attr("disabled", false);
				 $('#profileformreset').attr("disabled",false);
			 }
			
		},
		error:function(xhr,ajaxOptions, thrownError){
			$("#oldpassword").val("");
			$('#updateprofilebtn').attr("disabled", false);
			$('#profileformreset').attr("disabled",false);
			window.location.reload(true);
		}
	})
}
function getCityList(){
    
    var stateId= $('#state').val();
    var data = 'stateId='+stateId;
    var url='/app/getcitylist.htm';
       $.ajax({
		type:"POST",
		url: url,
        data: data,
		dataType: "json",
        cache: false,
        success: function(responseText){
           if(responseText.status == 'success'){
               var obj = responseText.cityMasterList;
               /*var cityselect='<select name="city" class="validate[required] select1 fLeft " id="city">' +
                              '<option value="">Select City</option>'; */
			   var cityselect='<option value="">Select City</option>';			  
               $.each(obj, function(key, value) {
                       cityselect =cityselect + '<option value="'+value.cityMasterId+'">'+value.cityName+'</option>';
               });
               //cityselect= cityselect + '</select>';
               $('#city').html(cityselect);
           }else{
               alert ("error");
           }
		},
        error:function (e) {
            
            
        }
	});
}
function checkOldpwdValue(obj){
	 var oldpasswd = $('#oldpassword').val();
	 if(oldpasswd.length < 6){
		 $('#oldpassword').focus();
		 $("#pswderromsg").html("Please provide old password correctly.");
		 $("#pswderromsg").show();
		 var passId = '#'+obj.id; 
		 $(passId).val('');
	 }
	 
}
function isNumberKey(evt,length,id) {
	 var charCode = (evt.which) ? evt.which : evt.keyCode;
	 var evtobj=window.event? event :evt;
	 if(evtobj.shiftKey){
		 return false;
	 }else if (evtobj.ctrlKey || charCode == 37|| charCode == 39){
		return true;
	 }
	 else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
   }
/***  profile related script ended   *****/
/****    contacts related script started    *****/
//editContact function *******************************
function editContact(obj, relId){
	
	var myParent = $("#basicContactWrap_" +  relId);
	$(".editOn", myParent).hide();
	$(".editOff", myParent).show();
	$(".accountpage-myContact-staticMode", myParent).hide();
	$(".accountpage-myContact-editMode" , myParent).fadeIn(300);
	$("#msgdiv").hide();
	$("#form"+relId).validationEngine('hide') ;
	$("#circlediv").hide();
}	
// saveContact function *******************************
function saveContact(obj, relId){
	var myParent = $("#basicContactWrap_" +  relId);
	$(".editOff" , myParent).hide();
	$(".editOn", myParent).show();
	$(".accountpage-myContact-staticMode", myParent).fadeIn(300);
	$(".accountpage-myContact-editMode", myParent).hide();
	$("#form"+relId)[0].reset();
	$("#msgdiv").hide();
	$("#form"+relId).validationEngine('hide') ;
	$("#circlediv").hide();
}	

// addConctact function *******************************
var contactRelId= 1; // Give unique number to newly added address block
function addConctact(parentDiv){
		 	var shipingAddBody = "<tr id=\"basicContactWrap_" +contactRelId+ "\"  > \
                                                    <td> \
                                                        <div class=\"basicContactWrap\">\
                                                            <p class=\"accountpage-myContact-staticMode\"   style=\"display:none;\"  >Contact Title &nbsp; &nbsp; &nbsp;<span class=\"color-lighter\" >|</span>&nbsp; &nbsp; &nbsp; <strong>+91 9819289791</strong>  &nbsp; &nbsp; &nbsp;<span class=\"color-lighter\" >|</span>&nbsp; &nbsp; &nbsp; Vodafone  </p>\
                                                            <div class=\"accountpage-myContact-editMode\">\
                                                            <form action=\"\"  >\
                                                                    <input type=\"text\" class=\" fLeft \" onfocus=\"clearText(this)\" onblur=\"clearText(this)\" value=\"Contact Title\"    >\
                                                                    <span class=\"fLeft\" >\
                                                                       <span class=\"mobCD\"><span>&nbsp;</span> <b class=\"ext\"> +91</b> \
                                                                       <input type=\"text\"   value=\"Phone Number\" onfocus=\"clearText(this)\" onblur=\"clearText(this)\" class=\"mobText   \" maxlength=\"10\">\
																	   </span>\
                                                                    </span>\
                                                                    <select class=\"input2 fLeft\" >\
                                                                            <option value=\"\">Select Service</option>\
                                                                    </select>\
                                                            </form>  \
                                                            </div>\
                                                        </div>\
                                                    </td> \
                                                    <td> \
                                                       <span class=\"fRight\" > \
                                                        	<span class=\"editOn\"  style=\"display:none;\" ><a href=\"javascript:void(0)\"  onClick=\"editContact(this, '" +contactRelId+ "');\">Edit</a>  &nbsp;<span class=\"color-lighter\" >|</span>&nbsp; </span>  \
                                                            <span class=\"editOff\"> \
                                                             <a href=\"javascript:void(0)\"   onClick=\"saveContact(this, '" +contactRelId+ "');\">Save</a>  &nbsp;<span class=\"color-lighter\" >|</span>&nbsp;  \
                                                              <a href=\"javascript:void(0)\" onClick=\"saveContact(this, '" +contactRelId+ "');\">Cancel</a>  &nbsp;<span class=\"color-lighter\" >|</span>&nbsp; \
                                                            </span> \
                                                            <a href=\"javascript:void(0)\"  onclick=\"\" >FreeCharge</a> \
                                                        </span> \
                                                    </td> \
                                                  </tr> <!-- / item --> \
                                                 <tr class=\"divider\" ><td colspan=\"5\" >&nbsp;</td></tr>" ;

			$(parentDiv).append(shipingAddBody);
			contactRelId++;
}
var cid=[];
var email='${userconts.email}'; 
function blockUI(){
	$.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    } }); 
}
function updateContact(obj,id){
	$("#msgdiv").hide();
	var flag=$("#form"+id).validationEngine('validate'); 
	if(!flag){
		return false;
		} 
	var data=$("#form"+id).serialize()+"&email="+email+"&operatorname="+$('#selectoperator'+id+' option:selected').html();
	blockUI();
	 $.ajax({
		type : "POST",
		url:"updatecontact.htm",
		data:data,
		success : function(responseText) {
			var loginstatus=responseText.loginstatus;
			var updstatus=responseText.status;
			if(!loginstatus){ 
				window.location.reload(true);
				return false;
		    }
			if(!updstatus){
				$("#form"+id)[0].reset();
				$("#msgdiv").html('Contact not updated');
				$("#msgdiv").attr("class",'message-small message-small-failure lf');
				$("#msgdiv").show();
				$("#circlediv").hide();
				$.unblockUI();
			}else if(updstatus){
				$("#contactno"+id).val(responseText.serviceno);
				$("input#contactname"+id).val(responseText.name);
				$("#staticname"+id).html(responseText.name);
				$("#staticno"+id).html(responseText.serviceno);
				$("#staticopr"+id).html(responseText.operator);
				$("#msgdiv").attr("class",'message-small-success lf');
				$("#msgdiv").html('Contact updated');
				$("#msgdiv").show();
				$.unblockUI();
			}
		},
		error:function(xhr,ajaxOptions, thrownError){
			window.location.reload(true);
		}
		
	}) 
	
}
function getOperator(obj,id){
	var length=$(obj).val().length;
	var min=4;
	var max=10;
	if(length == min || length == max){ 
	var url='/app/getoperatorcircle.htm?prefix='+$(obj).val()+"&productType=V";
    $.ajax({
    url: url,
    type: "GET",
    dataType: "json",
    cache: false,
    success: function (responseText) {
    var predata= responseText.prefixData;
    var   proMasterId;
    var  opeMasterId ;
    var  circleMasterId='';
    var circleList = '<option value="">Select Circle</option>';
    var count=0;
    $.each(predata, function(key1, value1) {
         proMasterId= value1.productMasterId;
         opeMasterId=value1.operatorMasterId;
         circleMasterId=value1.circleMasterId; 
         var circleName = value1.circleName;
         count=count+1;
         if (circleName != 'ALL') {
             circleList = circleList + '<option value="' + circleMasterId + '">' + circleName + '</option>';
         }else {
        	 }
         
        });
		    if(count == 1){
		    	$('#selectoperator'+id).val(opeMasterId);
		    	//$('#circle'+id).val(circleMasterId);
		    	//$("#circlediv").hide();
		    }else{
		    	//$("#circlediv").show();
		    	//$('#circle'+id).html(circleList);
		    }
    
	 	},


   error:function (xhr, ajaxOptions, thrownError){
        
             }
  });
	}
}

function isNumeric(evt,obj,id) {
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	 if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
	 getOperator(obj,id);
}
/****    contacts related script ended    *****/



$(document).ready(function(){

    //for payment option wallet
    Tipped.create('#fcb-tip', 'fcb-tooltip', {
        inline: true,
        skin: 'white',
        maxWidth: 300
    });

    jQuery("#addCashForm").validationEngine({

        ajaxFormValidation: false ,
        scroll:false
});
});

</script>
	