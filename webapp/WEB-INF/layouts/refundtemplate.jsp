<%@ include file="/WEB-INF/includes/taglibs.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>
            <tiles:getAsString name="title"/>
        </title>
    </head>

    <body>
        <div id="wrapper">
            <tiles:insertAttribute name="body"/>
        </div>
    </body>
</html>
