<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <jsp:include page="googleCode.jsp"/>
    <jsp:include page="htmlHeadIncludes.jsp" />
    <title>
        <tiles:getAsString name="title"/>
    </title>
    <tiles:insertAttribute name="css"/>
    <%--common scripts(plugins) to appear in <head>--%>
    <jsp:include page="commonScripts.jsp"/>
	<style type="text/css">
	    .footer{
	       display: block;
	 	}
	</style>
</head>

<body>

<noscript>
    <div class="no-js">
        <p>JavaScript is disabled in your browser! FreeCharge requires javascript</p>
    </div>
</noscript>

<tiles:insertAttribute name="header"/>

<section class="content-body custom-grid" id="main">
    <div class="container static-pages">
        <tiles:insertAttribute name="body"/>
   
    </div>
</section>

<tiles:insertAttribute name="footer"/>

</body>
</html>