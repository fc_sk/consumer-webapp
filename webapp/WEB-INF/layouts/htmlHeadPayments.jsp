<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="${mt:keyValue("imgprefix1")}/images/favicon-v2.ico" type="image/x-icon">
<link rel="shortcut icon" href="${mt:keyValue("imgprefix1")}/images/favicon-v2.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="${mt:keyValue("imgprefix1")}/images/apple-touch-icon-new.png">

<c:if test='${isExperimentEnabled}'>
    <link href="https://www.freecharge.in"/>
</c:if>
<meta name ="robots" content="index,follow" />
<meta name="author"  content="Free Recharge, Online Recharge, Prepaid Recharge, Mobile Recharge, DTH Top ups"/>
<meta name="classification"  content="Free Online Recharge" />
<meta name="document-classification" content="Free Online Recharge" />
<meta name="alexaVerifyID" content="_ld1uhM6KiPm6ru7cNPKE2MvpTg" />
<%--SEO logic --%>
<c:choose>
    <c:when test="${page eq 'home' || page eq 'campaign'}">
        <c:if test="${homeStartUrl != null}">
            <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.freecharge.in/m<c:out value='${homeStartUrl}'/>" >
        </c:if>
        <c:choose>
            <c:when test="${!empty seoAttributes.metaDescription}">
                <meta name="description" content="${seoAttributes.metaDescription}">
            </c:when>
            <c:otherwise>
                <meta name="description" content="Online Recharge service for All Prepaid Mobile, DTH and Data card operators. Get Free Coupons &amp; offers of equal or higher value.">
            </c:otherwise>
        </c:choose>
        <c:choose>
            <c:when test="${!empty seoAttributes.metaKeywords}">
                <meta name="keywords" content="${seoAttributes.metaKeywords}">
            </c:when>
            <c:otherwise>
                <meta name="keywords" content="Online Recharge, free recharge, recharge online, freecharge.in, freechargein, freecharge.com, , free charge, Mobile Recharge, Prepaid ">
            </c:otherwise>
        </c:choose>
        <c:choose>
            <c:when test="${!empty seoAttributes.pageTitle}">
                <title>${seoAttributes.pageTitle}</title>
            </c:when>
            <c:otherwise>
                <title>FREE Online Recharge - Prepaid Mobile, DTH &amp; Data Card Recharge</title>
            </c:otherwise>
        </c:choose>
    </c:when>
    <c:otherwise>
        <meta name="description" content="Easy & FREE Recharge service for prepaid mobile, DTH and data card. Instant Recharge for all Mobile and DTH operators using credit card, debit card, net banking and cash card. Get FREE COUPONS & offers of equal and higher value.">
        <meta name="keywords" content="Online Recharge,Mobile Recharge,Prepaid Recharge,Airtel,Vodafone,BSNL,Reliance,Docomo,Idea,Aircel,Free Online mobile recharge, free refill, free talk time, free prepaid mobile recharge,Free E-recharge, easy & instant mobile recharge, online recharge vouchers, online top up & Recharge airtel, free vodafone prepaid mobile recharge, reliance top up online, idea online top up, tata indicom prepaid top up, bsnl mobile recharge, docomo, aircel, videocon, loop, uninor, mts, s tel recharge & top up services">
    </c:otherwise>
</c:choose>