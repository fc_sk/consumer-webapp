<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/tabEffect.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/freecharge_popup.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.validationEngine.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.validationEngine-en.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.fancybox.pack.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/common.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.DOM.js?v=${mt:keyValue("version.no")}"></script>

<script  type="text/javascript">

function sendRequest(payType){
    if(payType == 'CC'){
       var paytypestatus=document.getElementById("CC-paytypestatus").value;
       if(paytypestatus != 'success'){
       getPaymentOptions(payType);
       }
    }else if(payType == 'DC'){
       var paytypestatus=document.getElementById("DC-paytypestatus").value;
       if(paytypestatus != 'success'){
       getPaymentOptions(payType);
       }
    }else if(payType == 'NB')
       var paytypestatus=document.getElementById("NB-paytypestatus").value;
       if(paytypestatus != 'success'){
       getPaymentOptions(payType);
       }
   }
function getPaymentOptions(payType){
       var url='paymentdetailslist.htm?paymenttype='+payType;
	    //var payType=   $('input:radio[name=paymenttype]:checked').val();
	    var url='paymentdetailslist.htm?paymenttype='+payType+'&lookupID='+'${lookupID}' ;
       $.ajax({
       url: url,
       type: "POST",
       dataType: "html",
       cache: false,
       timeout:1800000,
       success: function (data) {
       var divid='#'+payType+'-paymenttypeDiv';
       $(divid).html(data);
       var statusid = '#'+payType+'-paytypestatus';
       $(statusid).val('success');
      	},
       error:function (xhr, ajaxOptions, thrownError){
           var statusid = '#'+payType+'-paytypestatus';
           $(statusid).val('fail');
                    alert(xhr.statusText);
		    alert(thrownError);
                }
     });
}
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".paymentModes").tabEffect();
    });
</script>
