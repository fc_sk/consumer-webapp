<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        <tiles:getAsString name="title"/>
    </title>
    <tiles:insertAttribute name="css"/>
    <%--<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/lib/jquery.2.1.0.js?v=${mt:keyValue("version.no")}"></script>--%>
</head>

<body>
<tiles:insertAttribute name="header"/>
<div class="container">
	<tiles:insertAttribute name="body"/>
</div>
<tiles:insertAttribute name="js"/>
</body>
</html>
