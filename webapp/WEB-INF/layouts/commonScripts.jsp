<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ page import="com.freecharge.common.framework.util.CSRFTokenManager" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery-1.8.2.min.js"></script>

<script type="text/javascript">
    $.ajaxSetup({
        beforeSend:function(xhr){
            xhr.setRequestHeader('<%=CSRFTokenManager.CSRF_REQUEST_IDENTIFIER%>', '${mt:csrfToken()}');
        }
    });

    $(document).ready(function(ev){
        <c:set var="channelId" value="<%=FCConstants.CHANNEL_ID_WEB%>"/>
        $.getScript('${mt:trackerUrl(channelId)}');
    });

    var _fcTrackerEventQueue = _fcTrackerEventQueue || [];
    <%--
    //This function gets redefined by the call to TrackingController default method. Take a look at trackerJs.jsp to see the redefinition.
    //The logic here is that, until we get a response from TrackingController default method, we queue events up. As soon as the response is loaded,
    //we redefine fcTrack to send the events directly to the server. We do this dance because we want the tracker requests to the server to
    //have the cookies sent in the TrackingController call but we do not want to block the page load until we get a response from TrackingController.
    --%>
    function fcTrack(params) {
        _fcTrackerEventQueue.push(params);
    }

</script>
