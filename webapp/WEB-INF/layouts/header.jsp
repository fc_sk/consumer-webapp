<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@page import="org.apache.commons.lang.WordUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isErrorPage="true"%>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeContextDirectory" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page import ="org.apache.commons.lang.WordUtils" %>


<%
    FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    String firstName = "";
    if (fs != null) {
        Map<String, Object> sessionData = fs.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
        firstName = (String) sessionData.get(WebConstants.SESSION_USER_FIRSTNAME);
        if (firstName != null && !firstName.isEmpty())
            firstName =  WordUtils.capitalize(firstName);
    }
%>
<%
    FreechargeSession freechargesession = FreechargeContextDirectory.get().getFreechargeSession();
    String lookupid = request.getParameter("lookupID");
    String lid = request.getParameter("lid");
    if (lookupid == null && lid != null) {
        lookupid = lid;
    }
    String productType = "";
    productType = FCSessionUtil.getRechargeType(lookupid, freechargesession);
    Integer productId = FCConstants.productMasterMapReverse.get(productType);
%>


<c:set var="lookupID" value="<%=request.getParameter(\"lookupID\")%>" scope="request" />
<c:set  value="<%=login%>" var="login" />
<c:set var="cartId" value="${cart.id}" scope="request"/>


<c:if test = "${mt:underMaintenanceHeader()}">
    <div class="system-warning">
        <p>
            <spring:message code="undergoing.maintainance" text="#undergoing.maintainance#"/>.
        </p>
    </div>
</c:if>

<input type="hidden" id="globalLookupIdHolder" value="${param.lid}" />

<%--<div class="header">
    <div class="container">
        <div class="sixteen columns">
            <div class="logo">
                <a  id="freeCharge"
                    data-login-status="${login}"
                    data-lookup-id="<%=lookupid%>"
                    data-txn-id="${txnHomePgID}"
                    data-product-type="<%=productType%>"
                    data-cart-id="${cartId}" 
                    data-edit-status="${isEdit}" 
                    data-img-prefix1="${mt:keyValue('imgprefix1')}"
                    data-festive-gift-enabled="${isFestiveGiftEnabled}" 
                    href="/">
                    <img class="freecharge" src="${mt:keyValue("imgprefix1")}/images/logo/freecharge.png" alt="<spring:message code="freecharge.in" text="#freecharge.in#"/>">
                    <c:if test="${isFestiveGiftEnabled}">
                    	<img class="freeCharge-festiveLogo"  style="opacity:0; filter:Alpha(opacity=0);" src="${mt:keyValue("imgprefix1")}/images/diwali/freecharge-diwaliLogo.png"
                     	alt="<spring:message code="freecharge.in" text="#freecharge.in#"/>">
                    </c:if>
                </a>
            </div>
            &lt;%&ndash; <div style="text-align:right; padding-right:60px; padding-top:8px"></div> &ndash;%&gt;
            <div class="links">
                <c:if test="${!login}">
                    <ul id="loginHeaderDiv" class="clearfix">
                            &lt;%&ndash; <li><a id="sign_up" href="#signupPageId" onclick="_gaq.push(['_trackEvent', 'Button', 'Signup', 'Home'])">Sign Up</a></li>  &ndash;%&gt;
                        <li><a class="support" href="http://support.freecharge.in"><i class="icon-support"></i><span class="link-text"><spring:message code="support" text="#support#"/></span></a></li>
                        <li><a id="login-header" class="header-login" href="#loginPage"><i class="icon-login"></i><span class="link-text"><spring:message code="login" text="#login#"/></span></a></li>
                    </ul>
                    &lt;%&ndash;login/signup/password-recovery&ndash;%&gt;
                    <c:if test="${!login}">
                        &lt;%&ndash;login&ndash;%&gt;
                        <jsp:include page="../views/fc/login.jsp"/>
                    </c:if>

                </c:if>
            </div>

            <div class="afterLogin" id="afterlogin">
                <c:if test="${login}">
                    <ul class='clearfix'>
                        <li class="first">Hi <span id='namediv'><% out.print(firstName); %></span></li>
                            <li><a href="/app/myrecharges.htm" class="myaccount-link"><i class="icon-account"></i><span class="link-text"><spring:message code="my.account" text="#my.account#"/></span></a></li>
                            <li><a href="/app/mycredits.htm" onclick="_gaq.push(['_trackEvent', 'Header', 'Click', 'viewMyBalance']);"><i class="icon-balance"></i><span class="link-text"><spring:message code="fc.money" text="#fc.money#"/> (<span id="showbalancediv"></span>)</span></a></li>
                            <li><a href="http://support.freecharge.in" target="_blank"><i class="icon-support"></i><span class="link-text">Support</span></a></li>
                            <li class="last"><a href="/app/logout.htm?<%=CSRFTokenManager.CSRF_REQUEST_IDENTIFIER%>=${mt:csrfToken()}">
                                <i class="icon-logout"></i><span class="link-text"><spring:message code="logout" text="#logout#"/></span>
                            </a></li>
                    </ul>
                </c:if>
            </div>
        </div>                  
    </div>
</div>--%>



<%--
    New Header - like the one in singlepage
--%>

<header class="header custom-grid">
    <div class="main-head">
        <div class="container">
            <a href="/" id="freecharge" class="logo freeCharge">
                <img src="${mt:keyValue("imgprefix1")}/images/singlepage/freecharge_white.png" alt="freecharge.in" />
            </a>
        </div>
    </div>
</header>
