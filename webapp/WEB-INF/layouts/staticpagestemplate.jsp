<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <jsp:include page="googleCode.jsp"/>
    <jsp:include page="htmlHeadIncludes.jsp" />
    <title>
        <tiles:getAsString name="title"/>
    </title>
    <tiles:insertAttribute name="css"/>

    <style type="text/css">
        @media (min-width: 0px){
            .container {
                width: 100%;
            }
        }
        .header .main-head{
            background: #D55E36;
        }
        .menu-link{
            display: inline-block;
            position: absolute;
            right: 7px;
            top: 7px;
            padding: 6px 0;
            text-align: center;
            cursor: pointer;
            display: none;
        }
        .menu-link img{
            width: 50%;
        }
        .custom-grid .container {
            min-width: 100%;
        }
        .static-pages nav{
            float: none;
            position: absolute;
            top: 80px;
            left: 20px;
            z-index: 10;
        }
        .static-pages nav ul{
            margin: 0;
        }
        @media (max-width: 600px){
            body .menu-link{
                display: inline-block;
            }
            body .static-pages nav{
                top: 50px;
                left: 0px;
                width: 100%;
                text-align: center;
                background-color: #fff;
                display: none;
            }
            body .static-pages nav ul li{
                margin: 0;
            }
            body .static-pages nav ul li a{
                padding: 12px 0;
                color: #D55E36;
                border-bottom: 1px solid #ededed;
            }
            body .static-pages nav ul li a:hover{
                color: #fff;
                background: #D55E36;
                border-color: #D55E36;
            }
            .static-pages nav ul li a.active{
                background: #D55E36;
                color: #fff;
                border-color: #D55E36;
            }
            body .static-pages .static-content, body .static-pages .sitemap-content{
                margin: 0;
                padding: 14px;
                border: 0;
            }
            body .footer .footer-one{
                padding-bottom: 42px;
                margin-bottom: 12px;
            }
            body .footer .footer-one .footer-links li{
                margin: 8px;
            }
        }
    </style>
    <%--common scripts(plugins) to appear in <head>--%>
    <jsp:include page="commonScripts.jsp"/>
</head>

<body>
<%--<jsp:include page="openBodyTag.jsp"/>--%>
<noscript>
    <div class="no-js">
        <p>JavaScript is disabled in your browser! FreeCharge requires javascript</p>
    </div>
</noscript>

<tiles:insertAttribute name="header"/>

<section class="content-body custom-grid" id="main">
    <div class="menu-link">
        <img src="${mt:keyValue("imgprefix1")}/images/hamburger.png?v=${mt:keyValue("version.no")}">
    </div>
    <div class="container static-pages">
        
        <tiles:insertAttribute name="body"/>
        <tiles:insertAttribute name="js"/>
    </div>
</section>

<tiles:insertAttribute name="footer"/>


<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){
        $('.menu-link').on('click', function(){
            $('nav').slideToggle();
        });
    });
</script>

<%--campaign tracking codes--%>
<%--<jsp:include page="campaignTracking.jsp" />--%>

</body>
</html>