<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:choose>
    <c:when test="${rechargeInitiateWebDo.paymentResponse eq 'true'}">
        <c:if test="${rechargeInitiateWebDo.rechargeResponse=='00' || rechargeInitiateWebDo.rechargeResponse=='0'}">
            <tiles:insertAttribute name="successbody"/>
        </c:if>

        <c:if test="${rechargeInitiateWebDo.rechargeResponse =='08'}">
            <tiles:insertAttribute name="successbody"/>
        </c:if>

        <c:if test="${rechargeInitiateWebDo.rechargeResponse =='06'}">
            <tiles:insertAttribute name="rechargefailbody"/>
        </c:if>

        <c:if test="${rechargeInitiateWebDo.rechargeResponse !='00' && rechargeInitiateWebDo.rechargeResponse !='08' && rechargeInitiateWebDo.rechargeResponse !='06'}">
            <tiles:insertAttribute name="rechargeunderprocessbody"/>
        </c:if>
    </c:when>
</c:choose>

