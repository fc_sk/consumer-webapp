<!-- Load the Google Content Experiment JavaScript API client for the experiment -->
<script src="//www.google-analytics.com/cx/api.js?experiment=9etz2BXJRY-KmZnrkslBCQ"></script>

<script type="text/javascript">
  // Ask Google Content Experiment which variation to show the visitor
  var chosenVariation = cxApi.chooseVariation();

  // Define JavaScript for each page variation of this Google Content Experiment
  var pageVariations = [
      function() {},  // Original: Do nothing. This will render the default HTML.
      function() {    // Variation 1: Show "Proceed to Pay" on button (instead of "Continue")
          $('#checkoutButton').find('button').text('Proceed to Payment');
      }
  ];

  // Wait for the DOM to load, then execute the view for the chosen variation.
  $(document).ready(function() {
    // Execute the chosen view
    pageVariations[chosenVariation].call();
  });

</script>
