<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="lookupID" value='<%=request.getParameter(FCConstants.LOOKUPID)%>' />

<%--spinner(optional-for ajax loaded content) & tipped = canvas based tooltip--%>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/spinners.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tipped.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.fancybox.pack.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/tabEffect.js?v=${mt:keyValue("version.no")}"></script>
<%--<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/freecharge_popup.js?v=${mt:keyValue("version.no")}"></script>--%>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.validationEngine.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.validationEngine-en.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery-uiblock.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/common.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">
    var _hookpoint = "page2";
    var _lookupid =  $("#globalLookupIdHolder").val();
</script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/ardeal.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">
    ARDEAL = new ArDeal({hookpoint: _hookpoint, lookupid: _lookupid}).
            checkArDeal(function() {
                ARDEAL.showArDeal();
            });
    $(document).ready(function() {
        /*tooltip call*/
        // loop over all elements creating a tooltip based on their data-tooltip-id attribute
        //for upsell
        $('.tooltip').each(function() {
            var selector = '#' + $(this).data('tooltip-id');
            Tipped.create(this, $(selector)[0], {
                skin : 'white',
                maxWidth: 400
            });
        });

        //for discount code section
        Tipped.create('#discount-terms-link', 'discount-terms', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });


        //for payment option wallet
        Tipped.create('#fcb-tip', 'fcb-tooltip', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });

        //for fcbalance amount
        Tipped.create('#fcb-amount-tip', 'fcb-amount-tooltip', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });

        //for promo-code
        Tipped.create('#promo-tip', 'promo-tooltip', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });

        //for secret-code
        Tipped.create('#secretCode-tip', 'secretCode-tooltip', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });



        //for bin-offer
        Tipped.create('#bin-offer-tip', 'bin-offer-tooltip', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });
        Tipped.create('#bin-offer-tip-2', 'bin-offer-tooltip-2', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });

        // confirm shipping validation
        $('#shippingaddressform').validationEngine({
            ajaxFormValidation: true ,
            scroll:false
        });


        // discount codes - accordion type
        $("#discount_accordion > li > p").click(function(){
            if (false == $(this).next().is(':visible')){
                if ($("#discount_accordion .remove-link").is(':visible')) {
                    alert("Not so fast! Only one unique offer can be availed of at any given point in time.");
                    return false;
                }
                $("#discount_accordion .discount-content").slideUp(300);
                $("#discount_accordion .discount-title span").removeClass("open");
                $("#discount_accordion .fc-message").hide();
            }
            if ($(this).next().find(".remove-link").is(':visible')) {
                $(this).next().find(".remove-link").trigger('click');
            }
            $(this).next().slideToggle(300);
            if ($(this).find("span").hasClass("open")) {
                $(this).find("span").removeClass("open");
            } else {
                $(this).find("span").addClass("open");
            }

        });
        //$('#discount_accordion .discount-content:eq(0)').show();


        // Freefund payment option block popup
        $('.otherPaymentOpt').fancybox({
            modal : true,
            keys : {close  : [27]} // escape key
        });

});



</script>

<script type="text/javascript">
var globalCartId = '';
var states="<option value=''>Select state</option>";
<c:forEach var="state" items = "${stateMaster}" varStatus='count'>
	states=states+"<option value='${state.stateMasterId}'>${state.stateName}</option>";
</c:forEach>

<%--edit address --%>
$("#editaddr-link a").fancybox();

function getCityList(stateselectId,cityselectspanId,cityselectname,cityselectId){
    var stdId='#'+ stateselectId;
    var stateId= $(stdId).val();
    var data = 'stateId='+stateId;
    var url='/app/getcitylist.htm';
    $.ajax({
		type:"POST",
		url: url,
        data: data,
		dataType: "json",
        cache: false,
        timeout:1800000,
		success: function(responseText){
           if (responseText.status == 'success') {

               var obj = responseText.cityMasterList;
               var cityselect = '<option value="">Select city</option>';
               $.each(obj, function(key, value) {
                       cityselect =cityselect + '<option value="'+value.cityMasterId+'">'+value.cityName+'</option>';
               });
               var cityspanId = '#'+cityselectspanId;
               $('#' + cityselectId).html(cityselect);
           }else{
               //alert ("error");
           }
		},
        error:function (e) {
            
            $("#paymentLoginError").html(e);
        }
	});
}
</script>