<div class="row footer clearfix">
    <div class="container">
        <div class="two-thirds column">
            <div class="clearfix">
                <div class='footer-section-one'>
                    <ul class="footer-mobile-recharge">
                        <li class="title" ><a href="/online-mobile-recharge">Mobile Recharge</a></li>
                        <li><a href="/airtel-prepaid-mobile-recharge">Airtel</a></li>
                        <li><a href="/aircel-prepaid-mobile-recharge">Aircel</a></li>
                        <li><a href="/vodafone-prepaid-mobile-recharge">Vodafone</a></li>
                        <li><a href="/bsnl-prepaid-mobile-recharge">BSNL</a></li>
                        <li><a href="/tata-docomo-online-recharge">Tata Docomo</a></li>
                        <li><a href="/idea-prepaid-mobile-recharge">Idea</a></li>
                        <li><a href="/tata-indicom-walky-prepaid-mobile-recharge">Tata Walky</a></li>
                        <li><a href="/mtnl-trump-prepaid-mobile-recharge">MTNL Trump</a></li>
                        <li><a href="/reliance-cdma-prepaid-mobile-recharge">Reliance CDMA</a></li>
                        <li><a href="/reliance-gsm-prepaid-mobile-recharge">Reliance GSM</a></li>
                        <li><a href="/tata-indicom-prepaid-mobile-recharge">Tata Indicom</a></li>
                        <li><a href="/uninor-prepaid-mobile-recharge">Uninor</a></li>
                        <li><a href="/mts-prepaid-mobile-recharge">MTS</a></li>
                        <li><a href="/videocon-prepaid-mobile-recharge">Videocon</a></li>
                        <li><a href="/tata-indicom-walky-prepaid-mobile-recharge">Virgin CDMA</a></li>
                        <li><a href="/tata-docomo-online-recharge">Virgin GSM</a></li>
                    </ul>
                </div>

                <div class='footer-section-two'>
                    <ul class="footer-dth-recharge">
                        <li class="title"><a href="/online-dth-recharge">DTH (TV)</a></li>
                        <li><a href="/online-airtel-dth-recharge">Airtel Digital</a></li>
                        <li><a href="/online-big-tv-recharge">BIG TV</a></li>
                        <li><a href="/online-dish-tv-recharge">Dish TV</a></li>
                        <li><a href="/online-tata-sky-recharge">Tata Sky</a></li>
                        <li><a href="/online-sun-direct-recharge">Sun Direct</a></li>
                        <li><a href="/videocon-d2h-online-recharge">Videocon D2H</a></li>
                    </ul>
                    <ul class="footer-datacard-recharge">
                        <li class="title"><a href="/online-datacard-recharge">Data Card</a></li>
                        <li><a href="/tata-photon-online-recharge">Tata Photon</a></li>
                        <li><a href="/mts-mblaze-recharge">MTS MBlaze</a></li>
                        <li><a href="/mts-mbrowse-datacard-recharge">MTS MBrowse</a></li>
                        <li><a href="/reliance-netconnect-datacard-recharge">Reliance NetConnect</a></li>
                    </ul>
                </div>


                <div class="footer-section-three">
                    <ul class="footer-freecharge-links">
                        <li class="title"><a href="/">Freecharge</a></li>
                        <li><a href="/app/aboutus.htm" target="_blank">About Us</a></li>
                        <li><a href="/payment-options/freecharge-credits" target="_blank"><spring:message code="fc.money" text="#fc.money#"/></a></li>
                        <li><a href="/app/termsandconditions.htm" target="_blank">T &amp; C</a></li>
                        <li><a href="/offers/bank-cashback" target="_blank">Bank Offers</a></li>
                        <!-- <li><a href="/app/faq.htm" target="_blank">FAQ</a></li> -->
                        <li><a href="/mobile-app-download" target="_blank">Mobile App</a></li>
                        <li><a href="http://support.freecharge.in" target="_blank">Support</a></li>
                        <li><a href="http://blog.freecharge.in" target="_blank">Blog</a></li>
                        <li><a href="/app/contactus.htm" target="_blank">Contact Us</a></li>
                        <li><a href="http://geekery.freecharge.in" target="_blank">Tech Blog</a></li>
                        <li><a href="/app/privacypolicy.htm" target="_blank">Privacy Policy</a></li>
                        <li><a href="/app/sitemap.htm" target="_blank">Sitemap</a></li>
                        <li><a href="/security/disclosure.htm" target="_blank">Security Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="one-third column">
            <div class="info">
                <div class="social" >
                    <h4>Follow us on</h4>
                    <ul>
                        <li><a href="https://www.facebook.com/freecharge" class="facebook" target="_blank" rel="nofollow">facebook</a></li>
                        <li><a href="https://twitter.com/freecharge"  class="twitter" target="_blank" rel="nofollow">Twitter</a></li>
                        <li><a href="https://plus.google.com/+freecharge/"  class="googleplus" target="_blank" rel="nofollow">Google +</a></li>
                    </ul>
                </div>
                <div class="security">
                    <a class="security-emblem" target="_blank">Secure Payment</a>
                </div>
            </div>
            <div class='security-logos'>
                <ul>
                    <li class='visa-logo'><a class="visa" target="_blank"><img src="${mt:keyValue("imgprefix1")}/images/footer/visa-mastercard.png" alt="Verified By Visa, Mastercard Secured"/>Verified By Visa, Mastercard Secured</a></li>
                    <li class='pci-logo'><a href="javascript:CCPopUp('http://seal.controlcase.com/', 4252874532);"  class="pci-compliance" title="PCI Compliant" >PCI Compliance</a></li>
                    <li class="symantecSSL-logo" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications." ><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.freecharge.in&amp;size=XS&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script></li>
                    <li class='geotrust-logo'><span class='geotrust'><script language="javascript" type="text/javascript" src="//smarticon.geotrust.com/si.js"></script></span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="copyright">
    <div class='container'>
        <p>
            <span class="footer-companyname">&copy;<spring:message code="m.company.name" text="#m.company.name"/></span>
            <span><a class="footer-payment-option maestro" target="_blank" >Maestro</a></span>
            <span><a class="footer-payment-option mastercard" target="_blank">Mastercard</a></span>
            <span><a class="footer-payment-option visa" target="_blank">Visa</a></span>
            <span><a class="footer-payment-option cash-card" target="_blank" >Cash Card</a></span>
            <span><a class="footer-payment-option netbanking" target="_blank">Netbanking</a></span>
            <span><a class="footer-payment-option wallet" target="_blank">Wallet</a></span>
        </p>
    </div>
</div>
