<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>


<%--
  Created by IntelliJ IDEA.
  User: Toshiba
  Date: Apr 25, 2012
  Time: 10:17:29 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">--%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        <tiles:getAsString name="title"/>
    </title>
    <tiles:insertAttribute name="css"/>

    <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery-1.8.2.min.js?v=${mt:keyValue("version.no")}"></script>
</head>

<body class="ardealIdentPage1">

<noscript>
    <div class="no-js">
        <p>JavaScript is disabled in your browser! Freecharge.in requires javascript</p>
    </div>
</noscript>

<div id="wrapper">

    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="body"/>
    <tiles:insertAttribute name="js"/>
     
    <div class="footer">
        <tiles:insertAttribute name="footer"/>
        <tiles:insertAttribute name="globalFooter"/>
    </div>

</div>
</body>
</html>
