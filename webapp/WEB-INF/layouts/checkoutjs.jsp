<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="lookupID" value='<%=request.getParameter(FCConstants.LOOKUPID)%>' />

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/spinners.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tipped.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.fancybox.pack.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/tabEffect.js?v=${mt:keyValue("version.no")}"></script>
<%--<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/freecharge_popup.js?v=${mt:keyValue("version.no")}"></script>--%>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.validationEngine.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.validationEngine-en.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery-uiblock.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/common.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/payment.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        /*tooltip call*/
        // loop over all elements creating a tooltip based on their data-tooltip-id attribute

        //for payment option wallet
        Tipped.create('#fcb-tip', 'fcb-tooltip', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });

        /*for fcbalance amount(Credits Amount)*/
        Tipped.create('#fcb-amount-tip', 'fcb-amount-tooltip', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });

        //for bin-offer
        Tipped.create('#bin-offer-tip', 'bin-offer-tooltip', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });
        Tipped.create('#bin-offer-tip-2', 'bin-offer-tooltip-2', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });

        $('.otherPaymentOpt').fancybox({
            modal : true,
            keys : {close  : [27]} // escape key
        });
    });
</script>
