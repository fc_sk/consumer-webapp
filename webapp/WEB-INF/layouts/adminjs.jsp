<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery-uiblock.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/tooltip.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/tabEffect.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.validationEngine.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery.validationEngine-en.js?v=${mt:keyValue("version.no")}"></script>
<!-- <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/common.js?v=${mt:keyValue("version.no")}"></script> -->
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/bootstrap-datetimepicker.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">
// Filter function 
function filterCall(field, table, defaultOption){
	var filter = field.options[field.selectedIndex].value;
	var tableRows = "#" + table + " tbody tr";
	if(filter == defaultOption)
		$(tableRows).show();
	else {
		$(tableRows).hide();
		$(tableRows +"." +filter).show();	
	}
}
</script>