<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeContextDirectory" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.freecharge.web.util.WebConstants" %>



<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.validationEngine.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.validationEngine-en.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/json2.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.fancybox.pack.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery-uiblock.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tipped.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/spinners.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/common.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/home-common.js?v=${mt:keyValue("version.no")}"></script>