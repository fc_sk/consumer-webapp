<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="${mt:keyValue("imgprefix1")}/images/favicon-v2.ico" type="image/x-icon">
<link rel="shortcut icon" href="${mt:keyValue("imgprefix1")}/images/favicon-v2.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="${mt:keyValue("imgprefix1")}/images/apple-touch-icon-new.png">

<c:if test='${isExperimentEnabled}'>
<link href="https://www.freecharge.in"/>
</c:if>
<meta name ="robots" content="index,follow" />
<meta name="author"  content="Free Recharge, Online Recharge, Prepaid Recharge, Mobile Recharge, DTH Top ups"/>
<meta name="classification"  content="Free Online Recharge" />
<meta name="document-classification" content="Free Online Recharge" />
<%--<meta name="alexaVerifyID" content="_ld1uhM6KiPm6ru7cNPKE2MvpTg" />--%>
<%--SEO logic --%>
<c:choose>
	<c:when test="${page eq 'home' || page eq 'campaign'}">
		<c:if test="${homeStartUrl != null}">
			<link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.freecharge.in/m<c:out value='${homeStartUrl}'/>" >
		</c:if>
		<c:choose>
			<c:when test="${!empty seoAttributes.metaDescription}">
				<meta name="description" content="${seoAttributes.metaDescription}">
			</c:when>
			<c:otherwise>
				<meta name="description" content="Online Recharge service for All Prepaid Mobile, DTH and Data card operators. Get Free Coupons &amp; offers of equal or higher value.">
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${!empty seoAttributes.metaKeywords}">
				<meta name="keywords" content="${seoAttributes.metaKeywords}">
			</c:when>
			<c:otherwise>
				<meta name="keywords" content="Online Recharge, free recharge, recharge online, freecharge.in, freechargein, freecharge.com, , free charge, Mobile Recharge, Prepaid ">
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${!empty seoAttributes.pageTitle}">
				<title>${seoAttributes.pageTitle}</title>
			</c:when>
			<c:otherwise>
				<title>FREE Online Recharge - Prepaid Mobile, DTH &amp; Data Card Recharge</title>
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:when test="${page eq 'aboutus'}">
		<meta name="description" content="Freecharge.in is India's 1st FREE Online Prepaid Mobile Recharge & Top Up Service Provider Company. Freecharge.in offers Equal Amount of FREE Coupons & Shopping Vouchers to the user for the Amount user Recharge her/his Prepaid mobile on the website.">
		<meta name="keywords" content="free online mobile recharge, free recharge vouchers, online prepaid mobile recharge, prepaid mobile top up free, free mobile talk time services, free vodafone prepaid mobile recharge, reliance top up online, idea online top up, tata indicom prepaid top up, bsnl mobile recharge, docomo, aircel, videocon, loop, uninor, mts, s tel recharge & top up services">
	</c:when>
	<c:when test="${page eq 'contactus'}">
		<meta name="description" content="Contact Freecharge.in to Recharge & Top Up your Prepaid Mobiles and to get Equal Amount of FREE Coupons & Shopping Vouchers. Register Today and Recharge your AirTel, Idea, BSNL, Vodafone, Reliance, MTS, Tata Indicom, DOCOMO prepaid mobile to get Free Coupons.">
		<meta name="keywords" content="online prepaid mobile recharge, free online mobile recharge, free recharge vouchers, prepaid mobile top up free,? free vodafone prepaid mobile recharge, reliance top up online, free mobile talk time services, idea online top up, tata indicom prepaid top up, bsnl mobile recharge, videocon, loop, uninor, mts, docomo, aircel, s tel recharge & top up services">
	</c:when>
	<c:when test="${page eq 'faq'}">
		<meta name="description" content="Freecharge.in - FAQ & Find Answers to all your Prepaid Recharge, Coupons, Payment, Registration related queries. Recharge & Top Up your Prepaid Mobile Online on Freecharge.in India's most popular and highest paying website for Recharge Users.">
		<meta name="keywords" content="Free Online mobile recharge, free refill, free talk time, free prepaid mobile recharge, recharge your prepaid mobile free, Free E-recharge, easy & instant mobile recharge, online recharge vouchers, online top up &? Recharge airtel, free vodafone prepaid mobile recharge, reliance top up online, idea online top up, tata indicom prepaid top up, bsnl mobile recharge, docomo, aircel, videocon, loop, uninor, mts, s tel recharge & top up services">
	</c:when>
	<c:when test="${page eq 'feedback'}">
		<meta name="description" content="Give your valuable feedback to help us improving our Online Prepaid Recharge & Top Up services for you. We value your suggestions; as your feedback help us to serve you better! Our Customer satisfaction is our AIM!">
		<meta name="keywords" content="recharge your prepaid mobile free, Free E-recharge, easy & instant mobile recharge, Free Online mobile recharge, free refill, free talk time, free prepaid mobile recharge, online recharge vouchers, reliance top up online, idea online top up, tata indicom prepaid top up, online top up &? Recharge airtel, free vodafone prepaid mobile recharge">
	</c:when>
	<c:when test="${page eq 'termsandconditions'}">
		<meta name="description" content="Terms & Conditions and User Agreement for Freecharge.in. Freecharge.in offers hassle free online prepaid mobile recharge and top-up services to its users, we are committed to provide best and error free services to the Indian Prepaid Mobile users.">
		<meta name="keywords" content="Terms & Conditions and User Agreement, Online recharge, recharge your prepaid mobile free, Free E-recharge, easy & instant mobile recharge, Free Online mobile recharge, free refill, free talk time, free prepaid mobile recharge, online recharge vouchers, reliance top up online, idea online top up, tata indicom prepaid top up, online top up &? Recharge airtel, free vodafone prepaid mobile recharge">
	</c:when>
	<c:when test="${page eq 'privacypolicy'}">
		<meta name="description" content="Freecharge.in - India's Top Online Prepaid Mobile Recharge Service Provider is committed towards protecting its user privacy.">
		<meta name="keywords" content="Privacy Policy , Online recharge, free prepaid mobile recharge, online recharge vouchers, recharge your prepaid mobile free, online top up &? Recharge airtel, free vodafone prepaid mobile recharge, Free E-recharge, easy & instant mobile recharge, Free Online mobile recharge, free refill, free talk time, reliance top up online, idea online top up, tata indicom prepaid top up">
	</c:when>
	<c:when test="${page eq 'gosf'}">
		<meta name="description" content="GOSF-2014.BIG FreeCharge offers during the Great Online Shopping Festival 2014.299 offer &amp; cash back offer on prepaid,postpaid,DTH,datacard mobile online recharge .">
		<meta name="keywords" content="Online Recharge,Mobile Recharge,Prepaid Recharge,Airtel,Vodafone,BSNL,Reliance,Docomo,Idea,Aircel,Free Online mobile recharge, free refill, free talk time, free prepaid mobile recharge,Free E-recharge, easy &amp; instant mobile recharge, online recharge vouchers, online top up &amp; Recharge airtel, free vodafone prepaid mobile recharge, reliance top up online, idea online top up, tata indicom prepaid top up, bsnl mobile recharge, docomo, aircel, videocon, loop, uninor, mts, s tel recharge &amp; top up services">
	</c:when>
	<c:otherwise>
		<meta name="description" content="Easy & FREE Recharge service for prepaid mobile, DTH and data card. Instant Recharge for all Mobile and DTH operators using credit card, debit card, net banking and cash card. Get FREE COUPONS & offers of equal and higher value.">
		<meta name="keywords" content="Online Recharge,Mobile Recharge,Prepaid Recharge,Airtel,Vodafone,BSNL,Reliance,Docomo,Idea,Aircel,Free Online mobile recharge, free refill, free talk time, free prepaid mobile recharge,Free E-recharge, easy & instant mobile recharge, online recharge vouchers, online top up & Recharge airtel, free vodafone prepaid mobile recharge, reliance top up online, idea online top up, tata indicom prepaid top up, bsnl mobile recharge, docomo, aircel, videocon, loop, uninor, mts, s tel recharge & top up services">
	</c:otherwise>
</c:choose>

<%--Fonts--%>
<%--<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>--%>
<%--<link href='http://fonts.googleapis.com/css?family=Lato|Open+Sans:400,600' rel='stylesheet' type='text/css'>--%>
<link href='https://fonts.googleapis.com/css?family=Lato:100,700' rel='stylesheet' type='text/css'>

<%-- Pingdom RUM --%>
<script>
var _prum = [['id', '526574f3abe53d2433000000'],
             ['mark', 'firstbyte', (new Date()).getTime()]];
(function() {
    var s = document.getElementsByTagName('script')[0]
      , p = document.createElement('script');
    p.async = 'async';
    p.src = '//rum-static.pingdom.net/prum.min.js';
    s.parentNode.insertBefore(p, s);
})();
</script>
<%-- Done Pingdom RUM --%>
