<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <jsp:include page="htmlHeadIncludes.jsp" />
    <title>
        <tiles:getAsString name="title"/>
    </title>
    <tiles:insertAttribute name="css"/>
    <%--<jsp:include page="visualWebOptimiser.jsp"/>--%>
	<jsp:include page="googleCode.jsp"/>
    <%--common scripts(plugins) to appear in <head>--%>
    <jsp:include page="commonScripts.jsp"/>
    
	<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.fancybox.pack.js?v=${mt:keyValue("version.no")}"></script>
	<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jquery-uiblock.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/common.js?v=${mt:keyValue("version.no")}"></script>
</head>

<body>

<jsp:include page="openBodyTag.jsp"/>
<noscript>
    <div class="no-js">
        <p>JavaScript is disabled in your browser! FreeCharge requires javascript.</p>
    </div>
</noscript>

<tiles:insertAttribute name="header"/>
<div class="container window">
    <tiles:insertAttribute name="body"/>
</div>
<tiles:insertAttribute name="footer"/>
<tiles:insertAttribute name="js"/>

<%--campaign tracking codes--%>
<jsp:include page="campaignTracking.jsp" />


</body>
</html>