<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
    <c:when test="${ not empty seoAttributes.footerTextParaOne}">
        <div class="seo-text custom-grid">
            <div class="container">
                <p>${seoAttributes.footerTextParaOne}</p>
                <p>${seoAttributes.footerTextParaTwo}</p>
            </div>
        </div>
    </c:when>
    <c:otherwise></c:otherwise>
</c:choose>
<footer class="footer custom-grid">
    <div class="container">
        <div class="footer-block">
            <div class="footer-one">
                <ul class="footer-links">
                    <li><a href="/app/aboutus.htm" target="_blank">About Us</a></li>
                    <li><a href="/app/termsandconditions.htm" target="_blank">T &amp; C</a></li>
                    <!-- <li><a href="/app/faq.htm" target="_blank">FAQ</a></li> -->
                    <li><a href="http://support.freecharge.in" target="_blank">Support</a></li>
                    <li><a href="/app/contactus.htm" target="_blank">Contact Us</a></li>
                    <li><a href="http://blog.freecharge.in" target="_blank">Blog</a></li>
                    <li><a href="/app/privacypolicy.htm" target="_blank">Privacy Policy</a></li>
                    <li><a href="/security/disclosure.htm">Security Policy</a></li>
                    <li><a href="/app/sitemap.htm" target="_blank">Sitemap</a></li>
                </ul>
                <p class="copyright">&copy; Accelyst Solutions Pvt. Ltd. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</footer>