<%@ page import="com.freecharge.tracker.TrackerController"%>
<%@ page language="java" contentType="text/javascript" pageEncoding="UTF-8"%>

<%
    response.setHeader("Content-Type", "text/javascript");
    response.setHeader("Pragma", "No-cache");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0, private");
    response.setHeader("Expires", "Thu, 29 Oct 1990 17:04:19 GMT"); //Random date
%>

<%-- Redfine the fcTrack function to post the events instead of queueing them up. --%>
fcTrack = function(eventJson) {
    doAsyncEventPost(eventJson);
};

function flushTrackerEventQueue() {
    if ("_fcTrackerEventQueue" in window) {
        for (var i = 0; i < _fcTrackerEventQueue.length; ++i) {
            var eventJson = _fcTrackerEventQueue[i];
            doAsyncEventPost(eventJson);
        }
    }
}

function doAsyncEventPost(eventJson) {
    $.ajax({
        type: "POST",
        url: "<%=TrackerController.EVENT_ACTION%>",
        data: eventJson,
        async: true
    });
}

<%-- Flush the event queue so that all piled up events waiting for this response are sent to the server.--%>
flushTrackerEventQueue();

<%-- Push client details json loaded in home page --%>
if ("TRACKER_CLIENT_DATA" in window) {
    $.ajax({
        type: "POST",
        url: "<%=TrackerController.CLIENT_DETAILS_ACTION%>",
        data: TRACKER_CLIENT_DATA,
        async: true
    });
}