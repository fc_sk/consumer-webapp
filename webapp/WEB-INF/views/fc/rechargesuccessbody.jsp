<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="fc-card" id="recharge-status-container">
    <div class="status recharge-success">
        <h2>Recharge Successful</h2>
        <p>*High five*</p>
        <p>Yay! Your recharge was <span class="text-color-2"><strong>successful</strong></span>. </p>
        <p>Here are your recharge details: </p>
        <br>
        <p>Your Order ID for this transaction is - <strong>${rechargeInitiateWebDo.orderId}</strong></p>
    </div>
    <h6>Btw, your recharge is happening as we speak, but in case the process is not successful, don&#39;t you worry, the money will be safely refunded to your Credits.</h6>
    <div class="recharge-info clearfix">
        <div class="operator-logo">
            <img src="${mt:keyValue("imgprefix1")}/${operatorImgUrl}" title="${operator}" alt="operator-img">
        </div>
        <div class="service-number">
            <div><c:out value="${rechargeInitiateWebDo.mobileNo}"/></div>
            <p>${product} | ${operator} | ${circle}</p>
        </div>
        <div class="amount">
            Rs. <c:out value="${rechargeInitiateWebDo.amount}"/>
        </div>
    </div>
</div>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/facebook.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/ardeal.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">
    _gaq.push(['_trackEvent', 'Recharge Status', 'IN Success']);
    var _lookupId = "${lookupId}";
    $(document).ready(function(){
        ARDEAL = new ArDeal({hookpoint: "page4", lookupid: _lookupId}).
                checkArDeal(function() {
                    ARDEAL.showArDeal();
                });
    });

</script>
