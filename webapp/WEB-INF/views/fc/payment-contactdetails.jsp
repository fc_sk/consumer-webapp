<c:set var="contactdetails" value="${data}" scope="request" />

<div class="confirm-address box row">
    <h2>Shipping details</h2>
    <div id="editaddr-link" class="edit">
        <a href="#editforaddress">Click here to edit</a>
    </div>
    <div class="layer">
        <p id="addresssuccessmessage" class="message-success-small" style="display: none">error message</p>
        <div id="confirm-address-block" class="form">
            <div class="row name">
                <label>Name</label>
                <span id="confirm-title">
                    <c:if test="${!(contactdetails.title eq 'null') && (not empty contactdetails.title)}">
                        <c:out value="${contactdetails.title}"/>.
                    </c:if>
                </span>
                <span id="confirm-name">
                   <c:if test="${!(contactdetails.firstName eq 'null') && (not empty contactdetails.firstName)}">
                       <c:out value="${contactdetails.firstName}"/>
                   </c:if>
                </span>
            </div>

            <div class="row address">
                <label >Address</label>
                <span id="confirm-address">
                    <c:if test="${!(contactdetails.address1 eq 'null') && (not empty contactdetails.address1)}"><c:out value="${contactdetails.address1}"/>,</c:if>
                </span>
                <span>
                    <c:if test="${!(contactdetails.area eq 'null') && (not empty contactdetails.area)}"><c:out value="${contactdetails.area}"/>,</c:if>
                    <c:if test="${!(contactdetails.landmark eq 'null') && (not empty contactdetails.landmark)}"><c:out value="${contactdetails.landmark}"/>,</c:if>
                    <c:if test="${!(contactdetails.cityName eq 'null') && (not empty contactdetails.cityName)}">
                        <c:out value="${contactdetails.cityName}"/>,
                    </c:if>
                    <c:if test="${!(contactdetails.postalCode eq 'null') && (not empty contactdetails.postalCode)}"><c:out value="${contactdetails.postalCode}"/></c:if></span>
                </span>
            </div>
        </div>
    </div>
</div>

<%-- change address --%>
<div class="lightbox confirm-address-edit-block" id="editforaddress" style="display:none;">
    <h3>Add/Edit your shipping details</h3>
    <div class="form">
        <p>*This address is only for this one time. Any change here will not be updated in the your registered address.</p>
        <form action="#" name="shippingaddressform" id="shippingaddressform">
            <input type="hidden" name="mobile-for-communication" id="mobile-for-communication" value="${fn:escapeXml(contactdetails.mobile)}"/>
            <input type="hidden" name="txnFulfilmentId" id="txnFulfilmentId" value=""/>
            <div class="row">
                <label>Full name</label>
                <select id="edit-title"  name="title" class="validate[required]" style="width: auto">
                    <option value="">Title</option>
                    <option value="Mr">Mr.</option>
                    <option value="Ms">Ms.</option>
                    <option value="Mrs">Mrs.</option>
                </select>
                <c:choose>
                    <c:when test="${(contactdetails.firstName eq 'null') || !(not empty contactdetails.firstName)}">
                        <input type="text" id="edit-firstName" name="firstName" value="" class="validate[required] validate[funcCall[fullName]] validate[custom[onlyLetterSp]] validate[minSize[4]] validate[maxSize[25]]" placeholder="Full name"  maxlength="25" size="25"/>
                    </c:when>
                    <c:otherwise>
                        <input type="text" id="edit-firstName" name="firstName" value="${fn:escapeXml(contactdetails.firstName)}" class="validate[required] validate[funcCall[fullName]] validate[custom[onlyLetterSp]] validate[minSize[4]] validate[maxSize[25]]" placeholder="Full name" maxlength="25" size="25"/>
                    </c:otherwise>
                </c:choose>
            </div>

            <div class="row">
                <label>Address</label>
                <c:choose>
                    <c:when test="${(contactdetails.address1 eq 'null') || !(not empty contactdetails.address1)}">
                        <input type="text" id="edit-address" name="address" class="validate[required,funcCall[buildingOrStreet]]" maxlength="50" placeholder="Address" value="" />
                    </c:when>
                    <c:otherwise>
                        <input type="text" id="edit-address" name="address" class="validate[required,funcCall[buildingOrStreet]]" maxlength="50" placeholder="Address" value="${fn:escapeXml(contactdetails.address1)}" />
                    </c:otherwise>
                </c:choose>
            </div>

            <div class="row">
                <label>Area</label>
                <c:choose>
                    <c:when test="${(contactdetails.area eq 'null') || !(not empty contactdetails.area)}">
                        <input type="text" id="edit-area" name="area" class="validate[required] validate[funcCall[area]]" placeholder="Area" value=""/>
                    </c:when>
                    <c:otherwise>
                        <input type="text" id="edit-area" name="area" class="validate[required] validate[funcCall[area]]" placeholder="Area"  value="${fn:escapeXml(contactdetails.area)}"/>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="row">
                <label>Landmark</label>
                <c:choose>
                    <c:when test="${(contactdetails.landmark eq 'null') || !(not empty contactdetails.landmark)}">
                        <input type="text" id="edit-landmark" name="landmark" placeholder="Landmark" value=""/>
                    </c:when>
                    <c:otherwise>
                        <input type="text" id="edit-landmark" name="landmark" value="${fn:escapeXml(contactdetails.landmark)}" />
                    </c:otherwise>
                </c:choose>
            </div>

            <div class="row">
                <label>State</label>
                        <span id="edit-state" name="edit-state">
                            <select name="stateId" id="edit-select-state" class="validate[required]" onchange="getCityList(id,'edit-city-select-span','cityId','edit-city')">
                                <option value="">Select state</option>
                                <c:forEach items="${stateMaster}" var="state">
                                    <c:choose>
                                        <c:when test="${state.stateMasterId eq contactdetails.stateId}">
                                            <option value="${state.stateMasterId}" selected="selected"> ${state.stateName}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${state.stateMasterId}"> ${state.stateName}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </span>
            </div>
            <div class="row">
                <label>City</label>
                        <span id="edit-city-select-span">
                            <select name ="cityId" id="edit-city" class="validate[required]">
                                <option value="">Select city</option>
                                <c:forEach items="${cityMaster}" var="city">
                                    <c:choose>
                                        <c:when test="${(city.cityName) eq (contactdetails.cityName)}">
                                            <option value="${city.cityMasterId}" selected="selected">${city.cityName}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${city.cityMasterId}">${city.cityName}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </span>
            </div>

            <div class="row">
                <label>Pincode</label>
                <c:choose>
                    <c:when test="${(contactdetails.postalCode eq 'null') || !(not empty contactdetails.postalCode)}">
                        <input type="text" id="edit-pin" name="postalCode" onkeypress="return isNumberKey(event,6,id)" class="validate[required] validate[funcCall[postalCode]] validate[custom[onlyNumberSp]] validate[minSize[6]] validate[maxSize[6]]" placeholder="Pin code" value="" maxlength="6"/>
                    </c:when>
                    <c:otherwise>
                        <input type="text" id="edit-pin" name="postalCode" onkeypress="return isNumberKey(event,6,id)" class="validate[required] validate[funcCall[postalCode]] validate[custom[onlyNumberSp]] validate[minSize[6]] validate[maxSize[6]]" placeholder="Pin code" value="${fn:escapeXml(contactdetails.postalCode)}" maxlength="6"/>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="row">
                <input type="button" class="buttonOne" value="Update details" onclick="saveAddress('saveorupdate')">
                <input type="button" class="" value="Cancel" onclick="editAddressCancel();">
                <img id="addressLoad_Img" style="display:none;" src="${mt:keyValue("imgprefix1")}/images/ajax-loader.gif"/>
            </div>
            <div class="row" id="address-error-div" style="display: none;"><span id="address-error" style="color:red;float: left;text-align: center;width: 100%;"></span></div>
        </form>
    </div>

</div>




<script type="text/javascript">
    var userValue = '';
    var userRechargeContactId='';
    var defaultProfile = '';
    var firstName = '';
    var userProfileId='';
    var txnfulfilmentId ='';
    var txnHomePageId = '';
    var isEdit = 'yes';
    var globalTitle = '';
    var globalCityStr = '';

    $(document).ready(function(){
        userValue = '${contactdetails.userId}';
        userRechargeContactId = '${contactdetails.userRechargeContactId}';
        firstName = "<c:out value='${contactdetails.firstName}'/>";
        defaultProfile = '${defaultProfile}';
        userProfileId = '${contactdetails.userProfileId}';
        txnfulfilmentId = '${contactdetails.txnFulfilmentId}'
        txnHomePageId = '${contactdetails.txnHomePageId}';
        //alert (userValue +" and "+txnfulfilmentId);
        globalTitle = "${contactdetails.title}";
        $('#edit-title').val(globalTitle);
        globalCityStr = $('#edit-city').text();
    });

</script>


    
<script type="text/javascript">
<!--
function saveAddress(actionparam){
    var flag = $('#shippingaddressform').validationEngine('validate');
    if(flag){
	       var address = $('#edit-address').val();
	       var area = $('#edit-area').val();
	       var landmark = $('#edit-landmark').val();
	       var cityId = $('#edit-city').val();
	       var stateId=$('#edit-select-state').val();
	       var countryId='';
	       var pincode=$('#edit-pin').val();
	       var txnhomeId=txnHomePageId;
	       var userId = userValue;
	       var fName = $('#edit-firstName').val();
	       var title = $('#edit-title').val();
	       if(stateId == "" || stateId == undefined){
	    	   stateId = 0;
	       }
	       if(countryId == "" || countryId == undefined){
	    	   countryId = 0;
	       }	       
	       var formData=$('#shippingaddressform').serialize()+'&transactionHomePageId='+txnhomeId+'&txnfulfilmentId='+txnfulfilmentId+'&userProfileId='+userProfileId+'&userRechargeContactId='+userRechargeContactId+'&userId='+userId+'&defaultProfile='+defaultProfile+'&action='+actionparam+'&isEdit='+isEdit+'';
	       var url='/app/saveAddress.htm'
	              $.ajax({
	              url: url,
	              type: "POST",
	              data: formData,
	              dataType: "json",
	              cache: false,
	              timeout:1800000,
	              success: function (responseText) {
	                  if(responseText.status == 'success'){
	                      var obj = responseText.addressWebDO;
	                      var response_title=obj.title;
	                      var response_firstName=obj.firstName; 
	                      var response_address=obj.address;
	                      var response_area = obj.area;
	                      var response_landmark = obj.landmark;
	                      var response_city=obj.city;
	                      var response_cityid = obj.cityId;
	                      var response_postalcode=obj.postalCode;
	                      var response_countryid=obj.countryId;
	                      var response_stateid=obj.stateId;
	                      var stateMaster=obj.stateMaster;
	                      var countryMaster=obj.countryMaster;
	                      userProfileId=obj.userProfileId;
	                      txnfulfilmentId=obj.txnfulfillmentId;
	                      defaultProfile = obj.defaultProfile;
	                      isEdit = obj.isEdit;
	                      globalTitle = response_title;
	                      $('#confirm-name').text(response_firstName);
	                      $('#confirm-address').text(response_address+', '+response_area+', '+response_landmark+', '+response_city+', '+response_postalcode);
	                      var titleselectstr = '<select name="title" id="edit-title" class="validate[required] titleSelect softNormText fLeft">'+'';
	                      $("#edit-title option").each(function(){
	                    	  var titName=($(this).text());
	                          var titId=($(this).val());
	                          if(response_title == titId){
	                        	  titleselectstr = titleselectstr + '<option value="'+titId+'" selected="selected" >'+titName+'</option>';
	                          }else{
	                        	  titleselectstr = titleselectstr + '<option value="'+titId+'" >'+titName+'</option>';
	                          }
	                      });
	                      titleselectstr = titleselectstr + '</select>';
	                      $('#confirm-title').text(response_title+".");
	                      $('#editforaddress').hide();
	                      $('#confirm-address-block').show();
	                      $('#editaddr-link').show();
	                      
	                      var stateselectstr = '<select name="stateId" id="edit-select-state" class="validate[required] select1 softNormText fLeft" onchange="getCityList(id,\'edit-city-select-span\',\'ship-city\',\'edit-city\')">'+
	                                           '';
	                      $("#edit-select-state option").each(function(){
	                                var stName=($(this).text());
	                                var stId=($(this).val());
	                                if(response_stateid == stId){
	                                    stateselectstr=stateselectstr+ '<option value="'+stId+'" selected="selected">'+stName+'</option>';
	                                }else{
	                                    stateselectstr=stateselectstr+ '<option value="'+stId+'">'+stName+'</option>';
	                                }
	                              });
	                      stateselectstr = stateselectstr + '</select>';
	                      var cityoptionsstr = '';
	                      $("#edit-city option").each(function(){
	                    	  var ctName = $(this).text();
	                    	  var ctvalue = $(this).val();
	                    	  if((response_cityid) == (ctvalue)){
	                    		  cityoptionsstr = cityoptionsstr + '<option value="'+ctvalue+'" selected="selected">'+ctName+'</option>';
	                    	  }else{
	                    		  cityoptionsstr = cityoptionsstr + '<option value="'+ctvalue+'">'+ctName+'</option>';
	                    	  }
	                      });
	                      cityoptionsstr = cityoptionsstr + '';
	                      globalCityStr = cityoptionsstr;

	 					 $('#addresssuccessmessage').text("Address updated successfully.");
	 					 $('#addresssuccessmessage').show();
	                        $('#shippingaddressform').validationEngine({
	                	        ajaxFormValidation: true ,
	                	        scroll:false
	                	    });

	
	                  }else if(responseText.status == 'fail'){
	                      $('#address-error').text("Technical error occured. Please try after some time.");
	                      $('#address-error-div').show();
	
	                  }else if(responseText.status == 'required'){
	                	  $('#address-error').html(responseText.errors);
	                	  $('#address-error-div').show();
	                  }else if(responseText.status == 'sessionExpired'){
	                	  $('#address-error').text("Session expired. Please login to proceed.");
	                	  $('#address-error-div').show();
	                  }

	                  $.fancybox.close();
	
	                 },
	             error:function (xhr,status,error){
                      $('#address-error').text("Technical error occured. Please try after some time.");
                      $('#address-error-div').show();
	                 }
	            });
 	}
}

 function saveTxnAddress(){
     saveAddress('pay');
 }
 function editAddressCancel() {
 	$('#shippingaddressform').validationEngine('hide');
     $('#address-error').text('');
     $('#editforaddress').hide();
     //document.shippingaddressform.reset();
     //$('#edit-title').val(globalTitle);
     //$('#edit-city').text(globalCityStr);
     $.fancybox.close();
     $('#confirm-address-block').show();
     $('#editaddr-link').show();
 }

//-->
</script>


