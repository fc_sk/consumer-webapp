<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@page import="java.util.Map"%>
<%@page import="com.freecharge.common.util.FCConstants"%>
<%@page import="com.freecharge.web.webdo.CommonSessionWebDo"%>
<%@page import="com.freecharge.web.util.WebConstants"%>
<%@page import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<%@page import="com.freecharge.common.framework.session.FreechargeSession"%>
<%@ page import="com.freecharge.payment.util.PaymentConstants" %>
<%@ page import="com.freecharge.web.controller.ProductPaymentController" %>

<%
    FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    if (fs != null) {
        Map<String, Object> sessionData = fs.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
    }
    String lookId= request.getParameter("lid");
    String prodType = FCSessionUtil.getRechargeType(lookId, fs);
    Integer prodId = FCConstants.productMasterMapReverse.get(prodType);
%>
<c:set value="<%=login%>" var="login"/>

<c:set var="cartItems" value="${cart.itemsList}" scope="request"/>
<c:set var="primaryProductType" value='<%=FCConstants.PRIMARY_PRODUCT_TYPE%>' scope="application"/>
<c:set var="lid" value='<%=request.getParameter(FCConstants.LOOKUPID)%>' />
<c:set var="lookupID" value="<%=request.getParameter(\"lookupID\")%>" scope="request" />

<c:choose>
    <c:when test="${productType eq 'V' }">
        <c:set var="productName" value="Mobile" scope="request" />
    </c:when>
    <c:when test="${productType eq 'D' }">
        <c:set var="productName" value="DTH" scope="request" />
    </c:when>
    <c:when test="${productType eq 'C' }">
        <c:set var="productName" value="Data Card" scope="request" />
    </c:when>
</c:choose>

<div class="recharge-summary box">
    <h2>Recharge details</h2>
    <div class="edit"><a href="/app/edit.htm?lid=${lid}">Edit recharge details</a></div>
    <div class="layer">
        <c:forEach items="${cartItems}" var="items" varStatus="summaryCtr">
            <c:if test="${items.productType eq primaryProductType }">
                <c:if test="${!(items.itemTableName eq crosssellEntityName)}">
                    <div class="item clearfix">
                        <div class="operator">
                            <img src="${mt:keyValue("imgprefix1")}/${operatorImgUrl}" alt="${operatorName}" />
                        </div>
                        <div class="number">
                            ${serviceNumber}
							<p>${productName} | ${operatorName}
                                <c:if test="${productType != 'D'}">
                                | ${circleName}
                                </c:if>
                            </p>
                        </div>
                        <div class="amount">
                            <span class="webRupee">Rs.</span>${items.price}
							<%--<p>Talktime: 333 | Validity: 30 days</p>--%>
                        </div>
                        <%--<div><em>${operatorName}</em></div>--%>
                    </div>
                </c:if>
            </c:if>
        </c:forEach>
    </div>
</div>