<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
	<head>
		<title>FreeCharge.in - Recharge Made Free</title>

<link rel="apple-touch-icon" href="${mt:keyValue("imgprefix1")}/images/favicon-new.ico"/>
<link rel="icon" href="${mt:keyValue("imgprefix1")}/images/favicon-new.ico" type="image/x-icon">
<link rel="shortcut icon" href="${mt:keyValue("imgprefix1")}/images/favicon-new.ico" type="image/x-icon">
<link href="${mt:keyValue("cssprefix1")}/base.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css" >	
<link href="${mt:keyValue("cssprefix1")}/layout.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
<link href="${mt:keyValue("cssprefix2")}/invoice.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
<link href="${mt:keyValue("cssprefix1")}/ie6.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
        
         <!--[if lt IE 7]>
			<link rel="stylesheet" media="all" href="css/ie6.css?v=${mt:keyValue("version.no")}"/>
		<![endif]-->
 		</head>
		<body id="invoiceWrapper"  >
            	<div class="pageWidth" style="width:963px">
                    <h1 class="invoicePageHeader" ><strong>Thank you!!! </strong>for using www.freecharge.in</h1>
                    <div class="invvoiceMainConatienr" >
                    
                        <div class="invoiceCont" >
                        
                                <div class="invoiceContHeader clearfix" >
                                    <h4  class="titleMain fLeft" >TAX INVOICE</h4>
                                    <a href="#" class="invoiceLogo fRight" ><img src="${mt:keyValue("imgprefix1")}/images/logo/freecharge.png" alt="FreeCharge.in - Recharge Made Free" width="151" height="34" ></a>
                                </div> <!-- /.invoiceContHeader -->
                                
                                <div class="invoiceContMiddle clearfix" >
                                    <div class="detailsOuter clearfix" >
                                        <ul class="detailsList fLeft" >
                                       		 <li>
                                                <span class="ltTxt" >Customer Name</span>
                                                <span class="rtTxt" >:&nbsp;  <c:out value="${taxInvoiceDetails.userName}"/></span>
                                            </li>
                                            <li>
                                                <span class="ltTxt" >Order Id</span>
                                                <span class="rtTxt" >:&nbsp;  <c:out value="${taxInvoiceDetails.orderId}"/></span>
                                            </li>
                                            <li>
                                                <span class="ltTxt" >Invoice no</span>
                                                <span class="rtTxt" >:&nbsp;  <c:out value="${taxInvoiceDetails.invoiceId}"/></span>
                                            </li>
                                            <li>
                                                <span class="ltTxt" >Mobile no</span>
                                                <span class="rtTxt" >:&nbsp;  <c:out value="${taxInvoiceDetails.mobileNo}"/></span>
                                            </li>
                                            <c:if test="${taxInvoiceDetails.serviceNumber!=null}">
                                            <li>
                                                <span class="ltTxt" >Recharge done for</span>
                                                <span class="rtTxt" >:&nbsp;  <c:out value="${taxInvoiceDetails.serviceNumber}"/></span>
                                            </li>
                                            </c:if>
                                            <li>
                                                <span class="ltTxt" >Registered Email</span>
                                                <span class="rtTxt" >:&nbsp;  <c:out value="${taxInvoiceDetails.emailId}"/></span>
                                            </li>
                                            <li>
                                                <span class="ltTxt" >Invoice Date </span>
                                                <span class="rtTxt" >:&nbsp;  <c:out value="${taxInvoiceDetails.invoiceDate}"/></span>
                                            </li>
                                            <li>
                                                <span class="ltTxt" >Account Number </span>
                                                <span class="rtTxt" >:&nbsp;  
                                                	<c:out value="3820192000"/>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="ltTxt" >Service Accounting code </span>
                                                <span class="rtTxt" >:&nbsp;  <c:out value="00998599"/></span>
                                            </li>
                                        </ul> <!-- /.detailsList -->
                                           
                                        <div class="paymentTable fRight" >
                                           <table width="100%" border="0" cellspacing="0" cellpadding="0" class="invoiceTable" >
                                                <tr>
                                                    <th width="11%"  class="center"  >S.No</th>
                                                    <th width="55%"  >Particular</th>
                                                    <th width="34%" >Amount</th>
                                                </tr>
                                              <tr>
                                                <td class="center" >1.</td>
                                                    <td> Transaction  Amount</td>
                                                    <td>Rs. <c:out value="${taxInvoiceDetails.invoiceInfo.billAmount}"/> </td>
                                               </tr>
                                               <tr>
                                               <td class="center" >2.</td>
                                                   <c:choose>
                                                       <c:when test="${isCoupon}">
                                                           <td>FreeCharge Checkout Fee</td>
                                                       </c:when>
                                                       <c:otherwise>
                                                           <td> Payment Handling Charges</td>
                                                       </c:otherwise>
                                                   </c:choose>
                                                   <td>Rs. <c:out value="${taxInvoiceDetails.invoiceInfo.handlingCharges}"/> </td>
                                               </tr>
                                               <c:if test="${taxInvoiceDetails.invoiceInfo.taxableValue!=null}">
                                               <tr>
                                               <td class="center" >3.</td>
                                                    <td> Taxable Value of Supply</td>
                                                    <td>Rs. <c:out value="${taxInvoiceDetails.invoiceInfo.taxableValue}"/> </td>
                                               </tr> 
                                                <tr>
                                               <td class="center" >4.</td>
                                                    <td> CGST @9%</td>
                                                    <td>Rs. <c:out value="${taxInvoiceDetails.invoiceInfo.cgst}"/> </td>
                                               </tr> 
                                                <tr>
                                               <td class="center" >5.</td>
                                                    <td> SGST @9%</td>
                                                    <td>Rs. <c:out value="${taxInvoiceDetails.invoiceInfo.sgst}"/> </td>
                                               </tr> 
                                                <tr>   
                                                    <td colspan="2"> Total Amount</td>
                                                    <td>Rs. <c:out value="${taxInvoiceDetails.invoiceInfo.totalAmount}"/> </td>
                                               </tr> 
                                               <tr>   
                                                    <td colspan="2"> Whether the tax is payable on reverse charge basis</td>
                                                    <td>No</td>
                                               </tr> 
                                                </c:if>
                                          </table> <!-- /.invoiceTable -->
                                        </div> <!-- /paymentTable -->
                                    </div>    
                                    
                                    <p class="contactDetails cl" style="font-size:12px">
                                    	<c:if test="${accountType=='aspl'}">
	                                        Accelyst Solutions Pvt Ltd   
                                            <br>Registered Address: 01st Floor, Corporate Park II Building, Sion Trombay Road, Next to Swastik chambers, Mumbai -400071, Maharashtra
	                                        <br>Address: 16th Floor, DLF Cyber Greens, Tower-C, Phase-III, DLF Cyber City, Gurugram-122002
	                                        <br>Company Pan No. AAHCA2192H
	                                        <br>CIN: U72900MH2008PTC185202
	                                        <br>State Code: 06(Haryana)
	                                        <br>GSTIN: 06AAHCA2192H1ZS	                                        
	                                        <br>Service Category: Other Support Services
	                                        <br>Contact us at care@freecharge.com
	                                        <br>Website: www.freecharge.com
                                        </c:if>
                                         <c:if test="${accountType=='fcptl'}">
	                                          Freecharge Payment Technologies Pvt. Ltd
	                                          <br>Registered Address: 2nd Floor, Plot No. 25, Pusa Road, New Delhi-110005 
	                                          <br>Address- DLF Cyber Greens, 16th Floor, Tower-C, DLF Cyber City, DLF Phase-III, Gurugram,Haryana-122002, India
	                                          <br>CIN: U74140DL2015PTC275419
	                                          <br>Website: www.freecharge.com
	                                          <br>State Code: 06
	                                          <br>GSTIN: 06AAFCK7217F1ZO
	                                          <br>Company PAN No. AAFCK7217F
	                                          <br>Service Category: Other Support Services
	                                          <br>Contact us at care@freecharge.com
                                         </c:if>
                                        
                                    </p> <!-- /.contactDetails -->
                                    <p class="contactDetails cl" style="text-align:right"> <img src="https://d32vr05tkg9faf.cloudfront.net/content/images/invoice/SignInvoice.png"> <br> Authorised Signatory
                                    </p>
                                </div> <!-- /.invoiceContMiddle -->
                        </div><!-- /.invoiceCont -->
                     </div><!-- /.invvoiceMainConatienr -->   
                </div> <!-- /.pageWidth -->
		</body>
</html>
