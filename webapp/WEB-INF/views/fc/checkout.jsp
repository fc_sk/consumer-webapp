<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeContextDirectory" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ page import="com.freecharge.web.webdo.CommonSessionWebDo" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
    FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    if (fs != null) {
        Map<String, Object> sessionData = fs.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
    }
    
    String lookId= request.getParameter("lid");
    String lid= request.getParameter("lid");
    String lookupid = request.getParameter("lookupID");
    
    if(lookupid == null && lid != null){
          lookupid = lid;
    }

    String productType = FCSessionUtil.getRechargeType(lookupid, fs);
    Integer productId = FCConstants.productMasterMapReverse.get(productType);
%>
<c:set value="<%=login%>" var="login"/>

<c:set var="primaryProductType" value='<%=FCConstants.PRIMARY_PRODUCT_TYPE%>' scope="application"/>
<c:set var="secondaryProductType" value='<%=FCConstants.SECONDARY_PRODUCT_TYPE%>' scope="application"/>
<c:set var="chargesProductType" value='<%=FCConstants.CHARGES_PRODUCT_TYPE%>' scope="application"/>
<c:set var="crosssellEntityName" value='<%=FCConstants.ENTITY_NAME_FOR_CROSSSELL%>' scope="application"/>
<c:set var="couponEntityName" value='<%=FCConstants.COUPON_ENTITY_ID%>' scope="application"/>
<c:set var="lid" value='<%=request.getParameter(FCConstants.LOOKUPID)%>' />

<c:set var="lookupID" value="<%=request.getParameter(\"lookupID\")%>" scope="request" />
<c:set var="walletDisplayName" value="<%=FCConstants.WALLET_DISPLAY_NAME%>"></c:set>

<input type="hidden" id="productType" name="productType" value="<%=productType%>"/>
<input type="hidden" value="2" name="pagecount" id="pagecount"/>

<div class="twelve columns">
    <h2 class="page-header">Payment options</h2>

    <%--freecharge payment assurance--%>
    <div class="payment-benefits">
        <ul class="clearfix">
            <li class="payment-benefit-one"><span>Fast and easy</span></li>
            <li class="payment-benefit-two"><span>Safe and trusted</span></li>
            <li class="payment-benefit-three"><span>Multiple payment options</span></li>
            <li class="payment-benefit-four"><span>Over a million satisfied customers</span></li>
        </ul>
    </div>

    <%-- Payment options page --%>
    <jsp:include page="payment-options.jsp"></jsp:include>
</div>
<div class="four columns">
    <%@include file="../binoffer/binoffer_list.jsp"%>

    <div class="security-information">
        <div class="geotrust">
            <!-- GeoTrust QuickSSL [tm] Smart  Icon tag. Do not edit. -->
            <script language="javascript" type="text/javascript" src="//smarticon.geotrust.com/si.js"></script>
            <!-- end  GeoTrust Smart Icon tag -->
        </div>

        <div class="pci-compliant-seal">
            <a href="javascript:CCPopUp('http://seal.controlcase.com/', 4252874532);">
            <img src='${mt:keyValue("imgprefix1")}/images/PCI_logo.gif' oncontextmenu='return false;' border='0'></a></br>
        </div> 
        
        <div class="symantecSSL-logo" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications." >
        	<script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.freecharge.in&amp;size=M&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script>
        </div>

        <div class="other">
            <img src="${mt:keyValue("imgprefix1")}/images/icons/security-other.png" alt="Verified by Visa, MasterCard Secure" />
        </div>

    </div>
</div>
