<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@page errorPage="/WEB-INF/views/fc/error/error.jsp"%>
<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ page import="com.freecharge.common.util.SpringBeanLocator" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeContextDirectory" %>
<%@ page import="java.util.Map,java.util.List" %>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="springF" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ page import="com.freecharge.web.webdo.CommonSessionWebDo" %>
<%@ page import="java.util.Date" %>

<%
    FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    if (fs != null) {
        Map<String, Object> sessionData = fs.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
        if (login == null)
            login = false;
    }
%>

<%
    FreechargeSession freechargesession = FreechargeContextDirectory.get().getFreechargeSession();
    String lookupid = request.getParameter("lookupID");
    String lid = request.getParameter("lid");
    if (lookupid == null && lid != null) {
        lookupid = lid;
    }
    String userEmail = (String) freechargesession.getSessionData().get("userName");
    String productType = "";
    productType = FCSessionUtil.getRechargeType(lookupid, freechargesession);
    Integer productId = FCConstants.productMasterMapReverse.get(productType);
%>

<c:set  value="<%=login%>" var="login" />
<input type='hidden' id='user-email' value="<%=userEmail%>">
<c:choose >
    <c:when test="${seoAttributes.productId != null}" >
        <script type="text/javascript">
            $("#freeCharge").data('product-type', '${seoAttributes.productId}');
            // var proId = '${seoAttributes.productId}';
        </script>
        <c:set var="displayProductId" value="${seoAttributes.productId}" />
        <c:set var="displayTabOperatorName" value="${seoAttributes.operatorName}" />
    </c:when>
    <c:otherwise>
        <script type="text/javascript">
            $("#freeCharge").data('product-type', '${homeWebDo.tabId}');
            // var proId = '${homeWebDo.tabId}';
        </script>
        <c:set var="displayProductId" value="${homeWebDo.tabId}" />
        <c:set var="displayTabOperatorName" value="${homeWebDo.displayOperatorName}" />
    </c:otherwise>
</c:choose>


<%--SUN DTH amounts--%>
<%
    Integer[] sunDirectDenominationList = { 25, 30, 40, 100, 155, 185, 200, 210, 220, 300, 400, 440, 500, 525,
            560, 600, 850, 990, 999, 1000, 1210, 1600, 1900 };
    //Integer[] tataPhotonDenominationList = {100, 255, 365, 450, 500, 730, 775, 850, 1100, 1201};
    request.setAttribute("sunDirectDenominationList", sunDirectDenominationList);
    //request.setAttribute("tataPhotonDenominationList", tataPhotonDenominationList);
%>
    <div class="twelve columns">
        <div class="row rechargeWrap" id="services-wrap">
         <ul class="rechargeNav clearfix" id="services-nav">
            <c:if test="${isFestiveGiftEnabled}"> <%-- Visible if Festive Gift on --%>
            <li class="festive-recharge-link">
                <a href="#" data-product-type="FV" >Gift a recharge</a>
            </li>
            </c:if>
            <li class="active">
                <a href="#" data-product-type="V" class="mobile"><spring:message code="mobile" text="#mobile#"/></a>
            </li>
            <li>
                <a href="#" data-product-type="D" class="dth"><spring:message code="dth" text="#dth#"/></a>
            </li>
            <li>
                <a href="#" data-product-type="C" class="datacard"><spring:message code="datacard" text="#datacard#"/></a>
            </li>
        </ul>

        <div class="rechargeNav_content form clearfix" id="services-content">            
            <%--mobile recharge--%>
            <div class="rechargeForm clearfix" id="mobile-tab">
                <h2 class="unit-title"><spring:message code="recharge" text="#recharge#"/> <span id="titleSubstr-mobile"><c:if test='${displayProductId eq 1}' >${displayTabOperatorName}</c:if></span> <spring:message code="prepaid.mobile.online" text="#prepaid.mobile.online#"/></h2>
				<h2 class="unit-title festive-unit-title" style="display:none;" >This Diwali, gift a recharge to your friends and family</h2>

                <div class="previous-recharges">
                    <p class="title">Previous Successful Recharges:</p>
                    <ul class="previous-recharges-list"></ul>
                </div>

                <form:form modelAttribute="homeWebDo" action="javascript:void(0)" method="post" name="mobileForm" id="mobileForm">
                    <form:hidden path="type" value="V"/>
                    <ul class="form clearfix" id="recharge-mobile">
                        <li class="field-unit number">
                            <label class="rechargForm-label">Mobile Number</label>
                            <div>
                                <span>+91</span>
                                <form:input path="mobileNumber" type="text"                                 
                                maxlength="10"
                                id="number-mobile"
                                value="${fn:escapeXml(homeWebDo.mobileNumber)}"
                                cssClass="validate[required,custom[onlyNumberSp],funcCall[mobileNoRequired]] number-entry"
                                onkeypress="return isNumberKey(event);"  
                                placeholder="Mobile Number"
                                data-product-type="V" />
                            </div>
                            <form:errors path="mobileNumber"/>
                        </li>
                        <li class="field-unit operator">
                            <label class="rechargForm-label">Mobile Operator</label>
                            <form:select path="operator"
                                id="operator-mobile"
                                tabindex="1"
                                cssClass="validate[required]"
                                onchange="changeOperatorParams(this.options[this.selectedIndex].innerHTML,'mobile',this.options[this.selectedIndex].value)">
                                <form:option value="">Select operator</form:option>
                                <c:forEach items="${homeWebDo.operatorList}" var="itr">
                                    <c:if test="${itr.productName=='Mobile'}">
                                        <option value="${itr.operatorMasterId}"
                                        <c:if test="${(fn:toUpperCase(itr.operatorName) eq fn:toUpperCase(seoAttributes.operatorName)) || (fn:toUpperCase(itr.operatorCode) eq fn:toUpperCase(homeWebDo.displayOperatorName)) || (fn:toUpperCase(itr.operatorName) eq fn:toUpperCase(homeWebDo.displayOperatorName))}">
                                        selected='selected'</c:if>>${itr.operatorName}</option>
                                    </c:if>
                                    </c:forEach>
                                </form:select>
                        </li>

                        <li class="field-unit circle" style="display: none">
                            <label class="rechargForm-label">Mobile Circle</label>
                            <form:select name="circle" path="circle"
                                id="circle-mobile"
                                class="validate[required] circle-entry" >
                                <form:option value="">Select circle</form:option>
                                <c:forEach items="${homeWebDo.circleList}" var="itr">
                                    <c:if test="${itr.name!='ALL'}">
                                        <option value="${itr.circleMasterId}" 
                                            <c:if test="${itr.circleMasterId eq homeWebDo.circle}">selected="selected"</c:if>
                                        >${itr.name}
                                        </option>
                                    </c:if>
                                </c:forEach>
                            </form:select>
                        </li>

                        <li class="field-unit rechargetype"  style="display:none">
                            <label class="rechargForm-label">Recharge Type</label>
                            <select id='recharge-type'>
                                <option value='topup' selected>Top Up</option>
                                <option value='special'>Special Recharge</option>
                            </select>
                        </li>
                        
                        <li class="field-unit amount">
                            <label class="rechargForm-label">Recharge Amount</label>
                            <div>
                                <span>Rs. </span>
                                <form:input path="amount"
                                cssClass="validate[required,custom[onlyNumberSp],funcCall[maxMobileRecAmt]]"
                                id="amount-mobile"
                                onkeypress="return isNumberKey(event);" type="text"
                                value="${fn:escapeXml(homeWebDo.amount)}" 
                                placeholder="Amount" 
                                maxlength="4" />
                                <form:errors path="amount"/>
                            </div>
                            </li>
                            <li class="field-unit">
                                <p class="festive-message-info" style="display:none;"  >Type the personalised SMS you wish to send</p>
                                <div class="festive-message" style="display:none;" >
                                    <form:textarea path="festiveMessage"  maxlength="160"  
                                         placeholder="Your Message" cssClass="validate[required]" />
<!-- This festive season I decided to gift you a recharge (via FreeCharge.in) that will help you keep in touch with your loved ones. Enjoy! ~ &lt;sender&#39;s name&gt; -->
									<input type="hidden" id="hidden-festiveMessageFlag" value="0" name="itemIdArray"/>
							</div>
                            
                            <em class="recharge-button">
                                <!-- <input id="mobile-button" value="Proceed" data-product-type="V" type="submit" onclick="_gaq.push(['_trackEvent', 'Button', 'Proceed', 'Mobile']);" class="button buttonThree" /> -->
                                <button id="mobile-button" data-product-type="V" onclick="_gaq.push(['_trackEvent', 'Button', 'Proceed', 'Mobile']);" class="buttonThree">Proceed</button>
                            </em>

                        </li>                          
                    </ul>
                </form:form>
                <p class="popular-plans"><a href="#rechargePlansList" class="popular-plans-link">See Popular Plans</a></p>
            </div>

            <%--DTH recharge--%>
            <div class="rechargeForm dth clearfix" style="display:none" id="dth-tab">
                <h2 class="unit-title"><spring:message code="recharge" text="#recharge#"/>
                    <span id="titleSubstr-dth">
                    <c:choose>
                        <c:when test="${displayProductId eq 2 && displayTabOperatorName!= ''}">${displayTabOperatorName}</c:when>
                        <c:otherwise><spring:message code="dth" text="#dth#"/></c:otherwise>
                    </c:choose>
                    </span>
                    <spring:message code="online" text="#online#"/></h2>

                    <div class="previous-recharges">
                        <p class="title">Previous Successful Recharges:</p>
                        <ul class="previous-recharges-list"></ul>
                    </div>

                <form:form modelAttribute="homeWebDo" action="javascript:void(0)"
                    method="post" name="dthForm" id="dthForm">
                <form:hidden path="type" value="D"/>
                <form:hidden path="circle" name="circle"/>
                
                    <ul class="form clearfix" id="recharge-dth">
                    <li class="field-unit operator">
                        <label class="rechargForm-label">DTH operator</label>
                        <form:select path="operator"
                        id="operator-dth"
                        cssClass="validate[required] input2"
                        onchange="changeOperatorParams(this.options[this.selectedIndex].innerHTML,'dth',this.options[this.selectedIndex].value)">
                        <form:option value="">Select operator</form:option>
                        <c:forEach items="${homeWebDo.operatorList}" var="itr">
                            <c:if test="${itr.productName=='DTH'}">
                            <option value="${itr.operatorMasterId}"
                            <c:if test="${(fn:toUpperCase(itr.operatorName) eq fn:toUpperCase(seoAttributes.operatorName)) || (fn:toUpperCase(itr.operatorCode) eq fn:toUpperCase(homeWebDo.displayOperatorName)) || (fn:toUpperCase(itr.operatorName) eq fn:toUpperCase(homeWebDo.displayOperatorName))}">
                            selected='selected'
                            </c:if>>${itr.operatorName}</option></c:if>
                        </c:forEach>
                        </form:select>
                    </li>
                    <li class="field-unit number">
                        <label class="rechargForm-label">DTH Number</label>
                        <form:input path="dthNumber"
                        id="number-dth"
                        value="${fn:escapeXml(homeWebDo.dthNumber)}"
                        cssClass="validate[required,custom[onlyNumberSp]]" 
                        placeholder="DTH Number" 
                        onkeypress="return isNumberKey(event);" maxlength="14"/>                        
                    </li>
                    <li class="field-unit amount">
                        <label class="rechargForm-label">Recharge Amount</label>
                        <div>
                            <span>Rs. </span>
                            <form:input path="dthAmount"
                            id="amount-dth"
                            cssClass="validate[required,custom[onlyNumberSp]]"
                            type="text" value="${fn:escapeXml(homeWebDo.dthAmount)}"
                            onkeypress="return isNumberKey(event);" maxlength="4" placeholder="Amount" />
                            <select id="amount-dth-suntv" name="dthAmount" style="display:none;">
                                <c:forEach var="dthAmountItem" items="${sunDirectDenominationList}">
                                    <option value="${dthAmountItem}"
                                        <c:if test="${dthAmountItem eq fn:escapeXml(homeWebDo.dthAmount)}">selected='selected'</c:if>>${dthAmountItem}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <em class="recharge-button">
                            <button id="dth-button" class="buttonThree" data-product-type="D" onclick="_gaq.push(['_trackEvent', 'Button', 'Proceed', 'DTH']);">Proceed</button>
                        </em>
                    </li>                    
                </ul>
                <div id="dthRechargeInfo" class="dth-helper-text"></div>
            </form:form>
            </div>

            <%--Datacard recharge--%>
            <div class="rechargeForm clearfix" style="display:none" id="datacard-tab">
                <h2 class="unit-title"><spring:message code="recharge" text="#recharge#"/>
                    <span id="titleSubstr-dataCard">
                       <c:choose>
                       <c:when test="${displayProductId eq 3 && displayTabOperatorName!= ''}" >${displayTabOperatorName}</c:when>
                       <c:otherwise><spring:message code="datacard.small" text="#datacard.small#"/></c:otherwise>
                    </c:choose>
                </span>
                <spring:message code="online" text="#online#"/></h2>

                <div class="previous-recharges">
                    <p class="title">Previous Successful Recharges:</p>
                    <ul class="previous-recharges-list"></ul>
                </div>


                <form:form modelAttribute="homeWebDo" action="javascript:void(0)"
                method="post" name="dataCardForm" id="dataCardForm">
                <form:hidden path="type" value="C"/>
                <ul class="form clearfix" id="recharge-datacard">
                    <li class="field-unit number">
                        <label class="rechargForm-label" for="number-datacard">Data Card Number</label>
                        <div>
                            <span>+91</span>
                            <form:input path="dataCardNumber"
                                        id="number-datacard"
                                        type="text"
                                        maxlength="10"
                                        value="${fn:escapeXml(homeWebDo.dataCardNumber)}"
                                        cssClass="validate[required,custom[onlyNumberSp],funcCall[dataCardNoRequired]]"
                                        onkeypress="return isNumberKey(event);"
                                        placeholder="Data Card No."
										data-product-type="C" />
                        </div>
                    </li>
                    <li class="field-unit operator">
                        <label class="rechargForm-label" for="operator-datacard">Data Card Operator</label>
                        <form:select path="operator"
                        id="operator-datacard"
                        cssClass="validate[required]"
                        onchange="changeOperatorParams(this.options[this.selectedIndex].innerHTML,'dataCard',this.options[this.selectedIndex].value)">
                        <form:option value="">Select operator</form:option>
                        <c:forEach items="${homeWebDo.operatorList}" var="itr">
                        <c:if test="${itr.productName=='DataCard'}">
                        <option value="${itr.operatorMasterId}"
                        <c:if test="${(fn:toUpperCase(itr.operatorName) eq fn:toUpperCase(seoAttributes.operatorName)) || (fn:toUpperCase(itr.operatorCode) eq fn:toUpperCase(homeWebDo.displayOperatorName)) || (fn:toUpperCase(itr.operatorName) eq fn:toUpperCase(homeWebDo.displayOperatorName))}">selected='selected'</c:if>>${itr.operatorName}</option>
                        </c:if>
                        </c:forEach>
                        </form:select>
                    </li>

                    <li class="field-unit circle" style="display: none">
                        <label class="rechargForm-label" for="circle-datacard">Data Card Circle</label>
                        <form:select path="circle"
                        id="circle-datacard"
                        class="validate[required]" >
                        <form:option value="">Select circle</form:option>
                        <c:forEach items="${homeWebDo.circleList}" var="itr">
                            <c:if test="${itr.name!='ALL'}">
                                <option value="${itr.circleMasterId}"
                                    <c:if test="${itr.circleMasterId eq homeWebDo.circle}">
                                    selected='selected'
                                </c:if>>${itr.name}</option>
                            </c:if>
                        </c:forEach>
                    </form:select>
                    </li>


                    <li class="field-unit amount">
                        <label class="rechargForm-label" for="amount-datacard">Recharge Amount</label>
                        <div>
                            <span>Rs. </span>
                            <form:input path="dataCardAmount"
                            cssClass="validate[required,custom[onlyNumberSp],funcCall[maxDataCardRecAmt]]"
                            id="amount-datacard"
                            value="${fn:escapeXml(homeWebDo.dataCardAmount)}"
                            maxlength="4"
                            onkeypress="return isNumberKey(event);" 
                            placeholder="Amount"/>
                            <%-- <select name="dataCardAmount" id="amount-datacard-tataphoton"  class="restricted-amount" style="display:none;">
                                <c:forEach var="dataCardAmountItem" items="${tataPhotonDenominationList}">
                                    <option value="${dataCardAmountItem}"
                                            <c:if test="${dataCardAmountItem eq fn:escapeXml(homeWebDo.dataCardAmount)}">
                                                selected='selected'
                                            </c:if>>${dataCardAmountItem}</option>
                                </c:forEach>
                            </select> --%>
                        </div>
                        <em class="recharge-button">
                            <button id="datacard-button" class="buttonThree" data-product-type="C" onclick="_gaq.push(['_trackEvent', 'Button', 'Proceed', 'Data Card']);">Proceed</button>
                        </em>
                    </li>                    
                </ul>
                </form:form>
                
            </div>

            <%--recharge plan list--%>
            <div id="rechargePlansList" style="display: none">
                <div class="lightbox recharge-plans">
                    <h3><spring:message code="popular.plans" text="#popular.plans#"/></h3>
                    <p class="disclaimer"><spring:message code="recharge.plan.notice" text="#recharge.plan.notice#"/>.</p>
                    <p><spring:message code="select.circle.operator.plans" text="#select.circle.operator.plans#"/></p>
                    <form:form modelAttribute="homeWebDo" action="javascript:void(0)" method="post" name="popularPlanForm" id="popularPlanForm">
                        <ul class="clearfix">
                            <li>
                                <form:select path="operator" id="s_provider_plans" class='popular-plans-select' >
                                    <form:option value=""><spring:message code="select.operator" text="#select.operator#"/></form:option>
                                    <c:forEach items="${homeWebDo.operatorList}" var="itr">
                                        <c:if test="${itr.productName=='Mobile'}">
                                            <option value="${itr.operatorMasterId}"
                                                    class="${itr.operatorCode}"
                                                    <c:if test="${fn:toUpperCase(itr.operatorName) eq fn:toUpperCase(seoOperatorName)}">
                                                        selected='selected'
                                                    </c:if>>${itr.operatorName}</option>
                                        </c:if>
                                    </c:forEach>
                                </form:select>

                            </li>
                            <li>
                                <form:select name="circle" path="circle" id="mobile_circle_id_plans" class='popular-plans-select'>
                                    <form:option value=""><spring:message code="select.circle" text="#select.circle#"/></form:option>
                                    <c:forEach items="${homeWebDo.circleList}" var="itr">
                                        <c:if test="${itr.name!='ALL'}">
                                            <option value="${itr.circleMasterId}">${itr.name}
                                            </option>
                                        </c:if>
                                    </c:forEach>
                                </form:select>
                            </li>
                            <li>
                                <select name="rechargePlanNameList" path="rechargePlanNameList" id="recharge_plan_names" class='popular-plans-select'>
                                    <option value=""><spring:message code="select.rechargePlanName" text="#select.rechargePlanName#"/></option>
                                    <c:forEach items="${homeWebDo.rechargePlanNameList}" var="itr">
                                        <option value="${itr}"
                                                <c:if test="${fn:toUpperCase(itr) eq 'TOPUP'}">
                                                    selected='selected'
                                                </c:if>
                                                >${itr}</option>
                                    </c:forEach>
                                </select>
                            </li>
                        </ul>
                    </form:form>
                    <div id="rechargePlans">
                        <jsp:include page="rechargeplans.jsp" />
                    </div>
                </div>
            </div>

        </div>
    </div>
    
    <div class="row coupons" id="coupons">
        <div id="coupon-available-message" class="coupon-available-message">
            <div class="applaud"><strong>Awesome!</strong></div>
            <p class="next-message">You've gained access to equal value coupons worth <span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span><span id="coupon-eligible-value" > ${homeWebDo.couponWorthAmt+homeWebDo.remainingBalance}</span></p>
        </div>

        <div class="coupon-header clearfix">
            <h2 class="bg-header">Choose your equal value coupons</h2>
            <a href="javascript:void(0)" id="freeCoupons-tip">Can I select any amount?</a>
            <span class="menu">
                <select id="coupon-category">
                    <option value="ALL"><spring:message code="all.categories" text="#all.categories#"/></option>
                    <c:forEach items="${homeWebDo.categories}" var="itr">
                        <option value="${itr.catName}">${itr.catDetail}</option>
                    </c:forEach>
                </select>
            </span>
        </div>
        <div id="freeCoupons-tooltip" style="display: none">
            <div class="coupon-help">
                <h4 class="unit-title">Recharge Amount Vs Coupon Amount</h4>
                <p class="coupon-help-text">Recharge for Rs.10 - 50 &amp; get coupons worth Rs.50 </p>
                <p class="coupon-help-text">Recharge for Rs.51 - 100 &amp; get coupons worth Rs.100 </p>
                <p class="coupon-help-text">Recharge for Rs.101 - 150 &amp; get coupons worth Rs.150 </p>
                <p class="coupon-help-text">Recharge for Rs.151 - 200 &amp; get coupons worth Rs.200 </p>
            </div>
        </div>
		<div id="cityFilter" class="coupon-filter-city">
		</div>
        <div id="coupondiv" class="clearfix coupons-container"></div>

        <div class="coupon-instructions">
            <ul>
                <li>Click on "<strong>know more</strong>" to see details</li>
                <li><spring:message code="maximum.coupons" text="#maximum.coupons#"/></li>
                <li><spring:message code="handling.charge" text="#handling.charge#"/> of Rs.10 applicable</li>
            </ul>
        </div>

    </div>

    <c:choose>
        <c:when test="${seoAttributes.freeCouponText != null}" >
            <div class="row page-info">
                <p class="text-one">${seoAttributes.freeCouponText}</p>
                <c:if test="${seoAttributes.doubleCouponText != null}" >
                    <p class="text-two">${seoAttributes.doubleCouponText}</p>
                </c:if>
            </div>
        </c:when>
        <c:otherwise>
        <div class="row page-info">
            <h1>Online Recharge</h1>
            <p class="text-one">Recharge your prepaid Mobile, DTH or Data card online and get free coupons worth equal value. <br>With every online recharge, FreeCharge offers wide range of exclusive coupons to choose from. Online recharge facility is available for all prepaid Mobile, DTH and Data card operators. <br>FreeCharge also offers additional information such as popular online recharge plans for all service providers. </p>
            <p class="text-two"><strong>Tip:</strong> Use Maestro or MasterCard to recharge your prepaid Mobile, DTH or Data Card online and get coupons worth double the value of your recharge amount. </p>
        </div>
    </c:otherwise>
    </c:choose>

</div>
<div class="four columns">
    <div id="sidebar" class="sidebar">
        <div id="orderSummary" style="display: none">
                        
        <div class="orderSummary">

            <div class="rechargeDetails row">
                <ul class="clearfix">
                    <li><em id="order_operator">${homeWebDo.displayOperatorName}</em></li>
                    <li><strong><em id="order_serviceNo">${homeWebDo.displayServiceNo}</em></strong></li>
                    <li><span class="webRupee"><spring:message code="rupee" text="#rupee#"/></span>
                        <em id="order_rechargeAmount">${homeWebDo.displayAmount}</em></li>
                    </ul>
                </div>

                <div class="billing row">
                    <h2><spring:message code="order.summary" text="#order.summary#"/></h2>
                    <ul class="clearfix">
                        <li class="recharge">
                            <span class="type"><em id="bill_operator">${homeWebDo.displayOperatorName} </em>&nbsp;<spring:message code="recharge.small" text="#recharge.small#"/> </span>
                            <span class="value"><span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span> <em id="bill_rechargeAmount">${homeWebDo.displayAmount}</em></span>
                        </li>
                        <li id="coupon-eligible-info" style="
                        <c:choose><c:when test='${fn:length(cartAndCartItemsVO) gt 0 }'>display:block</c:when>
                        <c:otherwise>display:none</c:otherwise></c:choose>
                        ">
                            <span class="type">${homeWebDoCouponCounter}<spring:message code="coupons.worth" text="#coupons.worth#"/> <span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span> <em id="bill_couponValue">${homeWebDo.roundOffAmount}</em></span>
                            <span class="value"><spring:message code="free" text="#free#"/></span>
                        </li>
                        <%-- <li id="coupon-charge" style="
                        <c:choose><c:when test='${fn:length(cartAndCartItemsVO) gt 0 }'>display:block</c:when>
                        <c:otherwise>display:none</c:otherwise></c:choose>
                        ">
                        <span class="type"><spring:message code="handling.charge.summary" text="#handling.charge.summary#"/></span>
                        <span class="value"><span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span> ${serviceCharge}</span>
                    	</li> --%>
                    	<c:if test="${productId eq 1}">
                    		<li id="mobile-coupon-charge" style="
	                        <c:choose><c:when test='${fn:length(cartAndCartItemsVO) gt 0 }'>display:block</c:when>
	                        <c:otherwise>display:none</c:otherwise></c:choose>
	                        ">
	                        <span class="type"><spring:message code="handling.charge.summary" text="#handling.charge.summary#"/></span>
	                        <span class="value"><span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span> <span id="mobileServiceCharge">${mobileServiceCharge}</span></span>
	                    	</li>
                    	</c:if>
						<c:if test="${not(productId eq 1)}">
                    		<li id="mobile-coupon-charge" style="
	                        <c:choose><c:when test='${fn:length(cartAndCartItemsVO) gt 0 }'>display:none</c:when>
	                        <c:otherwise>display:none</c:otherwise></c:choose>
	                        ">
	                        <span class="type"><spring:message code="handling.charge.summary" text="#handling.charge.summary#"/></span>
	                        <span class="value"><span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span> <span id="mobileServiceCharge">${mobileServiceCharge}</span></span>
	                    	</li>
                    	</c:if>
                    	<c:if test="${productId eq 2}">
                    		<li id="dth-coupon-charge" style="
	                        <c:choose><c:when test='${fn:length(cartAndCartItemsVO) gt 0 }'>display:block</c:when>
	                        <c:otherwise>display:none</c:otherwise></c:choose>
	                        ">
	                        <span class="type"><spring:message code="handling.charge.summary" text="#handling.charge.summary#"/></span>
	                        <span class="value"><span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span> <span id="dthServiceCharge">${dthServiceCharge}</span></span>
	                    	</li>
                    	</c:if>
                    	<c:if test="${not(productId eq 2)}">
                    		<li id="dth-coupon-charge" style="
	                        <c:choose><c:when test='${fn:length(cartAndCartItemsVO) gt 0 }'>display:none</c:when>
	                        <c:otherwise>display:none</c:otherwise></c:choose>
	                        ">
	                        <span class="type"><spring:message code="handling.charge.summary" text="#handling.charge.summary#"/></span>
	                        <span class="value"><span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span> <span id="dthServiceCharge">${dthServiceCharge}</span></span>
	                    	</li>
                    	</c:if>
                    	<c:if test="${productId eq 3}">
                    		<li id="datacard-coupon-charge" style="
	                        <c:choose><c:when test='${fn:length(cartAndCartItemsVO) gt 0 }'>display:block</c:when>
	                        <c:otherwise>display:none</c:otherwise></c:choose>
	                        ">
	                        <span class="type"><spring:message code="handling.charge.summary" text="#handling.charge.summary#"/></span>
	                        <span class="value"><span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span> <span id="datacardServiceCharge">${datacardServiceCharge}</span></span>
	                    	</li>
                    	</c:if>
                    	<c:if test="${not (productId eq 3)}">
                    		<li id="datacard-coupon-charge" style="
	                        <c:choose><c:when test='${fn:length(cartAndCartItemsVO) gt 0 }'>display:none</c:when>
	                        <c:otherwise>display:none</c:otherwise></c:choose>
	                        ">
	                        <span class="type"><spring:message code="handling.charge.summary" text="#handling.charge.summary#"/></span>
	                        <span class="value"><span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span> <span id="datacardServiceCharge">${datacardServiceCharge}</span></span>
	                    	</li>
                    	</c:if>
                    <li class="total"><strong>
                        <span class="type"><spring:message code="total.payable" text="#total.payable#"/></span>
                        <span class="value"><span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span>&nbsp;<em id="bill_total">${homeWebDo.payableAmount}</em></span>
                    </strong></li>
                </ul>
            </div>

            <div class="couponSelection row">
                <div class="orderSummary-coupons">
                    <div class="coupons-remaining">
                        <div class="couponMetCont">
                            <span class="couponMeter"><em>&nbsp;</em></span>
                        </div>
                        <p><spring:message code="free.coupons.remaining" text="#free.coupons.remaining#"/>: <strong><span class="webRupee"><spring:message code="rupee" text="#rupee#"/>.</span>
                            <em id="couponMeter_balanceAmount">${homeWebDo.remainingBalance}</em></strong></p>
                        </div>

                        <%--<div id="coupon-select-message">
                            <p>You can select coupons worth <span class="webRupee">Rs.</span><em id="couponSelection_value">${homeWebDo.roundOffAmount}</em> for FREE!</p>
                        </div>--%>

                        <div class="coupon-select-list" id="couponSelectionDisplay">
                            <div class="coupon-message" id="coupon-message" <c:if test="${fn:length(cartAndCartItemsVO) gt 0 }">style="display: none" </c:if>>
                                <p><span>Don't forget to make your recharge free!</span><br><em>Collect your free coupons now!</em></p>
                            </div>
                            <ul id="coupon-cart" class="clearfix" <c:if test="${fn:length(cartAndCartItemsVO) gt 0 }">style="display: block" </c:if> >
                                <c:if test="${fn:length(cartAndCartItemsVO) gt 0 }">
                                <c:forEach items="${cartAndCartItemsVO}" var="data" varStatus="cnt">
                                <c:forEach begin="1" end="${data.quantity}" varStatus="loop">
                                <li>
                                    <a href="javascript:void(0)" title="Click to remove" class="icon-close-inverse close" data-coupon-id="${data.itemId}" data-coupon-value="${data.couponValue}"></a>
                                    <img src='${data.couponImagePath}' title="${data.campaignName}"/>
                                    <div><span class='webRupee'><spring:message code="rupee" text="#rupee#"/>.</span><em>${data.couponValue}</em></div>
                                </li>
                            </c:forEach>
                        </c:forEach>
                    </c:if>
                </ul>
            </div>

        </div>
    </div>

    <springF:form action="/app/saveCrossSell.htm" name="couponForm" method="post" id="homeContinue">
        <input type="hidden" id="hidden-inputLookupId" value="${homeWebDo.lookupID}" name="lookupID"/>
        <input type="hidden" id="hidden-txn-homepage-id" name="homePageId" value="${homeWebDo.txnHomePageId}"/>
        <input type="hidden" id="hidden-pro-type" name="proType" value="<%=productType%>"/>
        <input type="hidden" id="hidden-coupon-type" name="type" value="coupon"/>

        <input type="hidden" id="hidden-selectedCoupons" value="" name="itemIdArray"/>
        <input type="hidden" id="hidden-service-no-id" value="${homeWebDo.displayServiceNo}" name="serviceNo"/>
        <input type="hidden" id="hidden-operator-id" value="${homeWebDo.operator}" name="operatorId"/>
        <input type="hidden" id="hidden-operator-name" value="${homeWebDo.displayOperatorName}" name="operatorName"/>
        <input type="hidden" id="hidden-amount-id" value="" name="amount"/>

        <input type="hidden" id="hidden-service-charge" name="hidden-service-charge" value="${serviceCharge}"/>
        <input type="hidden" id="hidden-handling-charges" name="serviceCharge" value="0"/>
        <div class="page-action row">
            <button href="javascript:void(0)" class="button buttonThree"
                type="submit"
               id="home-continue"
               data-coupon-amount="${homeWebDo.roundOffAmount}"
               data-coupon-remaining-amount="${homeWebDo.roundOffAmount - homeWebDo.couponWorthAmt}"
               data-coupon-selected-amount="${homeWebDo.couponWorthAmt}"
               data-coupons="${homeWebDo.itemIdArr}"
               data-login-status="${login}"
               data-operator="${homeWebDo.displayOperatorName}"
               data-operator-id="${homeWebDo.operator}"
               data-product-type="<%=productType%>"
               data-recharge-amount="${homeWebDo.displayAmount}"
               data-service-number="${homeWebDo.displayServiceNo}"
               onclick="_gaq.push(['_trackEvent', 'Button', 'Confirm']);">
                <spring:message code="continue" text="#continue#"/> &rarr;</button>

            <!-- <a href="#loginPage" id="order_confirm_popup" style="display: none;"  ><%--  --%></a> -->
            <!-- <a href="#Nocoupons-message" id="nocoupons_confirm_popup" style="display: none;"  ><%--  --%></a> -->
        </div>
    </springF:form>
</div>
</div>

<div id="sidebar-display" <c:if test='${isEdit eq 1}'>style="display: none" </c:if>>
  <c:choose>
  	<c:when test="${isPepsiCampaignEnabled}">
  		<a href="#pepsi-promotion" id="pepsi-promotion-banner" onclick="_gaq.push(['_trackEvent', 'Home-Banner', 'Click', 'PEPSI']);"  >
			<img class="fluid" src='${mt:keyValue("imgprefix1")}/images/banner/Pepsi_HPbanner2.jpg' width="220" height="280" alt="Participate 60 Crore Tak Ka Recharge" title="Participate in 60 Crore Tak Ka Recharge" /></a>
	   <div id="pepsi-promotion" style="display: none;">
			<div class="lightbox">
				<img src="${mt:keyValue("imgprefix1")}/images/pepsi/pepsi-banner-small.png" width="162" height="103" >			
				<div class="brands">
					<img src="${mt:keyValue("imgprefix1")}/images/pepsi/pepsi-small.png" width="40" height="41" />
					<img src="${mt:keyValue("imgprefix1")}/images/pepsi/mountain-small.png" width="40" height="41" />
					<img src="${mt:keyValue("imgprefix1")}/images/pepsi/mirinda-small.png" width="40" height="41" />
					<img src="${mt:keyValue("imgprefix1")}/images/pepsi/sevenup-small.png" width="40" height="41" />
					<img src="${mt:keyValue("imgprefix1")}/images/pepsi/slice-small.png" width="40" height="41" />
				</div>
				<h4>Get Free Rs.20 recharge at FreeCharge.com</h4>
				<p>Pick up a bottle of Pepsi, Mountain Dew, Mirinda, 7UP <br> (1 L, 1.25 L, 2 L, 2.25 L) or Slice (1.2 L). 
				Look for a unique 11-digit code under the label of your bottle and redeem it.
				</p>			    
			    <a class="button buttonFour" href="/pepsipromotion#redeem" onclick="_gaq.push(['_trackEvent', 'Home-Popup', 'Click', 'Pepsi-Details']);" >DETAILS</a>
			    <a onclick="$.fancybox.close();_gaq.push(['_trackEvent', 'Home-Popup', 'Click', 'Pepsi-Understand']);" href="javascript:void(0)" class="button buttonFive" >OKAY, I UNDERSTAND</a>				
			</div>
		</div>
  	</c:when>
  </c:choose> 
  
   <c:if test="${isFestiveGiftEnabled}"> <%-- Visible if Festive Gift on --%>
	   <a href="javascript:void(0)" onclick="_gaq.push(['_trackEvent', 'Home-Banner', 'Click', 'diwaliFestiveGift']);" data-theme-title="diwali-theme" class="themeActivationTrigger" ><img class="fluid themeActivationTrigger" src="${mt:keyValue("imgprefix1")}/images/diwali/diwali_HPbanner.png" width="220" height="76" alt="GIft a recharge" title="GIft a recharge" /></a>
	   <img class="diwali-lantern" src="${mt:keyValue("imgprefix1")}/images/diwali/diwali-lantern.png" alt="Diwali Special" style="opacity:0;  filter:Alpha(opacity=0);" >
	   <img class="diwali-lamp" src="${mt:keyValue("imgprefix1")}/images/diwali/diwali-lamp.png" alt="Diwali Special" style="opacity:0;  filter:Alpha(opacity=0);" >    
   </c:if>	

    <div class="fbWidget row">
        <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Ffreecharge&amp;send=false&amp;layout=standard&amp;width=198&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=80&amp;appId=121182438053697" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:198px; height:80px;" allowTransparency="true"></iframe>
    </div>   
    
     <c:choose>
  	<c:when test="${isZedoAdEnabled}">
	  <!-- Nothing here -->
	</c:when>
    <c:otherwise>
      <img class="fluid" src='${mt:keyValue("imgprefix1")}/images/banner/testimonial_banner2.jpg' alt="I save money by using coupons. I FreeCharge" title="I save money by using coupons. I FreeCharge" />
    </c:otherwise>
  </c:choose>          

    <a href="/mobile-app-download" onclick="_gaq.push(['_trackEvent', 'Home-Banner', 'Click', 'mobileAppDownload']);"
    target="_blank" ><img class="fluid" src='${mt:keyValue("imgprefix1")}/images/banner/mobile-app-banner2.jpg' 
    width="220" height="210" alt="FreeCharge on Google play" title="FreeCharge on the go - Get it now on Google play" /></a>
    </div>
</div>

</div>
<div id="Nocoupons-message" style="display:none">
    <div class="nocoupons-message-alert clearfix">
        <h3>Continue without coupons?</h3>
        <div class="clearfix" >
            <img src="${mt:keyValue("imgprefix1")}/images/icons/icon_alert.png" width="99" height="88" class="fluid lf" />
            <div class="nocoupons-message-alert-instruction rf" >
              <p>Please confirm if you want to proceed without equal value coupons.</p>
              <a href="javascript:void(0)" id="skip-coupons" class="textlink lf" >Yes, Skip coupons</a>
              <a id="want-coupons" class="button buttonThree lf" href="javascript:void(0)" >No, I want my coupons</a>
            </div>
      </div>
       <%-- <input id="dont_show_alert" type="checkbox" checked="checked" class="lf" >
       <label for="dont_show_alert" class="lf" >Do not show me this again</label> --%>
   </div>
</div>

<div id="recharge-retry" style="display: none">
    <div class="lightbox">
        <p class='recharge-retry-message'>
            <span class='recharge-retry-message1'>On a recharging spree? Try after a 30 minute break.</span> 
            <span class='recharge-retry-message2'>In a hurry to get recharged? Check our popular plans and try a different denomination right now.</span> 
        </p>
        <a onclick="$.fancybox.close();" class="button buttonThree rf" href="javascript:void(0)" >Close</a>
    </div>
</div>
