<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GOSF 2014 - FreeCharge Offer:Mobile,DTH Recharge and Coupons Offer.</title>
    <meta name="description" content="GOSF-2014.BIG FreeCharge offers during the Great Online Shopping Festival 2014.299 offer &amp; cash back offer on prepaid,postpaid,DTH,datacard mobile online recharge .">
    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no">
    <link rel="icon" href="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/favicon-new.ico?v=${mt:keyValue("version.no")}" type="image/x-icon">
    <link rel="shortcut icon" href="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/favicon-new.ico?v=${mt:keyValue("version.no")}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/apple-touch-icon-new.png?v=${mt:keyValue("version.no")}">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'  type='text/css'>
    <link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/campaigns/gosf/style.min.css?v=${mt:keyValue("version.no")}" media="screen">
    <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/campaigns/gosf/jquery-1.11.1.min.js?v=${mt:keyValue("version.no")}"></script>
</head>

<body>
   <div class="gosf-container">
        <div class="gosf-header">
            <div class="gosf-fc-logo"><a href="http://www.freecharge.in"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/freecharge-logo-black.png" alt="Recharge nahin Freecharge" title="Recharge nahin Freecharge"></a></div>
            <div class="gosf-app-wrapper">
                <p>Download the app</p>
                <div class="appicon-wrapper"><a href="http://www.freecharge.in/fc/gosf" target="_blank"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/android.png" alt="Download Android App" title="Android"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/windows.png" alt="Download Windows App" title="Windows"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/apple.png" alt="Download iOS App" title="iOS"></a></div>
            </div>
        </div>
        <div class="gosf-banner">
            <div class="text-wrapper">
                <div class="text-part1"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/offer-text-1.png"></div>
                <div class="text-part2"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/gosf-img-1.png"></div>
                
            </div>
            <div class="text-part3"><b>Extra 5% cashback on Axis &amp; Kotak bank Debit/Credit cards</b></div>
        </div>
        <div class="gosf-offers-wrapper">
            <div class="offer-1 offer-details"><a href="http://www.freecharge.in/fc/gosf" target="_blank"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/offer-1.png"></a></div>
            <div class="offer-2 hide offer-details"><a href="http://www.freecharge.in/fc/gosf" target="_blank"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/offer-2.png"></a></div>
            <div class="offer-3 hide offer-details"><a href="http://www.freecharge.in/fc/gosf" target="_blank"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/offer-3.png"></a></div>
        </div>
        <div class="offer-details-wrapper">
            <div class="gosf-offers-nav-wrapper"><ul class="gosf-offers-nav" id="nav-wrapper"><li id="offer-1" class="active">10% Cash back on mobile &amp; data<span class="teeth"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/teeth.png" height="10"></span></li><li id="offer-2">₹ 50 cashback on dth<span class="teeth hide"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/teeth.png" height="10"></span></li><li id="offer-3">₹ 299 corner<span class="teeth hide"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/teeth-blue.png" height="10"></span></li></ul></div>
            <div class="offer-details offer-1">
                <div class="tnc-wrapper">
                    <h3>Terms &amp; Conditions</h3>
                    <ol class="offer1-tnc">
                        <li>Get 10% cash back only on prepaid, postpaid &amp; data card recharges on FreeCharge Mobile App (Android/iOS/Windows). Not valid on DTH recharge.</li>
                        <li>Maximum cashback of Rs.100 would be given in form of FreeCharge Credits post successful transaction. Offer valid at no minimum recharge value.</li> 
                        <li>Additional 5% cashback in form of FreeCharge Credits will be processed only on successful recharges for customers using Axis/Kotak bank Debit or Credit card.</li>
                        <li>Valid only once per user /mobile number / credit card / debit card.</li>
                        <li>Applicable only on recharges using Credit &amp; Debit card. Not valid for net-banking transactions, FreeCharge Credits as payment option and on ‘Add Cash’ transactions.</li>
                        <li>Valid from 00:00:01 AM of Dec 10 2014 to 11:59:59 PM of Dec 12 2014.</li>
                        <li>FreeCharge reserves the right to amend, modify, change, add or terminate (collectively “Changes”) the terms, contained herein at our sole discretion or discontinue this Offer without any further notice.</li>
                    </ol>
                </div>
                <div class="redeem-steps">
                    <h3>How to Redeem</h3>
                    <div class="steps-wrapper">
                        <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/step-1.png"><p>Download<br>the app</p></div>
                        <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/step-2.png"><p>Recharge<br>Mobile/Data</p></div>
                        <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/offer1-step-3.png"><p>Apply<br>promocode</p></div>
                        <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/step-4.png"><p>Pay through<br>Credit/Debit Card</p></div>
                    </div>
                    <p>On successful recharge, your cashback will be processed within 24 hours</p>
                </div>
            </div>

            <div class="offer-details offer-2 hide">
                <div class="tnc-wrapper">
                    <h3>Terms &amp; Conditions</h3>
                    <ol class="offer2-tnc">
                        <li>Get Rs.50 cash back on DTH Recharges of Rs.100 and above only on FreeCharge Mobile App (Android/iOS/Windows)</li>
                        <li>Cashback would be given in form of FreeCharge Credits post successful transaction.</li> 
                        <li>Additional 5% cashback in form of FreeCharge Credits will be processed only on successful recharges for customers using Axis/Kotak bank Debit or Credit card.</li>
                        <li>Valid only once per user /mobile number / credit card / debit card.</li>
                        <li>Applicable only on recharges using Credit &amp; Debit card. Not valid for net-banking transactions, FreeCharge Credits as payment option and on ‘Add Cash’ transactions.</li>
                        <li>Valid from 00:00:01 AM of Dec 10 2014 to 11:59:59 PM of Dec 12 2014.</li>
                        <li>FreeCharge reserves the right to amend, modify, change, add or terminate (collectively “Changes”) the terms, contained herein at our sole discretion or discontinue this Offer without any further notice.</li>
                    </ol>
                </div>
                <div class="redeem-steps">
                    <h3>How to Redeem</h3>
                    <div class="steps-wrapper">
                        <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/step-1.png"><p>Download<br>the app</p></div>
                        <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/step-2.png"><p>Recharge<br>your DTH</p></div>
                        <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/offer2-step-3.png"><p>Apply<br>promocode</p></div>
                        <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/step-4.png"><p>Pay through<br>Credit/Debit Card</p></div>
                    </div>
                    <p>On successful recharge, your cashback will be processed within 24 hours</p>
                </div>
            </div>

            <div class="offer-details offer-3 hide">
                <div>
                    <div class="gosf-offers-table">
                        <table>
                            <thead>
                                <tr>
                                    <th class="partner">Partner</th>
                                    <th class="offer-desc">Offer</th>
                                    <th class="offer-value">Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/costacoffee.png"></td>
                                    <td class="offer-desc">Buy 1 Beverage, Get 1 Free</td>
                                    <td class="offer-value">Rs.50</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/uspizza.png"></td>
                                    <td class="offer-desc">Buy 1 Medium Pizza, Get 1 Free</td>
                                    <td class="offer-value">Rs.150</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/foodpanda.png"></td>
                                    <td class="offer-desc">Rs.125 discount on min. billing of Rs.250</td>
                                    <td class="offer-value">Rs.125</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/gelato.png"></td>
                                    <td class="offer-desc">Buy 2 Regular Scoops, Get 1 Scoop Free</td>
                                    <td class="offer-value">Rs.50</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/myntra.png"></td>
                                    <td class="offer-desc">Rs.500 OFF on min. purchase of Rs.1650</td>
                                    <td class="offer-value">Rs.500</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/metro.png"></td>
                                    <td class="offer-desc">Rs.300 OFF on min. purchase of Rs.1250</td>
                                    <td class="offer-value">Rs.300</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/peterengland.png"></td>
                                    <td class="offer-desc">Flat Rs.200 discount at Peter England Stores</td>
                                    <td class="offer-value">Rs.200</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/snapdeal.png"></td>
                                    <td class="offer-desc">Rs.100 OFF on Mobile Cases &amp; Covers</td>
                                    <td class="offer-value">Rs.100</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/timezone.png"></td>
                                    <td class="offer-desc">Get Rs.300 &#43; 5 video attraction games on every loading of Rs.300</td>
                                    <td class="offer-value">Rs.300</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/enrich.png"></td>
                                    <td class="offer-desc">Rs.150 OFF on all services above Rs.500</td>
                                    <td class="offer-value">Rs.150</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/fourfountains.png"></td>
                                    <td class="offer-desc">Flat Rs.400 discount on any 60 minute &amp; above therapy</td>
                                    <td class="offer-value">Rs.400</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/yatra.png"></td>
                                    <td class="offer-desc">Rs.200 OFF on Bus ticket bookings more than Rs.1200</td>
                                    <td class="offer-value">Rs.200</td>
                                </tr>
                                <tr>
                                    <td class="partner"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/yatra.png"></td>
                                    <td class="offer-desc">Rs.500 OFF on Domestic Flight Bookings more than Rs.5000</td>
                                    <td class="offer-value">Rs.500</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="tnc-wrapper">
                        <h3>Terms &amp; Conditions</h3>
                        <ol class="offer3-tnc">
                            <li>Valid only on FreeCharge Mobile App (Android/iOS/Windows)</li>
                            <li>Valid only on minimum recharge of Rs.299 and above</li> 
                            <li>Use promocode <b>GOSF299</b> to receive <b>Rs.2990</b> worth coupons in your registered email address</li>
                            <li>Valid only once per user /mobile number / credit card / debit card.</li>
                            <li>Applicable only on recharges using Credit &amp; Debit card. Not valid for net-banking transactions, FreeCharge Credits as payment option and on ‘Add Cash’ transactions.</li>
                            <li>Valid from 00:00:01 AM of Dec 10 2014 to 11:59:59 PM of Dec 12 2014.</li>
                            <li>Additional 5% cashback in form of FreeCharge Credits will be processed only on successful recharges for customers using Axis/Kotak bank Debit or Credit card.</li>
                            <li>FreeCharge reserves the right to amend, modify, change, add or terminate (collectively “Changes”) the terms, contained herein at our sole discretion or discontinue this Offer without any further notice.</li>
                        </ol>
                    </div>
                    <div class="redeem-steps">
                        <h3>How to Redeem</h3>
                        <div class="steps-wrapper">
                            <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/step-1.png"><p>Download<br>the app</p></div>
                            <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/step-2.png"><p>Min. recharge<br>of Rs.299</p></div>
                            <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/offer3-step-3.png"><p>Apply<br>promocode</p></div>
                            <div class="steps"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/step-4.png"><p>Pay through<br>Credit/Debit Card</p></div>
                        </div>
                        <p>On successful recharge, your cashback will be processed within 24 hours</p>
                    </div>
            </div>
            <div class="partners"><b>Partners:</b>
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/costacoffee.png">
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/enrich.png">
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/foodpanda.png">
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/fourfountains.png">
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/gelato.png">
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/metro.png">
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/myntra.png"><br>
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/peterengland.png">
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/snapdeal.png">
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/timezone.png">
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/uspizza.png">
                <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/landing/partners/yatra.png">
            </div>
        </div>
            
        </div>
    </div>
    

    

    <script type="text/javascript">
            $('#nav-wrapper li').off('click');
            $('#nav-wrapper li').on('click', function(e){
                $(this).addClass('active').siblings().removeClass('active');
                $('.offer-details').addClass('hide');
                $(this).siblings().find('.teeth').addClass('hide');
                $(this).find('.teeth').removeClass('hide');
                $('.'+$(this).attr('id')).removeClass('hide')
            });
    </script>
</body>
</html>
