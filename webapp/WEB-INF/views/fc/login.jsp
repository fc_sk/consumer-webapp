<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<div id="loginPage" style="display: none;" >
    <div class="lightbox">
         <div class="revised-form clearfix" id="login-box">        
    		<ul class="revised-form-navigation clearfix" >
    			<li rel="0" >Old user? Login here</li>
    			<li rel="1" class="active" id="revised_sign_up" >New user? Register here</li>
    		</ul>
    		<div class="revised-form-content" >
    			<div class="login-box" >
	                <div class="sub-section-info" >
    					<h5>Already have a FreeCharge account?</h5>
    					<p>Login here</p>
    			    </div>
	                <form:form name="frmLogin" id="loginheader" action="javascript:void(0)">
	                    <div id="loginError" class="formErrorContent " style="display:none;"></div>
	                    <div class="field email">
	                        <input type="text" autofocus  name="email" placeholder="E-Mail Address" class="validate[required,custom[email]]" id="emailSingIn"  />
	                    </div>
	                    <div class="field password">
	                        <input type="password" name="password" placeholder="Password" class="validate[required]" id="passwordSignIn"  />
	                    </div>
	                    <div class="field action clearfix">
	                        <button id="login-submit" onclick="loginHeader();" class="buttonTwo">Login</button>
	                        <a href="javascript:void(0)" id="forgotPassword-link">Forgot password?</a>
	                    </div>
	                </form:form>         			
    			</div>
    			
    			
    			<div class="revised-signup-content" >   				
    				<div class="sub-section-info" >
    					<h5>Register with FreeCharge</h5>
    					<p>And get access to the best coupons </p>
    			   </div>
    				
	                <form:form  name="SignUpRevisedForm" action="javascript:void(0)" method="post" modelAttribute="userDO" id="SignUpForm">             		
                    		<div class="field name">
	                    		<spring:bind path="userDO.title">
	                                 <form:select path="title" id="title" name="title" cssClass="validate[required]" >
	                                     <option value=""><spring:message code="registration.title" text="#registration.title#"/></option>
	                                     <option value="Mr"><spring:message code="mr" text="#mr#"/>.</option>
	                                     <option value="Ms"><spring:message code="ms" text="#ms#"/>.</option>
	                                     <option value="Mrs"><spring:message code="mrs" text="#mrs#"/>.</option>
	                                 </form:select>
	                             </spring:bind>
	                             <spring:bind path="userDO.firstName">
	                                 <form:input id="firstName" path="firstName" name="firstName" placeholder="Full Name" cssClass="validate[required,funcCall[fullName],custom[onlyLetterSp]] text-input,minSize[4],maxSize[25]" />
	                             </spring:bind>
                             </div>
                             <div class="field mobile">
                                <spring:bind path="userDO.mobileNo">
                                    <form:input id="mobileNo"  path="mobileNo" placeholder="Mobile Number" maxlength="10" cssClass="validate[required,funcCall[yourMobileNoRequired] ,custom[phone]] text-input,minSize[10],maxSize[10] text-input" onkeypress="return isNumberKey(event,10,id)" />
                                    <form:errors path="mobileNo"></form:errors>
                                </spring:bind>
                            </div>                    	
                    		<div class="field email">
                                <p style="display: none;"  id="emailvalidationmessage" class="failureMsgSmall" ></p>
                                <spring:bind path="userDO.email">
                                    <form:input id="userEmailID" path="email" name="email" placeholder="E-Mail Address" cssClass="validate[required,funcCall[checkEmail],custom[email]] text-input,maxSize[90]" onblur="return checkUserAvail(id,'emailvalidationmessage');" />
                                    <form:errors path="email"></form:errors>
                                </spring:bind>
                            </div>
                            <div class="field password">
                                <input type="password" name="password" id="passwordSignUp" placeholder="Password" class="validate[required] text-input,minSize[6],maxSize[20]"  size="20" />
                            </div> 
                            <div class="field state">
                               <select id="state" name="stateId"  class="validate[required]" onchange="getCityListfromHeader()" >
                                   <option value=""><spring:message code="select.state" text="#select.state#"/></option>
                                   <c:forEach var="stateMaster" items="${stateMaster}">
                                       <option value='${stateMaster.stateMasterId}'>${stateMaster.stateName}</option>
                                   </c:forEach>
                               </select>
                            </div>
                            <div class="field city">   	                           
                               <spring:bind path="userDO.cityId">
                                   <form:select path="cityId" id="city" name="city" cssClass="validate[required]" >
                                       <option value=""><spring:message code="select.city" text="#select.city#"/></option>
                                   </form:select>
                               </spring:bind>
                           </div>              
	                </form:form>
	                <div class="button-wrap" >
	                	<button id="register-button" onclick="return validateSignUpForm();" class="buttonTwo">Register</button>
	                    <p>By signing-up you agree to our <br><a target="_blank" href="/app/termsandconditions.htm"><spring:message code="terms.conditions" text="#terms.conditions#"/></a></p>
	                </div>
    				
    			</div>
    		</div>                       
        </div>

        <div class="password-recovery-box clearfix" id="passRecovery-box" style="display: none;">
            <h3>Create a new password </h3>

            <div class="field" id="forgotpasswordmaindiv">
                <p class="instruction">Enter your email address below and we will send you a link to reset the password and create a new one.</p>
                <form method="post" name="frmForgetPassword" id="frmForgetPassword" action="javascript:getPassword()">
                    <p class="failureMsgSmall" style="display: none;" id="forgotfailureMsgSmall"><c:out value="${forgotPasswordResult}"/> </p>
                    <input placeholder="Email address" type="text" maxlength="40" value="" class="validate[required,funcCall[checkEmail],custom[email]] text-input,maxSize[90] email" id="txtforgetemail" name="txtforgetemail">
                    <input type="hidden" id="status" value="${status}"/>
                    <button class="buttonTwo">Send link</button>
                </form>
            </div>

            <div class="forgotPassSuccess" style="display:none;" id="forgotPassSuccess" > <%-- The block after success --%>
                <p class="successMsg"><b>We have sent an email to <span id="successEmailId" ><c:out value="${emailId}"/></span></b></p>
                <p class="txt" ><br>The email contains the instructions for resetting your password. If you don't receive this email, please check your junk mail folder or contact us at <a href="mailto:care@freecharge.com">care@freecharge.com</a> for further assistance. <br><br>If you do not have access to the above email address then you may <a href="javascript:void(0);" onclick="$('#forgotpasswordmaindiv').show();$('#forgotPassSuccess').hide();">click here</a> to retrieve your password.</p>
            </div>
        </div>
    </div>
</div>
