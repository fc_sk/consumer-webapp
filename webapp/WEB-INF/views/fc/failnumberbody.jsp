<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>

<!-- .mainContainer Starts -->
<div class="mainContainer">
    <div class="pageWidth clearfix">
        <h1 class="pageHeader clearfix " ><img src="${mt:keyValue("imgprefix1")}/images/cross.png" alt="" width="30" height="30" class="ico fLeft" > <span class="mainTxt fLeft" >Sorry! <span class="softTxt" >Recharge unsuccessful due to invalid number or  operator. </span></span></h1>
        <form:form name="recharge" method="post" modelAttribute="rechargeInitiateWebDo" action="javascript:void(0);">
            <div class="successMsgWrapp curveBlockWhite clearfix">                
                  <div class="rechargeSummary form" >                               	
	                   	<p class="txt" ><strong>Transaction Summary</strong></p>
	                     <div class="formFieldAdd clearfix">
	                         <label class="fLeft">Order ID  &nbsp; :   </label>
	                         <span class="inputOutput fLeft ">${rechargeInitiateWebDo.orderId}</span>
	                     </div>
						 <div class="formFieldAdd clearfix">
	                         <label class="fLeft">Recharge Amount &nbsp; :   </label>
	                         <span class="inputOutput fLeft "> <span class="webRupee">Rs</span><c:out value="${rechargeInitiateWebDo.amount}"/></span>
	                     </div>
	                     <div class="formFieldAdd clearfix">
	                         <label class="fLeft">Recharge Number  &nbsp; :   </label>                 
			                 <ul class="formRow" >
			                     <li class="clearfix" >
			                         <span class="mobCD"><span>&nbsp;</span> <b class="ext"> +91</b><input   maxlength="10"   class="input2 mobText" id="rcmobileNo" name="rcmobileNo"  value="${fn:escapeXml(rechargeInitiateWebDo.mobileNo)}" type="text" ></span> 
			                     </li>
			                     <li>
			                     	<form:select  path="operator" id="rcoperator" tabindex="3">
			                            <c:forEach items="${rechargeInitiateWebDo.operatorList}" var="itr">
			                                <c:if test="${itr.productName=='Mobile'}">
			                                    <c:choose>
				                                     <c:when test=" ${itr.operatorCode eq rechargeInitiateWebDo.operator}">
				                                         <form:option value="${itr.operatorCode}" cssClass="${itr.operatorCode}" selected="selected">${itr.operatorCode}</form:option>
				                                     </c:when>
			                                     <c:otherwise>
													 <form:option value="${itr.operatorCode}" cssClass="${itr.operatorCode}">${itr.operatorCode}</form:option>	                                     
			                                     </c:otherwise>
	  		                                     </c:choose>
			                                </c:if>
			                            </c:forEach>
			                        </form:select>
			                    </li>
								<li><button type="submit" name="submit"  class="tryAgainBtn" onclick="retryforrecharge();" > Try again </button> </li>
			                 </ul>
	                     <c:if test="${!empty msg}">
			                    	<div style="color: red;">${msg}</div>
			            </c:if>
	             		</div> 
	            </div> <!-- /.rechargeSummary -->
	             
          	</div>
       		 <!-- /.successMsgWrapper -->
        </form:form>
    </div>
</div>
<script>

var circleCode = "${rechargeInitiateWebDo.circleCode}";
var affiliateid = "${rechargeInitiateWebDo.affiliateid}";
var orderId = "${rechargeInitiateWebDo.orderId}";
var productType = "${rechargeInitiateWebDo.productType}";
var originaloperator = "${rechargeInitiateWebDo.operator}";
var originalMobileNumber = "${rechargeInitiateWebDo.mobileNo}";
var amount = "${rechargeInitiateWebDo.amount}";
var rechargeResponse = "${rechargeInitiateWebDo.rechargeResponse}";
var paymentResponse = "${rechargeInitiateWebDo.paymentResponse}";
var lookupID = "${rechargeInitiateWebDo.lookupID}";
var email = "${rechargeInitiateWebDo.email}";
var deliveryName = "${rechargeInitiateWebDo.deliveryName}";
var deliveryAdd = "${rechargeInitiateWebDo.deliveryAdd}";
var deliveryCity = "${rechargeInitiateWebDo.deliveryCity}";
var deliveryState = "${rechargeInitiateWebDo.deliveryState}";
var deliveryCountry = "${rechargeInitiateWebDo.deliveryCountry}";
var deliveryPincode = "${rechargeInitiateWebDo.deliveryPincode}";
var operatorList = "${rechargeInitiateWebDo.operatorList}";
var paymentPortalResultDesc = "${rechargeInitiateWebDo.paymentPortalResultDesc}";
var paymentPortalResultResponseCode = "${rechargeInitiateWebDo.paymentPortalResultResponseCode}";
var paymentPortalAmount = "${rechargeInitiateWebDo.paymentPortalAmount}";
var paymentPortalTransId = "${rechargeInitiateWebDo.paymentPortalTransId}";
var paymentPortalPaymentGateway = "${rechargeInitiateWebDo.paymentPortalPaymentGateway}";
var paymentPortalMode = "${rechargeInitiateWebDo.paymentPortalMode}";
var paymentPortalMerchantId = "${rechargeInitiateWebDo.paymentPortalMerchantId}";

function retryforrecharge(){
	
	var rcoperator = $("#rcoperator").val();
    var rcmobileNo = $("#rcmobileNo").val();
    if((rcoperator == originaloperator) && (rcmobileNo == originalMobileNumber)){
		alert("Please change Mobile number or Operator for Try again");	
    	return false;
    }
	var url = "/app/rechargeforretry.htm";
	var formData = "circleCode="+circleCode+"&affiliateid="+affiliateid+"&orderId="+orderId+"&productType="+productType+"&productType="+productType+"&originaloperator="+originaloperator+
				   "&originalMobileNumber="+originalMobileNumber+"&amount="+amount+"&rechargeResponse="+rechargeResponse+"&paymentResponse="+paymentResponse+
				   "&lookupID="+lookupID+"&email="+email+"&deliveryName="+deliveryName+"&deliveryAdd="+deliveryCity+"&deliveryCity="+deliveryCity+"&deliveryState="+deliveryState+
				   "&deliveryCountry="+deliveryCountry+"&deliveryPincode="+deliveryPincode+"&paymentPortalResultDesc="+paymentPortalResultDesc+
				   "&paymentPortalResultResponseCode="+paymentPortalResultResponseCode+"&paymentPortalAmount="+paymentPortalAmount+"&paymentPortalTransId="+paymentPortalTransId+
				   "&paymentPortalPaymentGateway="+paymentPortalPaymentGateway+"&paymentPortalMode="+paymentPortalMode+"&paymentPortalMerchantId="+paymentPortalMerchantId+
				   "&mobileNo="+rcmobileNo+"&operator="+rcoperator;
	$.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    } });
	$.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: "html",
        cache: false,
        timeout:1800000,
        success: function (responseText) {
        	$("#wrapper").html(responseText);
        	$.unblockUI();
        },
        error:function (e){
        		$.unblockUI();
                alert("Technical error occured. Please try after some time or contact our customer care."+e);
               }
      });
}

</script>

<!-- .mainContainer Ends -->
