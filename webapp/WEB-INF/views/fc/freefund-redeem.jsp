<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<c:choose>
    <c:when test="${not empty param.lid}">
        <div class="freefund-Holder"  >
            <div id="freefundApplyHolder">
                <p class="title clearfix">
                    <em>Do you have a FreeFund code</em>
                    <span class="icon-info" id="freefund-tip"></span>
                </p>
                <div id="freefund-tooltip" style="display: none">
                    A FreeFund code is like a gift voucher code. The amount is fixed and it can be deposited instantly in your FreeCharge Credits.
                </div>
                <div class="layer">
                    <input class="freeFundCode-input" type="text" placeholder="Enter your FreeFund code" value="" name="freefundCode"   id="freefundCodeId" />
                    <c:if test="${recaptcha_enabled eq 'true'}">
	           	     <table>
		           	     <tr>
		           	     	<td>
		           	     		<input name="captcha1" type="text" autocomplete="off" placeholder="Please enter the words shon in image below"  class="englishOnlyField required" />
		           	     		<div id="captcha-draw-id-freefund" style="height: 50px;">
		           	     			<jsp:include page="../captcha-draw.jsp">
		           	     			<jsp:param value="freefund" name="product"/>
		           	     			</jsp:include>
		           	     		</div> 
		           	     		<a href="javascript:void(0)" onclick="redrawCaptcha();">Refresh</a>
		           	     	</td>
		           		</tr>
	           	     </table>
	           	    </c:if>
                    <button class="buttonOne small-button" id="addFreefund" onClick="redeemFreefund('${param.lid}');">
                        <spring:message code="button.redeem"/>
                    </button>
                </div>
            </div>
            <div id="freefundApplyMsgHolder" class="fc-message promoMsgHolder"  style="display:none;"  ></div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="layer">
            <p class="title">
                Enter the code and click Redeem :
            </p>
                <input class="input-text" type="text" placeholder="FreeFund code" value="" name="freefundCode"   id="freefundCodeId" />
                <c:if test="${recaptcha_enabled eq 'true'}">
           	     <table>
	           	     <tr>
	           	     	<td>
	           	     		<input name="captcha1" type="text" autocomplete="off" placeholder="Please enter the words shon in image below"  class="englishOnlyField required" />
	           	     		<div id="captcha-draw-id-freefund" style="height: 50px;">
	           	     			<jsp:include page="../captcha-draw.jsp">
	           	     			<jsp:param value="freefund" name="product"/>
	           	     			</jsp:include>
	           	     		</div> 
	           	     		<a href="javascript:void(0)" onclick="redrawCaptcha();">Refresh</a>
	           	     	</td>
	           		</tr>
           	     </table>
           	    </c:if>
                <button class="buttonOne small-button" id="addFreefund" onClick="redeemFreefund('${param.lid}');">
                    <spring:message code="button.redeem" text="#button.redeem#"/>
                </button>
        </div>
        <div id="freefundApplyMsgHolder" class="fc-message promoMsgHolder"  style="display:none;"  ></div>
    </c:otherwise>
</c:choose>
<script type="text/javascript">
var isReCaptchaEnbled = false;
</script>
<c:if test="${recaptcha_enabled eq 'true'}">
<script type="text/javascript">
isReCaptchaEnbled = true;
</script>
</c:if>
<script type="text/javascript" >

    $(document).ready(function(){
        //for freefund-code
        Tipped.create('#freefund-tip', 'freefund-tooltip', {
            inline: true,
            skin: 'white',
            maxWidth: 300
        });
    });

    function redeemFreefund(lid) {
        var couponValue = $('#freefundCodeId').val();
        var formData = 'couponCode='+couponValue ;
        if(lid != '') {
        	formData += '&lid='+lid ;
        }
        if(isReCaptchaEnbled) {
        	formData += '&captcha='+$("input[name=captcha1]").val() ;
        }
    	
        var url='/freefund/redeem.htm';
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            dataType: "json",
            cache: false,
            timeout:1800000,
            success: function (responseText) {
                if (responseText.status == 'success') {
                	$("#freefundApplyMsgHolder").show().html('<p class="successMsgSmall " >Yay! Your FreeFund amount has been deposited in your Credits successfully. Go on and <a href="/app/mycredits.htm"> refresh </a> your Credits to see the new amount.</p>');
                } else if(responseText.status == 'failure') {
                	if(isReCaptchaEnbled) {
                		redrawCaptcha() ;
                	}
                    if(responseText.errorCode == 'couponLimboFailure') {
                        $("#freefundApplyMsgHolder").show().html('<p class="failureMsgSmall " >Ouch. Something is broken, can you please try again in a little while?</p>');
                    } else if(responseText.errorCode == 'couponMismatchFailure') {
                        $("#freefundApplyMsgHolder").show().html('<p class="failureMsgSmall " >Uh-oh, incorrect FreeFund code!</p>');
                    } else if(responseText.errorCode == 'couponExpiredFailure') {
                        $("#freefundApplyMsgHolder").show().html('<p class="failureMsgSmall " >Err, looks like that FreeFund code has already been used/expired!</p>');
                    } else if(responseText.errorCode == 'captchaServiceFail') {
                    	$("#freefundApplyMsgHolder").show().html('<p class="failureMsgSmall " >Please enter the words shown on the image correctly.</p>');
                    } else {
                        $("#freefundApplyMsgHolder").show().html('<p class="failureMsgSmall " >Something bad happened try again later.</p>');
                    }
                }
            },
            error:function (xhr,status,error){
                alert("Error occurred " + error);
            }
        });
    }
</script>
