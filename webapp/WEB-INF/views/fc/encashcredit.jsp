
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>  
<html>
<head>
<title>Encash Credits</title>
<style>
	body{
		background: #ededed;
	}
	.clearfix:after{
		content: "";
  		display: table;
  		clear: both;
	}
	.navbar{
		display: none; 
	}
	.container{
		background: #fff;
		margin: 28px auto;
		padding: 32px;
	}
	.wrapper{
		padding: 32px;
	}
	.header{
		position: relative;
		padding: 28px 0;
		margin-bottom: 24px;
		border-bottom: 1px solid #ededed;
	}
	.header h1{
		font-size: 24px;
		position: absolute;
		left: 12px;
		bottom: 12px;
	}
	.header img{
		float: right
	}
	.form{
		padding: 42px;
	}
	.form table{
		min-width: 400px;
	}
	.form label{
		text-transform: uppercase;
		font-size: 14px;
		color: #aaa;
		margin-bottom: 8px;
	}
	.form input{
		font-size: 18px;
		height: 40px;
		border: 1px solid #ededed;
		text-indent: 6px;
	}
	.form button{
		margin: 0 6px;
		padding: 8px 18px;
		color: #fff;
		background: #D85D30;
		border: 0;
		text-transform: uppercase;
	}
</style>

</head>
<body> 
<div class="container">
	<div id="wrapper">
		<div class="header clearfix">
		<img src="https://d32vr05tkg9faf.cloudfront.net/content/images/emailer/logo.png"/>
		<h1>Registered bank account details</h1>
		</div>
		<c:if test="${not empty message}">
    		<ul>
         		<i><font color="#E56717"><c:out value="${message}"/></font></i>
     	 	</ul>       
		</c:if>
		<div class="form">
		<c:if test="${error ne 'yes'}">
		    <form:form action="/app/onecheck/credits/encash/data/save" method="post" commandName="creditsEncashDetailsWebDOForm">
		        <table border="0" margin="0" width="100">
		            <tr height="80">
		                <td><label>Email Id:</label><br><form:input path="emailId" /></td>
		            </tr>
		            <tr height="80">
		                <td><label>Bank account no:</label><br><form:input path="bankAccountNo" /></td>
		            </tr>
		            <tr height="80">
		                <td><label>Beneficiary Name:</label><br><form:input path="beneficiaryName"  /></td>
		            </tr>
		            <tr height="80">
		                <td><label>IFSC Code:</label><br><form:input path="ifscCode" /></td>
		            </tr>
		            <tr id="editBtn"  height="80">
		                <td colspan="2" align="left">
		                	<button type="submit" value="Edit Details" >Update Details</button>
		                </td>
		            </tr>
		        </table>
		    </form:form>
		</c:if>
		</div>
	</div>
</div>

</body>
<script>
	document.addEventListener('DOMContentLoaded', function(){
		
	});
</script>
</html>