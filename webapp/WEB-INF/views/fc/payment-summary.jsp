<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@page import="java.util.Map"%>
<%@page import="com.freecharge.common.util.FCConstants"%>
<%@page import="com.freecharge.web.webdo.CommonSessionWebDo"%>
<%@page import="com.freecharge.web.util.WebConstants"%>
<%@page import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<%@page import="com.freecharge.common.framework.session.FreechargeSession"%>
<%@ page import="com.freecharge.payment.util.PaymentConstants" %>
<%@ page import="com.freecharge.web.controller.ProductPaymentController" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@page import="com.freecharge.common.framework.util.CSRFTokenManager"%>

<%
    FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    if (fs != null) {
        Map<String, Object> sessionData = fs.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
    }
    String lookId= request.getParameter("lid");
    String prodType = FCSessionUtil.getRechargeType(lookId, fs);
    Integer prodId = FCConstants.productMasterMapReverse.get(prodType);
%>
<c:set value="<%=login%>" var="login"/>

<c:set var="cartItems" value="${cart.itemsList}" scope="request"/>
<c:set var="cartId" value="${cart.id}" scope="request"/>
<c:set var="primaryProductType" value='<%=FCConstants.PRIMARY_PRODUCT_TYPE%>' scope="application"/>
<c:set var="secondaryProductType" value='<%=FCConstants.SECONDARY_PRODUCT_TYPE%>' scope="application"/>
<c:set var="chargesProductType" value='<%=FCConstants.CHARGES_PRODUCT_TYPE%>' scope="application"/>
<c:set var="crosssellEntityName" value='<%=FCConstants.ENTITY_NAME_FOR_CROSSSELL%>' scope="application"/>
<c:set var="couponEntityName" value='<%=FCConstants.COUPON_ENTITY_ID%>' scope="application"/>
<c:set var="couponCounter" value='0'/>
<c:set var="crossSellCounter" value='0'/>
<c:set var="totalCouponPrice" value='0'/>
<c:set var="totalCrossSellPrice" value='0'/>
<c:set var="charges" value='0'/>
<c:set var="lid" value='<%=request.getParameter(FCConstants.LOOKUPID)%>' />

<c:set var="lookupID" value="<%=request.getParameter(\"lookupID\")%>" scope="request" />
<c:set var="walletDisplayName" value="<%=FCConstants.WALLET_DISPLAY_NAME%>"></c:set>

<div id="orderSummary">
    <div class="orderSummary clearfix">
        <div class="rechargeDetails row">
            <ul class="clearfix">
                <c:forEach items="${cartItems}" var="items" varStatus="summaryCtr">
                    <c:if test="${items.productType eq primaryProductType }">
                        <c:if test="${!(items.itemTableName eq crosssellEntityName)}">
                            <li><img src="${mt:keyValue("imgprefix1")}/${operatorImgUrl}" title="${operatorName}" width="30" height="16" /></li>
                            <li><strong>${items.displayLabel}</strong></li>
                            <%--<li><em>${operatorName}</em></li>--%>
                            <li><span class="webRupee">Rs.</span>${items.price}</li>
                        </c:if>
                    </c:if>
                </c:forEach>
            </ul>
            <div class="edit"><a href="/app/edit.htm?lid=${lid}&amp;<%=CSRFTokenManager.CSRF_REQUEST_IDENTIFIER%>=${mt:csrfToken()}">Edit recharge details</a></div>
        </div>

        <div class="billing">
            <h2>Order summary</h2>
            <ul id="crossselldiv">
                <c:forEach items="${cartItems}" var="items" varStatus="summaryCtr">
                    <c:if test="${items.productType eq primaryProductType }">
                        <c:if test="${!(items.itemTableName eq crosssellEntityName)}">
                            <li id="crossselldetail-tr-${items.itemId}" class="recharge">
                                    <span class="type">
                                        <em>${operatorName} Recharge</em>
                                    </span>
                                    <span class="value">
                                        <span class="webRupee">Rs.</span> ${items.price}
                                    </span>
                            </li>
                        </c:if>
                        <input type="hidden" name="${items.itemTableName}-cart-ids" value="${items.itemId}"/>
                        <input type="hidden" name="" value="${items.itemTableName}"/>
                    </c:if>
                    <c:if test="${items.itemTableName eq crosssellEntityName}">
                        <c:set var="crossSellCounter" value='${crossSellCounter + items.quantity}'/>
                        <c:set var="totalCrossSellPrice" value='${totalCrossSellPrice + (items.price * items.quantity)}'/>
                    </c:if>
                    <c:if test="${(items.productType eq secondaryProductType) && (items.itemTableName eq couponEntityName)}">
                        <c:set var="couponCounter" value='${couponCounter + items.quantity}'/>
                        <c:set var="totalCouponPrice" value='${totalCouponPrice + (items.price * items.quantity)}'/>
                    </c:if>
                    <c:if test="${items.productType eq chargesProductType }">
                        <c:set var="charges" value='${charges + items.price}'/>
                    </c:if>
                </c:forEach>

                <li>
                    <span class="type">
                        <c:choose>
                            <c:when test="${couponCounter gt 0 }">
                                <span><%--${couponCounter}--%> Coupons worth <span class="webRupee">Rs.</span>${totalCouponPrice} </span>
                                <%-- DO NOT delete below code , Consult Suhel
                                 <div>
                                     <c:forEach items="${cartItems}" var="cartItem" >
                                        <c:if test="${cartItem.itemTableName eq couponEntityName}">
                                           ${couponStoreMap[cartItem.itemId].campaignName} &rarr; ${cartItem.quantity} x Rs. ${couponStoreMap[cartItem.itemId].faceValue}  <br/>
                                        </c:if>
                                     </c:forEach>
                                </div> --%>
                            </c:when>
                            <c:otherwise>
                                <span>No Coupons selected</span>
                            </c:otherwise>
                        </c:choose>
                    </span>
                    <span class="value">
                        FREE
                    </span>
                </li>

                <li>
                    <span class="type">Handling charge</span>
                        <span class="value">
                            <span class="webRupee">Rs.</span> ${charges}
                        </span>
                </li>
                <li id="orderSummary-upsell">
                    <span class="type">Special Deals (${crossSellCounter})</span>
                    <span class="value">
                        <span class="webRupee">Rs.</span> ${totalCrossSellPrice}
                    </span>
                </li>

                <c:set var="freefundEntityName" value='<%=FCConstants.ENTITY_NAME_FOR_FREEFUND%>' scope="application"/>
                <c:set var="freefundExistsInCart" value="false"/>
                <c:set var="freefundCode" value=""/>
                <c:set var="freefundAmount" value="0"/>
                <c:forEach items="${cartItems}" var="citems" varStatus="cartStatus">
                    <c:if test="${citems.itemTableName eq freefundEntityName}">
                         <c:set var="freefundExistsInCart" value="true"/>
                         <c:set var="freefundCode" value="${citems.displayLabel }"/>
                         <c:set var="freefundAmount" value="${citems.price }"/>
                    </c:if>
                </c:forEach>
                <c:if test="${freefundExistsInCart}">
                <li  id="promoCodePrice">
                    <span class="type">Discount</span>
                        <span class="value">
                            <span class="red"><span class="webRupee">Rs.</span><span id="promoCodePriceHolder">${freefundAmount}</span></span>
                        </span>
                </li>
                </c:if>

                <li class="total">
                    <span class="type">Total payable</span>
                        <span class="value">
                            <strong>
                                <span class="webRupee">Rs.</span>
                                <span id="totalCartPrice"> ${totalPayAmount}</span>
                                <c:set var="totalPayable" value="${totalPayAmount}"></c:set>
                            </strong>
                        </span>
                </li>
            </ul>
        </div>
    </div>
</div>
<script>
var crossSellCounter = parseInt("${crossSellCounter}");
var totalCrossSellPrice = parseInt("${totalCrossSellPrice}");
if(${totalPayable == 0}) {
	$("#promoContinue").show(); 
    $('.page-action').hide();
} else if (${totalPayable > 0}) {
	$("#promoContinue").hide(); 
    $('.page-action').show();
}
</script>
<script type="text/javascript">
function refreshCart(lid) {
	var url='/app/cart_summary.htm?lid='+lid;
	$.ajax({
	    url: url,
	    dataType: "html",
	    cache: false,
	    timeout:1800000,
	    success: function (responseText) {
	    	$("#payment-summary-holder").html(responseText); 
	    },
	    error:function (xhr,status,error){
	      $("#payment-summary-holder").html("Error occurred "); 
	    }
	});
}
</script>