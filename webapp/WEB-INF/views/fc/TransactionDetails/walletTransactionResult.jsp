<%@page import="com.freecharge.admin.CustomerTrailDetails.EmailInputController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">

		<c:forEach var="message" items="${messages}">
			<div class="alert alert-error" >
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  ${message}
			</div>
		</c:forEach>

		<c:if test="${not empty exception }">
			<div class="alert alert-error" >
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  ${exception}
			</div>
		</c:if>

		<div class="customer-trail-filter">
			  <p>Wallet Status</p>
			  <select id="wallet-status-filter" onchange="filterCall(this,'customer-trail-result','All')" >
					<option value="All" >All</option>
					<option value="depositEntry" >Deposit</option>
					<option value="withdrawalEntry" >Withdrawal</option>
			  </select>
		</div>

		<h5><spring:message code="wallet.transaction.details" text="#wallet.transaction.details#"/></h5>
		<div class="wallet-account">
			<div class="clearfix" >
				<div class="fcBalance-generalBlock lf" >
					<h6>
						<spring:message code="available.balance" text="#available.balance#"/>: <b><span class="webRupee"><spring:message
									code="rupee" text="#rupee#" /></span>.${balance}</b>
					</h6>
					<p>
						<strong><spring:message code="last.activity" text="#last.activity#"/>:</strong>
						<c:choose>
							<c:when test="${not empty lastEntry}">
								<c:choose>
									<c:when test="${lastEntry.IS_DEBIT}">
										<spring:message code="payment.using" text="#payment.using#" />
										<span class="webRupee"><spring:message code="rupee"
												text="#rupee#" /></span>.${lastEntry.txn_amount}
									</c:when>
									<c:otherwise>
										<span class="webRupee"><spring:message code="rupee"
												text="#rupee#" /></span>.${lastEntry.txn_amount} <spring:message
											code="deposited.fc" text="#deposited.fc#" />
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<spring:message code="none" text="#none#"/>
							</c:otherwise>
						</c:choose>
					</p>
					<p><b><spring:message code="order.id" text="#order.id#"/>:</b> ${lastEntry.order_id} </p>
					<p><b><spring:message code="date" text="#date#"/>:</b> ${lastEntry.created_ts}</p>
				</div>
			</div>

			<table class="table table-condensed" id="customer-trail-result" >
				<thead>
					<tr>
						<th width="11%" ><spring:message code="date" text="#date#"/></th>
						<th width="43%" ><spring:message code="description" text="#description]#"/></th>
						<th width="30%" ><spring:message code="order.id" text="#order.id#"/></th>
						<th width="8%" ><spring:message code="deposit" text="#deposit#"/></th>
						<th width="11%"><spring:message code="withdrawal" text="#withdrawal#"/></th>
						<th width="9%" ><spring:message code="balance" text="#balance#"/></th>
						<th width="12%"><spring:message code="fund.source" text="#fund.source#"/></th>
						<th width="12%"><spring:message code="fund.destination" text="#fund.destination#"/></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${rows}" var="each">
						<tr class="<c:choose >
										<c:when  test="${each.IS_DEBIT eq 'true'}" >withdrawalEntry</c:when>
										<c:otherwise>depositEntry</c:otherwise>
									</c:choose>" >
							<td>${each.created_ts}</td>

							<c:choose>
							  <c:when test="${each.operator != NULL}">
								<td>
								${each.description}
								<%-- <p class="clearfix" >${each.operator} | ${each.subscriber_number} | ${each.circle} </p> --%>
								<c:if test="${each.fund_source eq 'REFUND'}">
								<span class="flag-rechargeStatus" ><spring:message code="refund" text="#refund#"/></span>
								</c:if>

								<c:if test="${each.fund_destination eq 'RECHARGE' and each.recharge_failed eq true}">
								<span class="flag-rechargeStatus flag-rechargeStatus-failed" ><spring:message code="failed" text="#failed#"/></span>
								</c:if>

								<c:if test="${each.fund_destination eq 'RECHARGE' and each.recharge_failed eq false and each.postpaid_retry eq true}">
								<span class="flag-rechargeStatus flag-rechargeStatus-failed" ><spring:message  text="retry-success"/></span>
								</c:if>
								<c:if test="${each.fund_destination eq 'RECHARGE' and each.recharge_failed eq true and each.postpaid_retry eq true}">
								<span class="flag-rechargeStatus flag-rechargeStatus-failed" ><spring:message  text="retry"/></span>
								</c:if>

								<c:choose>
								  <c:when test="${each.fund_source eq 'RECHARGE_FUND'}">
								  </c:when>
								  <c:otherwise>
									<p class="clearfix" >${each.operator} | ${each.subscriber_number} | ${each.circle} </p>
								  </c:otherwise>

								</c:choose>
								</td>
							  </c:when>
							   <c:otherwise>
								<td>filter
								  ${each.description}
								</td>
							  </c:otherwise>
							</c:choose>

							<td>${each.order_id}</td>
							<c:choose>
								<c:when test="${each.IS_DEBIT eq 'true'}">
									<td></td>
									<td><span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span>${each.txn_amount}</td>
								</c:when>
								<c:otherwise>
									<td><span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span>${each.txn_amount}</td>
									<td></td>
								</c:otherwise>
							</c:choose>
							<td><span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span> ${each.NEW_BALANCE}</td>
							<td>${each.fund_source}</td>
							<td>${each.fund_destination }
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>