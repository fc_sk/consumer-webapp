<%@page import="com.freecharge.admin.CustomerTrailDetails.EmailInputController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@page import="com.freecharge.customercare.controller.CustomerTrailController"%>

<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp"/>
	</div>

	<div class="col-md-9 col-sm-9">
		<c:forEach var="message" items="${messages}">
			<div class="alert alert-error" >
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  ${message}
			</div>
		</c:forEach>

		<c:if test="${not empty exception }">
			<div class="alert alert-error" >
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  ${exception}
			</div>
		</c:if>

		<c:forEach var="customerCare" items="${customerCareList}">
		   <c:set var="isEmpty" value="customerCare"></c:set>
		</c:forEach>

		<c:forEach var="walletTransaction" items="${walletTransactionList}">
		   <c:set var="isEmpty"  value="walletTransaction"/>
		</c:forEach>
		
		<c:forEach var="upiTransaction" items="${upiTransactionsList}">
		   <c:set var="isEmpty"  value="upiTransaction"/>
		</c:forEach>

		<c:choose>
			<c:when test="${searchTime eq 'TWO'}">
			   <center><h4><u>Displays only 2 months recharge and addcash transactions</u></h4></center>
			</c:when>
			<c:when test="${searchTime eq 'ALL'}">
			   <center><h4><u>Displays all recharge and addcash transactions</u></h4></center>
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>

		<h3><span class="label label-info"><spring:message code="transaction.information.details" text="#transaction.information.details#"/></span></h3>

		<div class="customer-trail-filter">
			  <p>Payment Status</p>
			  <select id="payment-status-filter"  onchange="filterCall(this,'customer-trail-result','All')" >
					<option value="All" >All</option>
					<option value="Successfull" >Payment Successfull</option>
					<option value="UnSuccessfull" >Payment UnSuccessfull</option>
			  </select>
		</div>
		
		<b><i>FC-SD User Migration status : </i></b><a href="#" data-toggle="tooltip" data-placement="right" title="${userMigrationToolTipText}">${fcSdUserMigration}</a><br>
		<b><i>One Check Wallet Status: </i></b><c:out value="${ocwalletStatus}"/><br>
		<b><i>Account Status: </i></b><c:out value="${imsDetails.accountState}"/><br>
		<b><i>SD Identity: </i></b><c:out value="${sdIdentity}"/>
		<hr>

		<table class="table table-hover table-bordered" id="customer-trail-result" >
			<thead>
				<tr>
					<th><spring:message code="order.id" text="#order.id#"/></th>
					<th><spring:message code="order.date" text="#order.date#"/></th>
					<th><spring:message code="recharge.amount" text="#recharge.amount#"/></th>
					<th><spring:message code="total.amount" text="#total.amount#"/></th>
					<th><spring:message code="product.type" text="#product.type#"/></th>
					<th><spring:message code="payment.status" text="#payment.status#"/></th>
					<th><spring:message code="recharge.status" text="#recharge.status#"/></th>
					<th><spring:message code="pg" text="#pg#"/></th>
					<th><spring:message code="ag" text="ag"/></th>
					<th><spring:message code="p.vouchers" text="#p.vouchers#"/></th>
					<th><spring:message code="e.vouchers" text="#e.vouchers#"/></th>
					<th><spring:message code="coupons.status" text="#coupons.status#"/></th>
					<th><spring:message code="promocode"  text="#promocode#"/></th>
				</tr>
			</thead>

			<c:if test="${not empty isEmpty }">
				<tbody>
				<c:forEach var="customerCare" items="${customerCareList}">
					<tr class="${customerCare.paymentStatus}" >
						<td>
							<a href="/admin/customertrail/getcustomertrail?orderId=${customerCare.orderid}">${customerCare.orderid}</a>
						</td>
						<td>${customerCare.orderDate}</td>
						<td>${customerCare.rechargeAmount}</td>
						<td>${customerCare.totalAmount}</td>
						<td>${customerCare.productType}</td>
						<td>${customerCare.paymentStatus}</td>
						<td>${customerCare.rechargeStatus}</td>
						<td>${customerCare.pgName}</td>
						<td>${customerCare.agName}</td>
						<td>${customerCare.pVouchers}</td>
						<td>${customerCare.eVouchers}</td>
						<td>${customerCare.couponsStatus}</td>
						<td><a href="/admin/customertrail/getorderidbycouponcode?promocode=${customerCare.promocode}">${customerCare.promocode}</a></td>
					</tr>
				</c:forEach>
				</tbody>
			</c:if>
		</table>
		<script language="javascript" type="text/javascript">
		var tableFilters = {
				col_0: "select",
				col_1: "select",
				col_2: "select",
				col_3: "select",
				col_4: "select",
				col_5: "select",
				col_6: "select",
				col_7: "select",
				col_8: "select",
				col_9: "select",
				col_10: "select",
				col_11: "select",
				col_12: "select",
				display_all_text: "All"
			}
		setFilterGrid("customer-trail-result", tableFilters);
	</script>
		<hr>

		<h3><span class="label label-info"><spring:message code="addcash.transaction.information.details" text="#addcash.transaction.information.details#"/></span></h3>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
			  <th><spring:message code="order.id" text="#order.id#"/></th>
					<th><spring:message code="transaction.date" text="#transaction.date#"/></th>
					<th><spring:message code="transaction.type" text="#transaction.type#"/></th>
					<th><spring:message code="deposit.amount" text="#deposit.amount#"/></th>
					<th><spring:message code="old.balance" text="#old.balance#"/></th>
					<th><spring:message code="new.balance" text="#new.balance#"/></th>
					<th><spring:message code="fund.source" text="#fund.source#"/></th>
			</tr>
			</thead>

			<c:if test="${not empty isEmpty}">
				<tbody>
					<c:forEach var="walletTransaction" items="${walletTransactionList}">
						<tr>
							<td>
							   <a href="/admin/customertrail/getcustomertrail?orderId=${walletTransaction.orderId}">${walletTransaction.orderId}</a>
							 </td>
							   <td align="center">${walletTransaction.transactionDate}</td>
							   <td align="center">${walletTransaction.transactionType}</td>
							   <td align="center">${walletTransaction.transactionAmount}</td>
							   <td align="center">${walletTransaction.runningBalance - walletTransaction.transactionAmount}</td>
							   <td align="center">${walletTransaction.runningBalance}</td>
							   <td align="center">${walletTransaction.fundSource}</td>
						</tr>
				   </c:forEach>
				</tbody>
		  </c:if>
		</table>
		<HR size="4" color="sky blue">
		  <%--  <span class="label label-info">IMP</span>
		   <a href="/admin/email_input_transaction/alltxns/do.htm?email=${emailentered}"><i>Show all recharge and addcash transactions</i></a><font color="red"> (Click only if you need all transactions)</font> --%>
		<c:choose>
			<c:when test="${searchTime eq 'TWO'}">
				<span class="label label-important">IMP</span>
				<a
					href="/admin/email_input_transaction/alltxns/do.htm?email=${emailentered}"><i>Show
						all recharge and addcash transactions</i></a>
				<font color="red"> (Click only if you need all transactions)</font>
			</c:when>
			<c:when test="${searchTime eq 'ALL'}">
				<span class="label label-important">IMP</span>
				<a
					href="/admin/email_input_transaction/do.htm?email=${emailentered}"><i>Show
						only two months recharge and addcash transactions</i></a>
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
		<HR>
		<h3><span class="label label-info"><spring:message code="upi.transaction.information.details" text="#upi.transaction.information.details#"/></span></h3>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th><spring:message code="upi.fc.transactionid"	text="#upi.fc.transactionid#" /></th>
					<th><spring:message code="upi.transaction.id" text="#upi.transaction.id#" /></th>
					<th><spring:message code="upi.transaction.status" text="#upi.transaction.status#" /></th>
					<th><spring:message code="upi.transaction.date" text="#upi.transaction.date#" /></th>
					<th><spring:message code="upi.transaction.amount" text="#upi.transaction.amount#" /></th>
					<th><spring:message code="upi.source.vpa" text="#upi.source.vpa#" /></th>
					<th><spring:message code="upi.destination.vpa" text="#upi.destination.vpa#" /></th>
					<th><spring:message code="upi.errmsg" text="#upi.errmsg#" /></th>
					<th><spring:message code="upi.businessusecase" text="#upi.businessusecase#" /></th>
					<th><spring:message code="upi.errcode" text="#upi.errcode#" /></th>
					<th><spring:message code="upi.promocode" text="#upi.promocode#" /></th>
				</tr>
			</thead>

			<c:if test="${not empty isEmpty }">
				<tbody>
					<c:forEach var="upiTransaction" items="${upiTransactionsList}">
						<tr>
							<td><a
								href="/admin/customertrail/upi?transactionId=${upiTransaction.globalTxnId}&transactionType=${upiTransaction.globalTxnType}&mobileNo=${upiTransaction.mobileNumber}">
									${upiTransaction.globalTxnId}</a></td>
							<td>${upiTransaction.merchantOrderId}</td>
							<td>${upiTransaction.txnStatus}</td>
							<td>${upiTransaction.transactionDate}</td>
							<td>${upiTransaction.txnAmount}</td>
							<c:choose>
								<c:when test="${not empty upiTransaction.upiInfo}">
									<td>${upiTransaction.upiInfo.sourceVpa}</td>
									<td>${upiTransaction.upiInfo.destVpa}</td>
								</c:when>
								<c:otherwise>
									<td></td>
									<td></td>
								</c:otherwise>
							</c:choose>
							<td>${upiTransaction.upiNpciErrorMessage}</td>
							<td>${upiTransaction.businessUseCase}</td>
							<c:choose>
								<c:when test="${not empty upiTransaction.upiInfo}">
									<td>${upiTransaction.upiInfo.errCode}</td>
								</c:when>
								<c:otherwise>
									<td></td>
								</c:otherwise>
							</c:choose>
							<td>${upiTransaction.promoCode}</td>
						</tr>
					</c:forEach>
				</tbody>
		  </c:if>
		</table>
	</div>
</div>