<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.freecharge.customercare.controller.CustomerTrailController"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Landing Page</title>
</head>
<body>
<table>
	<c:forEach var="orderid" items="${orderids}">  
 		<tr>
 			<td><a href="<c:url value="<%=CustomerTrailController.CUSTOMER_TRAIL_LINK %>">
 			<c:param name="orderId" value="${orderid}"/>
 			</c:url>">${orderid}</a></td>
 		</tr>
 	</c:forEach>  
 </table>
</body>
</html>