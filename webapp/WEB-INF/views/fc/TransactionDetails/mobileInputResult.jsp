<%@page import="com.freecharge.admin.CustomerTrailDetails.MobileInputController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@page import="com.freecharge.customercare.controller.CustomerTrailController"%>
<div class="row">
    <div class="col-sm-3 col-md-3">
        <jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp"/>
    </div>

    <div class="col-sm-9 col-md-9">
        <c:forEach var="message" items="${messages}">
            <div class="alert alert-error" >
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              ${message}
            </div>
            <c:out value="${message}"/>
        </c:forEach>

        <c:if test="${not empty exception }">
            <div class="alert alert-error" >
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <c:out value="${exception}"/>
            </div>
        </c:if>
        <%-- <form:form method="POST" action="<%=MobileInputController.MOBILE_INPUT_LINK%>">
             <jsp:include page="/WEB-INF/views/fc/admin/customerTrailHome.jsp"/>
            <label for="mobile"><spring:message code="enter.user.mobile.number" text="#enter.user.mobile.number#"/>:</label>
            <form:input path = "mobile"></form:input>
            <input type="submit" name="Submit" value="Submit">
            <div class="red"><form:errors path = "mobile"></form:errors></div>
        </form:form>  --%>


        <h5><spring:message code="transaction.information.details" text="#transaction.information.details#"/></h5>
        <table class="table table-hover table-bordered" id="customer-trail-result" >
            <thead>
                <tr>
                    <th><spring:message code="order.id" text="#order.id#"/></th>
                    <th><spring:message code="order.date" text="#order.date#"/></th>
                    <th><spring:message code="recharge.amount" text="#recharge.amount#"/></th>
                    <th><spring:message code="total.amount" text="#total.amount#"/></th>
                    <th><spring:message code="product.type" text="#product.type#"/></th>
                    <th><spring:message code="payment.status" text="#payment.status#"/></th>
                    <th><spring:message code="recharge.status" text="#recharge.status#"/></th>
                    <th><spring:message code="pg" text="#pg#"/></th>
                    <th><spring:message code="ag" text="#ag#"/></th>
                    <th><spring:message code="p.vouchers" text="#p.vouchers#"/></th>
                    <th><spring:message code="e.vouchers" text="#e.vouchers#"/></th>
                    <th><spring:message code="coupons.status" text="#coupons.status#"/></th>

                </tr>
            </thead>
            <tbody>
            <c:forEach var="customerCare" items="${customerCareList}">
                <tr>
                    <td>
                        <a href="/admin/customertrail/getcustomertrail?orderId=${customerCare.orderid}">${customerCare.orderid}</a>
                    <td>${customerCare.orderDate}</td>
                    <td>${customerCare.rechargeAmount}</td>
                    <td>${customerCare.totalAmount}</td>
                    <td><c:out value="${customerCare.shippingAddress}"/></td>
                    <td>${customerCare.paymentStatus}</td>
                    <td>${customerCare.rechargeStatus}</td>
                    <td>${customerCare.pgName}</td>
                    <td>${customerCare.agName}</td>
                    <td>${customerCare.pVouchers}</td>
                    <td>${customerCare.eVouchers}</td>
                    <td>${customerCare.couponsStatus}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <%-- Fragment to display UPI transactions --%>
    <c:if test="${not empty upiTransactionsList }">
	    <div class="col-sm-9 col-md-9" style="float: right;">
	    	 <jsp:include page="/WEB-INF/views/fc/admin/upi/upiTransactionsList.jsp"/>
	    </div>
    </c:if>
</div>
