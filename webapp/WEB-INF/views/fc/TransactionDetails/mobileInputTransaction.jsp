<%@page import="com.freecharge.admin.CustomerTrailDetails.MobileInputController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<section class="main">
	<c:forEach var="message" items="${listmessages}">
		<div class="alert alert-error" >
	      <button type="button" class="close" data-dismiss="alert">&times;</button>
	      <c:out value="${message}"/>
	    </div>    
	</c:forEach>

	<form:form method="POST" action="<%=MobileInputController.MOBILE_INPUT_LINK%>">
	    <label for="mobile"><spring:message code="enter.user.mobile.number" text="#enter.user.mobile.number#"/></label>
	    <div class="input-append">
          <form:input path = "mobile" class="span2" />
          <button class="btn" type="submit">GO</button>
          <div class="red"><form:errors path = "mobile"></form:errors></div>
        </div>	    

	</form:form>
</section>