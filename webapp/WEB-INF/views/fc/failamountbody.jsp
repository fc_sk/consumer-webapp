<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- .mainContainer Starts -->
<div class="mainContainer"  >
    <div class="pageWidth clearfix">
        <h1 class="pageHeader clearfix " ><img src="${mt:keyValue("imgprefix1")}/images/cross.png" alt="Failure" width="30" height="30" class="ico fLeft" > <span class="mainTxt fLeft" >Sorry! <span class="softTxt" >Recharge unsuccessful due to  invalid recharge amount </span></span></h1>

        <div class="successMsgWrapp curveBlockWhite clearfix">
            <p>Recharge on <strong><c:out value="${rechargeInitiateWebDo.mobileNo}"/></strong> for
                <strong>Rs. <c:out value="${rechargeInitiateWebDo.amount}"/></strong> &nbsp;<span class="redClr">unsuccessful</span>
                <br>
                <span class="lightClr">Your refund has been initiated from our end, You will receive your money within a few days. <br> You would not receive FREE coupons for this unsuccessful transaction </span>              
            </p>
            <form:form modelAttribute="txnHomePageWebDO" action="home.htm" method="get" name="edit" id="retryForm"  >
                <input type="hidden" name="lookupID" value="${rechargeInitiateWebDo.lookupID}"/>
                <p><button onclick="javascript:submitForEdit('edit')" type="submit"  >Recharge with valid amount</button></p>             
            </form:form>
        </div>
        <!-- /.successMsgWrapper -->

    </div>
</div>
<!-- .mainContainer Ends -->
<script type="text/javascript" >
jQuery(document).ready(function() {
	$("button").focus(); // set default focus on the button
})
</script>
