<div class="row" >
	<div class="four columns">
    	<div class="staticpage-navigation"  id="freefund-tnc-tabs" >
			<ul class="staticpage-navigation">
				<li><a href="#tnc-baskin-robbins">Baskin Robbins</a></li>
			</ul>
		</div> <%-- /.staticpage-navigation --%>
    </div>
    
    <div class="twelve columns">
    	<div class="staticpage-content" id="freefund-tnc-content" >
			<div id="tnc-baskin-robbins">
				<h1>Baskin Robbins - Terms of the Program</h1>
				<ol>
					<li>This Talktime Promotion (&ldquo;Offer&rdquo;) is being brought to you by Baskin Robbins 
					(&ldquo;BR&rdquo;) in association with <a href="/" target="_blank" >FreeCharge.com</a>.</li>
					<li>This Offer is valid only in India from 9th Dec, 2013 to 10th Feb, 2014 (both dates included) 
					(&lsquo;offer period&rsquo;). Spend Rs.100/- &amp; above at BR and get an assured gift by way of a scratch 
					cards, hereinafter referred to as &ldquo;Gift&rdquo;.</li>
					<li>The offer is open to Indian residents except employees and the family members of Organizers, 
					their associate companies, advertising agencies and their auditors.</li>
					<li>Participation is optional. </li>
					<li>The campaign is only valid for the citizens of India, holding valid prepaid GSM / CDMA mobile phone connections of the following operators:
						<ol class="alpha operator-list clearfix">
							<li>Airtel</li>
							<li>Aircel</li>
							<li>Vodafone</li>
							<li>BSNL</li>
							<li>Tata Docomo</li>
							<li>Idea</li>
							<li>Tata Walky</li>
							<li>Loop</li>
							<li>MTNL Trump</li>
							<li>Reliance CDMA</li>
							<li>Reliance GSM</li>
							<li>Tata Indicom</li>
							<li>Uninor</li>
							<li>MTS</li>
							<li>Videocon</li>
							<li>Virgin CDMA</li>
							<li>Virgin GSM</li>
						</ol>
					</li>
					<li>Scratch and win free talktime recharge upto Rs.75/-.</li>
					<li>To participate,
						<ol class="alpha">
							<li>Visit and register on &lsquo;<a href="/" target="_blank" >www.FreeCharge.com</a>&rsquo; or 
							&lsquo;<a href="/m" target="_blank" >www.FreeCharge.com/m</a>&rsquo;.</li>
							<li>Enter your prepaid mobile number and enter recharge amount.</li>
							<li>Enter the unique promo code printed on the scratch card to get discount as mentioned in the scratch card.</li>
							<li>Pay the balance via Credit/Debit card to get instant recharge.</li>
						</ol>
					</li>
					<li>A code can be used to recharge only once. A prepaid mobile number/user account/Debit/Credit 
					card is entitled only 2 free talktime recharge during the period of the Offer. All subsequent attempts 
					to recharge the same mobile number/user account or using same Debit/credit card with a different code will be rejected.</li>
					<li>This offer is valid only for debit/credit card users. Users who pay the balance amount through 
					Debit/Credit cards will only be eligible for this discount.</li>
					<li>Participants already registered with FreeCharge can also use their existing credentials to avail 
					of the Offer.</li>
					<li>On direct payment of the applicable handling charges, FreeCharge may offer the Participant to 
					select exciting coupons of value equal to the recharge amount. In case the Participant avails such 
					add ons or other facility (ies) / offer(s) then participant should pay the additional amount to 
					FreeCharge via Debit/Credit cards.</li>
					<li>The talktime credited into a customer&#39;s account is non-transferrable, non-refundable; and 
					no cash payment will be made in lieu of this talktime. Actual talktime will vary as per the top-up plan of 
					the respective operator and is subject to applicable taxes and levies.</li>
					<li>The code cannot be redeemed retrospectively i.e. after the expiry of the offer period. Any unused 
					balance will not be refunded or credited when offer period has expired.</li>
					<li>The offer is not valid in conjunction with any other offer.</li>
					<li>Neither BR nor FreeCharge will be liable for any delay in crediting the talktime into the customer&#39;s 
					account while transacting on FreeCharge as the time taken for crediting the talktime into the customer&#39;s 
					account is dependent on the telecom operators of the prepaid mobile customer.</li>
					<li>BR and FreeCharge shall not be responsible for downtime on services rendered by the telecom operators / 
					provider that may occur for any reason whatsoever including without limitation due to decisions and / or 
					changes in regulations that are carried out by TRAI , DoT or any other regulatory body.</li>
					<li>BR and FreeCharge shall not be responsible for downtime caused due to technical difficulties or 
					downtime created at the telecom operator / provider&#39;s end either due to failure of hardware equipment at the 
					telecom providers end, configuration issues at the end of the Telecom operator /provider's end, network 
					congestion or for any other reason whatsoever. FreeCharge will not be responsible for any loss or damage of the code.</li>
					<li>All customers related queries for this promotion can be addressed to <a href="mailto:care@freecharge.com">care@freecharge.com</a>.</li>
					<li>FreeCharge reserves the right to update and/or amend these Terms and Conditions.</li>
					<li>FreeCharge reserves the right to extend or terminate this Offer without prior notice.</li>
					<li>FreeCharge reserves the right to refuse to credit the talktime into the participants&#39; mobile 
					account due to fraud or system security breaches.</li>
					<li>Decision of FreeCharge will be final and binding with regard to promotion and prize and no correspondence 
					will be entertained in this regard.</li>
					<li>At the request of FreeCharge, winners may be required to participate in promotional activities 
					(such as publicity and photography), free of charge, and he/she consents to the FreeCharge for using 
					their name, likeness, image and/or voice in the event the winner (including photograph, film and/or 
					recording of the same) in promotional material or in any media for an unlimited period without remuneration 
					for the purpose of promoting this promotion (including any outcome , and promoting any products/services of FreeCharge.</li>
					<li>FreeCharge.com is a registered trademark of Accelyst Solutions Pvt Ltd (ASPL).</li>
					<li>The participant is bound by the terms and conditions imposed by FreeCharge.com on its website.</li>
					<li>No cash payment will be made in lieu of the prize.</li>
					<li>FreeCharge accepts no responsibility for any tax implications that may arise from the prize winning. 
					Participants will have to bear incidental costs, if any, that may arise for redemption of the prize.</li>
					<li>FreeCharge shall not be responsible for any loss, injury or any other liability arising out of 
					the Promotion or due to participation by any person in the Promotion.</li>
					<li>FreeCharge shall not be liable for any loss or damage due to Act of God, Governmental actions, 
					other force majeure circumstances and shall not be liable to pay any amount as compensation or otherwise for any such loss.</li>
					<li>All disputes relating to this Promotion shall be subject to the exclusive jurisdiction of Courts 
					at Mumbai only.</li>
				</ol>
			</div>
		</div> <%-- /.staticpage-content --%>
    </div>
</div> 