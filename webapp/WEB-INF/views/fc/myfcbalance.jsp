<%@ page import="com.freecharge.common.util.FCConstants"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<c:set var="walletDisplayName" value="<%=FCConstants.WALLET_DISPLAY_NAME%>"></c:set>

<div class="my-account">
    <div class="sixteen columns">
        <h1><spring:message code="my.account" text="#my.account#"/></h1>
    </div>
    <div class="three columns">
        <div class="my-account-list">
            <ul>
                <li class="active"><a href="mycredits.htm"><spring:message code="fc.money" text="#fc.money#"/></a></li>
                <li><a href="myrecharges.htm"><spring:message code="previous.recharges" text="#previous.recharges#"/></a></li>
                <li><a href="mycontacts.htm"><spring:message code="contacts" text="#contacts#"/></a></li>
                <li><a href="myprofile.htm"><spring:message code="profile" text="#profile#"/></a></li>
                <li><a href="mycoupons.htm"><spring:message code="my.coupons" text="#coupons#"/></a></li>
            </ul>
        </div>
	</div>
	<div class="thirteen columns">
        <div class="my-account-content">
            <c:if test="${paymentStatus eq true}">
                <p class="message-success-small">You successfully deposited cash in your Credits.</p>
            </c:if>
            <div class="balanceWrapInner">
                <div class="balance_summary" >
                    <h2>Your available Credits:<strong class=""> <span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span> ${balance}</strong><i class="icon-info" id="fcb-tip"></i></h2>
                    <div class="getbalance_summary">
                        <p><strong>Get more <spring:message code="fc.money" text="#fc.money#"/>!</strong>We offer you a whole range of options for depositing more cash in your <spring:message code="fc.money" text="#fc.money#"></spring:message>.</p>
                    </div>
                </div>
                <div id="fcb-tooltip" class="tooltip" style="display: none">
                    <h6>What are <spring:message code="fc.money" text="#fc.money#"/>?</h6>
                    <p>If for some reason, your recharge is unsuccessful, we make a refund to your FreeCharge Credits. Your money is totally safe with us, and you can use it anytime for future recharging. Find out more,<a href="/payment-options/freecharge-credits"  target="_blank"> here.</a>What&#39;s more, you can also deposit cash to facilitate quick and secure recharging, try it below.</p>
                </div>

                <div class="balance_schemes" >
                    <div class="balance_scheme" >
                        <div class="balance_scheme_depositeCash balance_scheme_title" >
                            <span class="icon" > </span><h5>Deposit Cash</h5>
                            <p>Deposit money in your Credits and speed-up your recharging process!</p>
                        </div>
                        <div class="balance_scheme_form"  style="display: none" >
                            <a href="javascript:void(0)" class="closeLink" title="close"><i class="icon-x"></i></a>
                            <p class="title">Enter amount you want to add and click Deposit :</p>
                            <form:form id="addCashForm" action="/app/addcashproducttocart.htm" method="post" modelAttribute="addCashWebDo">
                                <div>
                                    <form:input path="addCashAmount"
                                                placeholder="Amount"
                                                cssClass="validate[required,funcCall[walletAddAmountTest], custom[onlyNumberSp]] input-text"
                                                onkeypress="return isNumberKey(event,4,id);"
                                                maxlength="4" />
                                    <%--<input class="button buttonOne small-button" value="proceed"  type="submit" onclick="handleAddCash();" />--%>
                                    <button class="buttonOne small-button" onclick="handleAddCash();">Deposit</button>
                                </div>
                            </form:form>
                        </div>
                    </div>

                    <div class="balance_scheme" >
                        <div class="balance_scheme_freefund balance_scheme_title" >
                            <span class="icon" > </span><h5>FreeFund <i class="schemes_tooltip icon-info tooltip" data-tooltip-id="freefund-tooltip"></i></h5>
                            <p>Use a Freefund code to add money to your Credits. </p>
                        </div>
                        <div id="freefund-tooltip" style="display: none">
                            A FreeFund code is like a gift voucher code. The amount is fixed and it can be deposited instantly in your FreeCharge Credits.
                        </div>
                        <div class="balance_scheme_form"  style="display: none" >
                            <a href="javascript:void(0)" class="closeLink" title="close"><i class="icon-x"></i></a>
                            <div id="redeemToWallet" class="add-freefund">
                                <jsp:include page="payment-promocode.jsp">
                    				<jsp:param value="credits" name="productType"/>
                    				<jsp:param value="" name="lookupID"/>
                    			</jsp:include>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="balanceSummaryWrapInner" >
                <div class="balanceSummary_title" >
                    <h3>FreeCharge Credits Summary</h3>
                    <em>Last activity </em>
                    <c:choose>
                        <c:when test="${not empty lastEntry}">
                            <c:choose>
                                <c:when test="${lastEntry.IS_DEBIT}">
                                    <em>was on <strong>${lastEntry.created_ts} :</strong></em> <spring:message code="payment.using" text="#payment.using#" /> <span class="webRupee"><spring:message code="rupee" text="#rupee#"/></span>${lastEntry.txn_amount}
                                </c:when>
                                <c:otherwise>
                                    <em>was on <strong>${lastEntry.created_ts} :</strong></em> <span class="webRupee"><spring:message code="rupee" text="#rupee#"/></span>${lastEntry.txn_amount} <spring:message code="deposited.fc" text="#deposited.fc#" /> 
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            None
                        </c:otherwise>
                    </c:choose>
                    <br><br>
                    <a href="javascript:void(0)" onclick="loadBalanceActivity('.balanceSummary_title')" >Click here to see your complete Credits activity.</a>
                </div>

                <table class="table-generic" id="balanceActivityTable" style="display:none;" >
                    <thead>
                    <tr>
                        <th width="11%" >Date</th>
                        <th width="40%" >Description</th>
                        <th width="21%" >Order ID</th>
                        <th width="8%" >Deposit</th>
                        <th width="11%">Withdrawal</th>
                        <th width="9%" >Balance</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${rows}" var="each">
                        <tr>
                            <td>${each.created_ts}</td>

                            <c:choose>
                                <c:when test="${each.operator != NULL}">
                                    <td>

                                        ${each.description}

                                        <c:if test="${each.fund_source eq 'REFUND'}">
                                            <span class="flag-rechargeStatus" >Refund</span>
                                        </c:if>

                                        <c:if test="${each.fund_destination eq 'RECHARGE' and each.recharge_failed eq true}">
                                            <span class="flag-rechargeStatus flag-rechargeStatus-failed" >Failed</span>
                                        </c:if>

                                        <c:choose>
                                            <c:when test="${each.fund_source eq 'RECHARGE_FUND'}">
                                            </c:when>
                                            <c:otherwise>
                                                <p class="clearfix" >${each.operator} | ${each.subscriber_number} | ${each.circle} </p>
                                            </c:otherwise>

                                        </c:choose>
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td>
                                        ${each.description}
                                        <c:if test="${each.fund_source eq 'CASH_FUND'}">
                                            <br>
                                            <a href="invoice.htm?orderId=${each.order_id}" target="_blank">View invoice</a>
                                        </c:if>
                                    </td>
                                </c:otherwise>
                            </c:choose>

                            <td>${each.order_id}</td>

                            <c:choose>
                                <c:when test="${each.IS_DEBIT eq 'true'}">
                                    <td></td>
                                    <td><span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span>${each.txn_amount}</td>
                                </c:when>
                                <c:otherwise>
                                    <td><span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span>${each.txn_amount}</td>
                                    <td></td>
                                </c:otherwise>
                            </c:choose>

                            <td><span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span> ${each.NEW_BALANCE}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</div>

<script type="text/javascript">

    function handleAddCash(){
    	return $("#addCashForm").validationEngine('validate');
    }
    
    function testWalletLimit(topUpAmount) {
    	url = "/app/getcashlimit/" + topUpAmount;
    	limitFailure = false;
    	failureMessage = "";
    	
        $.ajax({
            url: url,
            type: "GET",
            async: false,
            dataType: "JSON",
            cache: false,
            timeout:1800000,
            success: function (responseText) {
               if(responseText.limitStatus == 'fail')	{
            	   limitFailure = true;
            	   failureMessage = responseText.limitFailReason; 
               }
            },
            error:function (xhr,status,error){
            	 // do nothing. limit will be handled in the controller again.
            }
        });
        
        if (limitFailure) {
        	return failureMessage;
        }
    }
    
    function walletAddAmountTest(field, rules, i, options) {
        topUpAmount = field.val();
    	return testWalletLimit(topUpAmount);
    }
    
	$(document).ready(function(){
        //for balance_schemes
		$('.schemes_tooltip').each(function() {
			var selector = '#' + $(this).data('tooltip-id');
			Tipped.create(this, $(selector)[0], {
				skin : 'white',
				maxWidth: 400
			});
		}); 
    });
	
	$(document).ready(function(){
		$(".balance_scheme").contentSlider({expand : ".balance_scheme_form"});
	});
	
//contentSlider Plugin 
(function($){
	$.fn.extend({
		contentSlider:function(options){
			var defaults = {
						closeButton : ".closeLink",
						expand : ".expand",
						activeClass : "opened"
				};
			var options = $.extend(defaults, options);
			return this.each(function() {
					var o = options;
					var obj = $(this);
					var closeButton = o.closeButton;
					obj.bind("click",function(){
							obj.find(o.expand).show();
							obj.addClass(o.activeClass).siblings().removeClass(o.activeClass).find(o.expand).hide();
						})
					obj.find(closeButton).click(function(){
							obj.removeClass(o.activeClass).find(o.expand).hide();
							return false;	
						})	
			  });	
			}
	});
})(jQuery);

function loadBalanceActivity(caller){
		// Hi vipin u can put ur ajax call also here if required to load BalanceActivity
		$("#balanceActivityTable").show();
		$(caller).remove();
	}
 
</script>