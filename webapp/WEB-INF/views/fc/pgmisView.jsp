<%@ include file="/WEB-INF/includes/taglibs.jsp"%>

<%@page import="com.freecharge.payment.dao.PgMISRequestDataDAO"%>
<%@page import="com.freecharge.payment.dao.PgMISRequestDataDAO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MIS Service</title>
<style>

.error {
color: #ff0000;
font-style: italic;
}

.txt1 {
	   border-color: #AABBFF;
	   border: 1px solid; 
	   font: bold 84% 'trebuchet ms',helvetica,sans-serif; 
}

input.btn { 
    color: #0000FF;
    font: 95% ;
    padding: 3px 20px;
} 	

.theader{
	bgcolor="FFCCFF";
}
li.leftNavLink{
	list-style-type: none;
	padding:2px;
}
ul.leftNavPanel{
	margin:0px;
	paddding-left:20px;
}
th{
	background-color: gray;
	color: white;
	font-weight: bold;
	padding: 5px;
	text-align: left;
}
td{
	padding-left:6px;
	padding-right:10px;
	padding-top:3px;
	padding-bottom:3px;
	
}
td.entityID{
	width:30px;
	text-align: right;
}
tr.color0{
	background-color: #F0F0F0;
}
tr.color1{
	background-color: #D0D0D0;
}
body {
	font-family: verdana;
	font-size: 10px;
	color: #444444;
	
}
div.leftMenu{
	padding-right:60px;
	float:left;
}
div.displayList{
	float:left;
	width:60%;
}
p.addLink{
	padding-left:10px;
	float:left;
}
input.txt, select.txt{
padding:0px;
font-size: 10px;
width:100px;
}

p.addHeader{
	font-size:15px;
	 
}
div.header{
       background-color: #AAAAFF;
    color: #5555FF;
    font-family: Trebuchet MS;
    font-size: 35px;
    font-weight: bold;
    padding: 20px;
    text-align: center;

 
}
.floatRight{
	float:right;
}
div.searchCriteria{
    border: thin solid #AAAAFF;
    left: 5%;
    margin: 5px;
    position: relative;
    width: 90%;
	
}
</style>

			<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css?v=${mt:keyValue("version.no")}" type="text/css" media="all" />
			<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css?v=${mt:keyValue("version.no")}" type="text/css" media="all" />
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>
			<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>
			<script src="http://jquery-ui.googlecode.com/svn/tags/latest/external/jquery.bgiframe-2.1.2.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>
			

<script>
	$(function() {
		$( "#startDate" ).datepicker({dateFormat: 'yy/mm/dd'});
	});
	$(function() {
		$( "#endDate" ).datepicker({dateFormat: 'yy/mm/dd'});
	});
	function validateForm()
	{
	var x=document.forms["myForm"]["txnid"].value;
	if (isNaN(x))
	  {
		document.getElementById("msg").innerHTML='<font color="red">Please enter TXN ID as number</font>';
		return false;
	  }
	};
	
</script>


</head>

<body>
<div class="header"><spring:message code="payment.gateway.mis.service" text="#payment.gateway.mis.service#"/></div>
 <%
 	 int pageCount=0; 
     if(session.getAttribute("misRequest") != null){
    	 PgMISRequestDataDAO misRequest = (PgMISRequestDataDAO)session.getAttribute("misRequest");
    	 pageCount = misRequest.getPageNo();
    	/*  pageCount=pageCount+1; */
    	 misRequest.setPageNo(pageCount);
    	 session.setAttribute("misRequest", misRequest);
     }
 %>
 <div id="msg"></div>
<div>
<c:url var="addUserUrl" value="/admin/payment/searchRecords.htm" />
<form:form name="myForm" modelAttribute="misRequest" method="POST" action="${addUserUrl}"  onsubmit="javascript:validateForm()" >
	<div class="searchCriteria">
	<table align="center" width="100%" border="0">
	<tr>
		<td align="right" width="10"><spring:message code="start.date" text="#start.date#"/></td>
		<td align="left" width="10"><form:input id="startDate" path="startDate" class="txt"/></td>
		
		<td align="right" width="10"><spring:message code="start.time" text="#start.time#"/></td>
		<td align="left" width="10">
			<form:select path="startTime" cssClass="txt"  >
				<form:option value="0" selected="selected" label="0" />
				<form:option value="1" label="2" />
				<form:option value="2" label="3" />
				<form:option value="3" label="4" />
				<form:option value="4" label="5" />
				<form:option value="5" label="6" />
				<form:option value="6" label="7" />
				<form:option value="7" label="8" />
				<form:option value="8" label="9" />
				<form:option value="9" label="10" />
				<form:option value="10" label="11" />
				<form:option value="11" label="12" />
				<form:option value="12" label="13" />
				<form:option value="13" label="14" />
				<form:option value="14" label="15" />
				<form:option value="15" label="16" />
				<form:option value="16" label="17" />
				<form:option value="17" label="18" />
				<form:option value="18" label="19" />
				<form:option value="19" label="20" />
				<form:option value="20" label="21" />
				<form:option value="21" label="22" />
				<form:option value="22" label="23" />
				<form:option value="23" label="24" />
				
			</form:select>
		
		<td align="right" width="10"><spring:message code="pg.txn.id" text="#pg.txn.id#"/></td>
		<td align="left" width="10"><form:input path="txnID" id="txnid" cssClass="txt"  /></td>
		
		<td align="right" width="10"><spring:message code="payment.option" text="#payment.option#"/></td>
		<td align="left" width="10"><form:select path="paymentOption" cssClass="txt"  >
							<form:option value="" label="ALL" selected="true"/>
							<c:forEach items="${paymentTypeOptions}"  var="al">
								<form:option value="${al.code}" label="${al.code}"/>
							</c:forEach>
											
						</form:select></td>  
	</tr>
	
	<tr>
		<td align="right"><spring:message code="end.date" text="#end.date#"/></td>
		<td align="left"><form:input id="endDate" path="endDate" class="txt"/></td>
		
		<td align="right"><spring:message code="end.time" text="#end.time#"/></td>
		<td align="left">
			<form:select path="endTime" cssClass="txt"  >
				<form:option value="0" label="1" />
				<form:option value="1" label="2" />
				<form:option value="2" label="3" />
				<form:option value="3" label="4" />
				<form:option value="4" label="5" />
				<form:option value="5" label="6" />
				<form:option value="6" label="7" />
				<form:option value="7" label="8" />
				<form:option value="8" label="9" />
				<form:option value="9" label="10" />
				<form:option value="10" label="11" />
				<form:option value="11" label="12" />
				<form:option value="12" label="13" />
				<form:option value="13" label="14" />
				<form:option value="14" label="15" />
				<form:option value="15" label="16" />
				<form:option value="16" label="17" />
				<form:option value="17" label="18" />
				<form:option value="18" label="19" />
				<form:option value="19" label="20" />
				<form:option value="20" label="21" />
				<form:option value="21" label="22" />
				<form:option value="22" label="23" />
				<form:option value="23" selected="selected" label="24" />
				
			</form:select>
		<td align="right"><spring:message code="order.id.small" text="#order.id.small#"/></td>
		<td align="left"><form:input path="appTxnID" cssClass="txt"  /></td>
		<td align="right"><spring:message code="status" text="#status#"/></td>
		<td align="left">
			<form:select path="status" cssClass="txt"  >
				<form:option selected="true" value="" label="ALL" />
				<form:option value="success" label="Success" />
				<form:option value="failure" label="Failure" />
			</form:select>
	</tr>
	<tr>
		<td colspan="8" align="center"><input type="submit" value="Run Report" class="btn"/></td>
	</tr>
	</table>
	</div>
</form:form>
</div>
<div>
<table align="center" width="100%" border="0">			
			<tr>
				<th><spring:message code="fc.txn.id" text="#pg.txn.id#"/></th>
				<th><spring:message code="merchant.id" text="#merchant.id#"/></th>
				<th><spring:message code="request.receive" text="#request.receive#"/></th>
				<!--  <th>Mobile NO</th> -->
				<!-- <th>Operator</th> -->
				<th><spring:message code="payment.mode" text="#payment.mode#"/></th>
				<th><spring:message code="amount" text="#amount#"/></th>
				<th><spring:message code="order.id" text="#order.id#"/></th>
				<th><spring:message code="payment.gateway" text="#payment.gateway#"/></th>
				<th><spring:message code="payment.type" text="#payment.type#"/></th>
				<th><spring:message code="payment.option" text="#payment.option#"/></th>
				<th><spring:message code="payment.gateway.message" text="#payment.gateway.message#"/></th>
				<th><spring:message code="sent.pg" text="#sent.pg#"/></th>
				<th><spring:message code="received.pg" text="#received.pg#"/></th>
				<th><spring:message code="pg.txn.id" text="#pg.txn.id#"/></th>
				 <!-- <th>Request received Time</th> -->
				<th><spring:message code="txn.status" text="#txn.status#"/></th>
				<th><spring:message code="freefund.name" text="#freefund.name#"/></th>
				<th><spring:message code="freefund.price" text="#freefund.price#"/></th>
				 <!-- <th>Request received Time</th> -->
				<th><spring:message code="freefund.code" text="#freefund.code#"/></th>
				<th><spring:message code="cross.name1" text="#cross.name1#"/></th>
				<th><spring:message code="cross.price1" text="#cross.price1#"/></th>
				<th><spring:message code="cross.name2" text="#cross.name2#"/></th>
				<th><spring:message code="cross.price2" text="#cross.price2#"/></th>
				<th><spring:message code="cross.name3" text="#cross.name3#"/></th>
				<th><spring:message code="cross.price3" text="#cross.price3#"/></th>
				<th><spring:message code="cross.name4" text="#cross.name4#"/></th>
				<th><spring:message code="cross.price4" text="#cross.price4#"/></th>

				<th><spring:message code="wallet.id" text="wallet.id"/></th>
				<th><spring:message code="user.id" text="user.id"/></th>
				<th><spring:message code="fund.source" text="fund.source"/></th>
				<th><spring:message code="ag.name" text="ag.name"/></th>
				<th><spring:message code="ag.reference" text="ag.reference"/></th>
				<th><spring:message code="ag.operator" text="ag.operator"/></th>
				<th><spring:message code="ag.circle" text="ag.circle"/></th>
				<th><spring:message code="ag.amount" text="ag.amount"/></th>
				<th><spring:message code="channel.id" text="channel.id"/></th>
			</tr>
			
			<%-- <c:forEach items="${misdata.displayRecords}"  var="misDefault"  varStatus="loopStatus"> --%>
			<c:forEach items="${pgmisData}"  var="misDefault"  varStatus="loopStatus">
				<c:set var="colorIndex">${loopStatus.index mod 2}</c:set>
				<tr class="color${colorIndex}">
					<c:choose>
					<c:when test="${misDefault.attemptOrder != null}">
						<td align="center">${misDefault.attemptOrder}_${misDefault.txnId}</td>
					</c:when>
					<c:otherwise>
						<td align="center">${misDefault.txnId}</td>
					</c:otherwise>
					</c:choose>
 					<td align="center">${misDefault.mtxnId}</td>
					<td align="center">${misDefault.requestCreatedOn}</td>
					<td align="center">${misDefault.paymentMode}</td>
					<td align="center">${misDefault.amount }</td>
					<td align="center">${misDefault.appTxnId }</td>
					<td align="center">${misDefault.paymentGateway }</td>
					<td align="center">${misDefault.paymentType }</td>
					<td align="center">${misDefault.paymentOption }</td>
					<td align="center">${misDefault.paymentMessage}</td>
					<td align="center">${misDefault.sendToPgDate }</td>
					<td align="center">${misDefault.receivedFromPgdate }</td>
					<td align="center">${misDefault.gatewayTransactionID}</td>
					<td align="center">${misDefault.status}</td>
					<td align="center">${misDefault.freeFundName }</td>
					<td align="center">${misDefault.freeFundPrice}</td>
					<td align="center">${misDefault.freeFundCode}</td>
					<c:forEach items="${misDefault.crossSells}"  var="cs"  varStatus="loopVarStatus">
						<td align="center">${cs.crossSellName}</td>
						<td align="center">${cs.amount}</td>
					</c:forEach>
					<c:if test="${fn:length(misDefault.crossSells) < 4}">
						<c:forEach var="i" begin="${fn:length(misDefault.crossSells)}" end="3" step="1" varStatus ="status">
							<td align="center">&nbsp;</td>
							<td align="center">&nbsp;</td>
						</c:forEach>
					</c:if>
					
					<td align="center">${misDefault.walletId}</td>
					<td align="center">${misDefault.userId}</td>
					<td align="center">${misDefault.fundSource}</td>
					<td align="center">${misDefault.aggregatorName}</td>
					<td align="center">${misDefault.aggregatorReference}</td>
					<td align="center">${misDefault.operator}</td>
					<td align="center">${misDefault.circle}</td>
					<td align="center">${misDefault.agAmount}</td>
					<td align="center">${misDefault.channelId}</td>
				</tr>	
			</c:forEach>
<tr>
<td><a href="searchRecords.htm?iPageNo=<%=pageCount-1 %>"><spring:message code="previous" text="#previous#"/></a></td><td><a href="searchRecords.htm?iPageNo=<%=pageCount+1 %>"><spring:message code="next" text="#next#"/></a></td>

</tr>
<tr>
<td colspan="2"></td>
</tr>
<tr>
<td colspan="2"><a href="/admin/payment/excelReport.htm"><spring:message code="export.csv" text="#export.csv#"/></a></td>
</tr>
</table>

</div>
</body>
</html>