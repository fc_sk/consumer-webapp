<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="mainContainer innerPageGrad"  >
                        <div class="pageWidth clearfix" >
                            <h5 class="pageHeader " >Thank you! <span class="softTxt" >Payment successful</span></h5>
                            <div class="successMsgWrapp curveBlockWhite">
                                <p>Recharge on <strong><c:out value="${rechargeInitiateWebDo.mobileNo}"/></strong> for <strong><span class="webRupee" >Rs.</span><c:out value="${rechargeInitiateWebDo.amount}"/></strong>  in process  <span class="progressBar cl" ><img src="images/progress_bar.gif"  alt="" width="208" height="6" ></span> </p>

                                <p class="lightClr">Your FREE PRINTED Coupons will be delivered at the following address in 5-7 days:<br>
                                <b> Ram Singh <br>
                                181, Shalimar Bagh, Cool Road, Vikhroli (W)<br>
                                Mumbai 400007 <br> </b>
                                </p>
                                <p  class="lightClr" >Your E-Coupons have immediately been delivered to the following email address: <br>
                                <a href="#"><b>ajay_singh@gmail.com </b></a></p>
                                <p> Thank You for choosing FreeCharge! </p>
                                <input type="button" class="cmnSmlBtn fLeft viewInvoiceBtn" value="View invoice" >
                                <form:form modelAttribute="txnHomePageWebDO" action="home.htm" method="get" name="edit">
                        <input type="hidden" name="lookupID" value="${rechargeInitiateWebDo.lookupID}"/>
                        <a href="javascript:submitForEdit('edit')" class="submitButtonLrg">Proceed to another Recharge &gt;</a>
                    </form:form>
                             </div><!-- /.successMsgWrapper -->

                             <h1 class="pageHeaderInner" >Explore FreeCharge</h1>

                            <div class="curveBlock smlCouponContainer fLeft" >
                            	<h5 class="blockTitleSml clearfix" ><span class="fLeft" >Free Coupons from our partners </span> <a href="#" class="fRight" >See all coupons</a></h5>
                                <div class="slider clearfix" >
                                	<ul class="mycarousel" >
                                    	<li>
                                        	<h6>Coffee &amp; beverages</h6>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix1")}/images/merchant_logos/barista.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix2")}/images/merchant_logos/mcd.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                        </li> <!-- /item -->
                                        <li>
                                        	<h6>Travel</h6>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix3")}/images/merchant_logos/indigo.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix4")}/images/merchant_logos/indigo.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                        </li> <!-- /item -->
                                        <li>
                                        	<h6>Coffee &amp; beverages</h6>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix1")}/images/merchant_logos/barista.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix2")}/images/merchant_logos/mcd.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                        </li> <!-- /item -->
                                        <li>
                                        	<h6>Travel</h6>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix3")}/images/merchant_logos/indigo.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix4")}/images/merchant_logos/indigo.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                        </li> <!-- /item -->
                                        <li>
                                        	<h6>Coffee &amp; beverages</h6>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix1")}/images/merchant_logos/barista.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix2")}/images/merchant_logos/mcd.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                        </li> <!-- /item -->
                                        <li>
                                        	<h6>Travel</h6>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix3")}/images/merchant_logos/indigo.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                            <div class="couponSmall" >
                                            	<span class="couponImg" ><img src="${mt:keyValue("imgprefix4")}/images/merchant_logos/indigo.gif" alt="" width="68" height="50" ></span>
                                                <p><span class="webRupee" >Rs.</span> 50</p>
                                            </div>
                                        </li> <!-- /item -->
                                    </ul> <!-- /.mycarousel -->
                                </div> <!-- /.slider -->
                            </div> <!-- /.curveBlock -->

                            <div class="curveBlock doubleOfferContainer fRight" >
                            	<h5 class="blockTitleSml" > 2x Coupon </h5>
                                <div class="smlSlider smlCarousel clearfix" >
                                	<ul class="slides ">
                                    	<li><img src="${mt:keyValue("imgprefix1")}/images/slides/masterCard.gif" width="184" height="96" alt="" ></li>
                                        <li><img src="${mt:keyValue("imgprefix2")}/images/slides/masterCard.gif" width="184" height="96"  alt="" ></li>
                                        <li><img src="${mt:keyValue("imgprefix3")}/images/slides/masterCard.gif" width="184" height="96" alt=""  ></li>
                                    </ul>

                                    <div class="sliderPagination cl">
                                      <a href="#">1</a>
                                      <a href="#">2</a>
                                      <a href="#">3</a>
                                    </div>
                                </div>  <!-- /.smlSlider -->
                            </div> <!-- /.curveBlock -->


                        </div>
                    </div>
                    <!-- .mainContainer Ends  -->
