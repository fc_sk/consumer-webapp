<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isErrorPage="true"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>         
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

    <script>
    $(document).ready(function(){
        if('${status}' == 'ExpairedLink'){
            $('#expairedlinkid').show();
        }
        jQuery("#fgPasswordrecovery").validationEngine({
            ajaxFormValidation: false ,
              scroll:false
           });
    });

    function passwordRecoverySubmit(){
        var valid = $("#fgPasswordrecovery").validationEngine('validate');
        if(valid){
            var newPassword = $('#newPassword').val();
            if(newPassword == undefined){
                newPassword = '';
            }
            var confirmPassword = $('#confirmPassword').val();
            if(confirmPassword == undefined){
                confirmPassword = '';
            }
            var url='/app/passwordrecovery.htm';
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    'email': '<c:out value="${passwordRecovery.encryptEmail}"/>',
                    'userId': '<c:out value="${passwordRecovery.userId}"/>',
                    'encryptValue': '<c:out value="${passwordRecovery.encryptValue}"/>',
                    'newPassword': newPassword,
                    'confirmPassword': confirmPassword
                },
                dataType: "json",
                cache: false,
                timeout:1800000,
                success: function (responseText) {

                    var status = responseText.status;
                    if(status == 'success'){
                        $('#fgPasswordrecovery').html('');
                        //we have to handle expired link message
                        if(responseText.changePasswordSuccessMsg == 'expairedLink'){
                            //$('#changePasswordSuccessMsg')
                            $('#expairedlinkid').show();
                            return;
                        }
                        alert(responseText.changePasswordSuccessMsg);
                        window.location = "http://freecharge.in/";
                    }else if(status == 'exception'){
                        $('#changePasswordErrorMsg').html(responseText.error);
                        //$('#changePasswordSuccessMsg').show();
                    }else if(status == 'required'){
                        $('#changePasswordErrorMsg').html(responseText.changePasswordSuccessMsg);
                        //$('#changePasswordSuccessMsg').show();
                    }else{

                    }
                },
                error:function (e) {

                }
            });
        }
    }
    </script>

    <div class="container" id="passwordRecoveryMainContainer">
        <div class="sixteen columns" >
            <c:if test="${status eq 'ValidLink'}">
                <h1>Reset password</h1>
                <div class="change-password-block">
                    <p class="successMsgSmall" id="changePasswordSuccessMsg" style="display: none;"><c:out value="${changePasswordSuccessMsg}"/></p>

                    <form:form name="fgPassword" id="fgPasswordrecovery"  method="post"  modelAttribute="passwordRecovery"   action="javascript:void(0);" >
                        <div class="row">
                            <label>Email</label>
                            <span><c:out value="${passwordRecovery.email}"/></span>
                        </div>
                        <div class="row">
                            <label>Reset password</label>
                            <form:password id="newPassword"  path="newPassword"  cssClass="validate[required,minSize[6],maxSize[20]] text-input" maxlength="20"/>
                        </div>
                        <div class="row">
                            <label>Confirm password</label>
                            <form:password id="confirmPassword"  path="confirmPassword"  cssClass="validate[required,equals[newPassword],minSize[6],maxSize[20]] text-input" maxlength="20"/>
                        </div>

                        <p id="changePasswordErrorMsg" style="color: red;width: 100%;float: left;text-align: center;"></p>
                        <div class="row">
                            <label></label>
                            <input type="Reset" class="button"  value="Clear" />
                            <input type="submit" class="button buttonThree" value="Update" onclick="passwordRecoverySubmit();">
                        </div>
                    </form:form>
                </div>
            </c:if>

            <div class="errorMsgWrapp  " id="expairedlinkid" style="display: none;">
                <img src="${mt:keyValue("imgprefix1")}/images/oops.png" alt="Oops! This link has expired" title="Oops! This link has expired" width="130" height="130" >
                <h5 class="pageHeader " >We're sorry!!!</h5>
                <p>The link you are trying to access has expired.</p>
                <p> You may go back to <a href="${mt:keyValue("hostprefix")}">homepage</a>.</p>
            </div><!-- /.successMsgWrapper -->

            <c:if test="${status eq 'InvalidLink'}">
                <div class="errorMsgWrapp  ">
                    <img src="${mt:keyValue("imgprefix1")}/images/oops.png" alt="Oops! Invalid Link" title="Oops! Invalid Link" width="130" height="130" >
                    <h5 class="pageHeader " >We're sorry!!!</h5>
                    <p>The link you are trying to access is invalid.</p>
                    <p> You may go back to <a href="${mt:keyValue("hostprefix")}">homepage</a>.</p>
                </div><!-- /.successMsgWrapper -->

            </c:if>

        </div>
    </div>


