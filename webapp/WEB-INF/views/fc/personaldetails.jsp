<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeContextDirectory" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
    FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    if (fs != null) {
        Map<String, Object> sessionData = fs.getSessionData();
        if (sessionData != null) {
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
        }
    }
    String lookId = request.getParameter("lid");
    String prodType = FCSessionUtil.getRechargeType(lookId, fs);
    Integer prodId = FCConstants.productMasterMapReverse.get(prodType);
%>
<c:set value="<%=login%>" var="login"/>

<c:set var="cartItems" value="${cart.itemsList}" scope="request"/>
<c:set var="cartId" value="${cart.id}" scope="request"/>
<c:set var="primaryProductType" value='<%=FCConstants.PRIMARY_PRODUCT_TYPE%>' scope="application"/>
<c:set var="secondaryProductType" value='<%=FCConstants.SECONDARY_PRODUCT_TYPE%>' scope="application"/>
<c:set var="chargesProductType" value='<%=FCConstants.CHARGES_PRODUCT_TYPE%>' scope="application"/>
<c:set var="crosssellEntityName" value='<%=FCConstants.ENTITY_NAME_FOR_CROSSSELL%>' scope="application"/>
<c:set var="couponEntityName" value='<%=FCConstants.COUPON_ENTITY_ID%>' scope="application"/>
<c:set var="couponCounter" value='0'/>
<c:set var="crossSellCounter" value='0'/>
<c:set var="totalCouponPrice" value='0'/>
<c:set var="totalCrossSellPrice" value='0'/>
<c:set var="charges" value='0'/>
<c:set var="lid" value='<%=request.getParameter(FCConstants.LOOKUPID)%>' />

<c:set var="lookupID" value="<%=request.getParameter(\"lookupID\")%>" scope="request" />
<c:set var="walletDisplayName" value="<%=FCConstants.WALLET_DISPLAY_NAME%>"></c:set>

<%
    FreechargeSession freechargesession = FreechargeContextDirectory.get().getFreechargeSession();
    String lookupid = request.getParameter("lookupID");
    String lid = request.getParameter("lid");
    if (lookupid == null && lid != null) {
        lookupid = lid;
    }
    String productType = FCSessionUtil.getRechargeType(lookupid, freechargesession);
    Integer productId = FCConstants.productMasterMapReverse.get(productType);
%>
<input type="hidden" id="productType" name="productType" value="<%=productType%>"/>
<script type="text/javascript">
    var jsPrefix = '${mt:keyValue("jsprefix1")}';
    var version = '${mt:keyValue("version.no")}';
    var imgPrefix = '${mt:keyValue("imgprefix1")}/images';
</script>
<div class="twelve columns">
    <%--recharge info--%>
    <%--<jsp:include page="payment-recharge-details.jsp" />--%>


        <%-- Contact details page --%>
        <c:if test = "${contactDetailsReq}">
        <%@include file="payment-contactdetails.jsp"%>
        </c:if>

	<div class="upSell fc-unit row">
		<div class="upSell-header">
			<h2 class="bg-header">
				Promo Code<span class="icon-info" id="promo-tip"></span>
			</h2>
		</div>
		<div class="layer upSell-block">
			<ul class="clearfix">
				<li>
					<div class="unit clearfix">
						<c:choose>
							<c:when test="${isPromocodeEnabled}">
									<jsp:include page="payment-promocode.jsp">
									<jsp:param value="<%=productType%>" name="productType" />
									<jsp:param value="<%=lookupid%>" name="lookupID" />
								</jsp:include>
						  
						   
							</c:when>
							<c:otherwise>
                   			Promocode redemption down for maintenance. Please check later.
                   		</c:otherwise>
						</c:choose>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<%-- Cash back campaign --%>
        <%@include file="cashBackCampaign.jsp"%>


    <%-- Crossell details page --%>
    <%@include file="payment-crosssell.jsp"%>


</div>
<div class="four columns">
    <div class="sidebar">
        <%-- order summary page --%>
        <div id="payment-summary-holder"><jsp:include page="payment-summary.jsp"></jsp:include></div>

        <div class="page-action row">
            <c:choose>
                <c:when test="${totalPayAmount eq 0.0}">
                    <div id="checkoutButton" style="display: none;" >
                        <button id="checkoutContinue" type="button" onclick="_gaq.push(['_trackEvent', 'Button', 'Checkout']);checkout();" class="button buttonThree full-width">Continue &rarr;</button>
                    </div>
                    <c:choose>
                        <c:when test="${showFBConnect}">
                            <div id="zeropayButton" >
                                <p>Authenticate using facebook to redeem your promocode.</p>
                                <a href="#" id="fb-connect"><img src="${mt:keyValue("imgprefix1")}/images/social/fb-connect.png"></a>
                                <script type="text/javascript">
                                    $.getScript(jsPrefix+"/fbcheck.js?v="+version, function(){
                                        loadFacebookCheck(jsPrefix, version, imgPrefix);
                                    });
                                </script>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div id="zeropayButton" >
                                <jsp:include page="payment-options.jsp"></jsp:include>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </c:when>
                <c:otherwise>
                    <div id="checkoutButton" >
                        <button type="button" onclick="_gaq.push(['_trackEvent', 'Button', 'Checkout']);checkout();" class="button buttonThree full-width"><c:choose><c:when test='${isExperimentEnabled}'>Proceed To Pay</c:when><c:otherwise>Continue</c:otherwise></c:choose> &rarr;</button>
                    </div>
                    <div id="zeropayButton" style="display: none;"  >
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>

<%-- Payment options page --%>
<%-- <%@include file="payment-options.jsp"%> --%>

<input type="hidden" value="2" name="pagecount" id="pagecount"/>
<script type="text/javascript">
var crossSellCounter = parseInt("${crossSellCounter}");
var totalCrossSellPrice = parseInt("${totalCrossSellPrice}");

function checkout() {
	<c:if test = "${contactDetailsReq}">
        if(!$.trim($("#confirm-title").text()).length  || !$.trim($("#confirm-name").text()).length || !$.trim($("#confirm-address").text()).length) {
            $("#editaddr-link a").click();
            return false;
        }
	</c:if>
	document.location.href = '/app/checkout.htm?lid=<%=lookupid%>';
}

function refreshCheckoutButton() {
	var url='/app/total_amount_to_pay.htm?lid=<%=lookupid%>';
	$.ajax({
	    url: url,
	    dataType: "json",
	    cache: false,
	    timeout:1800000,
	    success: function (responseText) {
            if (responseText.status == 'success') {
                //var responseObject = responseText.data;
    	    	$(".total-amount-to-pay").html(responseText.payableTotalAmount);
                if(responseText.showFBConnect){
                    $("#zeropayButton").show();
                    $("#checkoutButton").hide();
                    $("#zeropayButton").html('<p>Authenticate using facebook to redeem your promocode.</p>' +
                            '<a href="#" id="fb-connect"><img src="'+imgPrefix+'/social/fb-connect.png"></a>');
                    $.getScript(jsPrefix+"/fbcheck.js?v="+version, function(){
                        loadFacebookCheck(jsPrefix, version, imgPrefix);
                    })
                }else {
                    if($.trim(responseText.payableTotalAmount) == '0' ||$.trim(responseText.payableTotalAmount) == '0.0' || $.trim(responseText.payableTotalAmount) == '0.00') {
                        $("#zeropayButton").show();
                        $("#checkoutButton").hide();
                        getAjaxPaymentOptions();
                    } else {
                        $("#zeropayButton").html('').hide();
                        $("#checkoutButton").show();
                    }
                }
            } else if (responseText.status == 'sessionExpired') {
                alert('Session expired. Please start over');
            } else {
                alert('Technical error occurred. Please try again after some time.');
            }
	    },
	    error:function (xhr,status,error){
	      $(".total-amount-to-pay").html("Error occurred "); 
	    }
	});
}

function getAjaxPaymentOptions() {
	var url='/app/getajaxpaymentoptions.htm?lid=<%=lookupid%>';
	$.ajax({
	    url: url,
	    dataType: "html",
	    cache: false,
	    timeout:1800000,
	    success: function (responseText) {
	    	$("#zeropayButton").html(responseText); 
	    },
	    error:function (xhr,status,error){
	      $("#zeropayButton").html("Error occurred "); 
	    }
	});
}

</script>


