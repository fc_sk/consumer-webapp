<%@page import="com.freecharge.recharge.businessdao.MisRequestData" %>
<%@ include file="/WEB-INF/includes/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MIS Service</title>
<style>

.error {
color: #ff0000;
font-style: italic;
}

.txt1 {
	   border-color: #AABBFF;
	   border: 1px solid; 
	   font: bold 84% 'trebuchet ms',helvetica,sans-serif; 
}

input.btn { 
    color: #0000FF;
    font: 95% ;
    padding: 3px 20px;
} 	

.theader{
	bgcolor="FFCCFF";
}
li.leftNavLink{
	list-style-type: none;
	padding:2px;
}
ul.leftNavPanel{
	margin:0px;
	paddding-left:20px;
}
th{
	background-color: gray;
	color: white;
	font-weight: bold;
	padding: 5px;
	text-align: left;
}
td{
	padding-left:6px;
	padding-right:10px;
	padding-top:3px;
	padding-bottom:3px;
	
}
td.entityID{
	width:30px;
	text-align: right;
}
tr.color0{
	background-color: #F0F0F0;
}
tr.color1{
	background-color: #D0D0D0;
}
body {
	font-family: verdana;
	font-size: 10px;
	color: #444444;
	
}
div.leftMenu{
	padding-right:60px;
	float:left;
}
div.displayList{
	float:left;
	width:60%;
}
p.addLink{
	padding-left:10px;
	float:left;
}
input.txt, select.txt{
padding:0px;
font-size: 10px;
width:100px;
}

p.addHeader{
	font-size:15px;
	 
}
div.header{
       background-color: #AAAAFF;
    color: #5555FF;
    font-family: Trebuchet MS;
    font-size: 35px;
    font-weight: bold;
    padding: 20px;
    text-align: center;

 
}
.floatRight{
	float:right;
}
div.searchCriteria{
    border: thin solid #AAAAFF;
    left: 5%;
    margin: 5px;
    position: relative;
    width: 90%;
	
}
</style>
			<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css?v=${mt:keyValue("version.no")}" type="text/css" media="all" />
			<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css?v=${mt:keyValue("version.no")}" type="text/css" media="all" />
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>
			<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>
			<script src="http://jquery-ui.googlecode.com/svn/tags/latest/external/jquery.bgiframe-2.1.2.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>
			

<script>
	$(function() {
		$( "#startDate" ).datepicker({dateFormat: 'yy/mm/dd'});
	});
	$(function() {
		$( "#endDate" ).datepicker({dateFormat: 'yy/mm/dd'});
	});
	function validateForm()
	{
	var x=document.forms["myForm"]["txnid"].value;
	if (isNaN(x))
	  {
		document.getElementById("msg").innerHTML='<font color="red">Please enter TXN ID as number</font>';
		return false;
	  }
	};
	
</script>



</head>

<body>
<div class="header"><spring:message code="mis.service" text="#mis.service#"/></div>
 <%
 	 int pageCount=0; 
     if(session.getAttribute("misRequest") != null){
    	 MisRequestData misRequest = (MisRequestData)session.getAttribute("misRequest");
    	 pageCount = misRequest.getPageNo();
    	 /* pageCount=pageCount+1; */
    	 misRequest.setPageNo(pageCount);
    	 session.setAttribute("misRequest", misRequest);
     }
 %>
 <div id="msg"></div>
<div>
<c:url var="addUserUrl" value="/admin/recharge/searchRecords.htm" />
<form:form name="myForm" modelAttribute="misRequest" method="POST" action="${addUserUrl}"  onsubmit="return validateForm()" >
	<div class="searchCriteria">
	<table align="center" width="90%" border="0">
	<tr>
	<td align="right" width="7%"><spring:message code="start.date" text="#start.date#"/></td>
	<td align="left"><form:input id="startDate" path="startDate" class="txt"/></td>
	<td align="right"><spring:message code="mobile.no" text="#mobile.no#"/></td>
	<td align="left"><form:input path="mobileNo" cssClass="txt"  /></td>
	<td align="right"><spring:message code="in.txn.id" text="#in.txn.id#"/></td>
	<td align="left"><form:input path="txnID" id="txnid" cssClass="txt"  /></td>
	 <td align="right"><spring:message code="operator" text="#operator#"/></td>
	 <td align="left"><form:select path="operator" cssClass="txt"  >
						<form:option value="" label="ALL" selected="true"/>
						<c:forEach items="${operatorMasters}"  var="al">
							<form:option value="${al.operatorCode}" label="${al.operatorCode}"/>
						</c:forEach>
										
					</form:select></td>
<%--	<td align="right">Circle</td>
	<td align="left"><form:select path="circle" cssClass="txt"  >
						<form:option value="" label="ALL" selected="true"/>
						<c:forEach items="${misdata.serviceInstanceList}"  var="al">
							<form:option value="${al.serviceInstantID}" label="${al.circleID}"/>
						</c:forEach>
										
					</form:select></td>  --%>
	<td align="right"><spring:message code="aggregator.name" text="#aggregator.name#"/></td>
	<td align="left"><form:select path="gateway" cssClass="txt"  >
						<form:option value="" label="ALL" selected="true"/>
						<c:forEach items="${inAggrOprCircleMaps}"  var="al">
							<form:option value="${al}" label="${al}"/>
						</c:forEach>
										
					</form:select></td> 
	</tr>
	
	<tr>
	<td align="right"><spring:message code="end.date" text="#end.date#"/></td>
	<td align="left"><form:input id="endDate" path="endDate" class="txt"/></td>
	<%-- <td align="right">App Name</td>
	<td align="left"><form:select path="appName" cssClass="txt"  >
						<form:option selected="true" value="" label="ALL" />
						<c:forEach items="${misdata.clientList}"  var="al">
							<form:option value="${al.clientID}" label="${al.clientName}"/>
						</c:forEach>
										
					</form:select></td> --%>
	<td align="right"><spring:message code="order.id" text="#order.id#"/></td>
	<td align="left"><form:input path="appTxnID" cssClass="txt"  /></td>
	<td align="right"><spring:message code="status" text="#status#"/></td>
	<td align="left">
		<form:select path="status" cssClass="txt"  >
			<form:option selected="true" value="" label="ALL" />
			<form:option value="success" label="Success" />
			<form:option value="failure" label="Failure" />
		</form:select>
	<td colspan ="4"></td>
	</tr>
	<br/>
	<tr>
	<td colspan="12" align="center"><input type="submit" value="Run Report" class="btn"/></td>
	</tr>
	</table>
	</div>
</form:form>
</div>
<div>
<table align="center" width="100%" border="0">			
			<tr>
				<th><spring:message code="in.txn.id" text="#in.txn.id#"/></th>
				<th><spring:message code="client.name" text="#client.name#"/></th>
				<th><spring:message code="mobile.no" text="#mobile.no#"/></th>
				<th><spring:message code="product.type" text="#product.type#"/></th>
				<th><spring:message code="operator" text="#operator#"/></th>
				<th><spring:message code="circle" text="#circle#"/></th>
				<th><spring:message code="amount" text="#amount#"/></th>
				<th><spring:message code="order.id" text="#order.id#"/></th>
				<th><spring:message code="in.resp.code" text="#in.resp.code#"/></th>
				<th><spring:message code="ag.resp.code" text="#ag.resp.code#"/></th>
				<th><spring:message code="msg" text="#msg#"/></th>
				<th><spring:message code="ag.refno" text="#ag.refno#"/></th>
				<th><spring:message code="operator.refno" text="#operator.refno#"/></th>
				<th><spring:message code="date.time" text="#date.time#"/></th>
				<th><spring:message code="aggregator" text="#aggregator#"/></th>
				 <th><spring:message code="recharge.type" text="#recharge.type#"/></th>
				<th><spring:message code="txn.status" text="#txn.status#"/></th>
			</tr>
			
			<%-- <c:forEach items="${misdata.displayRecords}"  var="misDefault"  varStatus="loopStatus"> --%>
			<c:forEach items="${misData}"  var="misDefault" varStatus="loopStatus">
				<c:set var="colorIndex">${loopStatus.index mod 2}</c:set>
				<tr class="color${colorIndex}">
					<c:choose>
					<c:when test="${misDefault.attemptOrder != null}">
						<td align="center">${misDefault.attemptOrder}_${misDefault.txnId}</td>
					</c:when>
					<c:otherwise>
						<td align="center">${misDefault.txnId}</td>
					</c:otherwise>
					</c:choose>
					<td align="center">Freecharge</td>
					<td align="center">${misDefault.mobileNo }</td>
					<td align="center">${misDefault.productType }</td>
					<td align="center">${misDefault.operator }</td>
					<td align="center">${misDefault.circle}</td>
					<td align="center">${misDefault.amount }</td>
					<td align="center">${misDefault.appTxnId }</td>
					<td align="center">${misDefault.respCode }</td>
					<td align="center">${misDefault.gatewayRespCode }</td>
					<td align="center">${misDefault.respMessage }</td>
					<td align="center">${misDefault.gatewayRefNo}</td>
					<td align="center">${misDefault.operatorRefNo }</td>
					<td align="center">${misDefault.date }</td>
					<td align="center">${misDefault.gateway}</td>
					<td align="center">${misDefault.options }</td>
					<td align="center">${misDefault.status}</td>
				</tr>	
			</c:forEach>
<tr>
<td><a href="searchRecords.htm?iPageNo=<%=pageCount-1 %>"><spring:message code="previous" text="#previous#"/></a></td><td><a href="searchRecords.htm?iPageNo=<%=pageCount+1 %>"><spring:message code="next" text="#next#"/></a></td>

</tr>
<tr>
<td colspan="2"></td>
</tr>
<tr>
<td colspan="2"><a href="/admin/recharge/excelReport.htm"><spring:message code="export.excel" text="#export.excel#"/></a></td>
</tr>
</table>

</div>
</body>
</html>