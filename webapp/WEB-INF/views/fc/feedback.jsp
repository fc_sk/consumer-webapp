<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row" >
	<div class="four columns">
    	<div class="staticpage-navigation">
            <ul>
                <li><a href="/app/aboutus.htm" >About us</a></li>
                <li><a href="/app/contactus.htm"  >Contact us</a></li>
                <li><a href="/app/faq.htm">FAQ</a></li>
                <li><a href="/app/sitemap.htm">Sitemap</a></li>
                <li><a href="/app/feedback.htm" class="active" >Feedback</a></li>
                <li><a href="http://support.freecharge.in" target="_blank" >Customer support</a></li>
                <li><a href="/app/termsandconditions.htm">Terms &amp; conditions</a></li>
                <li><a href="/app/privacypolicy.htm">Privacy policy</a></li>
            </ul>
        </div> <%-- /.staticpage-navigation --%>
    </div>
    
    <div class="twelve columns">
    	<div class="staticpage-content">
            <h1>Feedback</h1>
            <form:form modelAttribute="feedbackWebDO" name="feedbackform" action="#" method="POST" cssClass="form-feedback" id="feedbackform" >	
                    <label>Email</label>
                    <input maxlength="40" type="text" class="input1 softNormText" name="feedbackEmailId" id="feedbackEmailId" />
                    <label> *Topic Area</label>
                    <select class="validate[required] select1 softNormText" name="feedbackTopicArea" id="feedbackTopicArea">
                        <option value="" selected >Choose one</option>
                        <option value="Content" >Content</option>
                        <option value="Design" >Design</option>
                        <option value="Usability" >Usability</option>
                        <option value="Support Service" >Support Service</option>
                        <option value="Other" >Other</option>
                    </select>
                    <label> *Subject</label>
                    <input maxlength="40" type="text" class="validate[required] input1 softNormText"  name="feedbackSubject" id="feedbackSubject">
                    <label> *Your Message</label>
                    <textarea name="feedbackMessage" id="feedbackMessage" cols="1" rows="5" class="validate[required] input7 softNormText"></textarea>
                    <label><input  type="button" class="regularButton" value="Submit" onclick="sendFeedback()"></label>
                    <div id="feedbackStatus" ></div>
           </form:form>  <%--/frm_feedback --%>
        </div> <%-- /.staticpage-content --%>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	jQuery("#feedbackform").validationEngine({
	    ajaxFormValidation: false ,
	      scroll:false
	   });
});
function sendFeedback(){
	var flag = $('#feedbackform').validationEngine('validate');
	if(flag){
	var email =$('#feedbackEmailId').val();
	var topicArea = $('#feedbackTopicArea').val();
	var subject = $('#feedbackSubject').val();
	var message = $('#feedbackMessage').val();
	$.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    } }); 
	var url = '/app/feedback.htm';
	
	var formData = 'email='+email+'&topicArea='+topicArea+'&subject='+subject+'&message='+message+'';
	$.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: "json",
        cache: false,
        timeout:1800000,
        success: function (responseText) {
            
            if(responseText.status == 'success'){
            	document.feedbackform.reset();
                $('#feedbackStatus').show().html("<p class='message-small-success' >Feedback submitted successfully.</p>").delay(5000).fadeOut();
                $.unblockUI();
                
            }else if(responseText.status == 'fail'){
            	$('#feedbackStatus').show().html("<p class='message-small message-small-failure' >Technical error occurred. Please try again later.</p>").delay(5000).fadeOut();
                $.unblockUI();
            }else{
                $.unblockUI();
            }

        },
      error:function (e) {
    	  $('#feedbackStatus').show().html("<p class='message-small message-small-failure' >Technical error occured. Please try again later.</p>").delay(5000).fadeOut();
          $.unblockUI();

        }
  	}); 
	}
}
</script>