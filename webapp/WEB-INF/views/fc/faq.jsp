<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
        <nav>
            <ul>
                <li><a href="/app/aboutus.htm" >About us</a></li>
                <li><a href="/app/contactus.htm"  >Contact us</a></li>
                <li><a href="/app/faq.htm" class="active" >FAQ</a></li>
                <li><a href="/app/sitemap.htm">Sitemap</a></li>
                <%--<li><a href="/app/feedback.htm" >Feedback</a></li>--%>
                <li><a href="http://support.freecharge.in" target="_blank">Customer support</a></li>
                <li><a href="/app/termsandconditions.htm">Terms &amp; conditions</a></li>
                <!--<li><a href="/app/privacypolicy.htm">Privacy policy</a></li>
                 <li><a href="/security/disclosure.htm">Security Policy</a></li><br>-->
            </ul>
        </nav>
<div class="row">
    <div class="col-md-12">


        <section class="static-content">

            <h1>FAQ</h1>

            <ul id="cbp-ntaccordion" class="cbp-ntaccordion">
                <li>
                    <h3 class="cbp-nttrigger">RECHARGE RELATED QUERIES</h3>
                    <div class="cbp-ntcontent">
                        <ul class="cbp-ntsubaccordion">
                            <li>
                                <h4 class="cbp-nttrigger">What should I do if recharge is not done?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Wait for 5 minutes after the payment is successfully done. If your mobile number is still not recharged then go to link <a href="http://support.freecharge.in/" target="_blank" >support.freecharge.in</a> and register a complaint. We will respond to it as soon as possible.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Does the recharge happen immediately?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Yes. You get your recharge immediately. <br> As soon as payment is made, you will get a confirmation mail from FreeCharge.in and a recharge message from your mobile operator. Generally it takes less than 10 seconds for the transaction to complete.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Can I recharge for my friends and relatives also?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Absolutely. You can recharge for your loved ones using the same account. You just need to know the mobile number to make a successful recharge.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Can I recharge without selecting coupons?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Yes, you can recharge without selecting any coupons. <br>  Though, we would like you to reap maximum benefit of the coupons available and highly recommend to order coupons as well.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Is their any extra charge for recharging?</h4>
                                <div class="cbp-ntcontent">
                                    <p>No, there is absolutely no extra charge for recharging. You will get same talk time what your mobile service provider offers you. <br>
                                        We do not charge you anything extra for the recharge in the name of service charges, convenience charges or any other term.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <h3 class="cbp-nttrigger">COUPONS RELATED QUERIES</h3>
                    <div class="cbp-ntcontent">
                        <ul class="cbp-ntsubaccordion">
                            <li>
                                <h4 class="cbp-nttrigger">Am I charged extra for coupons?</h4>
                                <div class="cbp-ntcontent">
                                    <p>No,there is no charge for the coupons. Though, a handling charge of Rs.10 is applicable.<br>  You will not be charged anything apart from the recharge amount if you do not wish to order any coupons.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">What should I do if coupons are not been received?</h4>
                                <div class="cbp-ntcontent">
                                    <p>If you do not get, a) e-coupons instantly to your inbox after a successful transaction OR b) physical coupons within 7 business days, please go to the link <a href="http://support.freecharge.in/" target="_blank" >support.freecharge.in</a> and register your complaint.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How long does it take for coupons to reach me?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Your e-coupons are delivered instantly to your inbox via email after a successful transaction and your physical coupons are delivered at your doorstep via courier within 7 business days of your successful recharge.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How can I utilize coupons?</h4>
                                <div class="cbp-ntcontent">
                                    <p>You get e-coupons instantly after a successful transaction, which you can redeem at the respective merchant&#39;s website before expiry date. Your physical coupons get delivered within 7 business days of your successful recharge, which you can produce at the respective merchant&#39;s store before expiry date. T&amp;C apply as per respective merchant. See coupon details for more information.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Can I give these coupons to anybody?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Yes, these coupons are transferrable. You can give these coupons to your friends and relatives.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">What is the coupon handling charge?</h4>
                                <div class="cbp-ntcontent">
                                    <p>We charge Rs 10 per recharge as handling charge in case you have selected coupons during recharge.  Handling charge is to insure that coupons reach at your door step without fail.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">In how many days will i receive my coupons?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Your e-coupons are delivered instantly to your email after a successful transaction and your physical coupons are delivered at your doorstep within 7 business days of your successful recharge. If you do not receive them, please go to the link <a href="http://support.freecharge.in/" target="_blank" >support.freecharge.in</a> and register your complaint.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Can i order more than 1 quantity of same coupon?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Yes, you can do so by clicking ADD MORE button adjacent to the coupon.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Can i give the coupons to my friends?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Yes, you can give these coupons to any of your friends, relatives or colleagues. These coupons are transferable.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Can i order only coupons without recharging?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Our retail partners provide these exclusive coupons only when you recharge your mobile through FreeCharge.in. Hence, we can provide you coupons equivalent to recharge done through FreeCharge.in.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">I want to distribute coupons through you. what do i do?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Please feel free to contact us at <a href="mailto:info@freecharge.in" >info@freecharge.in</a></p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <h3 class="cbp-nttrigger">PAYMENT RELATED QUERIES</h3>
                    <div class="cbp-ntcontent">
                        <ul class="cbp-ntsubaccordion">
                            <li>
                                <h4 class="cbp-nttrigger">Is it safe to recharge online?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Absolutely. CC Avenue is our payment gateway partner and the same system is used by most Indian e-commerce portals. All the transactions are executed securely and no critical information regarding your payment is ever accessible by any employee or third party.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How can I make the payment?</h4>
                                <div class="cbp-ntcontent">
                                    <p>There are four modes of flexi-payments</p>
                                    <ul>
                                        <li>Debit/ATM card</li>
                                        <li>Internet Banking facility</li>
                                        <li>Credit Card</li>
                                        <li>Cash Cards</li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Can I use my debit/ATM card to make payment of recharge?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Yes, you can use your Debit/ATM card for most of the Indian Banks.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">What is CVV, CV2 and 3D security?</h4>
                                <div class="cbp-ntcontent">
                                    <p><em>CVV</em> or <em>CV2 code</em> is a 3 digit number mentioned at the back of your card. This is meant for security purpose. <br> The <span class="color-light">3D security code</span> is like a password for online transactions through Credit/Debit cards. You can check you bank&#39;s site for details.<br>   This is specially designed for the safe transaction. CVV no is the last 3 digits mentioned at the back side of your debit/credit card.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Contact details entered on payment gateway page</h4>
                                <div class="cbp-ntcontent">
                                    <p>Please enter your correct Mobile number and email on the payment gateway details section as this is the number email on which Refund details are sent by the Payment gateway in case a refund is done.</p>
                                </div>
                            </li>

                        </ul>
                    </div>
                </li>
                <li>
                    <h3 class="cbp-nttrigger">REGISTRATION RELATED QUERIES</h3>
                    <div class="cbp-ntcontent">
                        <ul class="cbp-ntsubaccordion">
                            <li>
                                <h4 class="cbp-nttrigger">How can I change my password?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Just Login and go to <strong>My Account</strong> tab. Go to <strong>Profile</strong> tab under My Account. Here you can change your password or any detail for that matter pertaining to your account. Don&#39;t forget to SAVE changes.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How can I edit my account details?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Jut Login and go to <strong>My Account</strong> tab. Go to <strong>Profile</strong> tab under My Account. Here you can edit your account details. Don&#39;t forget to SAVE changes.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">I forgot my password. What should I do?</h4>
                                <div class="cbp-ntcontent">
                                    <p>This is very simple. Just click on <strong>"Forgot password?"</strong> under Login Box right at the top of the website. This will ask you to provide your e-mail id registered with us. Your password will be e-mailed to you immediately.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How can I register?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Our registration process is very simple and short. Just click on  <strong>"Sign up"</strong> present on right top corner of home page. Enter your name, postal address for coupon delivery, e-mail id and password for FreeCharge account. Congratulations! You are registered.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Is it necessary to register with the site?</h4>
                                <div class="cbp-ntcontent">
                                    <p>This is just a one time exercise and absolutely free. Details saved during registration will be saved for the future transaction.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <h3 class="cbp-nttrigger">CREDITS FAQ</h3>
                    <div class="cbp-ntcontent">
                        <ul class="cbp-ntsubaccordion">
                            <li>
                                <h4 class="cbp-nttrigger">What are Credits?</h4>
                                <div class="cbp-ntcontent">
                                    <p>If for some reason, your recharge is unsuccessful, we make a refund to your FreeCharge Credits. Your money is absolutely safe, secure and available to you for future transactions on <a href="/app/home.htm" target="_blank"> FreeCharge.in</a>. What&#39;s more, you can also deposit cash in your Credits anytime to facilitate faster and easier recharging by clicking <a href="/app/mycredits.htm" target="_blank">here</a>.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How does money appear in my Credits?</h4>
                                <div class="cbp-ntcontent">
                                    <p>In case of an unsuccessful recharge where your payment has already gone through, we park your funds in your Credits. It&#39;s 100% safe and available for you to use anytime for future recharging.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">What can I use my Credits for?</h4>
                                <div class="cbp-ntcontent">
                                    <p>You can use it anytime for any transaction (Mobile/DTH/Data card) on <a href="http://www.freecharge.in" target="_blank" >www.freecharge.in</a></p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How do I know if the amount available in my Credits is sufficient for a transaction?</h4>
                                <div class="cbp-ntcontent">
                                    <ul>
                                        <li>Check the funds available in your Credits first. You can use it to pay for a transaction where the amount payable is less than or equal to your Credits.</li>
                                        <li>In case your total payable amount is more than the amount available in Credits, you can pay partially using Credits and the remaining using any other payment option</li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How do I pay for my next transaction using Credits?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Just login and proceed with your recharge, you will see <strong>Pay using Credits</strong> listed as a payment option along with others, select it and continue to pay. If you have lesser Credits than what is needed to pay for the transaction, you only need to pay the remaining amount using another payment option.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How do I see my Credits?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Check your Credits under &ldquo;<strong> My Account </strong>&rdquo;.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How is FreeCharge Credits a better payment option compared to Debit Card, Credit Card, Net banking, etc.?</h4>
                                <div class="cbp-ntcontent">
                                    <p>It is, and we'll tell you why. Credits save your time! You don't have to enter your card or banking details, remember all those passwords. Just utilize what you've already spent. That's not all, it's 100% secure too.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Do Credits expire?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Your Credits are safe and secure with us and you can keep it with us as long as you want to.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How long does it take for my money to reflect in Credits?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Your money against an unsuccessful recharge will reflect in Credits shortly. However, any deposit made to your Credits will be reflected instantly.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Can I get my money transferred from Credits to my bank account?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Not just yet. For now, we don&#39;t have the ability to refund your Credits into your original accounts (credit, debit or net banking). The funds available in Credits can be used for transactions on <a href="http://www.freecharge.in" target="_blank" >www.freecharge.in</a> only. <br/><br/>But Stay tuned, we&#39;re working on it!</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">My money is not reflecting in my Credits; however it has been deducted from my bank account.</h4>
                                <div class="cbp-ntcontent">
                                    <p>In some rare cases, it takes your bank&#39;s online system longer than usual to respond.
                                    <br/><br/>But you needn&#39;t worry, your money is not lost and it will be reflected in your Credits if the payment was successful but the recharge failed, else the money would be reflected in your bank account the next day.
                                    <br/><br/>If you need more clarity, reach out to us at <a href="mailto:care@freecharge.in" >care@freecharge.in</a>.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Can I add cash to my Credits?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Great question. The answer is yes! To top-ip your Credits, just click<a href="/app/mycredits.htm" target="_blank" > here</a>, add the desired amount, and proceed like you would for any other transaction. It&#39;s as simple as that!</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Will amount paid against Special Deals be refunded to my Credits?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Special deals are offers for which you get codes in your inbox if your recharge is successful. If you&#39;ve selected a special deal and your recharge fails, you won&#39;t get the code and your entire amount for that transaction will be refunded to your Credits.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">Will the amount paid against handling charge (for home page coupons) be refunded to my Credits as well?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Good news! If you've selected coupons and your payment goes through but recharge fails, we'll refund the handling charge paid for that transaction to your Credits.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <h3 class="cbp-nttrigger">GENERAL QUERIES</h3>
                    <div class="cbp-ntcontent">
                        <ul class="cbp-ntsubaccordion">
                            <li>
                                <h4 class="cbp-nttrigger">What is FreeCharge?</h4>
                                <div class="cbp-ntcontent">
                                    <p>FreeCharge.in is an online mobile recharge site which provides prepaid mobile recharge for most Indian telecom service providers including Vodafone, Airtel, Loop, Reliance, Idea, DOCOMO, Tata Indicom, S-Tel, Aircel, BSNL. <br> FreeCharge is special because you, as a user, have the power to make the recharge free.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How is my recharge free?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Your recharge is free as you get additional retail coupons, against every recharge, which can be redeemed at respective merchant&lsquo;s website or outlets. For example, if you recharged your mobile for Rs.100, you get coupons worth Rs.100 instantly via email or within 7 business days at your doorstep via courier. These retail coupons are issued for free. Though, a handling charge of Rs.10 will apply.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">What are the steps to recharge through FreeCharge?</h4>
                                <div class="cbp-ntcontent">
                                    <ol>
                                        <li>Enter Mobile number and amount for recharge.</li>
                                        <li>Choose coupons of equivalent amount.</li>
                                        <li>Pay online to get recharge instantly.</li>
                                        <li>Coupons will be delivered to your inbox instantly via email or to your home within 7 business days via courier.</li>
                                    </ol>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How can I know the tariff rates of my circle?</h4>
                                <div class="cbp-ntcontent">
                                    <p>Please check your service provider&#39;s website.</p>
                                </div>
                            </li>
                            <li>
                                <h4 class="cbp-nttrigger">How can I contact if I have some classified queries?</h4>
                                <div class="cbp-ntcontent">
                                    <p>You can contact us by going to link <a href="http://support.freecharge.in/" target="_blank" >support.freecharge.in</a> and register your query. We promise you quick response.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </section>
    </div>
</div>


<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.cbpNTAccordion.min.js"></script>
<script>
    $( function() {
        /*
         - how to call the plugin:
         $( selector ).cbpNTAccordion( [options] );
         - destroy:
         $( selector ).cbpNTAccordion( 'destroy' );
         */

        $( '#cbp-ntaccordion' ).cbpNTAccordion();

    } );
</script>