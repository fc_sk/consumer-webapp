<%@page import="com.freecharge.common.framework.properties.FCProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<script type="text/javascript"
    src="http://api.recaptcha.net/challenge?k=${recaptcha_public_key}">
</script>
<noscript>
    <iframe src="http://api.recaptcha.net/noscript?k=${recaptcha_public_key}"
        height="400" width="600" frameborder="0"></iframe><br>
    <input type="text" id="recaptcha_challenge_field" name="recaptcha_challenge_field" />
    <input type="hidden" id="recaptcha_response_field" name="recaptcha_response_field"  value="">
</noscript>
