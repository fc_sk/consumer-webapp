<%@page import="java.net.InetAddress"%>
<%@page import="com.freecharge.web.webdo.CommonSessionWebDo"%>
<%@page import="com.freecharge.common.framework.session.FreechargeContextDirectory"%>
<%@page import="com.freecharge.common.framework.session.FreechargeSession"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@page import="com.freecharge.common.util.FCConstants"%>
<c:set var="promoCode" value=""/>
<c:if test="${not empty freefundCoupon}">
	<c:set var="promoCode" value="${freefundCoupon.freefundCode }"/>
</c:if>
<%
String productType = (String) request.getParameter("productType");
boolean accessFromCredits = ("credits".equals(productType) ? true : false);
String codeTypeString = accessFromCredits ? "Freefund code" : "Promo code / Freefund code" ;
%>

<%if(accessFromCredits){ %>
<c:set var="failureMsgCss" value="failureMsgSmall-FromCredits"/>
<c:set var="successMsgCss" value="successMsgSmall-FromCredits"/>
<%}else{%>
<c:set var="failureMsgCss" value="failureMsgSmall-PromoCode"/>
<c:set var="successMsgCss" value="successMsgSmall-PromoCode"/>	
<%}%>
<div id="promo-tooltip" style="display: none">
    Promo codes are typically distributed to customers through our partners, and can be used to avail special discounts on your transaction.
    <br>
    <strong>Current promo code offers:</strong>
    <br>
    - HDFC Bank ( Debit Cards only )
    <br>
    - ICICI Bank ( For your user id and password <a target="_blank" href="http://www.icicibank.com/online-services/generate-password-personal.html">click here</a> )
	<br>
	- A FreeFund code is like a gift voucher code. The amount is fixed and it can be deposited instantly in your FreeCharge Credits.
</div>
<c:choose>
<c:when test="${not empty promoCode}">
     <div class="${successMsgCss}">You've applied the code successfully! 
        Your payment has been discounted by Rs. ${discountValue}</div>
</c:when>
<c:otherwise>
	<p class="discount-title clearfix" id="discount-title-promocode">
   	 <c:choose>
        <c:when test="${cart.totalCartPrice eq 20}">
          <strong>Got a Pepsi PromoCode ?</strong> Enter it in the space provided below.
       </c:when>
     <c:otherwise>
     	 <strong>Got a <%=accessFromCredits ? "FreeFund Code" : "Promo Code" %> ?</strong>
       		<%=accessFromCredits ? "" : " Enter it in the space provided below." %>
     </c:otherwise>
   </c:choose>
  </p>
<div id="redeeming-ajax" style="display:none;"><img src="${mt:keyValue("imgprefix1")}/images/ajax-loading.gif" /></div>
<div class="discount-content clearfix" id="promocodeTable">
   	     <table >
    	     <tr>
    	     	<td style="width: 200px; vertical-align: top;">
    	     		<input id="promoCodeId"  name="promoCode" class="discount-input" autocomplete="off" style="border: medium none; padding: 8px; margin: 0px; height: 24px; width: 290px; left: 0px; outline: medium none;" value="${promoCode}" type="text"/>
     	    		<br/><br/> 
     	    	</td>
     	    	
     	    	<td><div id="promocodeApplyMsgHolder" class="fc-message promoMsgHolder" style="display: none;"></div>
     	    	</td>
     	    </tr>
     	    <tr>	
     	    	<td id="captcha-placement-td" style="<c:if test="${not forceCaptcha}"> display:none; </c:if>padding-right: 20px;">
     	    		 <div id="captcha-placement-id"  >
     	    		 	Please enter the letters you see in the image below.
     	    		 	<div style="background-color: #F5F5F5;">
		     	    		<div id="captcha-draw-id" style="border: 1px dotted #dddddd; width:300px; padding-top: 5px; padding-left: 5px;">
		     				 	 <c:if test="${forceCaptcha}"> 
		     				 	 	<jsp:include page="../captcha-draw.jsp"></jsp:include>  
		     				 	 </c:if>
		     				</div> 
		     				<br/>
		     				<div  style="padding-top: 5px; padding-bottom: 5px; padding-left: 5px;">
		     	    			<input name="captcha" type="text" autocomplete="off" style="border: medium none; padding: 8px; 
		     	    			margin: 0px; height: 24px; width: 220px; left: 0px; outline: medium none; font-size: 14px;"  
		     	    			placeholder="Enter text here"  class="englishOnlyField required" /> 
		     	    			<a href="javascript:void(0)" onclick="redrawCaptcha();" style="padding-left: 12px;">Refresh</a>
	     	    			</div>
     	    			</div>
     	    		 </div>
     	    	</td>
     	    	<td style="vertical-align: bottom;">
     	    		<button class="buttonTwo" id="addPromo" onClick="applyPromo(this, '#removePromo', true);">REDEEM</button>
     	    		<button id="promoContinue" type="button" class="buttonThree">Proceed &rarr;</button>
     	    		
     	        </td>
     	    </tr>
   	     </table>
</div>
<script type="text/javascript" >

	var isReCaptchaEnbled = ${forceCaptcha};
	$('#promoContinue').hide();
	function redrawCaptcha() {
		$("#captcha-draw-id").html("");
		$.ajax({
			type : "GET",
			url:"<c:url value='/captcha/regenerate' />",
			cache: false,
			success : function(data) {
				$("#captcha-draw-id").html(data); 
			},
			error:function(xhr,ajaxOptions, thrownError){
				$("#captcha-draw-id").html("<a href=\"javascript:void(0)\" onclick=\"redrawCaptcha();\">Retry</a>"); 
			}
		}) 
	}

    function applyPromo(obj, removeId) {
    	
        var productType = "<%=request.getParameter("productType")%>";
        var lookupID = "<%=request.getParameter("lookupID")%>";
        var couponValue = $('#promoCodeId').val();
        
        if(couponValue == "") {
    		$("#promocodeApplyMsgHolder").show().html("<p class=\"${failureMsgCss} \" >Please enter the <%=codeTypeString%> </p>");
    		return;
    	}
       
        var formData = 'couponCode='+couponValue+'&productType='+productType+'&lookupID='+lookupID;
        
        if(isReCaptchaEnbled) {
        	if($("input[name=captcha]").val() == "") {
        		$("#promocodeApplyMsgHolder").show().html("<p class=\"${failureMsgCss} \" >You haven't entered the characters shown on the image.</p>");
        		return;
        	}
        	formData += '&captcha='+$("input[name=captcha]").val() ;
        	$("#captcha-placement-id").hide();
        }
        $("#redeeming-ajax").show();
        if($("#removeCashBackCampaign").is(':visible'))
        {
          alert("Err, looks like you have already availed your discount!");
          return false;
        }

        var url='/promocode/apply.htm';
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            dataType: "json",
            cache: false,
            timeout:1800000,
            success: function (responseText) {
            	$("#redeeming-ajax").hide();
                var freefundCoupon = responseText.freefundCoupon;
                if (responseText.status == 'success' || responseText.status == 'redeemed') {
                	
                    if(lookupID != "" && responseText.status == 'success') {
                    	refreshCart(lookupID);
                        refreshCheckoutButton();
                    }
                    
                    $( removeId ).hide();
                    $(obj).hide();
                    $( removeId ).hide();
                    $("#promoCodeId" ).attr("disabled", "disabled");  
                    $("#promocodeApplyMsgHolder").show().html("<p class=\"${successMsgCss} \" >"+responseText.successMsg+"</p>");
                } else if(responseText.status == 'failure') {
                	isReCaptchaEnbled = responseText.forceCaptcha;
                	if(isReCaptchaEnbled) {
                		redrawCaptcha() ;
                		$("#captcha-placement-id").show();
                		$("#captcha-placement-td").show();
                	}
                	$("#promocodeApplyMsgHolder").show().html('<p class="${failureMsgCss} " >'+responseText.errors+'</p>');
                }
                
            },
            error:function (xhr,status,error){
            	$("#redeeming-ajax").hide();
                alert("Error occurred " + error);
            }
        });

        return false;   
    }
    
    $('#promoContinue').on('click', function(e){
    	e.preventDefault();
    	$('#zeroPayContinue').click();
    });
    
       
</script>
</c:otherwise>
</c:choose>
