<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
  <script type="text/javascript">
    window.location = "https://freecharge.in/termsandconditions";
  </script>
<nav>
  <ul>
    <li><a href="/app/aboutus.htm" >About us</a></li>
    <li><a href="/app/contactus.htm"  >Contact us</a></li>
    <!--<li><a href="/app/faq.htm">FAQ</a></li>-->
    <li><a href="/app/sitemap.htm">Sitemap</a></li>
    <%--<li><a href="/app/feedback.htm" >Feedback</a></li>--%>
    <li><a href="http://support.freecharge.in" target="_blank" >Customer support</a></li>
    <li><a href="/app/termsandconditions.htm"  class="active" >Terms &amp; conditions</a></li>
        <!--<li><a href="/app/privacypolicy.htm" >Privacy policy</a></li>
         <li><a href="/security/disclosure.htm">Security Policy</a></li><br> -->
       </ul>
     </nav>
     <div class="row">
      <div class="col-md-12">
        <section class="static-content">
          <ul id="tnc-ntaccordion" class="cbp-ntaccordion">
            <li>
              <h3 class="cbp-nttrigger">Privacy Policy</h3>
              <div class="cbp-ntcontent">
                <p>Last Updated on January 17,2018 </p>
                <p> THIS PRIVACY POLICY IS AN ELECTRONIC RECORD IN THE FORM OF AN ELECTRONIC CONTRACT FORMED UNDER THE INFORMATION TECHNOLOGY ACT, 2000 AND THE RULES MADE THEREUNDER AND THE AMENDED PROVISIONS PERTAINING TO ELECTRONIC DOCUMENTS / RECORDS IN VARIOUS STATUTES AS AMENDED BY THE INFORMATION TECHNOLOGY ACT, 2000. THIS PRIVACY POLICY DOES NOT REQUIRE ANY PHYSICAL, ELECTRONIC OR DIGITAL SIGNATURE. </p>

                <p> THIS PRIVACY POLICY IS A LEGALLY BINDING DOCUMENT BETWEEN YOU AND FREECHARGE (BOTH TERMS DEFINED BELOW). THE TERMS OF THIS PRIVACY POLICY WILL BE EFFECTIVE UPON YOUR ACCEPTANCE OF THE SAME (DIRECTLY OR INDIRECTLY IN ELECTRONIC FORM, BY CLICKING ON THE I ACCEPT TAB OR BY USE OR THE WEBSITE OR BY OTHER MEANS) AND WILL GOVERN THE RELATIONSHIP BETWEEN YOU AND FREECHARGE FOR YOUR USE OF THE WEBSITE (DEFINED BELOW).
                  THIS DOCUMENT IS PUBLISHED AND SHALL BE CONSTRUED IN ACCORDANCE WITH THE PROVISIONS OF THE INFORMATION TECHNOLOGY (REASONABLE SECURITY PRACTICES AND PROCEDURES AND SENSITIVE PERSONAL DATA OF INFORMATION) RULES, 2011 UNDER INFORMATION TECHNOLOGY ACT, 2000; THAT REQUIRE PUBLISHING OF THE PRIVACY POLICY FOR COLLECTION, USE, STORAGE AND TRANSFER OF SENSITIVE PERSONAL DATA OR INFORMATION.
                </p>

                <p> PLEASE READ THIS PRIVACY POLICY CAREFULLY. BY USING THE WEBSITE, YOU INDICATE THAT YOU UNDERSTAND, AGREE AND CONSENT TO THIS PRIVACY POLICY. IF YOU DO NOT AGREE WITH THE TERMS OF THIS PRIVACY POLICY, PLEASE DO NOT USE THIS WEBSITE. YOU HEREBY PROVIDE YOUR UNCONDITIONAL CONSENT OR AGREEMENTS TO FREECHARGE AS PROVIDED UNDER   INFORMATION TECHNOLOGY ACT, 2000 AND RULES AND REGULATIONS MADE UNDER THERIN.</p>

                <p> By providing us your Information or by making use of the services provided by the Website, You hereby consent to the collection, storage, processing and transfer of any or all of Your Personal Information and Non-Personal Information by Freecharge as specified under this Privacy Policy. You further agree that such collection, use, storage and transfer of Your Information shall not cause any loss or wrongful gain to you or any other person. </p>

                <p> Freecharge Payment Technologies Private Limited its affiliates- Accelyst Solutions Private Limited (individually and/ or collectively, "Freecharge") is/are concerned about the privacy of the data and information of users (including merchants  and buyers/customers whether registered or non-registered) accessing, offering, selling or purchasing products or services on Freecharge's websites, mobile sites or mobile applications ("Website") on the Website and otherwise doing business with Freecharge. </p>

                <p> The terms "We" / "Us" / "Our" individually and collectively refer to each entity being part of the definition of Freecharge and the terms "You" / "Your" / "Yourself" refer to the users.  </p>

                <p> This Privacy Policy is a contract between You and the respective Freecharge entity whose Website You use or access or You otherwise deal with. This Privacy Policy shall be read together with the respective Terms Of Use or other terms and condition of the respective Freecharge entity and its respective Website or nature of business of the Website. </p>

                <p><b>1. Freecharge has provided this Privacy Policy to familiarize You with:</b></p>             
                <p>a. The type of data or information that You share with or provide to Freecharge and that Freecharge collects from You; </p>
                <p>b. The purpose for collection of such data or information from You;</p>
                <p>c. Freecharge's information security practices and policies; and</p>
                <p>d. Freecharge's policy on sharing or transferring Your data or information with third parties.</p>
                <p>e. This Privacy Policy may be amended / updated from time to time. Upon amending / updating the Privacy Policy, We will accordingly amend the date above. We suggest that you regularly check this Privacy Policy to apprise yourself of any updates. Your continued use of Website or provision of data or information thereafter will imply Your unconditional acceptance of such updates to this Privacy Policy.</p>             
                <p></p>            
                <p> <b>2. Information collected and storage of such Information: </b> </p>
                <p>The "Information" (which shall also include data) provided by You to Freecharge or collected from You by Freecharge may consist of "Personal Information" and "Non-Personal Information".</p>
                <p></p>
                <p> <b>3. Personal Information </b> </p>
                <p> Personal Information is Information collected that can be used to uniquely identify or contact You. Personal Information for the purposes of this Privacy Policy shall include, but not be limited to: </p>

                <p>a. Your user name along with Your password, </p>
                <p>b. Your name, </p>
                <p>c. Your address, </p>
                <p>d.  Your telephone number, </p>
                <p>e.  Your e-mail address or other contact information, </p>
                <p>f.  Your date of birth, </p>
                <p>g.  Your gender, </p>
                <p>h.  Information regarding your transactions on the Website, (including sales or purchase history), </p>
                <p>i.  Your financial information such as bank account information or credit card or debit card or other payment instrument details, </p>
                <p>j.  Internet Protocol address, </p>
                <p>k.  Any other items of 'sensitive personal data or information' as such term is defined under the Information Technology (Reasonable Security Practices And Procedures And Sensitive Personal Data Of Information) Rules, 2011 enacted under the Information Technology Act, 2000; </p>
                <p>l.  Identification code of your communication device which You use to access the Website or otherwise deal with any Freecharge entity, </p>
                <p>m.  Any other Information that You provide during Your registration process, if any, on the Website. </p>       
                <p>n. Such Personal Information may be collected in various ways including during the course of You:</p>

                <p>i. registering as a user on the Website, </p>
                <p>ii. registering as a merchant on the Website, </p>
                <p>iii. availing certain services offered on the Website. Such instances include but are not limited to making a participating in any online survey or contest, communicating with Freecharge's customer service by phone, email or otherwise or posting user reviews on the services/items available on the Website, or</p>
                <p>iv. otherwise doing business on the Website or otherwise dealing with any Freecharge entity. </p>
                <p>v. We may receive Personal information about you from third parties, such as social media services, commercially available sources and business partners. If you access Website through a social media service or connect a service on Website to a social media service, the information we collect may include your user name associated with that social media service, any information or content the social media service has the right to share with us, such as your profile picture, email address or friends list, and any information you have made public in connection with that social media service. When you access the Website or otherwise deal with any Freecharge entity through social media services or when you connect any Website to social media services, you are authorizing Freecharge to collect, store, and use and retain such    information and content in accordance with this Privacy Policy.</p>


                <p> <b>4. Non-Personal Information </b> </p>
                <p>a. Freecharge may also collect information other than Personal Information from You through the Website when You visit and / or use the Website. Such information may be stored in server logs. This Non-Personal Information would not assist Freecharge to identify You personally.</p>
                <p>b. This Non-Personal Information may include: </p>

                <p>i. Your geographic location,</p>
                <p>ii. details of Your telecom service provider or internet service provider,</p>
                <p>iii. the type of browser (Internet Explorer, Firefox, Opera, Google Chrome etc.),</p>
                <p>iv. the operating system of Your system, device and the Website You last visited before visiting the Website,</p>
                <p>v. The duration of Your stay on the Website is also stored in the session along with the date and time of Your access,</p>
                <p>vi. Non-Personal Information is collected through various ways such through the use of cookies. Freecharge may store temporary or permanent 'cookies' on Your computer. You can erase or choose to block these cookies from Your computer. You can configure Your computer's browser to alert You when we attempt to send You a cookie with an option to accept or refuse the cookie. If You have turned cookies off, You may be prevented from using certain features of the Website.</p>
                <p>vii. Ads: Freecharge may use third-party service providers to serve ads on Freecharge's behalf across the internet and sometimes on the Website. They may collect Non-Personal Information about Your visits to the Website, and Your interaction with our products and services on the Website.</p>

                <p>Please do note that Personal Information and Non-Personal Information may be treated differently as per this Privacy Policy.</p>

                <p>c. You hereby represent to Freecharge that: </p>

                <p>i. the Information you provide to Freecharge from time to time is and shall be authentic, correct, current and updated and You have all the rights, permissions and consents as may be required to provide such Information to Freecharge.</p>
                <p>ii. Your providing the Information to Freecharge and Freecharge's consequent storage, collection, usage, transfer, access or processing of the same shall not be in violation of any third party agreement, laws, charter documents, judgments, orders and decrees.</p>
                <p>iii. Freecharge and each of Freecharge entities officers, directors, contractors or agents shall not be responsible for the authenticity of the Information that You or any other user provide to Freecharge. You shall indemnify and hold harmless Freecharge and each of Freecharge entities officers, directors, contracts or agents and any third party relying on the Information provided by You in the event You are in breach of this Privacy Policy including this provision and the immediately preceding provision above.</p>
                <p>iv. Your Information will primarily be stored in electronic form however certain data can also be stored in physical form. We may store, collect, process and use your data in countries other than Republic of India but under compliance with applicable laws. We may enter into agreements with third parties (in or outside of India) to store or process your information or data. These third parties may have their own security standards to safeguard your information or data and we will on commercial reasonable basis require from such third parties to adopt reasonable security standards to safeguard your information / data.</p>             

                <p><b>5. Purpose for collecting, using, storing and processing Your Information</b></p>
                <p>a. Freecharge collects, uses, stores and processes Your Information for any purpose as may be permissible under applicable laws (including where the applicable law provides for such collection, usage, storage or processes in accordance with the consent of the user) connected with a function or activity of each of Freecharge entities and shall include the following:</p>             
                <p>i. to facilitate Your use of the Website or other services of Freecharge entities;</p>
                <p>ii. to respond to Your inquiries or fulfil Your requests for information about the various products and services offered on the Website;</p>
                <p>iii. to provide You with information about products and services available on the Website and to send You information, materials, and offers from Freecharge;</p>
                <p>iv. to send You important information regarding the Website, changes in terms and conditions, user agreements, and policies and/or other administrative information;</p>
                <p>v. to send You surveys and marketing communications that Freecharge believes may be of interest to You;</p>
                <p>vi. to personalize Your experience on the Website by presenting advertisements, products and offers tailored to Your preferences;</p>
                <p>vii. to help You address Your problems incurred on the Website including addressing any technical problems;</p>
                <p>viii. if You purchase any product or avail of any service from the Website, to complete and fulfil Your purchase, for example, to have Your payments processed, communicate with You regarding Your purchase and provide You with related customer service;</p>
                <p>ix. for proper administering of the Website;</p>
                <p>x. to conduct internal reviews and data analysis for the Website (e.g., to determine the number of visitors to specific pages within the Website);</p>
                <p>xi. to improve the services, content and advertising on the Website;</p>
                <p>xii. to provide value added services such as single sign on. single sign on shall mean a session/user authentication process that permits a user to enter his/her name or mobile number or e-mail address or any combination thereof and password in order to access multiple websites and applications;</p>
                <p>xiiito facilitate various programmes and initiatives launched by Freecharge or third party service providers and business associates</p>
                <p>xiv. to analyze how our services are used, to measure the effectiveness of advertisements, to facilitating payments</p>
                <p>xv. to conducting academic research and surveys</p>
                <p>xvi. to protect the integrity of the Website;</p>
                <p>xvii. to respond to legal, judicial, quasi judicial process and provide information to law enforcement agencies or in connection with an investigation on matters related to public safety, as permitted by law;</p>
                <p>xviii. to conduct analytical studies on various aspects including user behaviour, user preferences etc.;</p>
                <p>xix. to permit third parties who may need to contact users who have bought products from the Website to facilitate installation, service and any other product related support;</p>
                <p>xx. to implement information security practices;</p>
                <p>xxi. to determine any security breaches, computer contaminant or computer virus;</p>
                <p>xxii. to investigate, prevent, or take action regarding illegal activities and suspected fraud,</p>
                <p>xxiii. to undertake forensics of the concerned computer resource as a part of investigation or internal audit;</p>
                <p>xxiv. to trace computer resources or any person who may have contravened, or is suspected of having or being likely to contravene, any provision of law including the Information Technology Act, 2000 that is likely to have an adverse impact on the services provided on any Website or by Freecharge;</p>
                <p>xxv. to enable a potential buyer or investor to evaluate the business of Freecharge </p>
                <p>xxvi. [Individually and collectively referred to as ("<b>Purposes</b>")]</p>
                <p>xxvii. You hereby agree and acknowledge that the Information so collected is for lawful purpose connected with a function or activity of each of the Freecharge entities or any person on their respective behalf, and the collection of Information is necessary for the Purposes.</p>          
                <p><b>6. Sharing and disclosure of Your Information</b></p>
                <p>a. You hereby unconditionally agree and permit that Freecharge may transfer, share, disclose or part with all or any of Your Information, within and outside of the Republic of India to various Freecharge entities and to third party service providers / partners / banks and financial institutions for one or more of the Purposes or as may be required by applicable law. In such case we will contractually oblige the receiving parties of the Information to ensure the same level of data protection that is adhered to by Freecharge under applicable law.</p>

                <p>b. You acknowledge and agree that, to the extent permissible under applicable laws, it is adequate that when Freecharge transfers Your Information to any other entity within or outside Your country of residence, Freecharge will place contractual obligations on the transferee which will oblige the transferee to adhere to the provisions of this Privacy Policy.</p>

                <p>c. Freecharge may share statistical data and other details (other than Your Personal Information) without your express or implied consent to facilitates various programmes or initiatives launched by Freecharge, its affiliates, agents, third party service providers, partners or banks & financial institutions, from time to time. We may transfer/disclose/share Information (other than Your Personal Information) to those parties who support our business, such as providing technical infrastructure services, analysing how our services are used, measuring the effectiveness of advertisements, providing customer / buyer services, facilitating payments, or conducting academic research and surveys. These affiliates and third party service providers shall adhere to confidentiality obligations consistent with this Privacy Policy. Notwithstanding the above, We use other third parties such as a credit/debit card processing company, payment gateway, pre-paid cards etc. to enable You to make payments for buying products or availing services on Freecharge. When You sign up for these services, You may have the ability to save Your card details for future reference and faster future payments. In such case, We may share Your relevant Personal Information as necessary for the third parties to provide such services, including your name, residence and email address. The processing of payments or authorization is solely in accordance with these third parties policies, terms and conditions and we are not in any manner responsible or liable to You or any third party for any delay or failure at their end in processing the payments.</p>

                <p>d. Freecharge may also share Personal Information if Freecharge believe it is necessary in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of various terms and conditions or our policies.</p>
                <p>e. We reserve the right to disclose your information when required to do so by law or regulation, or under any legal obligation or order under law or in response to a request from a law enforcement or governmental agency or judicial, quasi-judicial or any other statutory or constitutional authority or to establish or exercise our legal rights or defend against legal claims.</p>

                <p>f. You further agree that such disclosure, sharing and transfer of Your Personal Information and Non-Personal Information shall not cause any wrongful loss to You or to any third party, or any wrongful gain to us or to any third party.</p>
                <p></p>
                <p><b>7. Links to third party websites</b></p>
                <p>a. Links to third-party advertisements, third-party websites or any third party electronic communication service may be provided on the Website which are operated by third parties and are not controlled by, or affiliated to, or associated with, Freecharge unless expressly specified on the Website.</p>

                <p>b. Freecharge is not responsible for any form of transmission, whatsoever, received by You from any third party website. Accordingly, Freecharge does not make any representations concerning the privacy practices or policies of such third parties or terms of use of such third party websites, nor does Freecharge control or guarantee the accuracy, integrity, or quality of the information, data, text, software, music, sound, photographs, graphics, videos, messages or other materials available on such third party websites. The inclusion or exclusion does not imply any endorsement by Freecharge of the third party websites, the website's provider, or the information on the third party website. The information provided by You to such third party websites shall be governed in accordance with the privacy policies of such third party websites and it is recommended that You review the privacy policy of such third party websites prior to using such websites.</p>
                <p></p>
                <p><b> 8. Security &amp; Retention </b></p>
                <p>a. The security of your Personal Information is important to us. Freecharge strives to ensure the security of Your Personal Information and to protect Your Personal Information against unauthorized access or unauthorized alteration, disclosure or destruction. For this purpose, Freecharge adopts internal reviews of the data collection, storage and processing practices and security measures, including appropriate encryption and physical security measures to guard against unauthorized access to systems where Freecharge stores Your Personal Information. Each of the Freecharge entity shall adopt reasonable security practices and procedures as mandated under applicable laws for the protection of Your Information. Provided that Your right to claim damages shall be limited to the right to claim only statutory damages under Information Technology Act, 2000 and You hereby waive and release all Freecharge entities from any claim of damages under contract or under tort.</p>

                <p>b. If you choose a payment gateway to complete any transaction on Website then Your credit card data may be stored in compliance with industry standards/ recommended data security standard for security of financial information such as the Payment Card Industry Data Security Standard (PCI-DSS).</p>

                <p>c. Freecharge may share your Information with third parties under a confidentiality agreement which inter alia provides for that such third parties not disclosing the Information further unless such disclosure is for the Purpose. However, Freecharge is not responsible for any breach of security or for any actions of any third parties that receive Your Personal Information. Freecharge is not liable for any loss or injury caused to You as a result of You providing Your Personal Information to third party (including any third party websites, even if links to such third party websites are provided on the Website).</p>
                <p>d. Notwithstanding anything contained in this Policy or elsewhere, Freecharge shall not be held responsible for any loss, damage or misuse of Your Personal Information, if such loss, damage or misuse is attributable to a Force Majeure Event (as defined below).</p>

                <p>e. A <b>Force Majeure Event</b> shall mean any event that is beyond the reasonable control of Freecharge and shall include, without limitation, sabotage, fire, flood, explosion, acts of God, civil commotion, strikes or industrial action of any kind, riots, insurrection, war, acts of government, computer hacking, unauthorised access to computer, computer system or computer network,, computer crashes, breach of security and encryption (provided beyond reasonable control of Freecharge), power or electricity failure or unavailability of adequate power or electricity.</p>

                <p>f. While We will endeavor to take all reasonable and appropriate steps to keep secure any Personal Information which We hold about You and prevent unauthorized access, You acknowledge that the internet or computer networks are not fully secure and that We cannot provide any absolute assurance regarding the security of Your Personal Information.</p>

                <p>g. You agree that all Personal Information shall be retained till such time required for the Purpose or required under applicable law, whichever is later. Non-Personal Information will be retained indefinitely.</p>
                <p></p>
                <p><b>9. User discretion and opt out</b></p>
                <p>a. You agree and acknowledge that You are providing your Information out of your free will. You have an option not to provide or permit Freecharge to collect Your Personal Information or later on withdraw Your consent with respect to such Personal Information so provided herein by sending an email to the grievance officer or such other electronic address of the respective Freecharge entity as may be notified to You. In such case, You should neither visit the Website nor use any services provided by Freecharge entities nor shall contact any of Freecharge entities. Further, Freecharge may not deliver products to You, upon Your order, or Freecharge may deny you access from using certain services offered on the Website.</p>
                <p>b. You can add or update Your Personal Information on regular basis. Kindly note that Freecharge would retain Your previous Personal Information in its records.</p>
                <p></p>

                <p><b>10. Grievance Officer</b></p>
                <p>a. If you find any discrepancies or have any grievances in relation to the collection, storage, use, disclosure and transfer of Your Personal Information under this Privacy Policy or any terms of Freecharge's Terms of Use, and other terms and conditions or polices of any Freecharge entity, please contact the following:</p>

                <p><b>For Accelyst Solution Private Limited:</b></p>
                <p>Mr. Velmurugan Arumugam, the designated grievance officer under Information Technology Act, 2000 </p>
                <p>E-mail: grievanceofficer@freecharge.com. </p>

                <p><b>For Freecharge Payment Technologies Private Limited:</b></p>
                <p>Mr. Nemash Simaria, the designated grievance officer under Information Technology Act, 2000 </p>
                <p>E-mail: grievanceofficer@freecharge.com. </p>
                <p>Working Days: Monday to Friday<br/>
                  Working Hours:9:30 am to 6:30 pm
                </p>
                <p>b. We will strive to address your feedback and concerns in a timely and effective manner. The details of the grievance officer may be changed by us from time to time by updating this Privacy Policy.</p>
                <p></p>
                <p><b>11. Business / Assets Sale or Transfers </b></p>
                <p>a. Freecharge may sell, transfer or otherwise share some or all of its assets, including Your Information in connection with a merger, acquisition, reorganization or sale of assets or business or in the event of bankruptcy. Should such a sale or transfer occur, such Freecharge entity will reasonably ensure that the Information you have provided and which we have collected is stored and used by the transferee in a manner that is consistent with this Privacy Policy. Any third party to which any of Freecharge entity transfers or sells as aforesaid will have the right to continue to use the Information that You provide to us or collected by Us immediately prior to such transfer or sale.</p>
                <p></p>
                <p><b>12. Further Acknowledgements</b></p>
                <p>a. You hereby acknowledge and agree that this Privacy Policy:</p>
                <p>i. is clear and easily accessible and provide statements of Freecharge policies and practices with respective to the Information; </p>
                <p>ii. provides for the various types of personal or sensitive personal data of information to be collected; </p>
                <p>iii. provides for the purposes of collection and usage of the Information; </p>
                <p>iv. provides for disclosure of Information; and </p>
                <p>v. provides for reasonable security practices and procedures.</p>             
              </div>
            </li>
            <li>
              <h3 class="cbp-nttrigger">Terms and Conditions for usage of FreeCharge</h3>
              <div class="cbp-ntcontent">
                <p>Last Updated on January 17,2018 </p>
                <h4>In using this website you are deemed to have read and agreed to the following terms and conditions:</h4>
                <p></p>
                <p>1. The following terminology applies to these Terms and Conditions:</p>
                <ul>
                  <li>"Client", "You" and "Your" refers to you, the person accessing this website and accepting the Company's terms and conditions. </li>
                  <li>"The Company", "Ourselves", “Freecharge”, "We" and "Us", refers to Freecharge.in or Accelyst Solutions Pvt. Ltd.  </li>
                  <li>"Party", "Parties" refers to both the Client and ourselves, or either the Client or ourselves.</li>
                </ul>
                <p></p>
                <p>2. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner. Any use of the above terminology or other words in the singular, plural, capitalisation and/or he/she or they, are taken as interchangeable and therefore as referring to same. </p>
                <p></p>
                <p>3. By becoming a registered user of FreeCharge.in and our affiliate sites, you agree to be contacted by FreeCharge.in and our affiliates via communication modes including but not limited to email, SMS, MMS, and telephonic calls.</p>
                <p></p>
                <p>4. You agree that Freecharge may, in its sole discretion and without prior notice, terminate Your access to the Website and block Your future access to the Website if Freecharge determines that You have violated the terms of these Terms and Conditions or any other agreement(s). You also agree that any violation by You of the agreement(s) will cause irreparable harm to Freecharge, for which monetary damages may be inadequate, and You consent to Freecharge obtaining any injunctive or equitable relief that Freecharge deems necessary or appropriate in such circumstances. These remedies are in addition to any other remedies Freecharge may have at law or in equity.</p>
                <p></p>
                <p>5. Freecharge reserves the right to refuse access to use the services offered at the Website to new Users or to terminate access granted to existing Users at any time without according any reasons for doing so.</p>
                <p></p>
                <p>6. Payment: We accept Visa and Master Credit Card, Debit Cards and Net banking as mode of payments which are processed through our payment gateway partner. No information pertaining to payment is shared with us as you fill all the payment related information on your bank's site.</p>
                <p></p>
                <p>7. Recharge: Freecharge has tied up with a third-party enabler for the recharge. Freecharge is not responsible for any delay, pricing or cancellation of recharge from mobile operators end. Though, best efforts are made to keep the service above acceptance levels.</p>
                <p></p>
                <p>8. User is solely responsible for the selection of mobile operator and the recharge amount. Freecharge is not responsible for talk-time given against recharge done through freecharge.in as this is purely at mobile operator's disposal.</p>
                <p></p>
                <p>9. Consent to use Contact Information: You agree that we may collect, use and share certain information regarding the contacts contained in Your device’s phone book (“Contact Information”) for promotional and marketing purpose and also share it with any third party in accordance with the Privacy Policy. By allowing Contact Information to be collected, You give Freecharge a right to use that Contact Information as a part of the service provided by Freecharge and you guarantee that you have any and all permissions required to share such Contact Information with us.</p>
                <p></p>
                <p>10. Refunds: All sales are final with no refund or exchange permitted. You are responsible for the payment of the mobile number or DTH account number or bill payment and all charges that result from these payments. We shall not be responsible for any payment for an incorrect mobile number or DTH account number or bill payment number. In case, money has been charged to your card or bank account or pre-paid payment instrument and a payment/service is not delivered by the service provider within 24 hours of your completion of the transaction then you may inform us regarding the same by sending an email to care@freecharge.in. Please include in the email the following details: the mobile number or DTH account number or bill payment number, service provider name, payment value, transaction date and order number. We shall investigate such incidents and if it is found that money was indeed charged to your card or bank account or pre-paid payment instrument without delivery of the payment/service then you will be refunded the money within 21 working days from the date of receipt of your email. All refunds will be credited to your card or bank account or the Freecharge Balance Account. We shall have the sole discretion to determine the mode of reversal from the above-mentioned options.</p>
                <p></p>
                <p>11. Coupon Redemption: Coupon redemption is purely subjected to standard and specified terms and conditions mentioned by the respective retailer. Coupons are issued on behalf of the respective retailer. Hence, any damages, injuries, losses incurred by the end user by using the coupon is not the responsibility of freecharge.in.</p>
                <p>12. Exclusions and Limitations: The information on this web site is provided on an "as is" basis. To the fullest extent permitted by law, this Company:<br/>
                  (a) excludes all representations and warranties relating to this website and its contents or which is or may be provided by any affiliates or any other third party, including in relation to any inaccuracies or omissions in this website and/or the Company's literature; and<br/>
                  (b) excludes all liability for damages arising out of or in connection with your use of this website. This includes, without limitation, direct loss, loss of business or profits (whether or not the loss of such profits was foreseeable, arose in the normal course of things or you have advised this Company of the possibility of such potential loss), damage caused to your computer, computer software, systems and programs and the data thereon or any other direct or indirect, consequential and incidental damages.
                </p>
                <p></p>
                <p>13. Limitation of Liability: All transactions on the Freecharge website, monetary and otherwise, are non-refundable, non-exchangeable and non-reversible, save and except in the event of proven gross negligence on the part of Freecharge or its representatives, in which event the user agrees that the user shall only be entitled to a refund of the amount actually paid by the user and actually received by Freecharge with respect to the recharge done by the user. This limitation applies whether the alleged liability is based on contract, tort, negligence, strict liability, or any other basis, even if Freecharge has been advised of the possibility of such damage. Because some jurisdictions do not allow the exclusion or limitation of incidental or consequential damages, Freecharge liability in such jurisdictions shall be limited to the extent of the amount actually paid for by any user while availing any of the services on the FreeCharge website.</p>
                <p></p>
                <p>14. Modification of These Terms and Conditions of Use: The Company reserves the right to change these conditions from time to time as it sees fit and your continued use of the site will signify your acceptance of any adjustment to these terms. User is required to check terms and conditions at every visit. No notification communication will be done subject to change in terms and conditions.</p>
                <p></p>
                <p>15. Force Majeure: Neither party shall be liable to the other for any failure to perform any obligation under any agreement which is due to an event beyond the control of such party including but not limited to any Act of god, terrorism, war, Political insurgence, insurrection, riot, civil unrest, act of civil or military authority, uprising, earthquake, flood or any other natural or man made eventuality outside of our control, which causes the termination of an agreement or contract entered into, nor which could have been reasonably foreseen. Any Party affected by such event shall forthwith inform the other Party of the same and shall use all reasonable endeavours to comply with the terms and conditions of any agreement contained herein.</p>
                <p></p>
                <p>16. General Provision: All the terms and conditions between you and Freecharge.in shall be governed by the law of India. By visiting Freecharge.in, you agree to the laws. Any dispute of any sort that might arise between you and Freecharge.in or its affiliates is subject to New Delhijurisdiction only.</p>
                <p></p>
                <h4>Freecharge is headquartered at:</h4>
                <p>
                  Accelyst Solutions Private Limited,<br>
                  Unit No. 205-206, Plot No 1, 2nd floor, Vaibhav Chambers,<br>
                  Bandra Kurla Complex, Bandra East, Mumbai-400051, India<br>
                  CIN- U72900MH2008PTC185202
                </p>
                <p></p>
                <p>
                  Freecharge Payment Technologies Private Limited,<br>
                  68, Okhla Industrial Estate, Phase-3, New Delhi- 110020, India <br>
                  CIN- U74140DL2015PTC275419
                </p>
              </div>
            </li>
            <li>
              <h3 class="cbp-nttrigger">Terms and Conditions for FreeCharge account</h3>
              <div class="cbp-ntcontent">
                <p>Last Updated on January 17,2018 </p>
                <p>THIS DOCUMENT IS PUBLISHED IN COMPLIANCE OF AND SHALL BE GOVERNED BY INDIAN LAW, INCLUDING BUT NOT LIMITED TO (I) THE INDIAN CONTRACT ACT, 1872; (II) THE INFORMATION TECHNOLOGY ACT, 2000, THE RULES, REGULATIONS, GUIDELINES AND CLARIFICATIONS FRAMED THEREUNDER; (III) THE PAYMENT AND SETTLEMENT SYSTEMS ACT, 2007 AND APPLICABLE RULES, REGULATIONS AND GUIDELINES MADE THEREUNDER.</p>
                <p>THIS TERMS AND CONDITIONS IS A LEGALLY BINDING DOCUMENT BETWEEN YOU AND FREECHARGE (BOTH TERMS DEFINED BELOW). THE TERMS OF THIS DOCUMENT WILL BE EFFECTIVE UPON YOUR ISSUANCE OF FREECHARGE ACCOUNT BY FREECHARGE OR YOUR USAGE OF THE FREECHARGE ACCOUNT AND WILL GOVERN THE RELATIONSHIP BETWEEN YOU AND FREECHARGE FOR THE USE OF FREECHARGE ACCOUNT AND SERVICES (DEFINED BELOW). THESE TERMS ARE IN ADDITION TO AND NOT IN DEROGATION WITH TERMS AND CONDITIONS OF ANY OTHER PRODUCT OR SERVICES OF FREECHARGE.</p>
                <p>Before Freecharge Payment Technologies Private Limited issues You the Freecharge Account or before You use the Freecharge Account, please read these TERMS &amp; CONDITIONS carefully. </p>
                <p>By offering to acquire, accepting or using Freecharge Account, You are unconditionally agreeing to the Terms and Conditions set out below with Freecharge Payment Technologies Private Limited and will be bound by them.</p>
                <h4>By agreeing to these Terms and Conditions, You represent the following:</h4>
                <ul>
                  <li>You are 18 (eighteen) years old or older;</li>
                  <li>Capable of entering into a legally binding agreement;</li>
                  <li>You are an Indian resident;</li>
                  <li>You have not been previously suspended or removed from the Prepaid Payment Account Services or Single-Sign-On Services;</li>
                  <li>You have valid and subsisting right, authority, and capacity to enter into these Terms and Conditions and to abide by all of the terms and conditions contained herein; and</li>
                  <li>You are not impersonating any person or entity, or falsely stating or otherwise misrepresenting identity, age or affiliation with any person or entity.</li>
                </ul>
                <p>All references in these Terms and Conditions to You or Your shall refer to the users or Customer of Website or Merchant Site(s). <br/>
                  All references in these Terms and Conditions to We, Us, or Our shall refer to Freecharge Payment Technologies Private Limited, and their respective affiliates, as the case may be.
                </p>
                <p><strong>1. REGISTRATION FOR SERVICES</strong></p>  
                <p>1.1  You must register with us and open a Freecharge account ("Freecharge Account") in order to use the services. We allow both individuals and Legal Entities to register with Freecharge. You agree to:</p>
                <p>(a) provide only current, accurate and complete information about yourself in the course of registering to use the services (such information being the "Registration Information"); and</p> 
                <p>(b) maintain and promptly update the Registration Information, as necessary, to keep it true, accurate, current and complete at all times. If you provide any information that is untrue, inaccurate, not current or incomplete, or we have grounds to suspect that such information is untrue, inaccurate, not current or incomplete, we may suspend or terminate your account and refuse any and all current or future use of the services (or any portion thereof) and you will not have any cause of action against us for such suspension or termination of your account.</p>
                <p>1.2 You represent that you are the legal owner of, and that you are authorized to provide us with, all Registration Information, account information and other information necessary to facilitate your use of the services. For registration of your business, you are permitted to apply and enroll only if you represent a legitimate business and have the authority to enter into these Terms and Condition on behalf of the business. You represent and warrant that you are duly authorized by the business entity to accept this Terms and Conditions and have the authority to bind such business entity to the terms and conditions given in this Terms and Conditions. Further, you represent and warrant that the business entity will continue its acceptance through the then authorized signatories, the terms of this Terms and Conditions as may be modified from time to time as long as such business entity continues using the services.</p>
                <p>1.3 You further represent and warrant that the business entity has all the requisite consents, approvals, certificates, agreements, registrations and licenses in accordance with the laws, regulations, rules and guidelines in force in India from time to time.</p>
                <p></p>
                <p><strong>2. ELIGIBILITY REQUIREMENT FOR OPENING A FREECHARGE ACCOUNT</strong></p>
                <p>2.1  By creating/opening a Freecharge Account, you represent and confirm that you are<br/>
                  (a) 18 (eighteen) years of age or older; <br/>
                  (b) not 'incompetent to contract' within the meaning of the Indian Contract Act, 1972;  and <br/>
                  (c) entering into and performing this Terms and Conditions, as per applicable law.
                </p>
                <p>2.2  You further represent and confirm that you are not a person debarred from using the Freecharge sites and/or receiving the services under the laws of India or other applicable laws.
                </p>
                <p>2.3  Your right to access and use the sites and the services is personal to you and is not transferable by you to any other person or entity. You are only entitled to access and use the sites and services for lawful purposes.
                </p>
                <p></p>
                <p><strong>3. USER NAME AND PASSWORD</strong></p>
                <p>3.1  As part of the Freecharge registration process, you will create a username and password for your Freecharge Account. You must choose a reasonably descriptive username that clearly identifies you or your business. In addition, your password should not contain any details about you that is easily available or identifiable. You are responsible for maintaining the confidentiality of the password and the Freecharge Account. For security purposes, we recommended that you memorize your password and do not write it down. You agree not to disclose these credentials to any third party. Any person to whom you give your password will have full access to your payment information, and you assume all risk of loss resulting from any such access. All information and instructions received from your Freecharge Account will be deemed to have been authorized by you and the recipients of this information shall rely on its authenticity based on the use of your password. You will be responsible for all actions taken by anyone accessing the services using your username and password.</p>
                <p>3.2 In the event of any dispute between two or more parties as to ownership of a particular Freecharge Account, you agree that Freecharge will be the sole arbiter of such dispute. Freecharge's decision (which may include termination or suspension of any account subject to dispute) will be final and binding on all parties.</p>
                <p></p>
                <p><strong>4. VERIFICATIONS OF INFORMATION</strong></p>
                <p>4.1  We may share some or all of the information you provide with the Payment System Providers, Card Associations, issuing Banks, Partner Bank, Nodal Banks, Merchants/ Billers (defined hereinafter) and other statutory, regulatory and governmental authorities. By accepting these Terms and Conditions, you authorize Freecharge to request for supplemental documentation at any time (before or after your Freecharge Account has been activated), in order to verify your identity, the accuracy of the information provided. If we cannot verify that this information is accurate and complete, we may deny your use of the services, or close your Freecharge Account at any time. At any time, Freecharge and/or the Payment System Providers may conclude that you will not be permitted to use the services.</p>
                <p></p>
                <p><strong>5. OUR RELATIONSHIP WITH YOU</strong></p>
                <p>5.1  We provide online payment facilitation services. Through the Freecharge aggregate payment gateway services, we facilitate you in making payments to our registered merchants/sellers ("Merchants") for purchase of goods and services or to our registered billers ("Billers") for paying your bill using your debit card, credit card, net banking and any other acceptable modes of payment mechanism provided by us. These transactions are between the Merchants/Billers and you. We are only acting as an intermediary. We facilitate the collection of payments from you and facilitate the settlement of such payments to the respective Merchant/Biller. We are not involved in the clearing or payment of the transaction. In order to serve in this role, we have entered into agreements with various Nodal Banks, Payment System Providers, as defined under the Payment and Settlement Systems Act, 2007, Card Associations and other payment processing system providers, to enable use of internet payment gateways developed by/for them in order for them to effect payments between you and the Merchants/Billers and provide clearing, payment and settlement services with respect to your transaction. It is to be clarified that the payment instructions are authenticated, authorized and processed by the Card Associations and your issuing bank through the Payment System Provider's payment gateway and we does not have any role in the same.</p>
                <p>5.2  In addition, we provide value added services such as bill presentment, bill reminders, and dashboard for transparency of transactions, loyalty points and discounts etc. As a part of the service offerings, we also facilitate distribution and marketing of prepaid payment instruments by the name of Freecharge wallet/gift instruments issued by Axis Bank Limited ("Partner Bank")  (" Freecharge Balance"). Freecharge Balance is issued by Axis  Bank Limited and marketed and distributed by Freecharge on the sites. This  Freecharge Balance may only be used on the Freecharge site(s) to make payments for goods and services purchased from registered merchants or make bill payments to registered billers or for transfer of funds or for any such other reasons as mutually agreed from time to time. If you avail the services of the  Freecharge Balance, you agree and accept the terms and conditions provided on the website <a href="https://www.freecharge.com" target="Freecharge">www.freecharge.com </a></p>
                <p>5.3  Use of our services may be available through a compatible mobile device or other device, Internet and/or network access and may require software. You agree that you are solely responsible for these requirements, including any applicable changes, updates and fees as well as the terms of your agreement with your mobile device and telecommunications provider.</p>
                <p>5.4  FREECHARGE MAKES NO WARRANTIES OR REPRESENTATIONS OF ANY KIND, EXPRESS, STATUTORY OR IMPLIED AS TO: <br/>
                  (i) THE AVAILABILITY OF TELECOMMUNICATION SERVICES FROM YOUR PROVIDER AND ACCESS TO THE SERVICES AT ANY TIME OR FROM ANY LOCATION; <br/>
                  (ii) ANY LOSS, DAMAGE, OR OTHER SECURITY INTRUSION OF THE TELECOMMUNICATION SERVICES; AND <br/>
                  (iii) ANY DISCLOSURE OF INFORMATION TO THIRD PARTIES OR FAILURE TO TRANSMIT ANY DATA, COMMUNICATIONS OR SETTINGS CONNECTED WITH THE SERVICES.
                </p>
                <p></p>
                <p><strong>6. RIGHTS YOU GRANT TO US</strong></p>
                <p>6.1  By submitting information, data, passwords, user names, other log-in information, materials and other content to us, you are granting us the right to use that for the purpose of providing the services. We may use and store the content in accordance with this terms & conditions and our Privacy Policy, without any obligation by us to pay any fees or be subject to any restrictions or limitations. By using the services, you expressly authorize Freecharge to access your account information and payment information and submit the same further for processing your payments to any party to whom it is required to be submitted for the purpose of providing you services. We shall be acting as an intermediary, while providing you payment facilitation services. You agree that our role is limited to facilitating your payment instructions and providing other values added services.</p>
                <p></p>
                <p><strong>7. YOUR OBLIGATION TOWARDS PAYMENT SYSTEM PROVIDERS AND CARD ASSOCIATIONS</strong></p>
                <p>7.1  As you will be using the services of Payment System Providers and Card Associations to process your payment instructions, you consent and agree to comply with the rules, guidelines, directions, instructions, requests, etc. ("<strong>Guidelines</strong>") made by the Payment System Providers and Card Associations from time to time. Notwithstanding our assistance in understanding the Payment System Providers and Card Association Guidelines, you expressly acknowledge and agree that you are assuming the risk of compliance with all applicable Guidelines. You further acknowledge that the Payment System Providers, Card Associations and your issuing bank may also put limitations and restrictions on you, at its sole discretion. You are responsible for keeping yourself up -to- date and compliant with all such Guidelines. In addition, the Payment System Providers and Card Associations have the right to reject payments made by you for any reason whatsoever. If you fail to comply with your obligations towards the Payment System Providers, we may suspend or terminate your Freecharge Account.</p>
                <p><strong>8. REJECTION OF AUTHENTICATION AND AUTHORIZATION</strong></p>
                <p>8.1  You understand that the Payment System Providers and/or Card Association and/or your issuing bank may reject authentication and/or authorization of transaction placed by you for any reason including but not limited to insufficient funds, incorrect authentication details provided, expired card/bank account, risk management, suspicion of fraudulent, illegal or doubtful transactions, selling of banned items, use of compromised cards or bank account numbers, use of banned/blacklisted cards or bank account numbers, use of suspicious API or in accordance with the RBI, Acquiring Banks, Issuing Institution and/or Card Association rules, guidelines, regulations, etc. and any other laws, rules, regulations, guidelines in force in India. </p>
                <p>8.2  You further acknowledge that as a security measure we and/or the Payment System Providers may at our sole discretion, permanently or temporarily, block any card number, account numbers, group of cards or transactions from any specific blocked or blacklisted cards /, accounts, specific, group of IP addresses, devices, geographic locations and / or any such risk mitigation measures it wishes to undertake.</p>
                <p>8.3  As a risk management tool, we and/or the Payment System Providers reserve the right to limit or restrict transaction size, amount and/or monthly volume at any time. We will consider a variety of factors in making a decision and such determination will be at our sole discretion.</p>
                <p></p>
                <p><strong>9. TRANSACTION CONFIRMATION AND ACCOUNT HISTORY</strong></p>
                <p>9.1  When your payment instructions are successfully processed with respect to a transaction, we will update your Freecharge Account activity and provide you with a transaction confirmation. This confirmation will serve as your receipt. You acknowledges that we will only release the transaction confirmation upon receiving confirmation with respect to the authentication, authorization and processing of such Transaction (your bank account or debit or credit card being debited or charged) from the Payment System Provider ("<strong>Transaction Confirmation</strong>").We will not be responsible for any transactions that have not been confirmed to us by the Payment System Providers. </p>
                <p>9.2 The summary of your transaction history is available on your Freecharge Account dashboard. Except as required by law, you are solely responsible for<br/>
                  (a) compiling and retaining permanent records of all transactions and other data and<br/>
                  (b) reconciling all transaction information that is associated with your Freecharge Account. If you believe that there is an error or unauthorized Transaction activity that is associated with your Freecharge Account, you agree to contact us at <a href="mailto:care@freecharge.in">care@freecharge.in </a> immediately not later than 5 (five) days. 
                </p>
                <p></p>
                <p><strong>10. RISK MONITORING; UNUSUAL OR SUSPICIOUS TRANSACTIONS</strong></p>
                <p>10.1 In an effort to manage our risk, we may monitor your transactions and processing activity for high-risk practices or for fraudulent transactions. We may also engage third-party service providers to assist in these efforts and other elements of the service. If we believe there is suspicious or unusual activity, we may temporarily or permanently suspend your access to the Service. Suspicious or unusual activity includes, but is not limited to changes in your average transaction amount or processing pattern or use of different payment accounts.</p>
                <p></p>
                <p><strong>11. ADDITIONAL CLAUSES FOR BILL PAYMENT SERVICES</strong></p>
                <p>11.1 Description and Fees of Bill Payment Service: We provide you with bill payment services that enables you to make payments ("<strong>Bill Payment Services</strong>") to Billers. To be eligible to use the Bill Payment Services, you will need to provide us with your account information with Billers you choose (such as customer account number/ consumer identification number/code) and any information necessary for us to access your accounts with such Billers ("<strong>Bill Payment Account Information</strong>"). By using the Bill Payment Services, you expressly authorize us to store, use and access the Bill Payment Account Information, on your behalf for the purpose of providing Bill Payment Services to you. You may access information about your bill payment requests by logging into your Freecharge Account.</p>
                <p>11.2 Accuracy of Information: You understand and acknowledge that you are responsible for the accuracy of the information you provide about each payment request made by you, including the Bill Payment Account Information, telephone number, the amount of the transaction, etc.</p>
                <p>11.3 Scheduling your payments: Payment posting times will vary depending upon the Biller you  select and actual posting of payments and may take between 48 to 72 hours from the time the payment is made by you. When we receive a payment instruction, you authorize us to send instruction to the Payment System Provider's to debit your card/ bank account/prepaid account/card and remit payments to Billers on your behalf. However, since we have no control over when a Biller will actually process your payment after receipt, we are not responsible if a Biller credits a payment at a later date or denies acceptance of your payment. Further, if your payment due date falls on a weekend or holiday, you must submit the payment sufficiently in advance to include time for processing of the payment. It is solely your responsibility to submit payments so they arrive by the Biller's due date. Payments are subject to the policies and procedures of Billers. We are not liable (including for any penalties or late charges) for late or delayed posting of payments against your billing account either due to processing delays by the Biller or otherwise. You are responsible for paying any and all late charges and penalties imposed by a Biller.</p>
                <p>11.4 Payment history:  You may access information about your bill payment requests through your Freecharge Account. You should also confirm your bill payment requests against any statements or payment records provided by the Biller and your payment source.</p>
                <p></p>
                <p><strong>12. SECURITY PROCEDURES</strong></p>
                <p>12.1 Unauthorized access to your financial information: To help prevent unauthorized access to your personal financial information, You agree to:<br/>
                  (a) maintain the security of your account by not sharing your password with others and restricting access to your account on your computer or mobile or other device;<br/>
                  (b) ensure that you logout from your Freecharge Account each and every time you use it;<br/>
                  (c) ensure that you have a lock such as a pattern lock, password protection, etc. on your device and not leave your computer or mobile or other device unattended or unlocked; and<br/>
                  (d) take responsibility for all activities that occur under your Freecharge Account and accept all risks of unauthorized access. If you believe your password or devices has been lost or stolen, or if you suspect any fraudulent activity, please  report this to us immediately at <a href="mailto:care@freecharge.com ">care@freecharge.com  </a>  
                </p>
                <p>12.2 You are solely responsible for any unauthorized access to your personal or financial information that result from your failure to properly follow the Security Procedure detailed in these Terms and Conditions. Freecharge will not be liable for any loss, damage or other liability arising from your failure to comply with the terms and conditions herein or from any unauthorized access to or use of your Freecharge Account. We will also have no liability and you agree to assume all risk of loss that arises out of or relates to any loss or theft of your device or any information contained within any device.</p>
                <p>12.3 Risks: The use of a device such as mobiles, tablets and laptops involves risks as also does the usage of internet. For example, sensitive personal information or financial data that is transmitted from or to a device or internet may be intercepted and used by third parties without your knowledge. Viruses, spyware and other "malicious code" can also be downloaded to your devices without your knowledge. By using a device and internet to access the Sites, you assume all of those risks and agree that we will have no liability whatsoever to you for any loss or theft (including identity theft) occurring as a result of such risks. </p>
                <p>12.4 PCI DSS AND ISO Certification: We provide Payment Card Industry (PCI) Data Security Standard (DSS) certified services. We have implemented technical and organizational measures designed to secure your personal information from accidental loss and from unauthorized access, use, alteration or disclosure. However, we cannot guarantee that unauthorized third parties will never be able to defeat those measures or use your personal information for improper purposes. You acknowledge that you provide your personal information at your own risk. </p>
                <p>12.5 Encryption: Data transferred via the sites is encrypted in an effort to provide transmission security. Notwithstanding our efforts to ensure that the sites are secure, you acknowledge that mobile transmissions and the Internet are inherently insecure and that all data transfers, occur openly and potentially can be monitored and read by others. We cannot and do not warrant that all data transfers utilizing our sites will not be monitored or read by others. </p>
                <p>12.6 Additional Security Procedures: We may from time to time use reasonable additional or alternative procedures to ensure the security and confidentiality of your payment transactions through our sites. At all times you agree to comply with all security procedures we may impose in connection with your use of the sites. You agree that all security procedures we use are commercially reasonable, and to the extent allowed by law, you assume all risk of loss for unauthorized transactions where we have followed our then-current security procedures. </p>
                <p>12.7 Duty of Reasonable Care: We will exercise good faith and reasonable care in processing your transactions in accordance with these Terms and Conditions. You will similarly exercise good faith and reasonable care in:-<br/>
                  (i) using the sites,<br/>
                  (ii) observing and maintaining security procedures,<br/>
                  (iii) communicating with us, and<br/>
                  (iv) In reviewing your transaction records for any errors or discrepancies.<br/>
                </p>
                <p>12.8  Dropped calls, lost signals: If the internet connection to your device is interrupted or disconnected at any time before signing off, you must confirm that your payment requests have been received by us. We are not responsible for disconnections or interruptions in service or for failing to complete any payment request that we have not received as a result of any disconnection or interruption of the internet connection on your device. </p>
                <p>12.9  Compatibility: The protocol that we use may be different from that used by your device or internet carrier. We make no representations or warranties that your device or any carrier will be compatible with our system requirements or otherwise allow you to make payment requests using the Sites.</p>
                <p></p>
                <p><strong>13. DISCLAIMER REGARDING SELLERS/MERCHANT,  THEIR PRODUCTS AND RELATED THIRD PARTIES</strong></p>
                <p>13.1 All obligations with respect to the delivery of goods and services and/or acknowledgement of payment will be solely that of the Merchant/Biller and not of Freecharge. Notwithstanding the dispute resolution assistance provided by Freecharge, all disputes regarding quality, merchantability, non-delivery, and delay in delivery or otherwise will be directly between you and the Merchant/Biller without making us and/or the Payment System Providers, a party to such disputes. </p>
                <p>13.2 We make no representations or guarantees regarding Merchants/Billers utilizing our services. Use of our services in no way represents any endorsement by us of a Merchants/Billers existence, legitimacy/legality, ability, policies, practices, beliefs as well as the Merchants/Billers goods and services or reliability. The Merchant alone will be responsible to you and we will not have any responsibility or liability towards you in this respect. </p>
                <p>13.3 The relationship between us and the Merchants/Billers is on principal-to-principal basis. We have no connection or interest of whatsoever nature in the business of the Merchants/Billers or the goods/services offered / marketed by the Merchant/Biller. We do not in any manner take part in their business, directly or indirectly and are nowhere concerned or connected to the revenue of the Merchants/Billers. We will only provide payment aggregator services to the Merchant/Buyer in our capacity as an intermediary. For the use of our services, the Seller/Merchant pays us Fees as posted on our website and we nowhere connected or concerned about the revenues of the Seller/Mercahnt or the Payment System Providers. </p>
                <p>13.4 Further it is not our responsibility to monitor in any manner the use of the payment mechanisms by you for purchasing goods and services from the Merchant or aiming your bill payment to the Billers. You are using the payment mechanisms at your sole option and risks.</p>
                <p></p>
                <p><strong>14. LIMITED LICENSE TO USE THE SERVICES</strong></p>
                <p>14.1 We grant you a personal, limited, non-exclusive, revocable, non-transferable license, without the right to sublicense or assign, to electronically access and use the services solely to access and use the services for their intended purpose of enabling you to instruct a payment transaction, view your Freecharge Account and use other valued added services. You will be entitled to download updates to the services, subject to any additional terms made known to you at the time, when we makes these updates available. All other uses are prohibited.
                </p> 
                <p>14.2 While we want you to enjoy the services subject to these Terms and Conditions, you should not yourself or permit any third party to do any of the following: <br/>
                  (a) access or monitor any material or information on our system using any manual process or robot, spider, scraper, or other automated means unless you have separately executed a written agreement with us referencing this clause that expressly grants you an exception to this prohibition;<br/>
                  (b) copy, reproduce, alter, modify, dismantle, create derivative works, publicly display, republish, upload, post, transmit, resell or distribute in any way material or information from us;<br/>
                  (c) transfer any rights granted to you under these Terms and Conditions;<br/>
                  (d) violate the restrictions in any robot exclusion headers on the services, work around, bypass, or circumvent any of the technical limitations of the services, use any tool to enable features or functionalities that are otherwise disabled in the services, or decompile, disassemble, decode or attempt to decode or reverse engineer the services or to in any way override or break down any protection system integrated into the services;<br/>
                  (e) Post or transmit any file which contains viruses, worms, Trojan horses or any other contaminating or destructive features, or that otherwise interfere with the proper working of the services;<br/>
                  (f) perform or attempt to perform any actions that would interfere with the proper working of the services, prevent access to or use of the services by us to others, or impose an unreasonable or disproportionately large load on our infrastructure;<br/>
                  (g) create a derivative software program;<br/>
                  (h) any resale or commercial use of the services such as permit any third party to use and benefit from the Service via a rental, lease, timesharing, service bureau or other arrangement;<br/>
                  (i) the distribution, public performance or public display of the services or any materials therein;<br/>
                  (j) downloading (other than page caching) of any portion of the services or any information contained therein, except as expressly permitted by us; or<br/>
                  (k) Otherwise use the services except as expressly allowed under this Terms and Conditions.
                </p>
                <p>14.3 Any use of the services other than as specifically authorized herein may result in, among other things, termination or suspension of your right to use the services. Such unauthorized use may also violate applicable laws, including without limitation copyright and trademark laws and applicable communications regulations and statutes. Unless explicitly stated herein, nothing in these Terms and Conditions shall be construed as conferring any license to intellectual property rights, whether by estoppel, implication or otherwise. This license is revocable at any time, and will continue until such time as you cease to use or access the Services or your use or access to the Services is terminated by us.</p>
                <p></p>
                <p><strong>15. OWNERSHIP AND INTELLECTUAL PROPERTY RIGHTS</strong></p>
                <p>15.1 The service are protected by copyright, trademarks, patents, trade secret and/or other intellectual property laws. We own the title, copyright and other worldwide intellectual property rights in the services and all copies of the services. In addition, this Terms and Conditions does not grant you any rights to the intellectual property rights in the services; </p>
                <p>15.2 In addition, "Freecharge", "Freecharge.in", " Freecharge Application", and any other name, brand name, logo, wordmark, trademark, service marks, slogan of Freecharge are trademarks of Freecharge or of our affiliates, group company, service providers, etc. and shall  not be copied, imitated or used, in whole or in part, without our prior written permission or the applicable trademark holder. </p>
                <p>15.3 You shall  not use any metatags or any other "hidden text" utilizing "Freecharge" or any other name, trademark or product or service name of Freecharge without our prior written permission. In addition, the look and feel of the services, including all page headers, custom graphics, button icons and scripts, the service mark, user interface, trademark and/or trade dress of Freecharge and may not be copied, imitated or used, in whole or in part, without our prior written permission. </p>
                <p>15.4 Nothing contained herein authorizes you to use or in any manner exploit any intellectual property rights of ours or our affiliates, group companies, service providers, the Payment System Providers, without our prior written consent, expect where consent is already provided under these Terms and Conditions.</p>
                <p>15.5 You may choose to, or we may invite you to, submit comments or ideas about the Services, including without limitation about how to improve the Service or our products. By submitting any idea, you agree that your disclosure is gratuitous, unsolicited and without restriction and will not place us under any fiduciary or other obligation, and that we are free to use the idea without any additional compensation to you, and/or to disclose the idea on a non-confidential basis or otherwise to anyone. You further acknowledge that, by acceptance of your submission, we do not waive any rights to use similar or related ideas previously known to us, or developed by our employees, or obtained from sources other than you.</p>
                <p></p>
                <p><strong>16. COUPON REDEMPTION</strong></p>
                <p>16.1 Coupon redemption is purely subjected to standard and specified terms and conditions mentioned by the respective issuer of the coupon. Coupons are issued on behalf of the respective issuer of coupons. We cannot be held liable for any damages, injuries, losses incurred by the end you by use / non-use of such coupon.</p>
                <p></p>
                <p><strong>17. OFFERS, DISCOUNTS, CASHBACK, USER PROGRAMS</strong></p>
                <p>17.1 If you avail any offers, discounts, cashbacks, coupons and you programs, etc. ("Offers") provided by us or our partner, you are agreeing to comply with and accept the terms and conditions provided in respect of such Offers. Further, you understand that all terms and conditions provided with respect to any Offer shall form part of this Terms and Conditions and shall be read in conjunction with this Terms and Conditions.</p>
                <p></p>
                <p><strong>18. ONLINE AND MOBILE ALERTS AND COMMUNICATION POLICY</strong></p>
                <p>18.1 We may from time to time provide automatic alerts and voluntary Freecharge Account or payment related alerts. Further automatic alerts may be sent to you following certain changes to your Freecharge Account or information, such as a change in your Registration Information. </p>
                <p>18.2 Voluntary account alerts may be turned on by default as part of the Services. We may add new alerts from time to time, or cease to provide certain alerts at any time upon our sole discretion. Each alert has different options available, and you may be asked to select from among these options. </p>
                <p>18.3 By accepting the terms and conditions and/or using the services, you accept that we may send the alerts to your registered mobile phone number and/or registered email id. You acknowledge that the alerts will be received only if the mobile phone is in 'On' mode to receive the SMS. If the mobile phone is in 'Off' mode then you may not get / get after delay any alerts sent during such period.</p>
                <p>18.4 Electronic alerts will be sent to the email address provided by you as your primary email address for the services. If your email address changes, you are responsible for informing us of that change. You can also choose to have alerts sent to a mobile device that accepts text messages. Changes to your email address or mobile number will apply to all of your alerts.</p> 
                <p>18.5 We will make best efforts to provide the Service and it shall be deemed that you shall have received the information sent from us as an alert on your registered mobile phone number or email id and we shall not be under any obligation to confirm the authenticity of the person(s) receiving the alert. You cannot hold us liable for non-availability of the service in any manner whatsoever.</p> 
                <p>18.6 You acknowledge that the SMS service or email service provided by us is an additional facility provided for your convenience and that it may be susceptible to error, omission and/ or inaccuracy. In the event you observe any error in the information provided in the alert, you shall immediately inform us about the same. We will make best possible efforts to rectify the error as early as possible.</p> 
                <p>18.7 You further acknowledge that the clarity, readability and promptness of providing the service depend on many factors including the infrastructure, connectivity of the service provider. We shall not be responsible for any non-delivery, delayed delivery or distortion of the alert or for any errors in the content of an alert; or for any actions taken or not taken by you or any third party in reliance on an alert in any way whatsoever. Because alerts are not encrypted, we will never include your passcode. However, alerts may include your user name and some information about your Freecharge Accounts. Depending upon which alerts you select, information such as a Freecharge Account balance or the due date for your bill payment may be included. Anyone with access to your email will be able to view the content of these alerts.</p>
                <p>18.8 You agree to indemnify and hold us harmless and the SMS/email service provider including its officials from any damages, claims, demands, proceedings, loss, cost, charges and expenses whatsoever including legal charges and attorney fees which we and the SMS/email service provider may at any time incur, sustain, suffer or be put to as a consequence of or arising out of: <br/>
                  (i) misuse, improper or fraudulent information provided by you, and/or <br/>
                (ii) you providing incorrect number or providing a number that belongs to that of an unrelated third party. </p>
                <p>18.9 You also agree to receive information regarding what we perceive to be of your interest based on your usage history via SMS, email & phone call. This includes offers, discounts and general information.</p>
                <p>18.10  By accepting the terms and conditions you acknowledge and agree that Freecharge may call the mobile phone number provided by you, while registering for the Service or to any such number replaced and informed by you, for the purpose of collecting feedback from you regarding the Services. </p>
                <p>18.11  Grievances and claims related to Services should be reported to Freecharge Care Team in the manner provided on the website.</p>
                <p></p>
                <p><strong>19. OUR FEES</strong></p>
                <p>19.1 You understand that you may be charged a fee with respect to your use of the services. Further, we may also charge a fee with respect to certain optional valued added services that you elect to use. The amount of the fee will be displayed to you on the payment page. You agree to pay all such fees and charges as may be applicable to the services you use and authorize us to add the fee to the amount of the bill payment you request or bill you separately for such fees. We agree and accept that reserve the right to change the fee structure from time to time.</p>
                <p></p>
                <p><strong>20. DORMANT ACCOUNTS</strong></p>
                <p>20.1 If there is no activity in your Freecharge Account (including access or payment transactions) for a period of six (6) months, we may close your Freecharge Account.</p>
                <p></p>
                <p><strong>21. PRIVACY</strong></p>
                <p>21.1 Your privacy is very important to us. Upon acceptance of these Terms and Conditions, you confirm that you have read, understood and accepted our Privacy Policy.</p>
                <p></p>
                <p><strong>22. TERMINATION</strong></p>
                <p>22.1 Our Right: We reserve the right, without notice and in its sole discretion, to terminate your access to, and use of, of the services and to block or prevent your future access to, and use of, the services. Upon termination, you must uninstall any copies of the services from your computer and/or mobile or other devices. We will not be liable to you for the termination of the services or for any consequence arising out of termination of the services. </p>
                <p>22.2 Your Right: You may terminate this Terms and Conditions by closing your Freecharge Account at any time. Upon closure of a Freecharge Account, any pending transactions will be cancelled. </p>
                <p>22.3 Force Majeure: We will have to the option to suspend or terminate this Terms and Conditions and the services with immediate effect on the occurrence of a force majeure event.</p>
                <p></p>
                <p><strong>23. EFFECT OF TERMINATION</strong></p>
                <p>23.1 We will not be liable to you for compensation, reimbursement, or damages in connection with your use of the services, or any termination or suspension of the services. Any termination of this Terms and Conditions does not relieve you of any obligations to pay any fees or costs accrued prior to the termination and any other amounts owed by you to us, and/or the Payment System Providers as provided in this Terms and Conditions.</p>
                <p></p>
                <p><strong>24. DISCLAIMER</strong></p>
                <p>24.1 The services are provided on an "as is" and "as available" basis. Use of the service is at your own risk. To the maximum extent permitted by applicable law, the services is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, or non-infringement. </p>
                <p>24.2 Without limiting the foregoing, we, Payment System Providers, and its processors, (and their respective subsidiaries, affiliates, agents, directors, and employees) do not warrant that the content is accurate, reliable, correct or error free; that the services will meet your requirements; that the services and the Payment System Providers' payment mechanism will be available at any particular time or location, uninterrupted or secure; that any defects or errors will be corrected; or that the services is free of viruses or other harmful components; that the hyperlinks will function properly at all times; at any particular time, that information on the services will at all times or at any particular time be correct, complete or timely; or that all portions of the services are secure. We further make no warranty with regards to the products or services that are paid for with the services or the time or date in which scheduled payments are completed with the services.</p>
                <p>24.3 Any content downloaded or otherwise obtained through the use of the services is downloaded at your own risk, and you will be solely responsible for any damage to your property or loss of data that results from such download. No advice or information, whether oral or written, obtained by you from us or through the services will create any warranty not expressly stated herein.</p>
                <p>24.4 We will not be liable to you for any loss or damage whatsoever or howsoever caused or arising, directly or indirectly, including without limitation, as a result of loss of data; interruption or stoppage of access to payment gateway; interruption or stoppage Freecharge Sites; non-availability of connectivity links/hyperlinks.</p>
                <p>24.5 Our sole obligation and your sole and exclusive remedy in the event of interruption in services or loss of use and/or access to services, will be to use all reasonable endeavours to restore the Services and/or access as soon as reasonably possible.</p>
                <p>24.6 The services are controlled and operated from and in India. We make no representations that the services are appropriate or available for use in other locations. Those who access or use the services from other jurisdictions do so at their own volition and are entirely responsible for compliance with all applicable Indian laws, rules, regulations, guidelines including but not limited to export and import regulations and RBI regulations. You should not use the Service if you are a resident of a country embargoed by the India, or are a foreign person or entity blocked or denied by the Indian government. Unless otherwise explicitly stated, all materials found on the services are solely directed to individuals, companies, or other entities located in India.</p>
                <p></p>
                <p><strong>25. INDEMNIFICATION</strong></p>
                <p>25.1 You agree to defend, indemnify and hold harmless Freecharge, group company, Partner Bank, our independent contractors, Payment System Providers and our directors, employees and agents, from and against any claims, damages, costs, liabilities and expenses (including, but not limited to, reasonable attorneys' fees) arising out of or related to<br/>
                  (a) any actual or alleged breach of these Terms and Conditions or any other terms, policies or guidelines provided by us, Payment System Providers or Card Associations;<br/>
                  (b) any actual or alleged violation of applicable laws or rules of any payment card association, network or company;<br/>
                  (c) your wrongful or improper use of the services; or (d) your violation of the rights of any third party.
                </p>
                <p></p>
                <p><strong>26. LIMITATION OF LIABILITY</strong></p>   
                <p>26.1 In no event shall we, our directors, employees or agents, be liable to you for any direct, indirect, incidental, consequential, punitive, special or exemplary damages or for any damages of any kind, including but not limited to loss of use, loss of profits or loss of data, whether in an action in contract, tort (including but not limited to negligence) or otherwise, arising out of or in any way connected with the use or inability to use the services, including without limitation any damages caused by or resulting from reliance by you on any information obtained from us or that result from mistakes, omissions, interruptions, deletion of files, errors, defects, viruses, delays in operation or transmission or any failure of performance, whether or not resulting from acts of god, communications failure, theft, destruction or unauthorized access to our records, programs or services.</p>
                <p>26.2 In no event shall our aggregate liability, whether in contract, warranty, tort (including negligence, whether active, passive or imputed), product liability, strict liability or other theory, arising out of or relating to the use or inability to use the services exceed any fee, you pay to us, if any, to access or use the services. In addition, we shall not be liable for errors you make in using the Service, including the following:</p>
                <ul>
                  <li>you erroneously direct us to submit a payment instructions to a Merchant/Biller multiple times;</li>
                  <li>you direct us to submit the wrong amount to a Merchant/Biller;</li>
                  <li>you direct us to submit a payment instruction for a wrong Merchant/Biller; or</li>
                  <li>you provide us with incorrect or incomplete information.</li>
                </ul>
                <p>Any of the above issue(s) needs to be taken up directly with the Merchant/Biller.</p>
                <p>26.3 You understand and acknowledge that we do not have control of, or liability for, the goods or services that are paid for via the Service.</p>
                <p>26.4 Your liability: Except as otherwise provided by law, you will be liable for any loss or damage resulting from your breach of this Terms and Conditions or you negligence, or which resulted from unauthorized, fraudulent, or dishonest acts by others (other than us). You are liable for all payments that you make or which are made or requested under your Freecharge Account, even if that payment is unauthorized.</p>
                <p>26.5 Some jurisdictions do not allow limitations on implied warranties or the exclusion or limitation of certain damages. If these laws apply, some or all of the above disclaimers, exclusions or limitations may not apply. You agree that, if you are dissatisfied with the services or any portion thereof, your exclusive remedy shall be to stop using the services.</p>
                <p><strong>27. GOVERNING LAW AND JURISDICTION</strong></p>    
                <p>27.1 This Terms and Conditions will be governed by and construed in accordance with the laws of India. You agree that any legal action or proceedings arising out of this Terms and Conditions may be brought exclusively in the competent courts/tribunals having jurisdiction in New Delhi in India and irrevocably submit themselves to the jurisdiction of such courts / tribunals.</p>
                <p></p>
                <p><strong>28. RIGHT TO AMEND OR MODIFY</strong></p>
                <p>28.1 We reserve the right at any time and from time to time to modify or discontinue, temporarily or permanently, the sites or services with or without notice. We may also change the services, including applicable fees, in our sole discretion. If you do not agree to the changes, you may stop using the services. Your use of the services, after implementation of the change(s) will constitute your agreement to such change(s). You agree that we shall not be liable to you or to any third party for any modification, suspensions, or discontinuance of the services.</p>
                <p>28.2 We may modify these Terms and Conditions from time to time. Any and all changes to this Terms and Conditions may be provided to you by electronic means (i.e., via email or by posting the information on the sites). In addition, the Terms and Conditions t will always indicate the date it was last revised. You are deemed to accept and agree to be bound by any changes to the Terms and Conditions when you use the services after those changes are posted.</p>
                <p></p>
                <p><strong> 29. THIRD PARTY SERVICES AND LINKS TO OTHER WEB SITES</strong></p>
                <p>29.1 We may display third party content or advertisements through the services and may provide links that will take you to web pages and content of third parties that are not under our control (collectively, "Third Party Content"). We make no representation, warranty, promise or guarantee whatsoever concerning any aspect of any Third Party Content, including without limitation regarding its accuracy or completeness. You agree that your use of all Third Party Content is entirely at your own risk. We provide Third Party Content solely as a convenience to you, and the provision of such a link is not an endorsement by us of any aspect of the Third Party Content. You acknowledge and agree that we are not responsible or liable in any manner for any Third Party Content and undertake no responsibility to update or review any Third Party Content. Please remember that when you use a link to go from our website to another website, our Privacy Policy is no longer in effect. If you browse and interact on any other website, including those that have a link on our website, this will be subject to that website's own rules and policies.</p>
                <p></p>
                <p><strong>30. ASSIGNMENT</strong>
                  <p>30.1 These Terms and Conditions, and any rights and licenses granted hereunder, may not be transferred or assigned by you. We may assign, in whole or in part, the benefits or obligations of this Terms and Conditions. We will provide an intimation of such assignment to you, which will be binding on the parties to these Terms and Conditions.
                  </p>
                  <p></p>
                  <p> <strong>31. FORCE MAJEURE</strong></p>
                  <p>31.1 We will not be liable for failure to perform under this Terms and Conditions as a result of any event of force majeure like acts of god, fire, wars, sabotage, civil unrest, labour unrest, action of statutory authorities or local or state, central governments, change in laws, rules and regulations, affecting our and/or the Payment System Providers performance.</p>
                  <p></p>
                  <p><strong>32. WAIVER</strong></p>  
                  <p>32.1 Unless otherwise expressly stated in this Terms and Conditions, the failure to exercise or delay in exercising a right or remedy under these Terms and Conditions will not constitute a waiver of the right or remedy or a waiver of any other rights or remedies, and no single or partial exercise of any right or remedy under these Terms and Conditions will prevent any further exercise of the right or remedy or the exercise of any other right or remedy.</p>
                  <p></p>
                  <p><strong>33. SURVIVAL OF PROVISIONS</strong></p>
                  <p>33.1 The terms and provisions of these Terms and Conditions that by their nature and content are intended to survive the performance hereof by any or all parties hereto will so survive the completion and termination of this Terms and Conditions.</p>
                  <p></p>
                  <p><strong>34. SEVERABILITY</strong></p>
                  <p>34.1 If any provision of these Terms and Conditions is or becomes, in whole or in part, invalid or unenforceable but would be valid or enforceable if some part of that provision was deleted, that provision will apply with such deletions as may be necessary to make it valid. If any court/tribunal of competent jurisdiction holds any of the provisions of these Terms and Conditions unlawful or otherwise ineffective, the remainder of these Terms and Conditions will remain in full force and the unlawful or otherwise ineffective provision will be substituted by a new provision reflecting the intent of the provision so substituted.
                  </p>
                  <p></p>
                  <p><strong>35. NON-EXCLUSIVITY</strong></p>
                  <p>35.1 It is agreed and clarified that this Terms and Conditions is on a non-exclusive basis and the Parties are at liberty to enter into similar terms and agreements with others.</p>
                  <p></p>
                  <p><strong>36. NOTICES</strong></p>
                  <p>36.1 All notices, requests, demands, waivers and other communications required or permitted to be given under these Terms and Conditions will be in writing to be sent to the following addresses:</p>
                  <p>
                    <strong>For Freecharge:</strong><br/> 
                    Freecharge Payment Technologies Private Limited <br/>
                    DLF Cyber Green, 16th Floor, Tower C, <br/>
                    DLF Cyber City, DLF Phase-3, Gurugram, Haryana- 122002<br/>
                    care@freecharge.in  
                  </p>
                  <p></p>
                  <p>
                    <strong>For you: </strong><br/>
                    To the name and address provider for at the time of registration.<br/>
                    Or, in each case, at such other address as may be specified in writing to the other parties in accordance with the requirements of this clause. All such notices, requests, demands, waivers and other communications will be deemed duly given<br/>
                  </p>
                  <ul>
                    <li>if by personal delivery, on the day after such delivery, </li>
                    <li>if by certified or registered mail, on the second  day after the mailing thereof, </li>
                    <li>if by courier service or similar service, on the day delivered, or </li>
                    <li>if emailed, on the day following the day on which such email was sent, provided that a copy is also sent by registered mail.</li>
                  </ul>
                </div>
              </li>        
              <li class="margBottom">
          <h3 class="cbp-nttrigger">
                 Terms and Conditions for FreeCharge balance account issued by Axis Bank Limited
          </h3>
          <div class="cbp-ntcontent">
            <p>Last Updated on February 28, 2018</p>
            <p>
              THIS DOCUMENT IS PUBLISHED IN COMPLIANCE OF AND SHALL BE GOVERNED BY INDIAN LAW, INCLUDING BUT NOT LIMITED TO (I) THE INDIAN CONTRACT ACT, 1872; (II) THE INFORMATION TECHNOLOGY ACT, 2000, THE RULES, REGULATIONS, GUIDELINES AND CLARIFICATIONS FRAMED THEREUNDER; (III) THE PAYMENT AND SETTLEMENT SYSTEMS ACT, 2007 AND APPLICABLE RULES, REGULATIONS AND GUIDELINES MADE THEREUNDER; AND (IV) RESERVE BANK OF INDIA ACT, 1934 AND THE APPLICABLE RULES, REGULATIONS AND GUIDELINES MADE THEREUNDER FOR THE BANK FOR ISSUANCE AND OPERATION OF PRE-PAID PAYMENT INSTRUMENT AND FOR MONEY TRANSFER.
            </p>
            <p>
              THIS DOCUMENT IS DISCLOSED IN COMPLIANCE WITH SECTION 21 OF PAYMENT AND SETTLEMENT SYSTEMS ACT, 2007 AND AS REQUIRED TO BE DISCLOSED UNDER POLICY GUIDELINES ON ISSUANCE AND OPERATION OF PRE-PAID PAYMENT INSTRUMENT IN INDIA.
            </p>
            <p>
              THIS AGREEMENT IS A LEGALLY BINDING DOCUMENT BETWEEN YOU AND THE BANK (BOTH TERMS DEFINED BELOW). THE TERMS OF THIS DOCUMENT WILL BE EFFECTIVE UPON YOUR ISSUANCE OF PRE-PAID PAYMENT INSTRUMENT BY THE BANK OR YOUR USAGE OF PRE-PAID PAYMENT INSTRUMENT AND WILL GOVERN THE RELATIONSHIP BETWEEN YOU AND THE BANK FOR THE USE OF FREECHARGE BALANCE ACCOUNT AND SERVICES (DEFINED BELOW). THESE TERMS ARE IN ADDITION TO AND NOT IN DEROGATION WITH TERMS AND CONDITIONS OF ANY OTHER PRODUCT OR SERIVES OF THE BANK. Before Axis Bank Limited issues You the <strong>Freecharge Balance Account or before You use the Freecharge Balance Account issued by Axis Bank Limited</strong>, please read these TERMS & CONDITIONS carefully.
            </p>
            <p>
              By offering to acquire, accepting or using Freecharge Balance Account, You are unconditionally agreeing to the Terms and Conditions set out below with Axis Bank Limited and will be bound by them.<br/>
              By agreeing to these Terms and Conditions, You represent the following:
            </p>
            <ul>
              <li>
                You are 18 (eighteen) years old or older;
              </li>
              <li>
                Capable of entering into a legally binding agreement;
              </li>
              <li>
                You are an Indian resident;
              </li>
              <li>
                You have not been previously suspended or removed from the Prepaid Payment Account Services or Single-Sign-On Services;
              </li>
              <li>
                You have valid and subsisting right, authority, and capacity to enter into these Terms and Conditions and to abide by all of the terms and conditions contained herein; and
              </li>
              <li>
                You are not impersonating any person or entity, or falsely stating or otherwise misrepresenting identity, age or affiliation with any person or entity.
              </li>
            </ul>
            <p>
              All references in these Terms and Conditions to You or Your shall refer to the users or Customer of Website or Merchant Site(s). All references in these Terms and Conditions to We, Us, or Our shall refer to Freecharge, and their respective affiliates, as the case may be.
            </p>
            <p>
              <strong>1. DEFINITIONS: </strong>
              Unless the context requires otherwise the terms defined shall have the same meaning attributed to them herein:
            </p>
              <p>
                a) <strong>"Applicable Law(s)"</strong>
                shall mean all applicable central, state and local laws, statute, regulations, orders or directives as may be amended and in effect or re-enacted from time to time, order or other legislative action of any government authority to the extent having the force of law and in each case, any implementing regulation or interpretation issued thereunder including any successor Applicable Law.
              </p>
              <p>
                b) <strong>"Customer"</strong> shall mean the registered user of Freecharge account who also acquires Freecharge Balance Account from Axis Bank Limited.
              </p>
              <p>
                c) <strong>"Freecharge Account"</strong> shall mean the account created by the Customer by using the services provided by Freecharge, upon the Customer “signing in” or “signing up” for the purposes of entering into a Transaction with a Merchant or other Customer; which would give the Customer access inter alia to Freecharge Balance Account.
              </p>
              <p>
                d) <strong>"Freecharge Balance Account"</strong> or <strong>"Prepaid Payment Account"</strong> shall means a) a semi closed prepaid payment instrument, minimum KYC or full KYC as the case may be, issued by Axis Bank Limited and which is co-branded with Freecharge Payment Technologies Private Limited (“<strong>Freecharge</strong>”) in the form of a virtual payment account (“Freecharge Wallet”) and/or (b) prepaid gift instrument(s) issued by Axis Bank Limited in the form of a virtual payment account (“Freecharge Gift Instrument”),  which can be used on the Website or Merchants site(s) (a) to make payments for goods and services ("<strong>Products</strong>") purchased/availed through Merchant(s) or for transfer of funds from one Customer's Freecharge Wallet  to another Freecharge Wallet  if permissible under Applicable Law or  for any other purposes as may be specified from time to time by the Bank.
              </p>
              <p>
                e) <strong>"Freecharge"</strong> shall mean Freecharge Payment Technologies Private Limited, a private limited company incorporated in India under the Companies Act, 2013 and having its registered office at Ground Floor, 68, Okhla Industrial Estate, Phase III, New Delhi - 110020.
              </p>
              <p>
                f) <strong>"Freecharge Wallet"</strong> shall mean a semi closed prepaid payment instrument, minimum KYC or full KYC as the case may be, issued by Axis Bank Private Limited in the form of a virtual payment account.
              </p>
              <p>
                g) <strong>"Freecharge Gift Instrument"</strong> shall mean a prepaid gift instrument(s) issued by Axis Bank Limited in the form of a virtual payment account.
              </p>
              <p>
                h) <strong>"Merchant(s)"</strong> shall mean seller(s), retailers or e-commerce marketplace or such other business entities who accept Freecharge Balance Account as payment options for collecting payments from the Customer.
              </p>
              <p>
                i) <strong>"Mobile Application(s)"</strong> shall mean the software application(s) which inter alia, enables Freecharge account through the mobile-device.
              </p>
              <p>
                j) <strong>"Prepaid Payment Account Services"</strong> or <strong>"Services"</strong> shall mean the scheme under which Prepaid Payment Account is issued by the Bank and related services or functionalities as detailed under this Agreement enabling Customers to make Transaction.
              </p>
              <p>
                k) <strong>"RBI"</strong> shall mean Reserve Bank of India.
              </p>
              <p>
                l) <strong>"Single-Sign-On Services"</strong> shall mean the services provided by Freecharge to Customers through Freecharge account, or otherwise. For avoidance of doubt, the Freecharge Balance Account  is issued by Axis Bank Limited but can be accessed through Single-Sign-On Services.
              </p>             
            <p>
               m) <strong>"Terms and Conditions" shall mean these terms and conditions;</strong>
            </p>       
              
              <p>
                n) <strong>"Transaction"</strong> shall mean an online and offline purchase of Product; or payments for purchase of Products or for any other specified purpose to a Merchant; or to other Customer, or transfer of funds to any bank account, through Freecharge Balance Account or through any other modes as determined by the Bank from time to time and shall also include load/reload of Freecharge Balance Account.
              </p>
              <p>
                o) <strong>"Website"</strong> shall mean website having the domain https://www.freecharge.in or mobile site, Mobile Application(s), or online front end or back end interphases through which the Bank shall issue the Prepaid Payment Account after creation of Freecharge account.
              </p>
              <p>p) <strong>"Axis Bank Limited"</strong> or <strong>"The Bank"</strong> shall mean the Axis Bank Limited, a company incorporated under the Companies Act, 1956 and a banking company within the meaning of the Banking Regulation Act, 1949 and having its registered office at Axis Bank Limited, ‘Trishul’, 3rd Floor, Opp. Samartheshwar Temple, Near Law Garden, Ellisbridge, Ahmedabad - 380 006.</p>        
            <p>
              <strong>2. INTERPRETATIONS</strong>
            </p>
            <p>
              2.1. All references to singular include plural and vice versa and the word "includes" should be construed as "without limitation".
            </p>
            <p>
              2.2 Words importing any gender include the other gender.
            </p>
            <p>
              2.3 Reference to any statute, ordinance or other law includes all regulations and other instruments and all consolidations, amendments, re-enactments or replacements for the time being in force.
            </p>
            <p>
              2.4 All headings, bold typing and italics (if any) have been inserted for convenience of reference only and do not define limit or affect the meaning or interpretation of these terms.
            </p>
            <p>
              2.5. These Terms and Conditions shall be independent of the terms and conditions of Merchant(s)' site(s).
            </p>
            <p>
              2.6. An "authorisation" or "approval" includes an authorisation, consent, clearance, approval, permission, resolution, license, exemption, filing and registration.
            </p>
            <p>
              <strong>3.</strong>
              <strong> ISSUANCE AND REGISTRATION OF FREECHARGE BALANCE ACCOUNT / PREPAID PAYMENT ACCOUNT</strong>
            </p>
            <p>
              3.1. The Prepaid Payment Account will be issued by the Bank to Customer on the request of the Customer and pursuant to the Customer making an application for registration of Freecharge Balance Account and agreeing to these Terms and Conditions and other applicable terms and conditions in the manner prescribed by the Bank in this regard, provided the Customer has fulfilled the eligibility and Know Your Customer ("<strong>KYC</strong>") criteria and provided all relevant information and/or documentation request by the Bank.
            </p>
            <p>
              3.2. A Customer desirous of acquiring a Prepaid Payment Account should apply to the Bank using Website or using Merchant's Site as mentioned on the Website and accept these Terms and Conditions and other terms and conditions as may be specified.
            </p>
            <p>
              3.3 The Bank reserves the right to reject any application made for issuing a Prepaid Payment Account without assigning any reason.
            </p>
            <p>
              3.4 In order to acquire register, create and use a Prepaid Payment Account, the Bank may require You to submit certain personal information, such as Your name, mobile phone number, e-mail address, date of birth, preferred language of communication, etc as amended by the Bank from time to time and any other data as per Applicable Law ("<strong>Data</strong>"). You agree that the Data You provide to the Bank upon registration and at all other times will be true, accurate, current and complete. You shall immediately inform the Bank or its authorized service provider(s) about change in Data along with such proof of change.
            </p>
            <p>
              3.5 You hereby authorize the Bank, directly or through third parties, to make any inquiries that the Bank may consider necessary to validate Your identity and/or authenticate Your identity and Prepaid Payment Account information. This may include asking You for further information and/or documentation about Your account usage or identity or requiring You to confirm identification by furnishing KYC documentation, ownership of Your email address, telephone number or financial instruments, among others. This process is for internal verification purposes. You agree and understand that the Data as entered by You shall always be maintained by Bank's authorized service provider(s).
            </p>
            <p>
              3.6. The collection, verification, audit and maintenance of correct and updated Customer information is a continuous process and the Bank reserve the right, at any time, to take steps necessary to ensure compliance with all relevant and applicable KYC requirements.
            </p>
            <p>
              3.7 Notwithstanding anything contained herein, You shall complete Your KYC verification process through the e-KYC mode, as may be made available to You, You also agree to and accept the below terms:
            </p>
            <p>a. You hereby consent that the Bank may, through an authorized UIDAI regulated agency (“Agency”), authenticate ’Know Your Customer’ (KYC) information through biometric and/or One Time Password (OTP) based Adhaar authentication (“e-KYC”) and/or any other permitted means for the purpose of fulfilling the KYC requirements mandated by the RBI, in a manner as required under applicable laws. <br/>
              b. You hereby allow Bank to obtain from the Agency, and to store Your e-KYC for a period of 10 (ten) years from the date of termination of Your account with the Bank, or any other period as mandated under any applicable laws.<br/>
              c. You hereby declare that the details furnished by You are true, correct and complete to the best of Your knowledge. You will keep the Bank and Freecharge indemnified and harmless with regard to any inaccuracies, errors, discrepancies, etc. in the information provided, or in case of any incomplete information provided. <br/>
              d. You confirm that You are aware of and have read the laws applicable in relation to eKYC Data and that You are aware of Your rights and obligations thereunder and under all applicable laws. <br/>
              e. You agree that the Bank may also share Your information/ data for such purposes as may be mandated under Applicable Law or any order/request of a regulatory/ statutory authority or Court.
            </p>
            <p>3.8  The Bank reserves the right to discontinue the Services/ reject applications for Prepaid Payment Account Services at any time if there are discrepancies in information and/or documentation provided by You or if the information/documentation provided by You is found to be incorrect or wrong. In such an event, the Bank reserves the right to forfeit the balance therein to the extent and in accordance with Applicable Laws.</p>
            <p>
              4<strong>. EXTENT OF PREPAID PAYMENT ACCOUNT SERVICES</strong>
            </p>
            <p>
              4.1. The amount that can be transacted in the Prepaid Payment Account is governed by Applicable Laws including rules, regulations and guidelines laid down by RBI which include monthly limits, transaction limits and balance limits on the Prepaid Payment Account. Subject to any change in Applicable Law including guidelines/notifications issued by RBI from time to time the limitations stated hereunder may be reviewed and modified at the discretion of the Bank without prior intimation to the Customer:
            </p>
            <p>4.1.1  Freecharge Wallet:<br/>
              (A) Minimum KYC Freecharge Wallet Features:-
            </p>
            <table cellspacing="0" cellpadding="7" class="freeAxis" style="margin:15px 0px;">
              <tbody>
              <tr>
                <td valign="middle">
                  <p>
                    <strong>Sr No</strong>
                  </p>
                </td>
                <td valign="middle">
                  <p>
                    <strong>Feature</strong>
                  </p>
                </td>
                <td valign="middle">
                  <p>
                    <strong>Conditions</strong>
                  </p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>1</p>
                </td>
                <td valign="middle">
                  <p>Currency of Issue</p>
                </td>
                <td valign="middle">
                  <p>Indian Rupees</p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>2</p>
                </td>
                <td valign="middle">
                  <p>The maximum Outstanding Balance  </p>
                </td>
                <td valign="middle">
                  <p>The maximum outstanding balance must not exceed INR 10,000/- (Indian Rupees Ten Thousand Only) at any point in time</p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>3</p>
                </td>
                <td valign="middle">
                  <p>Loading or Re-loading funds</p>
                </td>
                <td valign="middle">
                  <p>The total amount loaded or reloaded in the prepaid payment account during any given month shall not exceed INR 10,000/- (Indian Rupees Ten Thousand Only) and the total amount loaded during the financial year shall not exceed INR 1,00,000/- (Indian Rupees One Lac Only) </p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>4</p>
                </td>
                <td valign="middle">
                  <p>Debit from prepaid payment account</p>
                </td>
                <td valign="middle">
                  <p>The total amount debited from the prepaid payment account during any given month shall not exceed INR 10,000/-(Indian Rupees Ten Thousand Only)</p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>5</p>
                </td>
                <td valign="middle">
                  <p>Cash withdrawal</p>
                </td>
                <td valign="middle">
                  <p>Cash withdrawal of any amount from prepaid payment account is prohibited under RBI regulations.</p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>6</p>
                </td>
                <td valign="middle">
                  <p>Validity period</p>
                </td>
                <td valign="middle">
                  <p>It shall have a minimum validity period of one year from the date of activation/issuance. </p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>7</p>
                </td>
                <td valign="middle">
                  <p>Mandatory conversion to Full KYC Freecharge Wallet </p>
                </td>
                <td valign="middle">
                  <p>Minimum KYC Freecharge Wallet shall be converted to Full KYC* Freecharge Wallet within a period of 12 (Twelve) months from the date of activation/issuance of the Minimum KYC Freecharge Wallet, failing which no further credit shall be allowed in such Minimum KYC Freecharge Wallet.</p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>8</p>
                </td>
                <td valign="middle">
                  <p>Making the prepaid payment account inactive </p>
                </td>
                <td valign="middle">
                  <p>In case of no financial transaction for a consecutive period of 1 (one) year in the Prepaid Payment Account, the Bank shall make the Prepaid Payment Account inactive. </p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>9</p>
                </td>
                <td valign="middle">
                  <p>Usage of the instrument </p>
                </td>
                <td valign="middle">
                  <p>The instrument can be used only for purchase of goods and services. Fund transfer to other Freecharge Wallet(s) or to bank accounts is not permitted. </p>
                </td>
              </tr>
              </tbody>
            </table>
            <p>(B) Full KYC* Freecharge Wallet Features:-  </p>
            <table cellspacing="0" cellpadding="7" style="margin:15px 0px;" class="freeAxis">
              <tbody>
              <tr>
                <td valign="middle">
                  <p>
                    <strong>Sr No</strong>
                  </p>
                </td>
                <td valign="middle">
                  <p>
                    <strong>Feature</strong>
                  </p>
                </td>
                <td valign="middle">
                  <p>
                    <strong>Conditions</strong>
                  </p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>1</p>
                </td>
                <td valign="middle">
                  <p>Currency of Issue</p>
                </td>
                <td valign="middle">
                  <p>Indian Rupees</p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>2</p>
                </td>
                <td valign="middle">
                  <p>Loading or Re-loading funds </p>
                </td>
                <td valign="middle">
                  <p>The maximum outstanding balance must not exceed INR 1,00,000/- (Indian Rupees One Lac only) at any given point in time across all prepaid products held by one customer.</p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>3</p>
                </td>
                <td valign="middle">
                  <p>Funds transfer from prepaid payment account </p>
                </td>
                <td valign="middle">
                  <p>(a) In case of pre-registered beneficiaries, the fund transfer limit shall not exceed INR 1,00,000/- (Indian Rupees One Lac Only) per month per beneficiary. <br/>
                     (b) for all other cases, fund transfer limit shall not exceed INR 10,000/- (Indian Rupees Ten Thousand Only) per month. 
                  </p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>4</p>
                </td>
                <td valign="middle">
                  <p>Making the prepaid payment account inactive</p>
                </td>
                <td valign="middle">
                  <p>In case of no financial transaction for a consecutive period of 1 (one) year in the Prepaid Payment Account, the Bank shall make the Prepaid Payment Account inactive. The same shall be reactivated only after due diligence and validation to the satisfaction of the Bank. </p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>5</p>
                </td>
                <td valign="middle">
                  <p>Cash withdrawal</p>
                </td>
                <td valign="middle">
                  <p>Cash withdrawal of any amount from Prepaid Payment Account is prohibited under RBI regulations.</p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>6</p>
                </td>
                <td valign="middle">
                  <p>Validity period</p>
                </td>
                <td valign="middle">
                  <p>1 (one) year from the date of last transaction or date of loading or re-loading of money in Prepaid Payment Account, whichever is later</p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>7</p>
                </td>
                <td valign="middle">
                  <p>Usage of the instrument</p>
                </td>
                <td valign="middle">
                  <p>The instrument can be used only for money transfer and purchase of goods and services</p>
                </td>
              </tr>
              </tbody>
            </table>
            <p style="font-size: 11px; margin-top: 0px;">*Note: KYC documentation shall be in line with the prevailing RBI guidelines on (KYC) Know your Customer. </p>
            <p style="font-size: 11px;">Above parameters and limit capping is applicable as per the Master direction on issuance and operation of prepaid card instruments dated 11th Oct 2017, and subject to change as when new guidelines is issued by RBI. </p> 
            
            <p>4.1.2.   Freecharge Gift Instruments<br/>
              a.  Maximum value of each such payment instrument shall not exceed INR 10,000/- (Indian Rupees Ten Thousand Only).<br/>
              b.  The maximum number of instruments which can be issued to an individual shall be 25 (twenty five) in a financial year. The validity of the prepaid gift instruments shall be 1095 days. However, it may vary between one year to three years as may be decided by the Bank at the time of issuance of prepaid gift instruments.
            </p>
            <p>4.2  The said Prepaid Payment Account is valid for purchase of Products and Transactions within territory of India in Indian Rupees only.</p>
            <p>4.3  Prepaid Payment Account or Services thereunder is not transferable unless required by operation of law.</p>
            <p>4.4  The Customer shall be able to use the Prepaid Payment Account only to the extent of the amount loaded onto the Prepaid Payment Account.</p>
            <p>4.5  The Prepaid Payment Account shall be activated subject to the such minimum amount being loaded on the Prepaid Payment Account as may be specified by the Bank from time to time and after creation of the Freecharge account.</p>
            <p>4.6 The Customer or any other person permitted to load the Prepaid Payment Account may credit the Prepaid Payment Account through any of the methods prescribed from time to time.</p>
            <p>4.7 The Customer may be able to carry out all or any kind of Transactions, as may be available from time to time.</p>
            <p>4.8 No cash withdrawal or cash remittance is permissible on the Freecharge Balance Account. No interest will be payable by the Bank to Customers on the available balance reflected on the Freecharge Balance Account.</p>
            <p>4.9 The Customer is permitted to maintain and operate only one Freecharge Balance Account. Any suspected non-conformity with this requirement shall be just cause for the suspension/ discontinuation of any/all Prepaid Payment Accounts associated with the Customer.</p>
            <p>4.10 The Bank may further as per its discretion introduce appropriate controls over the usage of the Prepaid Payment Account.</p>
            <p>
              <strong>5. NATURE OF USAGE</strong>
            </p>
            <p>
              5.1 The Customer shall at all times ensure that the Freecharge account/ Prepaid Payment Account credentials are kept safe and shall under no circumstances whatsoever allow the Freecharge Account and Prepaid Payment Account to be used by any other individual. You shall be responsible for all facilities granted by the Bank and for all related charges and shall act in good faith in relation to all dealings with the Prepaid Payment Account and the Bank.
            </p>
            <p>
              5.2 The Customer will be responsible for the security of the Prepaid Payment Account including Freecharge account and shall take all steps towards ensuring the safekeeping thereof. The Customer shall not disclose his/her/its password to anyone verbally or in writing nor record it elsewhere and if so done, same shall be at Customers’ sole risk and peril.
            </p>
            <p>
              5.3. The Customer will be liable for all charges incurred on the Prepaid Payment Account until the Prepaid Payment Account is reported for closure. You shall, immediately intimate the Bank of the occurrence of any fraud, hacking or unauthorised use and the Bank may, after due investigations, suspend or terminate the Prepaid Payment Account. It shall be solely Customers responsibility to ensure privacy and confidentiality of Prepaid Payment Account details.
            </p>
            <p>
              5.4 On creation of Prepaid Payment Account, You will have the opportunity to use various interactive aspects through which You can communicate with us and share information (collectively "<strong>Posted Information</strong>"). It is important that You act responsibly when providing such Posted Information. Do not use inappropriate language, make gratuitous personal criticisms or comments. When participating in interactive aspects of the Service, please do include all relevant information in a concise manner to help us provide You with a constructive response. You agree and acknowledge that:
            </p>
            <p>
              5.4.1 You are solely responsible for any Posted Information that You submit on the Website or transmit to our Team and/or other users of the Website;
            </p>
            <p>
              5.4.2 You may not post, distribute, or reproduce in any way any copyrighted material, trademarks, or other proprietary information without obtaining the prior written consent of the owner of such proprietary rights. You may not submit any content or material that infringes, misappropriates or violates the intellectual property, publicity, privacy or other rights of any party;
            </p>
            <p>
              5.4.3 You may not provide any Posted Information that falsely expresses or implies that such content or material is sponsored or endorsed by the Bank;
            </p>
            <p>
              5.4.4 You may not provide any Posted Information that is unlawful or that promotes or encourages illegal activity;
            </p>
            <p>
              5.4.5 You understand and agree that the Bank may (but is not obligated to) review and delete any Posted Information that in the sole discretion of the Bank violates these Terms or which might be offensive, illegal, or violate the rights of, harm, or threaten the safety of other users of the Website and/or any other person;
            </p>
            <p>
              5.4.6 You acknowledge and agree that the Bank does not and shall not be required to actively monitor nor exercise any editorial control whatsoever over the content of any message or other material or information created, obtained or accessible through the Services. The Bank does not endorse, verify or otherwise certify the contents of any comments or other material or information made by You. You are solely responsible for the contents of Your communications and may be held legally liable or accountable for the content of Your comments or other material or information;
            </p>
            <p>
              5.4.7 You agree that You will only provide Posted Information that You believe to be true and You will not purposely provide false or misleading information; and
            </p>
            <p>
              5.4.8 The following is a partial list of the kind of content and communications that are illegal or prohibited on/through the Website. The Bank reserves the right to investigate and take appropriate legal action in its sole discretion against anyone who violates this provision, including without limitation, removing the offending communication from the Services and terminating the membership of such violators or blocking Your use of the Services and/or the Website. You may not post content that:
            </p>
            <p>
              (a) belongs to another person and to which You do not have any right to;
            </p>
            <p>
              (b) is grossly harmful, harassing, blasphemous defamatory, obscene, pornographic, pedophilic, libelous, invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever;
            </p>
            <p>
              (c) harm minors in any way;
            </p>
            <p>
              (d) infringes any patent, trademark, copyright or other proprietary rights;
            </p>
            <p>
              (e) violates any law for the time being in force;
            </p>
            <p>
              (f) deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;
            </p>
            <p>
              (g) impersonates another person;
            </p>
            <p>
              (h) contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource;
            </p>
            <p>
              (i) threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting any other nation.
            </p>
            <p>
              5.5. You will notify the Bank at care@freecharge.in upon coming across any objectionable content on the Website and the Bank shall use best efforts to remove such objectionable content within the time period prescribed under Applicable Law.
            </p>
            <p>
              5.6. The Customer shall inform the Bank in writing within seven (7) days, if any discrepancies exist in the Transactions/ particulars of the Freecharge Balance Account on any statement / records that is made available to the Customer. If the Bank does not receive any information to the contrary within seven (7) days, the statement and the transactions shall be deemed to be correct and unconditionally and irrevocably binding on You. All records of Your instructions and such other details (including but not limited to payments made or received) maintained by the Bank, in electronic or documentary form pursuant to the Terms and Conditions herein, shall as against You, be deemed to be conclusive evidence of such instructions and such other details. In case of any dispute relating to the time of reporting and/ or Transaction/s made on the Freecharge Balance Account or any other matter in relation to the said Freecharge Balance Account, the Bank shall reserve the right to ascertain the time and/ or the authenticity of the disputed Transaction. For customer care contact details, kindly refer to the customer grievance policy available on https://www.freecharge.in.
            </p>
            <p>
              5.7. You may only use the Services for Transactions with the approved Merchants.
            </p>
            <p>
              5.8. You must ensure the availability of sufficient funds before executing any Transaction from the Prepaid Payment Account.
            </p>
            <p>
              5.9. You agree to adhere to all Applicable Laws and all such regulations, guidelines and rules prescribed from time to time by the Bank, RBI and any other regulatory body.
            </p>
            <p>
              5.10. The Customer agrees that he will not use the Prepaid Payment Account for payment of any illegal/unlawful purchases/purposes.
            </p>
            <p>
              5.11. You hereby agree and acknowledge that the Prepaid Payment Account is issued, loaded, withdrawn, terminated, closed down, suspended by the Bank only.
            </p>
            <p>
              5.12. You shall be bound to comply with the terms and conditions and all the policies stipulated by the Bank from time to time in relation to the Prepaid Payment Account. The Bank may, at its sole discretion, refuse to accept the application or to issue Prepaid Payment Account to the Customer.
            </p>
            <p>
              5.13. You shall not use the Services for any purpose that might be construed as contrary or repugnant to any Applicable Laws, regulations, guidelines, judicial dicta, the Banks policies or public policy or for any purpose that might negatively prejudice the goodwill of the Bank.
            </p>
            <p>
              5.14. You shall accept full responsibility for wrongful use of the Prepaid Payment Account and which is in contravention of these terms and conditions. You shall indemnify the Bank and Freecharge and their respective director, officers, personnel, contractors and agents, to make good any loss, damage, penalties, claims, demand, interest or any other financial charges (including lawyer fees) that the aforesaid persons may incur and or suffer whether directly or indirectly as a result of the Customer committing violations of these terms and conditions.
            </p>
            <p>
              5.15. The Bank reserves the absolute discretion and liberty to decline or honour the authorization request on the Prepaid Payment Account without assigning any reason thereto.
            </p>
            <p>
              5.16. The Customer acknowledges and understands that the Services are linked to internet connection (and in case of mobile, mobile phone connection) and the Bank or its authorised service providers shall not be responsible and the Customer is solely responsible for all liability arising from including but not limited to any loss or interruption of the Services or the unavailability of Services due to a mobile or internet not supporting Website or Merchant site(s).
            </p>
            <p>
              5.17. The Customer acknowledge and understand that the information submitted by the Customer for availing the Services or information submitted while using the Services may be shared with third parties inter alia, to facilitate the provision of the Services.
            </p>
            <p>
              5.18. The Bank may request the Customer to submit additional KYC information/documents as part of ongoing monitoring and due diligence.
            </p>
            <p>
              5.19. The Bank may at their sole discretion, utilize the services of external service providers/or agents and on such terms as required or necessary, in relation to Freecharge Balance and/or Services.
            </p>
            <p>
              6. <strong>INFORMATION/DATA USAGE</strong>
            </p>
            <p>
              6.1.Except for Posted Information that You submit, all of the information available on or through the Services and/or the Website, including without limitation, text, photographs, graphics and video and audio content, is owned by us and our licensors and is protected by copyright, trademark, patent and trade secret laws, other proprietary rights and international treaties. You acknowledge that the Services and any underlying technology or software used in connection with the Services contain the Bank’s proprietary information. We give You permission to use the aforementioned content for personal, non-commercial purposes only and do not grant, transfer, license any intellectual property rights to You or create any rights in Your favour by virtue of permitting Your use of the Services. You may print, download, and store information from the Website for your own convenience, but You may not copy, distribute, republish (except as permitted in this paragraph), sell, or exploit any of the content, or exploit the Website in whole or in part, for any commercial gain or purpose whatsoever. Except as is expressly and unambiguously provided herein, we do not grant You any express or implied rights.
            </p>
            <p>
              6.2. THE CUSTOMER UNDERSTANDS THAT THE TERMS AND CONDITIONS PROVIDED ON MERCHANTS SITE(S) SHALL BE INDEPENDENT OF THESE TERMS AND CONDITIONS MENTIONED HEREIN.
            </p>
            <p>
              <strong>7. TRANSACTION AND LOADING OF PREPAID PAYMENT ACCOUNT</strong>
            </p>
            <p>
              7.1 You can use multiple funding sources for loading money in the Prepaid Payment Account. These sources could be but not limited to Cash, Credit Cards, Debit Cards, UPI, Net Banking, and transfer from another Freecharge Wallet.
            </p>
            <p>
              7.2 The Prepaid Payment Account may also be loaded by transfer of refund money of Transactions carried out using services of Merchant(s) or such other manner as is acceptable to the Bank. The Prepaid Payment Account may also comprise of Gift Instruments issued by way of incentives such as cash back, gifts, etc. by Merchant or third parties or such other manner as is acceptable to the Bank.
            </p>
            <p>
              7.3 The Bank may impose charges/ fees etc., payable by the Customer for availing the said Prepaid Payment Account and funds shall be loaded on the Prepaid Payment Account after deduction of the applicable charges/ fees etc.
            </p>
            <p>
              7.4 In order to manage risk, the Bank may limit the funding sources available for Your use to fund any particular Transaction.
            </p>
            <p>
              7.5 The Bank may monitor each Transaction made into Your Prepaid Payment Account to monitor high-risk & fraudulent transactions. If Your Transaction is classified as a high-risk Transaction or is suspected of fraud, the Bank may place a hold on the Transaction and may ask You for more information on You and Your funding source. The Bank will conduct a review and accordingly the Bank will either clear or cancel the Transaction, as per Applicable Law. If the Transaction is cleared, Bank will notify You and update Your Prepaid Payment Account. Otherwise, the Bank will cancel the Transaction and the funds may be forfeited. The said funds will be refundable only to source account upon valid demand raised by holder of source account. The Bank will notify You by email and/or in the account history tab of Your Prepaid Payment Account if the Transaction is cancelled. You agree and understand that the Bank may, as per its internal security policies and regulatory and statutory guidelines, report transactions to appropriate authorities which it classify to be suspicious or fraudulent and the Bank shall not be liable for any loss caused to You for the exercise of such rights by the Bank, even if any such transaction is found to be regular and lawful at a later stage.
            </p>
            <p>
              7.6 When You load the Prepaid Payment Account, You are liable to the Bank for the full amount of the load plus any fees, if the load is later invalidated for any reason including but not limited to chargeback, reversal of Transaction, dispute by the owner of funding source of a Transaction, You agree to allow the Bank to recover any amounts due to the Bank by debiting Your Prepaid Payment Account. If there are insufficient funds in Your Prepaid Payment Account to cover Your liability, You agree to reimburse the Bank through other means. If the Bank is unable to recover the funds from Your primary funding source, the Bank may attempt to contact You and/or recover the funds from Your alternate funding sources, or may at their discretion, can take appropriate legal actions to collect the amount due, to the extent allowed by Applicable Law. In addition, the Bank reserves the right to suspend or terminate Your Prepaid Payment Account.
            </p>
            <p>
              <strong>8.TRANSACTIONS IN PREPAID PAYMENT ACCOUNT</strong>
            </p>
            <p>
              8.1 Customer can choose to withdraw the funds available in his Freecharge Wallet to any bank account by way of IMPS/NEFT in accordance with guidelines of RBI. However, the Bank may deny such withdrawal in any of the events as specified in Clause 10.2 herein below.
            </p>
            <p>
              8.2 The Bank reserves the right to delay withdrawals while screening for risk, or request You provide additional information to verify Your identity and may limit the amount You can withdraw until the information is verified.
            </p>
            <p>
              8.3 Any withdrawal found to be suspicious will be held back and reversed into the Freecharge Wallet. The Prepaid Payment Account and Freecharge Account will also be suspended for operations and no Transactions will be possible pending an investigation. A notification will be given to You if You are the subject of an investigation. If You are able to provide a justification for the withdrawal to the satisfaction of the Bank, Your account/Prepaid Payment Account will be removed from suspension and You would be free to transact using the Freecharge Wallet.
            </p>
            <p>
              8.4 In the case of no information being provided by You for a suspended Prepaid Payment Account, the Prepaid Payment Account will continue to be suspended till its validity and the amount will be forfeited in compliance with guidelines of RBI.
            </p>
            <p>
              <strong>9. SUSPENSION OR DISCONTINUANCE OF SERVICES</strong>
            </p>
            <p>
              9.1  Under no circumstances shall the Bank or any of its authorised service provider shall be held liable for a delay or failure or disruption of the Service resulting directly or indirectly from, acts of nature, forces or causes beyond its reasonable control including without limitation, internet failures, computer, telecommunications or any other equipment failures, electrical power failures or non-availability, strikes, labour disputes, riots, insurrections, civil disturbances, sabotage, wilful destruction, threat to national security, shortages of labour or materials, fire, flood, heavy rains, storms, explosions, Acts of God, war, governmental actions, orders of courts or tribunals or non-performance of third parties ("<strong>Force Majuere</strong>").
            </p>
            <p>
              9.2 The Bank reserves the right, without prior notice and at its sole discretion, to suspend, restrict, discontinue or deny access to or Your use of Services provided by the Bank:
            </p>
            <p>
              9.2.1 if You use or the Prepaid Payment Account is used or suspected to be used to defraud any person or entity;
            </p>
            <p>
              9.2.2 if You use the Prepaid Payment Account to engage in any unlawful activities including without limitation those which would constitute the infringement of intellectual property rights, a civil liability or a criminal offence;
            </p>
            <p>
              9.2.3 if You engage in any activities that would otherwise create any liability for the Bank or for any of its contractors or agents;
            </p>
            <p>
              9.2.4 for any suspected discrepancy in the particular(s), online application, documentation provided by the Customer;
            </p>
            <p>
              9.2.5 any Force Majeure reasons;
            </p>
            <p>
              9.2.6 if the same is due to technical failure, modification, upgradation, variation, relocation, repair, and/or maintenance due to any emergency or for any technical reasons;
            </p>
            <p>
              9.2.7 for any suspected violation of the rules, regulations, orders, directions, notifications issued by RBI and/or any other regulatory authority from time to time or for any violation of the terms and conditions mentioned herein; or
            </p>
            <p>
              9.2.8 if the Bank believes, in its reasonable opinion, that cessation/ suspension is necessary.
            </p>
            <p>
              <strong>10. TERMINATION OF PREPAID PAYMENT ACCOUNT</strong>
            </p>
            <p>
              10.1 Customer has the option to close the Prepaid Payment Account at any time. The closure of such Prepaid Payment Account may be communicated to the Bank over email at care@freecharge.in. Termination will be effective subject to payment of all amounts outstanding on the Prepaid Payment Account and there is no unused Gift Instrument. In case the Customer wishes to close the Prepaid Payment Account and there is balance in the Freecharge Wallet, the Customer can request the Bank to transfer the funds to his/her own bank account (duly verified by the Bank) after complying with the KYC requirements. Balance lying in Gift Instruments cannot be transferred to a bank account in case of closure of the Prepaid Payment Account.
            </p>
            <p>
              10.2 The Bank may also restrict, terminate or suspend the use of Prepaid Payment Account at any time without prior notice if the Bank reasonably believes it necessary for business or security reasons. The Prepaid Payment Account must not be used after these Terms and Conditions end or while use of Prepaid Payment Account is suspended.
            </p>
            <p>
              10.3 The Bank shall, upon adequate verification, block/suspend/close the Prepaid Payment Account and terminate all facilities in relation thereto following the receipt of such intimation and shall not be liable for any inconvenience or loss caused to the Customer in this regard.
            </p>
            <p>
              10.4 Any value in Your Freecharge Wallet must be utilized in the following manner:
            </p>
            <p>10.4.1 For Minimum KYC Freecharge Wallet- Within one year from the date of activation.</p>            
            <p>10.5  Any value in Your Freecharge Gift Instrument must be utilized in the following manner:</p>
            <p>10.5.1 Within 1095 days from the date of activation</p>
            <p>10.6 The Bank reserves the right to change the expiry period of the Freecharge Wallet or the Freecharge Gift Instrument as mentioned in 10.4 & 10.5 in line with Applicable Laws.</p>
            <p>10.7 Any value in Freecharge Wallet which is not utilized or withdrawn within 10 years from the date of issuance will be transferred to the Depositor Education and Awareness Fund. The Bank will send forty five (45) days advance communication to Customer before any forfeiture of outstanding amount in the Prepaid Payment Account by SMS at the mobile number and/or by email at email id which is provided by Customer for use of the Services. It is the responsibility of the Customer to ensure that the information provided by the Customer including the email id and the mobile number is updated at all times.</p>

            <p>
              <strong>11. YOUR OTHER OBLIGATIONS</strong>
            </p>
            <p>11.1 You shall be solely liable for entering into any Transaction and the risk arising thereof.</p>
            <p>11.2 You accept that at Your request and risk, the Bank has agreed to provide You the Prepaid Payment Account and You accept full responsibility for all Transactions recorded by use of Your Prepaid Payment Account.</p>
            <p>11.3  An instruction given by means of the Prepaid Payment Account shall be irrevocable.</p>
            <p>11.4  You shall, in all circumstances, accept full responsibility for the use of the Prepaid Payment Account, whether or not processed with Your knowledge or Your authority, expressed or implied unless same is covered as per clause 13.</p>
            <p>11.5 You irrevocably authorize the Bank to debit the amounts utilized by using the Prepaid Payment Account for Transactions from Your Prepaid Payment Account.</p>
            <p>11.6 You should hold the Bank indemnified and harmless for its actions in good faith and in the normal course of business based on Transactions.</p>
            <p>11.7  The Bank will employ its best efforts in carrying out the Transactions but will not incur any liability either to the Customer or any other person for any reason whatsoever including for its delay or inability to carry out a Transaction or an instruction.</p>
            <p>11.8 Any government charges, or debits, or tax payable as a result of the use of Prepaid Payment Account shall be Your responsibility.</p>
            <p>11.9 Amounts due and payable by Customer, if not paid separately shall be recovered by the Bank from the funds available in the Prepaid Payment Account to the extent permitted under Applicable Laws.</p>
            <p>11.10 The Bank shall have discretion to not allow You to carry out a Transaction where it has reason to believe that the use of Prepaid Payment Account is not authorized or the Transaction appears ambiguous or unclear or such as to raise a doubt.</p>
            <p>
              <strong>12. WINDING UP OF PREPAID PAYMENT ACCOUNT AND CONSEQUENCES THERETO</strong>
            </p>
            <p>
              12.1 To the extent permitted under Applicable Laws, the Bank may at its sole discretion close or wind up any Prepaid Payment Account Services or amend any of its features with notice to You.
            </p>
            <p>
              12.2 You hereby agree that, upon any closure or winding up of the Prepaid Payment Account Services, You may redeem the outstanding balance in Your Prepaid Payment Account or transfer the same to any prepaid payment instrument or bank account held/maintained by you with any third party with in such time as may be determined by the Bank, failing which, the Bank may transfer the balance in Your Prepaid Payment Account to Your another prepaid payment instrument issued by such entity having authorisation from RBI and with which the Bank has made appropriate arrangements.
            </p>
            <p>
              13. <strong>INDEMNITY AND LIMITATION OF LIABILITY</strong>
            </p>
            <p>
              13.1. The liability of the Bank or Customer for any unauthorized transaction shall be:
            </p>
            <p>13.1.1.  Zero Liability of a Customer- A customer’s entitlement to zero liability shall arise where the unauthorized transaction occurs in the following manner: </p>
            <p>(a) Contributory fraud/ negligence/ deficiency on the part of the bank (irrespective of whether or not the transaction is reported by the Customer)</p>
            <p>(b)Third party breach where the deficiency lies neither with the bank nor with the customer but lies elsewhere in the system, and the customer notifies the bank within three working days of receiving the communication from the bank regarding the unauthorised transaction.</p>
            <p>13.1.2. Limited Liability of a Customer- A Customer shall be liable for the loss occurring due to unauthorised transactions in the following cases:</p>
            <p>(a) In cases where the loss is due to negligence by a customer, such as where he has shared the payment credentials, the Customer will bear the entire loss until he reports the unauthorised transaction to the Bank. Any loss occurring after the reporting of the unauthorised transaction shall be borne by the Bank.</p>
            <p>(b) In cases where the responsibility for the unauthorised electronic banking transaction lies neither with the Bank nor with the Customer, but lies elsewhere in the system and when there is a delay (of four to seven working days after receiving the communication from the bank) on the part of the Customer in notifying the Bank of such a transaction, the per transaction liability of the customer shall be limited to the transaction value or INR 10,000/- whichever is lower.</p>
            <p>13.1.3. In case the Customer notifies the Bank after seven working days of receiving the communication from the Bank regarding the unauthorized transaction, the Bank shall evaluate and determine the same as per the Bank’s board approved policy.</p>

            <p>13.2. The cumulative liability of the Bank to You for any and all other claims (other than claims mentioned in Clause 13.1) relating to or arising out of Your use of the Website or Services, regardless of the form of action, exceed INR 1000/- (Indian Rupees One Thousand Only) or as per applicable law, whichever is lower.
            </p>
            <p>
              13.3 In consideration of the Bank providing the Customer with the facility of the Prepaid Payment Account, You hereby agree to indemnify and keep the Bank indemnified from and against all actions, claims, demands, proceedings, losses, damages, personal injury, costs, charges and expenses whatsoever (including lawyer's fees) which the Bank or its service providers may at any time incur, sustain, suffer or be put to as a consequence of or by reason of or arising out of directly or indirectly providing the Customer the said facility of the Prepaid Payment Account or by reason of the Bank's acting in good faith and taking or refusing to take or omitting to take action on the Customer's instructions, and in particular arising directly or indirectly out of the negligence, mistake or misconduct of the Customer; breach or noncompliance of the rules/ terms and conditions relating to the Prepaid Payment Account or breach or noncompliance of Applicable Law or fraud or dishonesty relating to any Transaction by the Customer.
            </p>
            <p>13.4 Without prejudice to the foregoing, the Bank or its authorised service providers shall be under no liability whatsoever to the Customer in respect of any loss or damage arising directly or indirectly out of:</p>

            <p>13.4.1 Any issue relating to the Products from Merchant;</p>
            <p>13.4.2 the refusal to honour or to accept the Prepaid Payment Account registration;</p>
            <p>13.4.3 the malfunction of any computer/point of sale terminal;</p>
            <p>13.4.4 effecting Transaction instructions other than by a Customer;</p>
            <p>13.4.5 handing over of the Prepaid Payment Account credentials by the Customer to a third Party;</p>
            <p>13.4.6 the exercise by the Bank of their right to demand and procure the surrender of the Prepaid Payment Account prior to the expiry, whether such demand and surrender is made and/ or procured by the Bank or by any person or computer terminal.</p>
            <p>13.4.7 the exercise by the Bank of their right to terminate any Prepaid Payment Account;</p>
            <p>13.4.8 any injury to the credit, character and reputation of the Customer alleged to have been caused by the repossession of the Prepaid Payment Account and/ or, any request for its return or the refusal of any Merchant honour or accept the Prepaid Payment Account;</p>
            <p>13.4.9 any misstatement, misrepresentation, error or omission in any details disclosed by the Bank except as otherwise required by law;</p>
            <p>13.4.10 If the Bank receives any process, summons, order injunction, execution distrait, levy, lien, information or notice which the Bank in good faith believes/ calls into question the Customer's ability, or the ability of someone purporting to be authorized by the Customer, to transact on the Prepaid Payment Account, the Bank may, at its option and without liability to the Customer or such other person, decline to allow the Customer to obtain any portion of his funds, or may pay such funds over to an appropriate authority and take any other steps required by Applicable Law. The Bank reserves the right to deduct from the balance available on the Prepaid Payment Account, a service charge and any expenses it incurs, including without limitation legal fees, due to legal action involving the Customer's Prepaid Payment Account;</p>
            <p>13.4.11 In the event a demand or claim for settlement of outstanding dues from the Customer is made, either by the Bank or any person acting on behalf of the Bank, the Customer agrees and acknowledges that such demand or claim shall not amount to be an act of defamation or an act prejudicial to or reflecting upon the character of the Customer, in any manner.</p>
            <p>
            13.5 Each party acknowledges that the other party has entered into these Terms and Conditions relying on the limitations of liability stated herein and that those limitations are an essential basis of the bargain between the parties. The exclusions and limitations of liability contained in this Section 13 shall not apply to damages arising out of or relating to gross negligence or wilful misconduct of a Party; or caused by breach of Applicable Laws, obligation of confidentiality or infringement of intellectual property rights.
            </p>
            <p>
              <strong>14. SPAMMING</strong>
            </p>
            <p>
              14.1 You may not use contact information provided by us or other users or harvest such information for the purpose of sending, or to facilitate the sending, of unsolicited bulk communications such as spam. You may not allow others to use Your account to violate the terms of this section. We may terminate Your membership or access to the Website immediately and take other legal action if You or anyone using Your credentials violates these provisions.
            </p>
            <p>
              <strong>15. ADDITIONAL TERMS</strong>
            </p>
            <p>
              15.1 Certain services on the Website may have additional terms (such as policies, guidelines, and rules) that will further govern Your use of that particular service and supplement these Terms. If You choose to register for or access any such services, You will be presented with any relevant additional terms and conditions at that time. By using those services, You agree to comply with such additional guidelines and rules.
            </p>
            <p>
              <strong>16. DISCLAIMER</strong>
            </p>
            <p>
              16.1 TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SERVICE IS PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS, AND THE BANK HEREBY EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF CONDITION, QUALITY, DURABILITY, PERFORMANCE, ACCURACY, RELIABILITY, AVAILABILITY, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT. ALL SUCH WARRANTIES, REPRESENTATIONS, CONDITIONS, UNDERTAKINGS AND TERMS ARE HEREBY EXCLUDED. TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE BANK MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE VALIDITY, ACCURACY, RELIABILITY, QUALITY, STABILITY, COMPLETENESS OR CURRENTNESS OF ANY INFORMATION PROVIDED ON OR THROUGH THE WEBSITE. Use of the Service is at Your own risk. No advice or information, whether oral or written, obtained by You from the Bank or through the Services will create any warranty not expressly stated herein. Without limiting the foregoing, the Bank does not warrant that the content is accurate, reliable or correct; that the Services will meet Your requirements; that the Services will be available at any particular time or location, uninterrupted or secure; that any defects or errors will be corrected; or that the Services is free of viruses or other harmful components. The Bank will not be liable for any loss or damage whatsoever or howsoever caused or arising, directly or indirectly, including without limitation, as a result of loss of data; interruption or stoppage of access to and/or use of the Services; interruption or stoppage Website; non-availability of connectivity links/hyperlinks.
            </p>
            <p>
              16.2 Any material downloaded or otherwise obtained through the use of the Service is done at each Customer's sole discretion and risk and each Customer is solely responsible for any damage to its computer system or property or loss of data that results from the download of any such material.
            </p>
            <p>
              16.3 The Bank does not warrant, endorse, guarantee, or assume responsibility for Products advertised or offered by a third party through the Services or any hyperlinked website or Service, or featured in any banner or other advertising, and the Bank will not be a party to or in any way monitor any Transaction between You and third-party providers of goods and/ or services. Any advertisement by a third party through the Services shall not constitute or deems to constitute as advice or solicitation for sale/purchase of third party products or services by Axis Bank and is not intended to create any rights and obligations.
            </p>
            <p>
              16.4 The Services are controlled and operated from and in India. The Bank and/or its authorized service provider(s) makes no representations that the Services are appropriate or available for use in other locations. Those who access or use the Services from other jurisdictions do so at their own volition and are entirely responsible for compliance with all Applicable Laws, rules, regulations, guidelines including but not limited to export and import regulations and RBI regulations. You may not use the Service if You are a resident of a country embargoed by the India, or are a foreign person or entity blocked or denied by the Indian government. Unless otherwise explicitly stated, all materials found on the Services are solely directed to individuals, companies, or other entities located in India.
            </p>
            <p>
              16.5 The Banks sole obligation and Your sole and exclusive remedy in the event of interruption in Services or loss of use and/or access to Services, will be to use all reasonable endeavours to restore the Services and/or access as soon as reasonably possible.
            </p>
            <p>
              16.6 The Bank makes no representations or guarantees regarding Merchants utilizing Prepaid Payment Account. Use of Prepaid Payment Account in no way represents any endorsement by the Bank of a Merchant's existence, legitimacy/legality, ability, policies, practices, beliefs as well as the Merchants Products and reliability. The Merchant alone will be responsible to the Customer and neither we nor anybody connected to us will have any responsibility or liability towards the Customer for goods and services purchased from the Merchant.
            </p>
            <p>
              16.7 The relationship between the Bank and the Merchant is on principal-to-principal basis.
            </p>
            <p>
              <strong>17. ADDITION, ALTERATION, WITHDRAWAL OF SERVICES AND AMENDMNET OF TERMS &amp; CONDITIONS</strong> :
            </p>
            <p>
              17.1 The Bank may, at its sole discretion, and to the extent permissible under Applicable Laws, make available to the Customer more services or make alteration in the services on the Prepaid Payment Account, point of sale terminals, internet or other wise and/ or other devices through shared networks for the Customer's convenience and use. All fees and charges related to Transactions done by the Customer at these devices as determined by the Bank from time to time will be recovered by a debit to the balance available on the Prepaid Payment Account. The Customer understands and agrees that the shared networks may provide different functionalities and service offerings and different charges for different services. The Bank shall also, in its sole discretion, at any time, without notice to the Customer, be entitled to withdraw, discontinue, cancel, suspend/ or terminate the facility to use the Prepaid Payment Account and/ or services related to it, at POS Terminal/ Internet/ other devices within India and shall not be liable to the Customer for any loss or damage suffered by him resulting in any way from such withdrawal, discontinuance, cancellation, suspension or termination.
            </p>
            <p>
              17.2 We have the right, at our sole and absolute discretion, to change, modify, or amend any portion of these Terms of Service at any time by posting notification on https://www.freecharge.in or otherwise communicating the notification to You. The changes will become effective, and shall be deemed accepted by You, after the initial posting and shall apply on a going-forward basis with respect to Transactions initiated after the posting date. You should make sure You view these Terms and Conditions periodically to make sure You are familiar with their most recent version. In the event that You do not agree with any such modification, Your sole and exclusive remedy is to terminate Your use of the Services.
            </p>
            <p>
             <strong>18. DISCLOSURE OF INFORMATION AND INTELLECTUAL PROPERTY RIGHTS</strong>
            </p>
            <p>
              18.1 The Bank reserves the right to disclose the Customer's information to any court of competent jurisdiction, quasi-judicial authorities, law enforcement agencies, regulatory authority and any other wing of Central Government or State Government. The Customer hereby consents to sharing of their information with the Bank and its authorized service provider(s) or agencies or partners for providing Services in relation to the Prepaid Payment Account.
            </p>
            <p>
              18.2 Any information relating to the Customer is generally used to provide the Services, improve the Services and otherwise for a detailed overview of what kind of information that the Bank collects, stores, uses, shares, please go through the privacy policy available on the Website.
            </p>
            <p>
              18.3 The user interfaces, graphics, logos, design, compilation, information, computer code (including source code and object code), products, software, services, and all other elements of the Service provided by the Bank are protected by copyright, designs, patent, and trademark laws, international conventions and other applicable intellectual property and proprietary rights of either the Bank or authorised service provider(s) of the Bank.
            </p>
            <p>
              18.4 Nothing contained herein shall authorize the Customer to use, apply, invade or in any manner exploit or infringe the intellectual property rights of the Bank, and the usage of the same shall be in compliance with these Terms and Conditions and such approval and policies as may be notified from time to time. In addition, Customer undertakes not to infringe the intellectual property rights of any third party.
            </p>
            <p>
              18.5 The Customer undertakes not to infringe the intellectual property rights of the Bank, whether directly or indirectly through any third party. The Customer warrants that it shall only use the Website, software application and the Services for the purposes of these Terms and Conditions. Customer or any other person empowered by the Customer shall not use the Website and software application and/or the Bank’s services and software in any form whatsoever, so as to:
            </p>
            <p>
              18.5.1 design, realize, distribute or market a similar or equivalent software program;
            </p>
            <p>
              18.5.2 adapt, modify, transform or rearrange the Bank or its third party service provider's software applications and Website or Merchant website for any reason whatsoever, including for the purpose, among other things, of creating a new software program or a derivative software program;
            </p>
            <p>
              18.5.3 allow unauthorized use of or access to the Bank software applications and Merchant's website;
            </p>
            <p>
              18.5.4 disassemble, reverse engineer, decompile, decode or attempt to decode the Bank software applications and Merchant's website;
            </p>
            <p>
              18.5.5 allow the Bank software applications and Merchant's website to be disassembled, reverse engineered, decompiled or decoded; and/or
            </p>
            <p>
              18.5.6 in any way override or break down any protection system integrated into the Bank software applications and Merchant's website.
            </p>
            <p>
              <strong>19. DISPUTES</strong>
            </p>
            <p>
              19.1 These Terms and Conditions will be governed by and be construed in accordance with the laws of India. All disputes are subject to the exclusive jurisdiction of the Courts in New Delhi, irrespective of whether any other Court may have concurrent jurisdiction in the matter. Each party hereby irrevocably waives any objection which such party may now or hereafter have to the laying of improper venue or forum non convenience. Each party agrees that a judgment in any such action or proceeding may be enforced in other jurisdictions by suit on the judgment or in any manner provided by law.
            </p>
            <p>
              <strong>20. COMMUNICATION</strong>
            </p>
            <p>
              20.1 When You use the Services or send emails or other data, information or communication to the Bank, You are aware of the risks involved in sending email Instructions, including the risk that email Instructions may: (a) be fraudulently or mistakenly written, altered or sent; and (b) not be received in whole or in part by the intended recipient; You declare and confirm that the You are fully aware of and having duly considered, the risks involved, (which risks shall be borne fully by You) have requested and authorised the Bank to rely upon and act on Instructions which may from time to time be given by email as mentioned above. The Bank may (but shall not be obliged to) act as aforesaid without inquiry as to You identity or authority giving or purporting to give any instruction or as to the authenticity of any email message and may treat the same as fully authorised by and binding on You regardless of the circumstances prevailing at the time of the instruction and notwithstanding any error, misunderstanding, lack of clarity, fraud, forgery, or lack of authority in relation thereto, and without requiring any confirmation provided that the concerned person acting on behalf of the Bank believed the instruction to be genuine at the time it was acted upon.
            </p>
            <p>
             20.2 You consent to receive communications via electronic records from the Bank periodically and as and when required. The Bank will communicate with You by email or by notices on Your Prepaid Payment Account or electronic records on the Website which will be deemed adequate service of notice / electronic record. You hereby waive all your rights to challenge the service of documents under any and all legal proceedings if the document is served on your designated electronic address including Your Prepaid Payment Account in terms of Information Technology Act, 2000.
            </p>
            <p>
              20.3 Notwithstanding anything contained herein or elsewhere, the Bank shall not be bound to act in accordance with the whole or any part of the instructions or directions contained in any Instruction and may in its sole discretion and exclusive determination, decline or omit to act pursuant to any instruction, or defer acting in accordance with any instruction, and the same shall be at Your risk and the Bank shall not be liable for the consequences of any such refusal or omission to act or deferment of action.
            </p>
            <p>
              20.4 Customers can contact the Bank at any of the below mentioned points: Email to care@freecharge.in.
            </p>
            <p>
              <strong>21. FEES AND CHARGES</strong>
            </p>
            <p>
              21.1 All fees and charges related to Prepaid Payment Account, as determined by the Bank will be recovered by a debit to the Prepaid Payment Account or through other means as available and applicable. The fee is not refundable. Any government charges, duty or debits, or tax payable as a result of the use of the Prepaid Payment Account shall be the Customer's responsibility and if imposed upon the Bank (either directly or indirectly), the Bank shall debit such charges, duty or tax against the Prepaid Payment Account.
            </p>
            <p>
              21.2. The Bank reserves the right at any time to charge the Customer any fees/ charges after providing one month prior notice for the Transactions carried out by using Your Prepaid Payment Account.
            </p>
            <p>
              21.3 Charges applicable for using Prepaid Payment Account are subject to change from time to time at the discretion of the Bank. Details of the currently applicable fees and charges as stipulated by the Bank, will be displayed on the Website.
            </p>
            <p>
              Existing Fees/ Schedule of Charges :
            </p>
            <table cellspacing="0" cellpadding="7"  class="freeAxis">
              <tbody>
              <tr>
                <td valign="middle">
                  <p>
                    <strong>Type of Prepaid Payment Account Transaction</strong>
                  </p>
                </td>
                <td valign="middle">
                  <p>
                    <strong>Fees</strong>
                  </p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>
                    Issuance Fee
                  </p>
                </td>
                <td valign="middle">
                  <p>
                    NIL
                  </p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>
                    Loading Fee
                  </p>
                </td>
                <td valign="middle">
                  <p>
                    NIL
                  </p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>
                    Fund Transfer (To another Freecharge Wallet)
                  </p>
                </td>
                <td valign="middle">
                  <p>
                    NIL
                  </p>
                </td>
              </tr>
              <tr>
                <td valign="middle">
                  <p>
                    Fund Transfer (To Bank Account)
                  </p>
                </td>
                <td valign="middle">
                  <p>
                    3% of the Transaction
                  </p>
                </td>
              </tr>
              </tbody>
            </table>
            <p>
              Goods and Service Tax, unless explicitly stated in this Schedule of Charges, as may be applicable from time to time on all fees, surcharge and other charges will be levied to the Customer.
            </p>
            <p>
              <strong>22. GENERAL PROVISIONS</strong>
            </p>
            <p>
              22.1 <strong>Assignment</strong>: You cannot assign or otherwise transfer this Agreements, or any rights granted hereunder or any obligations, to any third party and any such assignment or transfer or purported assignment or transfer shall be void ab initio. The Bank's rights and/or obligations under the Agreement are freely assignable or otherwise transferable by the Bank to any third parties without the requirement of seeking Your prior consent. The Bank may inform You of such assignment or transfer in accordance with the notice requirements under the Agreement on best effort basis.
            </p>
            <p>
              22.2 <strong> Severability</strong>: If, for any reason, a court of competent jurisdiction finds any provision of the Agreement, or portion thereof, to be unenforceable, that provision shall be enforced to the maximum extent permissible so as to give effect to the intent of the parties as reflected by that provision, and the remainder of the Agreement shall continue in full force and effect. The Bank may amend in a reasonable manner such provision to make it enforceable and such amendment will be given effect in accordance with the amendment terms of this Agreement.
            </p>
            <p>
              22.3 <strong>Waiver</strong> : Any failure or delay by a party to enforce or exercise any provision of the Agreement, or any related right, shall not constitute a waiver by such party of that provision or right. The exercise of one or more of a party’s rights hereunder shall not be a waiver of, or preclude the exercise of, any rights or remedies available to such party under this Agreement or in law or at equity. Any waiver by a party shall only be made in writing and executed by a duly authorized officer of such party.
            </p>
            <p>
              22.4 <strong>Force Majeure</strong> :  If performance of any service or obligation under this Agreement by the Bank is prevented, restricted, delayed or interfered with by reason of labor disputes, strikes, acts of God, floods, lightning, severe weather, shortages of materials, rationing, utility or communication failures, earthquakes, war, revolution, acts of terrorism, civil commotion, acts of public enemies, blockade, embargo or any law, order, proclamation, regulation, ordinance, demand or requirement having legal effect of any government or any judicial authority or representative of any such government, unavailability of any communication system, breach, interruption or breakdown or virus/malware/bugs in the processes or systems used for provision of Services, computer hacking, unauthorized access to computer data and storage devise, computer crashes, breach of security and encryption, etc. or any other act whatsoever, whether similar or dissimilar to those referred to in this clause, which are beyond the reasonable control of the Bank or its third parties performing such services as sub-contractor to the Bank and could not have been prevented by reasonable precautions (each, a "Force Majeure Event"), then the Bank shall be excused from such performance to the extent of and during the period of such Force Majeure Event. The Bank shall exercise all reasonable commercial efforts to continue to perform its obligations hereunder.
            </p>
            <p>
              22.5 Notwithstanding the foregoing, such terms of this Agreement as are meant to survive termination / expiry of this Agreement, will survive indefinitely unless and until the Bank chooses to terminate them.
            </p>
            <p>22.6 If You have any questions or concerns about this Agreement, please send us a thorough description to care@freecharge.in. We strive to respond to all User requests.</p>

            <p>
              22.7 If You have any questions or claims of copyright infringement, please contact us at care@freecharge.in
            </p>
          </div>
</li>
    <li>
      <h3 class="cbp-nttrigger">
        Terms and Conditions for Single Sign On Services 
      </h3>
      <div class="cbp-ntcontent">
        <p>Last Updated on January 17,2018</p>
        <p>THESE TERMS OF USE ("AGREEMENT") IS AN ELECTRONIC RECORD IN THE FORM OF AN ELECTRONIC CONTRACT FORMED UNDER INFORMATION TECHNOLOGY ACT, 2000 AND RULES MADE THEREUNDER AND THE AMENDED PROVISIONS PERTAINING TO ELECTRONIC DOCUMENTS / RECORDS IN VARIOUS STATUTES AS AMENDED BY THE INFORMATION TECHNOLOGY ACT, 2000. THIS AGREEMENT DOES NOT REQUIRE ANY PHYSICAL, ELECTRONIC OR DIGITAL SIGNATURE.</p>
        <p>THIS AGREEMENT IS A LEGALLY BINDING DOCUMENT BETWEEN YOU (DEFINED BELOW) AND FREECHARGE PAYMENT TECHNOLOGIES PRIVATE LIMITED, A COMPANY INCORPORATED UNDER COMPANIES ACT, 2013 HAVING ITS REGISTERED OFFICE AT 68, OKHLA INDUSTRIAL ESTATE, PHASE III, NEW DELHI - 110020 ("FREECHARGE"). </p>
        <p>THE TERMS OF THIS AGREEMENT WILL BE EFFECTIVE UPON YOUR ACCEPTANCE OF THE SAME (DIRECTLY OR INDIRECTLY IN ELECTRONIC FORM OR BY MEANS OF AN ELECTRONIC RECORD) AND WILL GOVERN THE RELATIONSHIP BETWEEN YOU AND FREECHARGE FOR THE USE OF SINGLE-SIGN-ON SERVICES (DEFINED BELOW).</p>
        <p>THIS DOCUMENT IS PUBLISHED AND SHALL BE CONSTRUED IN ACCORDANCE WITH THE PROVISIONS OF RULE 3 (1) OF THE INFORMATION TECHNOLOGY (INTERMEDIARIES GUIDELINES) RULES, 2011 UNDER INFORMATION TECHNOLOGY ACT, 2000.</p>
        <p>By accessing Website or Mobile Applications, for registering or for using our Services, You are agreeing to the Terms and Conditions set out below and will be bound by them. Unless the context requires otherwise the terms defined shall have the same meaning attributed to them in these Terms and Conditions.</p>
        <h4>By agreeing to these Terms and Conditions, You represent the following:</h4>
        <p>a. You are eighteen (18) years old or older. If You are below the age of eighteen (18) years, please read through these Terms and Conditions with Your parent or legal guardian, and only use the Services with the permission of Your parent or legal guardian in accordance with the applicable laws.<br/>
          b.  Capable of entering into a legally binding agreement.<br/>
          c.  You have not been previously suspended or removed from any services availed under Freecharge account.<br/>
          d.  You have valid and subsisting right, authority, and capacity to enter into these Terms and Conditions and to abide by all of the terms and conditions contained herein.<br/>
        e.  You are not impersonating any person or entity, or falsely stating or otherwise misrepresenting identity, age or affiliation with any person or entity.</p>

        <p>All references in this Terms and Conditions to "You" or "Your" shall refer to the users or Customer of Website or Merchant site(s). All references in this Terms and Conditions to "We," "Us," or "Our" shall refer to Freecharge.</p>

        <p><strong>1. Definitions</strong>In this document, unless the context otherwise requires, the following word and phrases shall have the meanings ascribed to them hereunder:</p>
        <p>a."<strong>Affiliate Site(s)</strong>" shall mean webpages, websites, mobile sites, mobile application and online platforms of Accelyst Solutions Private Limited (https://www.freecharge.in) prepaid payment account provided by Axis Bank Limited, and any other affiliate as may be determined by Freecharge from time to time.</p>
        <p>b. "<strong>Customer</strong>" means the registered user of Freecharge account.</p>
        <p>c. "<strong>Freecharge account</strong>" shall mean the account created by the Customer using Single-Sign-On Services provided by Freecharge which would give the Customer access to Affiliate Site(s) and Merchant Site(s).</p>
        <p>d. "<strong>Merchant(s)</strong>" shall mean seller(s), retailers or e-commerce marketplaces or such other business entities which allow Customers to use Freecharge account at their website, mobile site, mobile application or establishments.</p>
        <p>e. "<strong>Merchant Site(s)</strong>" shall mean webpage, websites, mobile sites, mobile application and online platform of Merchant(s)</p> 
        <p>f. "<strong>Mobile Application(s)</strong>" shall mean the software application(s) which inter alia, enables Freecharge account through the mobile-device.</p>
        <p>g. "<strong>Single-Sign-On Services</strong>" or "Services" shall mean a session/user authentication process that permits a user to enter his/her name or mobile number or e-mail address or any combination thereof and password in order to access Affiliate Site(s).</p>
        <p>h. "<strong>Terms and Conditions</strong>" shall mean these terms and conditions.</p>
        <p>i. "<strong>Website</strong>" shall mean website having the domain https://www.freecharge.in, mobile site, Mobile Application(s) or online interphases, through which Freecharge shall among other things enable Single-Sign-On Services.</p>

        <p><strong>2. REGISTRATION AND SINGLE-SIGN-ON SERVICES</strong></p>
        <p>a. A user desirous of availing the Freecharge account should register for Freecharge account using Single-Sign-On Services at any of the Affiliate Site(s) or Merchant Site(s) and accept these Terms & Conditions.</p>
        <p>b. Freecharge among other things offers Single-Sign-On Services to users. Single-Sign-On Services offer Customers the possibility, after a one-off registration on one of the Affiliate Sites to sign on to all the Affiliate Sites or Merchant Site(s) with the Sign On data. Single-Sign-On Services are enabled for all the users registering (signing up) at Affiliate Site(s). Single-Sign-On Services are also enabled for Customers at Affiliate Site(s) at the time of signing in or otherwise. Freecharge is however entitled to reject users or Customers for Single-Sign-On Services without giving reasons. Single-Sign-On Services may be extended by Freecharge for registration on Merchants' sites (other than Affiliate Site(s)) with the consent of users and Customers.</p>
        <p>c. Freecharge shall have a right, not to consider or reject any request for registration, without assigning any reason.</p>
        <p>d. In order to allow use of Single-Sign-On Services, Freecharge may require that You submit certain personal information, such as Your name, address, mobile phone number, e-mail address, date of birth, and permanent account number (PAN) to Freecharge ("Data" or "Sign On Data"). During the registration process, or when You access the Freecharge account through Single-Sign-On Services from a Mobile Application, Your mobile phone's device ID may be stored. You agree that the Data You provide upon registration and at all other times will be true, accurate, current and complete. You agree to maintain and update this Data in case of any changes or inconsistencies.</p>
        <p>e. Registration can also be completed by means of particular social media networks that are offered. This requires that You already have a user account at the relevant social media network. If You use this feature of the social media network, the terms and conditions of use and data privacy policies of that particular social media network shall also apply. Freecharge may acquire Your certain data or information from such social media network to the extent permitted by such social media network, and such data or information then will be subject to Freecharge Privacy Policy.</p>

        <p> <strong>3. USAGE AND DISCLOSURE OF DATA</strong></p>
        <p>a. Freecharge may share some or all information provided by You with Affiliate Site(s), Merchants or any other third parties as may be necessary for the efficient provision of Single-Sign-On Services by Freecharge. You hereby authorise Freecharge to provide such information to such third parties, provided that, Freecharge may, irrespective whether required under law or otherwise, seek additional information or documents from You on behalf of itself or such third parties, for verifying any information provided during the registration. Data available with Freecharge or otherwise obtained from the Website will not be used for any unauthorized and unlawful purpose.</p>
        <p>b. The Affiliate Sites may have their respective independent user terms and conditions and privacy policy which are independent of these terms and conditions. For the Single-Sign-On Services these Terms and Conditions apply exclusively, whereas for the use of the Affiliate Sites their business, user terms and conditions and privacy policy will apply.</p>
        <p>c. The Sign On Data is intended solely for personal use by the Customer and therefore always to be kept secret and safe. The Customer is not entitled to share his Sign On Data details with third parties to use the Single-Sign-On services or to disclose them otherwise. The user is obliged to inform us immediately on becoming aware of and/or suspecting a case of any unauthorized use, disclosure and / or misuse of their Sign On Data or of their user account. Furthermore, the user is obliged not to use the Sign On Data or user account of another person.</p>
        <p>d. You agree and acknowledge that You are providing your Information out of your free will. You have an option not to provide or permit Freecharge to collect Your Information or later on withdraw Your consent so provided herein by sending an email to the grievance officer or such other electronic address of Freecharge as may be notified to You. In such case, You should neither visit any Website nor use any services provided by Freecharge nor shall contact Freecharge. In case You chose not to provide or later on withdraw Your consent, Freecharge reserves its right not to provide Services for which the said information was sought.</p>
        <p>e. You hereby agree and acknowledge that in the event of non- compliance of the Terms and Conditions for usage and access to Single-Sign-On Services, Freecharge has the right to terminate Your access or usage limits and also have the authority to remove any/all non-compliant information.</p>
        <p>f. Freecharge may at its sole discretion, utilize the services of external service providers/or agents and on such terms as required or necessary, in relation to its Single-Sign-On Services.</p>
        <p>g. You shall be solely responsible for maintaining the confidentiality, prevention of unauthorised use or any other breach of security of the Freecharge account and Single-Sign-On Services and shall ensure that You take all necessary steps towards safekeeping of Sign On Data and authorized use of the Freecharge account and Single-Sign-On Services. You agree to (a) immediately notify Freecharge of any unauthorized use of Your Sign On Data or Freecharge account or any other breach of security, and (b) ensure that You securely exit from Your Freecharge account at the end of each session.</p>
        <p><strong>4. TERMINATION OF SERVICES</strong></p>
        <p>a. Freecharge may suspend, restrict, discontinue or deny access to or terminate the Services for, including but not limited to, any of the following actions:</p>
        <ul>
          <li>Impersonating another person or entity with the effect of misleading users about the identity of the creator of content (including misleading account user names, voices and sounds used in broadcasts, and account profile information);</li>
          <li>impersonating the Services or a employees and representatives of Freecharge;</li>
          <li>representing that content You post or transmit through the Services is created or endorsed by us;</li>
          <li>providing or sending any content which may be harmful or detrimental to Freecharge or its business associates, or which violates any restriction or policy established by Freecharge, its affiliates, Merchants or its business associates;</li>
          <li>submission of any content that falsely expresses or implies that such content is sponsored or endorsed by us;</li>
          <li>publishing on or transmitting from Freecharge's system any content that contains software viruses, trojan horses or any other computer code, files, or programs designed to interrupt, destroy, or limit functionality of any computer software or hardware or telecommunications equipment;</li>
          <li>manipulating Service identifiers in order to disguise the origin of any content transmitted via the Services;</li>
          <li>collecting or storing personal data about other users;</li>
          <li>interfering with or disrupting the Service or servers or networks connected to the Service, or disobeying any requirements, procedures, policies or regulations of networks connected to the Service;</li>
          <li>using any robot, spider, website search/retrieval application, or other device to retrieve or index any portion of the Service or collect information about users for any unauthorized purpose;</li>
          <li>creating user accounts by automated means or under false or fraudulent pretences;</li>
          <li>if Freecharge, in its reasonable opinion, that cessation/ suspension is necessary.</li>
        </ul>
        <p><strong>5. ADDITIONAL TERMS</strong></p>
        <p>a. Certain Services on the Website may have additional terms (such as policies, guidelines, and rules) that will further govern Your use of that particular Service and supplement these Terms. If You choose to register for or access any such Services, You will be presented with any relevant additional terms and conditions at that time. By using those Services, You agree to comply with such additional guidelines and rules.</p>
        <p><strong>6. FEES & CHARGES</strong></p>
        <p>a.   Freecharge reserves the right at any time to charge the Customer any fees / charges for using Single-Sign-On Services. However, such fees / changes will be applicable only with prospective effect giving prior notice to the Customer.</p>
        <p><strong>7. DISCLAIMER OF WARRANTIES AND LIMITATION OF LIABILITIES</strong></p>
        <p>a. Website, Single-Sign-On Services and all Information, content, materials, and services included on or otherwise made available to You through Website, or Single-Sign-On Services are provided by Freecharge on an "as is" and "as available" basis. Unless otherwise specified in writing, Freecharge makes no representations or warranties of any kind, express or implied, as to the operation of Website, Single-Sign-On Services, information, any application, content including without limitation: (A) any implied warranties of merchantability, satisfactory quality, suitability, reliability, completeness, performance, fitness for a particular purpose, title, or non-infringement; (B) that Website, or Single-Sign-On Services will meet Your requirements, will always be available, accessible, uninterrupted, timely, secure, operate without error, or will contain any particular features or functionality; (C) any implied warranty arising from course of dealing or trade usage, unless otherwise specified in writing. You expressly agree that Your use of Website and/or Single-Sign-On Services is at Your sole risk and are using Your best and prudent judgment before entering into any transactions through Website and/or Single-Sign-On Services; (D) electronic communications sent through Website and/or Single-Sign-On Services are free of viruses or other harmful components.</p>
        <p>b. Without limiting the foregoing, in no event will We or any of our Affiliates be liable to You for any failure or delay by us (or our employees, agents, or representatives) in performing our obligations under this Agreement, where such failure or delay is caused by abnormal and unforeseeable circumstances beyond our control, the consequences of which would have been unavoidable despite all reasonable efforts to the contrary, or where We are bound by other legal obligations covered by applicable law.</p>
        <p>c. To the fullest extent permitted by applicable law, We and our affiliates (and our and their respective employees, directors, agents and representatives) will not be liable for any indirect, incidental, punitive, or consequential damages arising out of or in connection with these Terms and Conditions, Single-Sign-On Services (including the inability to use the services). In addition, and without limiting the foregoing, to the fullest extent permitted by applicable law, We and our affiliates (and our and their respective employees, directors, agents and representatives) will not be liable for any damages arising out of or in connection with any services or products purchased or transactions entered into through the Services. To the fullest extent permitted by applicable law, in no event will our or our Affiliates's (and our and their respective employees, directors, agents and representatives) aggregate liability arising out of or in connection with this agreement or the transactions contemplated hereby, whether in contract, tort (including negligence, product liability or other theory), warranty, or otherwise, exceed INR 100/- (Indian Rupees One Hundred only).</p>
        <p><strong>8. INDEMNIFICATION</strong></p>
        <p>a. You agree to defend, indemnify and hold Freecharge, and its affiliates and their respective officers, directors, agents and employees, harmless from and against any claims, actions, demands, liabilities, judgments, and settlements, including without limitation, reasonable legal fees resulting from or alleged to result from breach of Your representations, warranties, or obligations set forth in these Terms and Conditions; or any actual or alleged infringement, misappropriation or violation of any third-party rights or applicable law by Your trademarks used in connection with the or using the Website and/or Service; or any transaction submitted by You through the Website and/or Service.</p>
        <p><strong>9. MODIFICATION</strong></p>
        <p>a. We reserve the right to make changes to Website, policies, and these Terms and Conditions at any time, in our sole discretion. Your use of the Website following any such modification constitutes Your agreement to follow and be bound by the Terms and Conditions as modified. Any additional terms and conditions, disclaimers, privacy policies and other policies applicable in general and/ or to specific areas of this Website or to particular Service are also considered as Terms and Conditions.</p>
        <p><strong>10. ELECTRONIC COMMUNICATIONS AND NOTICE</strong></p>
        <p>a. When You use the Website or send emails or other data, information or communication to Freecharge, You agree and understand that You are communicating with Freecharge through electronic records and You consent to receive communications via electronic records from Freecharge periodically and as and when required. Freecharge will communicate with You by email or by notices on Website or electronic records on the Website or on Your mobile number which will be deemed adequate service of notice / electronic record to the maximum extent permitted under any applicable law,</p>
        <p>b. All notices with respect to this Agreement from Freecharge will be served to You by email or by general notification on the Website. Any notice provided to Freecharge pursuant to these Terms and Conditions should be sent to Grievance Officer at "grievanceofficer@freecharge.com".</p>
        <p><strong>11. GENERAL PROVISIONS</strong></p>
        <p>a. These Terms shall be governed by and shall be construed in accordance with the laws of India ("<strong>Applicable Laws</strong>"). All disputes relating to these Terms shall be settled in the courts located at New Delhi, India. Each party hereby irrevocably waives any objection which such party may now or hereafter have to the laying of improper venue or forum non convenience. Each party agrees that a judgment in any such action or proceeding may be enforced in other jurisdictions by suit on the judgment or in any manner provided by law. Any and all service of process and any other notice in any such suit, action or proceeding with respect to these terms and conditions shall be effective against a party if given as provided herein.</p>
        <p>b.  <strong>Assignment:</strong>: You cannot assign or otherwise transfer this Agreements, or any rights granted hereunder or any obligations, to any third party and any such assignment or transfer or purported assignment or transfer shall be void ab initio. Freecharge's rights and/or obligations under the Agreement are freely assignable or otherwise transferable by Freecharge to any third parties without the requirement of seeking Your prior consent. Freecharge may inform You of such assignment or transfer in accordance with the notice requirements under the Agreement. Freecharge shall have right to transfer Your Freecharge account and Information to a third party who purchases Freecharge's business as conducted under the Website.</p>
        <p>c.<strong> Severability:</strong>: If, for any reason, a court of competent jurisdiction finds any provision of the Agreement, or portion thereof, to be unenforceable, that provision shall be enforced to the maximum extent permissible so as to give effect to the intent of the parties as reflected by that provision, and the remainder of the Agreement shall continue in full force and effect. Freecharge may amend in a reasonable manner such provision to make it enforceable and such amendment will be given effect in accordance with the amendment terms of this Agreement.</p>
        <p>d.<strong>Waiver:</strong> Any failure or delay by a party to enforce or exercise any provision of the Agreement, or any related right, shall not constitute a waiver by such party of that provision or right. The exercise of one or more of a party's rights hereunder shall not be a waiver of, or preclude the exercise of, any rights or remedies available to such party under this Agreement or in law or at equity. Any waiver by a party shall only be made in writing and executed by a duly authorized officer of such party.</p>
        <p>e.  <strong> Force Majeure:</strong> If performance of any service or obligation under this Agreement by Freecharge is prevented, restricted, delayed or interfered with by reason of labour disputes, strikes, acts of God, floods, lightning, severe weather, shortages of materials, rationing, utility or communication failures, computer system hacking, cyber-attack, viruses  earthquakes, war, revolution, acts of terrorism, civil commotion, acts of public enemies, blockade, embargo or any law, order, proclamation, regulation, ordinance, demand or requirement having legal effect of any government or any judicial authority or representative of any such government, or any other act whatsoever, whether similar or dissimilar to those referred to in this clause, which are beyond the reasonable control of Freecharge or its third parties performing such services as sub-contractor to Freecharge and could not have been prevented by reasonable precautions (each, a "Force Majeure Event"), then Freecharge shall be excused from such performance to the extent of and during the period of such Force Majeure Event. Freecharge shall exercise all reasonable commercial efforts to continue to perform its obligations hereunder.</p>
        <p>f. Notwithstanding the foregoing, such terms of this Agreement as are meant to survive termination/expiry of this Agreement, will survive indefinitely unless and until Freecharge chooses to terminate them.</p>
        <p><strong>12. Grievance Redressal:</strong> a.  In compliance with Information Technology Act, 2000 and the rules made thereunder, the Grievance Officer of Freecharge for the purpose of this Agreement shall be Nemash Simaria contactable on email address: grievanceofficer@freecharge.com. Freecharge may change the aforesaid details from time to time under intimation to You.</p>
      </div>
    </li>
    <li>
      <h3 class="cbp-nttrigger">Terms and Conditions for Mutual Fund distribution
      </h3>

      <div class="cbp-ntcontent">
        <p>
          Accelyst Solutions Pvt. Ltd. ("Accelyst" or "the Company") [ARN: 116600], is a company incorporated in India under the Companies Act, 1956. Accelyst presently has its registered office at Unit No. 2A, 2nd Floor, Vaibhav Chambers, Bandra Kurla Complex, Bandra (E), Mumbai - 400051, Maharashtra, India. These Terms and Conditions apply to Users. Accelyst owns and maintains the website i.e www.freecharge.com and Freecharge mobile application (the "Website") and offers you access to and use of the Website and the opportunity to avail of the services offered on the Website on and subject to the Terms and Conditions set out below and elsewhere in the Website (hereinafter referred to as the "Terms and Conditions").
        </p>
        <p>
          In the event of any inconsistency between the Terms and Conditions and the Privacy Policy, the Terms and Conditions supersede the Privacy Policy. Any User of the Services is deemed to have agreed to the present Terms and Conditions and has entered into a legally binding contract with the Company. "User" or "You" means any person who access or avail Services. By accessing the Website or service and/or by clicking "I agree", you agree to be bound by these Terms and Conditions and any amendment thereof.
        </p>
        <p>
          <strong>1. Description and Acceptance of Services</strong>
        </p>
        <p>
          These Terms and Conditions read together with the Privacy Policy of this Website shall constitute a legally binding agreement between you and the Company regarding your use of the Website and any services offered by the Company. The Company, hereby offers you with access to information primarily about certain financial products/services including, but not restricted, to mutual funds (the "Services"). The above said Services are provided on a commercially reasonably effort basis and you agree that your participation for availing the Services is purely at your will and consent.
        </p>
        <p>
          Your continued usage of the Services and ancillary services from time to time would also constitute acceptance of the Terms including any updation or modification thereof and you would be bound by this Agreement until this Agreement is terminated as per provisions defined herein.
        </p>
        <p>
          You agree and authorize the Company to share your information with its group companies and other third parties, in so far as required for joint marketing purposes/offering various services/report generations and/or to similar services to provide you with various value added services, in association with the Services or otherwise. You agree to receive communications through emails, telephone and/or sms, from the Company or its third party vendors/business partners regarding the Services updates, information/promotional emails and/or product announcements ("Alerts"). In this context, you agree and consent to receive such Alerts with respect to the deals, services and other facilities available on the Website.
        </p>
        <p>
          Company will retain and use your information as necessary to comply with our legal obligations, resolve disputes and enforce our agreements entered into for providing Services.
        </p>
        <p>
          <strong>2. Eligibility</strong>
        </p>
        <p>
          You hereby represent and warrant to the Company that you are an Indian resident and at least eighteen (18) years of age or above and are capable of entering, performing and adhering to these Terms and Conditions and that you agree to be bound by the following Terms and Conditions. While individuals under the age of 18 may utilize the Services, they shall do so only with the involvement &amp; guidance of their parents and / or legal guardians, under such parent /legal guardian's registered account. In case of a corporation, trust, association of persons or a firm, you must be authorised to agree to the Terms and Conditions and to access, use and avail of the Services. If you do not qualify, please do not access or use the Services. This Website is intended exclusively for the use of Resident Indians.
        </p>
        <p>
          <strong>3. Conduct of Users</strong>
        </p>
        <p>
          3.1 By agreeing to the Terms &amp; Conditions, the User agrees the following:
        </p>
        <p>
          a) to have read and understood the contents of the offer document(s) / scheme information document and addendum(s) thereto of the respective scheme(s) and agree to abide by the terms, conditions, and the provisions of the scheme(s) as applicable from time to time.
        </p>
        <p>
          b) to have understood the details of the scheme and have not received nor been induced by any rebate or gifts, directly or indirectly, in making any investment.
        </p>
        <p>
          c) the Customer(s) hereby declare that the amount being invested by them in the scheme(s) of mutual funds are derived through legitimate sources and are not held or designed for the purpose of contravention of any Act, Rules, Regulations or any statute or legislation or any other applicable laws or any Notifications, Directions issued by any governmental or statutory authority from time to time.
        </p>
        <p>
          d) to confirm that the details provided by the User(s) are true and correct.
        </p>
        <p>
          3.2 You shall not
        </p>
        <p>
          a) Restrict or inhibit any other person from accessing, using and enjoying the Website and/or Services;
        </p>
        <p>
          b) Use the Website and/or Services for any purpose that is unlawful in any jurisdiction or not permitted by the Terms and Conditions; modify, copy, distribute, transmit, display, perform, publish, license, create derivative works from, transfer or sell any information, designs, logos, trademarks, software, facilities, products or services obtained on or through the Website and/or Services, except as permitted by the copyright owner or other right holder thereof; post, transmit and receive any unlawful, fraudulent, libelous, defamatory, obscene, pornographic, profane, threatening, abusive, hateful, offensive, or otherwise objectionable information or statement of any kind including, without limitation, any information or statement constituting or encouraging conduct that would constitute sedition, a criminal offence, give rise to civil liability, or otherwise violate any local, state, national, foreign or other law; post, transmit any spam mails, information or software which contains a virus, Trojan horse, worm or other harmful component; post, publish, transmit, reproduce, distribute or in any way exploit any information, software or other material obtained from or through the Website and/or Services for commercial purposes (other than as expressly permitted by the provider of such information, software or other material);
        </p>
        <p>
          c) Attempt to decompile or reverse engineer any of the software available on the Website
        </p>
        <p>
          d) You will not make any attempt to hack into the Website or otherwise attempt to subvert any firewall or other security measure of the Website and if you become aware of any shortcoming in the security on the Website you shall forthwith inform Accelyst of the same in writing.
        </p>
        <p>
          <strong>4. Scheme document/ Offer document</strong>
        </p>
        <p>
          For information regarding a fund's investment objectives, load, expenses and risk considerations, please download the offer document/scheme document from the respective Mutual Fund's website or the link, if provided on this Website. Please read the offer document carefully before investing.
        </p>
        <p>
          <strong>5. Risk Factors</strong>
        </p>
        <p>
          Investments in mutual funds and securities are subject to market risks and the NAV of the schemes may fluctuate depending upon the factors and forces affecting the securities market including inter alia fluctuations in the interest rates. There can be no guarantee that a scheme's investment objectives will be achieved. All dividend distributions are subject to the investment performance of the schemes. The investments made by the schemes are subject to external risks.
        </p>
        <p>
          <strong>6. Disclaimers</strong>
        </p>
        <p>
          6.1  This Website is not intended to provide any tax, legal, insurance or investment advice, and nothing on the Website should be construed as a recommendation, by Accelyst or any third party, to acquire or dispose of any investment or security, or to engage in any investment transaction. Except as otherwise specified, you alone are solely responsible for determining whether any investment, security or any other product or service, is appropriate or suitable for you based on your investment objectives and personal and financial situation. You should consult a legal or tax professional regarding your specific legal or tax queries. Access and use of the Website and Services is entirely at your own risk. The Website, including any content or information on it, any related or linked site, products and services displayed, provided, availed of, licensed or purchased on, through or via the Website are provided "as is," without any representation or warranty of any kind, either express or implied, including without limitation, any representation or warranty for accuracy, continuity, uninterrupted access, timeliness, sequence, quality, performance, fitness for any particular purpose or completeness. Specifically,
        </p>
        <p>
          6.2 Accelyst disclaims any and all warranties including, but not limited to –
        </p>
        <p>
          a) Any warranties concerning the availability, accuracy, usefulness, or correctness, currency or completeness of information, products or services and
        </p>
        <p>
          b) Any warranties of title, warranty of non-infringement, freedom from computer virus, warranties of merchantability or fitness for a particular purpose, other than those warranties which are incapable of exclusion, restriction or modification under the laws applicable to the Terms and Conditions.
        </p>
        <p>
          c) Accelyst has not verified and shall not be liable or responsible for any content or other information on the Website or on web-sites linked to or with Accelyst. Accelyst does not, in any way, certify or warrant the performance, operation, content or availability of the Website or such other websites. Although Accelyst adopts security measures, which it considers appropriate for the Website, it does not assure or guarantee that no person will overcome or subvert the security measures and gain unauthorised access to the Website. Services under these Terms and Conditions is neither a solicitation to invest in any product nor to avail of a particular service. Accelyst operates and offers the Services strictly on a no-liability basis and Accelyst shall not be liable to you or any other third party for any direct, indirect, incidental, special, exemplary, punitive, consequential or other damages (including without limitation loss of profits, loss or corruption of data, loss of goodwill, work stoppage, computer failure or malfunction, or interruption of business) under any contract, negligence, strict liability or other law or theory arising out of or in connection with the Website, products or services mentioned or advertised on or accessed or availed on or through the Website or any contract or transaction entered into or executed in pursuance thereof (however arising, including negligence) or resulting from the use of or inability to use, access or avail of the Website, any service or product or out of any breach of any warranty. Under no circumstances shall Accelyst be liable for any damages whatsoever whether such damages are direct, indirect, incidental consequential and irrespective of whether any claim is based on loss of revenue, investment, production, goodwill, profit, interruption of business or any other loss of any character or nature whatsoever and whether sustained by you or any other person. If any disclaimers or limitation of liability in the Terms and Conditions are held to be unenforceable, the maximum liability of Accelyst to you shall not exceed the amount of fees paid by you to Accelyst for the products or services that you have ordered or availed of on or through the Website. Any search results displayed by or on the Website are automated and cannot be screened. Accordingly, Accelyst assumes no responsibility for the accuracy or otherwise of any search results or of the content of any site included in the search results or otherwise linked to the Website. A possibility exists that the Website could include inaccuracies or errors. Additionally, a possibility exists that unauthorised additions, deletions or alterations could be made by third parties to the Website. Although Accelyst attempts to ensure the integrity of the Website, they make no guarantee whatsoever as to its sequence, timeliness, completeness, correctness or accuracy. In the event that such an inaccuracy or incompleteness arises, please inform Accelyst so that it can be corrected. Links from the Website clicking on certain portions or links within the Website might take you to other websites without any intimation or indication of doing or having done so. The linked websites are not under the control of Accelyst and Accelyst assumes no responsibility whatsoever for such other websites whether as to content, availability, performance or otherwise. Accelyst also does not represent or warrant that these links shall operate satisfactorily. Accelyst provides these links only as a convenience and links to external web sites do not constitute an endorsement by Accelyst of such other sites, the sponsors of such sites or the content, products, advertising or other materials presented on or by such sites. Accelyst shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with the access or use of such other websites or reliance on or availing of any content, goods or services available on such other sites. For informational or educational purposes, or as a convenience to the users, the Accelyst Website may provide referrals to third party content or links to third party websites, including companies that have a relationship with Accelyst. Our decision to provide referral information to a third party or to pass on an offer made by a third party or to provide a link from Accelyst Website to a third party website shall not be construed as an endorsement of the third party products, services, content, tools and information. You will use third-party content at your own risk.
        </p>
        <p>
          <strong>7. Proprietary and Intellectual Property Rights</strong>
        </p>
        <p>
          Accelyst is the owner and/or authorized user of any trademark, registered trademark and/or service mark appearing on the Website, and is the copyright owner or licensee of the content and/or information on the Website including but not limited to any text, images, illustrations, audio clips, video clips and screens appearing on the Website. All rights on the Website are reserved and you may not download and/or save a copy of the Website or any part thereof including any of the screens or part thereof and/or reproduce, store it in a retrieval system or transmit it in any form or by any means - electronic, electrostatic, magnetic tape, mechanical printing, photocopying, recording or otherwise including the right of translation in any language without the express permission of Accelyst (except as otherwise provided on the Website or in the Terms and Conditions for any purpose) or use it in any manner that is likely to cause confusion or deception among persons or in any manner disparages or discredits Accelyst or Partners.
        </p>
        <p>
          <strong>8. Authority to Accelyst</strong>
        </p>
        <p>
          You irrevocably and unconditionally authorise Accelyst to access all information relating to you (including personal information and information relating to access and use of the Website by you and the transactions entered into by you). Subject to the Privacy Policy, all information submitted on or via the Website shall be deemed to be and remain the property of Accelyst and the Partners; and Accelyst, the Affiliate Partners shall be free to use, for any purpose, any ideas, concepts, know-how or techniques contained in any information you may provide to or through the Website. Accelyst, the Partners shall not be subject to any obligations of confidentiality regarding submitted information except as otherwise expressly agreed by it directly with you. Accelyst shall be deemed to acquire from you a non-exclusive, world-wide, perceptual, irrevocable, royalty free licence to use, adapt, reproduce, modify, publish, translate, create derivative works from, distribute, perform or display any ideas, concepts, know-how or techniques contained in any information provided by you to or through the Website.
        </p>
        <p>
          <strong>9. No Agency</strong>
        </p>
        <p>
          The Terms and Conditions and your use of or access to the Website are not intended to create an agency, partnership, joint-venture or employer-employee relationship between you and the Website, Accelyst, any Partner or any Insurance company, except where otherwise specifically agreed or appointed.
        </p>
        <p>
          <strong>10. Indemnity</strong>
        </p>
        <p>
          You agree to defend, indemnify and hold Accelyst, its directors, officers, employees, Affiliates Partners, and Content harmless from any and all claims, liabilities, damages, costs, expenses and proceedings, including reasonable attorneys' fees, arising in any way from any violation of the Terms and Conditions by you or users of your of your ID and password, and any claims dispute or differences between you and any supplier.
        </p>
        <p>
          <strong>11. Termination</strong>
        </p>
        <p>
          You acknowledge and agree that Accelyst may, without notice, suspend or terminate your ID, password or account or deny you access to all or part of the Services and/or Website without prior notice if you engage in any conduct or activities that Accelyst in its sole discretion believes violate any of the Terms and Conditions, violate the rights of Accelyst, or is otherwise inappropriate for continued access.
        </p>
        <p>
          <strong>12. Governing Law &amp; Jurisdiction</strong>
        </p>
        <p>
          The Website, the Services, the Terms and Conditions, all transactions entered into on or through the Website and the relationship between you and Accelyst shall be governed by and construed in accordance with the laws of India and no other nation, without regard to the laws relating to conflicts of law. You and Accelyst agree that all claims, differences and disputes arising under or in connection with or pursuant to the Website, the Terms and Conditions, any transactions entered into on or through the Website or the relationship between you and Accelyst shall be subject to the exclusive jurisdiction of the competent courts located in the city Mumbai, India and you hereby accede to and accept the jurisdiction of such courts. Provided that, notwithstanding what is stated above, if Accelyst so thinks fit Accelyst may institute proceedings against you in any other court or tribunal having jurisdiction. Accelyst accepts no liability whatsoever, direct or indirect for non-compliance with the laws of any country other than that of India.
        </p>
        <p>
          <strong>13. Notices</strong>
        </p>
        <p>
          Accelyst may give notice to you by e-mail, letter, telephone or any other means as Accelyst may deem fit to the address last given by you. Notices under the Terms and Conditions may be given to Accelyst by you in writing by delivering them by hand or by sending them by post to Accelyst's address mentioned on the Website.
        </p>
        <p>
          <strong>14. Grievance Redressal</strong>
        </p>
        <p>
          Redressal Mechanism: Any complaints, abuse or concerns with regards to content and or comment or breach of these terms shall be immediately informed in writing to Grievance Officer of Accelyst at the registered office of the Company
        </p>
        <p>
          <strong> Additional Terms and Conditions of Reliance Nippon Life Asset Management Company Limited: </strong>
        </p>
        <p>
          I/We would like to invest in Reliance Mutual Fund products subject to terms of the Statement of Additional Information (SAI), Scheme Information Document (SID), Key Information Memorandum (KIM) and subsequent amendments thereto. I/We have read, understood (before filling application form) and is/are bound by the details of the SAI, SID &amp; KIM including details relating to various services including but not limited to Reliance Any Time Money Card. I/We have not received nor been induced by any rebate or gifts, directly or indirectly, in making this investment. I / We declare that the amount invested in the Scheme is through legitimate sources only and is not designed for the purpose of contravention or evasion of any Act / Regulations / Rules / Notifications / Directions or any other Applicable Laws enacted by the Government of India or any Statutory Authority. I accept and agree to be bound by the said Terms and Conditions including those excluding/ limiting the Reliance Nippon Life Asset Management Limited (formerly Reliance Capital Asset Management Limited) (RNLAM) liability. I understand that the RNLAM may,at its absolute discretion, discontinue any of the services completely or partially without any prior notice to me. I agree RNLAM can debit from my folio for the service charges as applicable from time to time. The ARNholder has disclosed to me/us all the commissions (in the form of trail commission or any other mode), pay able to him for the different competing Schemes of various Mutual Funds from amongst which the Scheme is being recommended to me/us. I hereby declare that the above information is given by the undersigned and particulars given by me/us are correct and complete. Further, I agree that the transaction charge (if applicable) shall be deducted from the subscription amount and the said charges shall be paid to the distributors. I confirm that I am resident of India. I hereby declare that the information provided in the Form is in accordance with section 285BA of the Income Tax Act, 1961 read with Rules 114F to 114H of the Income Tax Rules, 1962 and the information provided by me /us in the Form, its supporting Annexures as well as in the documentary evidence provided by me/us are, to the best of our knowledge and belief, true, correct and complete.

        </p>
      </div>
    </li>
    <li>
      <h3 class="cbp-nttrigger">Terms and Conditions for Freecharge EMI
      </h3>

      <div class="cbp-ntcontent">
        <p>
          <strong> (A) Freecharge EMI Terms of Service </strong>
        </p>
        <p>
          PLEASE READ THESE TERMS OF SERVICE CAREFULLY. BY ACCESSING THE FREECHARGE EMI OR USING THE FREECHARGE EMI SERVICE ('SERVICE'), IN ADDITION TO THE TERMS AND CONDITIONS MENTIONED ON <a href="/termsandconditions">https://www.freecharge.in/termsandconditions</a> YOU AGREE TO BE BOUND BY THE (1) FREECHARGE EMI TERMS OF SERVICE (2) CASHCARE TERMS OF SERVICE, (3) CASHCARE PRIVACY POLICY (4) CASHCARE CIBIL CHECK. IF YOU DO NOT AGREE WITH THESE TERMS, PLEASE DO NOT ACCESS THE FREECHARGE EMI OR USE THE FREECHARGE EMI SERVICE.
        </p>
        <p>
          1. The Freecharge EMI Service ('Service') is provided by Accelyt Solutions Private Limited ('Accelyst') in partnership with CashCare Technology Pvt. Ltd ('Cashcare').
        </p>
        <p>
          2. In order to avail the Service, the Customer needs to select 'Freecharge EMI' option at the time of payment.
        </p>
        <p>
          3. Final EMI is calculated on the total value of your order at the time of payment.
        </p>
        <p>
          4. Cashcare charges annual interest rates according to the reducing monthly balance. In the monthly reducing cycle, the principal is reduced with every EMI and the interest is calculated on the outstanding balance.
        </p>
        <p>
          5. The minimum order value to avail the EMI payment option shall be shown on the transaction confirmation page.
        </p>
        <p>
          6. In case of any kind of refund in an EMI transaction, interest already billed in a particular transaction will not be refundable under any circumstances.
        </p>
        <p>
          7. The cancellation, refund or pre-closure shall be as per the refund policy. Refund Policy- Any refund initiated before 15 days from the date of transaction will be interest free. For refunds initiated after 15 days, the interest amount accrued until the next due EMI date will be charged. The interest amount will be debited on the due date of the EMI. If any EMI is already paid, refund for same shall be made after deducting interest amount, if applicable. The relevant lender/bank may charge processing fees or cancellation charges as per their internal policies. Such charges shall be paid by you to the relevant lender/bank. The lender/bank will not be obligated to refund processing fees charged, if any.
        </p>
        <p>
          8. For any complaints/queries regarding the Freecharge services kindly write to care@freecharge.com. For any complatins/queries regarding the cashcare services kindly write to contact@cashcare.in.
        </p>
        <p>
          9. CHARGES: Freecharge/Accelyst reserves the right to charge and recover from the Customer, service charges / other applicable charges for providing the Service. The Customer hereby authorises Freecharge/Accelyst to recover the service charge in a manner as Freecharge/Accelyst may deem fit along with such interest, if any, and/or suspension of the Service without any liability to Freecharge/Accelyst.
        </p>
        <p>
          10. AUTHORITY TO FREECHARGE/ACCELYST: Freecharge/Accelyst shall have no obligation to verify the authenticity of any transaction / instruction received or purported to have been received from the Customer. While Freecharge/Accelyst and/or Cashcare shall endeavour to carry out the instructions promptly, they shall not be responsible for any delay in carrying on the instructions due to any reason whatsoever, including due to failure of operational systems or any requirement of law. Freecharge/Accelyst may refuse to comply with the instructions without assigning any reason and shall not be under any duty to assess the prudence or otherwise of any instruction and have the right to suspend the Service if it has reason to believe that the Customer's  instructions will lead or expose to direct or indirect loss or may require an indemnity from the Customer.
        </p>
        <p>
          11. LIABILITY OF THE CUSTOMER AND FREECHARGE/ACCELYST: Neither Freecharge/Accelyst nor Cashcare shall be liable for any unauthorised transactions occurring through the use of the Service and the Customer hereby fully indemnifies and holds Freecharge/Accelyst and/or Cashcare harmless against any action, suit, proceeding initiated against it or any loss, cost or damage incurred by it as a result thereof. Freecharge/Accelyst shall under no circumstance be held liable to the Customer if the Service is not available in the desired manner for reasons including but not limited to natural calamities, legal restraints, faults in the telecommunication network or network failure, or any other reason beyond the control of Freecharge/Accelyst. Under no circumstances shall Freecharge/Accelyst be liable for any damages whatsoever whether such damages are direct, indirect, incidental consequential and irrespective of whether any claim is based on loss of revenue, interruption of business or any loss of any character or nature whatsoever and whether sustained by the Customer or by any other person. Illegal or improper use of the Service shall render the Customer liable for payment of financial charges as decided by Freecharge/Accelyst or will result in suspension of the Service.
        </p>
        <p>
          12. DISCLAIMER OF WARRANTIES: The Customer expressly agrees that use of the Freecharge website/ Service is at its sole risk. The Freecharge website/ Service is provided on an "as is" and "as available" basis. Except as warranted in the terms, Freecharge/Accelyst expressly disclaims all warranties of any kind, whether express or implied or statutory, including, but not limited to the implied warranties of merchantability, fitness for a particular purpose, data accuracy and completeness, and any warranties relating to non-infringement in the Service provided by Freecharge/Accelyst. Freecharge/Accelyst does not warrant that access to the Freecharge website/ Service shall be uninterrupted, timely, secure, or error free nor does it make any warranty as to the results that may be obtained from the Freecharge website or use, accuracy or reliability of the Service. Freecharge/Accelyst will not be liable for any virus that may enter the Customer's system as a result of the Customer using the Service. Freecharge/Accelyst does not guarantee to the Customer or any other third party that the Service would be virus free.
        </p>
        <p>
          13. Change of Terms: This Service shall be provided at the discretion of Freecharge/Accelyst who reserves the right to add, revise, suspend in whole or in part any of the services provided by it. By using the Service, the Customer shall be deemed to have accepted these Terms. Freecharge/Accelyst shall have the absolute discretion to amend or supplement any of the Terms and/or terms and conditions stipulated by Freecharge/Accelyst and/or Cashcare pertaining to the Service and/or to any services/facilities offered by Freecharge/Accelyst at any time. The Customer shall be responsible for regularly reviewing these Terms and the terms and conditions stipulated by Freecharge/Accelyst or Cashcare including amendments thereto as may be posted on the website. By using any new services as may be introduced by Freecharge/Accelyst, the Customer shall be deemed to have accepted the changed terms and conditions stipulated by Freecharge/Accelyst and/or Cashcare.
        </p>
        <p>
          14. The Customer shall be solely responsible for the payments made through the Service even in the event of the Customer claiming non-possession of the Freecharge Account with him/her. The Customer shall further be solely responsible for any payments made using the Service through the website and cannot claim the invalidity of the same.
        </p>
        <p>
          15. In case the Customer requests for disbursal of the loan as Freecharge Balance, the terms and conditions for prepaid payment instruments namely Freecharge Balance as published on the Freecharge's website shall be applicable. The amount shall not be transferrable or withdrawable or refundable and shall have a validity period of two years.
        </p>

        <p>
          <strong>(B) Cash Care Terms of Service</strong>
        </p>
        <p>
          PLEASE READ THESE TERMS OF SERVICE CAREFULLY. BY ACCESSING THE CASHCARE WEBSITE OR USING THE CASHCARE SERVICE, YOU AGREE TO BE BOUND BY THE (1) CASHCARE TERMS OF SERVICE, AND (2) CASHCARE PRIVACY POLICY. IF YOU DO NOT AGREE WITH THESE TERMS, PLEASE DO NOT ACCESS THE CASHCARE WEBSITE OR USE THE CASHCARE SERVICE. THE HEADINGS CONTAINED IN THIS AGREEMENT ARE FOR REFERENCE PURPOSES ONLY. YOU SHOULD PRINT A COPY OF THIS AGREEMENT FOR YOUR RECORDS.
        </p>
        <p>
          <strong>1. What are you agreeing to?</strong>
        </p>
        <p>
          1.1. The Parties to this Agreement.
        </p>
        <p>
          These Terms of Service describe a contractual relationship ("Agreement") between you ("you" or "your") and CashCare Technology Pvt. Ltd. ("CashCare," "we," "us," "our"), regarding your use of this web site ("Website"), your use of the "Buy with CashCare" service as described below in Section 2.1, and your access to your CashCare account information and service through the Website (together, the "CashCare Services" or "Services").
        </p>
        <p>
          1.2. Changes to this Agreement.
        </p>
        <p>
          CashCare may unilaterally decide to change this Agreement from time to time, provided, however, that such changes will not impose additional obligations on you with respect to actions you took before the change became effective unless you specifically agree to such changes (for example by clicking on the "Pay Now" button below the updated agreement). If this Agreement is changed, CashCare will give notice to users by posting a new version of this Agreement on our website 7 days before that version becomes effective. If CashCare makes any changes to this Agreement that it deems to be material, CashCare will make a reasonable effort to inform you of such changes, but it is your responsibility to review the Agreement posted to our website from time to time to see if it has been changed.
        </p>
        <p>
          1.3. Your Eligibility.
        </p>
        <p>
          To be eligible to use the Services, you must be at least 18 years old and an Indian citizen. You represent and warrant that you are eligible to use the Services.
        </p>
        <p>
          1.4. Enquiry.
        </p>
        <p>
          I authorize CashCare, on behalf of its Financing Partner, to make any enquiries with any other finance company / bank / registered credit bureau regarding my credit history with them.
        </p>
        <p>
          1.5. DND
        </p>
        <p>
         I, would like to know through telephonic calls, or SMS on my mobile number mentioned in the Application Form as well as in this undertaking, or through any other communication mode, various CashCare loan offer schemes or loan promotional schemes or any other promotional schemes and hereby authorize CashCareand it's employee, agent, associate to do so. I confirm that laws in relation to the unsolicited communication referred in "National Do Not Call Registry" (the "NDNC Registry") as laid down by TELECOM REGULATORY AUTHORITY OF INDIA will not be applicable for such communication/calls/ SMSs received from CashCare, its employees, agents and/or associates.
       </p>
       <p>
        <strong>2. How CashCare will serve you?</strong>
      </p>
      <p>
        2.1. The "Buy with CashCare" Service.
      </p>
      <p>
        "Buy with CashCare" is a closed-end instalment loan product with standard terms of repayment as offered by its authorised financing partners through CashCare's technology platform. "Buy with CashCare" allows you to buy goods or services offered by retail Merchants with whom CashCare partners ("Merchants"). If you agree to use "Buy with CashCare," the financing partner will pay the Merchant on your behalf in exchange for your promise to repay the same amount plus a finance charge as determined by your creditworthiness. In cases, where the retail partner is running an interest free offer, the finance charges with be borne by the retail partner and will not be paid by you.
      </p>
      <p>
        CashCare Technology ("CashCare") is technology and operating partner to its financing partners. As a service provider, CashCare is fully responsible communicating, transacting and processing on behalf of its financing partners. At no point, CashCare is representing as a lending company. Please refer to your Loan Agreement for details on your contract with the Financing Partner.
      </p>
      <p>
        2.2 List of authorised financing partners.
      </p>
      <p>
        CashCare has tied up with financial partners to ensure you get the best financing options. Please refer below for our complete list of financing partners
      </p>
      <ol>
        <li> Foresight Holdings Private Limited </li>
        <li> DMI Finance Pvt Ltd </li>
        <li> Aditya Birla Finance Ltd </li>
        <li> Kotak Mahindra Prime Ltd</li>
      </ol>
      <p>
        2.3. Cancelling Transactions.
      </p>
      <p>
        Loan approval process is as per the discretion of the Financing Partner. CashCare may choose not to provide service to you or to specific Merchants at any time for any reason, including but not limited to, your creditworthiness, your history of transactions on our site, the Merchant's account history or any other reason. CashCare may cancel transactions at any time before a Merchant delivers any goods or services if you violate any term of this Agreement.
      </p>
      <p>
        2.4. Collecting Information About You.
      </p>
      <p>
        By using the Website or CashCare Services, you authorize CashCare, directly or through third parties, to make any inquiries we consider necessary to validate your identity and to collect information about you in accordance with our Privacy Policy. This may include asking you for further information that will allow us to reasonably identify you, requiring you to take steps to confirm ownership of your email address or financial instruments, or verifying your information against third party databases or through other sources. We may also ask you for identifying documents to help us validate your identity. CashCare reserves the right to close, suspend, or limit access to your account and/or the Services in the event we are unable to obtain or verify this information.
      </p>
      <p>
        2.5. Credit Investigation and Reporting.
      </p>
      <p>
        By using the CashCare Service, you give CashCare, on behalf of the Financial Partner, permission to investigate your credit record and obtain your credit report in connection with the review of your application for credit. A credit report may also be requested in connection with a credit extension, credit limit request, account renewal, account collection action or dispute investigation. You understand that CashCare may report negative information (such as late payments, missed payments, or other defaults) about your account to credit reporting agencies
      </p>
      <p>
        2.6. Collection.
      </p>
      <p>
        You agree to allow CashCare, on behalf of the Financing Partner, to send you payment reminders from time-to-time. Notwithstanding whether you have consented, you agree that payment reminders may take the form of any available communication. You also agree that if you fail to pay an amount owed to the Financing Partner pursuant to this Agreement, CashCare may engage in collection efforts to recover such amounts from you. These collection efforts may involve contacting you directly, submitting your information to a collections agency, or taking legal action. FAILURE TO PAY MAY AFFECT YOUR CREDIT SCORE.
      </p>
      <p>
        2.7. Communication &amp; Notification.
      </p>
      <p>
        You agree that CashCare may provide you communications about your account and the CashCare Service electronically or through phone calls or in writing. Standard mobile, message, or data rates may apply and you are responsible for any such fees. CashCare reserves the right to close your account and get Financing Partner to immediately collect all due amounts if you withdraw your consent to receive electronic or other communications or if you revoke access to any third party site on which the CashCare Service relies (e.g., Facebook, Twitter, etc.) or if any such service should revoke or cancel your account on that site. Any electronic communications will be considered to be received by you within 24 hours of the time we email it to you or otherwise send it to your attention (such as via sms or other online notification). We may assume you have received any communications sent to you by postal mail 3 business days after we send it.
      </p>
      <p>
        If we need to contact you to service your account, you give direct consent to us, as well as servicers, agents, contractors and collectors of your account, to communicate with you in any way, such as calling, texting, or e-mail via:
      </p>
      <p>
        (a) a mobile phone or landline you provide to us, use to contact us, or at which we believe we can reach you (even if it is not yours),
      </p>
      <p>
        (b) any email address you provide to us or one of our Merchants,
      </p>
      <p>
        (c) automated dialer systems and automatic telephone dialing systems,
      </p>
      <p>
        (d) pre-recorded or artificial voice messages and other forms of communications.
      </p>
      <p>
        You also agree that these communications are not unsolicited (do not follow DND system) for purposes of any state or federal law, and you understand that this may result in additional mobile, text message, or data charges.
      </p>
      <p>
        2.8. Working with Third Parties.
      </p>
      <p>
        If you grant express permission to a third party to take specific actions on your behalf, or access particular information about your account, either through your use of the third party's product or service or through your CashCare account, you acknowledge that CashCare may disclose the information about your account that is authorized by you to this third party. You also acknowledge that granting permission to a third party to take specific actions on your behalf does not relieve you of any of your responsibilities under this Agreement. Further, you acknowledge and agree that you will not hold CashCare responsible for, and will indemnify CashCare from, any liability arising from the actions or inactions of this third party in connection with the permissions you grant.
      </p>
      <p>
        <strong>3. Your use of CashCare </strong>
      </p>
      <p>
        3.1. Agreement to Provide Accurate Information.
      </p>
      <p>
        When you provide information to CashCare, you agree to provide only true, accurate, current and complete information about yourself and you agree not to misrepresent your identity or your account information. You further agree to keep your account information up to date and accurate.
      </p>
      <p>
        3.2. Delays in Processing.
      </p>
      <p>
        In some cases when you attempt to use the CashCare Services to make a purchase, the transaction may be held as pending or be otherwise delayed for processing and confirmation by either CashCare or the Merchant and can be cancelled at any time until it is confirmed by CashCare.
      </p>
      <p>
        3.3. User Responsible for Fees.
      </p>
      <p>
        If you use the CashCare Services, you are responsible for any fees or other amounts that your phone service provider charges, such as fees for SMS, data services, and any other fees that your phone service provider may charge.
      </p>
      <p>
        3.4. Repayment Methods.
      </p>
      <p>
        You may use one of the acceptable methods of payment to make automatic monthly account payment.
      </p>
      <p>
        Currently acceptable methods of payment are:
      </p>
      <p>
        (a) Debit Card - Payments made by debit card will be subject to the terms and conditions established by the debit card issuer. If charges cannot be processed through your debit card, or if your bank draft or electronic funds transfer is returned for insufficient funds, you will be responsible for any fees incurred and CashCare will have no liability with respect thereto.
      </p>
      <p>
        (b) ECS / NACH Transfer - You may pay your account balance by authorizing an Electronic Clearing House (ACH) transaction from your bank account.
      </p>
      <p>
        (c) Post Dated Cheques - You may pay your account balance by writing a cheques payable to the  Financing Partner(d) NEFT /TPT Transfer
      </p>
      <p>
        3.5. Access to Your Account.
      </p>
      <p>
        You are responsible for maintaining the secrecy of the login credentials to your CashCare account. You agree to establish reasonable security procedures and controls to limit access to your password or other identifying information to authorized individuals, which includes choosing passwords and other credentials in a manner that will protect the security of your information. Your credentials include your username and password to your CashCare account and to any 3rd Party account you have used to login to your CashCare account. You are also responsible for maintaining the accuracy of the information in your CashCare account.
      </p>
      <p>
        3.6. Website Content.
      </p>
      <p>
        The information on the CashCare's website is for information purposes only. It is believed to be reliable, but CashCare does not make any promises as to its completeness, timeliness or accuracy. The information and materials contained in the Website, and in this Agreement, are subject to change without notice.
      </p>
      <p>
        Access to the Services may from time to time be unavailable, delayed, limited or slowed due to, among other things:
      </p>
      <p>
        (a) servers, networks, hardware failure (including your own computer), telecommunication lines and connections, and other electronic and mechanical equipment;
      </p>
      <p>
        (b) software failure, including among other things, bugs, errors, viruses, configuration problems, incompatibility of systems, utilities or applications, the operation of firewalls or screening programs, unreadable codes, or irregularities within particular documents or other content;
      </p>
      <p>
        (c) overload of system capacities;
      </p>
      <p>
        (d) damage caused by severe weather, earthquakes, wars, insurrection, riots, civil commotion, act of God, accident, fire, water damage, explosion, mechanical breakdown or natural disasters;
      </p>
      <p>
        (e) interruption (whether partial or total) of power supplies or other utility of service; strike or other stoppage (whether partial or total) of labor;
      </p>
      <p>
        (f) governmental or regulatory restrictions, exchange rulings, court or tribunal orders or other human intervention; or
      </p>
      <p>
        (g) any other cause (whether similar or dissimilar to any of the foregoing) whatsoever beyond the control of CashCare.
      </p>
      <p>
        3.7. Links to Other Websites.
      </p>
      <p>
        Links to non- CashCare websites are provided solely as pointers to information on topics that may be useful to users of the Services, and CashCare has no control over the content on such non-CashCare websites. CashCare makes no warranties concerning the content of such websites, including the accuracy, completeness, reliability of said websites, nor does CashCare warrant that such website or content is free from any claims of copyright, trademark or other infringement of the rights of third parties or that such site or content is devoid of viruses or other contamination. If you choose to follow a link to a website not controlled by CashCare, you must do so at your own risk. CashCare does not guarantee the authenticity of documents on the Internet. Links to non-CashCare websites do not imply any endorsement of or responsibility for the opinions, ideas, products, information or services offered at such sites, or any representation regarding the content at such websites.
      </p>
      <p>
        3.8. Closing Your Account.
      </p>
      <p>
        You may close your account at any time by contacting us at contact@cashcare.in. Upon account closure, we will cancel any pending transactions unless otherwise legally prohibited. If you owe any payment to the Financing Partner, CashCare will not close your account until that payment has been made, but we may limit your ability to make additional transactions using your account. You may not close your account to evade an investigation. You will remain liable for all obligations related to your account even after the account is closed. CashCare will retain your information in accordance with our Privacy Policy.
      </p>
      <p>
        3.9. Dormant Accounts.
      </p>
      <p>
        CashCare may close your account if you do not log in to the Website or use the CashCare Services for two or more years. CashCare will retain your information in accordance with our Privacy Policy.
      </p>
      <p>
        <strong>4. How you may NOT use CashCare?</strong>
      </p>
      <p>
        By using CashCare's Website or Services, you agree that:
      </p>
      <p>
        1. You will not engage in any activities related to the Services that are contrary to any applicable law or regulation or the terms of any agreements you may have with CashCare;
      </p>
      <p> 2. You will not provide false, inaccurate or misleading information; </p>
      <p> 3. You will not provide information belonging to any person other than yourself; </p>
      <p> 4. You will not use an account that belongs to another person for yourself or on behalf of another person; </p>
      <p> 5. You will not use any device, software, routine, file or other tool or technology, including but not limited to any viruses, trojan horses, worms, time bombs or cancelbots, intended to damage or interfere with the Services or to surreptitiously intercept or expropriate any system, data or personal information from the Services; </p>
      <p> 6. You will not breach this Agreement or any other agreement or policy that you have agreed to with CashCare; </p>
      <p> 7. You will not use the Services to accomplish a cash advance; </p>
      <p> 8. You will not commit unauthorized use of CashCare's Website and systems including but not limited to unauthorized entry into CashCare's systems, misuse of passwords, or misuse of any information posted to a site; and </p>
      <p> 9. You will not take any action that imposes an unreasonable or disproportionately large load on our infrastructure, including but not limited to denial of service attacks, "spam" or any other such unsolicited overload technique. </p>

      <p>
        <strong>5.  How CashCare will resolve disputes?</strong>
      </p>
      <p>
        5.1 Disputes with CashCare
      </p>
      <p>
        If a dispute arises between you and CashCare, our goal is to provide you with a neutral and cost-effective means of resolving the dispute quickly. In the event of a dispute we encourage you first to contact CashCare at contact@cashcare.in to try resolving your problem directly with us.
      </p>
      <p>
        5.2 Consumer Consent
      </p>
      <p>
        Before completing any transaction using the Buy with CashCare Services, you will be asked to consent to the CashCare Terms of Service by checking the terms and conditions box on the application form. This action constitutes your electronic signature and manifests your consent and agreement to the following terms:
      </p>
      <p> (a) CashCare may provide disclosures required by law and any other information about your legal rights and duties and your account to you electronically. </p>
      <p> (b) CashCare can send any communications, billing statements, or required disclosures (together, "Disclosures") to you electronically via our website or to the email address that you have provided to us, either directly or indirectly via the Merchant to whom CashCare will send funds on your behalf. </p>
      <p> (c) CashCare will notify you via e-mail when the Disclosures are available. The Disclosures will be available for at least 30 days and will be provided to you in a format that can either be printed or downloaded for your records. Whenever CashCare sends you an email or text message regarding the Disclosures, that email or text message will contain instructions regarding how to view the Disclosures. </p>
      <p> (d) Your electronic signature on agreements and documents has the same effect as if you signed them in ink. </p>
      <p> (e) This consent applies to any transaction undertaken through the Services in the past, to all future disclosures and communications on your account, to all future transactions in which you use the Services, at any time, to any transaction with us, and to other Disclosures that we provide to you by email. </p>

      <p>
        5.3 Minimum Requirements
      </p>
      <p>
       You understand that, in order to view and/or retain copies of the Disclosures, you may need a computer with an Internet connection (PCs should be running Windows 7 or higher and Internet Explorer 10 or higher, Chrome, or Firefox; Macs should be running OSX and Safari, Chrome, or Firefox); a mobile device (iOS 6.0 or higher devices running Safari or Chrome; Android 4.0 or higher devices running Android Browser or Chrome), a valid email address, a working mobile telephone number that can receive text messages, sufficient storage space to save Disclosures or the capability to print the Disclosures from the device on which you view them.
     </p>
     <p>
      5.4 Withdrawing Consent
    </p>
    <p>
     You are free to withdraw Your Consent at any time. If at any time you wish to withdraw Your Consent, you can send us your request by submitting a request through the 'contact us' link at www.cashcare.in or emailing contact@cashcare.in. If you decide to withdraw Your Consent, the legal effectiveness, validity and/or enforceability of prior electronic Disclosures will not be affected and your account will be closed.
   </p>
   <p>
    5.5 Updating Your Contact Information
  </p>
  <p>
   You can update the mobile phone number or email address CashCare uses to send you Disclosures or alerts by submitting a request through the 'contact us' link at www.cashcare.in,or emailing contact@cashcare.in.
 </p>
 <p>
  5.6 Legal Effect
</p>
<p>
 Your consent to this E-Sign Consent means that Disclosures CashCare provides to you electronically shall have the same meaning and effect as if provided in paper form. An text message or email alerting you that Disclosures are available electronically, provided that the Disclosures are made available online shall have the same meaning and effect as if CashCare had provided those Disclosures to you in paper form, regardless of whether you actually view those Disclosures, unless you have withdrawn your consent pursuant to Section 3, above, prior to receiving such Disclosures,
</p>
<p>
  5.7 Miscellaneous
</p>
<p>
 You agree that CashCare may modify or change the methods of disclosure described herein, and that CashCare may send you Disclosures in paper form at its option. You also agree that CashCare is not responsible for any delay or failure in your receipt of any text message or email notice that is not caused by CashCare's failure to send such a notice to the phone number or email address you have provided for that purpose.
</p>
<p> The product value shown on the product details page may be indicative on account of price fluctuations at the partner website. Please note that the order can be confirmed only once the intial payment has been made and the complete list of documents are provided for processing the application. Loan application and order processing may take upto 48 hours once the documents are provided. Any price changes during this period may have to be borne by the customer. </p>
<p> In some cases we are unable to check if the product can be delivered to your pincode by the ecommerce partner. In case the product is out of stock or cannot be delivered to your pincode, we will refund the intial payment made by you within 24 hours. Cashcare reserves the right to not refund the processing fees in case cancellation is made by the customer. </p>
<p> This loan offer is not valid with any other offers that may be available on your product. </p>
<p> <strong> (C) Cash Care- Privacy Policy </strong> </p>
<p> Effective Date: Effective from 05th October 2015 </p>
<p> PLEASE READ THIS PRIVACY POLICY CAREFULLY. THIS POLICY DESCRIBES THE WAYS CashCare Technology Pvt. Ltd. OR OUR VENDORS ("CashCare", "we", "us", "our") COLLECT, PROTECT, USE AND STORE YOUR PERSONAL INFORMATION. YOU ACCEPT THIS PRIVACY POLICY BY USING OUR PRODUCTS AND SERVICES ON OUR WEBSITE OR THROUGH ANY OTHER MEANS (COLLECTIVELY THE "SERVICES"). </p>
<p> We may amend this Privacy Policy at any time by posting a revised version on our website. We will attempt to give a reasonable notice period upon making any changes; however, unless otherwise stated, the revised version will be effective at the time we post it. If you have any questions or comments regarding this Privacy Policy, please contact us at contact@cashcare.in </p>
<p> <strong> 1. How CashCare collects your information?</strong> </p>
<p> 1.1. Information You Share with CashCare </p>
<p> If you open an account or use the CashCare Service, we may collect the following types of information: Personal information ¬ your name, PAN, address, phone, email, third-party application IDs, and other similar information. Financial information ¬ bank account numbers and/or credit card numbers that you link to your CashCare account or give us when you use the Services. Before permitting you to use the Services, we may require you to provide additional information, including but not limited to your date of birth, PAN, personal information stored by third parties such as social media sites (such as Facebook and Twitter) and other personal information. We may use this information to verify your identity or other information you provide. </p>
<p> 1.2. Information CashCare Learns From Your Use </p>
<p> When you visit the CashCare website or use the Services, we may collect information sent to us by your computer, mobile phone or any other device. This information may include your IP address, device information including but not limited to identifier, name and type, operating system, location, mobile network information and standard web log information, such as your browser type, traffic to and from our site, the pages you accessed on our website, and any other available information. We may also collect information about your use and interaction with our website, application or the Services. For example, we may evaluate your computer, mobile phone or other access device to identify any malicious software or activity that may affect the availability of the Services. When you use the Services, we may also store information based on your usage history. This includes, but is not limited to, details of your purchases, content you viewed, event information, click stream information, and cookies that may uniquely identify your browser or your account. We may also collect information about you from any contact you have with any of our services or employees, such as, with our customer support team, in surveys, or though interactions with our affiliates. </p>
<p> 1.3. Cookies and Anonymous Identifiers </p>
<p> We use various technologies to collect and store information when you use the Services, and this may include sending one or more cookies or anonymous identifiers to your device. We also use cookies and anonymous identifiers when you interact with the services we offer to our partners, such as advertising services or CashCare features that may appear on other sites and in any other manner that we deem necessary for our business purposes. </p>
<p> 1.4. Information Obtained From Third Parties </p>
<p> We may also obtain information about you from third parties such as credit bureaus (CIBIL) and identity verification services. You may choose to provide us with access to certain personal information stored by third parties such as social media sites (such as Facebook and Twitter). The information we have access to varies by site and is controlled by your privacy settings on that site and your authorization. By associating an account managed by a third party with your CashCare account and authorizing CashCare to have access to this information, you agree that CashCare may collect, store and use this information in accordance with this Privacy Policy. </p>
<p> <strong> 2. How CashCare protects your information? </strong> </p>
<p> 2.1. Safety Policies </p>
<p> The security of your Personal Information is important to us. When you enter sensitive information, such as personal email address or mailing address, on our registration or order forms, we encrypt that information using secure socket layer technology (SSL). </p>
<p> We follow generally accepted industry standards to protect the Personal Information submitted to us, both during transmission and once we receive it. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, while we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security. </p>
<p> <strong> 3. How CashCare uses your information?</strong> </p>
<p> 3.1. To Improve Our Service </p>
<p> CashCare uses information to perform and improve our services, contact you, conduct research, and provide anonymous reporting for clients. For example, we may use information to provide customer service and support, process transactions, resolve disputes, collect payments, prevent illegal activities, customize the Services, reduce risk to all parties involved in our transactions, and verify the accuracy of information. </p>
<p> 3.2. To Serve Relevant Marketing to You </p>
<p> We may use information to deliver targeted marketing, service update notices, and promotional offers based on your communication preferences. We may combine your information with information we collect from other companies and use it to improve and personalize the Services, content, and advertising. </p>
<p> <strong> 4. When CashCare shares your information?</strong> </p>
<p> 4.1. For Our Everyday Business Purposes </p>
<p> We share your personal information with employees, affiliates, vendors, partners and third parties as required to offer the CashCare Service. This includes, but is not limited to, processing transactions, maintaining your account, responding to court orders and legal investigations, litigation purposes, complying with audits or other investigations, and reporting to credit bureaus. You may opt out of certain sharing as discussed in Section 5 of this privacy policy. </p>
<p> 4.2 When Required By Law </p>
<p> We will share your information with any party when required by law or by a government request to do so or to combat fraud or criminal activity. </p>
<p> 4.3 With Credit Rating Agencies &amp; RBI </p>
<p> As part of the contract with the Credit Rating Agencies, CashCare on behalf of its financing partners is mandated to share loan performance data. </p>
<p> 4.4 Does Not Sell Your Information To Third Parties </p>
<p> CashCare takes your privacy seriously and does not sell your data to third parties. CashCare may share personal information with partners under contract who help with certain parts of our business operations including application evaluation, customer services, payment, fraud prevention, secure data storage, collections and other similar services. </p>
<p> 4.5 In Case of Default </p>
<p> CashCare make choose to publicly announce about your default amongst your friend list and group, etc., with your details on various social media platforms and other avenues. </p>
<p> <strong> 5. What are your options to Opt-Out?</strong> </p>
<p> 5.1 Notifications </p>
<p> If you no longer wish to receive notifications about our Services, you may change your notification preferences by accessing your account on the Website or by contacting us at contact@cashcare.in. CashCare reserves the right to close your account should you opt of the crucial notices that are required to perform the CashCare Service. You may not opt-out of receiving notifications about due or past due amounts that you owe CashCare or any other collections efforts. </p>
<p> 5.2 SMS Messaging </p>
<p> You can opt out of receiving SMS messages by accessing your account on the Website or by contacting us at contact@cashcare.in. Again, CashCare reserves the right to close your account should you opt of the crucial notices that are required to perform the CashCare Service. You may not opt out of receiving notifications about due or past due amounts that you owe CashCare or any other collections efforts. </p>
<p> 5.3 Access Your Information </p>
<p> You can review and edit your personal information at any time by logging in to your account on the Website or by contacting us at contact@cashcare.in. You can also request to close your account by contacting us at contact@cashcare.in. If you close your CashCare account, we will mark your account in our database as "Closed," but will keep your account information in our database to comply with our legal obligations. This is necessary in order to deter fraud, by ensuring that persons who try to commit fraud will not be able to avoid detection simply by closing their account and opening a new account. If you close your account, your personally identifiable information will not be used by us for any further purposes, nor sold or shared with third parties, except as necessary to prevent fraud and assist law enforcement, as required by law or in accordance with this Privacy Policy. </p>
<p> <strong> 6. Other sites</strong> </p>
<p>There might be affiliates or other sites linked to our web site. Personal information that you provide to those sites are not our property. The affiliated sites may have different privacy practices and we encourage you to read their privacy policies. These sites may place their cookies or other files on your computer, collect data or solicit personal information.</p>
<p> <strong> 7. Contact CashCare</strong> </p>
<p>If you have questions or concerns regarding this Privacy Policy, you should contact us at contact@cashcare.in.</p>
<p><strong> (D) CashCare CIBIL Check </strong></p>
<p> 1. As a part of the loan application, the Financing Partner may require to access your CIBIL/Credit Score. </p>
<p> 2. I authorize CashCare, on behalf of its Financing Partner, to make any enquiries with any other finance company / bank / registered credit bureau regarding my credit history with them. </p>
</div>
</li>
<li class="upiListStyle">
  <h3 class="cbp-nttrigger">Terms of Use of Freecharge UPI</h3>
  <div class="cbp-ntcontent">    
  <p>Last Updated on January 17,2018 </p>   
    <p>
      THIS DOCUMENT IS PUBLISHED IN COMPLIANCE OF AND SHALL BE GOVERNED BY INDIAN LAW, INCLUDING BUT NOT LIMITED TO (I) THE INDIAN CONTRACT ACT, 1872; (II) THE INFORMATION TECHNOLOGY ACT, 2000, THE RULES, REGULATIONS, GUIDELINES AND CLARIFICATIONS FRAMED THEREUNDER INCLUDING THE INFORMATION TECHNOLOGY (REASONABLE SECURITY PRACTICES AND PROCEDURES AND SENSITIVE PERSONAL INFORMATION) RULES, 2011, AND THE PROVISIONS OF RULE 3 (1) OF THE INFORMATION TECHNOLOGY (INTERMEDIARIES GUIDELINES) RULES, 2011; (III) THE PAYMENT AND SETTLEMENT SYSTEMS ACT, 2007 AND APPLICABLE RULES, REGULATIONS AND GUIDELINES MADE THEREUNDER; AND (IV) RESERVE BANK OF INDIA ACT, 1934 AND THE APPLICABLE RULES, REGULATIONS AND GUIDELINES MADE THEREUNDER.
    </p>
    <p>
      THIS DOCUMENT IS AN ELECTRONIC RECORD IN THE FORM OF AN ELECTRONIC CONTRACT FORMED UNDER INFORMATION TECHNOLOGY ACT, 2000 AND RULES MADE THEREUNDER AND THE AMENDED PROVISIONS PERTAINING TO ELECTRONIC DOCUMENTS / RECORDS IN VARIOUS STATUTES AS AMENDED BY THE INFORMATION TECHNOLOGY ACT, 2000. THIS AGREEMENT DOES NOT REQUIRE ANY PHYSICAL, ELECTRONIC OR DIGITAL SIGNATURE. 
    </p>
    <p>
      THIS AGREEMENT IS A LEGALLY BINDING DOCUMENT BETWEEN YOU AND FREECHARGE (BOTH TERMS DEFINED BELOW). THE TERMS OF THIS DOCUMENT WILL BE EFFECTIVE UPON YOUR ACCEPTANCE OF THE SAME (IN ELECTRONIC FORM OR BY MEANS OF AN ELECTRONIC RECORD OR OTHER MEANS) AND WILL GOVERN THE RELATIONSHIP BETWEEN YOU AND FREECHARGE FOR THE USE OF FREECHARGE UPI AND SERVICES (DEFINED BELOW). IF ANY TERMS OF THIS DOCUMENT CONFLICT WITH ANY OTHER DOCUMENT/ELECTRONIC RECORD IN THIS BEHALF, THE TERMS AND CONDITIONS OF THIS AGREEMENT SHALL PREVAIL, UNTIL FURTHER CHANGE / MODIFICATIONS ARE NOTIFIED BY FREECHARGE.
    </p>
    <p>
      These terms and conditions regulate the payments under the Unified Payment Interface ("UPI"), a payment service platform ("Platform") developed by National Payments Corporation of India ("NPCI"), an umbrella organisation incorporated in 2008 and acting as the settlement/clearing house/regulatory agency for UPI services. The application software ("Freecharge App") which connects the Platform is being offered by Freecharge Payment Technologies Private Limited a company incorporated under the Companies Act, 2013 with its registered office at 68, Okhla Industrial Estate, Phase-III, New Delhi – 110020 India (hereinafter referred to as "Freecharge ") in partnership/association with Axis Bank Limited ("Axis Bank"), a bank licensed under the Banking Regulation Act, 1949. 
    </p>
    <p>The UPI services ("Services") are being offered under the brand "Freecharge". </p>
    <p>Your use of the Freecharge UPI and related tools are governed by the following terms and conditions ("Terms of Use") which are incorporated herein by way of reference. </p>
    <p>1. For the purpose of these Terms of Use, wherever the context so requires-</p>

      <p>a. 'You/Your' or "User" shall mean any natural or legal person who has registered for Freecharge Services, linked his/her bank account for UPI payments and who has accepted these Terms of Use.</p>
      <p>b. "We", "Us", "Our" shall mean Freecharge Payment Technologies Private Limited. </p>
      <p>c. "Merchant" shall include any establishment and/or entity which  accepts the UPI payments through Freecharge App ("Freecharge UPI") as a payment method for online or offline purchase of goods and/or services. </p>
      <p>d. "Buyer" shall refer to the person who purchases any of the goods or services provided online or offline by the Merchants. </p>
    
    <p>2. If You transact using the Freecharge App or any Merchant Website/ Merchant Platform, these conditions will be applicable. By mere use of the Freecharge App, You shall be contracting with Freecharge and these terms and conditions including the policies constitute Your binding obligations, with Freecharge.</p>     
    <p>3. We reserve the right, at Our sole discretion, to change, modify, add or remove portions of these Terms of Use, at any time without any prior written notice to You. It is Your responsibility to review these Terms of Use periodically for updates / changes. Your continued use of Freecharge App following the posting of changes will mean that You accept and agree to the revisions including additional Terms or removal of portions of these Terms, modifications etc.  As long as You comply with these Terms of Use, We grant You a personal, non-exclusive, non-transferable, limited privilege to enter and avail the Services. </p>
    <p>4. USING FREECHARGE APP INDICATES YOUR AGREEMENT TO ALL THE TERMS AND CONDITIONS UNDER THESE TERMS OF USE, SO PLEASE READ THE TERMS OF USE CAREFULLY BEFORE PROCEEDING. By impliedly or expressly accepting these Terms of Use, You also accept and agree to be bound by Freecharge Policies (including but not limited to Privacy Policy available on the Freecharge website www.freecharge.com as amended from time to time.</p>
    <p>5. About UPI</p>
     
      <p>a. The Unified Payments Interface (UPI) is a payment platform built by NPCI (National Payments Corporation of India) that allows instant online payments between the bank accounts of any two parties. UPI offers an architecture and a set of standard API specifications to facilitate these online payments. It aims to simplify and provide a single interface across all NPCI systems besides creating interoperability and superior customer experience. </p>
    
    <p>6. Your Account and Registration Obligations</p>
    <ul>
      <li>a. To enable UPI payments, the user first needs to have a Freecharge account. User can register and create a Freecharge account by:
        <ul>
          <li>i. Downloading the Freecharge App and registering himself/herself. Or,</li>
          <li>ii. Registering with Freecharge from a third party partner platform.</li>
          <li>iii. User has to necessarily install the Freecharge App on his/her smartphone to be able to register and transact using Freecharge UPI.</li>
        </ul>
      </li>
      <li>b. Thereafter, You will be able to register for UPI payments by:
        <ul>
          <li>i. Selecting your bank: </li>           
          <li>ii. You first need to select the bank from amongst the list of UPI-member banks as shown in the App. The list comprises of all the banks connected to the UPI Platform of NPCI.</li>
          <li>iii. If the name of Your bank does not appear in the list, You can still add Your bank by providing the bank account number and the IFSC code. However, You will not be able to make UPI payments from Your bank account. </li>          
        </ul>
      </li>
      <li>c. If You have selected a bank from the UPI-member list, Please note that Your mobile number, should be registered with Your bank. If Your mobile number is not registered, then Your OTP verification will fail. You will not be able to avail the UPI Service unless You first register your mobile number with the Bank.</li>
      <li>d. Your bank will share the masked bank account numbers against the registered mobile number. You need to select the bank account which You wish to link with Freecharge App. </li>
      <li>e. The selected bank account can be linked with Freecharge App in the following manner:
        <ul>
          <li>i. If You already have the 4-6 digit UPI PIN (UPI Personal Identification Number) for Your bank account, then You need to simply select the "Link" option and continue.</li>
          <li>ii. If you do not remember the UPI PIN, you can click on "forgot UPI PIN" and set a new one by providing the Debit card number linked with Your bank account and the Expiry date of the Card. Your mobile number will be OTP verified by Your bank. If the verification is successful, you will be taken to the NPCI page, to set a new UPI PIN. After setting the UPI PIN, you will be taken back to the Freecharge App screen, where you have to click on "Link" and then continue.</li>
          <li>iii. You can Reset your UPI PIN by clicking on "Reset UPI PIN". You can then set a new UPI PIN by providing the old and new UPI PINs.</li>         
          <li>iv. If You have never set the UPI PIN before, then You will see an option to Set UPI PIN in settings . Please click on the link and set the UPI PIN by providing the Debit card number linked with Your bank account and the Expiry date of the Card. Your mobile number will be OTP verified by Your bank. If the verification is successful, You will be taken to the NPCI page, to set a new UPI PIN. After setting the UPI PIN, you will be taken back to the Freecharge App screen, where you have to click on "Link" and then continue.</li>
          <li>v. You need to authorize every UPI payment transaction by providing the UPI PIN.</li>
        </ul>
      </li>
      <li>f. "KYC" stands for Know Your Customer and refers to the various norms, rules, laws and statutes issued by RBI from time to time under which Freecharge is required to procure personal identification details from You before any Services can be delivered. Know Your Customer (KYC) documents may be asked  by the Merchant (on behalf of Freecharge) from You at the time of activation and/ or on a later date, for availing and / or continuation of Services. Freecharge/Merchant shall not be responsible for wrong details being entered by the User.</li>
      <li>g. In the event the beneficiary/KYC details provided are found to be incorrect/ insufficient, Freecharge retains the right to block Your Freecharge account. Freecharge retains the right to share the details of the transaction undertaken using the Freecharge account and the end beneficiary/ KYC details with RBI, as per statutory guidelines issued from time to time.</li>
      <li>h. You are solely responsible for linking Your correct bank account.</li>
      <li>i. Since Your mobile number is treated as the primary identifier, Your mobile number needs to be updated with the bank linked to Freecharge App in case of any changes.</li>
      <li>j. If You change the mobile number registered with Your Freecharge account, You will have to re-register Your new mobile number with Freecharge. To re-enable the Service, You need to register Your new mobile number with Your bank as well.</li>
      <li>k. You agree that Your UPI ID will act as Your identity for all payment transactions and all requests to Your Freecharge account will be raised to Your UPI ID unless another UPI ID is explicitly specified.</li>
      <li>l. You agree that if You provide any information that is untrue, inaccurate, not current or incomplete or We have reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, or not in accordance with the this Terms of Use, We shall have the right to indefinitely suspend or terminate or block access to Your Account.</li>
      <li>m. We shall ensure that all Your confidential data pertaining to Your money transactions, bank account details, and all other sensitive personal information are protected and kept confidential by employing best available protection standards, which are more fully set out in our Privacy Policy. We/Axis Bank reserves the right to use Your transaction data for cross-selling/promotions/offers/value added services/increasing transactions/better user experience/such other purpose as may be required by Us/AxisBank</li>
      <li>n. You are solely responsible to keep Your OTP, UPI PIN and bank account related details confidential. Sharing such information with others may lead to unauthorized usage, for which Freecharge shall not be responsible.
      </li>
    </ul>
    <p>7. Transactions through Freecharge UPI</p>   
    <ul>
      <li>a. Freecharge UPI can be used for Person to Person money transfers or to purchase products and services online or offline on any Merchant platform.</li>
      <li>b. The UPI PIN needs to be entered on Your mobile to authorize every Freecharge UPI payment. You need to ensure that You are connected to the internet at the time of making any transaction.</li>
      <li>c. Freecharge UPI is one of the payment options available to the Users on various Merchant platforms and We assume no responsibility for the products or services purchased using Freecharge UPI and any liability thereof is expressly disclaimed.</li>
      <li>d. Freecharge UPI is one of the payment options available to the Users on various Merchant platforms and We assume no responsibility for the products or services purchased using Freecharge UPI and any liability thereof is expressly disclaimed.</li>
      <li>e. You shall  only use Freecharge UPI Services to process a Transaction to make payment for legitimate and bona fide purposes including purchase of goods and services. You shall  not use Freecharge UPI Service to process Payment Transactions in connection with the sale or exchange of any illegal goods or services or any other underlying illegal transaction.</li>
      <li>f. We may establish general practices and limits concerning use of Freecharge UPI. We reserve the right to change, suspend or discontinue any aspect of Freecharge UPI at any time, including hours of operation or availability of Freecharge UPI Services or any Freecharge UPI Service feature, without notice and without liability. We also reserve the right to impose limits on certain features or restrict access to parts or all of the service without notice and without liability. We may decline to process any Payment Transaction without prior notice to Sender or Recipient. We also reserve the right to automatically block any communication received by a User from a non- Freecharge UPI account, including any payment requests, that We deem to be spam or a fraudulent communication.</li> 
      <li>g. We do not warrant that the functions contained in Freecharge UPI will be uninterrupted or error free, and We shall not be responsible for any Service interruptions (including, but not limited to, power outages, system failures or other interruptions that may affect the receipt, processing, acceptance, completion or settlement of payment transactions or any other reasons beyond reasonable control of Freecharge).</li>
      <li>h. We may reject a Transaction and/or settlement of payments  for various reasons, including but not limited to risk management, suspicion of fraudulent, illegal or doubtful Transactions, selling of prohibited items, use of compromised or blacklisted cards or UPI accounts, chargebacks/complaints or for other reasons as prescribed under Applicable Laws or rules of Participating Banks. In the event that a Transaction is rejected or is unable to be completed, We will either transfer the funds back to the Your Funding Account or will handle the funds in accordance with Applicable Laws or Participating Bank Rules.</li>
      <li>i. UPI limits issued by NPCI</li>
    </ul>
    <p>8. Requirements and Limits for Freecharge UPI</p>
    <table  class="freeAxis" border="0" cellpadding="7" cellspacing="0">
      <tr>
        <td>Particular</td>
        <td>Transaction Limit</td>
      </tr>
      <tr>
        <td>Monthly</td>
        <td>None</td>
      </tr>
      <tr>
        <td>Per Transaction</td>
        <td>Rs. 1 lakh</td>
      </tr>
    </table>
    <p>9. Charges</p>
    <ul>
      <li>a. Membership is free for Users. Freecharge does not charge its Users any fee for creating an Account or use of Services. Freecharge reserves the right to change its fee policy from time to time. In particular, Freecharge may at its sole discretion introduce new services and modify some or all of the existing services offered on the website. In such an event, We reserve the right to introduce fees for the new services offered or amend/introduce fees for existing services, as the case may be. Changes to the fee policy shall be posted on the website/app and such changes shall automatically become effective immediately after they are posted. Unless otherwise stated, all fees shall be quoted in Indian Rupees. </li>
      <li>b. Your bank may charge You a nominal transaction fee for UPI transfers- please check with Your bank for any such charges.</li>
      <li>c. The reporting and payment of any applicable taxes arising from the use of Freecharge UPI is Your responsibility. You hereby agree to comply with any and all applicable tax laws in connection with Your use of Freecharge UPI, including without limitation, the reporting and payment of any taxes arising in connection with Payments made through Freecharge UPI, or funds/income received through Freecharge UPI.</li>
    </ul>
    <p>10. You herein agrees and accepts that, in case of any discrepancy in the information provided by You for availing this Service and the onus thereof shall always be upon the You only and thus You agrees to furnish accurate information at all times to the Freecharge .</p>
    <p>11. If You suspects that there is an error in the information supplied by Freecharge, You shall inform Freecharge immediately. Freecharge will endeavor to correct the error promptly wherever possible on a best effort basis. </p>
    <p>12. Freecharge shall not be held liable & responsible for any loss, cost or damage suffered by You due to disclosure of Your Personal or any other information to a third party including Govt./Regulatory authorities by Freecharge, for reasons inclusive but not limited to participation in any telecommunication or electronic clearing network, in compliance with a legal or regulatory directive, for statistical analysis or for credit rating or for any legal or regulatory compliance. </p>
    <p>13. You are solely responsible for protecting Your Mobile phone/device, virtual address and UPI PIN set for the availing of the Services..</p>
    <p>14. You shall be liable for any kind of unauthorized or unlawful use, misuse or leakage  of any of the virtual address or MPIN/Passwords/Passcodes issued by Freecharge in respect of the Services or any fraudulent or erroneous instruction given by You and any financial charges thus incurred shall be payable by You  only. </p>
    <p>15. You shall be liable for all loss, cost or damage, if You have  breached the terms and conditions contained herein or other additional terms & conditions mentioned above or contributed or caused the loss by negligent actions or a failure on Your part to advise Freecharge within a reasonable time about any unauthorized access in Your Freecharge account used for this Services. </p>
    <p>16. We reserve the right, in our sole and absolute discretion, to suspend or terminate Your use of one or more Freecharge UPI Services, without notice and without liability to You or any third party, for any reason, including without limitation inactivity or violation of these Freecharge UPI Terms of Use or other policies We e may establish from time to time.</p>
    <p>17. <strong>Disclaimer:</strong></p>
    <p>
      THE SERVICES, INCLUDING ALL CONTENT, SOFTWARE, FUNCTIONS, MATERIALS, AND INFORMATION MADE AVAILABLE ON, PROVIDED IN CONNECTION WITH OR ACCESSIBLE THROUGH THE SERVICES, ARE PROVIDED "AS IS." TO THE FULLEST EXTENT PERMISSIBLE BY LAW, FREECHARGE, AND THEIR AGENTS, CO-BRANDERS OR OTHER PARTNERS, INCLUDING BUT NOT LIMITED TO, DEVICE MANUFACTURERS (COLLECTIVELY, " FREECHARGE PARTIES"), MAKE NO REPRESENTATION OR WARRANTY OF ANY KIND WHATSOEVER FOR THE SERVICES OR THE CONTENT, MATERIALS, INFORMATION AND FUNCTIONS MADE ACCESSIBLE BY THE SOFTWARE USED ON OR ACCESSED THROUGH THE SERVICES, OR FOR ANY BREACH OF SECURITY ASSOCIATED WITH THE TRANSMISSION OF SENSITIVE INFORMATION THROUGH THE SERVICES. EACH FREECHARGE PARTY DISCLAIMS WITHOUT LIMITATION, ANY WARRANTY OF ANY KIND WITH RESPECT TO THE SERVICES, NONINFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. FREECHARGE DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THE SERVICES WILL BE UNINTERRUPTED OR ERROR FREE. FREECHARGE  SHALL NOT BE RESPONSIBLE FOR ANY SERVICE INTERRUPTIONS, INCLUDING BUT NOT LIMITED TO SYSTEM FAILURES OR OTHER INTERRUPTIONS THAT MAY AFFECT THE RECEIPT, PROCESSING, ACCEPTANCE, COMPLETION OR SETTLEMENT OF PAYMENT TRANSACTIONS, P2P PAYMENTS OR THE SERVICES.
    </p>
    <p>THE FREECHARGE PARTIES ARE NOT RESPONSIBLE FOR THE ACCURACY OF ANY PAYMENT INSTRUMENT, INCLUDING WITHOUT LIMITATION WHETHER SUCH INFORMATION IS CURRENT AND UP-TO-DATE. WITHOUT LIMITING THE GENERALITY OF THE PRECEDING SENTENCE, YOU EXPRESSLY ACKNOWLEDGE AND AGREE THAT SUCH INFORMATION IS REPORTED BY THE BANK AS OF A PARTICULAR TIME ESTABLISHED BY THE BANK  AND MAY NOT ACCURATELY REFLECT YOUR CURRENT TRANSACTIONS, AVAILABLE BALANCE, OR OTHER ACCOUNT OR PROGRAM DETAILS AT THE TIME THEY ARE DISPLAYED TO YOU THROUGH THE SERVICES OR AT THE TIME YOU MAKE A PURCHASE OR REDEMPTION. YOU MAY INCUR FEES, SUCH AS OVERDRAFT FEES OR OTHER CHARGES AS A RESULT OF SUCH TRANSACTIONS, PER YOUR AGREEMENT WITH YOUR BANK , OR YOUR ATTEMPT TO MAKE A PURCHASE OR REDEMPTION MAY NOT BE SUCCESSFUL.
    </p>
    <p>18.  You agree to release Freechage and its partner Bank (Axis Bank), agents, contractors, officers and employees, from all claims, demands and damages (actual and consequential) arising out of or in any way connected to any transaction made through Freechage App. You agree that you will not involve Freecharge in any litigation or other dispute arising out of or related to any transaction, agreement, or arrangement with any Recipient, Sender, Merchant, advertiser or other third party in connection with the Freecharge UPI Service. If You attempt to do so, (i) You shall pay all costs and attorneys' fees of Freecharge, and Group Companies and shall provide indemnification as set forth below, and (ii) the jurisdiction for any such litigation or dispute shall be limited as set forth below. </p>
    <p>
      You agree to indemnify, defend and hold harmless Freecharge, its partner Bank (Axis Bank), Group Companies and its Directors, officers, owners, agents or other partners, employees, contractors and other applicable third parties (collectively "Indemnified Parties") from and against any and all claims, demands, causes of action, debt or liability, including reasonable attorneys fees, including without limitation attorneys fees and costs incurred by the Indemnified Parties arising out of, related to, or which may arise from:
    </p>
    <p>(a) Your use of the Services;</p>

    <p>(b) any breach or non-compliance by You of any term of this Freecharge UPI Terms of Use or any related  policies;</p>

    <p>(c) any dispute or litigation caused by Your actions or omissions; or</p>

    <p>(d) Your negligence or violation or alleged violation of any applicable law or rights of a third party.</p>
    <p>19 <strong>Jurisdiction :</strong> You agree that any legal action or proceedings arising out of the aforementioned documents shall  be brought exclusively in the competent courts/tribunals having jurisdiction in New Delhi, India and irrevocably submit themselves to the jurisdiction of such courts/tribunals. </p>
    <p>20. These Freecharge UPI Terms and any rights and licenses granted hereunder, shall  not be transferred or assigned by You.  However, We may assign, in whole or in part, the benefits or obligations of this Agreement. We will provide an intimation of such assignment to You, which will be binding on the parties to these Terms and Conditions.</p>
    <p>21. Unless otherwise expressly stated in this Agreement, the failure to exercise or delay in exercising a right or remedy under Freecharge UPI Terms of Use  will not constitute a waiver of the right or remedy or a waiver of any other rights or remedies, and no single or partial exercise of any right or remedy under the Freecharge UPI Terms of Use will prevent any further exercise of the right or remedy or the exercise of any other right or remedy.</p>
    <p>22. The terms and conditions of the Freecharge UPI Terms of Use, which by their nature and content are intended to survive the performance hereof by any or all parties hereto will so survive the completion and termination of this Contract.</p>
    <p>23. If any provision of the Freecharge UPI Terms of Use is or becomes, in whole or in part, invalid or unenforceable but would be valid or enforceable if some part of that provision was deleted, that provision will apply with such deletions as may be necessary to make it valid. If any court/tribunal of competent jurisdiction holds any of the provisions of the Freecharge UPI Terms of Use unlawful or otherwise ineffective, the remainder of the Freecharge UPI Terms of Use will remain in full force and the unlawful or otherwise ineffective provision will be substituted by a new provision reflecting the intent of the provision so substituted.</p>
  </div>
</li> 
</ul>
</section>
</div>
</div>
<style type="text/css">
table.freeAxis tr td {
  border: 1px solid #a4a9ac;
  font-size: 14px;
  padding: 5px;
}
table.freeAxis tr td p{
  margin-bottom: 0px !important;
}
table.freeAxis{
  border-collapse: collapse;
}
.static-pages .static-content li.upiListStyle ul{
  list-style-type: none !important;
}
.static-content li.margBottom p{
    margin-top:10px;
} 
</style>
<script type="text/javascript">
  $( function() {
    $('#tnc-ntaccordion .cbp-nttrigger').off('click').on('click', function(){
      var $contentEl = $(this).parent();
      if($contentEl.is('.cbp-ntopen')){
        $contentEl.removeClass('cbp-ntopen');
      }else{
        $contentEl.addClass('cbp-ntopen');
      }
    });

  } );
</script>
