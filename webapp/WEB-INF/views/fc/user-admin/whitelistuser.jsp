<%@page import="com.freecharge.web.controller.WhiteListFraudController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<form:form method="POST" action="<%=WhiteListFraudController.WHITELIST_USER_ACTION_LINK%>">
    <label for="email"><spring:message code="enter.user.email.id" text="#enter.user.email.id#"/>:</label>
    <form:input path = "email"></form:input>
    <input type="submit" name="Submit" value="Whitelist">
    <div class="red"><form:errors path = "email"></form:errors></div>
</form:form>

<form:form method="POST" action="<%=WhiteListFraudController.BLACKLIST_USER_ACTION_LINK%>">
    <label for="email"><spring:message code="enter.user.email.id" text="#enter.user.email.id#"/>:</label>
    <form:input path = "email"></form:input>
    <input type="submit" name="Submit" value="Blacklist">
    <div class="red"><form:errors path = "email"></form:errors></div>
</form:form>