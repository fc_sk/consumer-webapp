<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Result Of Whitelisting User</title>
</head>
<body>
    <h4>
        <c:out value="${result}" />
    </h4>
    <c:if test="${not empty alreadyWhitelisted}">
        <h4>The following users were Already Banned</h4>
        <c:forEach var="alreadyWhitelistedUser" items="${alreadyWhitelisted}">
           ${alreadyWhitelistedUser}<br>
        </c:forEach>
    </c:if>
    <c:if test="${not empty newlyWhiteListed}">
        <h4>The following users are newly banned</h4>
        <c:forEach var="newlyWhitelistedUser" items="${newlyWhiteListed}">
          ${newlyWhitelistedUser}<br>
        </c:forEach>
    </c:if>
    <c:if test="${not empty notFoundList}">
       <h4>The following users were not found</h4>
       <c:forEach var="notFoundList" items="${notFoundList}">
          ${notFoundList}<br>
        </c:forEach>
    </c:if>
</body>
</html>