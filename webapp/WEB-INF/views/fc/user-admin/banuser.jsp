<%@page import="com.freecharge.web.controller.UserBanController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<form:form method="POST" action="<%=UserBanController.UNBLOCK_USER_ACTION_LINK%>">
    <label for="email"><spring:message code="enter.user.email.id" text="#enter.user.email.id#"/>:</label>
    <form:input path = "email"></form:input>
    <input type="submit" name="Submit" value="Unblock">
    <div class="red"><form:errors path = "email"></form:errors></div>
</form:form>

<form:form method="POST" action="/admin/user/ban/blockmanyusers">
	<label for="email"><spring:message code="enter.user.email.ids"
			text="#enter.user.email.ids#" />:</label>
	<a href="#" rel="tooltip" class="tip"
		title="Enter each email id in new line" data-original-title="Help">[?]</a>
	</b>
	<br />
	<textarea id="emailIds" name="emailIds"></textarea>
	<br />
	<input type="submit" name="Submit" value="Block">
	<div class="red">
		<form:errors path="email"></form:errors>
	</div>
</form:form>