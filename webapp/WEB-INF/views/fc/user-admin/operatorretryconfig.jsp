<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- .mainContainer Starts  -->
<div class="mainContainer"  >
    <div class="pageWidth clearfix form">
           <div class="manage_plans boxDesign" >
             <h1>Manage Operator Retry Configuration</h1>
             <form:form  name="operatorretryform" action="#" method="POST" cssClass="form" id="operatorretryform" > 
                <div class="manage_plans_Form">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <p> *Aggregator Name</p>
                                <select class="validate[required] select1 softNormText"  name="aggrName" id="aggrName">
                                    <option value="euronet"  >euronet</option>
                                    <option value="oxigen"  >oxigen</option>
                                    <option value="billPaymentOxigenClient"  >billPaymentOxigenClient</option>
                                    <option value="BillPaymentEuronetClient"  >BillPaymentEuronetClient</option>
                                </select>
                                <c:if test="${not empty OPERATOR_RETRY}">
                                    <script type="text/javascript">
                                        $("#aggrName").val("${OPERATOR_RETRY.aggrName}");
                                    </script> 
                                </c:if>
                            </td>
                            <td>
                                <p> *Operator Name</p>
                                <select class="validate[required] select1 softNormText" name="operator" id="operator">
                                    <option value="" selected >Choose one</option>
                                    <c:forEach var="allOperators" items="${ALL_OPERATORS}">
                                        <option value="${allOperators.operatorMasterId}"  >${allOperators.operatorCode}</option>
                                    </c:forEach>
                                </select>
                                <c:if test="${not empty OPERATOR_RETRY}">
                                    <script type="text/javascript">
                                        $("#operator").val(${OPERATOR_RETRY.fkOperatorMasterId});
                                    </script> 
                                </c:if>
                            </td>

                            <td>
                                 <p> *Aggregator Response Code </p>
                                <input maxlength="40" type="text" class="validate[required] input1 softNormText"  name="aggrRespCode" id="aggrRespCode" value="${OPERATOR_RETRY.aggrRespCode}">
                                <c:if test="${not empty OPERATOR_RETRY}">
                                    <script type="text/javascript">
                                        $("#aggrRespCode").val('${OPERATOR_RETRY.aggrRespCode}');
                                    </script> 
                                </c:if>
                            </td>
                            
                             <td>
                                <p> *Is Retryable</p>
                                <select class="validate[required] select1 softNormText"  name="isRetryable" id="isRetryable">
                                    <option value="true"  selected>true</option>
                                    <option value="false"  >false</option>
                                </select>
                                <c:if test="${not empty OPERATOR_RETRY}">
                                    <script type="text/javascript">
                                        $("#isRetryable").val("${OPERATOR_RETRY.isRetryable}");
                                    </script> 
                                </c:if>
                            </td>
                            
                            <td>
                                <p> *Retry Operator Name</p>
                                <select class="validate[required] select1 softNormText" name="retryOperator" id="retryOperator">
                                    <option value="" selected >Choose one</option>
                                    <c:forEach var="allOperators" items="${ALL_OPERATORS}">
                                        <option value="${allOperators.operatorMasterId}"  >${allOperators.operatorCode}</option>
                                    </c:forEach>
                                </select>
                                <c:if test="${not empty OPERATOR_RETRY}">
                                    <script type="text/javascript">
                                        $("#retryOperator").val(${OPERATOR_RETRY.fkRetryOperatorMasterId});
                                    </script> 
                                </c:if>
                            </td>
                            <td>
                                <p> *Retry Priority</p>
                                <select class="validate[required] select1 softNormText"  name="retryPriority" id="retryPriority">
                                    <option value="" selected >Choose one</option>
                                    <option value="1"  >1</option>
                                    <option value="2"  >2</option>
                                    <option value="3"  >3</option>
                                    <option value="4"  >4</option>
                                    <option value="5"  >5</option>
                                    <option value="6"  >6</option>
                                    <option value="7"  >7</option>
                                    <option value="8"  >8</option>
                                    <option value="9"  >9</option>
                                </select>
                                <c:if test="${not empty OPERATOR_RETRY}">
                                    <script type="text/javascript">
                                        $("#retryPriority").val(${OPERATOR_RETRY.retryPriority});
                                    </script> 
                                </c:if>
                            </td>
                        </tr>
                    </table>
                    <c:choose>
                        <c:when test="${not empty OPERATOR_RETRY}">
                            <input type="hidden" name="id" id="id" value="${OPERATOR_RETRY.id}" />
                            <p><input  type="button" class="regularButton" value="Save" onclick="saveOperatorRetryConfig()"> &nbsp&nbsp&nbsp
                            <input  type="button" class="regularButton" value="Delete" onclick="deleteOperatorRetryConfig()"></p>
                        </c:when>
                        <c:otherwise>
                            <input type="hidden" name="id" id="id" value="-1" />
                            <p><input  type="button" class="form_button" value="Add" onclick="saveOperatorRetryConfig()"></p>
                        </c:otherwise>
                    </c:choose>
                    <p><br/><span id="operatorRetryStatus" style="color: red;"></span></p>
                </div>
             </form:form>
             
            <div  class="view_plans boxDesign" >
            <h1>View All Operator Retry Configs</h1>
            
            <br/>
            <hr/>
            <br/>
            <c:choose>
				<c:when test="true">
				<table style="width:100%" class="myRechargesTable tablesorter" id="operatorTable">
				
				<thead>
				    <tr>
				        <th filter='false'>Chk<br><input type="checkbox" name="checkit" id="checkit" value="Check All"></th><th filter='true'>id</th><th filter-type='ddl'>Aggregator Name</th><th filter-type='ddl'>Operator Name</th><th filter-type='ddl'>Aggregator Response Code</th><th filter-type='ddl'>Is Retryable</th>
				        <th  filter-type='ddl'>Retry Operator Name</th><th  filter-type='ddl'>Retry Priority</th><th>Edit</th>
				    </tr>
				</thead>
				<tbody>
				    <c:forEach var="retryMap" items="${ALL_OPERATOR_RETRY_MAP}">
				        <tr>
				            <td><input type="checkbox" name="check" id=${retryMap.id} value=${retryMap.id} /></td>
				            <td><a href="/admin/operatorretry/home?id=${retryMap.id}">${retryMap.id}</a></td><td>${retryMap.aggrName}</td><td>${retryMap.fkOperatorMasterId}</td><td>${retryMap.aggrRespCode}</td><td>${retryMap.isRetryable}</td><td>${retryMap.retryOperatorName}</td><td>${retryMap.retryPriority}</td>
				            <td><a href="/admin/operatorretry/home?id=${retryMap.id}">Edit</a></td>
				        </tr>
				    </c:forEach>
				</tbody>    
				</table>    
				</c:when>
				<c:otherwise>
				No Config found in database.
				</c:otherwise>
				</c:choose>
          </div>  
           </div>
           </div>
           </div>
           
           <script type="text/javascript">
	           function saveOperatorRetryConfig(){
	        	    var flag = $('#operatorretryform').validationEngine('validate');
	        	    if(flag){
	        	    var aggrName =$('#aggrName').val();
	        	    var fkOperatorMasterId = $('#operator').val();
	        	    var aggrRespCode = $('#aggrRespCode').val();
	        	    var isRetryable = $('#isRetryable').val();
	        	    var fkRetryOperatorMasterId = $('#retryOperator').val();
	        	    var retryPriority = $('#retryPriority').val();
	        	    var id = $('#id').val();
	        	    
	        	    $.blockUI({ css: {
	        	        border: 'none',
	        	        padding: '15px',
	        	        backgroundColor: '#000',
	        	        '-webkit-border-radius': '10px',
	        	        '-moz-border-radius': '10px',
	        	        opacity: .5,
	        	        color: '#fff'
	        	    } }); 
	        	    var url = '/admin/operatorretry/save';
	        	    
	        	    var postObject = {
	                        'aggrName': aggrName,
	                        'fkOperatorMasterId':fkOperatorMasterId,
	                        'aggrRespCode':aggrRespCode,
	                        'isRetryable':isRetryable,
	                        'fkRetryOperatorMasterId':fkRetryOperatorMasterId,
	                        'retryPriority':retryPriority,
	                        'id':id
	                    };
	        	    
	        	    $.ajax({
	        	        url: url,
	        	        type: "POST",
	        	        data: postObject,
	        	        dataType: "html",
	        	        cache: false,
	        	        timeout:1800000,
	        	        success: function (responseText) {
	        	            var responseObj = {};
	        	            try {
	        	                responseObj = jQuery.parseJSON(responseText);
	        	            } catch(e) {
	        	                
	        	            }
	        	            if(responseObj.STATUS == 'Error'){
	        	                $('#operatorRetryStatus').html(responseObj.ERROR_MESSAGE);
	        	                
	        	            } else if(responseObj.STATUS == 'Updated'){
	        	                document.location.href = window.location.href.split('?')[0];
	        	            } else{
	        	                document.operatorretryform.reset();
	        	                document.location.href = window.location.href.split('?')[0];
	        	            }
	        	            $.unblockUI();
	        	        }
	        	    }); 
	        	    }
	        	}
	           
	           function deleteOperatorRetryConfig(){
	        	    var flag = $('#operatorretryform').validationEngine('validate');
	        	    if(flag){
	        	    var aggrName =$('#aggrName').val();
	        	    var fkOperatorMasterId = $('#operator').val();
	        	    var aggrRespCode = $('#aggrRespCode').val();
	        	    var isRetryable = $('#isRetryable').val();
	        	    var fkRetryOperatorMasterId = $('#retryOperator').val();
	        	    var retryPriority = $('#retryPriority').val();
	        	    var id = $('#id').val();
	        	    
	        	    $.blockUI({ css: {
	        	        border: 'none',
	        	        padding: '15px',
	        	        backgroundColor: '#000',
	        	        '-webkit-border-radius': '10px',
	        	        '-moz-border-radius': '10px',
	        	        opacity: .5,
	        	        color: '#fff'
	        	    } }); 
	        	    var url = '/admin/operatorretry/delete';
	        	    
	        	    var postObject = {
	                        'aggrName': aggrName,
	                        'fkOperatorMasterId':fkOperatorMasterId,
	                        'aggrRespCode':aggrRespCode,
	                        'isRetryable':isRetryable,
	                        'fkRetryOperatorMasterId':fkRetryOperatorMasterId,
	                        'retryPriority':retryPriority,
	                        'id':id
	                    };
	        	    
	        	    $.ajax({
	        	        url: url,
	        	        type: "POST",
	        	        data: postObject,
	        	        dataType: "html",
	        	        cache: false,
	        	        timeout:1800000,
	        	        success: function (responseText) {
	        	            var responseObj = {};
	        	            try {
	        	                responseObj = jQuery.parseJSON(responseText);
	        	            } catch(e) {
	        	                
	        	            }
	        	            if(responseObj.STATUS == 'Error'){
	        	                $('#operatorRetryStatus').html(responseObj.ERROR_MESSAGE);
	        	                
	        	            } else if(responseObj.STATUS == 'Deleted'){
	        	                alert("Deleted Successfully.");
	        	                document.location.href = window.location.href.split('?')[0];
	        	            } else{
	        	                document.operatorretryform.reset();
	        	                document.location.href = window.location.href.split('?')[0];
	        	            }
	        	            $.unblockUI();
	        	        }
	        	    }); 
	        	    }
	        	}
	           
				function getCheckedList(){
				    var check = document.getElementsByName("check");
				    var checked_id_list=[];
				    for(var i = 0; i < check.length; i++) {
				        if(check[i].checked) {
				            checked_id_list.push(check[i].value);
				        }
				    }
				    return checked_id_list.toString();
				}
				$('#checkit').click(function() {
				    var list = $('[name="check"]');
				    var visible_list= list.filter(':visible');
				    for(var i = 0; i < visible_list.length; i++) {
				        if($('#checkit').is(':checked'))
				            visible_list[i].checked=true;
				        else
				            visible_list[i].checked=false;
				    }
				});
				</script>
				
				<link href="${mt:keyValue("jsprefix2")}/tablesorter/themes/blue/style.css" rel="stylesheet" type="text/css"/>
				<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tablesorter/jquery.tablesorter.min.js?v=${mt:keyValue("version.no")}"></script>
				
				<script type="text/javascript">
				$(function() {      
				    $("#operatorTable").tablesorter({
				        sortList:[[2,1]], 
				        widgets: ['zebra'],
				        headers: { 
				            0: { 
				                sorter: false 
				            },
				            11: { 
				                sorter: false 
				            }
				        } 
				    });
				}); 
				</script>
				<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tablefilter/table.filter.min.js?v=${mt:keyValue("version.no")}"></script>
				<script type="text/javascript">
				$(document).ready(function() {
				    
				    var options = {
				
				            clearFiltersControls: [$('#cleanfilters')], 
				            enableCookies : false,
				
				        };
				
				    $('#operatorTable').tableFilter(options);
				});
		</script>
