<%@ page import="com.freecharge.cashbackcampaign.controller.CashBackCampaignController" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<c:if test="${isEligibleForCashBackCampaign}">
    <div class="secretCode">
        <p class="discount-title clearfix">
            <em><spring:message code="cash.back.campaign.heading"/></em>
            <span class="icon-info" id="secretCode-tip"></span>
        </p>
        <div id="secretCode-tooltip" style="display: none">
            <spring:message code="cash.back.campaign.description"/>
        </div>
        <div class="layer discount-content">
	         <input id="cashBackCampaignCodeId"  name="cashBackCampaignCode"  class="discount-input" value="${cashBackCampaignCode}" type="text" placeholder="<spring:message code="cash.back.campaign.input.placeholder"/>" />
		    <c:if test="${showCampaign}">
		    	<button class="buttonOne small-button" id="applyCashBackCampaign" onClick="applyCashBackCampaign(this, '#removeCashBackCampaign', true);" >Apply</button>
		    	<button class="small-button remove-link" style="display:none" id="removeCashBackCampaign" onClick="applyCashBackCampaign('#applyCashBackCampaign', '#removeCashBackCampaign', false)">Remove</button>
		    </c:if>
		    <c:if test="${alreadyOpted}">	
		    	<button class="buttonOne small-button"  style="display:none" id="applyCashBackCampaign" onClick="applyCashBackCampaign(this, '#removeCashBackCampaign', true);" >Apply</button>
		    	<button class="small-button remove-link" id="removeCashBackCampaign" onClick="applyCashBackCampaign('#applyCashBackCampaign', '#removeCashBackCampaign', false)">Remove</button>
			</c:if>
        </div>
        <div id="cashBackCampaignApplyMsgHolder" class="fc-message promoMsgHolder"  style="display:none;"></div>
    </div>
<script type="text/javascript" >
function applyCashBackCampaign(obj, removeId, flag) {
        if (flag) {
        	
        	 if($("#removePromo").is(':visible'))
             {
               alert("Err, looks like you have already availed your discount!");
                 return false;
             }
        	 
        	var url = '<%=CashBackCampaignController.APPLY_ACTION%>';
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    'code': $('#cashBackCampaignCodeId').val()
                },
                dataType: "json",
                cache: false,
                timeout:1800000,
                success: function (responseText) {
                    if (responseText.valid) {
                        $( removeId ).show();
                        $(obj).hide();
                        $("#cashBackCampaignCodeId" ).attr("disabled", "disabled");
                        $("#cashBackCampaignApplyMsgHolder").show().html('<p class="successMsgSmall">' + responseText.message + '</p>');
                    } else {
                    	$("#cashBackCampaignApplyMsgHolder").show().html('<p class="failureMsgSmall">' + responseText.message + '</p>');
                    }
                },
                error:function (xhr,status,error){
                    alert("Error occurred " + error);
                }
            })
        } else {
        	var url = '<%=CashBackCampaignController.REMOVE_ACTION%>';
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    'code': $('#cashBackCampaignCodeId').val()
                },
                dataType: "json",
                cache: false,
                timeout:1800000,
                success: function (responseText) {
                    if(responseText.valid) {
                        $( removeId ).hide();
                        $(obj).show();
                        $('#cashBackCampaignCodeId').val('');
                        $("#cashBackCampaignCodeId").removeAttr("disabled");
                        $("#cashBackCampaignApplyMsgHolder").hide().html('');
                    } else {
                        alert("Error occurred")
                    }
                },
                error:function (xhr,status,error){
                    alert("Error occurred " + error);
                }
            })
        }

        return false;
    }
	</script>
</c:if>
