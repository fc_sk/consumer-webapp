<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
    $(document).ready(function(){
      $('#frmUserLogin').validationEngine({
        ajaxFormValidation: true ,
        scroll:false
    });
    $('#frmRegister').validationEngine({
        ajaxFormValidation: true ,
        scroll:false
    }); 
    });
</script>

<h2 class="mainHeader">Continue to Checkout</h2>
<div class="clearfix form curveBlockWhite">
	<div class="formSignInWrap fLeft richText" >
        <h3 class="icoTitle" >
            <span class="ico signInIco" ></span>
            <span class="txt" ><strong>Returning Users</strong>  </span>
        </h3>

        <p id="paymentLoginError" class="failureMsgSmall" style="display: none;"></p>

        <form:form id="frmUserLogin" modelAttribute="userDO" action="javascript:void(0);" name="frmUserLogin" method="post">
            <div class="contentInner clearfix"  style="position:relative"  >
                <div class="formPatternVert clearfix  ">
                  <p class="label  ">Your Email Id :</p>
                  <span id="txt_email"></span>
                  <input type="text" name="email" class="validate[required,funcCall[checkEmail],custom[email]] softText input1"  maxlength="40"  id="emailId" />
                </div>
                <div class="formPatternVert clearfix">
                    <p class="label">Password :</p>
                    <span id="txt_password"></span>
                    <%--<input type="text" name="tsingInword2"  id="tsingInword2"   size="20" class="validate[required,funcCall[checkPassword],minSize[6],maxSize[20]] softText input1 fLeft"   value="Password" />--%>
                    <input type="password" name="password" class="validate[required] text-input,minSize[6],maxSize[20] input1 fLeft" id="personalDetailsSignInActual" />
                </div>

               <div class="formFieldRt clearfix">
                    <%--<input class="checkbox fLeft" type="checkbox" name="rememberme1" id="rememberme1"  > <label for="rememberme1" class="checkBoxLbl fLeft">Remember me</label>--%>
                    <a class="forgotPwdPop" href="javascript:void(0)">Forgot Password?</a>
               </div>
               <div class="formFieldRt clearfix">
                    <input type="submit" class="regularButton" value="  Sign In &amp; Continue  " onclick="_gaq.push(['_trackEvent', 'Button', 'Login' , 'Payment']);loginSubmit();"  >
                    <div  id="ajax-loginimage"  style="display:none;"  class="formLoaderOuter" >
                        <img src="${mt:keyValue("imgprefix1")}/images/ajax-loader.gif"/>
                    </div>
               </div>
            </div>  <!-- /.contentInner -->
        </form:form>
            
     </div><!-- /.formSignInWrap -->
     
 	<div class="formSignUpWrap fLeft richText" >
    	         
            <form class="clearfix" name="frmRegister" id="frmRegister" action="javascript:void(0);" method="post">
            <h3 class="icoTitle" >
                <span class="ico signUpIco" ></span>
                <span class="txt" ><strong>New Users</strong></span>
            </h3>
            <div class="contentInner clearfix"  style="position:relative; " >
                <div class="formWrapMainLt">
                   <div class="formFieldRt clearfix">
                       <label class="fLeft">Full Name : </label>
                       <select name="title" class="validate[required] titleSelect fLeft" id="title-info"  >
                            <option value="">Title</option>
                            <option value="Mr">Mr.</option>
                            <option value="Ms">Ms.</option>
                            <option value="Mrs">Mrs.</option>
                       </select>
                       <input type="text" class="validate[required ,funcCall[fullName] ,custom[onlyLetterSp]] text-input,minSize[4],maxSize[25] smlFirstName fLeft" maxlength="25"
                                   onBlur="clearText(this,'Full Name')" onFocus="clearText(this,'Full Name')"
                                   size="25" name="firstName" id="fname-info" value="Full Name"  />
                 
                   </div>
                   <div class="formFieldRt clearfix">
                       <label class="fLeft">Email  : </label>
                       <input type="text" class="validate[required,funcCall[checkEmail],custom[email],maxSize[90]] text-input softText input1 fLeft" maxlength="40" onBlur=" clearText(this,'Email');return checkUserAvail(id,'email-validation-message')"
                                   onFocus="clearText(this,'Email')" size="25" 
                                   name="email" id="email-info" value="Email"  >                       
                   </div>
                   <p style="display: none;color: red;" id="email-validation-message" class="failureMsgSmall" ></p>
                   
                   <div class="formFieldRt clearfix">
                       <label class="fLeft">Password  : </label>
                    <!--  <input type="password" value="Password" class="validate[required,funcCall[checkPassword],minSize[6],maxSize[20]] softText input1 fLeft" name="password" id="pass-info" 
                       			onfocus="passwordFocus(this);" 
                       			onblur="passwordBlur(this);"  maxlength="20"/>
                     -->
                       <input type="text" name="tsingInword3"  id="tsingInword3"   size="20" class="validate[required,funcCall[checkPassword],minSize[6],maxSize[20]] softText input1 fLeft"   value="Password"  onfocus="setFields1('tsingInword3','pass-info')" />
                       <input type="password" name="password" class="validate[required] text-input,minSize[6],maxSize[20] input1 fLeft" id="pass-info"   onBlur="setFields2('tsingInword3','pass-info')"  style="display: none;" />                      	               
 		           			
                       			
                   </div>
                   <div class="formFieldRt clearfix">
                       <label class="fLeft">Confirm Password  : </label>
                       <span>
                       <!-- <input type="password" value="Password" class="validate[required,funcCall[checkPassword],equals[pass-info],minSize[6],maxSize[20]] softText input1 fLeft" name="confirmPassword" id="cpass-info" 
                       			   onfocus="cpasswordFocus(this);" 
                       			   onblur="cpasswordBlur(this);" maxlength="20" />
                        -->			   
                       	<input type="text" name="tsingInword4"  id="tsingInword4"   size="20" class="validate[required,funcCall[confPassword] ,equals[passwordSignUp]] text-input,minSize[6],maxSize[20] input1 fLeft"   value="ConfirmPass"  onfocus="setFields1('tsingInword4','cpass-info')"  />
  						<input type="password" name="confirmPassword" id="cpass-info"   style="display: none;" onblur="setFields2('tsingInword4','cpass-info')" class="validate[required,equals[pass-info]] text-input,minSize[6],maxSize[20] input1 fLeft"  size="20"   onpaste = "return false;"/>		   
                       			   
                       </span>
                   </div>
                   
                </div> <!-- /.formWrapMainLt -->
                <%--<p class="formContTitleRt fLeft" > To check and track your past and present transactions. </p>--%>
             	<%--<span class="clear" >&nbsp;</span>--%>
             	
             	<div class="formWrapMainLt">
                   <div class="formFieldRt clearfix">
                       <label class="fLeft">Address  : </label>
                     	<textarea rows="1" cols="1"  name="address1" class="validate[required,funcCall[buildingOrStreet]] text-input softText input1 fLeft" id="address-info" maxlength="50"  onfocus="clearText(this,'Building / Street')" onblur="clearText(this,'Building / Street')"   >Building / Street</textarea>
                 </div>
                   <div class="formFieldRt clearfix">
                       <label class="fLeft">Area  : </label>
                       
                       <input class="validate[required,funcCall[area]] softText input1 fLeft" maxlength="50" onBlur="clearText(this,'Area')"
                                   onFocus="clearText(this,'Area')" value="Area" size="27" name="area"
                                   id='area-info'  >
                   </div>
                   <div class="formFieldRt clearfix">
                       <label class="fLeft">Landmark  : </label>
                       <input class="softText input1 fLeft" maxlength="50" onBlur="clearText(this,'Landmark')"
                                   onFocus="clearText(this,'Landmark')" value="Landmark" size="27" name="landmark"
                                   id='landmark-info'  >
                   </div>
           
                   <div class="formFieldRt clearfix">
                       <label class="fLeft">State  : </label>
                       <select id="state-info" name="stateId" onchange="getCityList(id,'citydiv1','cityId','city-info')" class="validate[required] softText select1 fLeft"  >
                                <option value="">Select State</option>
                                <c:forEach items="${stateMaster}" var="state">
                                    <option value="${state.stateMasterId}"> ${state.stateName}</option>
                                </c:forEach>
                            </select>                   
                   </div>
                   <div class="formFieldRt clearfix">
                       <label class="fLeft">City  : </label>
                       <span id="citydiv1">
                            <select name="cityId" class="validate[required] softText select1 fLeft" id="city-info"  >
                                <option value="">Select City</option>
                                
                            </select>
                        </span>
                   </div>
           
                  <div class="formFieldRt clearfix">
                       <label class="fLeft">Pin Code  : </label>
                       <input class="validate[required,funcCall[postalCode], custom[pincode]] validate[minSize[6]] validate[maxSize[6]] softText postalCode input1 fLeft" type="text" maxlength="6" size="22"
                                   onBlur="clearText(this,'PostalCode')" onFocus="clearText(this,'PostalCode')"
                                   value="PostalCode" name="postalCode" onkeypress="return isNumberKey(event,6,id)" id="pincode-info"  >                   
                   </div>
                   <div class="formFieldRt clearfix">
                       <label class="fLeft">Contact Number  : </label>
     					<input type="text" value="Mobile No" id="mobile-info" name="mobileNo" size="25" onfocus="clearText(this,'Mobile No')" onblur="clearText(this,'Mobile No')" maxlength="10" class="validate[required,funcCall[mobileNo] ,custom[phone]] validate[minSize[10]] validate[maxSize[10]] softText  input1 fLeft" onkeypress="return isNumberKey(event,10,id)"  >            
                   </div>
                   <div class="formFieldRt clearfix">
                        <label class="fLeft">&nbsp; </label>
                        <input type="checkbox" value="yes" name="terms"  id="terms-cond-info"   class="checkbox validate[required] fLeft" /><label for="terms-cond-info" class="checkBoxLbl fLeft"  id="txt_terms_cond">I agree to <a target="_blank" class="link2"  href="/app/termsandconditions.htm" tabindex="-1">terms and conditions</a></label>
                   </div>

                   <span id="paymentSignupError" style="color:red; width:100%; float:left; text-align:center; display:none;"></span>
                </div> <!-- /.formWrapMainLt -->
                <%--<p class="formContTitleRt fLeft" > Need to send your Free Coupons at your door step. </p>--%>
                <%--<span class="clear" >&nbsp;</span>--%>
                 <div class="formFieldRt clearfix">
                      <label class="fLeft">&nbsp; </label>
                      <input type="submit"  class="regularButton" value="  Register &amp; Continue  "  onclick="_gaq.push(['_trackEvent', 'Button', 'Signup' , 'Payment']);signupSubmit();" >
                 </div>
                 <div  id="ajax-signupimage"  style="display:none;"  class="formLoaderOuter" ><img src="${mt:keyValue("imgprefix1")}/images/ajax-loader.gif"/></div>   
           </div>
           
       </form>
    
 
   </div><!-- /.formSignUpWrap -->
   
</div>   
<script type="text/javascript">
function passwordFocus(obj){
	var passid = '#'+obj.id;
	if($(passid).val() == 'Password'){
		$(passid).val('');
		$(passid).removeClass('validate[required,funcCall[checkPassword],minSize[6],maxSize[20]] softText input1 fLeft');
		$(passid).addClass('validate[required,minSize[6],maxSize[20]] softText input1 fLeft');
	}
}
function passwordBlur(obj){
	var passid = '#'+obj.id;
	if($(passid).val() == ''){
		$(passid).val('Password');
		$(passid).removeClass('validate[required,minSize[6],maxSize[20]] softText input1 fLeft');
		$(passid).addClass('validate[required,funcCall[checkPassword],minSize[6],maxSize[20]] softText input1 fLeft');
	}
}
function cpasswordFocus(obj){
	var passid = '#'+obj.id;
	if($(passid).val() == 'Password'){
		$(passid).val('');
		$(passid).removeClass('validate[required,funcCall[checkPassword],equals[pass-info],minSize[6],maxSize[20]] softText input1 fLeft');
		$(passid).addClass('validate[required,equals[pass-info],minSize[6],maxSize[20]] softText input1 fLeft');
	}
}
function cpasswordBlur(obj){
	var passid = '#'+obj.id;
	if($(passid).val() == ''){
		$(passid).val('Password');
		$(passid).removeClass('validate[required,equals[pass-info],minSize[6],maxSize[20]] softText input1 fLeft');
		$(passid).addClass('validate[required,funcCall[checkPassword],equals[pass-info],minSize[6],maxSize[20]] softText input1 fLeft');
	}
}
</script> 