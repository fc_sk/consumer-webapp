<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MIS Login</title>
<style>

p.error {
color: RED;

}

.txt1 {
	   border-color: #AABBFF;
	   border: 1px solid; 
	   font: bold 84% 'trebuchet ms',helvetica,sans-serif; 
}

input.btn { 
    color: #0000FF;
    font: 95% ;
    padding: 3px 20px;
} 	

.theader{
	bgcolor="FFCCFF";
}
li.leftNavLink{
	list-style-type: none;
	padding:2px;
}
ul.leftNavPanel{
	margin:0px;
	paddding-left:20px;
}
th{
	background-color: gray;
	color: white;
	font-weight: bold;
	padding: 5px;
	text-align: left;
}
td{
	padding-left:6px;
	padding-right:10px;
	padding-top:3px;
	padding-bottom:3px;
	
}
td.entityID{
	width:30px;
	text-align: right;
}
tr.color0{
	background-color: #F0F0F0;
}
tr.color1{
	background-color: #D0D0D0;
}
body {
	font-family: verdana;
	font-size: 13px;
	color: #444444;
}
div.header{
			
		
    background-color: #AAAAFF;
    color: #5555FF;
    font-family: Trebuchet MS;
    font-size: 35px;
    font-weight: bold;
    padding: 20px;
    text-align: center;

 
}
div.leftMenu{
	padding-right:60px;
	float:left;
}
div.displayList{
	float:left;
	width:60%;
}
p.addLink{
	padding-left:10px;
	float:left;
}
input.txt, select.txt{
padding:5px;
font-size: small;
width:200px;
}
p.addHeader{
	font-size:15px;
	 
}
</style>

</head>
<body topmargin="0" leftmargin="0" rightmargin="0">
<div class="header">Generic MIS VIEW</div>
<c:url var="adminLogin" value="/genericMis/login" />
<form:form modelAttribute="AdminUsers" method="POST" action="/genericMis/login.htm">

	<table align="center" width="35%" border="0">
	<tr>
	<td align="right">User Name</td>
	<td align="left"><form:input path="adminUserName" cssClass="txt"  /></td>
	</tr>
	<tr>
	<td align="right">Password</td>
	<td align="left"><form:password path="adminUserPassword" cssClass="txt"  /></td>
	</tr>
	<tr><td align="center" colspan="2" ><p class="error"><c:out value="${message}"/></p></td></tr>
	<tr>
	<td></td>
	<td align="left"><input type="submit" value="Login" class="btn"  /></td>
	</tr>
	</table>
</form:form>

</body>
</html>
