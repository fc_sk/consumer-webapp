<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript">

var PROMOCODE = new function()  {

	this.issuePromocodeReward = function(id){
		var $this = $('#'+id);
		var requestUrl = "/reward/issue/" + $this.data("order-id") ;
		__issueReward(id, requestUrl);
	}
	
	this.redrawCaptcha = function(){
		$("#captcha-draw-div").load('/captcha/regenerate.htm');
	}
	
	this.redeemCode = function() {
		var couponCode = $("input[name='freefundCode']").val();
		fcTrack({eventName: 'RedeemNowAttempt', val0: couponCode});
		$(".freefund-code-error").html("");
		$(".freefund-code-success").html("");
		$(".freefund-code-error").hide();
		var responseText = $.ajax({
	        url : "/promocode/apply.htm",
	        type : "POST",
	        data : {"couponCode" :couponCode.trim(), "lookupID" : '<%=request.getParameter("lid")%>', "captcha" : $("input[name='captcha']").val(), "isGosf" : '<%=request.getParameter("gosf")%>'},
	        dataType : "json",
	        async : false,
	        cache : false
	    }).responseText;
		
		var responseJson = null;
	    try {
	    	responseJson = jQuery.parseJSON(responseText);
	    } catch(err) {
	    	console.log(err);
	    }	
	    
	    if (responseJson == null || responseJson.length == 0){
	    	console.log( "Exception processing response." );
	    	$(".freefund-code-error").show();
        	$(".freefund-code-error").html("Something went wrong. Please try later.");
        	fcTrack({eventName: 'RedeemNowFailure', val0: couponCode});
	    }  else if (responseJson.status == 'redeemed') {
			$(".freefund-code-success").show();
			$(".freefund-code-success").html(responseJson.successMsg);
			//window.location.reload(true);
		} else if (responseJson.status == 'success') {
			$(".freefund-code-success").show();
			$(".freefund-code-success").html(responseJson.successMsg);
			if((responseJson.codeType == "generic-cashback-promocode") || (responseJson.codeType == "cashback-promocode"))  {
				return;
			}
			window.location.reload(true);
		} else if(responseJson.status == 'failure') {
        	if(responseJson.forceCaptcha) {
        		$("#captcha-placement-div").show()
        		this.redrawCaptcha() ;
        	}
        	$(".freefund-code-error").show();
        	$(".freefund-code-error").html(responseJson.errors);
        	fcTrack({eventName: 'RedeemNowFailure', val0: couponCode});
        }
	}
	
	var __issueReward = function(id, requestUrl){ //private function
		var $this = $('#'+id);
		var rewardData = $.ajax({
	        url : requestUrl,
	        dataType : "json",
	        async : false,
	        cache : false
	    }).responseText;
	      
	    var rewardDataJson = null;
	    try {
	    	rewardDataJson = jQuery.parseJSON(rewardData);
	    } catch(err) {
	    	console.log(err);
	    }	
	    
	    if (rewardDataJson == null || rewardDataJson.length == 0){
	    	console.log( "No promocode reward found." );
	    } else if (rewardDataJson.hasused) {
	    	console.log( "Sorry ! This transaction not valid for a reward." );
	    } else {
	    	
	    	$("#promocode-gift-price").html(rewardDataJson.value);
	    	$("#promocode-gift-validity").html(rewardDataJson.validUpto);
	    	$("#promocode-gift-code").html(rewardDataJson.promocode);
	    	$this.show();
	    }  <%--end else --%>
	    
	}
	
}  
	</script>
