<%@ page import="com.freecharge.common.util.FCConstants"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@page pageEncoding="UTF-8"%>
<%@page contentType="text/html;charset=UTF-8"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>

<c:forEach items="${resultMap}" var="mapEntry">
	<c:set var="requestMap"
		value="${requestMap}&${mapEntry.key}=${mapEntry.value}"></c:set>
	<c:if test="${mapEntry.key == 'rcmobilenumber'}">
		<c:set var="rcmobilenumber" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'Amount'}">
		<c:set var="amount" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'suc'}">
		<c:set var="issuccess" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'shippingaddress'}">
		<c:set var="shippingaddress" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customeremailid'}">
		<c:set var="customeremailid" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'orderId'}">
		<c:set var="orderId" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'Paymentstatus'}">
		<c:set var="Paymentstatus" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'RechargeResponseCode'}">
		<c:set var="RechargeResponseCode" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customername'}">
		<c:set var="customername" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customeraddress'}">
		<c:set var="customeraddress" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customerarea'}">
		<c:set var="customerarea" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customerlandmark'}">
		<c:set var="customerlandmark" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customercity'}">
		<c:set var="customercity" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customerstate'}">
		<c:set var="customerstate" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customerpincode'}">
		<c:set var="customerpincode" value="${mapEntry.value}"></c:set>
	</c:if>
</c:forEach>
<div class="twelve columns">
    <c:choose>
        <c:when test="${RechargeResponseCode == '00'}">
            <jsp:include page="rechargesuccessbody.jsp" />
        </c:when>
        <c:when test="${RechargeResponseCode == '08' || RechargeResponseCode == '-1'}">
            <jsp:include page="rechargesuccessbody.jsp" />
        </c:when>
        <c:when test="${RechargeResponseCode == '06'}">
            <jsp:include page="rechargefailbody.jsp" />
        </c:when>
        <c:otherwise>
            <jsp:include page="rechargefailbody.jsp" />
        </c:otherwise>
    </c:choose>
    <div id="promocode-gift-summary" style="display: none;" class="promocode-gift-summary" data-order-id="${orderId}">
	   <div class="box">
           <h2>Promo Code Gift</h2>
           <div class="layer promocode-gift-details" >
                <h6>Congratulations!</h6>
                <p>You have been awarded a complimentary gift-code. Please check your inbox.</p>
           </div>
           <div class="promocode-gift-voucher clearfix" >
                <div class="promocode-gift-voucher-amount" >
                    <span><strong>Rs.</strong> <span id="promocode-gift-price"></span> </span>
                    <p>Instant Discount</p>
                </div>
                <div class="promocode-gift-voucher-code" >
                    <strong id="promocode-gift-code"></strong>
                    <p>valid until: <b  id="promocode-gift-validity"></b></p>
                </div>
           </div>
	   </div>
    </div>  
    <div id="order-coupons">
	   <div class="coupon-summary box">
	           <h2>Coupon & Offer Codes</h2>
	           <div class="layer clearfix">
	                   	<div id="couponsDiv" data-order-id="${orderId}"></div>
	   		</div>
	   </div>
    </div>    
    <div id="final-summary">
    <c:if  test="${(pCouponsSelected) || (eCouponsSelected)}" >
        <div class="coupon-summary box">
            <h2>Coupon Summary</h2>
            <div class="layer clearfix">
            <c:if test="${(pCouponsSelected)}" >
                <div <c:if test="${pCouponsSelected && eCouponsSelected}" > class="left" </c:if> >
                    <p>
                        Your <span class="text-color-2">printed coupons</span> (if selected) will be sent to your shipping address within 7
                        working days.
                    </p>
                    <div class="block-content">
                        <p> <c:out value="${customername}" /> </p>
                        <p>
                            <c:out value="${customeraddress}" />,<c:out value="${customerarea}" />,
                            <c:out value="${customerlandmark}" />,<c:out value="${customercity}" />,
                            <c:out value="${customerstate}" />,<c:out value="${customerpincode}" />
                        </p>
                    </div>
                </div>
                </c:if>
                <c:if test="${(eCouponsSelected)}" >
                <div  <c:if test="${pCouponsSelected && eCouponsSelected}" > class="right"</c:if> >
                    <p>
                        Your <span class="text-color-2">e-coupons</span> (if selected) will be sent to the following e-mail address.
                    </p>
                    <div class="block-content">
                        <p>
                            <c:out value="${userEmailId}" />
                        </p>
                    </div>
                </div>
                </c:if>
            </div>
        </div>
        </c:if>
        <div class="special-offers-summary box">
            <h2>Special Offers</h2>
            <div class="layer">
                <p>
                    Your <span class="text-color-2">special offers</span> will be sent
                    to the following e-mail address:
                    <div class="block-content">
                        <p><c:out value="${customeremailid}" /> </p>
                    </div>
                    <c:if test="${RechargeResponseCode == '00' && isCouponDisplayEnabled}">
                    	<div id="couponsDiv" data-order-id="${orderId}"></div>
                    </c:if>	
                </p>
            </div>
        </div>
        <div class="customer-support">
            Got a query or concern? Get in touch with customer care at <a href="mailto:care@freecharge.in">care@freecharge.in</a> and we&#39;ll sort things out.
        </div>
        <div class="actions">
            <p><em><strong>Hint:</strong> You can try recharging again the same number and plan in 30 minutes! </em></p>
            <div class="buttons">
            <c:if test="${RechargeResponseCode == '00'}">
                <a id="recharge-invoice" href="/app/invoice.htm?orderId=${orderId}" target="_blank" class="button">View Invoice</a>
            </c:if>
                <a href="/?recharge-again" class="button buttonTwo">Do Another Recharge</a>
                <%--</form:form>--%>
            </div>
        </div>
    </div>
</div>

<div class="four columns rechargestatus_banner">
  <c:choose>
    <c:when test="${isZedoAdEnabled}">
      <div>
         <center>
         <!-- Javascript tag  -->
         <!-- begin ZEDO for channel:  Post Transaction Page New Ver1 , publisher: freecharge.in , Dimension: Wide Skyscraper - 160 x 600 -->
	 <script language="JavaScript">
	   var zflag_nid="2427"; var zflag_cid="4"; var zflag_sid="2"; var zflag_width="160"; var zflag_height="600"; var zflag_sz="7";
	 </script>
	 <script language="JavaScript" src="https://saxp.zedo.com/jsc/sxp2/fo.js"></script>
	 <!-- end ZEDO for channel:  Post Transaction Page New Ver1 , publisher: freecharge.in , Dimension: Wide Skyscraper - 160 x 600 -->
         </center>
       </div>
    </c:when>
    <c:otherwise>
      <a href="/mobile-app-download" onclick="_gaq.push(['_trackEvent', 'Success-Banner', 'Click', 'mobileAppDownload']);" 
       target="_blank" ><img class="fluid" src='${mt:keyValue("imgprefix1")}/images/banner/mobile-app-banner2.jpg' 
       width="220" height="210" alt="FreeCharge on Google play" title="FreeCharge on the go - Get it now on Google play" /></a>

      <div class="googlePlus_banner">
        <div class="g-plus" data-width="170" data-height="69"
            data-href="//plus.google.com/+freecharge/" data-rel="publisher"></div>
      </div>
    </c:otherwise>
  </c:choose>
</div>

<script type="text/javascript">
    $(document).ready(function (){
    	PROMOCODE.issuePromocodeReward("promocode-gift-summary");
        <c:if test="${isCouponDisplayEnabled}">
        loadCouponsForOrder("couponsDiv");
        </c:if>

        $(".marketing-campaign-banner").fancybox();
    });

</script>

<jsp:include page="./promocode-js.jsp"></jsp:include>
<jsp:include page="./coupons-js.jsp"></jsp:include>
