<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- .mainContainer Starts -->
<div class="mainContainer">
    <div class="pageWidth clearfix">
    	 <h1 class="pageHeader clearfix " ><img src="${mt:keyValue("imgprefix1")}/images/cross.png" alt="" width="30" height="30" class="ico fLeft" > <span class="mainTxt fLeft" >Sorry! <span class="softTxt" > Payment unsuccessful  due to invalid payment mode.</span> </span></span></h1>

        <div class="successMsgWrapp curveBlockWhite clearfix">
        
        	<div class="rechargeSummary rechargeSummaryShort" >                               	
               	<p class="txt" ><strong>Order Summary</strong></p>
                <div class="formFieldAdd clearfix">
                    <label class="fLeft">Order ID  &nbsp; :   </label>
                    <span class="inputOutput fLeft ">NIL</span>
                </div>
                <div class="formFieldAdd clearfix">
                    <label class="fLeft">Mobile Number  &nbsp; :   </label>
                    <span class="inputOutput fLeft "><c:out value="${rechargeInitiateWebDo.mobileNo}"/></span>
                </div>
                
                <div class="formFieldAdd clearfix">
                    <label class="fLeft">Recharge Amount &nbsp; :   </label>
                    <span class="inputOutput fLeft "> <span class="webRupee">Rs</span><c:out value="${rechargeInitiateWebDo.amount}"/></span>
                </div>
           </div>
           
           <p> <!-- Recharge on <strong>9278126722</strong> for <strong>Rs. 500</strong>  &nbsp;<span class="redClr" >is unsuccessful due to invalid payment mode.</span> <br> -->
           	Please select another payment options.     </p>
            <form:form modelAttribute="txnHomePageWebDO" action="payment_section.html" method="get">
                <input type="hidden" name="lookupID" value="${rechargeInitiateWebDo.lookupID}"/>

                <a href="javascript:submitForEdit('edit')" class="regularButton fLeft">Select Payment Option <span><span>&nbsp;</span></span> </a>
 
            </form:form>

            <br><br>
        </div>
        <!-- /.successMsgWrapper -->

    </div>
</div>
<!-- .mainContainer Ends -->
