<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MIS Service</title>
<style>
.error {
	color: #ff0000;
	font-style: italic;
}

.txt1 {
	border-color: #AABBFF;
	border: 1px solid;
	font: bold 84% 'trebuchet ms', helvetica, sans-serif;
}

input.btn {
	color: #0000FF;
	font: 95%;
	padding: 3px 20px;
}

.theader {bgcolor = "FFCCFF";
	
}

li.leftNavLink {
	list-style-type: none;
	padding: 2px;
}

ul.leftNavPanel {
	margin: 0px;
	paddding-left: 20px;
}

th {
	background-color: gray;
	color: white;
	font-weight: bold;
	padding: 5px;
	text-align: left;
}

td {
	padding-left: 6px;
	padding-right: 10px;
	padding-top: 3px;
	padding-bottom: 3px;
}

td.entityID {
	width: 30px;
	text-align: right;
}

tr.color0 {
	background-color: #F0F0F0;
}

tr.color1 {
	background-color: #D0D0D0;
}

body {
	font-family: verdana;
	font-size: 10px;
	color: #444444;
}

div.leftMenu {
	padding-right: 60px;
	float: left;
}

div.displayList {
	float: left;
	width: 60%;
}

p.addLink {
	padding-left: 10px;
	float: left;
}

input.txt,select.txt {
	padding: 0px;
	font-size: 10px;
	width: 100px;
}

p.addHeader {
	font-size: 15px;
}

div.header {
	background-color: #AAAAFF;
	color: #5555FF;
	font-family: Trebuchet MS;
	font-size: 35px;
	font-weight: bold;
	padding: 20px;
	text-align: center;
}

.floatRight {
	float: right;
}

div.searchCriteria {
	border: thin solid #AAAAFF;
	left: 5%;
	margin: 5px;
	position: relative;
	width: 90%;
}
</style>
<link rel="stylesheet"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css?v=${mt:keyValue("version.no")}"
	type="text/css" media="all" />
<link rel="stylesheet"
	href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css?v=${mt:keyValue("version.no")}"
	type="text/css" media="all" />
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js?v=${mt:keyValue("version.no")}"
	type="text/javascript"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js?v=${mt:keyValue("version.no")}"
	type="text/javascript"></script>
<script
	src="http://jquery-ui.googlecode.com/svn/tags/latest/external/jquery.bgiframe-2.1.2.js?v=${mt:keyValue("version.no")}"
	type="text/javascript"></script>
</head>

<body>
	<div class="header">Customer Balance Transaction UI</div>
	<div id="msg"></div>

	<div>
		<table align="left" width="100%" border="0">
			<tr>
				<td align="left"><h2>Now your Current FC
						Balance balance is Rs ${balance}</h2>
				</td>
			</tr>
		</table>
	</div>

	<c:url var="walletWithdrawal" value="/admin/withdrawal" />
	<form:form modelAttribute="AdminUsers" method="POST"
		action="/admin/withdrawal.htm">

		<tr>
			<td align="right" colspan="2"><p class="success">
					<h1><c:out value="${message}"/></h1>
				</p>
			</td>
		</tr>
		</table>
	</form:form>
</body>
</html>
