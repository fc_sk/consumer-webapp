<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<div id="signupPageId" style="display: none;">
    <div class="lightbox">
        <div class="register-window">
            <div class="main">
                <h3><spring:message code="sign.up" text="#sign.up#"/></h3>
                <p><spring:message code="welcome" text="#welcome#"/>! <spring:message code="signin.repeatuser" text="#signin.repeatuser#"/>.</p>
                <form:form  name="SignUpForm" action="javascript:void(0)" method="post" modelAttribute="userDO" id="SignUpForm">
                    <div class="signup-form" >
                        <div class="left-panel">
                            <h4><spring:message code="contact.info" text="#contact.info#"/>.</h4>
                            <div class="row title">
                                <spring:bind path="userDO.title">
                                    <form:select path="title" id="title" name="title" cssClass="validate[required]" >
                                        <option value=""><spring:message code="select.title" text="#select.title#"/></option>
                                        <option value="Mr"><spring:message code="mr" text="#mr#"/>.</option>
                                        <option value="Ms"><spring:message code="ms" text="#ms#"/>.</option>
                                        <option value="Mrs"><spring:message code="mrs" text="#mrs#"/>.</option>
                                    </form:select>
                                </spring:bind>
                            </div>
                            <div class="row name">
                                <spring:bind path="userDO.firstName">
                                    <form:input id="firstName" path="firstName" name="firstName" placeholder="Full name" cssClass="validate[required,funcCall[fullName],custom[onlyLetterSp]] text-input,minSize[4],maxSize[25]" />
                                </spring:bind>
                            </div>
                            <div class="row mobile">
                                <spring:bind path="userDO.mobileNo">
                                    <form:input id="mobileNo"  path="mobileNo" placeholder="Mobile number" maxlength="10" cssClass="validate[required,funcCall[yourMobileNoRequired] ,custom[phone]] text-input,minSize[10],maxSize[10] text-input" onkeypress="return isNumberKey(event,10,id)" />
                                    <form:errors path="mobileNo"></form:errors>
                                </spring:bind>
                            </div>

                            <div class="row email">
                                <p style="display: none;"  id="emailvalidationmessage" class="failureMsgSmall" ></p>
                                <spring:bind path="userDO.email">
                                    <form:input id="userEmailID" path="email" name="email" placeholder="email address" cssClass="validate[required,funcCall[checkEmail],custom[email]] text-input,maxSize[90]" onblur="return checkUserAvail(id,'emailvalidationmessage');" />
                                    <form:errors path="email"></form:errors>
                                </spring:bind>
                            </div>

                            <div class="row password">
                                <input type="password" name="password" id="passwordSignUp" placeholder="password" class="validate[required] text-input,minSize[6],maxSize[20]"  size="20" />
                            </div>
                            <div class="row password-confirm">
                                <input placeholder="Confirm password" type="password" name="confirmPassword" id="cpasswordSignUp" class="validate[required,equals[passwordSignUp]] text-input,minSize[6],maxSize[20]"  size="20"   onpaste = "return false;"/>
                            </div>
                        </div>

                        <div class="right-panel">
                            <h4><spring:message code="address.details" text="#address.details#"/></h4>
                            <div class="row address">
                                <input placeholder="Address" type="text" id="address1" name='address1' maxlength="50" class="validate[required,funcCall[buildingOrStreet]] text-input" />
                            </div>
                            <div class="row area">
                                <spring:bind path="userDO.area">
                                    <form:input id="area"  path="area" placeholder="Area" maxlength="50" cssClass="validate[required,funcCall[area]] text-input"  />
                                </spring:bind>
                            </div>
                            <div class="row landmark">
                                <spring:bind path="userDO.landmark">
                                    <form:input id="landmark"  path="landmark" maxlength="50" placeholder="Landmark" />
                                </spring:bind>
                            </div>
                            <div class="row state">
                                <select id="state" name="stateId"  class="validate[required]" onchange="getCityListfromHeader()" >
                                    <option value=""><spring:message code="select.state" text="#select.state#"/></option>
                                    <c:forEach var="stateMaster" items="${stateMaster}">
                                        <option value='${stateMaster.stateMasterId}'>${stateMaster.stateName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="row city">
                                <spring:bind path="userDO.cityId">
                                    <form:select path="cityId" id="city" name="city" cssClass="validate[required]" >
                                        <option value=""><spring:message code="select.city" text="#select.city#"/></option>
                                    </form:select>
                                </spring:bind>
                            </div>
                            <div class="row pincode">
                                <spring:bind path="userDO.postalCode">
                                    <form:input id="postalCode" type="text" maxlength="6" path="postalCode" placeholder="Pincode" size="10" cssClass="validate[required,funcCall[postalCode], custom[pincode]] text-input,minSize[6],maxSize[6]" onkeypress="return isNumberKey(event,6,id)" />
                                </spring:bind>
                            </div>
                        </div>

                        <div class="row button-wrap">
                            <p><spring:message code="signup.agree" text="#signup.agree#"/> <a target="_blank" href="/app/termsandconditions.htm"><spring:message code="terms.conditions" text="#terms.conditions#"/></a></p>
                            <button id="register-button" onclick="return validateSignUpForm();"
                                    class="buttonTwo">Sign Up</button>
                        </div>
                    </div>
                </form:form>
            </div>
            <div class="sub">
                <p><spring:message code="already.freecharger" text="#already.freecharger#"/>! <a id="login" href="#loginPage"><strong><spring:message code="login.here" text="#login.here#"/></strong></a></p>
            </div>
        </div>
    </div>
</div>
