        <nav>
            <ul>
                <li><a href="/app/aboutus.htm">About Us</a></li>
                <li><a href="/app/contactus.htm"  >Contact Us</a></li>
                <!--<li><a href="/app/faq.htm">FAQ</a></li>-->
                <li><a href="/app/sitemap.htm" class="active" >Sitemap</a></li>
                <%--<li><a href="/app/feedback.htm" >Feedback</a></li>--%>
                <li><a href="http://support.freecharge.in" >Customer Support</a></li>
                <li><a href="/app/termsandconditions.htm">Terms &amp; Conditions</a></li>
                <!--<li><a href="/app/privacypolicy.htm">Privacy Policy</a></li>
                 <li><a href="/security/disclosure.htm">Security Policy</a></li><br>-->
            </ul>
        </nav>

<div class="row">
    <div class="col-md-12">

        <section class="sitemap-content">
            <h1>Sitemap</h1>
            <a href="https://www.freecharge.in/online-mobile-recharge"><h2>Online Mobile Recharge</h2></a>
            <ul class="circle" >
                <li>
                    <a href="https://www.freecharge.in/airtel-prepaid-mobile-recharge">Airtel</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/aircel-prepaid-mobile-recharge">Aircel</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/vodafone-prepaid-mobile-recharge">Vodafone</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/bsnl-prepaid-mobile-recharge">BSNL</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/tata-docomo-gsm-prepaid-mobile-recharge">Tata Docomo GSM</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/idea-prepaid-mobile-recharge">Idea</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/tata-indicom-walky-prepaid-mobile-recharge">Tata Indicom Walky</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/mtnl-trump-prepaid-mobile-recharge">MTNL Trump</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/reliance-cdma-prepaid-mobile-recharge">Reliance CDMA</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/reliance-gsm-prepaid-mobile-recharge">Reliance GSM</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/tata-indicom-prepaid-mobile-recharge">Tata Indicom</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/uninor-prepaid-mobile-recharge">Uninor</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/mts-prepaid-mobile-recharge">MTS</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/videocon-prepaid-mobile-recharge">Videocon</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/virgin-cdma-prepaid-mobile-recharge">Virgin CDMA</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/virgin-gsm-prepaid-mobile-recharge">Virgin GSM</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/tata-docomo-cdma-prepaid-mobile-recharge">Tata Docomo CDMA</a>
                </li>
            </ul>
        </section>
        <section class="sitemap-content">
            <h2 class="color1">Online Mobile Recharge Plans</h2>
            <ul class="circle">
                <li><a href="https://www.freecharge.in/airtel-prepaid-plans">Airtel Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/aircel-prepaid-plans">Aircel Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/vodafone-prepaid-mobile-plans">Vodafone Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/bsnl-prepaid-mobile-plans">BSNL Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/tata-docomo-gsm-Prepaid-Plans">Tata Docomo GSM Plans</a></li>
                <li><a href="https://www.freecharge.in/idea-Prepaid-Mobile-Plans">IDEA Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/indicom-walky-Prepaid-Plans">Tata Indicom Walky Plans</a></li>
                <li><a href="https://www.freecharge.in/mtnl-delhi-Prepaid-Plans">MTNL Trump Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/reliance-cdma-plans">Reliance CDMA Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/reliance-Prepaid-Mobile-Plans">Reliance GSM Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/tata-indicom-Prepaid-Plans">Tata Indicom Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/uninor-Prepaid-Mobile-Plans">Uninor Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/mts-Prepaid-Mobile-Plans">MTS Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/videocon-Prepaid-Mobile-Plans">Videocon Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/virgin-cdma-Prepaid-Mobile-Plans">Virgin CDMA Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/virgin-gsm-Prepaid-Mobile-Plans">Virgin GSM Recharge Plans</a></li>
                <li><a href="https://www.freecharge.in/tata-docomo-cdma-Prepaid-Plans">Tata Docomo CDMA Plans</a></li>
            </ul>
        </section>
        <section class="sitemap-content">
            <h2><a href="https://www.freecharge.in/online-datacard-recharge">Online DataCard Recharge</a></h2>
            <ul class="circle">
                <li>
                    <a href="https://www.freecharge.in/tata-photon-plus-datacard-recharge">Tata Photon Plus</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/tata-photon-whiz-datacard-recharge">Tata Photon Whiz</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/mts-mblaze-recharge">MTS MBlaze</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/mts-mbrowse-datacard-recharge">MTS MBrowse</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/reliance-netconnect-datacard-recharge">Reliance NetConnect</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/airtel-datacard-recharge">Airtel</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/bsnl-datacard-recharge">BSNL</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/aircel-datacard-recharge">Aircel</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/mtnl-delhi-datacard-online-recharge">MTNL Delhi</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/vodafone-datacard-online-recharge">Vodafone</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/idea-datacard-recharge">Idea</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/mtnl-mumbai-datacard-recharge">MTNL Mumbai</a>
                </li>
            </ul>
        </section>
        <section class="sitemap-content">
            <h2><a href="https://www.freecharge.in/online-dth-recharge">Online DTH(TV) Recharge</a></h2>
            <ul class="circle">
                <li>
                    <a href="https://www.freecharge.in/online-airtel-dth-recharge">Airtel Digital TV</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/online-big-tv-recharge">Reliance Digital TV</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/online-dish-tv-recharge">Dish TV</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/online-tata-sky-recharge">Tata Sky</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/online-sun-direct-recharge">Sun Direct</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/videocon-d2h-online-recharge">Videocon D2H</a>
                </li>
            </ul>
        </section>
        <section class="sitemap-content">
            <h2 class="color1">PostPaid Bill Payment</h2>
            <ul class="circle">
                <li>
                    <a href="https://www.freecharge.in/airtel-mobile-postpaid-bill-payment">Airtel Bill Payment</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/bsnl-mobile-postpaid-bill-payment">BSNL Bill Payment</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/tata-docomo-gsm-mobile-postpaid-bill-payment">Tata Docomo GSM Bill Payment</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/tata-docomo-cdma-mobile-postpaid-bill-payment">Tata Docomo CDMA Bill Payment</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/idea-mobile-postpaid-bill-payment">Idea Bill Payment</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/vodafone-mobile-postpaid-bill-payment">Vodafone Bill Payment</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/reliance-gsm-mobile-postpaid-bill-payment">Reliance GSM Bill Payment</a>
                </li>
                <li>
                    <a href="https://www.freecharge.in/reliance-cdma-mobile-postpaid-bill-payment">Reliance CDMA Bill Payment</a>
                </li>
            </ul>
        </section>
        <section class="sitemap-content">
            <h2 class="color1">Freecharge</h2>
            <ul class="circle">
                <li><a href="/app/aboutus.htm">About US</a></li>
                <li><a href="/app/faq.htm">FAQ</a></li>
                <li><a href="http://support.freecharge.in">Support</a></li>
                <li><a href="/app/contactus.htm">Contact Us</a></li>
                <li><a href="/app/privacypolicy.htm">Privacy policy</a></li>
                <li><a href="/app/sitemap.htm" class="active">Sitemap</a></li>
                <li><a href="/app/termsandconditions.htm">T &amp; C</a></li>
                <li><a href="http://blog.freecharge.in/">Blog</a></li>
                <li><a href="/security/disclosure.htm">Security Policy</a></li>
            </ul>
        </section>
        <section class="sitemap-content">
            <h2 class="color1">Mobile</h2>
            <ul class="circle">
                <li><a href="https://play.google.com/store/apps/details?id=com.freecharge.android">Android App</a></li>
                <li><a href="http://www.windowsphone.com/en-in/store/app/freecharge-recharge-mobile-dth-data-card/353ad694-42a4-4b60-bfe6-c250a6526f17">Windows</a></li>
                <li><a href="https://www.freecharge.in/m">Mobile Site</a></li>
                <li><a href="https://itunes.apple.com/us/app/recharge-mobile-dth-online/id877495926">iOS</a></li>
            </ul>
        </section>
	</div>
</div>