<div class="message-bar" id="home-message-bar">
    <div class="container">
        <div class="sixteen columns">
            <p>
                <i class="icon-info-new"></i>Read what our customers have to say about their FreeCharge experience, 
                <a href="http://blog.freecharge.in/2013/09/16/for-the-love-of-freecharge/" 
                target="_blank" onclick="_gaq.push(['_trackEvent', 'Home info bar', 'More', 'Learn more'])" >here</a>.</p>
                <a onclick="$(this).parents('.message-bar').slideUp(200);" class="close icon-close"
                 onclick="_gaq.push(['_trackEvent', 'Home info bar', 'Close', 'Hide message'])"></a>
        </div>
    </div>
</div>