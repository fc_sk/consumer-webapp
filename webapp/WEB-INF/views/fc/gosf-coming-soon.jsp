<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GOSF 2014 - FreeCharge Offer:Mobile,DTH Recharge and Coupons Offer.</title>
    <meta name="description" content="GOSF-2014.BIG FreeCharge offers during the Great Online Shopping Festival 2014.299 offer &amp; cash back offer on prepaid,postpaid,DTH,datacard mobile online recharge .">
    <meta name="viewport" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no">
    <link rel="icon" href="${mt:keyValue('imgprefix1')}/images/campaign/gosf/favicon-new.ico?v=${mt:keyValue("version.no")}" type="image/x-icon">
    <link rel="shortcut icon" href="${mt:keyValue('imgprefix1')}/images/campaign/gosf/favicon-new.ico?v=${mt:keyValue("version.no")}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${mt:keyValue('imgprefix1')}/images/campaign/gosf/apple-touch-icon-new.png?v=${mt:keyValue("version.no")}">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/campaigns/gosf/style.min.css?v=${mt:keyValue("version.no")}" media="screen">
    <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/campaigns/gosf/jquery-1.11.1.min.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/campaigns/gosf/timer.min.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            startCountDown({
                secondsColor : "#2d5696",
                secondsShadow  : "none",
                
                minutesColor : "#4e8528",
                minutesShadow  : "none",
                
                hoursColor   : "#d49222",
                hoursShadow    : "none",
                
                daysColor    : "#a82738",
                daysShadow     : "none",
                
                startDate   : (new Date('12/01/2014').getTime())/1000,
                endDate     : (new Date('12/10/2014').getTime())/1000,
                currentDate : (new Date().getTime())/1000
            });
        });
    </script>   
</head>

<body>
    <div class="banner">
        <div class="fclogo">
                <a href="http://www.freecharge.in"><img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/freecharge-logo.png" alt="FreeCharge" title="Freecharge"></a>
        </div>
        <div class="ribbon">
           <a href="http://www.freecharge.in"> <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/ribbon.png" ></a>
        </div>
        <div class="offerImg" style="">
            <img src="${mt:keyValue('imgprefix1')}/images/campaign/gosf/offer.png" alt="Exciting offers" title="Exciting offers">
        </div>
    </div>
    <div class="wrapper">
        <div class="timer">
            <div class="days slot">
                <canvas id="can_days" width="180" height="180"></canvas>
                <div style="display:block;">
                    <p class="value">0</p>
                    <p class="text">Days</p>
                </div>
            </div>
            <div class="blinks">
                <blink> : </blink>
            </div>
            <div class="hours slot">
                <canvas id="can_hours" width="180" height="180"></canvas>
                <div style="display:block;">
                    <p class="value">0</p>
                    <p class="text">Hours</p>
                </div>
            </div>
            <div class="blinks">
                <blink> : </blink>
            </div>
            <div class="minutes slot">
                <canvas id="can_minutes" width="180" height="180"></canvas>
                <div style="display:block;">
                    <p class="value">0</p>
                    <p class="text">Minutes</p>
                </div>
            </div>
            <div class="blinks">
                <blink> : </blink>
            </div>
            <div class="seconds slot">
                <canvas id="can_seconds" width="180" height="180"></canvas>
                <div style="display:block;">
                    <p class="value">0</p>
                    <p class="text">Seconds</p>
                </div>
            </div>
        </div>
        <div class="bet">
            <p>You've seen Nothing like it,<br>
                WE BET!!</p>
        </div>
        <div class="beFirst">
            <p class="email-error"></p>
            <form id="emailForm">
                <label>Be the first to know</label>
                <input name="email" placeholder="Enter your Email ID" id="emailId"> <button id="submitButton">SUBMIT</button>
            </form>
            <p class="email-success" ></p>
        </div>
        <div class="aboutGOSF">
            <h1>Great Online Shopping Festival – GOSF India 2014</h1>
            <p>GOSF by Google India, brings to you the biggest online shopping festival on 10,11 &amp; 12 of December, 2014. The Great Online Shopping Festival (GOSF) was created by Google India on 12 December 2012 in collaboration with a number of Indian online shopping portals. The GOSF was conceptualized in order to boost the eCommerce sales in India on the lines of Black Friday by promoting online services &amp; products from major eCommerce players and by giving huge discounts to customers. Customers can get different online deals and offers from a wide range of categories. Customers can walk through offers from different ecommerce websites in GOSF and find suitable deals and offers based on their interest. <br><br>FreeCharge in association with GOSF, brings to you some exciting offers on all online recharges including prepaid, postpaid recharge, data card recharge and DTH recharges you do on FreeCharge during GOSF days. Some of the Biggest Offers ever are waiting for you. Stay tuned!</p>
        </div>
    </div>
    <script src="${mt:keyValue("jsprefix2")}/campaigns/gosf/parsley.min.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript">
        function blinkIt() {
           if (!document.getElementsByTagName('blink')) {
            return;
            }
           else {
             for(i=0;i<document.getElementsByTagName('blink').length;i++){
                  s=document.getElementsByTagName('blink')[i];
                  s.style.visibility=(s.style.visibility=='visible')?'hidden':'visible';
                }
            }
        }
        $(document).scroll(function() {
            var scrollTop = $(document).scrollTop(),
                about = $('.aboutGOSF').position().top;

            if($('.beFirst').offset().top + $('.beFirst').height() 
             >= $('.aboutGOSF').offset().top - 10)
                $('.beFirst').css('position', 'relative');
            if($(document).scrollTop() + window.innerHeight < $('.aboutGOSF').offset().top)
                $('.beFirst').css('position', 'fixed'); 
        });


        function callback(dataWeGotViaJsonp){
            if(dataWeGotViaJsonp.status == "failure"){
                    $(".email-error").html(dataWeGotViaJsonp.message);
            }
            else if(dataWeGotViaJsonp.status == "success"){
            $(".email-error").html('');
              $("#emailForm").fadeOut("normal").remove();
              $(".email-success").html(dataWeGotViaJsonp.message);
            }
        }

        function processRequest() {
            $.ajax({
               url:"userInput/email?emailId=" + $('#emailId').val(),
               dataType: 'json',
               error:function(data){
                 $(".email-error").html("Something went wrong. Please try again later");
               },
               success:function(data){
                callback(data);
               }      
            });       
        }
        $(document).ready(function(){
            setInterval('blinkIt()',500);


            $('#submitButton').off('').on('click', function(event) {
                  event.preventDefault();     
                  $(".email-error").html('');
                  var isValid = $('#emailForm').parsley('validate'); 
                  if(isValid) {
                      processRequest()
                  }       
            });  
        });
        
      
    </script>
</body>
</html>
