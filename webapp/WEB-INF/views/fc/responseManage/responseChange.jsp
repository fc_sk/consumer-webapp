<%@page import="com.freecharge.admin.responsehandler.RechargeResponseController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<form:form method="POST" action="<%=RechargeResponseController.RESPONSE_HANDLER_ACTION_LINK%>">
    <label for="affiliateTransId"><spring:message code="affiliate.transaction.id" text="#affiliate.transaction.id#"/>:</label>
    <form:input path="affiliateTransId" tabindex="0"></form:input>
    <input type="submit" name="Submit" value="Submit">
    <div class="red"><form:errors path="affiliateTransId"></form:errors></div>
</form:form>

