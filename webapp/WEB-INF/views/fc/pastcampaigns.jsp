<div class="row">
	<div class="col-md-12">
		<header><h1>Past Campaigns</h1></header>
		<section class="fc-box past-campaign-list">
           <div class="past-campaign-banner" >
           	   <h4>Axis Bank Credit / Debit Card Offer</h4>
               <a href="http://offers.freecharge.com/AxisBank/index.html" target="_blank" >
               		<img id="axisbank" title="Axis Bank Credit / Debit Card Offer" alt="Axis Bank Credit / Debit Card Offer"
               			src="http://offers.freecharge.com/AxisBank/images/AxisBank.png"
               			class="img-responsive center-block" ></a>
	       </div>
	       <div class="past-campaign-banner" >
	       		<h4>Dance India Dance 4</h4>
                <a href="https://www.freecharge.in/DanceIndiaDance4-details" target="_blank" >
               		<img id="did4" title="Dance India Dance 4" alt="Dance India Dance 4"
               			src="https://dmx246cm6p7k8.cloudfront.net/content/images/banner/DanceIndiaDance4-1.png"
               			class="img-responsive center-block" ></a>
           </div>
	       <div class="past-campaign-banner" >
	       		<h4>ICICI Bank Credit / Debit Card Offer</h4>
               	<a href="http://offers.freecharge.com/ICICI/index.html" target="_blank" >
               		<img id="icici" title="ICICI Bank Credit / Debit card Offer" alt="ICICI Bank Credit / Debit card Offer"
               			src="http://offers.freecharge.com/ICICI/images/ICICI.png"
               			class="img-responsive center-block" ></a>
	       </div>

	       <div class="past-campaign-banner" >
	       		<h4>Pepsi 60 Crore Tak Ka recharge</h4>
               	<a href="http://offers.freecharge.com/Pepsi-Redeem/pepsi-promotion.html" target="_blank" >
               		<img id="pepsipromotion" title="PEPSI Rs.60 Crore Tak Ka Recharge" alt="PEPSI Rs.60 Crore Tak Ka Recharge"
               			src="https://dmx246cm6p7k8.cloudfront.net/content/images/pepsi/banner2.png"
               			class="img-responsive center-block" ></a>
	       </div>

	       <div class="past-campaign-banner" >
	       		<h4>KINGFISHER</h4>
               		<img id="kingfisher" title="Kingfisher Offer"
               			src="https://www.freecharge.in/content/images/banner/kingfisherLogo_HPbanner.jpg"
               			class="img-responsive center-block" >
	       </div>
	    </section>
     </div>
</div>