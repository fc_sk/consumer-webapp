<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:choose>
    <c:when test="${not empty ALL_RECHARGE_PLANS}">
        <table class="zebra" id="myRechargesTable">
            <thead>
            <tr>
                <th width="30"></th>
                <th width="90">Amount</th>
                <th width="90">Talktime</th>
                <th>Offer details</th>
                <th width="100">Validity</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="rechargeplan" items="${ALL_RECHARGE_PLANS}">
                <tr>
                    <td>
                        <input type="radio" value="${rechargeplan.amount}" name="rechargePlan_amount">
                    </td>
                    <td><fmt:formatNumber type="number"
                                          maxFractionDigits="0" value="${rechargeplan.amount}" /></td>
                    <td><fmt:formatNumber type="number"
                                          maxFractionDigits="0" value="${rechargeplan.talktime}" /></td>
                    <td>${rechargeplan.description}</td>
                    <td>${rechargeplan.validity}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:when>
    <c:otherwise>
        <p>We are getting the best plans for you soon.</p>
    </c:otherwise>
</c:choose>