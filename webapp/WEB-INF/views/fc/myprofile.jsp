<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="my-account" >
    <div class="sixteen columns">
		<h1><spring:message code="my.account" text="#my.account#"/></h1>
    </div>
    <div class="three columns">
        <div class="my-account-list">
            <ul>
				<li><a href="mycredits.htm"><spring:message code="fc.money" text="#fc.money#"/></a></li>
				<li><a href="myrecharges.htm"><spring:message code="previous.recharges" text="#previous.recharges#"/></a></li>
				<li><a href="mycontacts.htm"><spring:message code="contacts" text="#contacts#"/></a></li>
				<li class="active"><a href="myprofile.htm"><spring:message code="profile" text="#profile#"/></a></li>
            	<li><a href="mycoupons.htm"><spring:message code="my.coupons" text="#coupons#"/></a></li>
			</ul>
        </div>
    </div>
    <div class="thirteen columns">
        <div class="my-account-content">
            <div id="profilecontentdiv">
				<%@ include file="Profilecontent.jsp"%>
			</div>
		</div>
	</div>
</div>