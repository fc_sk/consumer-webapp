<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<style>
.vcardtxn {
    background-color: LightCyan;
}
.p2ptxn{
	background-color: MintCream;
}
</style>
<div class="row">
	<div class="col-sm-3 col-md-3">
		<jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp"/>
	</div>
    <div class="col-sm-9 col-md-9">
    	   <div>
       		 <h4><i>Transaction details :</i></h4>
             <form method="GET" action="/admin/customertrail/onecheck/searchbyemail/result">
                	<div class="row customer-trail-input">
                	<div class="clearfix">
        				<div class="col-sm-3 col-md-3">
                			<h5>Email Id</h5>
                				<input type="text" name="emailid" value="${emailid}" style="width: 175px;">
                		</div>
                	    <div class="col-sm-3 col-md-3">
                			<h5>Start Date</h5>
                				<input type="text" name="startdate" id="startdate" value="${startdate}" style="width: 175px;">
                		</div>
                		<div class="col-sm-3 col-md-3">
                			<h5>End Date</h5>
                				<input type="text" name="enddate" id="enddate" value="${enddate}" style="width: 175px;">
                			</div>
                		<div class="col-sm-3 col-md-3">
                	<br/><br/><input type="submit" class="btn btn-default" value="Get">
                	</div>
                	</div>
                	</div>
             </form>
             <div class = "row">
             		<c:if test="${not empty prevkey}">
                        <div class="col-sm-3 col-md-3 pull-left customer-trail-input">
                            <form method="GET" action="/admin/customertrail/onecheck/searchbyemail/result">
                            <input type="hidden" name="emailid" value="${emailid}">
                            <input type="hidden" name="startdate" id="startdate" value="${startdate}">
                            <input type="hidden" name="enddate" id="enddate" value="${enddate}">
                            <input type="hidden" name="evaluatedkey" value="${prevkey}">
                            <input type="submit" value="<<Previous">
                            </form>
                        </div>
             		</c:if>
             		<c:if test="${not empty nextkey}">
                        <div class="col-sm-3 col-md-3 pull-right">
                            <form method="GET" action="/admin/customertrail/onecheck/searchbyemail/result">
                            <input type="hidden" name="emailid" value="${emailid}">
                            <input type="hidden" name="startdate" id="startdate" value="${startdate}">
                            <input type="hidden" name="enddate" id="enddate" value="${enddate}">
                            <input type="hidden" name="evaluatedkey" value="${nextkey}">
                            <input type="submit" value="Next>>">
                            </form>
                        </div>
                    </c:if>
			 </div>
  	       </div>  
  	       <hr>
           <div>
            <c:choose>
						<c:when test="${not empty fcwalletmaplist}">
						    <h5><b>Wallet Transaction details :</b></h5>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Timestamp</th>
										<th>Client</th>
										<th>OrderId</th>
										<th>Deposit</th>
										<th>Amount Credited to Bank</th>
										<th>Withdrawal Fee</th>
										<th>Total Balance</th>
										<th>Transaction status type</th>
										<th>IdempotencyId</th>
										<th>Meta data</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="wallettxnmap" items="${fcwalletmaplist}">
									    <c:choose>
                                            <c:when test="${wallettxnmap['type'] eq 'vcard'}">
                                                <tr class="vcardtxn">
                                            </c:when>
                                            <c:otherwise>
                                            	<c:choose>
		                                            <c:when test="${wallettxnmap['type'] eq 'p2p-p2m'}">
		                                                <tr class="p2ptxn">
		                                            </c:when>
		                                            <c:otherwise>
		                                                <tr class="wallettxn">
		                                            </c:otherwise>
                                        		</c:choose>
                                            </c:otherwise>
                                        </c:choose>
                                                    <td>${wallettxnmap['transactionDate']}</td>
                                                    <td>${wallettxnmap['merchantName']}</td>
                                                    <c:choose>
                                                        <c:when test="${wallettxnmap['hyperlinkref'] eq 'yes' }">
                                                           <td><a href="/admin/customertrail/getcustomertrail?orderId=${wallettxnmap['txnId']}">${wallettxnmap['txnId']} </a></td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:choose>
                                                                <c:when test="${wallettxnmap['hyperlinkref'] eq 'OC'}">
                                                                   <td><a href="/admin/customertrail/onecheck/OC/searchtxn.htm?txnId=${wallettxnmap['txnId']}">${wallettxnmap['txnId']}</a></td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                	<c:choose>
                                                                		<c:when test="${wallettxnmap['type'] eq 'vcard'}">
                                                                			<td><a href="/admin/customertrail/onecheck/vcardtxnstatus?vcardtxnId=${wallettxnmap['txnId']}">${wallettxnmap['txnId']}</a></td>
                                                                		</c:when>
                                                                		<c:otherwise>
                                                                    		<c:choose>
	                                                                    		<c:when test="${wallettxnmap['type'] eq 'p2p-p2m'}">
	                                                                				<td><a href="/admin/customertrail/getcustomertrail?orderId=${wallettxnmap['txnId']}">${wallettxnmap['txnId']}</a></td>
	                                                                			</c:when>
	                                                                			<c:otherwise>
	                                                                    			<td>${wallettxnmap['txnId']}</td>
	                                                                    		</c:otherwise>
                                                                    		</c:choose>
                                                                    	</c:otherwise>
                                                                    </c:choose>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <td>${wallettxnmap['deposit']}</td>
                                                    <td>${wallettxnmap['withdraw']}</td>
                                                    <td>${wallettxnmap['withdrawFee']}</td>
                                                    <td>${wallettxnmap['runningBalance']}</td>
                                                    <td><a href="#" data-toggle="tooltip" data-placement="right" title="${wallettxnmap['txnTypeToolTip']}">${wallettxnmap['transactionType']}</a></td>
                                                    <td>${wallettxnmap['idempotencyId']}</td>
                                                    <td>${wallettxnmap['eventContext']}</td>
                                                </tr>
									</c:forEach>
								</tbody>
							</table>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
           </div>
           <div>
           		<c:choose>
						<c:when test="${not empty klickpaymaplist}">
						    <h5><b>PG Transaction details :</b></h5>
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Timestamp</th>
										<th>Client</th>
										<th>OrderId</th>
										<th>Transaction amount</th>
										<th>Transaction status type</th>
										<th>PG transaction Id</th>
										<th>PG Used</th>
										<th>PG Underlier</th>
										<th>Meta data</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="klickpaytxnmap" items="${klickpaymaplist}">
										<tr>
											<td>${klickpaytxnmap['transactionDate']}</td>
											<td>${klickpaytxnmap['merchantName']}</td>
											<c:choose>
												<c:when test="${klickpaytxnmap['hyperlinkref'] eq 'yes'}">
												   <td><a href="/admin/customertrail/getcustomertrail?orderId=${klickpaytxnmap['txnId']}">${klickpaytxnmap['txnId']} </a></td>
												</c:when>
												<c:otherwise>
													<c:choose>
                                                    	<c:when test="${klickpaytxnmap['hyperlinkref'] eq 'OC'}">
                                                    	   <td><a href="/admin/customertrail/onecheck/OC/searchtxn.htm?txnId=${klickpaytxnmap['txnId']}">${klickpaytxnmap['txnId']}</a></td>
                                                    	</c:when>
                                                    	<c:otherwise>
                                                    		<td>${klickpaytxnmap['txnId']}</td>
                                                    	</c:otherwise>
                                                    </c:choose>
												</c:otherwise>
											</c:choose>
											<td>${klickpaytxnmap['txnamount']}</td>
											<td><a href="#" data-toggle="tooltip" data-placement="right" title="${klickpaytxnmap['txnTypeToolTip']}">${klickpaytxnmap['transactionType']}</a></td>
											<td>${klickpaytxnmap['pgtransactionid']}</td>
											<td>${klickpaytxnmap['pgused']}</td>
											<td>${klickpaytxnmap['pgunderlier']}</td>
											<td>${klickpaytxnmap['eventContext']}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
           </div>
           <div>
           		<c:choose>
						<c:when test="${not empty consumerCreditTxnMapList}">
						    <h5><b>Line Of Credit Transaction Details :</b></h5>
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Timestamp</th>
										<th>Merchant Name</th>
										<th>Merchant ID</th>
										<th>FC OrderId</th>
										<th>Transaction Amount</th>
										<th>Transaction Status</th>
										<th>Lender Transaction Id</th>
										<th>Name of Lender</th>
										<th>Lender ID</th>
										<th>EMI Tenure</th>
										<th>EMI Amount</th>
										<th>Interest Rate</th>
										<th>Credit Availability Status</th>
										<th>Total Approved Credit</th>
										<th>Credit Currently Available</th>
										<th>Loan Destination</th>
										<th>Processing fee</th>
										<th>Bank Details</th>
										<th>Account Number</th>
										<th>Loan Amount</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="consumerCreditTxnMap" items="${consumerCreditTxnMapList}">
										<tr>
											<td>${consumerCreditTxnMap['timestamp']}</td>
											<td>${consumerCreditTxnMap['merchantName']}</td>
											<td>${consumerCreditTxnMap['merchantId']}</td>
											<td>${consumerCreditTxnMap['fcOrderId']}</td>
											<td>${consumerCreditTxnMap['txnAmount']}</td>
											<td>${consumerCreditTxnMap['txnStatus']}</td>
											<td>${consumerCreditTxnMap['lenderTxnId']}</td>
											<td>${consumerCreditTxnMap['lenderName']}</td>
											<td>${consumerCreditTxnMap['lenderId']}</td>
											<td>${consumerCreditTxnMap['emiTenure']}</td>
											<td>${consumerCreditTxnMap['emiAmount']}</td>
											<td>${consumerCreditTxnMap['interestRate']}</td>
											<td>${consumerCreditTxnMap['creditAvailabilityStatus']}</td>
											<td>${consumerCreditTxnMap['totalApprovedCredit']}</td>
											<td>${consumerCreditTxnMap['creditCurrentlyAvailable']}</td>
											<td>${consumerCreditTxnMap['loanDestination']}</td>
											<td>${consumerCreditTxnMap['processingFee']}</td>
											<td>${consumerCreditTxnMap['bankDetails']}</td>
											<td>${consumerCreditTxnMap['accountNumber']}</td>
											<td>${consumerCreditTxnMap['loanAmount']}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
           </div>
           <div>
           		<c:choose>
						<c:when test="${not empty upiTxnMapList}">
						    <h5><b>UPI Transaction Details :</b></h5>
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>FC Transaction Id</th>
										<th>UPI Transaction Id</th>
										<th>Status</th>
										<th>Transaction date</th>
										<th>Amount</th>
										<th>Source UPI Id</th>
										<th>Destination UPI Id</th>
										<th>Error</th>
										<th>Result</th>
										<th>Error Code</th>
										<th>Promo Code</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="upiTxnMap" items="${upiTxnMapList}">
										<tr>
											<td>${upiTxnMap['fcTxnId']}</td>
											<td>${upiTxnMap['txnId']}</td>
											<td>${upiTxnMap['txnStatus']}</td>
											<td>${upiTxnMap['txnDate']}</td>
											<td>${upiTxnMap['txnAmount']}</td>
											<td>${upiTxnMap['sourceVPA']}</td>
											<td>${upiTxnMap['destinationVPA']}</td>
											<td>${upiTxnMap['errorMsg']}</td>
											<td>${upiTxnMap['businessUseCase']}</td>
											<td>${upiTxnMap['errorCode']}</td>
											<td>${upiTxnMap['promoCode']}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
           </div>
           <div>
           		<c:choose>
						<c:when test="${not empty debitEMITxnMapList}">
						    <h5><b>Debit EMI Transaction Details :</b></h5>
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Timestamp</th>
										<th>Merchant Name</th>
										<th>Merchant ID</th>
										<th>FC OrderId</th>
										<th>Transaction Amount</th>
										<th>Transaction Status</th>
										<th>Lender Transaction Id</th>
										<th>Name of Lender</th>
										<th>Lender ID</th>
										<th>EMI Tenure</th>
										<th>EMI Amount</th>
										<th>Interest Rate</th>
										<th>Loan Destination</th>
										<th>Processing fee</th>
										<th>Loan Amount</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="debitEMITxnMap" items="${debitEMITxnMapList}">
										<tr>
											<td>${debitEMITxnMap['timestamp']}</td>
											<td>${debitEMITxnMap['merchantName']}</td>
											<td>${debitEMITxnMap['merchantId']}</td>
											<td>${debitEMITxnMap['fcOrderId']}</td>
											<td>${debitEMITxnMap['txnAmount']}</td>
											<td>${debitEMITxnMap['txnStatus']}</td>
											<td>${debitEMITxnMap['lenderTxnId']}</td>
											<td>${debitEMITxnMap['lenderName']}</td>
											<td>${debitEMITxnMap['lenderId']}</td>
											<td>${debitEMITxnMap['emiTenure']}</td>
											<td>${debitEMITxnMap['emiAmount']}</td>
											<td>${debitEMITxnMap['interestRate']}</td>
											<td>${debitEMITxnMap['loanDestination']}</td>
											<td>${debitEMITxnMap['processingFee']}</td>
											<td>${debitEMITxnMap['loanAmount']}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
           </div>
    </div>  
</div>
<script type="text/javascript">

$(".oc-txn-id").on('click', function(){
    var result,
        $this = $(this);
    // find payment request status
    var transactionId = $this.text();
    $.blockUI({ css: {
           border: 'none',
           padding: '15px',
           backgroundColor: '#000',
           '-webkit-border-radius': '10px',
           '-moz-border-radius': '10px',
           opacity: .5,
           color: '#fff',
           'z-index' : '5001'
       },
       overlayCSS: {
           'z-index' : '5000'
       }
      });

    // send transaction detail request - ajax<a href="#user-email" data-toggle="modal" >${user.email}</a>
    $.ajax({
        url: '/admin/customertrail/onecheck/OC/gettxndetails?transactionId=' + transactionId,
        data: { get_param: 'value' }

    }).done( function (response){
        $.unblockUI();
        result = response.details;
        if(result==null)
        {
        	alert("Transaction : " + transactionId +"\n" + "Internal error occurred fetching details from OneCheck");
        }
        else
        {
        	alert("Transaction : " + response.transactionId+"\n" + "Details : "  + JSON.stringify(result, null, 2));
        }
    }).fail( function (response){
              $.unblockUI();
              result = response;
              alert("Error fetching status from server : " + response.status + " " + response.statusText);
          });
});


$(function() {
	$( "#startdate" ).datepicker({dateFormat: 'yy-mm-dd'});
	$( "#enddate" ).datepicker({dateFormat: 'yy-mm-dd'});
});
</script>