<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="createdOnDateValue" class="java.util.Date"/>
<jsp:useBean id="updatedOnDateValue" class="java.util.Date"/>
<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
		<div>
		    <c:if test="${not empty creditTokenStatus}">
		        <c:choose>
		            <c:when test="${creditTokenStatus eq 'Success'}">
                        <div class="alert alert-success">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <p><c:out value="${creditTokenStatus}"/>: <c:out value="${creditTokenMessage}"/> </p>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="alert alert-error">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <p><c:out value="${creditTokenStatus}"/>: <c:out value="${creditTokenMessage}"/> </p>
                        </div>
                    </c:otherwise>
                </c:choose>
            </c:if>
	        <h2><b> Bolt History List: </b></h2>
	        <c:url value="/admin/customertrail/growthclient/bolthistorybyemail/view.htm" var="userEmailURL">
               <c:param name="emailid" value="${user.email}"/>
            </c:url>
	        <h4> Email: <a href="${userEmailURL}"><c:out value="${user.email}"/></a> </h4>
	        <h4> Mobile No: <c:out value="${user.mobileNo}"/> </h4>
	        <c:if test="${not empty userPointDetailList}">
                    <h4> Total Bolts: <c:out value="${BoltBalance}"/></h4>
                <c:if test="${calledFrom eq 'BoltOrder'}">
                    <button class="btn btn-mini btn-primary creditTokensButton" data-toggle="modal"
                    data-target="#creditTokensModal">
                            Credit Tokens
                    </button>
                </c:if>
                <br><br>
                <table class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Points</th>
                            <th>Transaction Type</th>
                            <th>Reference ID</th>
                            <th>Created on</th>
                            <th>Updated on</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="userPointDetail" items="${userPointDetailList}">
                            <tr>
                                <td>${userPointDetail.userId}</td>
                                <td>${userPointDetail.points}</td>
                                <td>${userPointDetail.type}</td>
                                <td><a href="/admin/customertrail/growthclient/bolthistorybyorder/view.htm?orderid=${userPointDetail.referenceId}">${userPointDetail.referenceId}</a></td>
                                <td>
                                    <jsp:setProperty name="createdOnDateValue" property="time" value="${userPointDetail.createdOn}"/>
                                    <c:out value="${createdOnDateValue}"/>
                                </td>
                                <td>
                                    <jsp:setProperty name="updatedOnDateValue" property="time" value="${userPointDetail.updatedOn}"/>
                                    <c:out value="${updatedOnDateValue}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <!-- Modal for credit tokens -->
                <div class="modal fade" id="creditTokensModal" tabindex="-1" role="dialog"
                    aria-labelledby="myModalLabel" style="display: none; height:40%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Credit Tokens to User</h4>
                    </div>
                    <div class="modal-body">
                        <form name="creditTokensForm" method="GET" action="/admin/customertrail/growthclient/credittokenstouser/view.htm">
                            <div class="field-unit">
                                 <label>User ID</label>
                                 <input type="text" name="userid" id="userid"
                                    value="${user.userId}" readonly="readonly" />
                                 <label>Order ID</label>
                                 <input type="text" id="orderid" name="orderid"
                                    value="${boltorderid}" readonly="readonly"/>
                                 <label>Points</label>
                                 <input type="text" id="points" name="points" />
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="creditTokensSubmit">Credit
                                    Tokens</button>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal -->
                <!-- End of credit tokens modal -->

                <c:if test="${calledFrom eq 'BoltOrder'}">
                    <jsp:include page="rewardhistory.jsp" />
                </c:if>
			</c:if>
		</div>
	</div>
</div>