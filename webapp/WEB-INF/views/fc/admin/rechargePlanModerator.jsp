<%@include file="/WEB-INF/includes/taglibs.jsp"%> 

<div id="statusMessage" class="alert alert-success"  style="display:none;">
</div>
<div id="errorMessage" class="alert alert-failure"  style="display:none;">
</div>

<h1><spring:message code="recharge.moderation" text="#recharge.moderation#"/></h1> 
 <table class="myRechargesTable tablesorter" id="myRechargesModerationTable" data-csrf="${mt:csrfToken()}">
      <h2><spring:message code="list.recharge.plans" text="#list.recharge.plans#"/></h2>
      <a id="cleanfilters" href="javascript:void(0);">Clear Filters</a>
    <thead>
         <tr>
           <th><spring:message code="recharge.id" text="#recharge.id#"/></th>
           <th filter-type='ddl'><spring:message code="operator.master.id" text="#operator.master.id#"/></th>
           <th filter-type='ddl'><spring:message code="circle.master.id" text="#circle.master.id#"/></th>
           <th filter-type='ddl'><font color="green"><spring:message code="name" text="#name#"/></th>
           <th><spring:message code="amount" text="#amount#"/></th>
           <th><font color="green"><spring:message code="talk.time" text="#talk.time#"/></font></th>
           <th><font color="green"><spring:message code="validity" text="#validity#"/></font></th>
           <th><font color="green"><spring:message code="description" text="#description#"/></font></th>
           <th><spring:message code="source" text="#source#"/></th>
           <th><spring:message code="issue" text="#issue#"/></th>
          <th filter="false"><spring:message code="edit.info" text="#edit.info#"/></th>
         </tr>
    </thead>
       <tbody id="planListBody">
        <c:forEach var="plan" items="${ALL_RECHARGE_PLANS_IN_MODERATION}">
            <c:set var="count" value="${count + 1}" scope="page"/>
            <tr id=${plan._id}>
                <td readonly class="planId" align="center"><a
                    href="/admin/rechargeplans/home?recharge_plan_id=${plan._id }">${plan._id }</a></td>
                
                <c:forEach var="operator" items="${ALL_OPERATORS}">
                    <c:if test="${operator.operatorMasterId == plan.operatorMasterId}">
                        <td readonly class="planOperator" value='${plan.operatorMasterId}' align="center">${operator.operatorCode}</td>
                        
                    </c:if>
                 </c:forEach>
                 
                <c:forEach var="circle" items="${ALL_CIRCLES}">
                    <c:if test="${circle.circleMasterId == plan.circleMasterId}">
                        <td readonly class="planCircle" value='${plan.circleMasterId}' align="center">${circle.name}</td>
                    </c:if>
                </c:forEach>

                <td class="planName" align="center">${plan.name}</td>
                <td readonly class="planAmount" align="center">${plan.amount }</td>
                <td class="planTalktime" align="center">${plan.talktime }</td>
                <td class="planValidity" align="center">${plan.validity }</td>
                <td class="planDescription" align="center">${plan.description }</td>
                <td readonly class="planSource" align="center">${plan.source }</td>
                <td readonly class="planCause" align="center">${plan.cause }</td>
				<td readonly>
				    <a href="/admin/rechargeplans/moderation/edit.htm?_id=${plan._id }">Edit</a>&nbsp;&nbsp;
					<a href="javascript:void(0);" class="delete" data-plan-id=${plan._id}>Disable</a>&nbsp;&nbsp;
					<a href="javascript:void(0);" class="push"
					data-plan-id='${plan._id}' 
					data-plan-circle='${plan.circleMasterId}' 
					data-plan-operator='${plan.operatorMasterId}'>Enable</a>
				</td>
			</tr>
        </c:forEach>
    </tbody> 
 </table> 
 
<script type="text/javascript"> 
$(function () {
    $("td").dblclick(function () {
    	if(!$(this).is('[readonly]'))
    	{
	        var OriginalContent = $(this).text();
	        var clas = String($(this)[0].className);

	        if ( clas.indexOf("planName") != -1 ) {
	        	$(this).addClass("cellEditing");
	        	$(this).html("<select style='max-width:95%;'>" +
	        	"<option value='Topup'>Topup</option> " +
	        	"<option value='Data/2G'>Data/2G</option>" +
	        	"<option value='3G'>3G</option>" +
	        	"<option value='Other'>Other</option></select>");
	        }
	        else {
	        	$(this).addClass("cellEditing");
	        	$(this).html("<textarea style='max-width:95%;' >" + OriginalContent + "</textarea>");
	        }

	        $(this).children().first().focus();

	        $(this).children().first().keypress(function (e) {
	            if (e.which == 13) {
	                var newContent = $(this).val();
	                $(this).parent().text(newContent);
	                $(this).parent().removeClass("cellEditing");
	            }
	        });
	        
		    $(this).children().first().blur(function(){
		    	var newContent = $(this).val();
                $(this).parent().text(newContent);
                $(this).parent().removeClass("cellEditing");
		    });
    	}
    });
});

$('body').on('click', '.delete', function(event){
     var planIdVar = $(this).data('planId'),
        that = this;
     $.ajax({
        url:'/admin/rechargeplans/moderation/delete/'+planIdVar,
        type : 'GET',
        dataType:'JSON'
    }).done(function(response) {
    	if(response.status != 'Success') {
    		$(that).parent().parent().hide().remove();
             //$('#errorMessage').html('Error: Occured.' + response.result +' But deleted from Moderation.').show();
            alert('Error: Occured.' + response.result +' But deleted from Moderation.');
  } else {
	     $(that).parent().parent().hide().remove();
	     /* $('#statusMessage').html('Deleted Successfuly').show(); */
	     alert('Successfuly Deleted.');
  }
	});
});

$('body').on('click', '.push', function(event){
     event.preventDefault();
     var planIdVar = $(this).data('planId'),
                        amount=$(this).parent().siblings('.planAmount').text(),
                        name=$(this).parent().siblings('.planName').text(),
                        operator=$(this).data('planOperator'),
                        circle=$(this).data('planCircle');
     var that=this;
     var dataObject = { 
    		  rechargePlanId : $(this).data('planId'),
              name:  name,
              amount: amount,
              talktime: $(this).parent().siblings('.planTalktime').text(),
              circleMasterId: circle,
              operatorMasterId: operator,
              description: $(this).parent().siblings('.planDescription').text(),
              validity: $(this).parent().siblings('.planValidity').text(),
              csrfRequestIdentifier: $('#myRechargesModerationTable').data('csrf')
    };
     if (planIdVar == "" || amount == "" || name == "" || operator == "" || circle == "" || 
    		 dataObject.description == "" || dataObject.validity == "") {
         alert('Please enter valid data for all the fields.');
         return;
     }
     /* Fix for GET request when name is Data/2G */ 
     if(name == "Data/2G") {
    	 name = "2G";
     }
     console.log(dataObject);
     $.ajax({
             url: '/admin/rechargeplans/moderation/push',
             type: 'POST',
             data: dataObject,
             dataType: 'JSON'
      }).done(function(response) {
          if(response.STATUS == 'Fail') {
              $('#errorMessage').html(response.STATUS+': '+' Occured. '+response.ERROR_MESSAGE).show();
              $.ajax({
                  url: '/admin/rechargeplans/moderation/delete/'+rechargePlanId,
                  type : 'GET',
                  dataType:'JSON'
                }).done(function(response){
                    alert("Failed to Push. " + response.ERROR_MESSAGE + ". Deleted from moderation.");
                });
          } else {
        	  $.ajax({
                  url: '/admin/rechargeplans/moderation/duplicate/'+amount+'/'+name+'/'+circle+'/'+operator,
                  type : 'GET',
                  dataType:'JSON'
               }).done(function(response){
            	   $(response.result).each(function(index, element) {
	            	   $.ajax({
	                     url: '/admin/rechargeplans/moderation/delete/'+element,
	                     type : 'GET',
	                     dataType:'JSON'
	                   }).done(function(response){
	                	   if(response.status != 'Success') {
	                		   $("#"+element+"").hide().remove();
	                           $('#errorMessage').html(response.status+': '+' Occured. '+response.result).show();
	                           alert('Failed to delete some plans from moderation. ' + response.result);
	                       }
	                	   else {
	                		   $("#"+element+"").hide().remove();
	                	   }
		               });
        	     });
            	   /* $('#statusMessage').html('Recharge Plan Pushed Successfully. Also Deleted similar plans from Moderation').show(); */
            	   alert('Recharge Plan Pushed Successfully. Also Deleted similar plans from Moderation');
              });
           }
      });
});

</script>

<link href="${mt:keyValue("jsprefix2")}/tablesorter/themes/blue/style.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tablesorter/jquery.tablesorter.min.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript">
<!--
$(function() {      
    $("#myRechargesModerationTable").tablesorter({
        sortList:[], 
        widgets: ['zebra'],
        headers: { 
            0: { 
                sorter: false 
            },
            11: { 
                sorter: false 
            }
        } 
    });
}); 
//-->
</script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tablefilter/table.filter.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">
<!--
$(document).ready(function() {
    
    var options = {
    		clearFiltersControls: [$('#cleanfilters')],
            enableCookies : false
        };

    $('#myRechargesModerationTable').tableFilter(options);
});
//-->
</script>
