<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- .mainContainer Starts  -->
<c:choose>
<c:when test="${RECHARGE_PLANS_AVAILABLE}">
<a id="cleanfilters" href="javascript:void(0);">Clear Filters</a>

<p>
<input  type="button" class="form_button" value="Disable Status" onclick="location.href='javascript:updateStatus(getCheckedList(),false)'">&nbsp;&nbsp;
<input  type="button" class="form_button" value="Enable Status" onclick="location.href='javascript:updateStatus(getCheckedList(),true)'">
</p>
<table style="width:100%" class="myRechargesTable tablesorter" id="myRechargesTable">

<thead>
	<tr>
		<th filter='false'>Chk<br><input type="checkbox" name="checkit" id="checkit" value="Check All"></th><th filter='true'>id</th><th>Name</th><th>Amount</th><th>Min Amount</th><th>Max Amount</th>
		<th  filter-type='ddl'>Talktime</th><th  filter-type='ddl'>Validity</th><th>Description</th><th  filter-type='ddl'>Status</th><th  filter-type='ddl'>Sticky</th><th>Updated</th><th filter='false'>Edit</th><th>Source</th>
	</tr>
</thead>
<tbody>
	<c:forEach var="plan" items="${ALL_RECHARGE_PLANS}">
		<tr>
			<td><input type="checkbox" name="check" id=${plan.rechargePlanId} value=${plan.rechargePlanId} /></td>
			<td><a href="/admin/rechargeplans/home?recharge_plan_id=${plan.rechargePlanId}">${plan.rechargePlanId}</a></td><td>${plan.name}</td><td>${plan.amount}</td><td>${plan.minAmount}</td><td>${plan.maxAmount}</td><td>${plan.talktime}</td><td>${plan.validity}</td>
			<td>${plan.description}</td>
			<c:choose>
				<c:when test="${plan.status}">
					<td><b>Enabled</b> <a href="javascript:updateStatus(${plan.rechargePlanId},false)">Disable</a></td>
				</c:when>
				<c:otherwise>
					<td><b>Disabled</b> <a href="javascript:updateStatus(${plan.rechargePlanId},true)">Enable</a></td>
				</c:otherwise>
			</c:choose>
            <c:choose>
                <c:when test="${plan.sticky}">
                    <td><b>Enabled</b> <a href="javascript:updateSticky(${plan.rechargePlanId},false)">Disable</a></td>
                </c:when>
                <c:otherwise>
                    <td><b>Disabled</b> <a href="javascript:updateSticky(${plan.rechargePlanId},true)">Enable</a></td>
                </c:otherwise>
            </c:choose>
            <td>${plan.createdTime}</td>
			<td><a href="/admin/rechargeplans/home?recharge_plan_id=${plan.rechargePlanId}">Edit</a></td>
			<c:choose>
				<c:when test="${plan.ireffID != null}">
					<td>IREFF ${plan.ireffID}</td>
				</c:when>
				<c:otherwise>
					<td>OTHER</td>
				</c:otherwise>
			</c:choose>
			
		</tr>
	</c:forEach>
</tbody>	
</table>	
</c:when>
<c:otherwise>
No plans found.
</c:otherwise>
</c:choose>

<script type="text/javascript">
function getCheckedList(){
	var check = document.getElementsByName("check");
	var checked_id_list=[];
	for(var i = 0; i < check.length; i++) {
		if(check[i].checked) {
			checked_id_list.push(check[i].value);
		}
	}
	return checked_id_list.toString();
}
$('#checkit').click(function() {
	var list = $('[name="check"]');
	var visible_list= list.filter(':visible');
	for(var i = 0; i < visible_list.length; i++) {
		if($('#checkit').is(':checked'))
			visible_list[i].checked=true;
		else
			visible_list[i].checked=false;
	}
});
</script>

<link href="${mt:keyValue("jsprefix2")}/tablesorter/themes/blue/style.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tablesorter/jquery.tablesorter.min.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript">
<!--
$(function() {		
	$("#myRechargesTable").tablesorter({
		sortList:[[2,1]], 
		widgets: ['zebra'],
		headers: { 
            0: { 
                sorter: false 
            },
            11: { 
                sorter: false 
            }
        } 
	});
});	
//-->
</script>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tablefilter/table.filter.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">
<!--
$(document).ready(function() {
	
	var options = {

            clearFiltersControls: [$('#cleanfilters')], 
            enableCookies : false,

        };

	$('#myRechargesTable').tableFilter(options);
});
//-->
</script>
