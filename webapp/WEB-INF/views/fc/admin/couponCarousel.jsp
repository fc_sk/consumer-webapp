<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<body>
<div class="container-fluid">
    <div class="alert" style="display: none">

    </div>
    <div class="row-fluid">
        <div class="span12">
            <h2>Coupon carousel: </h2>
            <form class="form-horizontal" id="con-form">
                    <div class="control-group">
                        <div class="controls">
                            <input type="text" value="${fn:escapeXml(values)}" name="carousel">
                        </div>
                    </div>
                <div class="form-actions">
                    <a href="#" id="add-con"><i class="icon-plus-sign"></i> </a>
                    <a href="#" id="save-button" class="btn btn-primary">Save</a>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('a#save-button').on('click', function(ev){
            ev.preventDefault();
            var data = "[]";
            $('div.controls').each(function(){
                var value = $(this).find('input[name="carousel"]').val();
                if(value.trim().length>0){
                    data = value;
                }
            });
            $.ajax('/admin/appconfig/couponCarousel', {
                data: {values: data},
                type: 'POST',
                dataType: 'json',
                success: function(result){
                    showMessage("success", 'Coupon carousel updated successfully.');
                }
            });
        });

        $('a#add-con').on('click', function(ev){
            ev.preventDefault();
            $(this).parent().before('<div class="control-group"><div class="controls"> <input type="text" name="key"> ' +
                    '<input type="text" name="value"></div></div>');
        });
    });
    function showMessage(status, message){
        var html = message+'<a class="close" href="#">&times;</a>';
        if(status == 'success'){
            $('div.alert').removeClass('alert-error').addClass('alert-success').html(html).show();
        }else {
            $('div.alert').removeClass('alert-success').addClass('alert-error').html(html).show();
        }
    }
</script>
</body>
