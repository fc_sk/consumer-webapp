<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class= "row">
    <div class="col-md-3 col-sm-3">
        <jsp:include page="customerTrailSearch.jsp" />
    </div>
    <div class="col-md-9 col-sm-9">
        <%@page
        	import="com.freecharge.customercare.controller.CustomerTrailController"%>
        <%@page
        	import="com.freecharge.customercare.controller.CustomerTrailController"%>
        <%@include file="/WEB-INF/includes/taglibs.jsp"%>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <div class="customer-trail">
        	<c:choose>
        		<c:when test="${not empty paidCouponsTxnView}">
        			<div class="cs-display">
        		<!-- payment info -->
		        		<section class="unit">
		        			<div class="layer">
		        				<div>
		        					<c:choose>
		        						<c:when test="${not empty paidCouponsTxnView.localPGWalletTxns}">
		        							<h4>
		        								<span class="label label-info"><spring:message
		        										code="payment.attempts" text="#payment.attempts#" /></span>
		        							</h4>
		        							<!-- <table class="table table-hover"> -->
		        							<table class="table table-bordered table-hover">
		        								<thead>
		        									<tr>		        										
		        										<th><spring:message code="pg.transaction.id"
		        												text="#pg.transaction.id#" /></th>
		        										<th><spring:message code="merchant.txn.id"
		        												text="#merchant.txn.id#" /></th>
		        										<th><spring:message code="amount" text="#amont#" /></th>
		        										<th><spring:message code="status" text="#status#" /></th>
		        										<th><spring:message code="payment.gateway"
		        												text="#payment.gateway#" /></th>
		        										<th><spring:message code="sent.topg" text="#sent.topg#" />
		        										</th>
		        										<th><spring:message code="received.frompg"
		        												text="#received.frompg#" /></th>
		        										<th>PG Status</th>
		        									</tr>
		        								</thead>
		        								<tbody>
		        									
		        									<c:forEach var="localPGWalletTxn"
		        										items="${paidCouponsTxnView.localPGWalletTxns}">
		        										<tr>
		        											<td>${localPGWalletTxn['pgTxnId']}</td>
		        											<td>${localPGWalletTxn['merchantTxnId']}</td>
		        											<td>${localPGWalletTxn['amount']}</td>
		        											<td>${localPGWalletTxn['status']}</td>
		        											<td>${localPGWalletTxn['paymentGateway']}</td>
		        											<td><fmt:formatDate type="date" pattern="dd-MMM-yyyy"
		        													value="${localPGWalletTxn['sendToPg']}" /> <fmt:formatDate
		        													type="time" timeStyle="short"
		        													value="${localPGWalletTxn['sendToPg']}" />
		        											<td><fmt:formatDate type="date" pattern="dd-MMM-yyyy"
		        													value="${localPGWalletTxn['receivedFromPg']}" /> <fmt:formatDate
		        													type="time" timeStyle="short"
		        													value="${localPGWalletTxn['receivedFromPg']}" /></td>
		                                                    <td><!-- PG Status Check/Refund button -->
		                                                        <form name="pgStatusCheckForm" method="GET"
		                                                            action="/admin/refund/manual/doPayStatusCheckAndRefund.htm"
		                                                            style="float:left;margin-right:12px">
		                                                            <input type="hidden" name="orderIds" value="${localPGWalletTxn['merchantTxnId']}">
		                                                            <button type="submit" class="btn btn-mini btn-primary">PG Status</button>
		                                                        </form>
		                                                    </td>
		        										</tr>
		        									</c:forEach>
		        								</tbody>
		        							</table>
		        						</c:when>
		        						<c:otherwise>
		        						</c:otherwise>
		        					</c:choose>
		        				</div>
		        			</div>
		        		</section>

        		<!-- ------------------------------Coupons Info----------------------------------------------------->
        		<section class="unit">
        			<div class="layer">
        				<div>
        					<c:choose>
        						<c:when test="${not empty paidCouponsTxnView.localCouponsTxns}">
        							<hr>
        							<h4>
        								<span class="label label-info"><spring:message
        										code="coupons.information" text="#coupons.information#" /></span>
        							</h4>
        							<div class="table">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th><spring:message code="coupon.name"
                                                            text="#coupon.name#" /></th>
                                                    <th><spring:message code="coupon.value"
                                                            text="#coupon.value#" /></th>
                                                    <th><spring:message code="coupon.type" 
                                                    		text="#coupon.type#" /></th>
                                                    <th><spring:message code="expiry.date"
                                                            text="#expiry.date#" /></th>
                                                    <th><spring:message code="total.price"
                                                            text="#total.price#" /></th>
                                                    <th><spring:message code="terms.conditions"
                                                            text="#terms.conditions#" /></th>
                                                </tr>
                                            </thead>

                                            <tbody class="customer-trail-rows">
                                                 <c:forEach var="localCouponsTxn" items="${paidCouponsTxnView.localCouponsTxns}">
                                                     <tr>
                                                         <td>${localCouponsTxn['couponName']}</td>
                                                         <td>${localCouponsTxn['couponValue']}(${localCouponsTxn['quantity']})</td>
                                                         <td>${localCouponsTxn['couponType']}</td>
                                                         <td>${localCouponsTxn['validityDate']}</td>
                                                         <td>${localCouponsTxn['totalPrice']}</td>
                                                         <td style="white-space: pre-line" >${localCouponsTxn['termsConditions']}</td>
                                                         
                                                     </tr>
                                                 </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
        						</c:when>
        						<c:otherwise>
        						</c:otherwise>
        					</c:choose>
        				</div>
        			</div>
        		</section>
        	</div>
        	<section class="unit">
				<c:if test="${not empty paidCouponsTxnView.globalTxnMetadataMap}">
				<div class="layer">
					<h2>
        				<span class="label label-info"><spring:message
        					code="customer.information" text="#customer.information#" /></span>
        			</h2>
					<ul>
						<c:if test="${paidCouponsTxnView.globalTxnMetadataMap.get('displayName') != null}">
						<li><em><spring:message code="customer.name"
									text="#customer.name#" />:</em>${paidCouponsTxnView.globalTxnMetadataMap.get("displayName")}</li>
						</c:if>
						<c:if test="${paidCouponsTxnView.globalTxnMetadataMap.get('email') != null}">
						<li><em><spring:message code="customer.email"
									text="#customer.email#" />:</em>${paidCouponsTxnView.globalTxnMetadataMap.get("email")}</li>
						</c:if>
						<c:if test="${paidCouponsTxnView.globalTxnMetadataMap.get('mobileNumber') != null}">
						<li><em><spring:message code="mobile.no"
									text="#mobile.no#" />:</em>${paidCouponsTxnView.globalTxnMetadataMap.get('mobileNumber')}</li>
						</c:if>
						<c:if test="${paidCouponsTxnView.globalTxnMetadataMap.get('merchantOrderId') != null}">
						<li><em><spring:message code="merchant.orderId"
									text="#merchant.orderId#" />:</em>${paidCouponsTxnView.globalTxnMetadataMap.get("merchantOrderId")}</li>
						</c:if>		
						<c:if test="${not empty paidCouponsTxnView.createdDate}">
						<li><em><spring:message code="creation.date" text="#creation.date#" />:</em> <fmt:formatDate type="date"
        									pattern="dd-MMM-yyyy" value="${paidCouponsTxnView.createdDate}" /> <fmt:formatDate
        									type="time" timeStyle="short" value="${paidCouponsTxnView.createdDate}" /></li>
						</c:if>
					</ul>
				</div>
				</c:if>	        		
				</section>
    			</c:when>
   				<c:otherwise>
        					<!-- var1 is NOT empty or null. -->
    			</c:otherwise>
        	</c:choose>
        	<script type="text/javascript">

        $(document).ready(function(){

            var customerTrailRows = $('.customer-trail-rows').find('tr');
            customerTrailRows.eq(0).show();

            if(customerTrailRows.length < 2){
                $('#show-more').hide();
            }

            $('#show-more').on('click', function(event) {
                var value = $(this).text();
                value === 'Show more' ? value = 'Show less' : value = 'Show more';
                $(this).text(value);
                $('.customer-trail-rows').find('tr').toggle();
                $('.customer-trail-rows').find('tr').eq(0).show();
            })
        });
        </script>
    </div>
</div>
</div>