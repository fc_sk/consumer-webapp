<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
		<div class="alert alert-error">
			<h3> Failed to fetch Prediction History!</h3>
			<h5> Error Code: <c:out value="${ErrorCode}"/> </h5>
			<h5> Error Message: <c:out value="${ErrorMessage}"/> </h5>
		</div>
	</div>
</div>