<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<form id="reconForm" action="${s3PostURL}" method="post" enctype="multipart/form-data">
      <input type="hidden" name="key" value="${uploadDirectory}/\${filename}">
      <input type="hidden" name="AWSAccessKeyId" value="${accessKey}"> 
      <input type="hidden" name="acl" value="private"> 
      <input type="hidden" name="success_action_redirect" value="${uploadSuccessUrl}">
      <input type="hidden" name="policy" value="${uploadPolicy}">
      <input type="hidden" name="signature" value="${formSignature}"/>
      <input type="hidden" name="Content-Type" value="text/csv">
      <!-- Include any additional input fields here -->

      File to upload to S3: 
      <input name="file" type="file"> 
      <br> 
      <input type="submit" value="Initiate Reconciliation"> 
</form>