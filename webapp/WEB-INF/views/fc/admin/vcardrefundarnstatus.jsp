<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
	<div class="col-sm-3 col-md-3">
		<jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp"/>
	</div>
    <div class="col-sm-9 col-md-9">
        <h3>Virtual Card Refund for ARN: ${VCTransactons.acquirerRefNo} </h3>

		<c:choose>
		<c:when test="${VCTransactons.status eq 'SUCCESS'}">

				<%--  FundPost Refunds --%>  
				<h4>VCRecon FundPost Refunds </h4> 
				
				<c:choose>
		        <c:when test="${not empty VCTransactons.fundPostRefunds}" >
		        
				<table class="table table-hover table-bordered" style="font-size:11px" >
					<thead>
						<tr>
							<th>Transaction Ref No</th>
							<th>Card Id</th>
							<th>Retrieval Ref No</th>
							<th>Version</th>
							<th>Debit / Credit Flag</th>
							<th>Amount</th>
							<th>Settlement Status</th>
							<th>Settlement Ts</th>
							<th>Exception</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${VCTransactons.fundPostRefunds}" var="vcFundPostRefunds" >
							<tr>
								<td>${vcFundPostRefunds.transactionRefNo}</td>
								<td>${vcFundPostRefunds.cardHashId}</td>
								<td>${vcFundPostRefunds.retrievalReferenceNumber}</td>
								<td>${vcFundPostRefunds.version}</td>
								<td>
									<c:choose>
										<c:when test="${vcFundPostRefunds.debitCreditFlag eq 0}">DEBIT</c:when>
										<c:otherwise>CREDIT</c:otherwise>
									</c:choose>				
								</td>
								<td>${vcFundPostRefunds.adjustmentBillingAmount}</td>
								<td>
									<c:choose>
										<c:when test="${vcFundPostRefunds.settlementStatus eq 0}">FAILED</c:when>
										<c:otherwise>SUCCESS</c:otherwise>
									</c:choose>				
								</td>
								<td>${vcFundPostRefunds.settlementTs}</td>
								<td>${vcFundPostRefunds.exception}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
		
				</c:when>
				
				<c:otherwise>
					<p style="font-size:14px;" >No Data Found</p>
				</c:otherwise>
				</c:choose>
		

				<br><br>
				
				<%--  Settlement CreditVouchers --%>  
				<h4>VCRecon Settlement CreditVouchers </h4> 
				
				<c:choose>
        <c:when test="${not empty VCTransactons.settlementsCreditVouchers}" >
        <c:set var="vcSettlements" value="${VCTransactons.settlementsCreditVouchers}" />
		<table class="table table-hover table-bordered" style="font-size:11px" >
			<thead>
				<tr>
					<th>Record Indicator</th>
					<th>Trace Number</th>
					<th>Card Id</th>
					<th>Retreival Ref No</th>
					<th>Amount</th>
					<th>Txn Type</th>
					<th>Terminal Id</th>
					<th>Merchant Name</th>
					<th>Merchant Category Code</th>
					<th>Merchant Id</th>
					<th>
						<table class="table" style ="margin:0" >
							<tr><td style="font-size:13px; border:0; border-bottom:1px solid #ddd;"  >Settlement Recon</td></tr>
							<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Status</td></tr>
							<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception</td></tr>
							<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception Summary</td></tr>
							<tr><td style="margin: 0px; padding: 2px; border:0;" >Updated On</td></tr>
						</table>
					</th>				
					<th>Transaction Initiated On</th>
					<th>Created On</th>
					<th>Last Updated On</th>
				</tr>
			</thead>
			<tbody>
					<tr>
						<td>${vcSettlements.recordIndicator}</td>
						<td>${vcSettlements.traceNumber}</td>
						<td>${vcSettlements.cardHashId}</td>
						<td>${vcSettlements.retreivalRefNo}</td>
						<td>${vcSettlements.billingAmount}</td>
						<td>${vcSettlements.txnType}</td>
						<td>${vcSettlements.terminalId}</td>
						<td>${vcSettlements.merchantName}</td>
						<td>${vcSettlements.merchantCategoryCode}</td>
						<td>${vcSettlements.merchantId}</td>
						<td>
							<table class="table" style ="margin:0" >
								<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcSettlements.settlementReconStatusStr}</td></tr>
								<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcSettlements.settlementExceptionStr}<br></td></tr>
								<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcSettlements.settlementExceptionSummary}<br></td></tr>
								<tr>
									<td style="margin: 0px; padding: 2px; border:none;" ><c:if test="${not empty vcSettlements.settlementReconStatusStr}" >${vcSettlements.settlementReconTs}</c:if></td></tr>
							</table>	
						</td>
						<td>${vcSettlements.localTransactionDate}</td>
						<td>${vcSettlements.createdTs}</td>
						<td>${vcSettlements.updatedTs}</td>
					</tr>
			</tbody>
		</table>

		</c:when>
		
		<c:otherwise>
			<p style="font-size:14px;" >No Data Found</p>
		</c:otherwise>
		</c:choose>

		</c:when>
		
		<c:otherwise>
			<p style="font-size:14px;" >ErrorCode : ${VCTransactons.errorCode}</p>
			<p style="font-size:14px;" >ErrorMessage : ${VCTransactons.exception}</p>
		</c:otherwise>			
		</c:choose>
		
    </div>
</div>