<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<c:if test="${not empty messages }">
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		${messages}
	</div>
</c:if>
<div>
	<form:form method="post"
		action="/admin/fraud/reportbypg/handler/process.htm"
		enctype="multipart/form-data">

		<center>
			<h3>Handler for fraud reported by the PG</h3>
		</center>
		<h5>Instructions :</h5>
		<ul>
			<li>Use either TextArea or File Upload at a time to upload
				OrderId/MerhcntTxnId/PaymentTxnId list.</li>
			<li>Each line contains single request
				OrderId/MerhcntTxnId/PaymentTxnId in TextArea or Uploaded file.</li>
		</ul>
		<br>
		<h6>
			<i>Enter list of OrderId/MerchanttxnId/PaymentTxnId in TextArea
				OR upload a CSV File of OrderId/RequestId</i>
		</h6>

		<table class="table table-striped table-bordered"
			style="max-width: 69%">
			<tr>
				<td><textarea id="inputIds" name="inputIds">${inputIds}</textarea>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="label label-warning">OR</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="file" class="input" id="fileInputIds"
					name="fileInputIds"></td>
			</tr>
		</table>
		<br>
		<input type="submit" value="Handle Fraud" />
	</form:form>
</div>