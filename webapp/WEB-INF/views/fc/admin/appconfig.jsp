<%@ include file="/WEB-INF/includes/taglibs.jsp"%>

<section class="main">
  <h2>App Config Toggle</h2>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Configuration Attribute</th>
        <th>Current Value</th>
        <th>Configure</th>
      </tr>
    </thead>
    <tbody>
      <c:forEach items="${view}" var="view">
        <tr>
          <td>${view.display}</td>
          <td class="indicator">
          	${view.value}
          </td>
          <td>
          	<a href="#" onClick='toggle("${view.toggleUrl}")' class="btn btn-primary">Toggle</a>
          </td>
        </tr>    
      </c:forEach>
      <c:forEach items="${editableView}" var="editableView">
        <tr>
          <td>${editableView.display}</td>
          <td class="indicator">
            <textarea name="${editableView.display}">${editableView.value}</textarea>
          </td>
          <td>
            <a href="#" onClick='update("${editableView.toggleUrl}", "${editableView.display}")' class="btn btn-success">Update</a>
          </td>
        </tr>    
      </c:forEach>
        <tr>
            <td colspan="3">
                <a href="/admin/appconfig/custom-props" class="btn-link">Manage Custom App Config Properties</a>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                FB Fraud Conditions: ${fbFraudConditions}
                <a href="/admin/appconfig/fraud/fbconditions.htm">Change</a>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                IP Check Conditions: ${ipCheckConditions}
                <a href="/admin/appconfig/ipcheckconditions">Change</a>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Coupon Fraud Values: ${couponFraudValues}
                <a href="/admin/appconfig/couponFraudValues">Change</a>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Metro report whitelisted EmailId: ${metroReportEmailIds}
                <a href="/admin/appconfig/metroReportEmailIds">Change</a>
            </td>
        </tr>
        <tr>
            <td colspan="3">
               Force Upgrade Details: ${forceUpgradeDetails}
                <a href="/admin/appconfig/forceUpgradeDetails">Change</a>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                ATM Coupons: ${atmCoupons}
                <a href="/admin/appconfig/atmCoupons">Change</a>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Coupon carousel: ${couponCarousel}
                <a href="/admin/appconfig/couponCarousel">Change</a>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Split Traffic Weightage: ${weight}
                <a href="/admin/appconfig/splittrafficweightage">Change</a>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                In Memory App config Values: ${InMemoryAppConfig}
             </td>
        </tr>
    </tbody>
  </table>
</section>

<script type="text/javascript">
  function toggle(url) {
    $.post(url, {}, function(data) {
        location.reload(200);
    }, 'json');
  }

  function update(url, elementName) {
    var userData = $('textarea[name=' + elementName + ']').val();
    var attrData = {};
    attrData[elementName] = userData;
    $.post(url, attrData, function(data) {
        location.reload(200);
    }, 'json');
  }

  $(function() {
    // Stuff to do as soon as the DOM is ready;
    if ($(".indicator").text() == 'true') {$(this).html('<span class="label label-success">True</span>');console.log('wtttf');}    
    else { console.log('wtf'); }
  });
  
</script>
