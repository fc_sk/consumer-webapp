<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@ include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
		<div class="customer-trail">
			<div class="cs-display">

				<div class="row">
					<div class="col-md-4 col-sm-4"></div>
				</div>
				<hr>

				<section class="unit">
					<div>
						<h1>
							<span class="label label-info"><spring:message
									code="shopo.information" text="#shopo.information#" /></span>
						</h1>
					</div>

					<c:if test="${not empty shopoViewList}">
						<c:forEach var="shopoview" items="${shopoViewList}">
							<div class="layer">
								<h1>
									<span class="label label-info"><spring:message
											code="customer.information" text="#customer.information#" /></span>
								</h1>
								<ul>

									<li><em><spring:message code="partyName"
												text="#partyName#" />:</em> ${shopoview.name}</li>
									<li><em><spring:message code="partyEmail"
												text="#partyEmail#" />:</em> <a
										href="/admin/customertrail/onecheck/searchbyemail/result.htm?emailid=${shopoview.email}"><c:out
												value="${shopoview.email}" /></a></li>
									<li><em><spring:message code="partyContact"
												text="#partyContact#" />:</em> ${shopoView.contact}</li>
									<li><em><spring:message code="partyTxnId"
												text="#partyTxnId#" />:</em> ${shopoview.txnId}</li>
									<li><em><spring:message code="type" text="#type#" />:</em>
										${shopoview.type}</li>
									<li><em><spring:message code="amount" text="#amount#" />:</em>
										${shopoview.amount}</li>
									<li><em><spring:message code="partyTimeStamp"
												text="#partyTimeStamp#" />:</em> ${shopoview.timeStamp}</li>
									<li><em><spring:message code="platform"
												text="#platform#" />:</em> ${shopoview.platform}</li>
								</ul>
							</div>
							<div class="layer">
								<h1>
									<span class="label label-info"><spring:message
											code="transaction.information"
											text="#transaction.information#" /></span>
								</h1>

								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th><spring:message code="walletTxnId"
													text="#walletTxnId#" /></th>
											<th><spring:message code="state"
													text="#state#" /></th>
											<th><spring:message code="status" 
													text="#status#" /></th>
											<!--  <th><spring:message code="finalizedAmount"
													text="#finalizedAmount#" /></th>
											<th><spring:message code="cancelledAmount"
													text="#cancelledAmount#" /></th> 
											<th><spring:message code="holdingDuration"
													text="#holdingDuration#" /></th> -->
											<th><spring:message code="idempotencyId"
													text="#idempotencyId#" /></th>
											<!-- <th><spring:message code="orderName"
													text="#orderName#" /></th>  -->
											<th><spring:message code="orderId"
													text="#orderId#" /></th>
											<!-- <th><spring:message code="quantity"
													text="#quantity#" />
											<th><spring:message code="itemPrice"
													text="#itemPrice#" /></th>
											<th><spring:message code="seller"
													text="#seller#" /></th>  -->
											<th><spring:message code="otherPartyName"
													text="#otherPartyName#" /></th>
											<th><spring:message code="otherPartyEmail"
													text="#otherPartyEmail#" /></th>
											<th><spring:message code="otherPartyContact"
													text="#otherPartyContact#" /></th>
										</tr>
									</thead>
									<tbody class="customer-trail-rows">
										<tr>
											<td>${shopoview.walletTxnId}</td>
											<td>${shopoview.state}</td>
											<td><c:choose>
													<c:when test="${shopoview.status == 'SUCCESS'}">
														<span class="label label-success"><spring:message
																code="txn.successful" text="${shopoview.status}" /></span>
													</c:when>
													<c:otherwise>
														<span class="label label-important"><spring:message
																code="txn.unsuccessful" text="${shopoview.status}" /> </span>
													</c:otherwise>
												</c:choose></td>
										<!--  	<td>${shopoview.finalizedAmount}</td>
											<td>${shopoview.cancelledAmount}</td>
											<td>${shopoview.holdingDuration}</td> -->
											<td>${shopoview.idempotencyId}</td>
										<!--  <td>${shopoview.orderName}</td> -->
											<td>${shopoview.orderId}</td>
										<!-- <td>${shopoview.quantity}</td>
											<td>${shopoview.itemPrice}</td>
											<td>${shopoview.seller}</td>  -->
											<td>${shopoview.destName}</td>
											<td><a
												href="/admin/customertrail/onecheck/searchbyemail/result.htm?emailid=${shopoview.destEmail}"><c:out
														value="${shopoview.destEmail}" /></a></td>
											<td>${shopoview.destContact}</td>
										</tr>

									</tbody>
								</table>

							</div>
						</c:forEach>
					</c:if>

				</section>

				<!-- -------------------------------Campaign History---------------->
				<section class="unit">
					<div class="layer">
						<jsp:include page="rewardhistory.jsp" />
					</div>
				</section>
			</div>

			<script type="text/javascript">
		        $(document).ready(function(){
		
		            var customerTrailRows = $('.customer-trail-rows').find('tr');
		            customerTrailRows.eq(0).show();
				   
		        }
	        </script>
		</div>
	</div>
</div>