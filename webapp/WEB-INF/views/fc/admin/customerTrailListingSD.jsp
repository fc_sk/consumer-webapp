<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
		<%@page
			import="com.freecharge.customercare.controller.CustomerTrailController"%>
		<%@page
			import="com.freecharge.customercare.controller.CustomerTrailController"%>
		<%@include file="/WEB-INF/includes/taglibs.jsp"%>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<div class="customer-trail">
			<div class="cs-display">

				<div class="row">
					<div class="col-md-4 col-sm-4"></div>
				</div>
				<hr>

				<section class="unit">

					<c:if test="${not empty globalTransactionViewList}">
						<c:forEach var="globalTransactionView"
							items="${globalTransactionViewList}">

							<div class="layer">
								<h1>
									<span class="label label-info"><spring:message
											code="globalTxn.information" text="#globalTxn.information#" /></span>
								</h1>

								<ul>
									<li><em><spring:message code="status"
												text="#status#" />:</em> <c:choose>
											<c:when
												test="${globalTransactionView.globalTxnResponse.tsmState == 'SUCCESS'}">
												<span class="label label-success"><spring:message
														code="globalTxn.successful"
														text="${globalTransactionView.globalTxnResponse.tsmState}" /></span>
											</c:when>
											<c:otherwise>
												<span class="label label-important"><spring:message
														code="globalTxn.unsuccessful"
														text="${globalTransactionView.globalTxnResponse.tsmState}" />
												</span>
											</c:otherwise>
										</c:choose></li>
									<li><em><spring:message code="merchant.name"
													text="#merchant.name#" />:</em>
										
										<c:choose>
										
										<c:when test="${globalTransactionView.merchantName != 'N/A'}">
											${globalTransactionView.merchantName}	
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${not empty globalTransactionView.localTxnList}">
													<c:choose>
														<c:when test="${not empty globalTransactionView.localTxnList[0]}">
															<c:choose>
																<c:when test="${not empty (globalTransactionView.localTxnList[0])[0]}">
																	${(globalTransactionView.localTxnList[0])[0].bussinessEntity}
																</c:when>
																<c:otherwise>
																	${globalTransactionView.merchantName}
																</c:otherwise>
															</c:choose>
														</c:when>
														<c:otherwise>
															${globalTransactionView.merchantName}
														</c:otherwise>
													</c:choose>
												</c:when>
												<c:otherwise>
													${globalTransactionView.merchantName}
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
									
									</li>
									
									
									<li><em><spring:message code="merchant.sourcedSysType"
												text="#merchant.sourcedSysType#" />:</em>${globalTransactionView.globalTxnResponse.sourceSystem}</li>
									<c:if test="${globalTransactionView.globalTxnMetadataMap.get('merchantOrderId') != null}">
									<li><em><spring:message code="merchant.orderId"
												text="#merchant.orderId#" />:</em>${globalTransactionView.globalTxnMetadataMap.get("merchantOrderId")}</li>
									</c:if>
									<c:if test="${globalTransactionView.globalTxnMetadataMap.get('OrderId') != null}">
									<li><em><spring:message code="merchant.orderId"
												text="#merchant.orderId#" />:</em>${globalTransactionView.globalTxnMetadataMap.get("OrderId")}</li>
									</c:if>
									<c:if test="${globalTransactionView.globalTxnMetadataMap.get('displayName') != null}">
									<li><em><spring:message code="customer.name"
												text="#customer.name#" />:</em>${globalTransactionView.globalTxnMetadataMap.get("displayName")}</li>
									</c:if>
									<c:if test="${globalTransactionView.globalTxnMetadataMap.get('CustomerName') != null}">
									<li><em><spring:message code="customer.name"
												text="#customer.name#" />:</em>${globalTransactionView.globalTxnMetadataMap.get("CustomerName")}</li>
									</c:if>
									<c:if test="${globalTransactionView.globalTxnMetadataMap.get('email') != null}">
									<li><em><spring:message code="customer.email"
												text="#customer.email#" />:</em>${globalTransactionView.globalTxnMetadataMap.get("email")}</li>
									</c:if>
									
									
									<li><em><spring:message code="globalTxn.id"
												text="#globalTxn.id#" />:</em>${globalTransactionView.globalTxnResponse.globalTxnId}</li>
									<li><em><spring:message code="globalTxn.type"
												text="#globalTxn.type#" />:</em>${globalTransactionView.globalTxnResponse.globalTxnType
     								}</li>
									<li><em>Amount:</em>${globalTransactionView.globalTxnResponse.globalTxnAmount}</li>
									
								</ul>


								<div>
									<c:if test="${not empty globalTransactionView.localTxnList}">
									<h1>
										<span class="label label-info"><spring:message
												code="localTxn.information" text="#localTxn.information#" /></span>
									</h1>
									<table class="table table-bordered table-hover">
										<thead>
											<tr>
												<th><spring:message code="transaction.id"
														text="#transaction.id#" /></th>
												<th><spring:message code="transaction.type"
														text="#transaction.type#" /></th>
												<!-- <th><spring:message code="transaction.businessentity"
														text="#transaction.businessentity#" /></th> -->
												<th><spring:message code="transaction.businesstype"
														text="#transaction.businesstype#" /></th>
												<th><spring:message code="transaction.amount"
														text="#transaction.amount#" /></th>
												<th><spring:message code="transaction.runningbalance"
														text="#transaction.runningbalance#" /></th>
												<th><spring:message code="transaction.reference"
														text="#transaction.reference#" /></th>
												<th><spring:message code="transaction.idempotencyid"
														text="#transaction.idempotencyid#" /></th>
												<th><spring:message code="transaction.timestamp"
														text="#transaction.timestamp#" /></th>
											</tr>
										</thead>
										<tbody class="customer-trail-rows">
												<c:forEach var="localtransaction"
													items="${globalTransactionView.localTxnList}">
													<c:forEach var="transaction" items="${localtransaction}">
														<tr>
															<td>${transaction.transactionId}</td>
															<td>${transaction.transactionType}</td>
														<!-- <td>${transaction.bussinessEntity}</td>  -->	
															<td>${transaction.businessTransactionType}</td>
															<td>${transaction.transactionAmount}</td>
															<td>${transaction.runningBalance}</td>
															<td>${transaction.transactionReference}</td>
															<td>${transaction.idempotencyId}</td>
															<td>${transaction.timestamp}</td>
														</tr>
													</c:forEach>
												</c:forEach>
										</tbody>	
									</table>
									</c:if>
								</div>

								
								<div>
									<c:if test="${not empty globalTransactionView.localTxnListPG}">
									<h1>
										<span class="label label-info"><spring:message
												code="localTxnPG.information" text="#localTxnPG.information#" /></span>
									</h1>
									<table class="table table-bordered table-hover">
										<thead>
											<tr>
												<th><spring:message code="transaction.id"
														text="#transaction.id#" /></th>
												<th><spring:message code="transaction.amount"
														text="#transaction.amount#" /></th>
<%-- 												<th><spring:message code="transaction.partyId" --%>
<%-- 														text="#transaction.partyId#" /></th> --%>
												<th><spring:message code="transaction.partyType"
														text="#transaction.partyType#" /></th>
												<th><spring:message code="transaction.reference"
														text="#transaction.reference#" /></th>
												<th><spring:message code="transaction.state"
														text="#transaction.state#" /></th>
												<th><spring:message code="transaction.clientId"
														text="#transaction.clientId#" /></th>
												<th><spring:message code="transaction.tsmState"
														text="#transaction.tsmState#" /></th>
												<th><spring:message code="transaction.effectiveAmount"
														text="#transaction.effectiveAmount#" /></th>
<%-- 												<th><spring:message code="transaction.metadata" --%>
<%-- 														text="#transaction.metadata#" /></th> --%>			
											</tr>
										</thead>
										<tbody class="customer-trail-rows">
												<c:forEach var="localtransactionPG"
													items="${globalTransactionView.localTxnListPG}">
														<tr>
															<td>${localtransactionPG.localTxnId}</td>
															<td>${localtransactionPG.amount}</td>
															<%-- <td>${localtransactionPG.partyId}</td> --%>
															<td>${localtransactionPG.partyType}</td>
															<td>${localtransactionPG.txnReferenceNumber}</td>
															<td>${localtransactionPG.state}</td>
															<td>${localtransactionPG.clientId}</td>
															<td>${localtransactionPG.tsmState}</td>
															<td>${localtransactionPG.effectiveAmount}</td>
															<%-- <td>${localtransactionPG.metadata}</td> --%>
														</tr>
													
												</c:forEach>	
										</tbody>										
									</table>
									</c:if>
								</div>
							</div>
							
							<hr>

						</c:forEach>
					</c:if>	        		

				</section>
				
							
					<!-- -------------------------------Campaign History---------------->
        		<section class="unit">
	        		<div class="layer">
	   						<jsp:include page="rewardhistory.jsp" />
					</div>
        		</section> 
        	
			</div>



			<script type="text/javascript">
		        $(document).ready(function(){
		
		            var customerTrailRows = $('.customer-trail-rows').find('tr');
		            customerTrailRows.eq(0).show();
				   
		        }
	        </script>
		</div>
	</div>
</div>