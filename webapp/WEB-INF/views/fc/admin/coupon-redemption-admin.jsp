<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- .mainContainer Starts  -->
<div class="mainContainer">
	<div class="pageWidth clearfix form">
		<div class="create_merchant_login boxDesign">
			<h1>Create Merchant Login</h1>
			<form:form name="createloginform" action="#" method="POST"
				cssClass="form" id="createloginform">
				<div class="create_login_Form">
					<table style="width: 100%;">
						<tr>
							<td>
								<p>*UserName( xyz@freecharge.com)</p> <input maxlength="40" type="text"
								name="username" id="username"
								value="${CREATE_MERCHNAT_LOGIN.username}">
							</td>
							<td>
								<p>*Merchants</p> <select
								class="validate[required] select1 softNormText" name="merchant"
								id="merchant">
									<option value="" selected>Choose one</option>
									<c:forEach var="item" items="${ALL_MERCHANTS}">
										<option value="${item.id}-${item.merchantTitle}">${item.merchantTitle}</option>
									</c:forEach>
							</select>
							</td>
						</tr>
					</table>
					<p>
						<input type="button" class="regularButton" value="Create Login"
							onclick="CreateMerchantLogin()">
					</p>
					<p>
						<br />
						<span id="statusMsg_merchantLogin"  style="color: red;"></span>
					</p>
				</div>
			</form:form>
 		</div>
	</div>
		<div class="pageWidth clearfix form">
		<div class="block_coupon_codes boxDesign">
			<h1>Block coupon codes for test</h1>
			<form:form name="blockcouponsform" action="#" method="POST"
				cssClass="form" id="blockcouponsform">
				<div class="block_coupon_codes">
					 
								<p>*Campaigns</p> 
								<select class="validate[required] select1 softNormText" name="couponStoreId" id="couponStoreId" onchange="onCampaignChange()">
									<option value="" selected>Choose one</option>
									<c:forEach var="item" items="${ALL_CAMPAIGNS}">
										<option value="${item.couponStoreId}">${item.campaignName} </option>
									</c:forEach>
							</select>
							<p>*Is M coupon <input type="checkbox" name="IsMcoupon" id="IsMcoupon"/></p>
							 
								<p>*Number of coupons to be blocked</p> 
								<select id="coupon_count"></select>
					<p>
						<input type="button" class="regularButton" value="Block Coupons"
							onclick="BlockCoupons()"><sub> Please refresh the page to see updated mCoupon status</sub>
					</p>
				</div>
			</form:form>
			<p>
						<br />
						<span id="statusMsg_blockCoupons"  style="color: red;"></span>
						<span id="coupons"  style="color: red;"></span>
					</p>
 		</div>
	</div>

</div>
<!-- .mainContainer Ends  -->
<script type="text/javascript">
$(window).load(function(){
	$(function(){
	    var $select = $("#coupon_count");
	    for (i=1;i<=25;i++){
	        $select.append($('<option></option>').val(i).html(i))
	    }
	});
	});
	
	function onCampaignChange(){
		var status =  criteria[$.trim($('#couponStoreId').val())] 
		if(status === 'true'){
			$('#IsMcoupon').prop('checked',true);			
		}else{
			$('#IsMcoupon').prop('checked',false);
		}
	}
		
	function validateCouponFormFields() {
		var msg;
		 var couponStoreId = $.trim($('#couponStoreId').val());
		if (couponStoreId == '') {
			msg = 'Invalid campaign';
			$('#statusMsg_blockCoupons').html(msg);
			return false
		} 
		
		var numReg = new RegExp(/^\+?[1-9]\d*$/);
		if (!numReg.test($.trim($('#coupon_count').val()))) {
			msg = 'Invalid number.';
			$('#statusMsg_blockCoupons').html(msg);
			return false;
		}
		
		return true;
	}

	function BlockCoupons() {
		$('#statusMsg_blockCoupons').html("");
		var couponhtml = "";
		$('#coupons').html(couponhtml);

		if (validateCouponFormFields() == true) {
			
			 $.blockUI({ css: {
		        border: 'none',
		        padding: '15px',
		        backgroundColor: '#000',
		        '-webkit-border-radius': '10px',
		        '-moz-border-radius': '10px',
		        opacity: .5,
		        color: '#fff'
		    } }); 
			
			var couponStoreId = $.trim($('#couponStoreId').val());
			var couponCount = $.trim($('#coupon_count').val());
			var mcoupon = false;
			var toggleMcouponStatus = false;
			if ($('#IsMcoupon').is(":checked")){
				var mcoupon  = true
			}

			if(criteria[$.trim($('#couponStoreId').val())] !== mcoupon.toString()){
				toggleMcouponStatus = true;
			}
			
			var url = '/admin/coupon-redemption-admin/block';

			var formData = 'couponStoreId=' + couponStoreId + '&coupon_count=' + couponCount + '&ismcoupon=' +mcoupon +'&toggleMcouponStatus='+toggleMcouponStatus;

			$.ajax({
				url : url,
				type : "POST",
				data : formData,
				dataType : "html",
				cache : false,
				timeout : 1800000,
				success : function(responseText) {
					var responseObj = {};
					try {
						responseObj = jQuery.parseJSON(responseText);
					} catch (e) {

					}
					//document.blockcouponsform.reset();
					$('#statusMsg_blockCoupons').html(responseObj.STATUS);
					var couponJson = responseObj.coupons;
 					couponhtml = "<table border='1' bordercolor='FFCC00' style='background-color:FFFFCC' width='100%' cellpadding='3' cellspacing='3'>"
						+ "<tr><td>CouponCode Id</td><td>Coupon Code</td></tr>";
					$.each(couponJson, function(key, value) {
						couponhtml +="<tr><td>"+couponStoreId+"</td><td>"+value+
							"</td><tr>";
						});
					$('#coupons').html(couponhtml);
 					$.unblockUI();

				}
			});
		}

	}

	function validateMerchantFormFields() {
		var msg;
		new RegExp(/^([\w\.\-]+)@freecharge.com$/i);
		var emailReg = new RegExp(/^([\w\.\-]+)@([\w\.\-]+).([\w\.\-]+)$/i);
		if (!emailReg.test($.trim($('#username').val()))) {
			msg = 'Invalid email. Username format is xyz@abc.com';
			$('#statusMsg_merchantLogin').html(msg);
			return false;
		}
		var merchant = $.trim($('#merchant').val());
		if (merchant == '') {
			msg = 'Invalid merchant';
			$('#statusMsg_merchantLogin').html(msg);
			return false
		}
		return true;
	}

	function CreateMerchantLogin() {

		if (validateMerchantFormFields() == true) {
			
			$.blockUI({ css: {
		        border: 'none',
		        padding: '15px',
		        backgroundColor: '#000',
		        '-webkit-border-radius': '10px',
		        '-moz-border-radius': '10px',
		        opacity: .5,
		        color: '#fff'
		    } }); 
			var merchant = $.trim($('#merchant').val());
			var username = $.trim($('#username').val());
			var password = $.trim($('#password').val());

			var url = '/admin/coupon-redemption-admin/create-login';

			var formData = 'merchant=' + merchant + '&username=' + username
					+ '&password=' + password;
			$.ajax({
				url : url,
				type : "POST",
				data : formData,
				dataType : "html",
				cache : false,
				timeout : 1800000,
				success : function(responseText) {
					var responseObj = {};
					try {
						responseObj = jQuery.parseJSON(responseText);
					} catch (e) {

					}
					//document.createloginform.reset();
					$('#statusMsg_merchantLogin').html(responseObj.STATUS);
					 $.unblockUI();
				}
			});
		}

	}
</script>
<script>
		var criteria = [];
		<c:forEach var="item" items="${ALL_CAMPAIGNS}">
		    criteria["${item.couponStoreId}"] = "${item.isMCoupon}";
		</c:forEach>
</script>
		 
		 
		 