<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ include file="/WEB-INF/includes/taglibs.jsp"%>
<form method="POST" enctype="multipart/form-data">
  <div class="form-style">
  <div class="row">
      <label>Select a prefix-list CSV file to upload</label>
      <input type="file" name='uploadFile' />
    </div>
    <div class="row">
      <input type="checkbox" name="ignoreFirst" />
      <label>Ignore first row (if the file has header row, you should check this box)</label>
    </div>
    <div class="row">
      <input type="checkbox" name="effectChanges" />
      <label>Effect changes (unless checked, only performs dry-run)</label>
    </div>
  <div class="row">
    <input type="submit" value="Upload" onClick="$('#busy').show()" />
  </div>
  <div class="row">
    ${result}
  </div>
  <div class="row" id='busy' style="display: none">
    <img alt="Waiting for response" src="${mt:keyValue("imgprefix1")}/images/ajax-loading.gif">
  </div>
  </div>
</form>