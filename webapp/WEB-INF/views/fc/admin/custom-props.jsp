<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<body>
<div class="container-fluid">
    <div class="alert" style="display: none">

    </div>
    <div class="row-fluid">
        <div class="span12">
            <h2>Add/Edit Custom App Config Property.</h2>
            <p>Click on + to add more key/val pairs.</p>
            <form class="form-horizontal" id="con-form">
                <div class="control-group">
                    <label class="control-label">Property Key</label>
                    <div class="controls">
                        <input type="text" value="" name="key" id="key">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Property Value</label>
                    <div class="controls">
                        <div class="control-list" id="prop-values"></div>
                    </div>
                </div>
                <div class="form-actions">
                    <a href="#" id="add-con"><i class="icon-plus-sign"></i> </a>
                    <a href="#" id="save-props" class="btn btn-primary">Save</a>
                </div>
            </form>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <table class="table table-bordered table-striped">
                <tr>
                    <th></th>
                    <th>Key</th>
                    <th>Value</th>
                </tr>
                <c:forEach items="${props}" var="prop" varStatus="count">
                    <tr>
                        <td><a href="#" class="btn btn-link edit-prop" data-count="${count.index}" data-key="${prop.key}">edit</a></td>
                        <td>${prop.key}</td>
                        <td id="prop-val-${count.index}">${prop.value}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('a#save-props').on('click', function(ev){
            ev.preventDefault();
            var data = {key: $('#key').val()};
            var params = {};
            $('div.params').each(function(){
                var key = $(this).find('.vKey').val();
                var value = $(this).find('.vVal').val();
                if(key.trim().length>0 && value.trim().length>0){
                    params[key] = value;
                }
            });
            data['value'] = JSON.stringify(params);

            $.ajax('/admin/appconfig/custom-props.htm', {
                data: data,
                type: 'POST',
                dataType: 'json',
                success: function(result){
                    showMessage("success", result.message);
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                }
            });
        });

        $('a#add-con').on('click', function(ev){
            ev.preventDefault();
            $('#prop-values').append('<div class="params"><input type="text" class="input-large vKey"> = ' +
                    '<input type="text" class="input-xxlarge vVal"></div>');
        });

        $('a.edit-prop').on('click', function(ev){
            ev.preventDefault();
            $('#prop-values').html('');
            var $el = $(ev.currentTarget);
            $('#key').val($el.data('key'));
            var params = $.parseJSON($('#prop-val-'+$el.data('count')).text());
            for(var p in params){
                $('#prop-values').append('<div class="params"><input type="text" class="input-large vKey" value="'+p+'"> ' +
                        ' = <input type="text" class="input-xxlarge vVal" value="'+escapeHtml(params[p])+'"></div>');
            }
        });
    });
    function showMessage(status, message){
        var html = message+'<a class="close" href="#">&times;</a>';
        if(status == 'success'){
            $('div.alert').removeClass('alert-error').addClass('alert-success').html(html).show();
        }else {
            $('div.alert').removeClass('alert-success').addClass('alert-error').html(html).show();
        }
    }
    
    function escapeHtml(text) {
    	  var map = {
    	    '&': '&amp;',
    	    '<': '&lt;',
    	    '>': '&gt;',
    	    '"': '&quot;',
    	    "'": '&#039;'
    	  };

    	  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    	}
</script>
</body>