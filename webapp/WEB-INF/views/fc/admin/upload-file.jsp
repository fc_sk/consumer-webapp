<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<div class="span12">
    <c:choose>
        <c:when test="${not empty url}">
            <div class="hero-unit">
                <strong>URL of the File Uploaded</strong><br>
                    ${url}
                <br>
                <p>
                    <img src="${url}">
                </p>
            </div>
        </c:when>
        <c:otherwise>
            <form action="/admin/s3/upload" method="post" enctype="multipart/form-data" class="form form-horizontal">
                <h2>This uploads file to <span style="color: #ddd49f; font-style: italic">fccontent-s3</span> bucket in s3.</h2>
                <div class="control-group">
                    <label class="control-label">Choose File to upload to s3</label>
                    <div class="controls">
                        <input type="file" name="file">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">
                        File uploaded for.
                    </label>
                    <div class="controls">
                        <input type="radio" value="missions" name="fileFor" checked="checked"> Missions
                    </div>
                    <div class="controls" style="margin-top: 20px;">
                        <input type="text" class="input-xxlarge" name="filePath" id="filePath" value="/content/images/missions">
                        <div class="help-inline">This is the default path of the selected component above. You can change it if you want but is not advised.</div>
                    </div>
                </div>
                <div class="form-actions">
                    <input type="submit" value="Upload" class="btn btn-inverse">
                </div>
            </form>
        </c:otherwise>
    </c:choose>
</div>
<script type="text/javascript">
    var FILE_PATHS = {
        missions: '/content/images/missions'
    };
    $(document).ready(function(){
        $('input[name="fileFor"]').on('click', function(ev){
            var fileFor = $('input[name="fileFor"]:checked').val();
            $('#filePath').val(FILE_PATHS[fileFor]);
        });
    });
</script>