<%@ include file="/WEB-INF/includes/taglibs.jsp"%>

<div class="aggregator-setting">
    <form class="form-horizontal">
      <fieldset>
        <legend><spring:message code="postpaid.aggregator.priority.settings" text="#postpaid.aggregator.priority.settings#"/></legend>

        <div class="control-group">
          <label class="control-label"><spring:message code="operator" text="#operator#"/></label>
          <div class="controls">
            <select id="operators">
              <c:forEach items="${operators}" var="op">
                <option value="${op.operatorMasterId}">${op.operatorCode}</option>
              </c:forEach>
            </select>
          </div>          
      </div>

      <div class="control-group">
        <div class="controls aggregators">
          <ul id="aggregators">
          </ul>
          <p><spring:message code="drag.reposition" text="#drag.reposition#"/>.</p>
        </div>          
      </div>

      </fieldset>
    </form>

</div>

<script type="text/javascript">
  function aggregatorsUrl() {
	  var op = $('#allOperators').is(':checked') ? -1: $("#operators").val();
	  return "/admin/postpaidAggregatorPriority/" + op + "/aggregators.htm";
  }
  function initShuffle() {
    $("#aggregators").sortable(
        { opacity: 0.6,
          cursor: 'move',
          update: function() {
          var order = $(this).sortable("serialize") + '&action=updateOrder';
          $.post(aggregatorsUrl(), order, function(theResponse) {
            // ignore result
          });
          return true;
          }
        }
    );
  }
  function loadAggregators() {
    var url = aggregatorsUrl();
    $.getJSON(url, function(data) {
      var adiv = $("#aggregators");
      var aggregators = data["data"];
      adiv.empty();
      for (var key in aggregators) {
        var val = aggregators[key];
        var child = "<li id='ag_" + val + "'><span>" + val + "</span></li>";
        adiv.append(child);
      }
    });
  }
  function init() {
    $("#operators").change(loadAggregators);
    $("#allOperators").change(loadAggregators);
    initShuffle();
    loadAggregators();
  }
  $(document).ready(init);
</script>