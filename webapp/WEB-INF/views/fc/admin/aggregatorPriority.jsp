<%@ include file="/WEB-INF/includes/taglibs.jsp"%>

<div class="row">
<div class="aggregator-setting span-6">
    <form class="form-horizontal">
      <fieldset>
        <legend><spring:message code="aggregator.priority.settings" text="#aggregator.priority.settings#"/></legend>

        <div class="control-group">
          <label class="control-label"><spring:message code="operator" text="#operator#"/></label>
          <div class="controls">
            <select id="operators">
              <c:forEach items="${operators}" var="op">
                <option value="${op.operatorMasterId}">${op.operatorCode}</option>
              </c:forEach>
            </select>
             / <input id="allOperators" type="checkbox" name="allOperators">
            <span><spring:message code="select.all.operators" text="#select.all.operators#"/></span>  
          </div>          
      </div>
      <div class="control-group">
          <label class="control-label"><spring:message code="circle" text="#circle#"/></label>
          <div class="controls">
            <select id="circles">
            <c:forEach items="${circles}" var="ci">
              <option value="${ci.circleMasterId}">${ci.name}</option>
            </c:forEach>
          </select>
          / <input id="allCircles" type="checkbox" name="allCircles">
          <span><spring:message code="select.all.circles" text="#select.all.circles#"/></span>
          </div>          
      </div>

      <div class="control-group">
        <div class="controls aggregators">
          <ul id="aggregators">
          </ul>
          <p><spring:message code="drag.reposition" text="#drag.reposition#"/>.</p>
        </div>          
      </div>

      </fieldset>
    </form>

</div>
<div class="span-6">
	<!-- <div class="well">
		<a href="javascript:boid(0);" onclick="alertAction('/move/to/euronet', 'Are sure to move all allowed to euronet ?');">Move All Allowed To Euronet</a>
		<hr/>
		<a href="javascript:boid(0);" onclick="alertAction('/move/to/oxigen', 'Are sure to move all allowed to oxigen ?');">Move All Allowed To Oxigen</a>
		<hr/>
		<a href="javascript:boid(0);" onclick="alertAction('/move/to/default', 'Are sure to reset to default ?');">Reset To Default </a>
		<hr/>
	</div> -->
</div>
</div>
<div class="row">
	<div class="span3">
		<div id='alertmessage' class="alert alert-warning"></div>
		<h3>Template List</h3>
		<a href="/admin/aggregatorPriority.htm">Home</a><br/>
		<c:forEach items="${templates}" var="template">
			<div id="template${template}">
			<a href="/admin/aggregatorPriority.htm?template=${template}">${template}</a> 
			[<a href="javascript:boid(0);" onclick="alertAction('/admin/aggregatorPriority/activatetemplate.htm?template=${template}', 'Activate template ${template} ?');">Activate</a>]
			[<a href="javascript:boid(0);" onclick="$('#template${template}').hide();alertAction('/admin/aggregatorPriority/deletetemplate.htm?template=${template}', 'Delete template ${template} ?');">Delete</a>]
			</div>
		</c:forEach>
	</div>
	<div class="span9">
		<h3>Create Template</h3>
		<form name="templatesaveform" action="/admin/aggregatorPriority/createtemplate" method="POST" >
			<table style="width: 100%">
				<tr> 
					<td>
						<b>Template Name</b> <br>(No spaces, No special chars)
					</td> 
					<td>
						<b>Priority Order</b>
					</td> 
					<td></td>
				</tr>
				<tr> 
					<td><input type="text" name="template_name" value="${param.template}" /> </td> 
					<td>
						<div class="aggregator-setting">
							<div class="controls aggregators">
								<ul id="aggprioritysorter" style="margin-top: 0px;">
									<li id="euronet" style="width:70px; float: left; margin-left: 5px;"><span>euronet</span></li>
									<li id="oxigen" style="width:70px; float: left;  margin-left: 5px;"><span>oxigen</span></li>
									<li id="cyberplat" style="width:70px; float: left;  margin-left: 5px;"><span>cyberplat</span></li>
								</ul>
							</div>
						</div>
					</td> 
					<td>
						<input type="button"  onclick="validateAndSubmit();" class="btn btn-primary" value="Save As Template" /> 
					</td>
				</tr>
			</table>
			<div>
				<table style="width:100%" id="aggrPriorityTable">
					<thead style="background-color: #ddd;">
						<tr>
							<th filter-type='ddl'>Operator</th>
							<th filter-type='ddl'>Circle</th>
							<th filter="false">Priorities</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${circleOperatorMappings}" var="comEntry">
							<tr>
								<td>${operatorsMap[comEntry.fkOperatorMasterId]}</td>
								<td>${circlesMap[comEntry.fkCircleMasterId]}</td>
								<td>
									<input type="hidden" name="chk" value="${comEntry.circleOperatorMappingId}" />
									<input type="text" readonly="readonly" name="prts${comEntry.circleOperatorMappingId}" value="${comEntry.aggrPriority}" />
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/tablefilter/table.filter.min.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">

$(document).ready(function() {
    $('#aggrPriorityTable').tableFilter({"enableCookies" : false});
});

$(function() {
	$( "#aggprioritysorter" ).sortable(
	        {   opacity: 0.6,
	            cursor: 'move',
	            update: function() {
		            var order = $(this).sortable("toArray");
		            var table = document.getElementById("aggrPriorityTable").tBodies[0];
		            $(table).children().each(function() {
		            	var tr = this;
		            	if($(tr).attr('filtermatch') == "false") {
		            		// do nothing
		            	} else {
		            		$(tr).children().each(function() {
			            		var td = this;
			            		$(td).children().each(function() {
			            			if(this.nodeName == 'INPUT' && $(this).attr("type") == 'text') {
			            				$(this).val(order);
			            			}
			            		});
			            	});
		            	}
		            });
		            return true;
	            }
	       }
	 );
	$( "#aggprioritysorter" ).disableSelection();
});

function validateAndSubmit() {
	if($("input[name=template_name]").val() == "") {
		alert("Please enter template name");
		return false;
	}
	document.templatesaveform.submit();
	return true;
}
  
  function alertAction(action, message) {
	  var r = confirm(message);
	  if (r == true) {
		  $("#alertmessage").html("Updating... Please wait");
	      $.getJSON(action, function(data){
	    	  $("#alertmessage").html("Priorities updated : "+data.updates );
	      });
	  } 
  } 	
  function aggregatorsUrl() {
      var op = $('#allOperators').is(':checked') ? -1: $("#operators").val();
      var ci = $('#allCircles').is(':checked') ? -1: $("#circles").val();
      return "/admin/aggregatorPriority/" + op + "/" + ci + "/aggregators.htm";
  }
  function initShuffle() {
    $("#aggregators").sortable(
        { opacity: 0.6,
          cursor: 'move',
          update: function() {
          var order = $(this).sortable("serialize") + '&action=updateOrder';
          $.post(aggregatorsUrl(), order, function(theResponse) {
            // ignore result
          });
          return true;
          }
        }
    );
  }
  function loadAggregators() {
    var url = aggregatorsUrl();
    $.getJSON(url, function(data) {
      var adiv = $("#aggregators");
      var aggregators = data["data"];
      adiv.empty();
      for (var key in aggregators) {
        var val = aggregators[key];
        var child = "<li id='ag_" + val + "'><span>" + val + "</span></li>";
        adiv.append(child);
      }
    });
  }
  function init() {
    $("#operators").change(loadAggregators);
    $("#circles").change(loadAggregators);
    $("#allOperators").change(loadAggregators);
    $("#allCircles").change(loadAggregators);
    initShuffle();
    loadAggregators();
  }
  $(document).ready(init);
</script>