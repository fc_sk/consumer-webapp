<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<table border="1">
    <thead>
        <tr>
            <th>Integration Gateway Transaction Id</th>
            <th>Payment Gateway Transaction Id</th>
            <th>Order Id</th>
            <th>Payment Gateway Success</th>
            <th>Integration Gateway Success</th>
            <th>Amount</th>
            <th>Refund</th>
        </tr>
    </thead>
<c:forEach items="${refundRows.source}" var="row">
    <tr>
        <td>${row.paymentTransactionId}</td>
        <td>${row.paymentGatewayTransactionId}</td>
        <td>${row.orderId}</td>
        <td>${row.paymentGatewaySuccess}</td>
        <td>${row.integrationGatewaySuccess}</td>
        <td>${row.amount}</td>
        <td><a href="/admin/refund/doRefund.htm?orderId=${row.orderId}&amount=${row.amount}">Refund</a></td>
</c:forEach>
</table>
