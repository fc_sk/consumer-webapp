<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp" />
	</div>

	<div class="col-md-9 col-sm-9">
		<c:if test="${not empty msg}">
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				${msg}
			</div>
		</c:if>

		<div>
			<c:choose>
				<c:when test="${not empty voucherMapList}">
					<h5>
						<b>User voucher details :</b>
					</h5>
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>VoucherId</th>
								<th>Business Entity</th>
								<th>Voucher amount</th>
								<th>Voucher status</th>
								<th>Created on</th>
								<th>Voucher Balance</th>
								<th>Expiry date</th>
								<th>Order Ids</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="voucherMap" items="${voucherMapList}">
								<tr>
									<td>${voucherMap['voucherid']}</td>
									<td>${voucherMap['businessentity']}</td>
									<td>${voucherMap['voucheramount']}</td>
									<td>${voucherMap['status']}</td>
									<td>${voucherMap['createdon']}</td>
									<td>${voucherMap['voucherbalance']}</td>
									<td>${voucherMap['expirydate']}</td>
									<td>
									 <c:forEach var="orderid" items="${voucherMap['orderids']}">
											<a href="/admin/customertrail/getcustomertrail?orderId=${orderid}">${orderid}</a>,
									 </c:forEach>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
		</div>
		<div>
		 <HR>
		 <p>
			 <b><i>Get Expired Vouchers : </i></b>
			 <input type="submit" id="GetExpiredVoucher" value="GET"/>
		 </p>
		 <div style="width:100%;overflow:auto;display: none;" id="expiredVoucher-wrapper">
				<table id="expiredVoucher" class="table table-bordered table-hover" style="font-size:12px;">
					<thead>
						<tr>
							<th>VoucherId</th>
							<th>Business Entity</th>
							<th>Voucher amount</th>
							<th>Voucher status</th>
							<th>Created on</th>
							<th>Voucher Balance</th>
							<th>Expiry date</th>
							<th>Order Ids</th>
						</tr>
					</thead>
					<tbody style="word-break:break-word;">
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#GetExpiredVoucher').click(function (e) {
	var id = "${onecheckid}";
   $.get('/admin/cs/onecheck/voucher/transaction/expired/details?onecheckid=' + id, function(response) {
   	var expiredvoucherlist = "";
   	for(var i=0; i<response.length; i++){
   		expiredvoucherlist += "<tr><td>"+response[i].voucherid+"</td><td>"+response[i].businessentity+"</td><td>"+
   		response[i].voucheramount+"</td><td>"+response[i].status+"</td><td>"+response[i].createdon+"</td><td>"+
   		response[i].voucherbalance+"</td><td>"+response[i].expirydate+"</td><td>"+response[i].orderids+"</td></tr>";
   	}
   	$("table#expiredVoucher TBODY").html(expiredvoucherlist);
   	$("#expiredVoucher-wrapper").show();
    });
    e.preventDefault(); // prevent actual form submit and page reload
});
</script>
