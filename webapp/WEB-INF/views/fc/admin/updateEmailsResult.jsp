<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:forEach var="successList" items="${successList}">
	<c:if test="${not empty successList}">
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<c:out value="${successList}" />
			<br>
		</div>
	</c:if>
</c:forEach>
<c:if test="${not empty successRowCountMessage}">
<div class="alert alert-info">
	<c:out value="${successRowCountMessage}" />
</div>
</c:if>

<c:forEach var="failedList" items="${failedList}">
	<c:if test="${not empty failedList}">
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<c:out value="${failedList}" />
			<br>
		</div>
	</c:if>
</c:forEach>
<c:if test="${not empty rowCountMessage}">
<div class="alert alert-info">
	<c:out value="${rowCountMessage}" />
</div>
</c:if>
