<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<c:if test="${not empty messages }">
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		${messages}
	</div>
</c:if>
<div>
	<form:form method="post"
		action="/admin/refund/manual/bulk/statuscheck/result.htm"
		enctype="multipart/form-data">

		<center>
			<h3>PG status check and fulfillment</h3>
		</center>
		<h5>Instructions :</h5>
		<ul>
			<li>Use either TextArea or File Upload at a time to upload
				OrderId list.</li>
			<li>Each line contains single orderId in TextArea or
				Uploaded file.</li>
		</ul>
		<br>

		<h6>
			<i> Enter list of OrderId in TextArea OR upload a CSV File (Each line contains single OrderId)</i>
		</h6>
		<table class="table table-striped table-bordered"
			style="max-width: 69%">
			<tr>
				<td><textarea id="inputIds" name="inputIds">${inputIds}</textarea>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="label label-warning">OR</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="file" class="input" id="fileInputIds"
					name="fileInputIds"></td>
			</tr>
		</table>
		<br>
		<input type="submit" value="Do PG status check" />
	</form:form>

	<div style="display: none;">
		<br>
		<h1>
			<span class="label label-success">Process success orderIds :</span>
		</h1>
		<br />
		<c:forEach var="successlistId"
			items="${successlist}">
			<c:out value="${successlistId}," />
		</c:forEach>
		<br>
		<h1>
			<span class="label label-important">Process failed orderIds:</span>
		</h1>
		<c:forEach var="enqueuedlistId"
			items="${enqueuedlist}">
			<c:out value="${enqueuedlistId}," />
		</c:forEach>
		<br>
	</div>
	<a href="javascript:void(0);" class="more-info">Show Status</a>
</div>



<script type="text/javascript">
    $(document).ready(function() {

        $(".more-info").on("click", function() {

            if ($(this).prev("div").is(':visible')) {
                $(this).prev("div").slideUp(200);
                $(this).text("Show Status");
            } else {
                $(this).prev("div").slideDown(200);
                $(this).text("Show less");
            }
        });
    });
</script>