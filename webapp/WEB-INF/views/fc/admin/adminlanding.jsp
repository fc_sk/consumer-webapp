<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<head>

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/lib/jquery.shuffle.min.js"></script>

<script>
		$(document).ready(function() {

			/* initialize shuffle plugin */
			var $grid = $('#grid');

			$grid.shuffle({
				itemSelector: '.item' // the selector for the items in the grid
			});

			/* reshuffle when user clicks a filter item */
			$('#filter a').click(function (e) {
				e.preventDefault();

				// set active class
                $('#charselect a').removeClass('active');
				$('#filter a').removeClass('active');
				$(this).addClass('active');

				// get group name from clicked item
				var groupName = $(this).attr('data-group');

				// reshuffle grid
				$grid.shuffle('shuffle', groupName );
			});

			/* select starting character when user clicks a charselect item */
			$('#charselect a').click(function (e)
			{
                e.preventDefault();

                // set active class
				$('#filter a').removeClass('active');
                $('#charselect a').removeClass('active');
                $(this).addClass('active');

                // get group name from clicked item
                var groupName = $(this).attr('char-group');

                // reshuffle grid
                $grid.shuffle('shuffle', groupName );
            });


		});
	</script>
	<style>
	html {overflow-y: scroll;}
		body {margin: 0; padding: 10px 0 20px; font-family: Arial, serif; font-size: 16px;}

		.float-left {float: left; position: relative; top: -10px; margin-right: 15px;}
		.float-right {float: right; position: relative; top: -10px; margin-right: 15px;}

		#container {width: 960px; margin: 0 auto;}

		/* filters */
		#filter {list-style-type: none; margin: 0 0 0 15px; padding:0px 0px 0px; float: left;}
		#filter li, #filter a {display: block; float: left; margin: 0}
		#filter a { text-decoration: none;}
		#filter a.active {background: yellow;}

		/* grid */
		#grid {clear: both; position: relative}

        /* char select*/
        #charselect {list-style-type: none; margin: 0 0 0px 15px; padding:0px 0px 0px; float: left;}
        #charselect li, #charselect a {display: block; float: left; margin: 0}
        #charselect a { text-decoration: none;}
        #charselect a.active {background: yellow;}


#charselect a{
  border: 1px solid #666;
  text-decoration: none;
  padding: 14px 20px;
  font-size: 16px;
  font-weight: normal;
  font-family: Times;
  background: #FFF;
  color: #000;
  border: 2px solid #fff;
}

#charselect a:hover, #charselect a.active, #charselect a.active:hover{
  background: darkblue;
  border: 1px solid #666;
  text-decoration: none;
  font-size: 16px;
  font-weight: normal;
  color: #fff;
  border: 2px solid #fff;
}

#filter a{
  border: 1px solid #666;
  text-decoration: none;
  padding: 14px 20px;
  font-size: 16px;
  font-weight: bold;
  background: #DAE;
  color: #fff;
  border: 2px solid #fff;
}

#filter a:hover, #filter a.active, #filter a.active:hover{
  background: darkmagenta;
  border: 1px solid #666;
  text-decoration: none;
  font-size: 16px;
  font-weight: bold;
  color: #fff;
  border: 2px solid #fff;
}
		
	</style>
</head>

<div class="container">

    <c:choose>
        <c:when test="${not loggedIn}">
            <a href="/admin/login" class="btn btn-info">Login via FreeCharge ID</a>
        </c:when>
        <c:otherwise>
           <div class="row">
               <ul id="charselect" class="charselectNav">
                <li><a href="#" char-group="ALL">
                         ALL</a></li>
                   <c:forEach var="firstLetter" items="${componentFirstLetters}">
                       <li>
                           <a href="#" char-group="${firstLetter}">
                          ${firstLetter}</a>
                       </li>

                   </c:forEach>
               </ul>
           </div>
           <div class="row">
           <ul id="filter" class="filterNav">
                 <li><a href="#" data-group="ALL">
                          ALL</a></li>
                    <c:forEach var="category" items="${categories}">
                        <li>
                            <a href="#" data-group="${category}">
                           ${category}</a>
                        </li>

                    </c:forEach>
                </ul>
            </div>
    
            <div class="row" >
                <ul class="nav nav-pills" id="grid">
              
                    <c:forEach var="component" items="${components}">    
                    	<c:set var="compKey" value="${component.key}" />	
                        <li class="active span4 item" style="margin-top: 10px;" data-groups='["ALL"${map[compKey]},"${fn:substring(component.key, 0, 1)}"]'>
                            <a href="${component.value}" class="btn btn-large btn-link"  style="background-color: #00a9ff">
                            <i class="icon-star"></i><span>${component.key}</span></a>
                        </li>
                        	
                    </c:forEach>
                </ul>
            </div>
        </c:otherwise>
    </c:choose>
</div>