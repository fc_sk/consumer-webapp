<%@page import="com.freecharge.admin.CustomerTrailDetails.EmailInputController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <div class="col-sm-3 col-md-3">
        <jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp"/>
    </div>

    <div class="col-sm-9 col-md-9">
        <c:if test="${not empty exception }">
            <div class="alert alert-error" >
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                ${exception}
            </div>
        </c:if>
		<h3>Referral Details</h3>
		<c:forEach var="referralEntity" items="${referralEntityList}">
            <div>
            	<table class="table table-hover table-bordered">
		            <thead>
		                <tr>
		                    <th>Referrer Code</th>
		                    <th>Cashback Earned</th>
		                    <th>Associated Orders</th>
		                    <th>Status</th>
		                </tr>
		            </thead>
		            <tr>
		                <td>${referralEntity.referralCode}</td>
		                <td>${referralEntity.referrerCashbackEarned}</td>
		                <td>${referralEntity.referredOrders}</td>
		                <td>${referralEntity.status}</td>
		            </tr>
		        </table>
            </div>
		</c:forEach>
        
	</div>
</div>