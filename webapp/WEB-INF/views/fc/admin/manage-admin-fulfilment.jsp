<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<head>
    <link href="/content/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="${mt:keyValue("cssprefix1")}/chosen.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
</head>
<div class="container">
    <div class="span12">
        <h2>Fulfilment</h2>
        Should be used only if recharge is final (success or failure).
        Resends recharge email/sms, coupons email/sms, crosssell email/sms etc. 
        Generates coupon/cross sell code if not already generated. 
        Resends the same code if already generated but not sent.
        <br/><br/><br/>
        <div>
        	<h4>Single Order</h4>
        	<form action="/admin/fulfilment" method="POST">
	        	Order Id: <input type="text" name="orderId"/>
	        	<input type="submit" id="fulfilmentSubmit" value="submit"/>
        	</form> 
        </div>	
        <c:out value="${message}"/>
    </div>
    <div class="span12">
        <br/><br/><br/>
        <div>
        	<h4>Multiple Orders</h4>
        	<form action="/admin/fulfilmentmultiple" method="POST"  enctype="multipart/form-data">
	        	Order CSV: <input type="file" name="uploadFile"/>
	        	<br/><br/>
	        	Ignore First: <input type="checkbox" name="ignoreFirst"/>
	        	<br/><br/>
	        	<input type="submit" id="fulfilmentMultipleSubmit" value="submit"/>
        	</form> 
        </div>
        <c:if test="${multiplemessage != null}"	>
			Following orders were successfully enqueued,
			<c:out value="${multiplemessage}"/>        	
        </c:if>
    </div>
</div>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/chosen.jquery.js?v=${mt:keyValue("version.no")}"></script>
