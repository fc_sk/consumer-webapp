<%@page import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@page import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class= "row">
   <div class="col-md-3 col-sm-3">
      <jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp"/>
   </div>
   <div class="col-md-9 col-sm-9">
      <div class="col-md-12 col-sm-12">
         <div class="customer-trail">
            <div class="cs-display">
               <div class="row">
                  <div class="col-md-4 col-sm-4">
                     <section class="unit">
                        <h2>
                           <span class="label label-info">
                              <spring:message
                                 code="upi.transaction.details" text="#upi.transaction.details#" />
                           </span>
                        </h2>
                        <div class="layer">
                           <ul>
                              <li>
                                 <em>
                                    <spring:message code="name" text="#name#" />
                                    :
                                 </em>
                                 <c:out value="${upiTransactionDetails.userDetails.firstName}" />
                              </li>
                              <li>
                                 <em>
                                    <spring:message code="email" text="#email#" />
                                    :
                                 </em>
                                 <form name="submitEmailForm" method="POST"
                                    action="/admin/email_input_transaction/do.htm"
                                    style="display: inline;">
                                    <input type="hidden" name="email" value="${upiTransactionDetails.userDetails.emailId}">
                                    <a href="javascript:document.submitEmailForm.submit()">${upiTransactionDetails.userDetails.emailId}</a>
                                 </form>
                              </li>
                              <li>
                                 <em>
                                    <spring:message code="mobile.no"
                                       text="#mobile.no#" />
                                    :
                                 </em>
                                 <c:out value="${upiTransactionDetails.userDetails.mobileNumber}" />
                              </li>
                              <li>
                                 <em>
                                    <spring:message code="upi.transaction.date"
                                       text="#upi.transaction.date#" />
                                    :
                                 </em>
                                 <c:out value="${upiTransactionDetails.transactionDate}" />
                              </li>
                              <li>
                                 <em>
                                    <spring:message code="upi.source.vpa"
                                       text="#upi.source.vpa#" />
                                    :
                                 </em>
                                 <c:out value="${upiTransactionDetails.upiInfo.sourceVpa}" />
                              </li>
                           </ul>
                        </div>
                     </section>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-12 col-sm-12">
         <h5>
            <spring:message code="transaction.information.details" text="#transaction.information.details#"/>
         </h5>
         <table class="table table-hover table-bordered" id="customer-trail-result" >
            <thead>
               <tr>
                  <th>
                     <spring:message code="upi.fc.transactionid" text="#upi.fc.transactionid#"/>
                  </th>
                  <th>
                  	<spring:message code="upi.transaction.id" text="#upi.transaction.id#"/>
                  </th>
                  <th>
                     <spring:message code="upi.transaction.status" text="#upi.transaction.status#"/>
                  </th>
                  <th>
                     <spring:message code="upi.transaction.date" text="#upi.transaction.date#"/>
                  </th>
                  <th>
                     <spring:message code="upi.transaction.amount" text="#upi.transaction.amount#"/>
                  </th>
                  <th>
                     <spring:message code="upi.source.vpa" text="#upi.source.vpa#"/>
                  </th>
                  <th>
                     <spring:message code="upi.destination.vpa" text="#upi.destination.vpa#"/>
                  </th>
                  <th>
                     <spring:message code="upi.errmsg" text="#upi.errmsg#"/>
                  </th>
                  <th>
                     <spring:message code="upi.businessusecase" text="#upi.businessusecase#"/>
                  </th>
                  <th>
                  	<spring:message code="upi.errcode" text="#upi.errcode#"/>
                  </th>
                  <th>
                  	<spring:message code="upi.promocode" text="#upi.promocode#"/>
                  </th>
               </tr>
            </thead>
            <tbody>
                  <tr>
		             <td>
		                 <a href="/admin/customertrail/upi?transactionId=${upiTransactionDetails.globalTxnId}&transactionType=${upiTransactionDetails.globalTxnType}&mobileNo=${upiTransactionDetails.userDetails.mobileNumber}"> ${upiTransactionDetails.globalTxnId}</a>
		             </td>
		             <td>${upiTransactionDetails.merchantOrderId}</td>
                     <td>${upiTransactionDetails.txnStatus}</td>
                     <td>${upiTransactionDetails.transactionDate}</td>
                     <td>${upiTransactionDetails.txnAmount}</td>
                     <td>${upiTransactionDetails.upiInfo.sourceVpa}</td>
                     <td>${upiTransactionDetails.upiInfo.destVpa}</td>
                     <td>${upiTransactionDetails.upiNpciErrorMessage}</td>
                     <td>${upiTransactionDetails.businessUseCase}</td>
                     <td>${upiTransactionDetails.upiInfo.errCode}</td>
                     <td>${upiTransactionDetails.promoCode}</td>
                  </tr>
            </tbody>
         </table>
      </div>
   </div>
</div>