<%@page import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@page import="com.freecharge.customercare.controller.CustomerTrailController"%>
<div class="row">
	<div class="col-sm-3 col-md-3">
		<jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp" />
	</div>
	<%-- Fragment to display UPI transactions --%>
	<c:if test="${not empty upiTransactionsList }">
		<div class="col-sm-9 col-md-9">
			<jsp:include
				page="/WEB-INF/views/fc/admin/upi/upiTransactionsList.jsp" />
		</div>
	</c:if>
</div>