<%@ page import="com.freecharge.customercare.controller.CustomerTrailController" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<c:if test="${not empty errMessage }">
	<c:forEach var="message" items="${errMessage}">
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			${message}
		</div>
		<c:out value="${message}" />
	</c:forEach>
</c:if>
<h5><spring:message code="upi.transaction.label" text="#upi.transaction.label#"/></h5>
 <table class="table table-hover table-bordered" id="upi-trail-result" >
     <thead>
         <tr>
             <th><spring:message code="upi.fc.transactionid" text="#upi.fc.transactionid#"/></th>
             <th><spring:message code="upi.transaction.id" text="#upi.transaction.id#"/></th>
			 <th><spring:message code="upi.transaction.status" text="#upi.transaction.status#"/></th>
			 <th><spring:message code="upi.transaction.date" text="#upi.transaction.date#"/></th>
			 <th><spring:message code="upi.transaction.amount" text="#upi.transaction.amount#"/></th>
			 <th><spring:message code="upi.source.vpa" text="#upi.source.vpa#"/></th>
			 <th><spring:message code="upi.destination.vpa" text="#upi.destination.vpa#"/></th>
			 <th><spring:message code="upi.errmsg" text="#upi.errmsg#"/></th>
			 <th><spring:message code="upi.businessusecase" text="#upi.businessusecase#"/></th>
			 <th><spring:message code="upi.errcode" text="#upi.errcode#"/></th>
			 <th><spring:message code="upi.promocode" text="#upi.promocode#"/></th>

         </tr>
     </thead>
     <tbody>
     <c:forEach var="upiTransaction" items="${upiTransactionsList}">
         <tr>
             <td>
                 <a href="/admin/customertrail/upi?transactionId=${upiTransaction.globalTxnId}&transactionType=${upiTransaction.globalTxnType}&mobileNo=${upiTransaction.mobileNumber}"> ${upiTransaction.globalTxnId}</a>
              </td>
             <td>${upiTransaction.merchantOrderId}</td>
             <td>${upiTransaction.txnStatus}</td>
             <td>${upiTransaction.transactionDate}</td>
             <td>${upiTransaction.txnAmount}</td>
             <td>${upiTransaction.upiInfo.sourceVpa}</td>
             <td>${upiTransaction.upiInfo.destVpa}</td>
             <td>${upiTransaction.upiNpciErrorMessage}</td>
             <td>${upiTransaction.businessUseCase}</td>
             <td>${upiTransaction.upiInfo.errCode}</td>
             <td>${upiTransaction.promoCode}</td>
         </tr>
     </c:forEach>
     </tbody>
 </table>



