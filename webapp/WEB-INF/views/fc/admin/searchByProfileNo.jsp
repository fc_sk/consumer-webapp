<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
		<div>
			<h5>
				<i><b>Email-Migration status :</b></i>
			</h5>
			<c:choose>
				<c:when test="${not empty user}">
					<c:choose>
						<c:when test="${not empty user.userDetails}">
							<b><c:out value="${user.userDetails.emailId}" /> :</b>
							<c:out
								value="${emailIdMigrationStatusMap[user.userDetails.emailId]}" />
							<br>
						</c:when>
						<c:otherwise>
							<h5>
								<i><b>Empty User Details Found</b></i>
							</h5>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<h5>
						<i><b>User Not Found</b></i>
					</h5>
				</c:otherwise>
			</c:choose>
		</div>
		<div>
			<div class="container">
				<div class="row clearfix">
					<div class="col-md-12 column">
						<c:choose>
							<c:when test="${not empty profileNumberTransactionList}">
								<h5 class="alert alert-info">
									Profile number :
									<c:out value="${profileNoEntered}" />
								</h5>
								<table class="table table-bordered table-condensed">
									<thead>
										<tr>
											<th>UserId</th>
											<th>Email</th>
											<th>Order Id</th>
											<th>Transaction amount</th>
											<th>Recharge status</th>
											<th>Payment status</th>
											<th>AG</th>
											<th>PG</th>
											<th>Product name</th>
											<th>Service provider</th>
											<th>Promocode</th>
											<th>Created on</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="profileNumberTransaction"
											items="${profileNumberTransactionList}">
											<tr>
												<td>${profileNumberTransaction.userId}</td>
												<td>${profileNumberTransaction.emailId}</td>
												<td><a
													href="/admin/customertrail/getcustomertrail?orderId=${profileNumberTransaction.orderId}">${profileNumberTransaction.orderId}</a></td>
												<td>${profileNumberTransaction.rechargeAmount}</td>
												<td>${profileNumberTransaction.rechargeStatus}</td>
												<td>${profileNumberTransaction.paymentStatus}</td>
												<td>${profileNumberTransaction.aggregator}</td>
												<td>${profileNumberTransaction.paymentGateway}</td>
												<td>${profileNumberTransaction.productName}</td>
												<td>${profileNumberTransaction.serviceProvider}</td>
												<td><a
													href="/admin/customertrail/getorderidbycouponcode?promocode=${profileNumberTransaction.promocode}">${profileNumberTransaction.promocode}</a></td>
												<td>${profileNumberTransaction.createdOn}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:when>
							<c:otherwise>
								<h5 class="alert alert-error">No transaction data found for
									this profile number in last three months.</h5>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>