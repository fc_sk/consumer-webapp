<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
	<div class="col-sm-3 col-md-3">
		<jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp"/>
	</div>
    <div class="col-sm-9 col-md-9">
        <h3>Virtual Cards:</h3>
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>User Status</th>
                    <th>Issued On</th>
                    <th>Expiry</th>
                    <th>AG Code</th>
                    <th>AG User Id</th>
                    <th>Ag Card Id</th>
                    <th>Created On</th>
                    <th>Last Updated</th>
                    <c:choose>
                        <c:when test="${isAuthorize eq 'yes'}">
                            <th></th>
                        </c:when>
                    </c:choose>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${vcardDataList}" var="vcardData">
                <tr>
                    <td><a href="/admin/customertrail/onecheck/searchbyemail/result.htm?emailid=${vcardData.emailId}"><c:out value="${vcardData.emailId}" /></a></td>
                    <td><c:out value="${vcardData.userStatus}" /></td>
                    <td><c:out value="${vcardData.issuedOn}" /></td>
                    <td><c:out value="${vcardData.expiry}" /></td>
                    <td><c:out value="${vcardData.agCode}" /></td>
                    <td><c:out value="${vcardData.agUserId}" /></td>
                    <td><a href="/admin/customertrail/onecheck/vcardtxns.htm?vcardId=${vcardData.agCardId}"><c:out value="${vcardData.agCardId}" /></a></td>
                    <td><c:out value="${vcardData.createdOn}" /></td>
                    <td><c:out value="${vcardData.lastUpdated}" /></td>
                    <c:choose>
                        <c:when test="${isAuthorize eq 'yes'}">
                            <td>
                                <form method="POST" action="/admin/customertrail/onecheck/vcardstatus/update.htm">
                                        <input type="hidden" name="emailId" value="${vcardData.emailId}">
                                        <input type="hidden" name="statusUpdate" value="${vcardData.changeStatus}">
                                        <button class="btn trim-input" type="submit"><c:out value="${vcardData.changeStatus}" /></button>
                                </form>
                            </td>
                        </c:when>
                    </c:choose>
                </tr>
            </c:forEach>
           </tbody>
        </table>
        
    </div>
</div>