<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="matchStartTime" class="java.util.Date"/>
<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
	    <div>
	        <h2><b> User Prediction List: </b></h2>
	        <c:url value="/admin/customertrail/growthclient/bolthistorybyemail/view.htm" var="userEmailURL">
               <c:param name="emailid" value="${user.email}"/>
            </c:url>
            <h4> Email: <a href="${userEmailURL}"><c:out value="${user.email}"/></a> </h4>
            <h4> Mobile No: <c:out value="${user.mobileNo}"/> </h4>
            <c:if test="${not empty predictionmasterlist}">
                <table class="table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>Prediction ID:</th>
                            <th>User ID:</th>
                            <th>Match ID:</th>
                            <th>Innings Count</th>
                            <th>Deliveries Bowled</th>
                            <th>Predicted Score</th>
                            <th>Prediction Correct</th>
                            <th>Order ID</th>
                            <th>Reward Status</th>
                            <th>Reward Value</th>
                            <th>Reward Type</th>
                            <th>Created On</th>
                            <th>Updated On</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="predictionMaster" items="${predictionmasterlist}">
                            <tr>
                                <td>${predictionMaster.id}</td>
                                <td>${predictionMaster.userId}</td>
                                <td><a href="#matchdetails">${predictionMaster.matchId}</a></td>
                                <td>${predictionMaster.inningsCount}</td>
                                <td>${predictionMaster.deliveriesBowled}</td>
                                <td>${predictionMaster.predictedScore}</td>
                                <td>${predictionMaster.isCorrect}</td>
                                <td><a href="/admin/customertrail/growthclient/bolthistorybyorder/view.htm?orderid=${predictionMaster.orderId}">${predictionMaster.orderId}</a></td>
                                <td>${predictionMaster.rewardStatus}</td>
                                <td>${predictionMaster.rewardValue}</td>
                                <td>${predictionMaster.rewardType}</td>
                                <td>${predictionMaster.createdOn}</td>
                                <td>${predictionMaster.updatedOn}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

                <h2> Match List </h2>
                <div id="matchdetails">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>Match Id</th>
                                <th>First Team</th>
                                <th>Second Team</th>
                                <th>Team 1 Score</th>
                                <th>Team 1 Wickets</th>
                                <th>Team 1 Balls Faced</th>
                                <th>Team 2 Score</th>
                                <th>Team 2 Wickets</th>
                                <th>Team 2 Balls Faced</th>
                                <th>Match Draw</th>
                                <th>Start Time</th>
                                <th>Winning Team</th>
                                <th>Match Finished</th>
                                <th>Match Started</th>
                                <th>Ground Name</th>
                                <th>City</th>
                                <th>Match Key</th>
                                <th>Winning Text</th>
                                <th>Prediction Allowed</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="match" items="${cricketmatchmap}">
                                <tr>
                                    <td>${match.value.matchId}</td>
                                    <td>${match.value.firstTeamName}</td>
                                    <td>${match.value.secondTeamName}</td>
                                    <td>${match.value.firstTeamScore}</td>
                                    <td>${match.value.firstTeamWickets}</td>
                                    <td>${match.value.firstTeamBallsFaced}</td>
                                    <td>${match.value.secondTeamScore}</td>
                                    <td>${match.value.secondTeamWickets}</td>
                                    <td>${match.value.secondTeamBallsFaced}</td>
                                    <td>${match.value.isDraw}</td>
                                    <td>
                                        <jsp:setProperty name="matchStartTime" property="time" value="${match.value.startTime}"/>
                                         <c:out value="${matchStartTime}"/>
                                    </td>
                                    <td>${match.value.winningTeamName}</td>
                                    <td>${match.value.isFinished}</td>
                                    <td>${match.value.isStart}</td>
                                    <td>${match.value.groundName}</td>
                                    <td>${match.value.cityName}</td>
                                    <td>${match.value.matchKey}</td>
                                    <td>${match.value.winningText}</td>
                                    <td>${match.value.predictionAllowed}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:if>
        </div>
	</div>
</div>