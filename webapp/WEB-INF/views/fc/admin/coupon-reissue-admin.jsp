
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form:form name="couponReissueform" action="#" method="POST"
	cssClass="form" id="couponReissueform" style="margin-bottom:50px;">
	<div class="coupon_reissue_form">
		<table style="width: 100%;">
			<tr>
				<td><p>OrderId</p> <input maxlength="40" type="text"
					name="orderId" id="orderId" value="${orderId}"></td>
			</tr>
		</table>
		<p>
			<input type="button" class="regularButton" value="Get Coupons"
				onclick="getCouponsByOrder()">
		</p>
		<!-- <p>
			<input type="button" class="regularButton" value="Retrigger Coupons Email"
				onclick="retriggerCouponsEmail()"/>
		</p> -->
		<p>
			<br/> 
			<span id="couponsReissueStatus"></span><br/> 
			<span id="rechargeDetails"></span><br/> 
			<span id="userCouponsText" style="display: none;margin-top:20px;"><b>Coupon details for order</b></span><br/> 
			<span id="coupons"></span> 
			<span id="couponSelectText"  style="display: none;margin-top:20px;"><b></>Select the coupons for reissue</b></span><br/>   
			<span id="availableCoupons"></span>
			</p>
	</div>
	<table id="new_cpn_list" style="width: 100%;margin-top:10px;"></table>
	<p>
		<input id="reissuebtn" type="button" style="display: none;margin-top:10px;margin-bottom:50px;"
			class="regularButton" value="ReissueCoupons"
			onclick="reissueCoupons()">
	</p>
</form:form>



<script type="text/javascript">
	function getCouponsByOrder() {
		resetCouponHtml();
		var url = '/admin/coupon-reissue-admin/getCoupons';
		window.orderId = $.trim($('#orderId').val());
		if (orderId == null || orderId.length === 0) {
			alert("Please enter a valid orderId");
			return;
		}
		var formData = 'orderId=' + orderId;
		blockUI();
		$.ajax({
				url : url,
				type : "POST",
				data : formData,
				dataType : "html",
				cache : false,
				timeout : 1800000,
				success : function(responseText) {
					var responseObj = {};
					try {
							responseObj = jQuery.parseJSON(responseText);
							$('#userCouponsText').show();
							$('#couponSelectText').show();
							$('#rechargeDetails').text(responseObj.rechargeDetails).css('font-weight', 'bold').show();
							var couponJson = responseObj.coupons;
							couponhtml = "<table id='old_cpn_list' border='1' width='100%' cellpadding='5' cellspacing='5' align='center' >"
										+ "<tr bgcolor='#f1f0f0'><td>Coupon Id</td>"
										+ "<td>Coupon Type</td>"
										+ "<td>Coupon Name</td>"
										+ "<td>Quantity</td>"
										+ "<td>Code</td>"
										+ "<td>Total</td>"
										+ "<td>Optin Type</td>"
										+ "<td>Price</td>"
										+"<td>Date</td></tr>";
								var totalCouponAmount = 0;
								var count = 0;
								$.each(couponJson,function(key, value) {
									couponData ="<td>"+value.couponStoreId+"</td>"+
			   						"<td>"+value.couponType+"</td>"+
									"<td>"+value.campaignName+"</td>"+
									"<td>"+1+"</td>"+
									"<td>"+value.couponCode+"</td>"+
									"<td>"+value.couponValue+"</td>"+
									"<td>"+value.optinType+"</td>"+
									"<td>"+value.price+"</td>"+
									"<td>"+value.createdOn+"</td>"; 
									
									if(value.optinType === "reissue"){
										couponhtml += "<tr bgcolor='#A9A9A9'>"+couponData+"</tr>";
									}else{
										couponhtml += "<tr>"+couponData+"</tr>";
									}
									totalCouponAmount += value.couponValue;
								});
								couponhtml += "<tr bgcolor='#f1f0f0' cellpadding='5'><td colspan='5' align='right'><b>Grand Total : </b></td>"
										+ "<td colspan='2' align='left'>"
										+ totalCouponAmount + "</td></tr></table>";
								$('#coupons').html(couponhtml);

								var availablecouponListJson = responseObj.couponStockList;

								var availablecouponListHtml = "<select id='couponList'>";

								 $.each(availablecouponListJson,function(key, value) {
									var split = value.split('|');
									availablecouponListHtml += '<option value="'+ value +'">'+ split[0]+ ' - Rs. '+ split[1] +' - '+ split[2] + ' (price=' + split[4]+ ')' +'</option>';
								}); 
								availablecouponListHtml += "</select>"
								var couponList = "<table border='1' width='100%' cellpadding='5' cellspacing='5' align='center' margin='10px'>"
										+ "<tr><td><table width='100%' border='0' cellpadding='0' cellspacing='0'>"
										+ "<tr><td>Coupon: "
										+ availablecouponListHtml+ "</td></tr>"
										+ "<tr><td>Quantity: <input type='text' name='qty' id='qty'/></td></tr>"
										+ "<tr><td><input class='button' type='button' value='Add' id='add' onclick='addCoupons()'/></td></tr>"
										+ "</table></td></tr></table>";
								$('#availableCoupons').html(couponList);
					} catch (e) {
					}
					
					$.unblockUI();
					//end success 

					},
					error : function(jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});
	}// end getcoupons by order

	//add new coupon to the list of new coupons to be issued
	function addCoupons() {
		qty = $("#qty").val();
		if (qty == "") {
			alert("Please enter quantity!");
			$("#qty").focus();
			return;
		} else if (parseInt(qty) < 1) {
			alert("Please enter valid quantity!");
			$("#qty").focus();
			return;
		}
		var selectedCoupon = $("#couponList").val();
		var couponParams = selectedCoupon.split("|");
		campaignName = couponParams[0];
		price = couponParams[1];
		type = couponParams[2];
		id = couponParams[3];

		var oldQty = 0;
		if ($("#newCoupon_" + id).length > 0) {
			oldQty = $("#h_" + id).text().split('#')[0];
			qty = parseInt(qty) + parseInt(oldQty);
			$("#newCoupon_" + id).remove();
		}
		tot = qty * price;
		text = campaignName + ' - Rs. ' + price + ' x ' + qty + ' = ' + tot;

		div = "<tr id='newCoupon_"+id+"'>" + 
			"<td><label id='t_"+id+"'>"+text+"</label></td>" +
			"<td><label style='display:none;' id='h_"+id+"' >"+qty+"#"+price+"#"+id+"</label></td>" + 
			"<td colspan='1'><a class='button' href='javascript:void(0)' onclick='javascript:del("+id+")'>Remove</a></td>"+
			"</tr>";
		$("#new_cpn_list").append(div);
		$('#reissuebtn').show();

	}

	function del(x) {
		$("#newCoupon_" + x).remove();
		if ($('#new_cpn_list tr').length < 1)
			$('#reissuebtn').hide();
	}

	function reissueCoupons() {
		if ($('#new_cpn_list tr').length < 1) {
			alert("Please select coupons to be reissued");
			return;
		}
		var newCpnarray = new Array();

		$('#new_cpn_list tr').each(function() {
			var couponStoreid = $(this).attr('id').split('_')[1];
			var quantity = $("#h_" + couponStoreid).text().split('#')[0];
			var couponInfo = couponStoreid + "#" + quantity;
			newCpnarray.push(couponInfo);
		});
		
		var newCouponStr = newCpnarray.toString();
		
		var url = '/admin/coupon-reissue-admin/reissue';
		var formData = 'orderId=' + orderId + '&newCouponStr=' + newCouponStr;
		blockUI();
		$.ajax({
			url : url,
			type : "POST",
			data : formData,
			dataType : "html",
			cache : false,
			timeout : 1800000,
			success : function(responseText) {
				resetCouponHtml();
				$('#couponsReissueStatus').text("New coupons have been successfully resissued for orderId : " + orderId);
				$.unblockUI();
			},
			error : function(jqXHR, textStatus, errorThrown) {
				resetCouponHtml();
				$('#couponsReissueStatus').text("An error occured in coupon resisse for orderId : " + orderId);
				$.unblockUI();
				alert(errorThrown);
			}
		});

	}

	function retriggerCouponsEmail() {
		var orderId = $.trim($('#orderId').val());
		var url = '/admin/coupon-reissue-admin/retriggerEmail';
		var formData = 'orderId=' + orderId;
		blockUI();
		$.ajax({
			url : url,
			type : "POST",
			data : formData,
			dataType : "html",
			cache : false,
			timeout : 1800000,
			success : function(responseText) {
				resetCouponHtml();
				$('#couponsReissueStatus').text("Coupons email resent for orderId : " + orderId);
				$.unblockUI();
			},
			error : function(jqXHR, textStatus, errorThrown) {
				resetCouponHtml();
				$('#couponsReissueStatus').text("An error occured in resending coupons email for orderId : " + orderId);
				$.unblockUI();
				alert(errorThrown);
			}
		});		
	}

	function blockUI() {
		$.blockUI({
			css : {
				border : 'none',
				padding : '15px',
				backgroundColor : '#000',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#fff'
			}
		});
	}
	
	function resetCouponHtml(){
		$('#new_cpn_list tr').each(function() {
			$(this).remove();
		});
		$('#reissuebtn').hide();
		$('#coupons').empty();
		$('#availableCoupons').empty();
		$('#couponsReissueStatus').text('');
		$('#userCouponsText').hide();
		$('#couponSelectText').hide();
		$('#rechargeDetails').hide();
	}
</script>

