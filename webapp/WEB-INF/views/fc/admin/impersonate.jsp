<%@ include file="/WEB-INF/includes/taglibs.jsp"%>

<section class="main">
	<div class="alert">
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	  <strong>Message : </strong> <c:out value="${note}"/>
	</div>

	
	  <form method="POST">
	  	<div>
	      <label>Email ID of the user to impersonate:</label>
	      <div class="input-append">
		  <input type="text" name="email" class="span2">
		  <button class="btn" type="submit">Impersonate</button>
			</div>
	    </div>
	  </form>
	
</section>