<%@ page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<style>
section.main {
	margin: 0 0
}

h1 {
	color: blue
}

input.span2 {
	width: 170px;
	margin-bottom: 0px;
	height: 30px;
}

.customer-trail-input input[type="text"] {
	width: 170px;
	height: 30px;
	float: none;
}
</style>
<section class="main">
	<c:if test="${not empty messages}">
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<ul>
				<c:forEach var="message" items="${messages}">
					<li><c:out value="${message}" /></li>
				</c:forEach>
			</ul>
		</div>
	</c:if>

	<c:if test="${not empty exception }">
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<c:out value="${exception}" />
		</div>
	</c:if>

	<!--<div class="clearfix">
       <h3 class="pull-left" style="color:#cccccc; font-size:32px; margin:-10px 0 0 0;"> Customer Trail</h3>
    </div>-->
	<div>
		<form method="POST"
			action="/admin/customertrail/getTransactions/singleTextBox">
			<label class="control-label" for="searchInput"><b>Get
					Transactions :</b></label>
			<div class="controls">
				<input name="searchInput" type="text" class="span2"
					placeholder="Enter here" value="${searchInputEntered}">
				<button class="btn trim-input" type="submit">Get it</button>
			</div>
		</form>
	</div>

	<hr>
	<div class="customer-trail-input">
		<div class="clearfix">
			<!-- UPI Id Search, Get list of all transactions associated with UPI ID-->
			<div>
				<form method="POST" action="/admin/customertrail/upiId">
					<label for="upiId"><b>UPI Id</b></label>
					<div>
						<input type="text" name="upiId" value="${upiIdEntered}"
							class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>
			<div>
				<form method="POST" action="/admin/customertrail/getcustomertrail">
					<label for="<%=CustomerTrailController.ORDER_ID_PARAM_NAME%>"><b><spring:message
								code="order.id" text="#order.id#" /></b></label>
					<div>
						<input class="span2" name="orderId" type="text"
							value="${orderidentered}">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- Search by Bbps Reference Id -->
			<div>
				<form method="POST" action="/admin/customertrail/getcustomertrail">
					<label for="bbpsReferenceId"><b>Bbps Reference Id</b></label>
					<div>
						<input type="text" name="bbpsReferenceId"
							value="${bbpsReferenceId}" class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>
			
			<!-- Order id search by merchant id; get list of all merchants in the dropdown -->
			<div>
				<form method="POST" action="/admin/customertrail/paidcoupons">
					<label for="globalTxnId"><b>Paid Coupons Id</b></label>
					<div>
						<input type="text" name="globalTxnId"
							value="${globaltxnidentered}" class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<div>
				<form method="GET"
					action="/admin/customertrail/batcave/getallmerchants/view.htm">
					<label for="email"><b>Merchant Order Id</b></label>
					<div>
						<input type="text" name="orderid" value="" class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<div>
				<form method="POST" action="/admin/email_input_transaction/do.htm">
					<label for="email"><b>User Email ID</b></label>
					<div>
						<input class="span2" type="text" name="email"
							value="${emailentered}">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<div>
				<form method="POST" action="/admin/mobile_input_transaction/do.htm">
					<label for="mobile"><b>User Mobile Number</b></label>
					<div>
						<input type="text" name="mobile" value="${mobilenumberentered}"
							class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>
			<!-- wallet txn - onecheck also-->
			<div>
				<form method="POST"
					action="/admin/customertrail/onecheck/searchbyemail/view.htm">
					<label for="email"><b>Wallet Txn by Email</b></label>
					<div>
						<input type="text" name="emailid" value="" class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<div>
				<form method="POST"
					action="/admin/customertrail/onecheck/p2pandp2mbyemail/view.htm">
					<label for="email"><b>P2P/P2M History by Email</b></label>
					<div>
						<input type="text" name="emailid" value="" class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- User history by Merchant; get list of all merchants in the dropdown -->
			<div>
				<form method="GET"
					action="/admin/customertrail/batcave/getallmerchants/view.htm">
					<label for="email"><b>P2P/P2M User-Merchant History By
							Email</b></label>
					<div>
						<input type="text" name="emailid" value="" class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<div>
				<form method="POST"
					action="/admin/customertrail/onecheck/p2psplitbyemail">
					<label for="email"><b>P2P Split History by Email</b></label>
					<div>
						<input type="text" name="emailid" value="" class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<div>
				<form method="POST"
					action="/admin/customertrail/onecheck/shopobyemail">
					<label for="id"><b>Shopo History by Email/Mobile</b></label>
					<div>
						<input type="text" name="id" value="" class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<div>
				<form method="POST"
					action="/admin/customertrail/getorderidbycouponcode">
					<label for="<%=CustomerTrailController.PROMO_CODE_PARAM_NAME%>"><b>Promo/Freefund
							code</b></label>
					<div>
						<input type="text" name="promocode" value="${promocodeentered}"
							class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>
			<div>
				<form method="POST"
					action="/admin/cardhash_input_transaction/getcashbackdetails">
					<label for="cardhash"><b>CardHash</b></label>
					<div>
						<input type="text" name="cardhash" value="${cardHashEntered}"
							class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- Search by profile No -->
			<div>
				<form method="POST" action="/admin/customertrail/searchByProfileNo">
					<label for="cardhash"><b>Profile No</b></label>
					<div>
						<input type="text" name="profileNo" value="${profileNoEntered}"
							class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>
			<!-- End of search by profile no -->
			<!-- Voucher transaction -->
			<div>
				<form method="GET"
					action="/admin/cs/onecheck/voucher/transaction/details">
					<label for="cardhash"><b>Email for Voucher data</b></label>
					<div>
						<input type="text" name="email" value="${email}" class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- VCard details by email -->
			<div>
				<form method="POST"
					action="/admin/customertrail/onecheck/vcardbyemail/view.htm">
					<label for="email"><b>Virtual Card by Email</b></label>
					<div>
						<input type="text" name="emailid" value="">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- Add Cash Transaction -->
			<div>
				<form method="GET"
					action="/admin/customertrail/onecheck/OC/searchtxn">
					<label for="cardhash"><b>Add Cash Txn ID</b></label>
					<div>
						<input type="text" name="txnId" value="${txnId}">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- VCard Transaction Search -->
			<div>
				<form method="GET"
					action="/admin/customertrail/onecheck/vcardtxnstatus.htm">
					<label for="cardhash"><b>VCard Txn Ref ID</b></label>
					<div>
						<input type="text" name="vcardtxnId" value="${vcardtxnId}">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- VCard Refund Search -->
			<div>
				<form method="GET"
					action="/admin/customertrail/onecheck/vcardrefundarnstatus.htm">
					<label for="cardhash"><b>VCard Refund ARN</b></label>
					<div>
						<input type="text" name="vcardRefundARN" value="${vcardRefundARN}">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- Referrer Email Search -->
			<div>
				<form method="GET"
					action="/admin/customertrail/getreferraldetailsbyemail">
					<label for="cardhash"><b>Referrer Email</b></label>
					<div>
						<input type="text" name="referrerEmail" value="${referrerEmail}">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- Prediction by Email Search -->
			<div>
				<form method="POST"
					action="/admin/customertrail/growthclient/predictionbyemail/view.htm">
					<label for="email"><b>Prediction History by Email</b></label>
					<div>
						<input type="text" name="emailid" value="${predictionemailid}">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- Bolt History by Email Search -->
			<div>
				<form method="GET"
					action="/admin/customertrail/growthclient/bolthistorybyemail/view.htm">
					<label for="email"><b>Bolt History by Email</b></label>
					<div>
						<input type="text" name="emailid" value="${boltemailid}">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- Bolt History by Order Search -->
			<div>
				<form method="GET"
					action="/admin/customertrail/growthclient/bolthistorybyorder/view.htm">
					<label for="email"><b>Bolt History by Order</b></label>
					<div>
						<input type="text" name="orderid" value="${boltorderid}">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>
			<!-- Lifeline Email Search -->
			<div>
				<form method="GET"
					action="/admin/customertrail/getlifelinedetailsbyemail">
					<label for="cardhash"><b>Lifeline Email</b></label>
					<div>
						<input type="text" name="lifelineEmail" value="${lifelineEmail}">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- Campaign History by Email and Campaign Search -->
			<div>
				<form method="GET"
					action="/admin/customertrail/campaignhistory/campaignview.htm">
					<label for="email"><b>Campaign History by Email</b></label>
					<div class="dropdown dropdown-submit-input">
						<input type="text" name="emailid" value="${campaignemailid}" />
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>

			<!-- FPS User Transactional Details Search By Email Id -->
			<div>
				<form method="GET"
					action="/admin/customertrail/batcave/getusertransactiondetails">
					<label for="email"><b>FPS User Transactions by Email</b></label>
					<div class="dropdown dropdown-submit-input">
						<input type="text" name="emailid" value="${emailid}" />
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>
			
			<!-- Search by Google Reference Id -->
			<div>
				<form method="POST" action="/admin/customertrail/getcustomertrail">
					<label for="googleReferenceId"><b>Google Reference Id</b></label>
					<div>
						<input type="text" name="googleReferenceId"
							value="${googleReferenceId}" class="span2">
						<button class="btn trim-input" type="submit">GO</button>
					</div>
				</form>
			</div>
			
		</div>
		<hr>
	</div>
</section>
<script type="text/javascript">
	$(function() {
		$(".trim-input").on('click', function(e) {
			// prevent default action
			e.preventDefault();

			var $prev = $(this).prev(), input_value = $prev.val();

			// trim value of input box which is before the button
			$prev.val($.trim(input_value));

			// now submit the form
			$(this).closest('form').submit();
		});
	});
</script>