<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<head>
<style type="text/css">
.BreakWord {
	word-break: break-all;
}
p.padding {
    padding-left: 0cm;
}
</style>
</head>

<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
	<div class="customer-trail">
			<div class="cs-display">

				<div class="row">
					<div class="col-md-4 col-sm-4"></div>
				</div>
				<hr>

				<section class="unit">
							<div class="layer">
								<h1>
									<span class="label label-info"><spring:message
											code="fps.information" text="#fps.information#" /></span>
								</h1>
								<ul>
									<li><em><spring:message code="customer.email"
												text="#customer.email#" />:</em>${emailid}</li>
								</ul>
							</div>
							
							<c:if test="${not empty usertransactiondetails}">
							
							<div>
								<h1>
									<span class="label label-info"><spring:message
											code="transactions" text="#transactions#" /></span>
								</h1>
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th><spring:message code="general.details"
													text="#general.details#" /></th>
											<!-- <th><spring:message code="transaction.amount"
													text="#transaction.amount#" /></th>  -->
											<!-- <th><spring:message code="request.details"
													text="#request.details#" /></th> -->
											<th><spring:message code="device.details"
													text="#device.details#" /></th>
											<th><spring:message code="device.info"
													text="#device.info#" /></th>
										</tr>
									</thread>
									<tbody class="customer-trail-rows">
										<c:forEach var="usertransaction" items="${usertransactiondetails}">
											<tr>
												<td>
													<ul>
														<li><b><em ><spring:message code="transaction.transactionid"
																	text="#transaction.transactionid#" />:</em></b><p class="padding">${usertransaction.requestParam.transactionId}</p></li>
														<li><b><em><spring:message code="transaction.transactiontype"
																	text="#transaction.transactiontype#" />:</em></b><p class="padding">${usertransaction.requestParam.transactionType}</p></li>
														<li><b><em><spring:message code="transaction.orderid"
																	text="#transaction.orderid#" />:</em></b><p class="padding">${usertransaction.orderId}</p></li>
														<li><b><em><spring:message code="transaction.amount"
																	text="#transaction.amount#" />:</em></b><p class="padding">${usertransaction.requestParam.amount}</p></li>
														<!--  <li><em><spring:message code="transaction.idempotencyid"
																	text="#transaction.idempotencyid#" />:</em>${usertransaction.idempotencyId}</li> -->
														<li><b><em><spring:message code="transaction.scoreoftransaction"
																	text="#transaction.scoreoftransaction#" />:</em></b><p class="padding">${usertransaction.scoreOfTransaction}</p></li>
														<li><b><em><spring:message code="transaction.recommendationstatus"
																	text="#transaction.recommendationstatus#" />:</em></b><p class="padding">${usertransaction.recommendationStatus}</p></li>
														<li><b><em><spring:message code="transaction.recommendationmessage"
																	text="#transaction.recommendationmessage#" />:</em></b><p class="padding">${usertransaction.recommendationMessage}</p></li>
														<li><b><em><spring:message code="transaction.createdon"
																	text="#transaction.createdon#" />:</em></b><p class="padding">${usertransaction.createdOn}</p></li>
																								
													</ul>						
												</td>
											<%-- 	<td>
														${usertransaction.requestParam.amount}
	 													<table class="table table-bordered table-hover" style="word-wrap:break-word; table-layout: fixed;">
														<thead>
															<tr>
																<th><spring:message code="transaction.amount"
																		text="#transaction.amount#" /></th>
															<!-- <th><spring:message code="transaction.freefundcode"
																		text="#transaction.freefundcode#" /></th>
																<th><spring:message code="transaction.promocode"
																		text="#transaction.promocode#" /></th>
																<th><spring:message code="transaction.riskcalculationprofile"
																		text="#transaction.riskcalculationprofile#" /></th>
																<th><spring:message code="transaction.version"
																		text="#transaction.version#" /></th>
																<th><spring:message code="transaction.orderitems"
																		text="#transaction.orderitems#" /></th>
																<th><spring:message code="transaction.paymentplan"
																		text="#transaction.paymentplan#" /></th>
																<th><spring:message code="transaction.destinationinstrumentdetails"
																		text="#transaction.destinationinstrumentdetails#" /></th>
																<th><spring:message code="transaction.sourceentity"
																		text="#transaction.sourceentity#" /></th>
																<th><spring:message code="transaction.destentity"
																		text="#transaction.destentity#" /></th>
																<th><spring:message code="transaction.billingaddress"
																		text="#transaction.billingaddress#" /></th>
																<th><spring:message code="transaction.shippingaddress"
																		text="#transaction.shippingaddress#" /></th> -->
																<!--  <th><spring:message code="transaction.timestamp"
																		text="#transaction.timestamp#" /></th> -->
															<!--  <th><spring:message code="transaction.rescore"
																		text="#transaction.rescore#" /></th>
																<th><spring:message code="transaction.sessionid"
																		text="#transaction.sessionid#" /></th> -->
															</tr>
														</thread>
														<tbody class="customer-trail-rows">
															<tr>
																<td>${usertransaction.requestParam.amount}</td>
															<!--  <td>${usertransaction.requestParam.freefundCode}</td>
																<td>${usertransaction.requestParam.promoCode}</td>
																<td>${usertransaction.requestParam.riskCalculationProfile}</td>
																<td>${usertransaction.requestParam.transactionVersion}</td>
																<td>
																	<c:forEach var="orderitem" items="${usertransaction.requestParam.orderItems}">
																		<ul>
																			<li><em><spring:message code="transaction.itemid"
																			text="#transaction.itemid#" />:</em>${orderitem.itemId}</li>
																			<li><em><spring:message code="transaction.quantity"
																			text="#transaction.quantity#" />:</em>${orderitem.quantity}</li>
																			<li><em><spring:message code="transaction.name"
																			text="#transaction.name#" />:</em>${orderitem.name}</li>
																			<li><em><spring:message code="transaction.itemprice"
																			text="#transaction.itemprice#" />:</em>${orderitem.itemPrice}</li>
																			<li><em><spring:message code="transaction.promotionid"
																			text="#transaction.promotionid#" />:</em>${orderitem.promotionId}</li>
																			<li><em><spring:message code="transaction.promotionamount"
																			text="#transaction.promotionamount#" />:</em>${orderitem.promotionAmount}</li>
																			<li><em><spring:message code="transaction.seller"
																			text="#transaction.seller#" />:</em>${orderitem.seller}</li>
																			<li><em><spring:message code="transaction.categories"
																			text="#transaction.categories#" />:</em>${orderitem.categories}</li>
																		</ul>
																		<br>
																	</c:forEach>
																</td>
																<td>
																	<c:forEach var="paymentplan" items="${usertransaction.requestParam.paymentPlan}">
																		<ul>
																			<li><em><spring:message code="transaction.paymentinstrumenttype"
																			text="#transaction.paymentinstrumenttype#" />:</em>${paymentplan.paymentInstrumentType}</li>
																			<li><em><spring:message code="transaction.txnamount"
																			text="#transaction.txnamount#" />:</em>${paymentplan.txnAmount}</li>
																			<li><em><spring:message code="transaction.paymentinstrumentdetails"
																			text="#transaction.paymentinstrumentdetails#" />:</em>${paymentplan.paymentInstrumentDetails}</li>
																		</ul>
																		<br>
																	</c:forEach>
																</td>
																<td>${usertransaction.requestParam.destinationInstrumentDetails}</td>
																<td>
																	<ul>
																		<li><em><spring:message code="transaction.entitytype"
																		text="#transaction.entitytype#" />:</em>${usertransaction.requestParam.sourceEntity.entityType}</li>
																		<li><em><spring:message code="transaction.entityid"
																		text="#transaction.entityid#" />:</em>${usertransaction.requestParam.sourceEntity.entityId}</li>
																	</ul>
																</td>
																<td>
																	<ul>
																		<li><em><spring:message code="transaction.entitytype"
																		text="#transaction.entitytype#" />:</em>${usertransaction.requestParam.destEntity.entityType}</li>
																		<li><em><spring:message code="transaction.entityid"
																		text="#transaction.entityid#" />:</em>${usertransaction.requestParam.destEntity.entityId}</li>
																	</ul>
																<td>${usertransaction.requestParam.billingAddress}</td>
																<td>${usertransaction.requestParam.shippingAddress}</td> -->	
																<!-- <td>${usertransaction.requestParam.timeStamp}</td> -->
															<!-- <td>${usertransaction.requestParam.rescore}</td>
																<td>${usertransaction.requestParam.sessionId}</td>  -->
															</tr>
														</tbody>
													</table>
													
												</td> --%>
												<td class="BreakWord">
													${usertransaction.requestParam.device}		
												</td>
												<td class="BreakWord">
													${usertransaction.requestParam.deviceInfo}
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							</c:if>
				</section>
			</div>

			<script type="text/javascript">
		        $(document).ready(function(){
		
		            var customerTrailRows = $('.customer-trail-rows').find('tr');
		            customerTrailRows.eq(0).show();
				   
		        }
	        </script>
		</div>
	</div>
</div>