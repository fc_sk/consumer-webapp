<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@ include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
			<div class="customer-trail">
			<div class="cs-display">

				<div class="row">
					<div class="col-md-4 col-sm-4"></div>
				</div>
				<hr>

				<section class="unit">
					<div>
						<h1>
									<span class="label label-info"><spring:message
											code="p2pandp2mTxn.information" text="#p2pandp2mTxn.information#" /></span>
						</h1>
					</div>
					
					<c:if test="${not empty p2pAndP2MViewList}">
						
							<div class="layer">
								
								
								<h1>
									<span class="label label-info"><spring:message
											code="customer.information" text="#customer.information#" /></span>
								</h1>
								<ul>
									
									<li><em><spring:message code="partyName"
													text="#partyName#" />:</em>
										${p2pAndP2MViewList[0].name}	
									</li>
									<li><em><spring:message code="partyEmail"
													text="#partyEmail#" />:</em>
										<a href="/admin/customertrail/onecheck/searchbyemail/result.htm?emailid=${p2pAndP2MViewList[0].email}"><c:out value="${p2pAndP2MViewList[0].email}"/></a>	
									</li>
									<li><em><spring:message code="partyContact"
													text="#partyContact#" />:</em>
										${p2pAndP2MViewList[0].contact}	
									</li>
									<li><em><spring:message code="partyOrderId"
													text="#partyOrderId#" />:</em>
										${p2pAndP2MViewList[0].orderId}	
									</li>
									<li><em><spring:message code="partyTimeStamp"
													text="#partyTimeStamp#" />:</em>
										${p2pAndP2MViewList[0].timeStamp}	
									</li>
									<li><em><spring:message code="transactionDomain"
													text="#transactionDomain#" />:</em>
										${p2pAndP2MViewList[0].domain}	
									</li>
									<!--  <li><em><spring:message code="partyMigrationStatus"
													text="#partyMigrationStatus#" />:</em>
										${p2pAndP2MViewList[0].migrationStatus}	
									</li>
									<li><em><spring:message code="partyRunningBalance"
													text="#partyRunningBalance#" />:</em>
										${p2pAndP2MViewList[0].runningBalance}	
									</li>-->
								</ul>
									
							</div>
							<div class="layer">
								<h1>
									<span class="label label-info"><spring:message
											code="transaction.information" text="#transaction.information#" /></span>
								</h1>
								
								<table class="table table-bordered table-hover">
										<thead>
											<tr>
												<th><spring:message code="status"
													text="#status#" />
												</th>
												<th><spring:message code="errorCode"
													text="#errorCode#" />
												</th>
												<th><spring:message code="transactionAmount"
															text="#transactionAmount#" />
												</th>
												<th><spring:message code="requestType"
															text="#requestType#" />
												</th>
												<!-- 		<th><spring:message code="idempotencyId"
															text="#idempotencyId#" />
													</th>  -->
												<th><spring:message code="partyTag"
															text="#partyTag#" />
												</th>
												<th><spring:message code="otherPartyType"
															text="#otherPartyType#" />
												</th>	
												<th><spring:message code="otherPartyName"
															text="#otherPartyName#" />
												</th>
											<!-- 	<th><spring:message code="completedAt"
															text="#completedAt#" />
												</th>  -->
												<th><spring:message code="otherPartyEmail"
															text="#otherPartyEmail#" />
												</th>
												<th><spring:message code="otherPartyContact"
															text="#otherPartyContact#" />
												</th>
												<!-- 		<th><spring:message code="otherPartyMigrationStatus"
															text="#otherPartyMigrationStatus#" />
												</th>
														<th><spring:message code="otherPartyWalletBalance"
															text="#otherPartyWalletBalance#" />
												</th>  -->
											</tr>
										</thead>
										<tbody class="customer-trail-rows">
											<c:forEach var="p2pAndP2MView"
												items="${p2pAndP2MViewList}">
													<tr>
														<td>
															<c:choose>
															<c:when
																test="${p2pAndP2MView.status == 'SUCCESS'}">
																<span class="label label-success"><spring:message
																		code="txn.successful"
																		text="${p2pAndP2MView.status}" /></span>
															</c:when>
															<c:otherwise>
																<span class="label label-important"><spring:message
																		code="txn.unsuccessful"
																		text="${p2pAndP2MView.status}" />
																</span>
															</c:otherwise>
															</c:choose>
														</td>
														<td>${p2pAndP2MView.errorCode}</td>
														<td>${p2pAndP2MView.txnAmount}</td>
														<td>${p2pAndP2MView.requestType}</td>
														<%-- <td>${p2pAndP2MView.idempotencyId}</td> --%>
														<td>${p2pAndP2MView.paytag}</td>
														<td>${p2pAndP2MView.otherPartyType}</td>
														<td>${p2pAndP2MView.otherPartyName}</td>
														<!-- <td>${p2pAndP2MView.completedAt}</td>  -->
														<td><a href="/admin/customertrail/onecheck/searchbyemail/result.htm?emailid=${p2pAndP2MView.otherPartyEmail}"><c:out value="${p2pAndP2MView.otherPartyEmail}"/></a></td>
														<td>${p2pAndP2MView.otherPartyContact}</td>
														<!--  <td>${p2pAndP2MView.otherPartyMigrationStatus}</td>
														<td>${p2pAndP2MView.otherPartyWalletBalance}</td>-->
													</tr>
												
											</c:forEach>	
										</tbody>
									</table>					
									
									<%-- 
									 <c:forEach var="p2pAndP2MView" items="${p2pAndP2MViewList}">
									<ul>
									<li><em><spring:message code="status"
												text="#status#" />:</em> <c:choose>
											<c:when
												test="${p2pAndP2MView.status == 'SUCCESS'}">
												<span class="label label-success"><spring:message
														code="txn.successful"
														text="${p2pAndP2MView.status}" /></span>
											</c:when>
											<c:otherwise>
												<span class="label label-important"><spring:message
														code="txn.unsuccessful"
														text="${p2pAndP2MView.status}" />
												</span>
											</c:otherwise>
										</c:choose></li>
									<li><em><spring:message code="transactionAmount"
													text="#transactionAmount#" />:</em>
										${p2pAndP2MView.txnAmount}	
									</li>
									<li><em><spring:message code="requestedBy"
													text="#requestedBy#" />:</em>
										${p2pAndP2MView.requestedBy}	
									</li>
									<li><em><spring:message code="idempotencyId"
													text="#idempotencyId#" />:</em>
										${p2pAndP2MView.idempotencyId}	
									</li>
									<li><em><spring:message code="startedAt"
													text="#startedAt#" />:</em>
										${p2pAndP2MView.startedAt}	
									</li>
									<li><em><spring:message code="completedAt"
													text="#completedAt#" />:</em>
										${p2pAndP2MView.completedAt}	
									</li>
									<li><em><spring:message code="otherPartyEmail"
													text="#otherPartyEmail#" />:</em>
										${p2pAndP2MView.otherPartyEmail}	
									</li>
									<li><em><spring:message code="otherPartyContact"
													text="#otherPartyContact#" />:</em>
										${p2pAndP2MView.otherPartyContact}	
									</li>
									<li><em><spring:message code="otherPartyMigrationStatus"
													text="#otherPartyMigrationStatus#" />:</em>
										${p2pAndP2MView.otherPartyMigrationStatus}	
									</li>
									<li><em><spring:message code="otherPartyWalletBalance"
													text="#otherPartyWalletBalance#" />:</em>
										${p2pAndP2MView.otherPartyWalletBalance}	
									</li>
								</ul>
								</c:forEach> 
								
								--%>
									
								
							</div>
						
					</c:if>

				</section>
				
					<!-- -------------------------------Campaign History---------------->
        		<section class="unit">
	        		<div class="layer">
	   					<jsp:include page="rewardhistory.jsp" />
	   				</div>
        		</section> 
				
			</div>



			<script type="text/javascript">
		        $(document).ready(function(){
		
		            var customerTrailRows = $('.customer-trail-rows').find('tr');
		            customerTrailRows.eq(0).show();
				   
		        }
	        </script>
		</div>
	</div>
</div>