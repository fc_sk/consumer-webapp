<%@page import="com.freecharge.admin.CustomerTrailDetails.EmailInputController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <div class="col-sm-3 col-md-3">
        <jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp"/>
    </div>

    <div class="col-sm-9 col-md-9">
        <c:if test="${not empty exception }">
            <div class="alert alert-error" >
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                ${exception}
            </div>
        </c:if>

        <h3>Promo/Freefund Class Details</h3>
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th><spring:message code="freefundcode" text="#freefundcode#"/></th>
                <th><spring:message code="freefund.promo.info" text="#freefund.promo.info#"/></th>
                <th><spring:message code="description" text="#description#"/></th>
                <th><spring:message code="freefund.value" text="#freefund.value#"/></th>
                <th><spring:message code="valid.from" text="#valid.from#"/></th>
                <th><spring:message code="valid.upto" text="#valid.upto#"/></th>
                <th><spring:message code="min.recharge.value" text="#min.recharge.value#"/></th>
                <th><spring:message code="max.redemption" text="#max.redemption#"/></th>
                <th><spring:message code="max.discount" text="#max.discount#"/></th>
                <th><spring:message code="free.campaign.name" text="#free.campaign.name#"/></th>
            </tr>
            </thead>
            <tr>
                <td>${freefundCoupon.freefundCode}</td>
                <td>${freefundClass.promoEntity}</td>
                <td>${freefundClass.description}</td>
                <td>${freefundClass.freefundValue}</td>
                <td><fmt:formatDate type="date" pattern="dd-MMM-yyyy HH:mm:SS" value="${freefundClass.validFrom.time}"/></td>
                <td><fmt:formatDate type="date" pattern="dd-MMM-yyyy HH:mm:SS" value="${freefundClass.validUpto.time}" /></td>
                <td>${freefundClass.minRechargeValue}</td>
                <td>${freefundClass.maxRedemptions}</td>
                <td>${freefundClass.maxDiscount}</td>
                <td>${freefundClass.name}</td>
            </tr>
        </table>

        <div style="display: none;">
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th><spring:message code="code.generated.date" text="#code.generated.date#"/></th>
                <th><spring:message code="class.id" text="#class.id#"/></th>
                <th><spring:message code="class.name" text="#class.name#"/></th>
                <th><spring:message code="free.class.update" text="#free.class.update#"/></th>
                <th><spring:message code="isactive" text="#isactive#"/></th>
                <th><spring:message code="discount.type" text="#discount.type#"/></th>
                <th><spring:message code="apply.condition.id" text="#apply.condition.id#"/></th>
                <th><spring:message code="payment.condition.id" text="#payment.condition.id#"/></th>
                <th><spring:message code="redeem.condition.id" text="#redeem.condition.id#"/></th>
            </tr>
            </thead>
            <tr>
                <td>${freefundCoupon.generatedDate}</td>
                <td>${freefundClass.id}</td>
                <td>${freefundClass.name}</td>
                 <td><fmt:formatDate type="date" pattern="dd-MMM-yyyy HH:mm:SS" value="${freefundClass.updatedAt.time}"/></td>
                <td>${freefundClass.isActive}</td>
                <td>${freefundClass.discountType}</td>
                <td>${freefundClass.applyConditionId}</td>
                <td>${freefundClass.paymentConditionId}</td>
                <td>${freefundClass.redeemConditionId}</td>
        </table>
        
        </div>
        <a href="javascript:void(0);" class="more-info btn btn-mini">Show more</a>


        <c:choose>
            <c:when test="${promocodeType == 'genericCode'}">
                <div id="test" style="border: 2px solid;">
                <h4>Generic Promocode Usage Details</h4>
                <form name="genericCodeUsageInfoForm" id="genericCodeUsageInfoForm" action="/admin/customertrail/genericCodeUsageDetails.htm">
                    <select class="input" name="mappingKey" title="Select Mapping Key" style="width: 200px;" >
                        <option value="email">Email Id</option>
                        <option value="serviceNumber">Service Number</option>
                        <option value="orderId">Order Id</option>
                    </select>
                    <input type="text" name="keyValue"/>
                    <input type="hidden" name="freefundCouponId" value="${freefundCoupon.id}"/>
                </form>
                <input type="button" class="btn trim-input" id="generic-code-usage-info" value="Get data"/>
                <hr>
                <div id="promocode-usage-details"></div>
                </div>
            </c:when>
            <c:when test="${promocodeType == 'multiuseUniqueCode'}">
                <h4><spring:message code="multiuse.unique.promocode.details" text="#multiuse.unique.promocode.details#"/></h4>
                <c:choose>
                    <c:when test="${not empty multiuseBlockedPromocodeData}">
                        <table   class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th><spring:message code="order.id" text="#order.id#"/>
                                <th><spring:message code="used.service.number" text="#used.service.number#"/></th>
                                <th><spring:message code="used.email.id" text="#used.email.id#"/></th>
                                <th><spring:message code="used.date" text="#used.date#"/></th>
                                <th><spring:message code="status" text="#status#"/></th>
                                <th><spring:message code="reset" text="#reset#"/>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${multiuseBlockedPromocodeData}" var="multiuseBlockedPromocode">
                                <tr>
                                    <td>${multiuseBlockedPromocode.order_id}</td>
                                    <td>${multiuseBlockedPromocode.service_number}</td>
                                    <td>${multiuseBlockedPromocode.email_id}</td>
                                    <td>${multiuseBlockedPromocode.created_at}</td>
                                    <td>${multiuseBlockedPromocode.status}</td>
                                    <td>
                                        <c:if test="${multiuseBlockedPromocode.status == 'BLOCKED'}">
                                        <a class="multiuse-code-reset" data-promocode="${multiuseBlockedPromocode.promocode}"
                                           data-service-number="${multiuseBlockedPromocode.service_number}"
                                           data-order-id="${multiuseBlockedPromocode.order_id}"
                                           href="#">reset</a>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:when>
                    <c:otherwise>
                        <hr>
                        <div class="alert-error"><i>No usage details for the promocode</i></div>
                    </c:otherwise>
                </c:choose>
                <hr>
            </c:when>

            <c:when test="${promocodeType == 'uniqueCode'}">
                <c:choose>
                    <c:when test="${freefundCoupon.promoEntity eq 'redeem-to-fcbalance'}">
                        <h4><spring:message code="freefund.code.details" text="#freefund.code.details#"/></h4>
                    </c:when>
                    <c:otherwise>
                        <h4><spring:message code="unique.promocode.details" text="#unique.promocode.details#"/></h4>
                    </c:otherwise>
                </c:choose>
                        <table   class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th><spring:message code="order.id" text="#order.id#"/>
                                <th><spring:message code="email" text="email"/></th>
                                <th><spring:message code="used.service.number" text="#used.service.number#"/></th>
                                <th><spring:message code="used.date" text="#used.date#"/></th>
                                <th><spring:message code="status" text="#status#"/></th>
                                <c:choose>
                                    <c:when test="${not empty reIssuedPromocode}">
                                        <th><spring:message code="reIssuedPromocode" text="#reIssuedPromocode#"/></th>
                                        <th><spring:message code="reIssuedPromocodeStatus" text="#reIssuedPromocodeStatus#"/></th>
                                    </c:when>
                                </c:choose>
                                <th><spring:message code="reset" text="#reset#"/>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                    <td>${attemptedByOrderIds[attemptedByOrderIds.size()-1]}</td>
                                    <td>${freefundCoupon.usedEmailId}</td>
                                    <td>${freefundCoupon.usedServiceNumber}</td>
                                    <td>${freefundCoupon.usedDate}</td>
                                    <td>${freefundCoupon.status}</td>
                                    <c:choose>
                                        <c:when test="${not empty reIssuedPromocode}">
                                            <td>${reIssuedPromocode}</td>
                                            <td>${reIssuedPromocodeStatus}</td>
                                        </c:when>
                                    </c:choose>
                                    <td>
                                        <c:if test="${freefundCoupon.promoEntity != 'redeem-to-fcbalance'}">
                                            <c:if test="${freefundCoupon.status == 'BLOCKED'}">
                                                <a href="#promoCodeReset" id="promo-code-reset" class="btn btn-link" data-toggle="modal" data-promocode="${freefundCoupon.freefundCode}">Reset</a>
                                            </c:if>
                                            <!-- Modal -->
                                            <div id="promoCodeReset" class="modal fade" style="height: 40%;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-header">
                                                    <button type="button" class="close close-dialog" data-dismiss="modal" aria-hidden="true">x</button>
                                                    <h3 id="myModalLabel">Confirm Promo/Freefund Code reset</h3>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to reset the Promo/Freefund Code?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                                    <button class="btn btn-primary" id="confirmPromoCodeReset">Reset Promocode</button>
                                                </div>
                                            </div>
                                        </c:if>
                                    </td>
                                </tr>
                        </tbody>
                        </table>
                <hr>
            </c:when>
        </c:choose>

        <!-- Successful redeemed orderid -->
        <div>
            <c:choose>
                <c:when test="${not empty reedemedOrderIdMap}">
                    <hr>
                    <h5>
                        Successfully redeemed orderId(Latest success-txn orderId using above freefund/promo code)
                    </h5>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Order Id</th>
                                <th>Generated date</th>
                        </thead>
                        <tbody>
                                <tr>
                                    <td><a href="/admin/customertrail/getcustomertrail?orderId=${reedemedOrderIdMap['redeemedOrderId']}">${reedemedOrderIdMap['redeemedOrderId']}</a></td>
                                    <td>${reedemedOrderIdMap['orderIdGeneratedDate']}</td>
                                </tr>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                </c:otherwise>
            </c:choose>
        </div>

        <c:if test="${not empty attemptedByOrderIds}">
           <h5><spring:message code="promo.free.attempted.orderids" text="#promo.free.attempted.orderids#"/></h5>
           [<c:forEach var="orderIdAttempted" items="${attemptedByOrderIds}">
                <a href="/admin/customertrail/getcustomertrail?orderId=${orderIdAttempted}" target="${orderIdAttempted}">${orderIdAttempted}</a>,
           </c:forEach>]
           <hr>
        </c:if>

		<h3>Referral Code Details</h3>
        <div>
        <table class="table table-hover table-bordered">
	          <thead>
	              <tr>
	                  <th>Referrer Email</th>
	                  <th>Referrer Code</th>
	                  <th>Cashback Earned</th>
	                  <th>Associated Orders</th>
	                  <th>Status</th>
	              </tr>
	          </thead>
         	 <tr>
              <td>${referralEntity.referrerEmail}</td>
              <td>${referralEntity.referralCode}</td>
              <td>${referralEntity.referrerCashbackEarned}</td>
              <td>${referralEntity.referredOrders}</td>
              <td>${referralEntity.status}</td>
          </tr>
       </table>
       </div>

        <h3>Condition Id Details</h3>
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th><spring:message code="condition.id" text="#condition.id#"/></th>
                    <th><spring:message code="conditionid.name" text="#conditionid.name#"/></th>
                    <th><spring:message code="condition.type.id" text="#condition.type.id#"/></th>
                    <th><spring:message code="conditionid.params" text="#conditionid.params#"/></th>
                    <th><spring:message code="condition.type.class.ident" text="#condition.type.class.ident#"/></th>
                    <th><spring:message code="description" text="#description#"/></th>
                </tr>
            </thead>
            <tr>
                <td>${arConditionEntity.id}</td>
                <td>${arConditionEntity.name}</td>
                <td>${arConditionEntity.fkARConditionTypeId}</td>
                <td>${arConditionEntity.params}</td>
                <td>${arConditionTypeEntity.classIdent}</td>
                <td>${arConditionTypeEntity.description}</td>
            </tr>
        </table>

        <div class="clearfix">
                 <h3>Pointers</h3>
                   <table class="table table-hover table-bordered">
                     <tr>
                       <td><h4>Freefund Code:</h4> A freefund code is a code which adds on to a user's Credits (is not a part of the recharge transaction).<br>
                       <h4>PromoCode :</h4> A promocode is a code which provides instant discount to a user in the recharge transaction.</td>
                       <td>A code which is in BLOCKED state can be reset.<br><br>
                          If a PromoCode which is in REDEEMED state is reset,
                          it may still be possible that the user is unable to use it due to fraud checks. In this case,
                          please give a new code to the user.<br><br>
                          A Freefund code which is in REDEEMED state cannot be reset.
                       </td>
                    </tr>
               </table>
        </div>

    </div>
</div>
	 
<script type="text/javascript">
$(document).ready(function(){
	$("#confirmPromoCodeReset").on('click', function(event) {
		event.preventDefault();
	      var promoCode = {
	    		 promoCode:$('#promo-code-reset').data("promocode")
	      };
	    $.ajax({
	         type: "GET",
	         url: "/admin/customertrail/resetfreefundorpromocode.htm",
	         data:promoCode,
	         success: function(response){
	        	$('.alert-error').find('ul').html('<li>'+response+'</li>');
	        	$('.close-dialog').trigger('click');
	         },
	         error: function(e){
	        	  console.log("ERROR");
	        }
	     });
	});
    $(".multiuse-code-reset").on('click', function(event) {
        event.preventDefault();
        var data = {
            promocode:$(this).attr("data-promocode"),
            orderId:$(this).attr("data-order-id")
        };
        $.ajax({
            type: "GET",
            url: "/admin/customertrail/resetMultiusePromocode.htm",
            data:data,
            success: function(response){
                alert("Unblocked successfully");
            },
            error: function(e){
                console.log("ERROR");
            }
        });
    });
    $("#generic-code-usage-info").on('click', function(event) {
        var genericCodeUsageInfoForm = $("#genericCodeUsageInfoForm");
        var keyValue = $("input[name='keyValue']", genericCodeUsageInfoForm);
        if(keyValue === undefined || keyValue === null || keyValue.length === 0 ) {
            alert("Enter keyValue");
            return false;
        }
        $.ajax({
            type: "GET",
            url: genericCodeUsageInfoForm.attr('action'),
            data: genericCodeUsageInfoForm.serialize(),
            success: function(response) {
                $("#promocode-usage-details").html(response);
            },
            error: function(response) {

            }
        });
    });
});

$(".more-info").on("click", function(){
    
    if($(this).prev("div").is(':visible')){
        $(this).prev("div").slideUp(200);
        $(this).text("Show more");
    }
    else {
        $(this).prev("div").slideDown(200);
        $(this).text("Show less");
    }           
});

</script>