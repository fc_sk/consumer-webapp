<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<center>
	<h4>Fraud case handler status</h4>
</center>
<div>
	<a
		href="/admin/fraud/reportbypg/handler/excelReportForFraudCase?fraudDataObjectId= <c:out value="${fraudDataObjectId}"/>"
		class="btn btn-mini btn-info"
		style="font-size: 10px; margin-bottom: 8px;"><i>Export to CSV</i></a>
</div>
<div>
	<div class="container">
		<c:choose>
			<c:when test="${not empty fraudCaseHandlerDataList}">
				<table class="table table-bordered table-condensed">
					<thead>
						<tr>
							<th>OrderId</th>
							<th>Merchant TxnId</th>
							<th>PaymentTxnId</th>
							<th>Service</th>
							<th>PG</th>
							<th>Email</th>
							<th>Customer Name</th>
							<th>Profile Num</th>
							<th>Address</th>
							<th>IpAddress</th>
							<th>Refunded Amount</th>
							<th>Refund Date</th>
							<th>BanEmail status</th>
							<th>Refunded orderId-amount</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="fraudCaseHandlerData"
							items="${fraudCaseHandlerDataList}">
							<tr>
								<td><a
									href="/admin/customertrail/getcustomertrail?orderId=${fraudCaseHandlerData.orderId}">${fraudCaseHandlerData.orderId}</a></td>
								<td>${fraudCaseHandlerData.merchantTxnId}</td>
								<td>${fraudCaseHandlerData.paymentTxnId}</td>

								<td>${fraudCaseHandlerData.service}</td>
								<td>${fraudCaseHandlerData.paymentGateway}</td>
								<td>${fraudCaseHandlerData.email}</td>
								<td>${fraudCaseHandlerData.customerName}</td>
								<td>${fraudCaseHandlerData.profileNumber}</td>
								<td>${fraudCaseHandlerData.address}</td>
								<td>${fraudCaseHandlerData.ipAddress}</td>
								<td>${fraudCaseHandlerData.refundedAmount}</td>
								<td>${fraudCaseHandlerData.refundDate}</td>
								<td>${fraudCaseHandlerData.banEmailId}</td>
								<td><a href="#" class="btn btn-mini btn-info"
									onclick="displayRefundOrderId('${fraudCaseHandlerData.refundedOrderIdAmountMap}')">Display</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:when>
			<c:otherwise>
				<h5 class="alert alert-error">No fraud handler process data</h5>
			</c:otherwise>
		</c:choose>
	</div>
	<div class="modal hide fade" id="orderIdModal" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">x</button>
					<h4 class="modal-title text-info">Refunded orderIds and amount</h4>
				</div>
				<div class="modal-body">
					<div id="refundedOrderIdList"></div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
</div>
<script type="text/javascript">
	function displayRefundOrderId(orderidlist) {
		$('#orderIdModal').modal('show');
		$('#refundedOrderIdList').text(orderidlist);
	}
</script>