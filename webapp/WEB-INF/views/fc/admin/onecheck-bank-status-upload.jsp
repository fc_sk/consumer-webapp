<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<c:if test="${not empty messages }">
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		${messages}
	</div>
</c:if>
<div>
	<form:form method="post" action="/admin/tools/onecheck/bank/transfer/upload/do.htm"
		enctype="multipart/form-data">
		<center><h3>Upload onecheck credits bank transfer status</h3></center>
		<h5>Instructions : </h5>
		<ul>
            <li>Use either TextArea or File Upload at a time</li>
            <li>Format of input : id, userId,bankAccountNo,ifscCode,beneficiaryName,emailId,referenceId,amount,transferStatus,transferDate,failureReason in each row</li>
        </ul>
		<table class="table table-striped table-bordered">
			<tr>
				<th align="left"><font color="grey">Upload
						a CSV file for upload transfer status</font></th>
			</tr>
			<tr>
				<td><input type="file" class="input" id="bankdatauploadfile"
					name="bankdatauploadfile"><br> <br></td>
			</tr>
		</table>
		<input type="submit" value="Upload" />
	</form:form>
<HR>
    Download Non success data : <a href="/admin/tools/onecheck/bank/transfer/download/do.htm">Download</a> 
<HR>
	<div style="display: none;">
		<br>
		<h1>
			<span class="label label-success">Upload process success:</span>
		</h1>
		<c:if test="${not empty successCount}">
			<span class="label label-success">Success count : <c:out
					value="${successCount}" /></span>
		</c:if>
		<br />
		<c:forEach var="successId"
			items="${successUserIds}">
		 	<c:out value="${successId}," />
			<br>
		</c:forEach>
		<br>
		<h1>
			<span class="label label-warning">Upload failed user Ids :</span>
		</h1>
		<c:if test="${not empty failureCount}">
			<span class="label label-warning">Failure count : <c:out
					value="${failureCount}" /></span>
		</c:if>
		<c:forEach var="failedUserId"
			items="${failedUserIds}">
				<c:out value="${failedUserId}," />
			<br>
		</c:forEach>
		<br>
	</div>
	<a href="javascript:void(0);" class="more-info">Show Status</a>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		$(".more-info").on("click", function() {

			if ($(this).prev("div").is(':visible')) {
				$(this).prev("div").slideUp(200);
				$(this).text("Show Status");
			} else {
				$(this).prev("div").slideDown(200);
				$(this).text("Show less");
			}
		});
	});
</script>