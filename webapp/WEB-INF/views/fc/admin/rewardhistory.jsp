<%@ page import="com.freecharge.customercare.controller.CustomerTrailController" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<c:choose>
    <c:when test="${not empty rewardHistoryWrapper}">
    <hr>
    <h4><span class="label label-info">Reward History</span></h4>
    <c:choose>
        <c:when test="${rewardHistoryWrapper.status eq 'Success'}">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Campaign</th>
                        <th>Stage (Trigger)</th>
                        <th>Condition Status</th>
                        <th>Condition Failure Msg</th>
                        <th>Detail Condition Failure Msg</th>
                        <th>Reward TimeStamp</th>
                        <th>Reward Results</th>
                        <!--<th></th>-->
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="rewardHistory"
                        items="${rewardHistoryWrapper.campaignHistoryList}">
                        <tr>
                            <td>${rewardHistory.campaignName}</td>
                            <td>${rewardHistory.trigger}
                            <td>${rewardHistory.conditionStatus}</td>
                            <td>${rewardHistory.conditionFailureMessage}</td>
                            <td>${rewardHistory.conditionFailureDetailedMessage}</td>
                            <td>${rewardHistory.timestamp}</td>
                            <td>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Reward Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="rewardResult" items="${rewardHistory.rewardStatusDetailMap}">
                                            <tr>
                                                <td>${rewardResult.key}</td>
                                                <td>
                                                    <table class="table table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Status</th>
                                                                <th>Name</th>
                                                                <th>Type</th>
                                                                <th>Message</th>
                                                                <th>Reward Value</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>${rewardResult.value.rewardStatus}</td>
                                                                <td>${rewardResult.value.name}</td>
                                                                <td>${rewardResult.value.rewardType}</td>
                                                                <td>${rewardResult.value.message}</td>
                                                                <td>${rewardResult.value.rewardValueMap}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </td>
                            <!--<td>
                                <c:if test="${rewardHistory.conditionStatus eq 'false'}">
                                    <button class="btn btn-mini btn-primary reattemptRewardButton" data-toggle="modal"
                                         data-campaignid="${rewardHistory.campaignId}"
                                         data-trigger="${rewardHistory.trigger}"
                                         data-uniqueid="${rewardHistory.uniqueId}">
                                         Re-Attempt
                                    </button>
                                </c:if>
                            </td>-->
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:when>
        <c:otherwise>
            <h5>${rewardHistoryWrapper.status}</h5>
        </c:otherwise>
    </c:choose>
    </c:when>
    <c:otherwise>
    </c:otherwise>
</c:choose>

<div class="modal fade" id="reattemptRewardModal" tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel"  style="display: none; height: 30%">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
      <h4 id="myModalLabel">Re-Attempt Reward</h4>
   </div>
   <div class="modal-body" >
      <form id="reattemptRewardForm">
        <div><input type="text" style="width:auto" id="campaignid" name="campaignId" readonly="readonly"></div>
        <div><input type="text" style="width:auto" id="trigger" name="trigger" readonly="readonly"></div>
        <div><input type="text" style="width:400px" id="uniqueid" name="uniqueId" readonly="readonly"></div>
        <div id="reattemptResponse" class="alert alert-error" style="display: none;"></div>
        <div><button type="submit" class="btn btn-small btn-primary">Re-Attempt</button></div>
      </form>
   </div>
</div><!-- /.modal -->


<script type="text/javascript">
    $(".reattemptRewardButton").click(function() {
        $("#campaignid").val($(this).data('campaignid'));
        $("#trigger").val($(this).data('trigger'));
        $("#uniqueid").val($(this).data('uniqueid'));
        $('#reattemptResponse').hide();
        $('#reattemptRewardModal').modal('show');
    });
    $("#reattemptRewardForm").submit(function(e) {
        e.preventDefault();

        $.ajax({
               type: "GET",
               url: "/admin/customertrail/campaignhistory/reward/reattempt.htm",
               data: $("#reattemptRewardForm").serialize(),
               success: function(data)
               {
                   if(data.status == true) {
                        $('#reattemptResponse').show();
                        $('#reattemptResponse').removeClass("alert-error").addClass("alert-success");
                        $('#reattemptResponse').text(data.status+": "+data.message);
                   } else {
                        $('#reattemptResponse').show();
                        $('#reattemptResponse').removeClass("alert-success").addClass("alert-error");
                        $('#reattemptResponse').text(data.status+": "+data.message);
                   }
               }
             });
    });
</script>