<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<!-- .mainContainer Starts  -->
<div class="mainContainer"  >
    <div class="pageWidth clearfix form">
    	<c:if test="${not empty param.message}">
    	<div style="background-color:yellow;padding:10px;"><c:out value="${param.message}"/></div>
    	</c:if>
    	   <div class="manage_plans boxDesign" style=" ">  <h1><spring:message code="revoke.fcbalance" text="#revoke.fcbalance#"/></h1> <br>
			   <form:form  name="revokefcbalanceform" action="/admin/wallet/dorevokefcbalance.htm" method="post" enctype="multipart/form-data" cssClass="form" id="revokefcbalanceform" >	
					<b><spring:message code="upload.orderids" text="#upload.orderids#"/></b> <spring:message code="upload.csv" text="#upload.csv#"/><br>
				   <input type="file" class="input"  id="orderids" name="orderids" ><br><br>
				   <input type="submit"  title="Submit" value="Submit" class="button" id="butsubmit" name="addbut">
			   </form:form>	
			   <br><br>
		 <div  style="display: none;">
		   <br><h3><spring:message code="count.msg" text="#count.msg#"/>:</h3><br/>
			         ${messages}
			  <br><h3><spring:message code="revoked.orderIds" text="#revoked.orderIds#"/></h3><br/>
			    <c:forEach var="revokedList" items="${revokedOrderIdList}">
			           <li><c:out value="${revokedList}"/></li>
			     </c:forEach>
			        <br><h3><spring:message code="nonrevoked.orderIds" text="#nonrevoked.orderIds#"/></h3>
			         <c:forEach var="nonRevokedList" items="${nonRevokedOrderIdList}">
			              <c:out value="${nonRevokedList}"/><br/>
			       </c:forEach>
			   </div>
			      <a href="javascript:void(0);" class="more-info">Show Status</a>	
			   <br><br>
           </div>  
           </div>  
    </div>     
<script type="text/javascript">

$(document).ready(function(){
	
	$(".more-info").on("click", function(){
		
		if($(this).prev("div").is(':visible')){
			$(this).prev("div").slideUp(200);
			$(this).text("Show Status");
		}
		else {
			$(this).prev("div").slideDown(200);
			$(this).text("Show less");
		}		
		
		
	});
	
});

</script>

