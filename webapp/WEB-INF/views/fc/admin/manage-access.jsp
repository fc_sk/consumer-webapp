<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<head>
<link href="/content/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/jquery-ui.css">
 <script src="${mt:keyValue("jsprefix2")}/lib/jquery-ui.1.11.4.js"></script>
   <link href="${mt:keyValue("cssprefix1")}/chosen.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/> 
</head>

<div class="container">
	<div class="alert" style="display:none"></div>
	<a href="/admin" id="homeAnchorTag" class="btn">Home</a>	
	<div id="tabs"> 	
	
		<ul>
			<li><a href="#list">User Management</a></li>
			<li><a href="#category">Category Management</a></li>
			<li><a href="#comp">Component Management</a></li>
		</ul>

		<div class="user-access-manager" id="list" style="">
			<h2>Access Control</h2>
			<table class="table table-bordered table-striped">
				<tr>
					<th><strong>Email</strong></th>
					<th><strong>Components</strong></th>
					<th><strong>User Type</strong></th>
					<th><strong>Category Access</strong></th>
					<th><strong>Action</strong></th>

				</tr> 	
				<c:forEach var="user" items="${users}">
					<tr class="row-user">
						<td>${user.email}</td>
						<td class="div-components">
							<%--adding components to multi select --%> <select
							multiple="multiple" class="input-xxlarge components">
								<c:forEach var="component" items="${components}">
									<c:if test="${component.active eq '1'}">
										<c:set var="selected" value="false" />
										<c:forEach var="com" items="${user.components}">
											<c:if test="${component.name eq com and component.active eq '1'}">
												<c:set var="selected" value="true" />
											</c:if>
										</c:forEach>
										<c:choose>
											<c:when test="${not selected}">
												<option value="${component.name}">${component.name}</option>
											</c:when>
											<c:otherwise>
												<option value="${component.name}" selected="selected">${component.name}</option>
											</c:otherwise>
										</c:choose>
									</c:if>
								</c:forEach>
						</select>
						</td>
						<td class="div-userTypes"><select class="userType">
								<c:forEach var="userType" items="${userType}">
									<c:choose>
										<c:when test="${userType eq user.userType}">
											<option value="${userType}" selected="selected">${userType}</option>
										</c:when>
										<c:otherwise>
											<option value="${userType}">${userType}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
						</select></td>
						<td class="div-userCategory"><select multiple="multiple" class="input userCategory">
								<c:forEach var="category" items="${userCategories}">
									<c:set var="selected" value="false" />
									<c:forEach var="cat" items="${user.categories}">
										<c:if test="${category eq cat}">
											<c:set var="selected" value="true" />
										</c:if>
									</c:forEach>
									<c:choose>
										<c:when test="${not selected}">
											<option value="${category}">${category}</option>
										</c:when>
										<c:otherwise>
											<option value="${category}" selected="selected">${category}</option>
										</c:otherwise>
									</c:choose>

								</c:forEach>
						</select></td>
						<td><a href="#" data-email="${user.email}" class="btn btn-success btn-mini save-access">Save</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<div class="category-component-manager" id="category">
			<!-- Category Container div-->
			<h2>Category Control</h2>
			<table class="table table-bordered table-striped" id="category-table">
				<tr>
					<th><strong>Category</strong></th>
					<th><strong>Components</strong></th>
					<th>Save</th>
					<th>Delete</th>
				</tr>
				<c:forEach var="category" items="${categories}" varStatus="status">
					
					<c:forEach var="cat" items="${userCategories}">
										<c:if test="${category.categoryName eq cat}">
											<c:set var="selected" value="true" />
										</c:if>
									</c:forEach>
							
					<c:if test="${selected eq true}" >
					<tr class="row-category">
						<!-- category name :${category.categoryName}-->
						<td><input value="${category.categoryName}" id="input${status.index}"  disabled="disabled" data-name="${category.categoryName}"/> 
						</td>
						<td class="div-category-components">
						<select multiple="multiple" class="input-xxlarge components">
								<c:forEach var="component" items="${components}">
									<c:if test="${component.active eq '1'}">
										<c:set var="selected" value="false" />
										<c:forEach var="com" items="${category.components}">
											<c:if test="${component.name eq com}">
												<c:set var="selected" value="true" />
											</c:if>
										</c:forEach>
										<c:choose>
											<c:when test="${not selected}">
												<option value="${component.name}">${component.name}</option>
											</c:when>
											<c:otherwise>
												<option value="${component.name}" selected="selected">${component.name}</option>
											</c:otherwise>
										</c:choose>
									</c:if>
								</c:forEach>
						</select></td>
						<td><a href="#"
							class="btn btn-success btn-mini save-category">save</a></td>
						<td><a href="#"
						class="btn btn-success btn-mini delete-category">x</a></td>
					</tr>
			 	</c:if>
				</c:forEach>
			</table>
			<button id="addCategory" href="#">Add</button>
		</div>
		<div class="component-manager" id="comp">
			<h2>Component Control</h2>
			<table class="table table-bordered table-striped" id="component-table">
				<tr>
					<th><strong>Component</strong></th>
					<th><strong>URL</strong></th>
					<th><strong>REST Endpoints</strong></th>
					<th><strong>Active</strong></th>
					<th><strong>Action</strong></th>
					<c:if test="${adminUser.userType eq superAdminString }">
					<th><strong>Delete</strong></th>
					</c:if>
				</tr>

				
				<c:if test="${adminUser.userType eq superAdminString }">
				<c:forEach var="component" items="${components}" varStatus="status">
					<tr class="row-component">
						<td><input value="${component.name}"
							id="name-input${status.index}" class="name edit"
							disabled="disabled" data-name="${component.name}" /> 
							</td>
						<td>
							<input value="${component.URL}" id="url-input${status.index}" class="url edit"disabled="disabled" />
							 <a href="#url-edit-tag-${status.index}" class="btn btn-success btn-mini edit-input" id="url-edit-tag-${status.index}">edit</a></td>
						<td class="div-components-restPat">
                        						<select multiple="multiple" class="input-xxlarge restPat">
                        								<c:forEach var="pattern" items="${restPatterns}">
                        										<c:set var="selected" value="false" />
																	<c:forEach var="com" items="${component.rest_patterns}">
                        											<c:if test="${pattern eq com}">
                        												<c:set var="selected" value="true" />
                        											</c:if>
                        										</c:forEach>
                        										<c:choose>
                        											<c:when test="${not selected}">
                        												<option value="${pattern}">${pattern}</option>
                        											</c:when>
                        											<c:otherwise>
                        												<option value="${pattern}" selected="selected">${pattern}</option>
                        											</c:otherwise>
                        										</c:choose>

                        								</c:forEach>
                        	</select></td>

						<td><input type="checkbox" value="active" id="active-input${status.index}" class="active-state" ${component.active eq '1'?'checked':''} /></td>
						<td>
							<button class="save-component">Save</button>
						</td>
						<c:if test="${adminUser.userType eq superAdminString }">
						<td><a href="#" class="btn btn-success btn-mini delete-component">x</a> </td>
						</c:if>
					</tr>
				</c:forEach>
				</c:if>

				<c:set var="type" value="Admin" />
				<c:if test="${type eq adminUser.userType}">
					<c:forEach var="component" items="${components}" varStatus="status">
						<c:forEach var="com" items="${adminUser.components}">
							<c:if test="${component.name==com}">
								<tr class="row-component">
									<td><input value="${component.name}" id="name-input${status.index}" class="name edit" disabled="disabled" data-name="${component.name}" /> 
									</td>
									<td><input value="${component.URL}" id="url-input${status.index}" class="url edit" disabled="disabled" /> 
									<a href="#url-edit-tag-${status.index}" class="btn btn-success btn-mini edit-input" id="url-edit-tag-${status.index}">edit</a></td>
						<td class="div-components-restPat">
                        						<select multiple="multiple" class="input-xxlarge restPat">
                        								<c:forEach var="pattern" items="${restPatterns}">
                        										<c:set var="selected" value="false" />
																	<c:forEach var="com" items="${component.rest_patterns}">
                        											<c:if test="${pattern eq com}">
                        												<c:set var="selected" value="true" />
                        											</c:if>
                        										</c:forEach>
                        										<c:choose>
                        											<c:when test="${not selected}">
                        												<option value="${pattern}">${pattern}</option>
                        											</c:when>
                        											<c:otherwise>
                        												<option value="${pattern}" selected="selected">${pattern}</option>
                        											</c:otherwise>
                        										</c:choose>

                        								</c:forEach>
                        	</select></td>
                        <td><input type="checkbox" value="active" id="active-input${status.index}" class="active-state"${component.active eq '1'?'checked':''} /></td>
									<td>
										<button class="save-component">Save</button> 
									</td>								
									<c:if test="${adminUser.userType eq superAdminString }">
									<td>
									<a href="#" class="btn btn-success btn-mini delete-component">x</a> </td>	
									</c:if>																	
								</tr>
							</c:if>
						</c:forEach>
					</c:forEach>
				</c:if>
			</table>
			<button id="add-component">Add</button>
		</div>
	</div>
</div>


<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/chosen.jquery.js?v=${mt:keyValue("version.no")}"></script>

<script type="text/javascript">
   $(document).ready(function(){
    	
        $(".components").chosen({no_results_text: "no component", min_width: 200});
        $(".userCategory").chosen({no_results_text: "no categories", min_width: 200});
        $(".restPat").chosen({no_results_text: "no categories", min_width: 200});
        $("#tabs").tabs().addClass( "ui-tabs-vertical ui-helper-clearfix");
        $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
        
        $('div.alert').on('click', function(ev){
            ev.preventDefault();
            $(this).hide();
        });
    });
   
    $('a.save-access').on('click', function(ev){
        ev.preventDefault();
        var data = {email: $(ev.currentTarget).data('email'),
            components: JSON.stringify($(ev.currentTarget).parents(".row-user").first().find('.components').val()),
            userType:$(ev.currentTarget).parents(".row-user").first().find('.userType').val(),
            categories: JSON.stringify($(ev.currentTarget).parents(".row-user").first().find('.userCategory').val())};
        $.ajax("/admin/access/saveUser", {
            data: data,
            type: "POST",
            dataType: "json",
            success: function(result){
                showMessage('success', result.message);
            },
            error: function(xhr){
                showMessage('error', $.parseJSON(xhr.responseText).message);
            }
        });
        
    });
  
    
    function showMessage(status, message){
        var html = message+'<a class="close" href="#">&times;</a>';
        if(status == 'success'){
            $('div.alert').removeClass('alert-error').addClass('alert-success').html(html).show();
        }else {
            $('div.alert').removeClass('alert-success').addClass('alert-error').html(html).show();
        }
    }
    	
        $('a.edit-input').on('click',function(ev)
        	     {
        	var id;
        	var queryName;
        	$(this).parent().find('input').each(function()
        	      {
        	 
        	queryName=$(this).val();
        	id=$(this).id;
     
        	$(this).attr('disabled',false);
        	
        	      });
        	$(this).attr('disabled',true);
        	     });
        
        
        $('a.save-category').on('click',function(ev)
        		{
        	
        	$(this).parents(".row-category").first().find('input').each(function()
        	      {
        		  $(this).attr('disabled',true);
        		  $('a.edit-input').attr('disabled',false);

        	      });
      	ev.preventDefault();
        	var data = {queryName:$(ev.currentTarget).parents(".row-category").first().find('input').data("name"),
        			categoryName:$(ev.currentTarget).parents(".row-category").first().find('input').val(),
                    components: JSON.stringify($(ev.currentTarget).parents(".row-category").first().find('.components').val())};
             $.ajax("/admin/access/saveCategory", {
                data: data,
                type: "POST",
                dataType: "json",
                success: function(result){
                    showMessage(result.status, result.message);
                },
                error: function(xhr){
                    showMessage('error', $.parseJSON(xhr.responseText).message);
                }
            });
       	});
   
   $('a.delete-category').on('click',function(ev)
        		{
		var queryName;
	   queryName=$(ev.currentTarget).parents(".row-category").first().find('input').data("name");
      	ev.preventDefault();
      	if(confirm("Are you sure you want to delete the category "+queryName+" ?")){	
      	var data = {queryName:queryName};
             $.ajax("/admin/access/deleteCategory", {
                data: data,
                type: "POST",
                dataType: "json",
                success: function(result){
                    showMessage('success', result.message);
                    location.reload(false);
                },
                error: function(xhr){
                    showMessage('error', $.parseJSON(xhr.responseText).message);
                    $(this).parents(".row-category").first().fadeOut('slow', function() {$(this).remove()});
                }
            });
      	}

        		
        		});
   
   
        $('#addCategory').on('click',function(){

        		var name=prompt("Enter the Name of the category");   	
        		if(name!=null){
        		var data = {categoryName: name,
                        components:JSON.stringify(null)};
                console.log(data);
       
        		$.ajax("/admin/access/addCategory", {
                     data: data,
                     type: "POST",
                     dataType: "json",
                     success: function(result){
                    		 showMessage(result.status, result.message);
                    		if(result.status==="success")
                    			location.reload(false);
                     },
                     error: function(xhr){
                         showMessage('error', $.parseJSON(xhr.responseText).message);
                         
                     }
               
        		});
        		}
        });
  $('#add-component').on('click',function(){
        	
        		var name=prompt("Enter the name of the component");   	
        		if(name!=null&&name!=""){
        		var url=prompt("Enter the URL of the component");
        		
        		if(url!=null&&url!=""){	
        		var data = {name: name,
        		URL:url,
        		active:"1"		
        		};
                console.log(data);
    
        		$.ajax("/admin/access/addComponent", {
                     data: data,
                     type: "POST",	
                     dataType: "json",
                     success: function(result){
                         showMessage(result.status, result.message);
                         if(result.status==="success")
                         location.reload(false);
                     },
                     error: function(xhr){
                         showMessage('error', $.parseJSON(xhr.responseText).message);
                         
                     }
               
        		});
        		}
        		}
       		
        });
  
  var name,url, restpattern;
  $('button.save-component').on('click',function(ev)
      		{
      		name=$(ev.currentTarget).parent().parent().find('input.name').val();
      		url=$(ev.currentTarget).parent().parent().find('input.url').val()
      		restpattern=JSON.stringify($(ev.currentTarget).parents(".row-component").first().find('.restPat').val());
      		var queryName=$(ev.currentTarget).parent().parent().find('input.name').data("name");              		
      		var data={query : queryName,
      				update :"NAME",
      				name : name,
      				URL : url,
      				componentRestPatterns: restpattern,
      				active:$(ev.currentTarget).parent().parent().find('input.active-state').is(":checked") ? '1' : '0'};
      		
      		
      		$.ajax("/admin/access/updateComponent", {
                  data: data,
                  type: "POST",
                  dataType: "json",
                  success: function(result){
                      showMessage(result.status, result.message);
                  },
                  error: function(xhr){
                      showMessage('error', $.parseJSON(xhr.responseText).message);
                  }
              });

      	      });
  $('a.delete-component').on('click',function(ev)
  		{
	  var queryName=$(ev.currentTarget).parent().parent().find('input.name').data("name");
	ev.preventDefault();
	if(confirm("Are you sure you want to delete the Component "+queryName+" ?")){	
	var data = {queryName:queryName};
      console.log(data);
       $.ajax("/admin/access/deleteComponent", {
          data: data,
          type: "POST",
          dataType: "json",
          success: function(result){
              showMessage(result.status, result.message);
              location.reload(false);
          },
          error: function(xhr){
              showMessage('error', $.parseJSON(xhr.responseText).message);
              $(this).parents(".row-category").first().fadeOut('slow', function() {$(this).remove()});
          }
      });
	}

  		
  		});
  
</script>