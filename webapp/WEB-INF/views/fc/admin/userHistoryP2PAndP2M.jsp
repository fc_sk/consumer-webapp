<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
		<%@page
			import="com.freecharge.customercare.controller.CustomerTrailController"%>
		<%@page
			import="com.freecharge.customercare.controller.CustomerTrailController"%>
		<%@include file="/WEB-INF/includes/taglibs.jsp"%>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<div class="customer-trail">
			<div class="cs-display">

				<div class="row">
					<h1>
						<span class="label label-info"><spring:message
								code="p2pandp2m.history" text="#p2pandp2m.history#" /></span>
					</h1>
				</div>
				<hr>

				<section class="unit">

					<div class="layer">
						<c:if test="${not empty p2PAndP2MTransactionList}">
						<ul>
									<li><em><spring:message code="name"
												text="#name#"/>:</em>${p2PAndP2MTransactionList[0].srcParty.name}</li>
									<li><em><spring:message code="email"
												text="#email#"/>:</em>${p2PAndP2MTransactionList[0].srcParty.emailId}</li>
									<li><em><spring:message code="mobile"
												text="#mobile#"/>:</em>${p2PAndP2MTransactionList[0].srcParty.mobileNumber}</li>
															
						</ul>				
						<table class="table table-bordered table-hover">
							<thead>
								<!-- <tr>
									<th colspan=4>
									</th>
									<th colspan=3><spring:message code="other.party"
											text="#other.party#" />
									</th>
									<th colspan=3>
									</th>
								</tr> -->
								<tr>
									<th><spring:message code="transaction.id"
											text="#transaction.id#" /></th>
									<th><spring:message code="transaction.status"
											text="#transaction.status#" /></th>
									<th><spring:message code="transaction.date"
											text="#transaction.date#" /></th>		
									<th><spring:message code="transaction.amount"
											text="#transaction.amount#" /></th>
									<th><spring:message code="transaction.sourceName"
											text="#transaction.sourceName#" /></th>
									<th><spring:message code="transaction.sourceMobile"
											text="#transaction.sourceMobile#" /></th>
									<th><spring:message code="transaction.destName"
											text="#transaction.destName#" /></th>
									<th><spring:message code="transaction.destMobile"
											text="#transaction.destMobile#" /></th>
									<th><spring:message code="transaction.receiverType"
											text="#transaction.receiverType#" /></th>
									<th><spring:message code="request.type"
											text="#request.type#" /></th>
									<th><spring:message code="transaction.result"
											text="#transaction.result#" /></th>
								</tr>
							</thead>
							<tbody class="customer-trail-rows">
									
							<c:forEach var="transaction" items="${p2PAndP2MTransactionList}">
								<tr>
									<td><a href="/admin/customertrail/getcustomertrail?orderId=${transaction.txnId}"><c:out value="${transaction.txnId}"/></a></td>
									<%--<td>${transaction.txnId}</td>  --%>
									<td>${transaction.txnStatus}</td>
									<td>${transaction.txnDate}</td>
									<td>${transaction.txnAmount}</td> 
									<td>${transaction.srcParty.name}</td>
									<td>${transaction.srcParty.mobileNumber}</td>
									<td>${transaction.destParty.name}</td>
									<td>${transaction.destParty.mobileNumber}</td>
									<td>${transaction.destParty.partyType}</td>
									<td>${transaction.txnType}</td>
									<td>${transaction.requestType}</td>
								</tr>
							</c:forEach>
							
							</tbody>	
						</table>
						</c:if>
					</div>
	
				</section>
			</div>



			<script type="text/javascript">
		        $(document).ready(function(){
		
		            var customerTrailRows = $('.customer-trail-rows').find('tr');
		            customerTrailRows.eq(0).show();
				   
		        }
	        </script>
		</div>
	</div>
</div>