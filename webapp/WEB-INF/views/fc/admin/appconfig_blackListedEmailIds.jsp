<%@ include file="/WEB-INF/includes/taglibs.jsp"%>
<style media="screen" type="text/css">
    .table
    {
        display:table;
    }

    .table-row
    {
        display:table-row;
    }

    .table-cell
    {
        display:table-cell;
        padding: 2px 12px;
    }
</style>

<div id="">
    <form action="/admin/appconfig/save/blacklistedEmailIds" method="post">
        <textarea rows="10" cols="300" name="emailIds">${emailIds}</textarea>
        <br/>
        <input type="submit" value="Save" />
    </form>
</div>