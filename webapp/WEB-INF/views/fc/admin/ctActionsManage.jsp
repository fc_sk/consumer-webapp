<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<head>
<link href="/content/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link href="${mt:keyValue("
	cssprefix1")}/chosen.css?v=${mt:keyValue("version.no")}
	" rel="stylesheet" type="text/css" />
	<style>
	input{
	   width: 140px;
	}
	.updatedatetime{
	   width: 180px;
	}
	</style>
</head>
 
<div class="container">
	<div class="alert" style="display: none"></div>
	<div class="span12">
		<h3>Control customer trail actions</h3>
		<c:if test="${not empty message}">
        <div class="alert alert-error">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <c:out value="${message}"/>      
        </div>
        </c:if>
		<p style="padding: 20px;">
		<a href="/admin/customertrail/actionscontrol/resetAll.htm" style="float:right;margin-right:-30px" class="btn btn-mini btn-info">RESET AVAIL LIMIT</a>
		</p>
		<table class="table table-bordered table-striped">
			<tr>
				<th><strong>Email</strong></th>
				<th><strong>Last max-limit updated</strong>
				<th><strong>Credits available</strong>
				<th><strong>Views available</strong>
				<th><strong>Max credits given</strong></th>
				<th><strong>Max views given</strong></th>
				<th>&nbsp;</th>
			</tr>
			<c:forEach var="ctActionsControlData"
				items="${ctActionsControlDataList}">
				<tr>
					<td>${ctActionsControlData.emailId}</td>
					<td><input type="text" name="updateTime" id="updateTime"
						class="updatedatetime" value="${ctActionsControlData.createdAt}"
						readonly></td>
					<td><input type="text" name="availableCredit" id="availableCredit"
                        class="availablecredit" size="20" value="${ctActionsControlData.availableCredits}"
                        readonly></td>
                    	
					<td><input type="text" name="availableLimit" id="availableLimit"
                        class="availablelimit" value="${ctActionsControlData.availableViewLimits}"
                        readonly></td>
					<td><input type="text" name="maxcredits" id="maxcredits"
						class="credits" value="${ctActionsControlData.maxCredits}">
					</td>
					<td><input type="text" name="viewLimits" id="viewLimits"
						class="views" value="${ctActionsControlData.viewLimits}">
					</td>
					<td><a href="#" data-email="${ctActionsControlData.emailId}"
						class="btn btn-info btn-mini save-access">Save</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</div>
<script type="text/javascript" src="${mt:keyValue("
	jsprefix1")}/chosen.jquery.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">

$(document).ready(function(){
    $('a.save-access').on('click', function(ev){
        ev.preventDefault();
        var data = {emailid: $(ev.currentTarget).data('email'),
        viewlimits: $(ev.currentTarget).parent().prev().find('.views').val(),
        maxcredits : $(this).parent().parent().find('.credits').val()};
        $.ajax("/admin/customertrail/actionscontrol/save", {
            data: data,
            type: "POST",
            dataType: "json",
            success: function(result){
            	if (result.status === 'success') {
            		$(ev.currentTarget).parent().parent().find('.updatedatetime').val(result.updatedDateTime);
            		$(ev.currentTarget).parent().parent().find('.availablecredit').val(result.creditsAvailable);
            		$(ev.currentTarget).parent().parent().find('.availablelimit').val(result.viewsAvailable);
            		alert(result.message);
            	} else {
            		alert(result.message);
            	}
            },
            error: function(e){
            }
        }); 
   });
});

</script>
