<%@page import="com.freecharge.admin.CustomerTrailDetails.EmailInputController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="createdOnDateValue" class="java.util.Date"/>
<div class="row">
    <div class="col-sm-3 col-md-3">
        <jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp"/>
    </div>

    <div class="col-sm-9 col-md-9">
        <c:if test="${not empty exception }">
            <div class="alert alert-error" >
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                ${exception}
            </div>
        </c:if>
		<h3>Lifeline Details</h3>
		<c:choose>
			<c:when test="${not empty lifelineInfo}">
				
					<div class="alert alert-info" >
		                 <strong>User Mobile Information</strong>
		            </div>
		            <div>
		            	<table class="table table-hover table-bordered">
				            <thead>
				                <tr>
				                    <th>Mobile</th>
				                    <th>OperatorID</th>
				                    <th>CircleId</th>
				                </tr>
				            </thead>
					            <tr>
					                <td>${lifelineInfo.userMobileInformation.mobile}</td>
					                <td>${lifelineInfo.userMobileInformation.operatorId}</td>
					                <td>${lifelineInfo.userMobileInformation.circleId}</td>
					            </tr>
				        </table>
		            </div>
		            <div class="alert alert-info" >
		                 <strong>User Point History</strong>
		            </div>
		            <div>
		            	<table class="table table-hover table-bordered">
				            <thead>
				                <tr>
				                    <th>UserId</th>
				                    <th>Points</th>
				                    <th>Created On</th>
				                    <th>Updated On</th>
				                    <th>Type</th>
				                    <th>ReferenceId</th>
				                </tr>
				            </thead>
				            <c:forEach var="userPointHistory" items="${lifelineInfo.pointHistoryList}">
					            <tr>
					                <td>${userPointHistory.userId}</td>
					                <td>${userPointHistory.points}</td>
					                <td>${userPointHistory.createdOn}</td>
					                <td>${userPointHistory.updatedOn}</td>
					                <td>${userPointHistory.type}</td>
					                <td>${userPointHistory.referenceId}</td>
					            </tr>
				            </c:forEach>
				        </table>
		            </div>
		            
		            <div class="alert alert-info" >
		                 <strong>Lifeline Order History</strong>
		            </div>
		            <div>
		            	<table class="table table-hover table-bordered">
				            <thead>
				                <tr>
				                    <th>OrderId</th>
				                    <th>Status</th>
				                    <th>Data</th>
				                    <th>Amount</th>
				                    <th>CreatedOn</th>
				                </tr>
				            </thead>
				            <c:forEach var="lifelineOrder" items="${lifelineInfo.dataUserRewardList}">
				            	<jsp:setProperty name="createdOnDateValue" property="time" value="${lifelineOrder.createdOn}"/>
					            <tr>
					                <td>${lifelineOrder.orderId}</td>
					                <td>${lifelineOrder.status}</td>
					                <td>${lifelineOrder.data}</td>
					                <td>${lifelineOrder.amount}</td>
					                <td>${createdOnDateValue}</td>
					            </tr>
				            </c:forEach>
				        </table>
		            </div>
			</c:when>
			<c:otherwise>
				<div class="alert alert-info" >
		                 <strong>No Lifeline Details Found For This Email</strong>
		        </div>
			</c:otherwise>
		</c:choose>
        
	</div>
</div>