<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@ include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
		<%@page
			import="com.freecharge.customercare.controller.CustomerTrailController"%>
		<%@page
			import="com.freecharge.customercare.controller.CustomerTrailController"%>
		<%@include file="/WEB-INF/includes/taglibs.jsp"%>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<div class="customer-trail">
			<div class="cs-display">

				<div class="row">
					<div class="col-md-4 col-sm-4"></div>
				</div>
				<hr>

				<section class="unit">
					<div>
						<h1>
							<span class="label label-info"><spring:message
									code="p2pSplitTxn.information" text="#p2pSplitTxn.information#" /></span>
						</h1>
					</div>

					<c:if test="${not empty splitView}">

						<div class="layer">


							<h1>
								<span class="label label-info"><spring:message
										code="initiator.information" text="#initiator.information#" /></span>
							</h1>
							<ul>

								<li><em><spring:message code="partyName"
											text="#partyName#" />:</em> ${splitView.name}</li>
								<li><em><spring:message code="partyEmail"
											text="#partyEmail#" />:</em> <a
									href="/admin/customertrail/onecheck/searchbyemail/result.htm?emailid=${splitView.email}"><c:out
											value="${splitView.email}" /></a></li>
								<li><em><spring:message code="partyContact"
											text="#partyContact#" />:</em> ${splitView.contact}</li>
								<li><em><spring:message code="partyOrderId"
											text="#partyOrderId#" />:</em> ${splitView.orderId}</li>
								<li><em><spring:message code="status"
											text="#status#" />:</em>			
											<c:choose>
												<c:when test="${splitView.status == 'SUCCESS'}">
													<span class="label label-success"><spring:message
															code="txn.successful" text="${splitView.status}" /></span>
												</c:when>
												<c:otherwise>
													<span class="label label-important"><spring:message
															code="txn.unsuccessful"
															text="${splitView.status}" /> </span>
												</c:otherwise>
											</c:choose>
											</li>
								<li><em><spring:message code="partyTimeStamp"
											text="#partyTimeStamp#" />:</em> ${splitView.timeStamp}</li>
								<li><em><spring:message code="transactionDomain"
											text="#transactionDomain#" />:</em> ${splitView.domain}</li>
								<li><em><spring:message code="amount" text="#amount#" />:</em>
									${splitView.amount}</li>
								<li><em><spring:message code="initiatorshare"
											text="#initiatorshare#" />:</em> ${splitView.share}
							</ul>

						</div>
						<div class="layer">
							<h1>
								<span class="label label-info"><spring:message
										code="split.information" text="#split.information#" /></span>
							</h1>

							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th><spring:message code="transactionId" text="#transactionId#" /></th>
										<th><spring:message code="status"
												text="#status#" /></th>
										<th><spring:message code="errorCode" text="#errorCode#" />
										</th>
										<th><spring:message code="transactionAmount"
												text="#transactionAmount#" /></th>
										<th><spring:message code="transactionTime"
												text="#transactionTime#" /></th>
										<!-- <th><spring:message code="otherPartyType"
												text="#otherPartyType#" /></th> -->
										<th><spring:message code="otherPartyName"
												text="#otherPartyName#" /></th>
										<th><spring:message code="otherPartyEmail"
												text="#otherPartyEmail#" /></th>
										<th><spring:message code="otherPartyContact"
												text="#otherPartyContact#" /></th>
										<th><spring:message code="requestType"
												text="#requestType#" /></th>
									</tr>
								</thead>
								<tbody class="customer-trail-rows">
									<c:if test="${not empty splitView.participantViewList}">
										<c:forEach var="participantView"
											items="${splitView.participantViewList}">
											<tr>
												<td><a href="/admin/customertrail/getcustomertrail?orderId=${participantView.transactionId}"><c:out value="${participantView.transactionId}"/></a></td>
												<td><c:choose>
														<c:when test="${participantView.status == 'SUCCESS'}">
															<span class="label label-success"><spring:message
																	code="txn.successful" text="${participantView.status}" /></span>
														</c:when>
														<c:otherwise>
															<span class="label label-important"><spring:message
																	code="txn.unsuccessful"
																	text="${participantView.status}" /> </span>
														</c:otherwise>
													</c:choose></td>
												<td>${participantView.errorCode}</td>
												<td>${participantView.txnAmount}</td>
												<td>${participantView.transactionTime}</td>
											<!--	<td>${participantView.otherPartyType}</td> -->
												<td>${participantView.otherPartyName}</td>
												<td><a
													href="/admin/customertrail/onecheck/searchbyemail/result.htm?emailid=${participantView.otherPartyEmail}"><c:out
															value="${participantView.otherPartyEmail}" /></a></td>
												<td>${participantView.otherPartyContact}</td>
												<td>${participantView.requestType}</td>
											</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
						</div>
					</c:if>
				</section>
				
				<section class="unit">
	        		<div class="layer">
	   						<jsp:include page="rewardhistory.jsp" />
					</div>
        		</section> 
				<script type="text/javascript">
		        $(document).ready(function(){
		
		            var customerTrailRows = $('.customer-trail-rows').find('tr');
		            customerTrailRows.eq(0).show();
				   
		        }
	        </script>
			</div>
		</div>
	</div>
</div>