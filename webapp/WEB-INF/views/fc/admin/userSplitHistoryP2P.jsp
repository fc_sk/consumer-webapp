<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">
   <head>
	 	<script type="text/javascript">
		        $(document).ready(function(){
		            var customerTrailRows = $('.customer-trail-rows').find('tr');
		            customerTrailRows.eq(0).show();
		        }
		</script>
  	</head>
	<body>
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<jsp:include page="customerTrailSearch.jsp" />
			</div>
			<div class="col-md-9 col-sm-9">
				<div class="customer-trail">
					<div class="cs-display">
						<div class="row">
							<c:choose>
								<c:when test="${shopoflag}">
									<h1>
										<span class="label label-info"><spring:message
											code="shopo.history" text="#shopo.history#" /></span>
									</h1>
								</c:when>
								<c:otherwise>
									<h1>
										<span class="label label-info"><spring:message
											code="p2psplit.history" text="#p2psplit.history#" /></span>
									</h1>
								</c:otherwise>
							</c:choose>
						</div>
						<hr>
						<section class="unit">
							<div class="layer">
								<c:if test="${not empty p2pSplitDetailsList}">
								<ul>
									<li><em><spring:message code="name"
												text="#name#"/>:</em>${p2pSplitDetailsList[0].name}</li>
									<li><em><spring:message code="email"
												text="#email#"/>:</em>${p2pSplitDetailsList[0].email}</li>
									<li><em><spring:message code="mobile"
												text="#mobile#"/>:</em>${p2pSplitDetailsList[0].contact}</li>
								</ul>				
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th><spring:message code="transaction.id"
													text="#transaction.id#" /></th>
											<th><spring:message code="transaction.status"
													text="#transaction.status#" /></th>
											<th><spring:message code="transaction.date"
													text="#transaction.date#" /></th>		
											<th><spring:message code="transaction.amount"
													text="#transaction.amount#" /></th>
											<th><spring:message code="transaction.sourceName"
													text="#transaction.sourceName#" /></th>
											<th><spring:message code="transaction.sourceMobile"
													text="#transaction.sourceMobile#" /></th>
											<th><spring:message code="transaction.destName"
													text="#transaction.destName#" /></th>
											<th><spring:message code="transaction.destMobile"
													text="#transaction.destMobile#" /></th>
											<th><spring:message code="transaction.receiverType"
													text="#transaction.receiverType#" /></th>
											<th><spring:message code="request.type"
													text="#request.type#" /></th>
											<th><spring:message code="transaction.result"
													text="#transaction.result#" /></th>
										</tr>
									</thead>
									<tbody class="customer-trail-rows">
											
									<c:forEach var="transaction" items="${p2pSplitDetailsList}">
										<tr>
											<td><a href="/admin/customertrail/getcustomertrail?orderId=${transaction.referenceId}"><c:out value="${transaction.orderId}"/></a></td>
											<td>${transaction.status}</td>
											<td>${transaction.timeStamp}</td>
											<td>${transaction.txnAmount}</td> 
											<td>${transaction.name}</td>
											<td>${transaction.contact}</td>
											<td>${transaction.otherPartyName}</td>
											<td>${transaction.otherPartyContact}</td>
											<td>${transaction.otherPartyType}</td>
											<td>${transaction.txnType}</td>
											<td>${transaction.requestType}</td>
										</tr>
									</c:forEach>
									
									</tbody>	
								</table>
								</c:if>
							</div>
			
						</section>
					</div>
				</div>
			</div>
		</div>
	</body>
