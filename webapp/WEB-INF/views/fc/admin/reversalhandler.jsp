<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<c:if test="${not empty messages }">
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		${messages}
	</div>
</c:if>
<div>
	<form:form method="post"
		action="/admin/rechargeRequestStatus/reversalHandler.htm"
		enctype="multipart/form-data">

		<center>
			<h3>Reversal case handler for changing recharge status</h3>
		</center>
		<h5>Instructions :</h5>
		<ul>
			<li>Use either TextArea or File Upload at a time to upload
				request Id/OrderId list.
				 <ul>
					<li><b>Input via Text Area:</b>  <br> 
					    <ul>
							<li>Each line contains comma separated list of  'Request Id/OrderId' , 'AG Ref Id', 'Recharge Reversal Status [SUCCESS/ FAILED]' in fixed sequence  eg: <b>123xxReqIdOrOrderID,123xxAGRefId,SUCCESS</b> </li>
							<li>For empty 'AG Ref Id' leave data blank. eg: <b>123xxReqIdOrOrderID,,FAILED</b>  </li>
						</ul>
					</li>
			
					<li><b>Input via Uploaded CSV file:</b>  <br> 
					    <ul>
							<li>Each line contains 'Request Id/OrderId' , 'AG Ref Id', 'Recharge Reversal Status [SUCCESS/ FAILED]' in first, second and third column respectively</li>
						</ul>
					</li>
				</ul> 
			</li>	
			<li>Must select a aggregator name.</li>
			<li>Right only for Prepaid and Postpaid case.</li>
		</ul>
		<br>
		<h6>
			<span class="badge">Step 1 </span> &nbsp;&nbsp;<i> Select Aggregator</i>
		</h6>
		
		 <table class="table table-striped table-bordered"
			style="max-width: 69%">
			<tr>
				<td><select name="aggregatorName">
				<c:forEach items="${aggNameList}" var="aggName">
					<c:choose>
						<c:when test="${aggName eq 'Select aggregator'}">
							<option value="" selected>${aggName}</option>
						</c:when>
						<c:otherwise>
							<option value="${aggName}">${aggName}</option>
						</c:otherwise>
					</c:choose>
					</c:forEach>
				</select></td>
			</tr>
		</table>

		<h6>
			<span class="badge">Step 2 </span>&nbsp;&nbsp;<i> Enter list of
				OrderId/RequestId in TextArea OR upload a CSV File (Each line contains requestId/OrderId and recharge reversal status as single row element)</i>
		</h6>
		<table class="table table-striped table-bordered"
			style="max-width: 69%">
			<tr>
				<td><textarea id="inputIds" name="inputIds" style="width: 350px;" >${inputIds}</textarea>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="label label-warning">OR</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="file" class="input" id="fileInputIds"
					name="fileInputIds"></td>
			</tr>
		</table>

		<br>
		<input type="submit" value="Do recharge reversal" />
	</form:form>

	<div style="display: none;">
		<br>
		<h1>
			<span class="label label-success">Reversal Success Status :</span>
		</h1>
		<c:if test="${not empty successCount }">
			<span class="label label-success">Success count : <c:out
					value="${successCount}" /></span>
		</c:if>
		<br />
		<c:forEach var="successStatusChangeRequestId"
			items="${successStatusChangeList}">
			<c:forEach var="successStatusChange"
				items="${successStatusChangeRequestId}">
				<c:out value="${successStatusChange}," />
			</c:forEach>
			<br>
		</c:forEach>
		<br>
		<h1>
			<span class="label label-important">Reversal Failure status :</span>
		</h1>
		<c:if test="${not empty failureCount }">
			<span class="label label-important">Failure count : <c:out
					value="${failureCount}" /></span>
		</c:if>
		</br>
		<c:forEach var="failureStatusChangeRequestId"
			items="${failureStatusChangeList}">
			<c:forEach var="failureStatusChange"
				items="${failureStatusChangeRequestId}">
				<c:out value="${failureStatusChange}," />
			</c:forEach>
			<br>
		</c:forEach>
		<br>
	</div>
	<a href="javascript:void(0);" class="more-info">Show Status</a>
</div>



<script type="text/javascript">
    $(document).ready(function() {

        $(".more-info").on("click", function() {

            if ($(this).prev("div").is(':visible')) {
                $(this).prev("div").slideUp(200);
                $(this).text("Show Status");
            } else {
                $(this).prev("div").slideDown(200);
                $(this).text("Show less");
            }
        });
    });
</script>