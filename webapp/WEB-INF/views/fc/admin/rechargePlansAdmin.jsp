<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- .mainContainer Starts  -->
<div class="mainContainer"  >
    <div class="pageWidth clearfix form">
    	   <div class="manage_plans boxDesign" >
       		 <h1>Manage Recharge Plans</h1>
             <form:form  name="rechargeplansform" action="#" method="POST" cssClass="form" id="rechargeplansform" >	
                <div class="manage_plans_Form">
                	<table style="width:100%;">
                		<tr>
                			<td>
                				<p> *Name</p>
                    			<select class="validate[required] select1 softNormText"  name="name" id="name">
                    				
			                        <c:forEach var="name" items="${ALL_NAMES}">
			                        	<option value="${name}"  >${name}</option>
			                        </c:forEach>
			                    </select>
			                    <c:if test="${not empty RECHARGE_PLAN}">
			                    	<script type="text/javascript">
			                    		$("#name").val("${RECHARGE_PLAN.name}");
			                    	</script> 
			                    </c:if>
                			</td>
                			<td>
                				<p> *Product</p>
			                    <select class="validate[required] select1 softNormText" name="product" id="product">
			                        <option value="" selected >Choose one</option>
			                        <c:forEach var="product" items="${ALL_PRODUCTS}">
			                        	<option value="${product.productMasterId}"  >${product.productName}</option>
			                        </c:forEach>
			                    </select>
			                    <c:if test="${not empty RECHARGE_PLAN}">
			                    	<script type="text/javascript">
			                    		$("#product").val(${RECHARGE_PLAN.productType});
			                    	</script> 
			                    </c:if>
                			</td>
                			<td>
                				<p> *Operator</p>
			                    <select class="validate[required] select1 softNormText" name="operator" id="operator">
			                        <option value="" selected >Choose one</option>
			                        <c:forEach var="operator" items="${ALL_OPERATORS}" >
	                                    <option value="${operator.operatorMasterId}"  >${operator.operatorCode}</option>
	                                </c:forEach>
			                    </select>
			                    <c:if test="${not empty RECHARGE_PLAN}">
			                    	<script type="text/javascript">
			                    		$("#operator").val(${RECHARGE_PLAN.operatorMasterId});
			                    	</script> 
			                    </c:if>
                			</td>
                		</tr>
                		<tr>
                			<td>
                				<p> *Circle</p>
			                    <select class="validate[required] select1 softNormText" name="circle" id="circle">
			                        <option value="" selected >Choose one</option>
			                        <option value="all">All Circles (Warning)</option>
			                        <c:forEach var="circle" items="${ALL_CIRCLES}">
			                        	<option value="${circle.circleMasterId}"  >${circle.name}</option>
			                        </c:forEach>
			                    </select>
			                    <c:if test="${not empty RECHARGE_PLAN}">
			                    	<script type="text/javascript">
			                    		$("#circle").val('${RECHARGE_PLAN.circleMasterId}');
			                    	</script> 
			                    </c:if>
                			</td>
                			<td>
                				<p> *Denomination</p>
			                    <select class="validate[required] select1 softNormText" name="denomination" id="denomination" onchange="showAmount(this.value)">
			                        <c:forEach var="denomination" items="${ALL_DENOMINATION_TYPES}">
			                        	<option value="${denomination.name}"  >${denomination.name}</option>
			                        </c:forEach>
			                    </select>
			                    <c:if test="${not empty RECHARGE_PLAN}">
			                    	<script type="text/javascript">
			                    		$("#denomination").val(${RECHARGE_PLAN.denominationType});
			                    	</script> 
			                    </c:if>
                			</td>
                			<td>
                				<div id="FixedAmountId" >
			                    	<p> *Amount</p>
			                    	<input maxlength="40" type="text" class="validate[required]  input1 softNormText"  name="amount" id="amount" value="${RECHARGE_PLAN.amount}">
			                    </div>
			                    <div id="VariableAmountId" style="display: none;">
				                    <p> Min Amount</p>
				                    <input maxlength="40" type="text" class="validate[required] input1 softNormText"  name="min_amount" id="min_amount" value="${RECHARGE_PLAN.minAmount}">
				                    <p> Max Amount</p>
				                    <input maxlength="40" type="text" class="validate[required] input1 softNormText"  name="max_amount" id="max_amount" value="${RECHARGE_PLAN.maxAmount}">
			                    </div>
                			</td>
                		</tr>
                		<tr>
                			<td>
                				<p> *Talktime Amount</p>
                    			<input maxlength="40" type="text" class="validate[required] input1 softNormText"  name="talktime" id="talktime" value="${RECHARGE_PLAN.talktime}">
                			</td>
                			<td>
                				 <p> *Validity Days </p>
                    			<input maxlength="40" type="text" class="validate[required] input1 softNormText"  name="validity" id="validity" value="${RECHARGE_PLAN.validity}">	
                			</td>
                			<td>
                				<p> *Description</p>
                    			<textarea style="height:40px;" name="description" id="description" cols="1" rows="5" class="validate[required] input1  softNormText">${RECHARGE_PLAN.description}</textarea>
                			</td>
                		</tr>
                	</table>
                	<c:choose>
                		<c:when test="${not empty RECHARGE_PLAN}">
                			<input type="hidden" name="recharge_plan_id" id="recharge_plan_id" value="${RECHARGE_PLAN.rechargePlanId}" />
                			<p><input  type="button" class="regularButton" value="Save" onclick="saveRechargePlan()"></p>
                		</c:when>
                		<c:otherwise>
                			<input type="hidden" name="recharge_plan_id" id="recharge_plan_id" value="-1" />
                			<p><input  type="button" class="form_button" value="Submit" onclick="saveRechargePlan()"></p>
                		</c:otherwise>
                	</c:choose>
                	<p><br/><span id="rechargeplansStatus" style="color: red;"></span></p>
                </div>
             </form:form> <%-- /frm_rechargeplans --%>
           </div> <%-- /.manage_plans --%>
           <HR>
           <!-- ---------------Upload bulk ---------------------->
           <h5>Bulk plan upload</h5>
           <h6>(Each row : Name,OperatorId,CircleId,amount,talktime,validity,description,productId)</h6>
           <div class="manage_plans boxDesign" >
           		<form:form method="post" action="/admin/rechargeplans/uploadfile.htm" enctype="multipart/form-data">
					<input type="file" class="input" id="planuploadfile" name="planuploadfile"><br>
					<input type="submit" class="btn btn-mini btn-success" value="Uplad plans" />
				</form:form>
           </div>
           <HR>
           <!-- -------------------End upload bulk------------ -->
          
          <div  class="view_plans boxDesign" >
            <h1>View Recharge Plans</h1>
            <form:form  name="rechargeplanslistform"  method="GET" cssClass="form" id="rechargeplanslistform" >	
                <table class="recharge_plans_form_table" >
                    <tr>
                        <td>
                            <p>Operator</p>
                            <select class="select1 softNormText" name="operator"  id="operator_l">
                               <option value="" selected >Choose one</option>
                               <c:forEach var="operator" items="${ALL_OPERATORS}" >
                                    <option value="${operator.operatorMasterId}"  >${operator.operatorCode}</option>
                                </c:forEach>
                            </select>
                            <c:if test="${not empty param.operator}">
                                    <script type="text/javascript">
                                        $("#operator_l").val('${param.operator}');
                                    </script> 
                             </c:if>
                        </td>
                        <td>
                            <p>Circle</p>
                            <select class="select1 softNormText" name="circle" id="circle_l">
                                <option value="" selected >Choose one</option>
                                <option value="all">All Circles</option>
                                <c:forEach var="circle" items="${ALL_CIRCLES}">
                                    <option value="${circle.circleMasterId}"  >${circle.name}</option>
                                </c:forEach>
                            </select>
                             <c:if test="${not empty param.circle}">
                                    <script type="text/javascript">
                                        $("#circle_l").val('${param.circle}');
                                    </script> 
                             </c:if>
                        </td>
                        <td valign="bottom" >
                           <input  type="submit" class="form_button" value="Submit" >
                        </td>
                    </tr>
                </table>
            </form:form>   <%-- /rechargeplanslistform --%>
            <br/>
            <hr/>
            <br/>
            
            <div id="rechargePlansList" class="plan_result"  >
               <jsp:include page="list.jsp"></jsp:include>
            </div>    <%-- /#rechargePlansList --%>   
		  </div>   <%-- /.view_plans --%>
           
    </div>  
    
</div>
<!-- .mainContainer Ends  -->
<script type="text/javascript">
$(document).ready(function(){
	jQuery("#rechargeplansform").validationEngine({
	    ajaxFormValidation: false ,
	      scroll:false
	   });
});

function saveRechargePlan(){
	var flag = $('#rechargeplansform').validationEngine('validate');
	if(flag){
	var product =$('#product').val();
	var operator = $('#operator').val();
	var circle = $('#circle').val();
	var name = $('#name').val();
	var denomination = $('#denomination').val();
	var amount = $('#amount').val();
	var min_amount = $('#min_amount').val();
	var max_amount = $('#max_amount').val();
	var talktime = $('#talktime').val();
	var validity = $('#validity').val();
	var description = $('#description').val();
	var recharge_plan_id = $('#recharge_plan_id').val();
	
	$.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    } }); 
	var url = '/admin/rechargeplans/save';
	
	var formData = 'product='+product+'&operator='+operator+'&circle='+circle+'&name='+name+'&denomination='+denomination+'&amount='+amount+'&min_amount='+min_amount+'&max_amount='+max_amount+'&talktime='+talktime+'&validity='+validity+'&description='+description+'&recharge_plan_id='+recharge_plan_id;
	$.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: "html",
        cache: false,
        timeout:1800000,
        success: function (responseText) {
        	var responseObj = {};
        	try {
        		responseObj = jQuery.parseJSON(responseText);
        	} catch(e) {
        		
        	}
        	
        	if(responseObj.STATUS == 'Error'){
            	$('#rechargeplansStatus').html(responseObj.ERROR_MESSAGE);
                
            } else if(responseObj.STATUS == 'Updated'){
            	alert("Recharge plan updated successfully.");
            	document.location.href = window.location.href.split('?')[0] + "?operator=" + operator + "&circle=" + circle;
            } else{
            	document.rechargeplansform.reset();
                $('#rechargePlansList').html(responseText);
            }
        	$.unblockUI();
        }
  	}); 
	}
}


function updateStatus(recharge_plan_id, status){
	
	$.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    } }); 
	var url = '/admin/rechargeplans/updateStatus';
	
	var formData = 'status='+status+'&recharge_plan_id='+recharge_plan_id;
	$.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: "html",
        cache: false,
        timeout:1800000,
        success: function (responseText) {
        	var responseObj = jQuery.parseJSON(responseText);
        	if(responseObj.STATUS == 'Error'){
            	$('#rechargeplansStatus').html(responseObj.ERROR_MESSAGE);
                
            } else if(responseObj.STATUS == 'Updated'){
            	var url = '/admin/rechargeplans/updateSticky';
                
                var formData = 'sticky=true&recharge_plan_id='+recharge_plan_id;
                $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    dataType: "html",
                    cache: false,
                    timeout:1800000,
                    success: function (responseText) {
                        var responseObj = jQuery.parseJSON(responseText);
                        if(responseObj.STATUS == 'Error'){
                            $('#rechargeplansSticky').html(responseObj.ERROR_MESSAGE);
                            
                        } else if(responseObj.STATUS == 'Updated'){
                            alert("Recharge plan updated successfully.");
                            $("#rechargeplanslistform").submit();
                        }
                        $.unblockUI();
                    }
                });
            }
        }
  	}); 
}


function updateSticky(recharge_plan_id, sticky){
    
    $.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    } }); 
    var url = '/admin/rechargeplans/updateSticky';
    
    var formData = 'sticky='+sticky+'&recharge_plan_id='+recharge_plan_id;
    $.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: "html",
        cache: false,
        timeout:1800000,
        success: function (responseText) {
            var responseObj = jQuery.parseJSON(responseText);
            if(responseObj.STATUS == 'Error'){
                $('#rechargeplansSticky').html(responseObj.ERROR_MESSAGE);
                
            } else if(responseObj.STATUS == 'Updated'){
                alert("Recharge plan updated successfully.");
                $("#rechargeplanslistform").submit();
            } 
            $.unblockUI();
            
        }
    }); 
    
}


function showAmount(type) {
	$('#FixedAmountId').hide();
	$('#VariableAmountId').hide();
	if(type == 'FIXED_DENOMINATION') {
		$('#FixedAmountId').show();
	} else if (type == 'VARIABLE_DENOMINATION') {
		$('#VariableAmountId').show();
	} 
}
</script>