<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${not empty errorMessage}">
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <c:out value="${errorMessage}" /><br>
    </div>
</c:if>
<c:if test="${not empty limitExceedMessage}">
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <c:out value="${limitExceedMessage}" /><br>
    </div>
</c:if>
<c:if test="${not empty notEvenMessage}">
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <c:out value="${notEvenMessage}" /><br>
    </div>
</c:if>
<c:if test="${rowcount != 0 and rowcount != null}">
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<c:out value="${successMessage}" /><br>
		<c:out value="${rowCountMessage}" />
	</div>
</c:if>
<c:if test="${rowcount == 0}">
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<c:out value="${message}" /><br>
		<c:out value="${rowCountMessage}" />
	</div>
</c:if>

<br>
<br>
<div id="updateSingleEmailId" class="sectionDiv">
	<h4>Update Email Address</h4>
	<form name="submitEmailForm" method="POST"
		action="/admin/emailupdate/singleemail.htm" style="display: inline;">
		<b><spring:message code="old.email.id" text="#old.email.id#" /> <a
			href="#" rel="tooltip" class="tip" title="Enter old email id"
			data-original-title="Help">[?]</a></b><br /> <input type="text"
			name="oldemailid" value=""></input><br> <b><spring:message
				code="new.email.id" text="#new.email.id#" /> <a href="#"
			rel="tooltip" class="tip" title="Enter new enail id"
			data-original-title="Help">[?]</a></b><br /> <input type="text"
			name="newemailid" value=""></input><br> <input type="submit"
			value="Update" class="btn btn-primary" /><br />
	</form>
</div>
<br>
<br>
<div id="updateEmailIds" class="sectionDiv">
	<h4>Enter emailAddresses to update</h4>
	<a href="#" rel="tooltip" class="tip"
		title="Enter email address as: oldemailid,newemmailid[eg:abc@example.com,xyz@gmail.com]"
		data-original-title="Help">[?]</a></b><br />
	<form name="submitEmailidsForm" method="POST"
		action="/admin/emailupdate/bulkemailids.htm" style="display: inline;">
		<textarea id="emailids" name="emailids"></textarea>
		<br /> <input type="submit" value="Update" class="btn btn-primary" />

	</form>
</div>
