<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<c:if test="${not empty msg}">
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		${msg}
	</div>
</c:if>
<div>
    <div>
           <center><h4>Credit encash bank details</h4></center>
    	   <div>
       		 <h5><i>User registerd email id :</i></h5>
             <form method="GET" action="/admin/tools/onecheck/bank/transfer/searchbyemail/result">
                	<div class="customer-trail-input">
                	<div class="clearfix">
        				<div class="column">
                			<h5>Email Id</h5>
                				<input type="text" name="emailid" value="${emailid}" style="width: 175px;">
                		</div>
                		<div class="column">	
                	<br/><br/><input type="submit" class="btn btn-default" value="Get">
                	</div>
                	</div>
                	</div>
             </form>  
  	       </div>  
  	       
  	        <div>
       		 <h5><i>User Entered email id :</i></h5>
             <form method="GET" action="/admin/tools/onecheck/bank/transfer/searchby/enteredemail">
                	<div class="customer-trail-input">
                	<div class="clearfix">
        				<div class="column">
                			<h5>Email Id</h5>
                				<input type="text" name="emailentered" value="${emailentered}" style="width: 175px;">
                		</div>
                		<div class="column">	
                	<br/><br/><input type="submit" class="btn btn-default" value="Get">
                	</div>
                	</div>
                	</div>
             </form>  
  	       </div>  
  	       <hr>
           <div>
           			<c:choose>
						<c:when test="${not empty creditEncashList}">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Email Id</th>
										<th>Bank Account No</th>
										<th>IFSC Code</th>
										<th>Beneficiary Name</th>
										<th>Transfer Status</th>
										<th>Transfer Date</th>
										<th>Failure Reason</th>
										<th>Reference Id</th>
										<th>Amount</th>
										<th>Utr number</th>
									</tr>
								</thead>
								<tbody>
								     <c:forEach var="creditEncash" items="${creditEncashList}">
										<tr>
											<td>${creditEncash['email_id']}</td>
											<td>${creditEncash['bank_account_no']}</td>
											<td>${creditEncash['ifsc_code']}</td>
											<td>${creditEncash['beneficiary_name']}</td>
											<td>${creditEncash['transfer_status']}</td>
											<td>${creditEncash['transfer_date']}</td>
											<td>${creditEncash['failure_reason']}</td>
											<td>${creditEncash['reference_id']}</td>
											<td>${creditEncash['amount']}</td>
											<td>${creditEncash['utr_number']}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
           </div>
    </div>  
</div>
