<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:if test="${not empty messages}">
        <div class="alert alert-error">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <ul>
            <c:forEach var="message" items="${messages}">
                <li><c:out value="${message}"/></li>
            </c:forEach>
            </ul>       
        </div>    
</c:if>

<div>
    <h4><font color="green">Reward coupons :</font></h4>
    <br>
        <form:form commandName="rewardCouponForm"
            action="/admin/reward-coupon/search/display" method="POST" name="rewardCouponForm"
            id="rewardCouponForm" class="form-inline">
            <label for="emailId"><b>Email Id</b></label>&nbsp;&nbsp;&nbsp;
            <form:input path="emailId" maxlength="50"/><font color="red"> *</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      
            <label for="campaignId"><b>Reward campaign</b></label>&nbsp;&nbsp;&nbsp;
            <form:select path="campaignId">
            	<form:option value="">Select campaign name</form:option>
            	<form:options items="${rewardCouponForm.rewardCouponMasterData.allRewardCouponCampaign}" itemLabel="name" itemValue="id" />
            </form:select><font color="red"> *</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" class="btn"><b>Get it</b></button>
       	</form:form>        
 </div>
 <HR>
 <div>
   <c:choose>
 		<c:when test="${not empty rewardCouponLists}">
 		    <h5>Reward coupons details :</h5>
 	   			Email Id : <i><c:out value='${emailId}'/></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 	   			Reward campaign : <i><c:out value='${rewardCampaign}'/></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 	   			Freefund code : <i><c:out value='${freefundCode}'/></i>
   <hr> 
 			<c:forEach var="rewardCouponList" items="${rewardCouponLists}">			
 				Order Id : <a href="/admin/customertrail/getcustomertrail?orderId=${rewardCouponList[0].orderId}"><i>${rewardCouponList[0].orderId}</i></a>	
 				<table class="table table-bordered table-hover">
 			    	<thead>
 			    		<tr>
 			    			<th>Coupon campaign</th>
 			    			<th>Coupon value</th>
 			    			<th>Coupon code</th>
 			    			<th>Coupon type</th>
 			    			<th>Price</th>
 			    			<th>Optin Type</th>
 			    			<th>Created on</th>
 			    			
 			    		</tr>
 			    	</thead>
 			    	<tbody>
 			    		<c:forEach var="rewardCoupon" items="${rewardCouponList}">
 			    			<tr>
 			    				<td>${rewardCoupon.campaignName}</td>
 			    				<td>${rewardCoupon.couponValue}</td>
 			    				<td>${rewardCoupon.couponCode}</td>
 			    				<td>${rewardCoupon.couponType}</td>
 			    				<td>${rewardCoupon.price}</td>
 			    				<td>${rewardCoupon.optinType}</td>
 			    				<td>${rewardCoupon.createdOn}</td>
 			    		    </tr>		
 			    		</c:forEach>
 			    	</tbody>
 				</table>	
 			</c:forEach>
   		</c:when>
   		<c:otherwise>
   		</c:otherwise>	
  </c:choose>
 </div>