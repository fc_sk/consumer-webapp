<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<section class="main">
    
    <c:if test="${not empty messages}">
        <div class="alert alert-error">
          <ul>
            <c:forEach var="message" items="${messages}">
                <li><c:out value="${message}"/></li>
            </c:forEach>
          </ul>       
        </div>    
    </c:if>
    
    <c:if test="${not empty exception}">
		<div class="alert alert-error" >
	      ${exception}
	    </div>    
	</c:if>

    <h3>Social Share</h3>	

	<div class="customer-trail-input">
	  <hr>
	   <div class="clearfix">
	     <form method="POST" action="/admin/socialshare/email_input_shares.htm" id="socialShareSearchForm" >
		    <div class="column"> 		  	   
		       <label for="email">User Email ID</label>
		       <div class="controls">
		           <input class="input" type="text" name="email" value="${emailEntered}">
		       </div>	       
		   </div>
		    <div class="column">
	           <label for="referralCampaign">Referral Campaign</label>
	           <div class="controls">
	           		<select class="form-control" name="referralCampaign" >
	           			<c:forEach items="${arDeals}" var="referralCampaign"  >
	           				<option value="${referralCampaign.id}" 
	           					<c:if test="${referralCampaign.name eq arDealSearchEntityName}" >selected="selected"</c:if>
	           				>${referralCampaign.name}</option>
	           		    </c:forEach>		
	           		</select>              
	           </div>
		    </div>
		    
		    <div class="column column-last">
	           <label>Submit </label>
	           <div class="controls" >
	           		<button class="btn trim-input" type="submit">GO</button>
	           </div>
		    </div>
		    
	    </form>
	   </div>
	    <hr>
	</div>
</section>

<c:choose>
  <c:when test="${shareCount gt 0}">   
		<h3>Search Result</h3>	
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>Referral Campaign</th>
				    <th>Provider</th>			    
				    <th>Date</th>
				    <th>Total Shares</th>
				</tr>	
			</thead>
			<tbody>			
			    <tr>		
					<td>${arDealSearchEntityName}</td>
					<td>${provider}</td>
					<td>${shareTime}</td>
					<td>${shareCount}</td>
	           </tr>
		   </tbody>	  
		</table>
</c:when>
<c:otherwise>
	No Result Found
</c:otherwise>
</c:choose>
<script type="text/javascript">
   $(function(){
       $(".trim-input").on('click', function(e){
           e.preventDefault();           
		var $form = $("#socialShareSearchForm");
		$form.find("input").each(function(){
			this.value=$(this).val().trim();
	    })
		$form.submit();
       });
   });
</script>