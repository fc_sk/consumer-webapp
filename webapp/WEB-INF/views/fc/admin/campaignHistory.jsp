<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="createdOnDateValue" class="java.util.Date"/>
<jsp:useBean id="updatedOnDateValue" class="java.util.Date"/>
<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
		<div>
            <h3><i><b>Campaign History List for: </b></i><c:out value="${campaignemailid}"/></h3>
            <form method="GET" action="/admin/customertrail/campaignhistory/searchbyemail.htm">
                <div class="row customer-trail-input">
                    <div class="clearfix">
                        <div class="col-sm-3 col-md-3">
                            <h5>Email Id</h5>
                            <input type="text" name="emailid" value="${campaignemailid}" style="width: 175px;">
                        </div>
                        <div class="col-sm-3 col-md-3 dropdown dropdown-submit-input">
                            <br/><br/>
                            <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                 <span data-bind="label">Select Campaign</span>&nbsp;<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <c:forEach var="campaign" items="${campaignList}">
                                  <li><a href="#" data-campaignid="${campaign.id}" data-campaignname="${campaign.name}">
                                    <c:out value="${campaign.name}"/>
                                  </a></li>
                              </c:forEach>
                            </ul>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <br/><br/><input type="submit" class="btn btn-default" value="Get">
                        </div>
                    </div>
                </div>
            </form>
		</div>
		<div>
			<div class="container">
				<div class="row clearfix">
					<div class="col-md-12 column">
						<c:choose>
							<c:when test="${Status eq 'Failed'}">
								<h3 class="alert alert-info">
									<c:out value="${Status}" /> : <c:out value="${ErrorMessage}" />
								</h3>
						    </c:when>
						    <c:otherwise>
						        <c:choose>
						            <c:when test="${not empty campaignHistoryList}">
                                        <table class="table table-bordered table-condenseds">
                                            <thead>
                                                <tr>
                                                    <th>Campaign Name</th>
                                                    <th>Product Type</th>
                                                    <th>Order Id</th>
                                                    <th>Trigger</th>
                                                    <th>Reward Result</th>
                                                    <th>Failure Message</th>
                                                    <th>Detailed Failure Message</th>
                                                    <th>Reward Process Results</th>
                                                    <th>Campaign Error</th>
                                                    <th>Created On</th>
                                                    <th>Updated On</th>
                                                    <!--<th></th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="campaignHistory"
                                                    items="${campaignHistoryList}">
                                                    <tr>
                                                        <td>${campaignHistory.campaignName}</td>
                                                        <td>${campaignHistory.productType}</td>
                                                        <td>${campaignHistory.orderId}</td>
                                                        <td>${campaignHistory.trigger}</td>
                                                        <td>${campaignHistory.conditionResult.status}</td>
                                                        <td>${campaignHistory.conditionResult.failedRule.failureMessage}</td>
                                                        <td>${campaignHistory.conditionResult.failedRule.detailedFailureMessage}</td>
                                                        <td>
                                                            <c:if test="${not empty campaignHistory.rewardProcessResults}">
                                                                <table class="table table-condensed">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Status</th>
                                                                            <th>Message</th>
                                                                            <th>Type</th>
                                                                            <th>Credited Amount</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <c:forEach var="rewardEntry" items="${campaignHistory.rewardProcessResults}">
                                                                            <td>${rewardEntry.value.status}</td>
                                                                            <td>${rewardEntry.value.message}</td>
                                                                            <td>${rewardEntry.value.type}</td>
                                                                            <td>${rewardEntry.value.info.creditedAmount}</td>
                                                                        </c:forEach>
                                                                    </tbody>
                                                                </table>
                                                            </c:if>
                                                        </td>
                                                        <td>
                                                            <c:if test="${not empty campaignHistory.campaignErrors}">
                                                                <table class="table table-condensed">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Failed Service</th>
                                                                            <th>Message</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <c:forEach var="campaignError" items="${campaignHistory.campaignErrors}">
                                                                            <td>${campaignError.failedService}</td>
                                                                            <td>${campaignError.message}</td>
                                                                        </c:forEach>
                                                                    </tbody>
                                                                </table>
                                                            </c:if>
                                                        </td>
                                                        <td>
                                                            <jsp:setProperty name="createdOnDateValue" property="time" value="${campaignHistory.createdAt}"/>
                                                            <c:out value="${createdOnDateValue}"/>
                                                        </td>
                                                        <td>
                                                            <jsp:setProperty name="updatedOnDateValue" property="time" value="${campaignHistory.updatedOn}"/>
                                                            <c:out value="${updatedOnDateValue}"/>
                                                        </td>
                                                        <!--<td>
                                                            <c:if test="${campaignHistory.conditionResult.status eq 'false'}">
                                                                <button class="btn btn-mini btn-primary reattemptRewardButton" data-toggle="modal"
                                                                     data-campaignid="${campaignHistory.campaignId}" data-trigger="${campaignHistory.trigger}"
                                                                     data-uniqueid="${campaignHistory.uniqueId}">
                                                                     Re-Attempt
                                                                </button>
                                                            </c:if>
                                                        </td>-->
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
						            </c:when>
						        </c:choose>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="reattemptRewardModal" tabindex="-1" role="dialog"
           aria-labelledby="myModalLabel"  style="display: none; height: 30%">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
              <h4 id="myModalLabel">Re-Attempt Reward</h4>
           </div>
           <div class="modal-body" >
              <form id="reattemptRewardForm">
                <div><input type="text" style="width:auto" id="campaignid" name="campaignId" readonly="readonly"></div>
                <div><input type="text" style="width:auto" id="trigger" name="trigger" readonly="readonly"></div>
                <div><input type="text" style="width:400px" id="uniqueid" name="uniqueId" readonly="readonly"></div>
                <div id="reattemptResponse" class="alert alert-error" style="display: none;"></div>
                <div><button type="submit" class="btn btn-small btn-primary">Re-Attempt</button></div>
              </form>
           </div>
        </div><!-- /.modal -->
	</div>
</div>


<script type="text/javascript">
    $(".reattemptRewardButton").click(function() {
        $("#campaignid").val($(this).data('campaignid'));
        $("#trigger").val($(this).data('trigger'));
        $("#uniqueid").val($(this).data('uniqueid'));
        $('#reattemptResponse').hide();
        $('#reattemptRewardModal').modal('show');
    });
    $("#reattemptRewardForm").submit(function(e) {
        e.preventDefault();

        $.ajax({
               type: "GET",
               url: "/admin/customertrail/campaignhistory/reward/reattempt.htm",
               data: $("#reattemptRewardForm").serialize(),
               success: function(data)
               {
                   if(data.status == true) {
                        $('#reattemptResponse').show();
                        $('#reattemptResponse').removeClass("alert-error").addClass("alert-success");
                        $('#reattemptResponse').text(data.status+": "+data.message);
                   } else {
                        $('#reattemptResponse').show();
                        $('#reattemptResponse').removeClass("alert-success").addClass("alert-error");
                        $('#reattemptResponse').text(data.status+": "+data.message);
                   }
               }
             });
    });
    $('.dropdown-submit-input .dropdown-menu a').click(function (e) {
        e.preventDefault();
        var campaignid = $(this).data('campaignid');
        var campaignname = $(this).data('campaignname');
        $(this).closest('.dropdown-submit-input').find('[data-bind="label"]').text(campaignname).end()
        $(this).closest('form').append('<input type="hidden" value="'+campaignid+'" name="campaignid"/>');
        //$(this).closest('form').submit();
    });
</script>