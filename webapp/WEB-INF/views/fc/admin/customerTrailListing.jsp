<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class= "row">
    <div class="col-md-3 col-sm-3">
        <jsp:include page="customerTrailSearch.jsp" />
    </div>
    <div class="col-md-9 col-sm-9">
        <%@page
        	import="com.freecharge.customercare.controller.CustomerTrailController"%>
        <%@page
        	import="com.freecharge.customercare.controller.CustomerTrailController"%>
        <%@include file="/WEB-INF/includes/taglibs.jsp"%>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <div class="customer-trail">
        	<div class="cs-display">

        		<div class="row">
        			<div class="col-md-4 col-sm-4">
        				<section class="unit">
        					<h2>
        						<span class="label label-info"><spring:message
        								code="customer.information" text="#customer.information#" /></span>
        					</h2>
        					<div class="layer">
        						<ul>
        							<li><em> <spring:message code="name" text="#name#" />:
        							</em> <c:out value="${userProfile.firstName}" /></li>
        							<li><em> <spring:message code="email" text="#email#" />:
        							</em>
        								<form name="submitEmailForm" method="POST"
        									action="/admin/email_input_transaction/do.htm"
        									style="display: inline;">
        									<input type="hidden" name="email" value="${user.email}">
        									<a href="javascript:document.submitEmailForm.submit()">${user.email}</a>
        								</form></li>
        							<li><em> <spring:message code="mobile.no"
        										text="#mobile.no#" />:
        							</em> <c:out value="${user.mobileNo}" /></li>
        							<li><em> <spring:message code="order.id"
        										text="#order.id#" />:
        							</em>${order.orderId}</li>
        							<li><em><spring:message code="creation.date"
        										text="#creation.date#" />:</em> <fmt:formatDate type="date"
        									pattern="dd-MMM-yyyy" value="${order.createdOn}" /> <fmt:formatDate
        									type="time" timeStyle="short" value="${order.createdOn}" /></li>
        							<li><em> <spring:message code="isactive"
        										text="#isactive#" />:
        							</em> <a href="#user-ban" data-toggle="modal" id="user-status-id">
        									<c:choose>
        										<c:when test="${user.isActive}">
        											<spring:message code="active" text="#active#" />
        										</c:when>
        										<c:otherwise>
        											<spring:message code="inactive" text="#inactive#" />
        										</c:otherwise>
        									</c:choose>
        							</a></li>
        							<li><em> <spring:message code="channel.id"
        										text="#channel.id#">:</spring:message>
        							</em>${channelName}</li>
        							<li><em> Mobile app version :
        							</em>${appversion}</li>
        							<li><em> FC-SD User Migration status:
        							   </em><a href="#" data-toggle="tooltip" data-placement="right" title="${userMigrationToolTipText}">${fcSdUserMigration}</a></li>
        							<li><em> Total balance:
        							</em>
        								<form name="submitWalletForm" method="POST" action="/admin/customertrail/onecheck/searchbyemail/view.htm" style="display: inline;">
        									<input type="hidden" name="emailid" value="${user.email}">
        										<a href="javascript:document.submitWalletForm.submit()">${walletbalance}</a>
        								</form>
        				            </li>

        						</ul>
        						<div style="display: none;">
        							<ul>
        							    <c:if test="${balance != null}" >
        								   <li><em> General balance:
        								   </em> ${balance.generalBalance}</li>
        								   <li><em> Voucher balance:
        								   </em> ${balance.voucherBalance} </li>
        								</c:if>
        								<li><em> <spring:message code="sms.sentdate"
        											text="#sms.sentdate#" />:
        								</em> <fmt:formatDate value="${transactionFulfilment.smsSent}"
        										pattern="dd-MMM-yyyy" /></li>
        								<li><em> <spring:message code="email.sentdate"
        											text="#email.sentdate#" />:
        								</em> <fmt:formatDate value="${transactionFulfilment.emailSent}"
        										pattern="dd-MMM-yyyy" /></li>
        							</ul>
        						</div>

        						<a href="javascript:void(0);" class="more-info btn btn-mini">Show
        							more</a>
        					</div>
        				</section>

        			</div>

        			        			
        			<div class="col-md-4 col-sm-4">       			
        				<section class="unit">
        				<!-- Google Recharge Info --> 
        				<c:if test="${not empty googleRechargeDetailMap}">     					
        					
        					<h2>
        						<span class="label label-info"><spring:message
        								code="transaction.information" text="#transaction.information#" /></span>
        					</h2>
						<div class="layer">
        						<ul>
        							<li><em> <spring:message code="reference.id" text="#reference.id#" />:
        							</em> <c:out value="${googleRechargeDetailMap[ReferenceId]}" /></li>
        							<li><em> <spring:message code="source" text="#source#" />:
        							</em> <c:out value="${googleRechargeDetailMap[Source]}" /></li>
        							
        						</ul>
        				 </div>
        				</c:if>	
 						<!-- Order Information -->
        					<h2>
        						<span class="label label-info"><spring:message
        								code="payment.plan" text="#payment.plan#" /></span>
        					</h2>
        					<div class="layer">
        						<ul>
        							<li><em><spring:message code="is.successful"
        										text="#is.successful#" />:</em> <c:choose>
        									<c:when test="${paymentTransaction.isSuccessful}">
        										<span class="label label-success"><spring:message
        												code="payment.successful" text="#payment.successful#" /></span>
        									</c:when>
        									<c:otherwise>
        										<span class="label label-important"><spring:message
        												code="payment.unsuccessful" text="#payment.unsuccessful#" />
        										</span>
        									</c:otherwise>
        								</c:choose></li>
        							<li><em><spring:message code="merchant.txn.id"
        										text="#merchant.txn.id#" />:</em>${paymentTransaction.merchantTxnId}</li>
        							<li><em><spring:message code="pg.name" text="#pg.name#" />:</em>${paymentGatewayMaster.name
        								}</li>
        							<li><em>PG Amount:</em>${paymentPlan.pgAmount}</li>
        							<li><em><spring:message code="fc.money"
        										text="#fc.money#" />:</em>${paymentPlan.walletAmount}</li>
        							<li><em><spring:message code="payment.type"
        										text="#payment.type#" />:</em>${paymentTypeMaster.name}</li>
        							<li><em><spring:message code="payment.option"
        										text="#payment.option#" />:</em>${paymentTypeOption.displayName}</li>
        						</ul>

        						<table class="table table-bordered table-hover">
        							<thead>
        								<tr>
        									<th>Cart Item</th>
        									<th>Amount</th>
        									<th>Qty</th>
        								</tr>
        							</thead>
        							<tbody>
        								<c:forEach var="cartItem" items="${cart.cartItems}">
        									<c:if
        										test="${cartItem.entityId != 'site_merchant' and cartItem.entityId != '' and !cartItem.deleted }">
        										<c:choose>
        											<c:when test="${cartItem.productMasterId eq '1'}">
        												<tr>
        													<td>Recharge Amount</td>
        													<td>${cartItem.price}</td>
        													<td></td>
        												</tr>
        											</c:when>
        											<c:when test="${cartItem.productMasterId eq '8' }">
        												<tr>
        													<td><form name="submitCode" method="POST"
        															action="/admin/customertrail/getorderidbycouponcode"
        															style="display: inline;">
        															<input type="hidden" name="promocode"
        																value="${cartItem.displayLabel}"> <a
        																href="javascript:document.submitCode.submit()"
        																target="${cartItem.displayLabel}">${cartItem.displayLabel}</a>
        														</form>(${freefundCoupon.promoEntity })</td>
        													<td>${cartItem.price}</td>
        													<td></td>
        												</tr>
        											</c:when>
        											<c:otherwise>
        												<tr>
        													<td>${cartItem.displayLabel}</td>
        													<td>${cartItem.price} <c:choose>
        															<c:when
        																test="${cartItem.entityId eq 'FreeFund' and cartItem.productMasterId eq 9}">
        																<span class="label label-success"> Discount</span>
        															</c:when>
        															<c:when
        																test="${cartItem.entityId eq 'BinOffer' and cartItem.productMasterId eq 11}">
        																<span class="label label-success"> Cashback</span>
        															</c:when>
        															<c:otherwise>
        															</c:otherwise>
        														</c:choose>
        													</td>
        													<td></td>
        												</tr>
        											</c:otherwise>
        										</c:choose>
        									</c:if>
        								</c:forEach>
        							</tbody>
        						</table>
        						<!-- Paid coupons in cart -->
        						<c:if test="${isPaidCouponsSelected}">
        							<table class="table table-bordered table-hover">
        								<tr>
        									<th>Total price for paid-coupons in cart :</th>
        									<th style="width: 48px;">${paidCouponTotalPrice}</th>
        								</tr>
        							</table>
        						</c:if>
        						<!-- End of Paid coupons -->
        					</div>
        				</section>
        			</div>
        			<div class="col-md-4 col-sm-4">
        				<section class="unit">
        					<c:if test="${finalProductType eq 'V' or finalProductType eq 'D' or finalProductType eq 'C' or finalProductType eq 'F' or finalProductType eq 'X'}">
        						<h2>
        							<span class="label label-info">Recharge Details</span>
        						</h2>
        						<c:if test="${isOperatorRetry}">
        							   <li><em><font color="red">Operator Retry</font></em></li>
        						</c:if>
        						<c:if test="${forcedRefund}">
        							   <div><em><font color="red">Forced Refund</font></em></div>
        						</c:if>
        						<c:if test="${isReverselCase eq 'reversalCase'}">
        							<font color="red"><c:out
        									value="${inTransactionData.inResponse.inRespMsg}" /></font>
        						</c:if>
        						<div class="layer">
        							<ul>
        								<li><em> <spring:message code="recharge.status"
        											text="#recharge.status#" />:
        								</em> <c:choose>
        										<c:when
        											test="${inTransactionData.inResponse.inRespCode eq '0' or inTransactionData.inResponse.inRespCode eq '00'}">
        											<span class="label label-success"> <spring:message
        													code="recharge.successful" text="#recharge.successful#" />
        											</span>
        										</c:when>
        										<c:when
        											test="${inTransactionData.inResponse.inRespCode eq '08'}">
        											<span class="label label-warning"> <spring:message
        													code="transaction.underprocess"
        													text="#transaction.underprocess#" />
        											</span>
        										</c:when>
        										<c:when
        											test="${inTransactionData.inResponse.inRespCode eq '-1' }">
        											<span class="label label-important"> <spring:message
        													code="pending.transaction" text="#pending.transaction#" />
        											</span>
        										</c:when>
        										<c:when test="${empty inTransactionData.inResponse.inRespCode}">
        										    <c:if test="${shouldShowAsQueued}">
        												<span class="label label-important">
        												    <spring:message code="scheduled.transaction" text="#scheduled.transaction#" />
        												</span>
        											</c:if>
        											<c:if test="${not shouldShowAsQueued}">
        												<span class="label label-important">
        												    <spring:message code="no.recharge.attempt" text="#no.recharge.attempt#" />
        												</span>
        											</c:if>

        										</c:when>
        										<c:otherwise>
        										    <c:if test="${shouldShowAsQueued}">
        												<span class="label label-important">
        												    <spring:message code="scheduled.transaction" text="#scheduled.transaction#" />
        												</span>
        											</c:if>
        											<c:if test="${not shouldShowAsQueued}">
        												<span class="label label-important"> <spring:message
        														code="recharge.unsuccessful" text="#recharge.unsuccessful#" />
        												</span>
        											</c:if>
        										</c:otherwise>
        									</c:choose></li>
        								<li><em>Product Name : </em> ${productName}</li>
        								<c:choose>
        										<c:when test="${isAuthorize eq 'yes'}">
        											<li><em>Order Id : </em> <a href ="#" class="recharge-status-check" data-producttype="${rechargeProductType}" orderid="${order.orderId}">${order.orderId}</a></li>
        										</c:when>
        										<c:otherwise>
        											<li><em>Order Id : </em>
        											${order.orderId}</li>
        										</c:otherwise>
        								</c:choose>
        								<li><em>RequestId:</em> ${inTransactionData.inResponse.requestId}</li>
        								<li><em>Recharge Amount: </em>
        									${inTransactionData.inRequest.amount}</li>
        								<li><em>Operator:</em>
        									${inTransactionData.inRequest.operator}</li>
        								<li><em>Circle:</em> ${inTransactionData.inRequest.circle}
        								</li>
        								<li><em>Mobile Number:</em>
        									${inTransactionData.inRequest.subscriberNumber}</li>
        								<li>
        								<li><em>Aggregator:</em>${inTransactionData.inResponse.aggrName}</li>
        								<li><em> <spring:message code="aggregator.txn.id"
        											text="#aggregator.txn.id#" />:
        								</em>${inTransactionData.inResponse.aggrTransId}</li>
        								<li><em> <spring:message code="operator.txn.id"
        											text="#operator.txn.id#" />:
        								</em>${inTransactionData.inResponse.oprTransId}</li>
        								<li><em>Aggregator Message:</em>
        									${inTransactionData.inResponse.aggrRespMsg}</li>
        								<li><c:choose>
        										<c:when
        											test="${not empty inTransactionData.inRequest.rechargePlan}">
        											<em>Recharge Plan</em>
                                                ${inTransactionData.inRequest.rechargePlan}
                                            </c:when>
        										<c:otherwise>
        										</c:otherwise>
        									</c:choose></li>
        							</ul>
        							<div style="display: none;">
        								<ul>
        									<li><em><span class="label">Raw_request:</span></em>
        										${inTransactionData.inResponse.rawRequest}</li>
        									<li><em><span class="label">Raw_response:</span></em>
        										${inTransactionData.inResponse.rawResponse}</li>
        								</ul>
        							</div>
        							<a href="javascript:void(0);" class="more-info btn btn-mini">Show more</a>
        						</div>
        					</c:if>
        				<c:if test="${finalProductType eq 'M'}">
        					<c:choose>
        					    <c:when test="${billPaymentStatus!=null}">
        							<h2><span class="label label-info">Postpaid BillPayment Details</span></h2>
        							<c:if test="${isPostpaidReversalCase eq 'reversalCase'}">
        								<font color="red"><c:out
        									value="${billPaymentStatus.authDetail}" /></font>
        							</c:if>
        							<div class="layer">
        							<ul>
        							<c:if test="${isOperatorRetry}">
        							   <li><em><font color="red">Operator Retry</font></em></li>
        							</c:if>
        							<c:if test="${forcedRefund}">
        							   <div><em><font color="red">Forced Refund</font></em></div>
        							</c:if>
        								<li><em> <spring:message code="billpayment.status"
        											text="#billpayment.status#" />:
        								</em> <c:choose>
        										<c:when test="${billPaymentStatus.success}">
        											<span class="label label-success"> <spring:message
        													code="billpayment.successful"
        													text="#billpayment.successful#" />
        											</span>
        										</c:when>
        										<c:when test="${billPaymentStatus.success == false}">
        											<span class="label label-important"> <spring:message
        													code="billpayment.failure" text="#billpayment.failure#" />
        											</span>
        										</c:when>
        										<c:otherwise>
        											<span class="label label-warning"> <spring:message
        													code="billpayment.pending" text="#billpayment.pending#" />
        											</span>
        										</c:otherwise>
        									</c:choose></li>
        								<li><em>BillPayment ID : </em> ${billPaymentStatus.id}</li>
        								<li><em>BillPayment Product Type : </em>
        									${billPaymentStatus.productType}</li>
        									<c:choose>
        										<c:when test="${isAuthorize eq 'yes'}">
        											<li><em>Order ID:</em> <a href="#" class="recharge-status-check" data-producttype="${billPaymentStatus.productType}" orderid="${order.orderId}">${billPaymentStatus.affiliateTransID}</a>
        											<ul></ul></li>
        										</c:when>
        										<c:otherwise>
        											<li><em>Order ID: </em>
        									        	${billPaymentStatus.affiliateTransID}</li>
        										</c:otherwise>
        									</c:choose>
        								<li><em>BillPayment Amount: </em>
        									${billPaymentStatus.amount}</li>
        								<li><em>Billpayment Postpaid number:</em>
        									${billPaymentStatus.postpaidNumber}</li>
        								<li><em>Postpaid merchant name:</em>
        									${postpaidMerchantName}</li>
        								<li><em>Account number:</em>
        									${billPaymentStatus.postpaidAccountNumber}</li>
        								<li><em>BillPayment Gateway:</em>
        									${billPaymentStatus.billPaymentGateway}</li>
        								<li>
        								<li><em>Gateway Ref Number:</em>
        									${billPaymentStatus.gatewayReferenceNumber}</li>
        								</ul>
        								<div style="display: none;">
        								   <ul>
        								    <li><em>Created At:</em> ${billPaymentStatus.createdAt}</li>
        								    <li><em>Updated At:</em> ${billPaymentStatus.updatedAt}</li>
        								    <li><em><span class="label">Raw_request:</span></em> ${billPaymentAttempt.rawRequest}</li>
        							        <li><em><span class="label">Raw_response:</span></em> ${billPaymentAttempt.rawResponse}</li>
        							       </ul>
        							    </div>
        							   <a href="javascript:void(0);" class="more-info btn btn-mini">Show more</a>
        							</div>
        					  </c:when>
        					  <c:otherwise>
        					  		<h2><span class="label label-info">Postpaid BillPayment Details</span></h2>
        							<div class="layer">
        								<ul>
        								<li><em><spring:message code="billpayment.status"
        											text="#billpayment.status#" /> : </em>
        									<span class="label label-important">
        										  <spring:message code="no.recharge.attempt" text="#no.recharge.attempt#" />
        								    </span>
        								</li>
        								</ul>
        							</div>
        					  </c:otherwise>
        					</c:choose>
        				</c:if>
        				<c:if test="${isUtilitiesPayment}">
                        	<c:choose>
                        		<c:when test="${billDetails!=null}">
                                <h2><span class="label label-info">BillPayment Details</span></h2>
                                <div class="layer">
                                    <ul>
                                    	<c:if test="${isOperatorRetry}">
        							   		<li><em><font color="red">Operator Retry</font></em></li>
        								</c:if>
        								<c:if test="${forcedRefund}">
		        							<li><em><font color="red">Forced Refund</font></em></li>
		        						</c:if>
                                        <li><em> <spring:message code="billpayment.status"
                                                    text="#billpayment.status#" />:
                                        </em> <c:choose>
                                                <c:when test="${billDetails.transactionStatus eq '00'}">
                                                    <span class="label label-success"> <spring:message
                                                            code="billpayment.successful"
                                                            text="#billpayment.successful#" />
                                                    </span>
                                                </c:when>
                                                <c:when test="${billDetails.transactionStatus eq '-1'}">
                                                    <span class="label label-warning"> <spring:message
                                                            code="billpayment.pending" text="#billpayment.pending#" />
                                                    </span>
                                                </c:when>
                                                <c:when test="${billDetails.transactionStatus eq '08'}">
                                                    <span class="label label-warning"> <spring:message
                                                            code="transaction.underprocess"
                                                            text="#transaction.underprocess#" />
                                                    </span>
                                                </c:when>
                                                <c:otherwise>
                                                    <span class="label label-important"> <spring:message
                                                            code="billpayment.failure" text="#billpayment.failure#" />
                                                    </span>
                                                </c:otherwise>
                                            </c:choose></li>
                                        <li><em>BillPayment ID : </em> ${billDetails.billTransactionId}</li>
                                        <li><em>BBPS Reference ID : </em>${bbpsTransactionDetail.bbpsReferenceId}</li>
                                        <li><em>Product Type : </em>
                                            ${userTransaction.productType}</li>
                                        <c:choose>
        									<c:when test="${isAuthorize eq 'yes'}">
                                        		<li><em>Order ID:</em> <a href="#" class="recharge-status-check" data-producttype="${userTransaction.productType}" orderid="${order.orderId}">${order.orderId}</a></li>
                                             </c:when>
        									<c:otherwise>
        										<li><em>Order ID:</em>
        										${order.orderId}</li>
        									</c:otherwise>
        								</c:choose>
                                            <li><em>BillPayment Amount: </em>
                                            ${userTransaction.transactionAmount}</li>
                                        <li><em>Billpayment account number:</em>
                                            ${userTransaction.subscriberIdentificationNumber}</li>
                                        <li><em>Service merchant name:</em>
                                            ${userTransaction.serviceProvider}</li>
                                        <li><em>BillPayment Gateway:</em>
                                            ${billDetails.aggregator}</li>
                                        <li>
                                        <li><em>Gateway Ref Number:</em>
                                            ${billDetails.aggrBillTransactionId}</li>
                                        </ul>
                                        <div style="display: none;">
                                           <ul>
                                            <li><em>Created At:</em> ${billDetails.createdOn}</li>
                                            <li><em>Updated At:</em> ${billDetails.lastUpdated}</li>
                                            <li><em><span class="label">Raw_request:</span></em> ${billDetails.rawRequest}</li>
                                            <li><em><span class="label">Raw_response:</span></em> ${billDetails.rawResponse}</li>
                                           </ul>
                                        </div>
                                       <a href="javascript:void(0);" class="more-info btn btn-mini">Show more</a>
                                </div>
                                </c:when>
                                <c:otherwise>
                                	<h2><span class="label label-info">BillPayment Details</span></h2>
        							<div class="layer">
        								<ul>
        								<li><em><spring:message code="billpayment.status"
        											text="#billpayment.status#" /> : </em>
        									<span class="label label-important"> Not Attempted/Received </span></li>
        								</ul>
        							</div>
        					    </c:otherwise>
                            </c:choose>
                        </c:if>
        			</section>
        		</div>
        	</div>
         	<hr>
        <c:choose>
            <c:when test="${isAuthorize eq 'yes'}">
        		<section class="unit clearfix">

        			<!-- Cash back tool -->
        			<button class="btn btn-mini btn-primary" style="float:left;margin-right:12px" data-toggle="modal"
        				data-target="#cashBackModal">Cash back</button>
        		<!-- End of cash back tool -->
        		<!-- Bank refund -->
        		<button class="btn btn-mini btn-primary" data-toggle="modal"
        			data-target="#bankRefundModal" style="float:left;margin-right:12px">Refund to bank through PG</button>
        		<!-- End of bank refund -->

        		<!-- Reversal button success to failure -->
        				<form name="reversalForm" id="reversalForm"
        					style="float: right; margin-right: 12px">
        					<input type="hidden" name="requestId"
        						value="${inTransactionData.inResponse.requestId}"> <input
        						type="hidden" name="rechargeStatus" value="FAILED">
        						<input type="hidden" name="reversalOrderId" value="${order.orderId}">
        						<!-- transactionHomePage -->
        						<input type="hidden" name="reversalProductType" value="${finalProductType}">
        					<button type="submit" class="btn btn-mini btn-warning">Reverse
        						status to failure</button>
        				</form>
        	    <!-- End of reversal button -->
                </section>
            </c:when>
        	<c:otherwise>
        	</c:otherwise>
        </c:choose>
                <section class="unit clearfix">
        	    <!-- Recharge Status check button -->
                          <c:if test="${finalProductType eq 'V' or finalProductType eq 'D' or finalProductType eq 'C' or finalProductType eq 'F'}">
          	    			<button class="btn btn-mini btn-warning recharge-status-check" style="float:right;margin-right:12px" data-producttype="${rechargeProductType}" orderid="${order.orderId}">Recharge status</button>
                          </c:if>
                          <c:if test="${finalProductType eq 'M'}">
                          	<button class="btn btn-mini btn-warning recharge-status-check" style="float:right;margin-right:12px" data-producttype="${billPaymentStatus.productType}" orderid="${order.orderId}">Bill status</button>
                          </c:if>
                          <c:if test="${isUtilitiesPayment}">
                          	<button class="btn btn-mini btn-warning recharge-status-check" style="float:right;margin-right:12px" data-producttype="${userTransaction.productType}" orderid="${order.orderId}">Bill status</button>
                          </c:if>
        	    <!-- End of Recharge status check button -->

                </section>

        		<!-- payment info -->
        		<section class="unit">

        			<div class="layer">
        				<div>
        					<c:choose>
        						<c:when test="${not empty paymentTransactionAttemptList}">
        							<hr>
        							<h4>
        								<span class="label label-info"><spring:message
        										code="payment.attempts" text="#payment.attempts#" /></span>
        							</h4>
        							<!-- <table class="table table-hover"> -->
        							<table class="table table-bordered table-hover">
        								<thead>
        									<tr>
        										<th><spring:message code="sr.no" text="#sr.no#" /></th>
        										<th><spring:message code="db.transaction.id"
        												text="#db.transaction.id#" /></th>
        										<th><spring:message code="pg.transaction.id"
        												text="#pg.transaction.id#" /></th>
        										<th><spring:message code="merchant.txn.id"
        												text="#merchant.txn.id#" /></th>
        										<th><spring:message code="amount" text="#amont#" /></th>
        										<th><spring:message code="status" text="#status#" /></th>
        										<th><spring:message code="payment.gateway"
        												text="#payment.gateway#" /></th>
        										<th><spring:message code="sent.topg" text="#sent.topg#" />
        										</th>
        										<th><spring:message code="received.frompg"
        												text="#received.frompg#" /></th>
        										<th>PGType</th>
                                                <th></th>
        										<c:choose>
											  		<c:when test="${isAuthorize eq 'yes'}">
														<th></th>
													</c:when>
												</c:choose>
        									</tr>
        								</thead>
        								<tbody>
        									<c:set var="srNo" value="0">
        									</c:set>
        									<c:forEach var="paymentAttempt"
        										items="${paymentTransactionAttemptList}">
        										<c:set var="srNo" value="${srNo + 1 }">
        										</c:set>
        										<tr>
        											<td>${srNo}</td>
        											<c:choose>
        												<c:when
        													test="${paymentAttempt.paymentGateway eq 'fw' or paymentAttempt.paymentGateway eq 'zpg'}">
        													<td>${paymentAttempt.paymentTxnId}</td>
        												</c:when>
        												<c:otherwise>
        													<td><a class="payment-txn-id"
        														href="javascript:void(0)">${paymentAttempt.paymentTxnId}</a></td>
        												</c:otherwise>
        											</c:choose>
        											<td>${paymentAttempt.transactionId}</td>
        											<td>${paymentAttempt.merchantTxnId}</td>
        											<td>${paymentAttempt.amount}</td>
        											<td><c:choose>
        													<c:when test="${paymentAttempt.isSuccessful}">
        														<span class="label label-success"> <spring:message
        																code="yes" text="#yes#" />
        														</span>
        													</c:when>
        													<c:otherwise>
        														<span class="label label-important"> <spring:message
        																code="no" text="#no#" />
        														</span>
        													</c:otherwise>
        												</c:choose></td>
        											<td>${paymentAttempt.paymentGateway}</td>
        											<td><fmt:formatDate type="date" pattern="dd-MMM-yyyy"
        													value="${paymentAttempt.setToPg}" /> <fmt:formatDate
        													type="time" timeStyle="short"
        													value="${paymentAttempt.setToPg}" /></tfailured>
        											<td><fmt:formatDate type="date" pattern="dd-MMM-yyyy"
        													value="${paymentAttempt.recievedToPg}" /> <fmt:formatDate
        													type="time" timeStyle="short"
        													value="${paymentAttempt.recievedToPg}" /></td>
        											<td><c:if test="${paymentAttempt.paymentGateway ne 'fw'}">${pgTypeMap[paymentAttempt.merchantTxnId]}</c:if></td>
                                                    <td><!-- PG Status Check/Refund button -->
                                                        <form name="pgStatusCheckForm" method="GET"
                                                            action="/admin/refund/manual/doPayStatusCheckAndRefund.htm"
                                                            style="float:left;margin-right:12px">
                                                            <input type="hidden" name="orderIds" value="${paymentAttempt.merchantTxnId}">
                                                            <button type="submit" class="btn btn-mini btn-primary">PG Status</button>
                                                        </form>
                                                    </td>
        											<c:choose>
												  		<c:when test="${isAuthorize eq 'yes'}">
															<td>
																<c:choose>
																	<c:when test="${paymentAttempt.isSuccessful}">
																		<c:choose>
																			<c:when test="${paymentAttempt.setToPg le manualRefundCutoffTime}">
																				<!-- Manual refund button -->
																					<button class="btn btn-mini btn-primary manualRefundButton" data-toggle="modal" style="float:left;margin-right:12px" data-mtxnid="${paymentAttempt.merchantTxnId}">Manual refund</button>
																				<!-- End of manual refund -->
																			</c:when>
																		</c:choose>
																	</c:when>
																</c:choose>
															</td>
														</c:when>
													</c:choose>
        										</tr>
        									</c:forEach>
        								</tbody>
        							</table>
        						</c:when>
        						<c:otherwise>
        						</c:otherwise>
        					</c:choose>

        					<c:choose>
        						<c:when test="${not empty refunds}">
        							<hr>
        							<h4>
        								<spring:message code="refund.details" text="#refund.details#" />
        							</h4>
        							<table class="table table-bordered table-hover">
        								<thead>
        									<tr>
        										<th><spring:message code="order.id" text="#order.id#" />
                                                </th>
        										<th><spring:message code="merchant.txn.id" text="#merchant.txn.id#" />
        										</th>
        										<th><spring:message code="refund.amount"
        												text="#refund.amount#" /></th>
        										<th><spring:message code="refund.status"
        												text="#refund.status#" /></th>
        										<th><spring:message code="pg" text="#pg#" /></th>
        										<th><spring:message code="pg.response"
        												text="#pg.response#" /></th>
        										<th><spring:message code="refund.ref.no"
        												text="#refund.ref.no#" />.</th>
        										<th><spring:message code="refund.date.time"
        												text="#refund.date.time#" /></th>
        									    <th>raw_pg_response:</th>
        									</tr>
        								</thead>
        								<tbody>
        									<c:forEach var="refund" items="${refunds}">
        										<tr>
        											<td>${refund.paymentTransaction.orderId}</td>
        											<td>${refund.paymentTransaction.merchantTxnId}</td>
        											<td>${refund.refundedAmount.amount}</td>
        											<td>${refund.status}</td>
        											<td>${refund.refundTo}</td>
        											<td>${refund.pgResponseDescription}</td>
        											<td>${refund.pgTransactionReferenceNo}</td>
        											<td><fmt:formatDate type="date" pattern="dd-MMM-yyyy"
        													value="${refund.receivedFromPG}" /></td>
        											<td>${refund.rawPGResponse}</td>
        										</tr>
        									</c:forEach>
        								</tbody>
        							</table>
        						</c:when>
        						<c:otherwise>
        						</c:otherwise>
        					</c:choose>
        				</div>
        			</div>
        		</section>

        		<section class="unit">

        			<div class="layer">
        				<div>
        					<%--  <c:forEach var="orderIdList" items="${promoOrderId}">
                                                <c:if test="${orderIdList eq order.orderId }">
                                                      <c:out value="${orderIdList}"/>
                                                 </c:if>
                                                     </c:forEach>
                                           </c:forEach> --%>

        					<c:choose>
        						<c:when test="${not empty freefundCoupon}">
        							<hr>
        							<h4>
        								<spring:message code="promocode.information"
        									text="#promocode.information#" />
        							</h4>
        							<table class="table table-bordered table-hover">
        								<thead>
        									<tr>
        										<th><spring:message code="sr.no" text="#sr.no#" /></th>
        										<th><spring:message code="promocode" text="#promocode#" />
        										</th>
        										<th><spring:message code="campaign.name.id"
        												text="#campaign.name.id#" /></th>
        										<th><spring:message code="status" text="#status#" /></th>
        									</tr>
        								</thead>
        								<tbody>
        									<tr>
        										<td>1</td>
        										<td>${freefundCoupon.freefundCode}</td>
        										<td>${freefundClass.name}</td>
        										<td>${freefundCoupon.status}</td>
        									</tr>
        								</tbody>
        							</table>
        						</c:when>
        					</c:choose>
        				</div>
        			</div>

        		</section>

        		<section class="unit">
        			<h2>
        				<span class="label label-info"><spring:message
        						code="coupons.information" text="#coupons.information#" /></span>
        			</h2>
        			<div class="layer">
        				<c:forEach var="copounlist" items="${coupons}">
        					<c:if test="${couponlist.couponType eq 'P'}">
        						<c:set var='couponVar' value='P' />
        					</c:if>
        				</c:forEach>

        				<div>
        					<table class="table table-bordered table-hover">
        						<thead>
        							<tr>
        								<th><spring:message code="campaign.name"
        										text="#campaign.name#" /></th>
        								<th><spring:message code="coupon.value"
        										text="#coupon.value#" /></th>
        								<th><spring:message code="coupon.codes"
        										text="#coupon.codes#" /></th>
        								<th><spring:message code="coupon.type" text="#coupon.type#" />
        								</th>
        								<th><spring:message code="email.sentdate"
        										text="#email.sentdate#" /></th>
        								<th><spring:message code="email" text="#email#" /></th>
        								<c:if test="${isPaidCouponsSelected}">
        								    <th>Price</th>
        								    <th>Total price</th>
        								 </c:if>
        							</tr>
        						</thead>
        						<tbody>
        							<c:forEach var="coupon" items="${coupons}">
        								<tr>
        									<td>${coupon.campaignName} [ ${coupon.couponStoreId}]</td>
        									<td>${coupon.couponValue}
        										(${quantityMap[coupon.couponStoreId]})</td>
        									<td>${codesMap[coupon.couponStoreId]}</td>
        									<td>${coupon.couponType}</td>
        									<td>${transactionFulfilment.emailSent}</td>
        									<td><c:choose>
        											<c:when test="${coupon.couponType eq 'P'}">
        												<a href="#" id="user-email-retrigger">${user.email}</a>
        											</c:when>
        											<c:when test="${coupon.couponType eq 'E'}">
        												<a href="#" id="user-email-retrigger">${user.email}</a>
        											</c:when>
        											<c:when test="${coupon.couponType eq 'C'}">
        												<a href="#" id="user-crossell-email-retrigger">${user.email}</a>
        											</c:when>
        											<c:otherwise></c:otherwise>
        										</c:choose></td>
        									<c:if test="${isPaidCouponsSelected}">
        									   <td>${coupon.price}</td>
        									   <td>${paidCouponPriceMap[coupon.couponStoreId]}</td>
        									</c:if>
        								</tr>
        							</c:forEach>
        						</tbody>
        					</table>
        				</div>

        				<hr>

        				<div>
        					<c:choose>
        						<c:when test="${not empty orderFulfillmentDetailsList}">
        							<h4>
        								<span class="label label-info"><spring:message
        										code="courier.details" text="#courier.details#" /></span>
        							</h4>
        							<table class="table table-bordered table-hover">
        								<c:forEach var="courierItem"
        									items="${orderFulfillmentDetailsList}">
        									<tr>
        										<td><spring:message code="courier.status"
        												text="#courier.status#" />:</td>
        										<td><span class="label">${courierItem.fulfillmentStatus}</span></td>
        										<td><spring:message code="courier.tracking.number"
        												text="#courier.tracking.number#" />:</td>
        										<td><span class="label">${courierItem.awbNum}</span></td>
        									</tr>
        								</c:forEach>
        							</table>
        						</c:when>
        						<c:otherwise>
        						</c:otherwise>
        					</c:choose>
        				</div>

        				<hr>
        				<!-----------------------------------------------------------Reisssued coupons --------------------------------------------------------->
        				<div class="layer">
        					<div>
        						<c:choose>
        							<c:when test="${not empty reissuedCouponsMapList}">
        								<hr>
        								<h4>
        									<span class="label label-info"><spring:message
        											code="reissued.coupon.details"
        											text="#reissued.coupon.details#" /></span>
        								</h4>
        								<table class="table table-bordered table-hover">
        									<thead>
        										<tr>
        											<th><spring:message code="campaign.name.id"
        													text="#campaign.name.id#" /></th>
        											<th><spring:message code="coupon.value"
        													text="#coupon.value#" /></th>
        											<th><spring:message code="coupon.type"
        													text="#coupon.type#" /></th>
        											<th><spring:message code="coupon.quantity"
        													text="#coupon.quantity#" /></th>
        											<th><spring:message code="created.date"
        													text="#created.date#" /></th>
        									</thead>
        									<tbody>
        										<c:forEach var="rcouponItemMap"
        											items="${reissuedCouponsMapList}">
        											<tr>
        												<td>${rcouponItemMap['campaigName']}
        													(${rcouponItemMap['couponId']})</td>
        												<td>${rcouponItemMap['couponValue']}</td>
        												<td>${rcouponItemMap['couponType']}</td>
        												<td>${rcouponItemMap['couponQuantity']}</td>
        												<td>${rcouponItemMap['createdDate']}</td>
        											</tr>
        										</c:forEach>
        									</tbody>
        								</table>
        							</c:when>
        							<c:otherwise>
        							</c:otherwise>
        						</c:choose>
        					</div>

        				<div class="layer">
        					<div>
        						<c:choose>
        							<c:when test="${not empty couponOptinHistory}">
        								<hr>
        								<h4>
        									<span class="label label-info">Coupon Optin History</span>
        								</h4>

        								<table class="table table-bordered table-hover">
        									<thead>
        										<tr>
        											<th>Campaign Name</th>
        											<th>coupon Code</th>
        											<th>coupon Value</th>
        											<th>Optin Type</th>
        											<th>Coupon Quantity</th>
        											<th>Price</th>
        											<th>Created On</th>
        									</thead>
        									<tbody>
        										<c:forEach var="rcouponItemMap"
        											items="${couponOptinHistory}">
        												<c:choose>
        													<c:when test="${rcouponItemMap['optinType'] ne 'optin'}">
        													<tr>
        														<td>${rcouponItemMap['campaignName']}</td>
        														<td>${rcouponItemMap['couponCode']}</td>
        														<td>${rcouponItemMap['couponValue']}</td>
        														<td>${rcouponItemMap['optinType']}</td>
        		 												<td>1</td>
        		 												<td>${rcouponItemMap['price']}</td>
        														<td>${rcouponItemMap['createdOn']}</td>
        													</tr>
        													</c:when>
        												</c:choose>
        										</c:forEach>
        									</tbody>
        								</table>
        							</c:when>
        							<c:otherwise>
        								<c:choose>
											<c:when test="${not empty couponOptError}">
												<h4>
													<span class="label label-info">Coupon Optin History</span>
												</h4>
												<p><c:out value="${couponOptError}" /></p>
											</c:when>
										</c:choose>
        							</c:otherwise>
        						</c:choose>
        					</div>

        				</div>


        				</div>
        			</div>
        		</section>

        		<!-- ------------------------------Recharge attempts----------------------------------------------------->
        		<section class="unit">
        			<div class="layer">
        				<div class="row">
        					<c:choose>
        						<c:when test="${not empty inTransactionsList || not empty postpaidAttemptsDataList}">
        							<hr>
        							<h4>
        								<span class="label label-info"><spring:message
        										code="recharge.attempts" text="#recharge.attempts#" /></span>
        							</h4>
        							<div class="table">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th><spring:message code="request.id"
                                                            text="#request.id#" /></th>
                                                    <th><spring:message code="producttype.name"
                                                            text="Product Type" /></th>
                                                    <th><spring:message code="aggregator.name"
                                                            text="#aggregator.name#" /></th>
                                                    <th><spring:message code="aggregator.txn.id"
                                                            text="#aggregator.txn.id#" /></th>
                                                    <th><spring:message code="operator.txn.id"
                                                            text="#operator.txn.id#" /></th>
                                                    <th><spring:message code="recharge.status"
                                                            text="#recharge.status#" /></th>
                                                    <th><spring:message code="aggregator.response.code"
                                                            text="#aggregator.response.code#" /></th>
                                                    <th><spring:message code="aggregator.response.msg"
                                                            text="#aggregator.response.msg#" /></th>
                                                    <th><spring:message code="request.time"
                                                            text="#request.time#" /></th>
                                                    <th><spring:message code="response.time"
                                                            text="#response.time#" /></th>
                                                    <th><spring:message code="rawrequest.name"
                                                            text="Raw Request" /></th>
                                                    <th><spring:message code="rawresponse.name"
                                                            text="Raw Response" /></th>
                                                </tr>
                                            </thead>

                                            <tbody class="customer-trail-rows">
                                                <c:if test="${not empty inTransactionsList}">
                                                    <c:forEach var="inTransaction" items="${inTransactionsList}">
                                                        <tr style="display: none;">
                                                            <td>${inTransaction.fkRequestId}</td>
                                                            <td>${productTypeMap[finalProductType]}</td>
                                                            <td>${inTransaction.aggrName}</td>
                                                            <td>${inTransaction.aggrReferenceNo}</td>
                                                            <td>${inTransaction.aggrOprRefernceNo}</td>
                                                            <td>${inTransaction.transactionStatus}</td>
                                                            <td>${inTransaction.aggrResponseCode}</td>
                                                            <td>${inTransaction.aggrResponseMessage}</td>
                                                            <td>${inTransaction.requestTime}</td>
                                                            <td>${inTransaction.responseTime}</td>
                                                            <td>${inTransaction.rawRequest}</td>
                                                            <td>${inTransaction.rawResponse}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                <c:if test="${not empty postpaidAttemptsDataList}">
                                                  <c:forEach var="billPaymentAttempts" items="${postpaidAttemptsDataList}">
                                                    <tr style="display: none;">
                                                        <td>${billPaymentAttempts.id}</td>
                                                        <td>${productTypeMap[finalProductType]}</td>
                                                        <td>--</td>
                                                        <td>--</td>
                                                        <td>--</td>
                                                        <c:choose>
                                                           <c:when test="${billPaymentAttempts.success}">
                                                             <td>success</td>
                                                           </c:when>
                                                          <c:otherwise>
                                                             <td>failure</td>
                                                          </c:otherwise>
                                                        </c:choose>
                                                        <td>${billPaymentAttempts.authStatus}</td>
                                                        <td>${billPaymentAttempts.authDetail}</td>
                                                        <td>${billPaymentAttempts.createdAt}</td>
                                                        <td>--</td>
                                                        <td>${billPaymentAttempts.rawRequest}</td>
                                                        <td>${billPaymentAttempts.rawResponse}</td>
                                                    </tr>
                                                </c:forEach>
                                                </c:if>
                                            </tbody>
                                        </table>
                                    </div>
        							<a href="javascript:void(0);" class="btn btn-mini"
        								id="show-more">Show more</a>
        						</c:when>
        						<c:otherwise>
        						</c:otherwise>
        					</c:choose>
        				</div>
        			</div>
        		</section>

        		<!-- ------------------------------Operator Retry attempts----------------------------------------------------->
        		<section class="unit">
        			<div class="layer">
        				<div>
        					<c:choose>
        						<c:when test="${not empty operaratorRetryDatas}">
        							<hr>
        							<h4>
        								<span class="label label-info"><spring:message
        										code="operator.retry.attempts" text="Operator Retry Attempts" /></span>
        							</h4>
        							<table class="table table-bordered table-hover">
        								<thead>
        									<tr>
        										<th><spring:message code="retry.id"
        												text="Retry Id" /></th>
        										<th><spring:message code="producttype.name"
        												text="Product Type" /></th>
        										<th><spring:message code="retry.operator.name"
        												text="Retry Operator Name" /></th>
        										<th><spring:message code="aggregator.txn.id"
        												text="#aggregator.txn.id#" /></th>
        										<th><spring:message code="operator.txn.id"
        												text="#operator.txn.id#" /></th>
        										<th><spring:message code="aggregator.name"
        												text="Aggregator Name" /></th>
        										<th><spring:message code="aggregator.response.code"
        												text="#aggregator.response.code#" /></th>
        										<th><spring:message code="aggregator.response.msg"
        												text="#aggregator.response.msg#" /></th>
        										<th><spring:message code="final.retry"
        										text="Final Retry" /></th>
        										<th><spring:message code="request.time"
        												text="#request.time#" /></th>
        										<th><spring:message code="response.time"
        												text="#response.time#" /></th>
        									</tr>
        								</thead>

        								<tbody class="operator-retry-rows">
        									<c:if test="${not empty operaratorRetryDatas}">
        										<c:forEach var="operaratorRetryData" items="${operaratorRetryDatas}">
        											<tr style="display: none;">
        												<td>${operaratorRetryData.retryId}</td>
        												<td>${operaratorRetryData.productType}</td>
        												<td>${operaratorRetryData.retryOperatorName}</td>
        												<td>${operaratorRetryData.aggrTransId}</td>
        												<td>${operaratorRetryData.oprTransId}</td>
        												<td>${operaratorRetryData.aggrName}</td>
        												<td>${operaratorRetryData.aggrRespCode}</td>
        												<td>${operaratorRetryData.aggrRespMsg}</td>
        												<td>${operaratorRetryData.finalRetry}</td>
        												<td>${operaratorRetryData.createdTime}</td>
        												<td>${operaratorRetryData.updatedTime}</td>
        											</tr>
        										</c:forEach>
        									</c:if>
        								</tbody>
        							</table>
        							<a href="javascript:void(0);" class="btn btn-mini"
        								id="operator-retry-show-more">Show more</a>
        						</c:when>
        						<c:otherwise>
        						</c:otherwise>
        					</c:choose>
        				</div>
        			</div>
        		</section>

        		<!-- -------------------------------Failed Promocode ---------------->
        		<section class="unit">
<!--         		<div class="layer"> -->
<!--                 					<div> -->
<%--                 						<c:choose> --%>
<%--                 							<c:when test="${not empty rewardHistoryMapList}"> --%>
<!--                 								<hr> -->
<!--                 							<h4> -->
<!--                 								<span class="label label-info">Failed promocode details</span> -->
<!--                 							</h4> -->
<!--                 								<table class="table table-bordered table-hover"> -->
<!--                 									<thead> -->
<!--                 										<tr> -->
<!--                 											<th>Order Id</th> -->
<!--                 											<th>Service Number</th> -->
<!--                 											<th>Date</th> -->
<!--                 											<th>Campaign</th> -->
<!--                 											<th>Trigger</th> -->
<!--                 											<th>Details</th> -->
<!--                 											<th>Product Type</th> -->
<!--                 											<th>Past Successful Order Ids</th> -->
<!--                 										</tr>	 -->
<!--                 									</thead> -->
<!--                 									<tbody> -->
<%--                 										<c:forEach var="pcItemMap" --%>
<%--                 											items="${rewardHistoryMapList}"> --%>
<!--                 											<tr> -->
<%--                 											    <td>${pcItemMap['orderId']}</td> --%>
<%--                 												<td>${pcItemMap['serviceNumber']}</td> --%>
<%--                 												<td>${pcItemMap['createdDate']}</td> --%>
<%--                 												<td>${pcItemMap['campaign']}</td> --%>
<%--                 												<td>${pcItemMap['trigger']}</td> --%>
<%--                 												<td>${pcItemMap['details']}</td> --%>
<%--                 												<td>${pcItemMap['productType']}</td> --%>
<!--                 												<td> -->
<%--                 												<c:forEach items="${pcItemMap['pastOrderIds']}" var="entry"> --%>
<%--                                                                    <a href="/admin/customertrail/getcustomertrail?orderId=${entry.key}">${entry.key}</a> - ${entry.value}, --%>
<%--                                                                 </c:forEach></td> --%>
<!--                 											</tr> -->
<%--                 										</c:forEach> --%>
<!--                 									</tbody> -->
<!--                 								</table> -->
<%--                 							</c:when> --%>
<%--                 							<c:otherwise> --%>
<%--                 							</c:otherwise> --%>
<%--                 						</c:choose> --%>
<!--                 					</div> -->

        			<div class="layer">
        				<div>
        					<c:choose>
        						<c:when test="${not empty failedPromocodeDataMap}">
        							<h4>
        								<span class="label label-info">Failed promocode details</span>
        							</h4>
        							<table class="table table-bordered table-hover">
        								<thead>
        									<tr>
        										<th>Promocode</th>
        										<th>Failed message</th>
                                                <th>Re-issued Promocode</th>
        									</tr>
        								</thead>
        								<tbody>
        									<tr>
        										<td>${failedPromocodeDataMap['promocode']}</td>
        										<td>${failedPromocodeDataMap['failedMsg']}</td>
                                                <td>${failedPromocodeDataMap['reIssuedPromocode']}</td>
        									</tr>
        								</tbody>
        							</table>
        						</c:when>
        					</c:choose>
        				</div>
        			</div>
        		</section>
        		<!-- ------------------------------End of failed promocode----------->
        		       		
        		<!-- -------------------------------Campaign History---------------->
        		<section class="unit">
	        		<div class="layer">
	   					<div>
							<jsp:include page="rewardhistory.jsp" />
	   					</div>
	   				</div>
        		</section> 
        		        		
        		<!------------------------ binoffer-instant discount-------------------->
        		<section class="unit">
        			<div class="layer">
        				<div>
        					<c:choose>
        						<c:when test="${not empty binOfferInstantList}">
        							<hr>
        							<h4>
        								<spring:message code="binoffer.instant.discount"
        									text="#binoffer.instant.discount#" />
        							</h4>
        							<table class="table table-bordered table-hover">
        								<thead>
        									<tr>
        										<th><spring:message code="id" text="#id#" /></th>
        										<th><spring:message code="bank" text="#bank#" /></th>
        										<th><spring:message code="name" text="#name#" /></th>
        										<th><spring:message code="bin" text="#bin#" /></th>
        										<th><spring:message code="offer.key" text="#offer.key#" />
        										</th>
        										<th><spring:message code="offer.type" text="#offer.type#" />
        										</th>
        										<th><spring:message code="discount.type"
        												text="#discount.type#" /></th>
        										<th><spring:message code="discount" text="#discount#" />
        										</th>
        										<th><spring:message code="min.recharge.amount"
        												text="#min.recharge.amount#" /></th>
        										<th><spring:message code="coupon.factor"
        												text="#coupon.factor#" /></th>
        										<th><spring:message code="short.description"
        												text="#short.description#" /></th>
        										<th><spring:message code="valid.from" text="#valid.from#" />
        										</th>
        										<th><spring:message code="valid.upto" text="#valid.upto#" />
        										</th>
        										<th><spring:message code="bin.instant.txn.amount"
        												text="#bin.instant.txn.amount#" /></th>
        										<th><spring:message code="bin.instant.status"
        												text="#bin.instant.status#" /></th>
        									</tr>
        								</thead>
        								<tbody>
        									<c:forEach var="binOfferData" items="${binOfferInstantList}">
        										<tr>
        											<td>${binOfferData.binOffer.id}</td>
        											<td>${binOfferData.binOffer.bank}</td>
        											<td>${binOfferData.binOffer.name}</td>
        											<td>${binOfferData.binOffer.bin}</td>
        											<td>${binOfferData.binOffer.offerKey}</td>
        											<td>${binOfferData.binOffer.offerType}</td>
        											<td>${binOfferData.binOffer.discountType}</td>
        											<td>${binOfferData.binOffer.discount}</td>
        											<td>${binOfferData.binOffer.minRechargeAmount}</td>
        											<td>${binOfferData.binOffer.couponFactor}</td>
        											<td>${binOfferData.binOffer.shortDescription}</td>
        											<td><fmt:formatDate type="both" pattern="dd-MMM-yyyy"
        													value="${binOfferData.binOffer.validFrom.time}" /> <fmt:formatDate
        													type="time" timeStyle="short"
        													value="${binOfferData.binOffer.validFrom.time}" /></td>
        											<td><fmt:formatDate type="both" pattern="dd-MMM-yyyy"
        													value="${binOfferData.binOffer.validUpto.time}" /> <fmt:formatDate
        													type="time" timeStyle="short"
        													value="${binOfferData.binOffer.validUpto.time}" /></td>
        											<td>${binOfferData.amount}</td>
        											<td>${binOfferData.status}</td>
        										</tr>
        									</c:forEach>
        								</tbody>
        							</table>
        						</c:when>
        						<c:otherwise>
        						</c:otherwise>
        					</c:choose>
        				</div>
        			</div>
        		</section>
        		<!-- ------------------------------ BinBased CashBack------------------------- -->
        		<section class="unit">
        			<div class="layer">
        				<div>
        					<c:choose>
        						<c:when test="${not empty binOfferCashBackList}">
        							<hr>
        							<h4>
        								<spring:message code="binoffer.cashback"
        									text="#binoffer.cashback#" />
        							</h4>
        							<table class="table table-bordered table-hover">
        								<thead>
        									<tr>
        										<th><spring:message code="id" text="#id#" /></th>
        										<th><spring:message code="bank" text="#bank#" /></th>
        										<th><spring:message code="name" text="#name#" /></th>
        										<th><spring:message code="bin" text="#bin#" /></th>
        										<th><spring:message code="offer.key" text="#offer.key#" />
        										</th>
        										<th><spring:message code="offer.type" text="#offer.type#" />
        										</th>
        										<th><spring:message code="discount.type"
        												text="#discount.type#" /></th>
        										<th><spring:message code="discount" text="#discount#" />
        										</th>
        										<th><spring:message code="min.recharge.amount"
        												text="#min.recharge.amount#" /></th>
        										<th><spring:message code="coupon.factor"
        												text="#coupon.factor#" /></th>
        										<th><spring:message code="short.description"
        												text="#short.description#" /></th>
        										<th><spring:message code="valid.from" text="#valid.from#" />
        										</th>
        										<th><spring:message code="valid.upto" text="#valid.upto#" />
        										</th>
        										<th><spring:message code="bin.cashback.amount"
        												text="#bin.cashback.amount#" /></th>
        										<th><spring:message code="bin.cashback.status"
        												text="#bin.cashback.status#" /></th>
        									</tr>
        								</thead>
        								<tbody>
        									<c:forEach var="binOfferData" items="${binOfferCashBackList}">
        										<tr>
        											<td>${binOfferData.binOffer.id}</td>
        											<td>${binOfferData.binOffer.bank}</td>
        											<td>${binOfferData.binOffer.name}</td>
        											<td>${binOfferData.binOffer.bin}</td>
        											<td>${binOfferData.binOffer.offerKey}</td>
        											<td>${binOfferData.binOffer.offerType}</td>
        											<td>${binOfferData.binOffer.discountType}</td>
        											<td>${binOfferData.binOffer.discount}</td>
        											<td>${binOfferData.binOffer.minRechargeAmount}</td>
        											<td>${binOfferData.binOffer.couponFactor}</td>
        											<td>${binOfferData.binOffer.shortDescription}</td>
        											<td><fmt:formatDate type="both" pattern="dd-MMM-yyyy"
        													value="${binOfferData.binOffer.validFrom.time}" /> <fmt:formatDate
        													type="time" timeStyle="short"
        													value="${binOfferData.binOffer.validFrom.time}" /></td>
        											<td><fmt:formatDate type="both" pattern="dd-MMM-yyyy"
        													value="${binOfferData.binOffer.validUpto.time}" /> <fmt:formatDate
        													type="time" timeStyle="short"
        													value="${binOfferData.binOffer.validUpto.time}" /></td>
        											<td>${binOfferData.amount}</td>
        											<td>${binOfferData.status}</td>
        										</tr>
        									</c:forEach>
        								</tbody>
        							</table>
        						</c:when>
        						<c:otherwise>
        						</c:otherwise>
        					</c:choose>
        				</div>
        			</div>
        		</section>

        		<!-- ------------------------------ Recharge Schedule ------------------------- -->
                <section>
                  <c:if test="${not empty rechargeScheduleList}">
                    <hr>
                    <h4>
                      <span class="label label-info"><spring:message code="recharge.schedule" text="#recharge.schedule#" /></span>
        			</h4>

                    <table class="table table-bordered table-hover">
        	            <thead>
        	              <tr>
        	                <th><spring:message code="schedule.id" text="#schedule.id#" /></th>
        	                <th><spring:message code="order.id" text="#order.id#" /></th>
        	                <th><spring:message code="schedule.status" text="#schedule.status#" /></th>
        	                <th><spring:message code="schedule.reason" text="#schedule.reason#" /></th>
        	                <th><spring:message code="schedule.trigger.event" text="#schedule.trigger.event#" /></th>
        	                <th><spring:message code="schedule.creationtime" text="#schedule.creationtime#" /></th>
        	                <th><spring:message code="schedule.expirytime" text="#schedule.expirytime#" /></th>
        	                <th><spring:message code="schedule.createdby" text="#schedule.createdby#" /></th>
        	                <th><spring:message code="schedule.execute" text="#schedule.execute#" /></th>
        	                <th><spring:message code="schedule.cancel" text="#schedule.cancel#" /></th>
        	              </tr>
        	            </thead>

        	            <tbody>
        	              <c:forEach var="rechargeSchedule" items="${rechargeScheduleList}">
        	              <tr>

        	                <td>${rechargeSchedule.scheduleId}</td>
        	                <td>${rechargeSchedule.orderId}</td>
        	                <td>${rechargeSchedule.status}</td>
        	                <td>${rechargeSchedule.scheduleReason}</td>
        	                <td>${rechargeSchedule.triggerEvent}</td>

                            <td>
                              <fmt:formatDate type="both" pattern="dd-MMM-yyyy hh:mm a" value="${rechargeSchedule.createdTime}" />
                            </td>

                            <td>
                              <fmt:formatDate type="both" pattern="dd-MMM-yyyy hh:mm a" value="${rechargeSchedule.expiryTime}" />
                            </td>

                            <td>${rechargeSchedule.createdBy}</td>

                            <c:choose>
                              <c:when test="${rechargeSchedule.status eq 'Scheduled'}">
                                <td>
                                  <form name="executeScheduleForm" id="executeScheduleForm" style="float: right; margin-right: 12px">
                                    <input type="hidden" name="orderId" value="${rechargeSchedule.orderId}">
                                    <button type="submit" class="btn btn-mini btn-warning">Recharge Now</button>
                                  </form>
                                </td>
                                <td>
                                  <form name="cancelScheduleForm" id="cancelScheduleForm" style="float: right; margin-right: 12px">
                                    <input type="hidden" name="orderId" value="${rechargeSchedule.orderId}">
                                    <button type="submit" class="btn btn-mini btn-warning">Cancel And Refund</button>
                                  </form>
                                </td>
                              </c:when>
                              <c:otherwise>
                                <td>
        	                      <spring:message code="schedule.no.action" text="#schedule.no.action#" />
        	                    </td>
        	                    <td>
        	                      <spring:message code="schedule.no.action" text="#schedule.no.action#" />
        	                    </td>
                              </c:otherwise>
                            </c:choose>

        	              </tr>
        	              </c:forEach>
        	            </tbody>
                    </table>
                  </c:if>
                </section>
        	</div>

           <!-- ---------- Growth event status ------------ -->
          <c:if test="${not empty eventsList}">
           <section class="unit">
           	<div class="layer">
            <hr>
            	<h4>
                	<span class="label label-info">Growth event status</span>
        		</h4>

               <form name="geventStatus" id="geventStatus">
           		<select class="input" name="eventType" required>
              		<option value="">Event Type</option>
              		<c:forEach items="${eventsList}" var="event">
              			<option value="${event}">${event.description}</option>
              		</c:forEach>
           		</select>
           		<input type="hidden" name="searchKey" value="ORDER_ID">
           		<input type="hidden" name="value" value="${order.orderId}">
           		<input type="submit" class="btn btn-mini" name="Event status"/>
           	   </form>


            <div style="width:100%;overflow:auto;display: none;" id="eventStatusDetails-wrapper">
        		<table id="eventStatusDetails" class="table table-bordered table-hover" style="font-size:12px;">
        			<thead>
        				<tr>
        					<th>Event id</th>
        					<th>Event name</th>
        				    <th>Email</th>
        					<th>Orderid</th>
        					<th>Recharge numb</th>
        					<th>Status</th>
        					<th>Details</th>
        					<th>Timestamp</th>
        					<th>Retrigger SMS</th>
        				</tr>
        			</thead>
        				<tbody style="word-break:break-word;">
        				</tbody>
        		</table>
        	</div>
            </div>
            </section>
          </c:if>
        	<a href="#" id="new-campaign-status" class="btn btn-info">Get New CampaignStatus</a>
        	<div id="new-campaign-status-div" style="display: hidden"></div>
        	<%--user ban popup--%>
        	<div id="user-ban" class="modal fade" tabindex="-1" role="dialog"
        		aria-labelledby="myModalLabel" aria-hidden="true" style="height: 25%">
        		<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal"
        				aria-hidden="true">x</button>
        			<h3 id="myModalLabel">Login</h3>
        		</div>
        		<form class="form-horizontal no-margin" id="user-ban-form">
        			<div class="modal-body">
        				<div class="control-group">
        					<label class="control-label" for="inputPassword">Password</label>
        					<div class="controls">
        						<input type="password" id="inputPassword" placeholder="Password">
        					</div>
        				</div>
        			</div>
        			<div class="modal-footer">
        				<button type="submit" class="btn btn-primary"
        					id="user-ban-form-button">Sign in</button>
        			</div>
        		</form>
        	</div>

        	<div id="user-email" class="modal fade" tabindex="-1"
        		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="height:25%">
        		<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal"
        				aria-hidden="true">x</button>
        			<h3 id="myModalLabel">Login</h3>
        		</div>
        		<form class="form-horizontal no-margin" id="user-email-form">
        			<div class="modal-body">
        				<div class="control-group">
        					<label class="control-label" for="inputPassword">Password</label>
        					<div class="controls">
        						<input type="password" id="inputPasswordValue"
        							placeholder="Password">
        					</div>
        				</div>
        			</div>
        			<div class="modal-footer">
        				<button type="submit" class="btn btn-primary" id="user-email-button">Sign
        					in</button>
        			</div>
        		</form>
        	</div>

        	<div style="display: none;" id="reissue-form-value">
        		<form id="user-ban-form">
        			<select name="couponDetails" id="couponDetails">
        				<option value="true">true</option>
        				<c:forEach items="${reissueCouponObjList}" var="value">
        					<option value="${value}">${value}</option>
        				</c:forEach>
        			</select> <input type="text" id="input-coupon-quantity"
        				placeholder="Coupon quantity.."></br>
        			<button type="add" class="btn" id="reissue-coupon-trigger">Submit</button>
        		</form>
        	</div>

        	<!-- Setting Url for customer trail -->
        	<c:url value="/admin/customertrail/getcustomertrail" var="custTrailURL">
        		<c:param name="orderId" value="${order.orderId}" />
        		<c:param name="calledFrom" value="customerTrail" />
        	</c:url>

            <!-- Modal for cash back -->
            <div class="modal fade" id="cashBackModal" tabindex="-1" role="dialog"
                aria-labelledby="myModalLabel" style="display: none; height:30%;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Cash back</h4>
                        </div>
                        <div class="modal-body">
                            <form name="cashBackForm" id="cashBackForm">
                                <div class="field-unit">
                                    <input type="text" name="orderIdCashBack" id="orderIdCashBack"
                                        value="${order.orderId}" readonly="readonly" /> <label>Cash
                                        back amount</label> <input type="text" id="cashBackAmount"
                                        name="cashBackAmount" />
                                </div>

                                <div id="cashBackResponse" class="alert alert-error"
                                    style="display: none;"></div>

                                <div class="modal-footer">
                                    <a href="${custTrailURL}"><spring:message code="close"
                                            text="#close#" /></a>
                                    <button type="submit" class="btn btn-primary" id="cashbackFormSubmit">Do
                                        cashback</button>
                                </div>
                            </form>
                        </div>
            </div><!-- /.modal -->
        <!-- End of cash back model -->

        <!-- Bank refund modal -->
            <div id="bankRefundModal" class="modal  fade" tabindex="-1"
                role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="height: 40%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">x</button>
                    <h3 id="myModalLabel">Refund to bank through PG</h3>
                </div>
                <form class="form-horizontal no-margin" id="user-wallet-credit-form">
                    <div class="modal-body">
                        <div class="control-group">
                            <label class="control-label" for="refundBankPasswordValue">Payment Txn Id</label>
                            <div class="controls">
                                <input type="text" name="paymentTxnIdForBankRefund" id="paymentTxnIdForBankRefund" value="${paymentTransaction.paymentTxnId}">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="amountForBankRefund">Amount to refund</label>
                            <div class="controls">
                                <input type="text" name="amountForBankRefund" id="amountForBankRefund" value="${paymentTransaction.amount}">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="refundBankPasswordValue">Password</label>
                            <div class="controls">
                                <input type="password" id="refundBankPasswordValue"
                                    placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"
                            id="bank-refund-button">Refund</button>
                    </div>
                </form>
            </div>

            <div class="modal fade" id="resultRefundBank" tabindex="-1" role="dialog"
                aria-labelledby="myModalLabel" style="display: none; height:40%;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Result of Refund To Bank</h4>
                        </div>
                        <div class="modal-body">
                                <div id="refundToBankResultMsg" class="alert alert-info"></div>
                                <div class="modal-footer">
                                    <a a class="btn btn-primary" type="button" href="${custTrailURL}">OK</a>
                                </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
         <!-- End of Bank refund -->
            <!-- Modal for reversal result -->
            <div class="modal fade" id="reversalResult" tabindex="-1" role="dialog"
                aria-labelledby="myModalLabel" style="display: none; height:30%">
                        <div class="modal-header">
                            <h4 class="modal-title">Reversal result</h4>
                        </div>
                        <div class="modal-body">
                                <div id="reversalResultMsg" class="alert alert-error"></div>
                                <div class="modal-footer">
                                    <a a class="btn btn-primary" type="button" href="${custTrailURL}">OK</a>
                                </div>
                        </div>
            </div><!-- /.modal -->

            <!-- Modal for schedule handling -->
            <div class="modal fade" id="executeScheduleResult" tabindex="-1" role="dialog"
                aria-labelledby="myModalLabel" style="display: none; height:30%">
                        <div class="modal-header">
                            <h4 class="modal-title">Execute result</h4>
                        </div>
                        <div class="modal-body">
                                <div id="executeScheduleResultMsg" class="alert alert-error"></div>
                                <div class="modal-footer">
                                    <a a class="btn btn-primary" type="button" href="${custTrailURL}">OK</a>
                                </div>
                        </div>
            </div><!-- /.modal -->

            <div class="modal fade" id="cancelScheduleResult" tabindex="-1" role="dialog"
                aria-labelledby="myModalLabel" style="display: none; height:30%">
                        <div class="modal-header">
                            <h4 class="modal-title">Cancel result</h4>
                        </div>
                        <div class="modal-body">
                                <div id="cancelScheduleResultMsg" class="alert alert-error"></div>
                                <div class="modal-footer">
                                    <a a class="btn btn-primary" type="button" href="${custTrailURL}">OK</a>
                                </div>
                        </div>
            </div><!-- /.modal -->

           <div class="modal fade" id="manualRefundModal" tabindex="-1" role="dialog"
           	   aria-labelledby="myModalLabel"  style="display: none; height: 20%">
        	   <div class="modal-header">
               	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                  <h4 id="myModalLabel">Manual refund</h4>
               </div>
        	   <div class="modal-body" >
        	   	  <form name="manualRefundForm" method="POST" action="/admin/refund/manual/doCtManualRefund.htm">
        		  	<div><input type="text" id="mtxnid" name="orderIds"></div>
        		  	<div><button type="submit" class="btn btn-small btn-primary">Do refund</button></div>
        		  </form>
        	   </div>
           </div><!-- /.modal -->

        	<script type="text/javascript">

        $(document).ready(function(){

            var customerTrailRows = $('.customer-trail-rows').find('tr');
            customerTrailRows.eq(0).show();

            if(customerTrailRows.length < 2){
                $('#show-more').hide();
            }

            $('#show-more').on('click', function(event) {
                var value = $(this).text();
                value === 'Show more' ? value = 'Show less' : value = 'Show more';
                $(this).text(value);
                $('.customer-trail-rows').find('tr').toggle();
                $('.customer-trail-rows').find('tr').eq(0).show();
            })

            var operatorRetryRows = $('.operator-retry-rows').find('tr');
            operatorRetryRows.eq(0).show();

            if(operatorRetryRows.length < 2){
                $('#operator-retry-show-more').hide();
            }

            $('#operator-retry-show-more').on('click', function(event) {
                var value = $(this).text();
                value === 'Show more' ? value = 'Show less' : value = 'Show more';
                $(this).text(value);
                $('.operator-retry-rows').find('tr').toggle();
                $('.operator-retry-rows').find('tr').eq(0).show();
            })


        $("#reissue-coupon-trigger").on('click', function() {
            var orderId = "${order.orderId}";
            var resultValue;
            var status;
            var couponQuantity;
            var couponId ;
            var couponString;
            var formData;

            couponString = $("#couponDetails").val();reversalResultMsg
            couponString = $("#couponDetails").val().split("|");
            couponId = couponString[3];
            couponQuantity = $("#input-coupon-quantity").val();

            var deletedCouponInventoryDetails = couponId + '-' + couponId + '-' + 'false';
            var reissueCouponInventoryDetails = couponId + '-' + couponQuantity + '-' + 'false';
            var fcTicketId = 123;
            var remarks = 'Remarks';
            var createdBy = 'Nemash';
            var deletedBy = 'Nemash';
            var url = '/reissueCompensatoryCoupons.htm';
            var formValue = 'orderId=' + orderId + '&deletedCouponInventoryDetails=' + deletedCouponInventoryDetails + '&reissueCouponInventoryDetails=' + reissueCouponInventoryDetails + '&fcTicketId=' + fcTicketId + '&remarks=' + remarks + '&createdBy=' + createdBy + '&deletedBy=' + deletedBy;
            alert(formValue);
            $.ajax({
                async:false,
                url: url,
                type: "GET",
                data: formData,
                dataType: "json"
            }).done( function(response) {
                resultValue = response.RESULT;
                status = response.STATUS;
                if(status=='success') {
                    alert(resultValue);
                } else {
                alert(resultValue);
            }
        });
        });


        $("#user-crossell-email-retrigger").on('click', function() {
                var orderId = "${order.orderId}";
                var resultValue;
                var status;
                $.ajax({
                    url: '/protected/crosssell/api/retrigger_crossellemail.htm?orderid=' + orderId,
                    data: { get_param: 'value' }
                }).done( function(response) {
                    response = $.parseJSON(response);
                    status = response.STATUS;
                    if(status=='success') {
                    alert("Retrigger Email Success.");
                    } else {
                    alert("Retrigger Email FAILED.");
                }

        });
        });

        $("#user-email-retrigger").on('click', function() {
                var orderId = "${order.orderId}";
                var resultValue;
                var status;
                alert("email retrigger");
                $.ajax({
                    url: '/protected/retrigger_email.htm?orderid=' + orderId,
                    data: { get_param: 'value' }
                }).done( function(response) {
                    resultValue = response.RESULT;
                    status = response.STATUS;
                    if(status=='success') {
                    alert(resultValue);
                    } else {
                    alert(resultValue);
                }

        });
        });

        $("#user-ban-form-button").on('click', function(e){
                e.preventDefault();
                var resultValue;
                $this = $(this);
                emailId = "${user.email}";
                $.ajax({
                url: '/admin/user/ban/UserValidation.htm?password=' + $('input[id="inputPassword"]').val(),
                data: { get_param: 'value' }
            }).done( function (response){
                response = $.parseJSON(response);
                resultValue = response.result;
                if (resultValue == 'success') {
                $('#user-ban').modal('hide');
                $.ajax({
                url: '/admin/user/ban/updateUserCurrentStatus.htm?email=' + emailId,
                data: { get_param: 'value' }
            }).done( function (response){
                response = $.parseJSON(response);
                resultValue = response.result;
                if (response.result == 'User InActive Successfully') {
                    $("#user-status-id").text('InActive');
                    alert("User InActive Successfully");
                } else if(response.result == 'User Active Successfully') {
                    $("#user-status-id").text('Active');
                    alert("User Active Successfully");
            }
            });
                } else {
                    $('#user-ban').modal('hide');
                    alert("invalid password");
        }
            });

        });

        $(".recharge-status-check").on('click', function(){
            var result,
                $this = $(this);
            // find recharge request status
            // find the request id
            var order_id = $this.attr('orderid');
        	var product_type= $(this).attr('data-producttype');
            $.blockUI({ css: {
                   border: 'none',
                   padding: '15px',
                   backgroundColor: '#000',
                   '-webkit-border-radius': '10px',
                   '-moz-border-radius': '10px',
                   opacity: .5,
                   color: '#fff',
                   'z-index' : '5001'
               },
               overlayCSS: {
                   'z-index' : '5000'
               }
                });

            // send request - ajax
            $.ajax({
                url: '/admin/rechargeRequestStatus/getStatusCheck?orderId=' + order_id + '&' + 'productType=' + product_type ,
                //dataType: "text"
                data: { get_param: 'value' }

            }).done( function (response){
                $.unblockUI();
                var result = response.rechargeStatus;
                //response = $.parseJSON(response);
                //var result = response.string;
                if(!alert(result)){window.location.href="/admin/customertrail/getcustomertrail?orderId="+ order_id + "&" + "calledFrom=customerTrail"}

                //$this.text(request_id + "-" + result);

            });
        });


        $(".payment-txn-id").on('click', function(){
            var result,
                $this = $(this);
            // find payment request status
            var paymentTxnId = $this.text();
            $.blockUI({ css: {
                   border: 'none',
                   padding: '15px',
                   backgroundColor: '#000',
                   '-webkit-border-radius': '10px',
                   '-moz-border-radius': '10px',
                   opacity: .5,
                   color: '#fff',
                   'z-index' : '5001'
               },
               overlayCSS: {
                   'z-index' : '5000'
               }
                });

            // send payment request - ajax<a href="#user-email" data-toggle="modal" >${user.email}</a>
            $.ajax({
                url: '/admin/paymentAdminController/statusRequest?paymentTxnId=' + paymentTxnId,
                data: { get_param: 'value' }

            }).done( function (response){
                $.unblockUI();
                result = response.PAYMENT_STATUS;
                alert(result);
                //if(result == 'fail') {
                //  $this.addClass("label label-success");
                //}


            });
        });

            $(".more-info").on("click", function(){

                if($(this).prev("div").is(':visible')){
                    $(this).prev("div").slideUp(200);
                    $(this).text("Show more");
                }
                else {
                    $(this).prev("div").slideDown(200);
                    $(this).text("Show less");
                }


            });

                $("#reissue-coupons").on("click" , "#compensate", function(e){
                var value;
                var listOfCouponsInfo;
                var couponInfo;
                e.preventDefault();
                var selectInputHandle = "#reissue-coupons .reissue-coupons-select"
                if($(selectInputHandle + ":checked").length > 0){

                    $(selectInputHandle).attr("disabled","disabled")
                    $("#reissue-form-value").show();
                    $.ajax({
                    url : '/reissuecoupon/api/trigger.htm',
                    }).done( function(response) {
                        value = response.STATUS;
                        if (value=='SUCCESS') {
                            alert(value + 'value');
                            $('#reissue-form-value').show();
                        } else if (value=='FAILED') {
                                alert(response.RESULT);

                        } else if (value=='EXCEPTION') {
                                alert(response.RESULT)
                        }

                    });

                } else {
                    $(selectInputHandle).removeAttr("disabled")
                    alert("Please select coupons to replace");
                }

            });

             //Cash back submit script
             $('#cashBackForm').submit(function(e) {
            	 $(this).find('#cashbackFormSubmit').prop('disabled','disabled');
                    // will pass the form data using the jQuery serialize function
                    $.post('/admin/refund/manual/cashBack-customerTrail', $(this).serialize(), function(response) {
                    	$('#cashBackForm').find('#cashbackFormSubmit').removeAttr('disabled');
                    	if(response.status=='success') {
                            $('#cashBackResponse').show();
                            $('#cashBackResponse').removeClass("alert-error").addClass("alert-success");
                            $('#cashBackResponse').text(response.message);
                        } else {
                            $('#cashBackResponse').show();
                            $('#cashBackResponse').removeClass("alert-success").addClass("alert-error");
                            $('#cashBackResponse').text(response.message);
                        }
                    });

                    e.preventDefault(); // prevent actual form submit and page reload
             });

             //Bank refund call
            $("#bank-refund-button").on('click', function(e) {
                 var resultValue;
                 var responsekey;
                 e.preventDefault();
                 var ptxn = $('input[id="paymentTxnIdForBankRefund"]').val();
                 var amt = $('input[id="amountForBankRefund"]').val();
                 var refundMode = 'partial';
                 $.ajax({
                     url: '/admin/user/ban/UserValidation.htm?password=' + $('input[id="refundBankPasswordValue"]').val(),
                     data: { get_param: 'value' },
                     dataType: 'json',
                     cache: false
                 }).done( function(response) {
                     resultValue = response.result;
                     $('#bankRefundModal').modal('hide');
                     if(resultValue=='success') {
                         $.ajax({
                             type: 'get',
                             dataType: 'json',
                             url: '/admin/paymentAdminController/refundToPgViaWallet?paymentTxnId=' + ptxn + '&' + 'amountToBank=' + amt + '&' + 'bankRefundMode=' + refundMode,
                         }).done( function (responseValue){
                             $("#refundToBankResultMsg").text(responseValue.result);
                             $('#resultRefundBank').modal('show');
                         });
                     } else {
                         alert("password is incorrect");
                     }
               });
         });
         //End of bank refund
            $('#reversalForm').submit(function(e) {
                 // will pass the form data using the jQuery serialize function
                $.post('/admin/rechargeRequestStatus/reversal-customertrail', $(this).serialize(), function(response) {
                     if(response.callStatus=='success') {
                         $('#reversalResult').modal('show');
                         $('#reversalResultMsg').removeClass("alert-error").addClass("alert-success");
                         $('#reversalResultMsg').text(response.message);
                     } else {
                    	 $('#reversalResult').modal('show');
                         $('#reversalResultMsg').removeClass("alert-success").addClass("alert-error");
                         $('#reversalResultMsg').text(response.message);
                     }
                 });
                 e.preventDefault(); // prevent actual form submit and page reload
            });

            $('#geventStatus').submit(function(e) {
            	var isAuthorize = "${isAuthorize}";
                 // will pass the form data using the jQuery serialize function
                $.get('/admin/eventrewards/getRewardStatusForCt', $(this).serialize(), function(response) {
                	var growtheventlist = "";
                	for(var i=0; i<response.length; i++){
                		growtheventlist += "<tr><td>"+response[i].growthEventId+"</td><td>"+response[i].eventName+"</td><td>"+
                		response[i].emailId+"</td><td>"+response[i].orderId+"</td><td>"+response[i].rechargeMobileNumber+"</td><td>"+
                		response[i].status+"</td><td>"+response[i].details+"</td><td>"+
                		response[i].createdAt+"</td>";
                		if (response[i].retriggerSMSButtonEnable == "true" && isAuthorize == 'yes') {
                			growtheventlist += "<td><input type = 'button' id = 'triggerButton' style='font-size: 12px;'onclick = 'retriggerSMS(this,\""+ response[i].rechargeMobileNumber +"\",\""+ response[i].rewardValue +"\",\""+ response[i].growthEventId +"\")' value='Retrigger SMS'></td></tr>";
                		} else {
                			growtheventlist += "<td>"+" "+"</td></tr>"
                		}
                	}
                	$("table#eventStatusDetails TBODY").html(growtheventlist);
                	$("#eventStatusDetails-wrapper").show();
                 });
                 e.preventDefault(); // prevent actual form submit and page reload
            });

            $('#executeScheduleForm').submit(function(e) {
                // will pass the form data using the jQuery serialize function
                 $.post('/admin/rechargeRequestStatus/schedule/execute', $(this).serialize(), function(response) {
                    if(response.status=='success') {
                        $('#executeScheduleResult').modal('show');
                        $('#executeScheduleResultMsg').removeClass("alert-error").addClass("alert-success");
                        $('#executeScheduleResultMsg').text(response.message);
                    } else {
                   	    $('#executeScheduleResult').modal('show');
                        $('#executeScheduleResultMsg').removeClass("alert-success").addClass("alert-error");
                        $('#executeScheduleResultMsg').text(response.message);
                    }
                });
                e.preventDefault(); // prevent actual form submit and page reload
           });

            $('#cancelScheduleForm').submit(function(e) {
               // will pass the form data using the jQuery serialize function
                $.post('/admin/rechargeRequestStatus/schedule/cancel', $(this).serialize(), function(response) {
                   if(response.status=='success') {
                       $('#cancelScheduleResult').modal('show');
                       $('#cancelScheduleResultMsg').removeClass("alert-error").addClass("alert-success");
                       $('#cancelScheduleResultMsg').text(response.message);
                   } else {
                  	    $('#cancelScheduleResult').modal('show');
                       $('#cancelScheduleResultMsg').removeClass("alert-success").addClass("alert-error");
                       $('#cancelScheduleResultMsg').text(response.message);
                   }
               });
               e.preventDefault(); // prevent actual form submit and page reload
          });

        	$('#new-campaign-status').on('click', function(ev){
        		ev.preventDefault();
        		$.ajax('/admin/eventrewards/new-campaign-status/${order.orderId}', {
        			type: "GET",
        			dataType: "json",
        			success: function(res){
        				$('#new-campaign-status-div').empty();
        				if(res && res != null && res != "null"){
                            for(var i=0;i<res.length;i++){
        						var html = "<ul>";
                                if(res[i] && res[i]!= null && res!="null"){
                                    html+="<li> Reward Value: "+ res[i]['rewardValue']+"</li>";
                                    html+="<li> Details: "+res[i]['params']+"</li>";
                                    if(res[i]['status']){
                                        html+="<li> Status: "+res[i]['status']+"</li>";
                                    }
                                }
                                html = html+"</ul>";
                                $('#new-campaign-status-div').append(html);
                            }
                            $('#new-campaign-status-div').show();
                        }
        			}
        		})
        	});
        });

        function getCityListForState(stateId, cityId){
            var stateId= $('#'+stateId).val();
            var data = 'stateId='+stateId;
            var url='/app/getcitylist.htm';
               $.ajax({
                type:"POST",
                url: url,
                data: data,
                dataType: "json",
                cache: false,
                success: function(responseText){
                   if(responseText.status === 'success'){
                       var obj = responseText.cityMasterList;
                       var cityselect='<option value="" selected="selected">Select City</option>';
                       $.each(obj, function(key, value) {
                               cityselect =cityselect + '<option value="'+value.cityMasterId+'">'+value.cityName+'</option>';
                       });
                       $('#'+cityId).html(cityselect);
                   }else{
                       alert ("error");
                   }
                },
                error:function (e) {
                }
            });
        }

        function retriggerSMS(button, serviceNumber, rewardValue, eventId) {
            $.ajax({
                url: '/admin/eventrewards/retriggerSMS?serviceNumber='+serviceNumber+'&rewardValue='+rewardValue+'&eventId='+eventId,
                type:'GET',
                dataType: 'JSON'
            });
            $(button).prop('disabled',true);
            setTimeout(function(){$(button).prop('disabled',false)}, 300000); // disabling button for 5 mins
        }
        $("")

         $(".manualRefundButton").click(function(){ // Click to only happen on announce links
             $("#mtxnid").val($(this).data('mtxnid'));
             $('#manualRefundModal').modal('show');
           });
        </script>
    </div>
</div>