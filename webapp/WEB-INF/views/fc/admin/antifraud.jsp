<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<body>
<div class="container-fluid">
    <div class="alert" style="display: none">

    </div>
    <div class="row-fluid">
        <div class="span12">
            <h2>Edit Antifraud Properties.</h2>
            <form class="form-horizontal" id="con-form">
                <div class="control-group">
                    <label class="control-label">Property Key</label>
                    <div class="controls">
                    	<label id="key"></label>	
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Property Value</label>
                    <div class="controls">
                    	<div class="control-list" id="prop-value"></div>
                        
                    </div>
                </div>
                <div class="form-actions">
                    <a href="#" id="save-props" class="btn btn-primary">Save</a>
                </div>
            </form>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <table class="table table-bordered table-striped">
                <tr>
                    <th></th>
                    <th>Key</th>
                    <th>Value</th>
                </tr>
                <c:forEach items="${props}" var="prop" varStatus="count">
                    <tr>
                        <td><a href="#" class="btn btn-link edit-prop" data-count="${count.index}" data-key="${prop.key}">edit</a></td>
                        <td>${prop.key}</td>
                        <td id="prop-val-${count.index}">${prop.value}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('a#save-props').on('click', function(ev){
            ev.preventDefault();
            var data = {key: $('#key').html()};
			data['value'] = $('#value').val();
            $.ajax('/admin/antifraud/save', {
                data: data,
                type: 'POST',
                dataType: 'json',
                success: function(result){
                    showMessage("success", result.message);
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                }
            });
        });

        $('a.edit-prop').on('click', function(ev){
            ev.preventDefault();
            $('#prop-value').html('');
            var $el = $(ev.currentTarget);
            $('#key').html($el.data('key'));
            var val = $('#prop-val-'+$el.data('count')).text();
            $('#prop-value').append('<input type="text" value="'+val+'" name="value" id="value">');
            
        });
    });
    function showMessage(status, message){
        var html = message+'<a class="close" href="#">&times;</a>';
        if(status == 'success'){
            $('div.alert').removeClass('alert-error').addClass('alert-success').html(html).show();
        }else {
            $('div.alert').removeClass('alert-success').addClass('alert-error').html(html).show();
        }
    }
</script>
</body>