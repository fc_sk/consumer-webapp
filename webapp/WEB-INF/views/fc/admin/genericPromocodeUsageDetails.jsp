<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<h5>Usage details</h5>
<c:choose>
    <c:when test="${not empty promocodeUsageDetails}">
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th>Order Id</th>
                    <th>Service Number</th>
                    <th>User Email</th>
                    <th>Status</th>
                    <th>Status Date</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${promocodeUsageDetails}" var="promocodeUsageDetail">
                <tr>
                    <td>${promocodeUsageDetail.orderid}</td>
                    <td>${promocodeUsageDetail.phone_no}</td>
                    <td>${promocodeUsageDetail.email}</td>
                    <td>${promocodeUsageDetail.status}</td>
                    <td>${promocodeUsageDetail.statusDate}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:when>
    <c:when test="${not empty exception}">
        <h5>${exception}</h5>
    </c:when>
    <c:otherwise>
        <h5>No usage details for the promocode</h5>
    </c:otherwise>
</c:choose>