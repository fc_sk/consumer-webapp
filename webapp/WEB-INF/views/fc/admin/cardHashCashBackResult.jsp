<%@page import="com.freecharge.admin.CustomerTrailDetails.EmailInputController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
	<div class="col-sm-3 col-md-3">
		<jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp"/>
	</div>

	<div class="col-sm-9 col-md-9">
		<c:choose>
		  <c:when test="${not empty cashBackEntries}">
				<h3>Search Result</h3>
				<table class="table table-hover table-bordered">
					<thead>
						<tr>
							<th>Sr. No.</th>
							<th>Card Hash</th>
							<th><spring:message code="order.id" text="#order.id#"/></th>
							<th><spring:message code="campaign.name" text="#campaign.name"/></th>
							<th><spring:message code="generated.date" text="#generated.date#"/></th>
						</tr>
					</thead>
					<tbody>
					<c:forEach items="${cashBackEntries}" var="cashBackEntries" varStatus="status" >
						<tr>
							<td>${status.count}</td>
							<td>${cashBackEntries.cardHash}</td>
							<td>
							   <c:choose>
								 <c:when test="${empty cashBackEntries.orderId}">
									Not Found
								  </c:when>
								  <c:otherwise>
									  <a href="/admin/customertrail/getcustomertrail?orderId=${cashBackEntries.orderId}"
									  target="${cashBackEntries.orderId}">${cashBackEntries.orderId}</a>
								  </c:otherwise>
							   </c:choose>
							</td>
							<td>${cashBackEntries.ffClass}</td>
							<td>${cashBackEntries.createdAt}</td>
					   </tr>
					</c:forEach>
				   </tbody>
				</table>
		</c:when>
		</c:choose>
	</div>
</div>