<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
	<div class="col-md-9 col-sm-9">
		<div class="customer-trail">
			<div class="cs-display">

				<div class="row">
					<h1>
						<span class="label label-info"><spring:message
								code="usermerchant.history" text="#usermerchant.history#" /></span>
					</h1>
				</div>
				<hr>

				<section class="unit">

					<div class="layer">
						<c:if test="${not empty userMerchantHistory}">
							<ul>
								<!-- <li><em><spring:message code="name"
												text="#name#"/>:</em>${name}</li> -->
								<li><em><spring:message code="email" text="#email#" />:</em>${emailid}</li>
							</ul>
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th><spring:message code="transaction.id"
												text="#transaction.id#" /></th>
										<th><spring:message code="transaction.status"
												text="#transaction.status#" /></th>
										<th><spring:message code="transaction.date"
												text="#transaction.date#" /></th>
										<th><spring:message code="transaction.amount"
												text="#transaction.amount#" /></th>
										<th><spring:message code="transaction.sourceName"
												text="#transaction.sourceName#" /></th>
										<th><spring:message code="transaction.sourceMobile"
												text="#transaction.sourceMobile#" /></th>
										<th><spring:message code="transaction.destName"
												text="#transaction.destName#" /></th>
										<th><spring:message code="transaction.destMobile"
												text="#transaction.destMobile#" /></th>
										<th><spring:message code="transaction.partyType"
												text="#transaction.partyType#" /></th>
										<th><spring:message code="transaction.partyTag"
												text="#transaction.partyTag#" /></th>
										<th><spring:message code="request.type"
												text="#request.type#" /></th>
										<th><spring:message code="transaction.result"
												text="#transaction.result#" /></th>
									</tr>
								</thead>
								<tbody class="customer-trail-rows">
									<c:if
										test="${not empty userMerchantHistory.requestViewTransactionDto}">
										<c:forEach var="transaction"
											items="${userMerchantHistory.requestViewTransactionDto}">
											<tr>
												<td><a
													href="/admin/customertrail/getcustomertrail?orderId=${transaction.txnId}"><c:out
															value="${transaction.txnId}" /></a></td>
												<%--<td>${transaction.txnId}</td>  --%>
												<td>${transaction.txnStatus}</td>
												<td>${transaction.txnDate}</td>
												<td>${transaction.txnAmount}</td>
												<c:if test="${not empty transaction.srcParty}">
													<td>${transaction.srcParty.name}</td>
													<td>${transaction.srcParty.mobileNumber}</td>
												</c:if>
												<c:if test="${not empty transaction.destParty}">
													<td>${transaction.destParty.name}</td>
													<td>${transaction.destParty.mobileNumber}</td>
													<td>${transaction.destParty.partyType}</td>
													<td>${transaction.destParty.partyTag}</td>
												</c:if>
												<td>${transaction.txnType}</td>
												<td>${transaction.requestType}</td>
											</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
						</c:if>
					</div>

				</section>


			</div>



			<script type="text/javascript">
		        $(document).ready(function(){
		
		            var customerTrailRows = $('.customer-trail-rows').find('tr');
		            customerTrailRows.eq(0).show();
				   
		        }
	        </script>
		</div>
	</div>
</div>