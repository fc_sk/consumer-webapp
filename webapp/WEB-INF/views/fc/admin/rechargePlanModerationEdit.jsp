<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<div>
   <a  href="/admin/rechargeplans/moderation.htm"><button type="button" class="btn btn-lg"><b>BACK</b>
</button></a>  
</div>
<br>
  <div id="statusMessage" class="alert alert-success alert-dismissable" style="display:none;">
  </div>
  <div id="errorMessage" class="alert alert-danger alert-dismissable" style="display:none;">
  </div>
<h2>Edit Recharge Plan Moderation</h2>
<form:form name="PlanEditAndSaveForm" id="saveFormId"
   method="post" data-csrf="${mt:csrfToken()}">
	<b><spring:message code="recharge.id" text="#recharge.id#" /></b><br>
	<input type="text" id="planId" value="${planId}" data-plan-id='${planId}' readonly/><br>
	<b><spring:message code="circle.master.id" text="#circle.master.id#"/></b><br>
	<input type="text" id="circleMasterId" value="" readonly /><br>
    <b><spring:message code="operator.master.id" text="#operator.master.id#"/></b><br>
    <input type="text" id="operatorMasterId" value="" readonly/><br>
	<b><spring:message code="name" text="#name#"/></b><br>
	<select type="text" id="name" value="">
        <option value='Topup'>Topup</option>
        <option value='Data/2G'>Data/2G</option>
        <option value='3G'>3G</option>
        <option value='Other'>Other</option>
     </select><br>
	<b><spring:message code="amount" text="#amount#"/></b><br>
	<input type="text" id="amount" value="" readonly/><br>
	<b><spring:message code="talk.time" text="#talk.time#"/></b><br>
	<input type="text" id="talkTime" value=""/><br>
	<b><spring:message code="validity" text="#validity#"/></b><br>
    <input type="text" id="validity" value=""/><br>
	<b><spring:message code="description" text="#description#"/></b><br>
	<textarea type="text" id="description" value=""></textarea><br>
	<b><spring:message code="source" text="#source#"/></b><br>
	<input type="text" id="source" readonly/><br>
	<button type="submit" class="btn btn-success" value="submit" id="saveButton">Save</button>
	<button type="submit" class="btn btn-success" value="submit" id="pushButton">Enable Plan</button>
 </form:form> 

 <script type="text/javascript"> 
 $(document).ready(function () {
	 var planIdVar = $("#planId").data('planId');
	 $.ajax({
         url: '/admin/rechargeplans/moderation/get/'+planIdVar,
         type: 'GET',
         dataType: 'JSON'
     }).done(function(response) {
    	 $(response.result).each(function(index, element) {
    		 $('#name').val(element.name),
    		 $('#amount').val(element.amount),
    		 $('#talkTime').val(element.talktime),
    		 $('#circleMasterId').val(element.circleMasterId),
    		 $('#operatorMasterId').val(element.operatorMasterId),
    		 $('#description').val(element.description),
    		 $('#validity').val(element.validity),
    		 $('#source').val(element.source);
    	 });
     });
      $('#saveButton').click(function(event){
    	  event.preventDefault();
    	   var dataObject = { 
    			  rechargePlanId : $('#planId').val(),
    				name:  $('#name').val(),
    				amount: $('#amount').val(),
    				talktime:$('#talkTime').val(),
    				circleMasterId:$('#circleMasterId').val(),
    				operatorMasterId: $('#operatorMasterId').val(),
    				validity: $('#validity').val(),
    				description: $('#description').val(),
    				csrfRequestIdentifier: $('#saveFormId').data('csrf')    				
    	  };
    	   $.ajax({
                     url: '/admin/rechargeplans/moderation/save',
                     type: 'POST',
                     data: dataObject,
                     dataType: 'JSON'
                }).done(function(response) {
                	$('#statusMessage').show();
                	$('#statusMessage').html(response.STATUS+' '+'Successfully');
                });
    		 });
         $('#pushButton').click(function(event){
        	 event.preventDefault();
        	 var rechargePlanId = $('#planId').val();
             var dataObject = { 
                    rechargePlanId : $('#planId').val(),
                      name:  $('#name').val(),
                      amount: $('#amount').val(),
                      talktime:$('#talkTime').val(),
                      circleMasterId:$('#circleMasterId').val(),
                      operatorMasterId: $('#operatorMasterId').val(),
                      description: $('#description').val(),
                      csrfRequestIdentifier: $('#saveFormId').data('csrf')
            };
             $.ajax({
                     url: '/admin/rechargeplans/moderation/push',
                     type: 'POST',
                     data: dataObject,
                     dataType: 'JSON'
              }).done(function(response) {
            	  if(response.STATUS == 'Fail') {
            		  $('#errorMessage').html(response.STATUS+': '+' Occured. '+response.ERROR_MESSAGE).show();
            		  $.ajax({
                          url: '/admin/rechargeplans/moderation/delete/'+rechargePlanId,
                          type : 'GET',
                          dataType:'JSON'
                        }).done(function(response){
                            alert("Failed to Push. " + response.ERROR_MESSAGE + ". Deleted from moderation.");
                            location.href="/admin/rechargeplans/moderation.htm";
                        });
            } else {
              $.ajax({
                  url: '/admin/rechargeplans/moderation/duplicate/'+dataObject.amount+'/'+dataObject.name+'/'+dataObject.circleMasterId+'/'+dataObject.operatorMasterId,
                  type : 'GET',
                  dataType:'JSON'
               }).done(function(response){
                   $(response.result).each(function(index, element) {
                       $.ajax({
                         url: '/admin/rechargeplans/moderation/delete/'+element,
                         type : 'GET',
                         dataType:'JSON',
                       }).done(function(response){
                           if(response.status != 'Success') {
                        	   $(that).parent().parent().hide().remove();
                               $('#errorMessage').html(response.status+': '+' Occured. '+response.result).show();
                               alert(response.status+': '+' Occured. '+response.result);
                               return;
                           }
                       });
                 });
                   /* $('#statusMessage').html('Recharge Plan Pushed Successfully. Also Deleted similar plans from Moderation').show(); */
                   alert('Recharge Plan Pushed Successfully. Also Deleted similar plans from Moderation');
                   location.href="/admin/rechargeplans/moderation.htm";
              });
           }
    	 });
         });
 });
 </script>