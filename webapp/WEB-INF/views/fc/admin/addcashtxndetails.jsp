<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<div class="row">
	<div class="col-md-3 col-sm-3">
		<jsp:include page="customerTrailSearch.jsp" />
	</div>
    <div class="col-md-9 col-sm-9">
        <h3> Add Cash Transaction Ref: <c:out value="${txnRef}"/></h3>
        <h4> Add Cash Transaction ID: <c:out value="${txnId}"/></h4>
        <c:choose>
            <c:when test="${not empty txnDetailsList}">
                <h5><b>Transaction details :</b></h5>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Created Time</th>
                            <th>User ID</th>
                            <th>Amount</th>
                            <th>Context</th>
                            <th>Status</th>
                            <th>Txn Reversal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="txnDetail" items="${txnDetailsList}">
                            <tr>
                                <td>${txnDetail.createTime}</td>
                                <td>${userId}</td>
                                <td>${amount}</td>
                                <td>${txnDetail.context}</td>
                                <td>${txnDetail.status}</td>
                                <c:choose>
							  		<c:when test="${isAuthorize eq 'yes'}">
										<td>
											<!-- Reverse txn button -->
												<button class="btn btn-mini btn-primary reverseTxnButton" data-toggle="modal" style="float:left;margin-right:12px" data-txnId="${txnId}">Reverse Txn</button>
											<!-- End of reverse txn -->
										</td>
									</c:when>
								</c:choose>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:when>
        </c:choose>
        <iframe name="hiddenFrame" width="inherit" height="50" frameborder="0"></iframe>
    </div>
</div>
<div class="modal fade" id="reverseTxnModal" tabindex="-1" role="dialog"
	   aria-labelledby="myModalLabel"  style="display: none; height: 20%">
	 <div class="modal-header">
	    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
	      	<h4 id="myModalLabel">Reverse Wallet Transaction</h4>
	 </div>
	 <div class="modal-body" >
	 		<form name="reverseTxnForm" method="POST" action="/admin/customertrail/onecheck/OC/reversetxn" target="hiddenFrame">
	 			<div><input type="text" id="txnId" name="txnIdField"></div>
	 			<div><button type="submit" class="btn btn-small btn-primary">Reverse</button></div>
	 		</form>
	 </div>
</div>

<script>
$(".reverseTxnButton").click(function(){ // Click to only happen on announce links
             $("#txnId").val('${txnId}');
             $('#reverseTxnModal').modal('show');
 		});
</script>