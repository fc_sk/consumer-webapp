<!doctype html>

<%@page
	import="com.freecharge.customercare.controller.CustomerTrailController"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html lang="en">
<head>
<meta charset="utf-8">
<title>jQuery UI Autocomplete - Test</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-2.2.3.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">

<script>
  var list = $.parseJSON('${jsonmerchantlist}');
  $(function() {
    $( "#tags" ).autocomplete({
      source: list
    });
  });
  </script>
</head>
<body>

	<div class="row">
		<div class="col-md-3 col-sm-3">
			<jsp:include page="customerTrailSearch.jsp" />
		</div>
		<div class="col-md-9 col-sm-9">
			<div class="customer-trail">

				<div class="customer-trail-input">
					<div class="clearfix" id="id1">
						<c:if test="${not empty emailid}">
							<div>
								<form:form method="GET"
									action="/admin/customertrail/batcave/getusermerchanthistory/view.htm"
									modelAttribute="merchant">
									<label for="emailid"><b>User-Merchant History by
											User Email and Merchant Id</b></label>
									<div class="ui-widget">
										<label for="emailid"><b>Email Id </b></label> <input
											type="text" name="emailid" value="${emailid}"> <br></br>
										<label for="merchant"><b>Select Merchant</b></label>
										<!-- <input type="text" name="merchant" id="tags">  -->
										<form:input path="merchantId" name="merchant" id="tags" />
										<button class="btn trim-input" type="submit">GO</button>
									</div>
								</form:form>
							</div>
						</c:if>

						<c:if test="${not empty orderid}">
							<div>
								<form:form method="GET"
									action="/admin/customertrail/batcave/getdetailsbymercahntorderid/view.htm"
									modelAttribute="merchant">
									<label for="orderid"><b>Order Details by Merchant
											Order Id</b></label>
									<div class="ui-widget">
										<label for="orderid"><b>Order Id </b></label> <input
											type="text" name="orderid" value="${orderid}"> <br></br>
										<label for="merchant1"><b>Select Merchant</b></label>
										<!-- <input type="text" name="merchant1" id="tags1"> -->
										<form:input path="merchantId" name="merchant1" id="tags" />
										<button class="btn trim-input" type="submit">GO</button>
									</div>
								</form:form>
							</div>
						</c:if>
					</div>
				</div>

				<script type="text/javascript">
		        $(document).ready(function(){
		
		            var customerTrailRows = $('.customer-trail-rows').find('tr');
		            customerTrailRows.eq(0).show();   
		        }
	        </script>
			</div>
		</div>
	</div>

</body>
</html>