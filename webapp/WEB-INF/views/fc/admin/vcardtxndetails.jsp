<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
	<div class="col-sm-3 col-md-3">
		<jsp:include page="/WEB-INF/views/fc/admin/customerTrailSearch.jsp"/>
	</div>
    <div class="col-sm-9 col-md-9">
        <c:url value="/admin/customertrail/onecheck/searchbyemail/result.htm" var="emailurl">
          <c:param name="emailid" value="${emailid}" />
        </c:url>
        <h3>Virtual Card Txns for Email: <a href="${emailurl}"><c:out value="${emailid}" /></a></h3>
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th>Txn Reference Number</th>
                    <th>Card ID</th>
                    <th>Transaction Type</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Wallet Txn ID</th>
                    <th>Created On</th>
                    <th>Last Updated</th>
                    <th>Merchant Name</th>
                    <th>Merchant ID</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${vcardtxnlist}" var="vcardTxn">
                <tr>
                    <td><c:out value="${vcardTxn.agTxnRefNum}" /></td>
                    <td><c:out value="${vcardTxn.agCardId}" /></td>
                    <td><c:out value="${vcardTxn.txnType}" /></td>
                    <td><c:out value="${vcardTxn.amount}" /></td>
                    <td><c:out value="${vcardTxn.status}" /></td>
                    <td><c:out value="${vcardTxn.walletTxnId}" /></td>
                    <td><c:out value="${vcardTxn.createdOn}" /></td>
                    <td><c:out value="${vcardTxn.lastUpdated}" /></td>
                    <td><c:out value="${vcardTxn.agMerchantName}" /></td>
                    <td><c:out value="${vcardTxn.agMerchantId}" /></td>
                </tr>
            </c:forEach>
           </tbody>
        </table>
        <c:choose>
            <c:when test="${isAuthorize eq 'yes'}">
                <c:choose>
                    <c:when test="${allowReversal}">
                        <button class="btn btn-mini btn-primary manualRefundButton" data-toggle="modal" data-target="#vcRefundModal" style="float:left;margin-right:12px">Manual Refund</button>
                    </c:when>
                </c:choose>
            </c:when>
        </c:choose>
        <br><br>
        <c:if test="${not empty refundStatus}">
            <div class="alert alert-error">
              <p><c:out value="${refundStatus}"/> : <c:out value="${refundMessage}"/> </p>
            </div>
        </c:if>

        <%-- ############################### VC Transaction Details Started ############################### --%> 
         
        <h3>VCRecon Details</h3>
        
        <c:choose>
		<c:when test="${VCTransactons.status eq 'SUCCESS'}">
		
		        <%--  Wallet Transactions --%>  
		        <h4>VCRecon Wallet Txns</h4>
		        
		        <c:choose>
		        <c:when test="${not empty VCTransactons.walletTxns}" >
		     
				<table class="table table-hover table-bordered" style="font-size:11px" >
					<thead>
						<tr>
							<th>Wallet Txn Id</th>
							<th>Card ID</th>
							<th>Transaction Type</th>
							<th>Amount</th>
							<th>Unsettled Balance</th>
							<th>
								<table class="table" style ="margin:0" >
									<tr><td style="font-size:13px; border:0; border-bottom:1px solid #ddd;" >Auth Recon</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Status</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception Summary</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0;" >Updated On</td></tr>
								</table>				
							</th>
							<th>
								<table class="table" style ="margin:0" >
									<tr><td style="font-size:13px; border:0; border-bottom:1px solid #ddd;"  >Settlement Recon</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Status</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception Summary</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0;" >Updated On</td></tr>
								</table>
							</th>				
							<th>Transaction Initiated On</th>
							<th>Created On</th>
							<th>Last Updated On</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${VCTransactons.walletTxns}" var="vcWalletTxns">
							<tr>
								<td>${vcWalletTxns.walletTxnId}</td>
								<td>${vcWalletTxns.cardHashId}</td>
								<td>${vcWalletTxns.txnType}</td>
								<td>${vcWalletTxns.amount}</td>
								<td>
									<c:choose>
										<c:when test="${vcWalletTxns.txnType eq 'CREDIT'}" >${vcWalletTxns.unsettledBalance}</c:when>
										<c:otherwise>NA</c:otherwise>
									</c:choose>
								<td>
									<table class="table" style ="margin:0" >
										<tr>
											<td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcWalletTxns.callbackReconStatusStr}</td></tr>
										<tr>
											<td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcWalletTxns.callbackExceptionStr}<br></td></tr>
										<tr>
											<td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcWalletTxns.callbackExceptionSummary}<br></td></tr>
										<tr><td style="margin: 0px; padding: 2px; border:none;"  ><c:if test="${not empty vcWalletTxns.callbackReconStatusStr}" >${vcWalletTxns.callbackReconTs}</c:if></td></tr>
									</table>	
								</td>
								<td>
									<table class="table" style ="margin:0" >
										<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcWalletTxns.settlementReconStatusStr}</td></tr>
										<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcWalletTxns.settlementExceptionStr}<br></td></tr>
										<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcWalletTxns.settlementExceptionSummary}<br></td></tr>
										<tr>
											<td style="margin: 0px; padding: 2px; border:none;" ><c:if test="${not empty vcWalletTxns.settlementReconStatusStr}" >${vcWalletTxns.settlementReconTs}</c:if></td></tr>
									</table>	
								</td>
								<td>${vcWalletTxns.runningTs}</td>
								<td>${vcWalletTxns.createdTs}</td>
								<td>${vcWalletTxns.updatedTs}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				
				</c:when>
				
				<c:otherwise>
					<p style="font-size:14px;" >No Data Found</p>
				</c:otherwise>
				</c:choose>
				<br> <br> 
				
				<%--  Auth Transactions --%>   
				<h4>VCRecon Auth Txns</h4>
		        <c:choose>
		        <c:when test="${not empty VCTransactons.auth}" >
		        <c:set var="auth" value="${VCTransactons.auth}" />
				<table class="table table-hover table-bordered"  style="font-size:11px" >
					<thead>
						<tr>
							<th>Trace Number</th>
							<th>Card Id</th>
							<th>Amount</th>
							<th>merchantName</th>
							<th>merchantId</th>			
							<th>terminalId</th>
							<th>merchantCategoryCode</th>
							<th>responseCode</th>
							<th>responseDescription</th>
							<th>responseSettlementDate</th>
							<th>reversalCode</th>
							<th>reversalDescription</th>
							<th>
								<table class="table" style ="margin:0" >
									<tr><td style="font-size:13px; border:0; border-bottom:1px solid #ddd;" >Auth Recon</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Status</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception Summary</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0;" >Updated On</td></tr>
								</table>				
							</th>
							<th>Transaction Initiated On</th>
							<th>CreatedTs</th>
							<th>updatedTs</th>
						</tr>
					</thead>
					<tbody>			
						<tr>
							<td>${auth.traceNumber}</td>
							<td>${auth.cardHashId}</td>
							<td>${auth.cardAmount}</td>
							<td>${auth.merchantName}</td>
							<td>${auth.merchantId}</td>
							<td>${auth.terminalId}</td>
							<td>${auth.merchantCategoryCode}</td>
							<td>${auth.responseCode}</td>
							<td>${auth.responseDescription}</td>
							<td>${auth.responseSettlementDate}</td>
							<td>${auth.reversalCode}</td>
							<td>${auth.reversalDescription}</td>
							<td>
								<table class="table" style ="margin:0" >
									<tr>
										<td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${auth.callbackReconStatusStr}</td></tr>
									<tr>
										<td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${auth.callbackExceptionStr}<br></td></tr>
									<tr>
										<td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${auth.callbackExceptionSummary}<br></td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:none;"  ><c:if test="${not empty auth.callbackReconStatusStr}" >${auth.callbackReconTs}</c:if></td></tr>
								</table>	
							</td>
							<td>${auth.dateAdded}</td>
							<td>${auth.createdTs}</td>
							<td>${auth.updatedTs}</td>
						</tr>
					</tbody>
				</table>
				
				</c:when>
				
				<c:otherwise>
					<p style="font-size:14px;" >No Data Found</p>
				</c:otherwise>
				</c:choose>
				<br> <br> 
				
				<%--  Settlement Transactions --%>  
				<h4>VCRecon Settlement Txns</h4> 
				
				<c:choose>
		        <c:when test="${not empty VCTransactons.settlements}" >
		        
				<table class="table table-hover table-bordered" style="font-size:11px" >
					<thead>
						<tr>
							<th>Record Indicator</th>
							<th>Trace Number</th>
							<th>Card Id</th>
							<th>Acquirer Ref No</th>
							<th>Retreival Ref No</th>
							<th>Amount</th>
							<th>Txn Type</th>
							<th>Terminal Id</th>
							<th>Merchant Name</th>
							<th>Merchant Category Code</th>
							<th>Merchant Id</th>
							<th>
								<table class="table" style ="margin:0" >
									<tr><td style="font-size:13px; border:0; border-bottom:1px solid #ddd;"  >Settlement Recon</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Status</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception Summary</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0;" >Updated On</td></tr>
								</table>
							</th>				
							<th>Transaction Initiated On</th>
							<th>Created On</th>
							<th>Last Updated On</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${VCTransactons.settlements}" var="vcSettlements" >
							<tr>
								<td>${vcSettlements.recordIndicator}</td>
								<td>${vcSettlements.traceNumber}</td>
								<td>${vcSettlements.cardHashId}</td>
								<td>${vcSettlements.acquirerRefNo}</td>
								<td>${vcSettlements.retreivalRefNo}</td>
								<td>${vcSettlements.billingAmount}</td>
								<td>${vcSettlements.txnType}</td>
								<td>${vcSettlements.terminalId}</td>
								<td>${vcSettlements.merchantName}</td>
								<td>${vcSettlements.merchantCategoryCode}</td>
								<td>${vcSettlements.merchantId}</td>
								<td>
									<table class="table" style ="margin:0" >
										<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcSettlements.settlementReconStatusStr}</td></tr>
										<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcSettlements.settlementExceptionStr}<br></td></tr>
										<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcSettlements.settlementExceptionSummary}<br></td></tr>
										<tr>
											<td style="margin: 0px; padding: 2px; border:none;" ><c:if test="${not empty vcSettlements.settlementReconStatusStr}" >${vcSettlements.settlementReconTs}</c:if></td></tr>
									</table>	
								</td>
								<td>${vcSettlements.localTransactionDate}</td>
								<td>${vcSettlements.createdTs}</td>
								<td>${vcSettlements.updatedTs}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
		
				</c:when>
				
				<c:otherwise>
					<p style="font-size:14px;" >No Data Found</p>
				</c:otherwise>
				</c:choose>
		
				<br><br>
				
				<%--  FundPost Refunds --%>  
				<h4>VCRecon FundPost Refunds </h4> 
				
				<c:choose>
		        <c:when test="${not empty VCTransactons.fundPostRefunds}" >
		        
				<table class="table table-hover table-bordered" style="font-size:11px" >
					<thead>
						<tr>
							<th>Card Id</th>
							<th>Acquirer Ref No</th>
							<th>Retrieval Ref No</th>
							<th>Version</th>
							<th>Debit / Credit Flag</th>
							<th>Amount</th>
							<th>Settlement Status</th>
							<th>Settlement Ts</th>
							<th>Exception</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${VCTransactons.fundPostRefunds}" var="vcFundPostRefunds" >
							<tr>
								<td>${vcFundPostRefunds.cardHashId}</td>
								<td>${vcFundPostRefunds.acquirerReferenceNumber}</td>
								<td>${vcFundPostRefunds.retrievalReferenceNumber}</td>
								<td>${vcFundPostRefunds.version}</td>
								<td>
									<c:choose>
										<c:when test="${vcFundPostRefunds.debitCreditFlag eq 0}">DEBIT</c:when>
										<c:otherwise>CREDIT</c:otherwise>
									</c:choose>				
								</td>
								<td>${vcFundPostRefunds.adjustmentBillingAmount}</td>
								<td>
									<c:choose>
										<c:when test="${vcFundPostRefunds.settlementStatus eq 0}">FAILED</c:when>
										<c:otherwise>SUCCESS</c:otherwise>
									</c:choose>				
								</td>
								<td>${vcFundPostRefunds.settlementTs}</td>
								<td>${vcFundPostRefunds.exception}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
		
				</c:when>
				
				<c:otherwise>
					<p style="font-size:14px;" >No Data Found</p>
				</c:otherwise>
				</c:choose>
				
				<br><br>
				
				<%--  Settlement CreditVouchers --%>  
				<h4>VCRecon Settlement CreditVouchers [ All Potential Matches ]</h4> 
				
				<c:choose>
		        <c:when test="${not empty VCTransactons.settlementsCreditVouchers}" >
		        
				<table class="table table-hover table-bordered" style="font-size:11px" >
					<thead>
						<tr>
							<th>Record Indicator</th>
							<th>Trace Number</th>
							<th>Card Id</th>
							<th>Acquirer Ref No</th>
							<th>Retreival Ref No</th>
							<th>Amount</th>
							<th>Txn Type</th>
							<th>Terminal Id</th>
							<th>Merchant Name</th>
							<th>Merchant Category Code</th>
							<th>Merchant Id</th>
							<th>
								<table class="table" style ="margin:0" >
									<tr><td style="font-size:13px; border:0; border-bottom:1px solid #ddd;"  >Settlement Recon</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Status</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception Summary</td></tr>
									<tr><td style="margin: 0px; padding: 2px; border:0;" >Updated On</td></tr>
								</table>
							</th>				
							<th>Transaction Initiated On</th>
							<th>Created On</th>
							<th>Last Updated On</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${VCTransactons.settlementsCreditVouchers}" var="vcSettlements" >
							<tr>
								<td>${vcSettlements.recordIndicator}</td>
								<td>${vcSettlements.traceNumber}</td>
								<td>${vcSettlements.cardHashId}</td>
								<td><a href="/admin/customertrail/onecheck/vcardrefundarnstatus.htm?vcardRefundARN=${vcSettlements.acquirerRefNo}" target="_blank" >${vcSettlements.acquirerRefNo}</a></td>
								<td>${vcSettlements.retreivalRefNo}</td>
								<td>${vcSettlements.billingAmount}</td>
								<td>${vcSettlements.txnType}</td>
								<td>${vcSettlements.terminalId}</td>
								<td>${vcSettlements.merchantName}</td>
								<td>${vcSettlements.merchantCategoryCode}</td>
								<td>${vcSettlements.merchantId}</td>
								<td>
									<table class="table" style ="margin:0" >
										<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcSettlements.settlementReconStatusStr}</td></tr>
										<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcSettlements.settlementExceptionStr}<br></td></tr>
										<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcSettlements.settlementExceptionSummary}<br></td></tr>
										<tr>
											<td style="margin: 0px; padding: 2px; border:none;" ><c:if test="${not empty vcSettlements.settlementReconStatusStr}" >${vcSettlements.settlementReconTs}</c:if></td></tr>
									</table>	
								</td>
								<td>${vcSettlements.localTransactionDate}</td>
								<td>${vcSettlements.createdTs}</td>
								<td>${vcSettlements.updatedTs}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
		
				</c:when>
				
				<c:otherwise>
					<p style="font-size:14px;" >No Data Found</p>
				</c:otherwise>
				</c:choose>
				
				<br><br>
				
				<%-- Auth Releases  --%>  
				<h4>VCRecon Auth Release </h4> 
				
				<c:choose>
        <c:when test="${not empty VCTransactons.authRelease}" >
        
		<table class="table table-hover table-bordered" style="font-size:11px" >
			<thead>
				<tr>
					<th>Trace Number</th>
					<th>Card Id</th>
					<th>Amount</th>
					<th>
						<table class="table" style ="margin:0" >
							<tr><td style="font-size:13px; border:0; border-bottom:1px solid #ddd;"  >Recon</td></tr>
							<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Status</td></tr>
							<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception</td></tr>
							<tr><td style="margin: 0px; padding: 2px; border:0; border-bottom:1px solid #ddd;" >Exception Summary</td></tr>
							<tr><td style="margin: 0px; padding: 2px; border:0;" >Updated On</td></tr>
						</table>
					</th>				
					<th>Transaction Requested On</th>
					<th>Created On</th>
					<th>Last Updated On</th>
				</tr>
			</thead>
			<tbody>
				 <c:set var="vcAuthRelease" value="${VCTransactons.authRelease}" />
				<tr>
					<td>${vcAuthRelease.traceNumber}</td>
					<td>${vcAuthRelease.cardHashId}</td>
					<td>${vcAuthRelease.billAmount}</td>
					<td>
						<table class="table" style ="margin:0" >
							<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcAuthRelease.reconStatusStr}</td></tr>
							<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcAuthRelease.reconExceptionStr}<br></td></tr>
							<tr><td style="margin: 0px; padding: 2px; border:none; border-bottom:1px solid #ddd;" >${vcAuthRelease.reconExceptionSummary}<br></td></tr>
							<tr>
								<td style="margin: 0px; padding: 2px; border:none;" ><c:if test="${not empty vcAuthRelease.reconStatusStr}" >${vcAuthRelease.reconTs}</c:if></td></tr>
						</table>	
					</td>
					<td>${vcAuthRelease.transactionRequestDate}</td>
					<td>${vcAuthRelease.createdTs}</td>
					<td>${vcAuthRelease.updatedTs}</td>
				</tr>
			</tbody>
		</table>

		</c:when>
		
		<c:otherwise>
			<p style="font-size:14px;" >No Data Found</p>
		</c:otherwise>
		</c:choose>
		
		</c:when>
		
		<c:otherwise>
			<p style="font-size:14px;" >ErrorCode : ${VCTransactons.errorCode}</p>
			<p style="font-size:14px;" >ErrorMessage : ${VCTransactons.exception}</p>
		</c:otherwise>			
		</c:choose>
		
		<%-- ############################### VC Transaction Details Finished ############################### --%> 
		
		<%-- Modal--%> 
          <div class="modal fade" id="vcRefundModal" tabindex="-1" role="dialog"
               aria-labelledby="myModalLabel"  style="display: none; height: 20%">
               <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h4 id="myModalLabel">VC Transaction Refund</h4>
               </div>
               <div class="modal-body" >
                  <form name="manualRefundForm" method="POST" action="/admin/customertrail/onecheck/vcard/txn/refund.htm">
                    <div>Transaction Ref. Id: <input type="text" id="vcardtxnId" value="${txnrefid}" name="vcardtxnId"></div>
                    <div><button type="submit" class="btn btn-small btn-primary">Do refund</button></div>
                  </form>
               </div>
          </div>
    </div>
</div>