<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"		 uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" 	 uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tiles"  uri="http://tiles.apache.org/tags-tiles" %>

<fmt:message key="${kry}" />
<script type="text/javascript">
function submitForgotPasswordForm(formName)
{
	document.forms[formName].submit();
}
</script>
<div style="margin:5px">
    Result :  ${testWebResult.one}
    Result :  ${testWebResult.dbName}
        <div >
            <div >
                <div ><p>Integration Testing</p></div><br/><br/><br/><br/>
			<c:url value="/app/testingDelegateInt.htm" var="intTestUrl"/>
		    <form:form modelAttribute="testWebDO" action="${intTestUrl}" method="get" name="testWebDO" >
				<table cellpadding="5" cellspacing="5" width="100%">
					<tr>
						<td width="10%">&nbsp;</td>
					</tr>
					<tr>
						<td width="20%">
							<p >Field One</p>
						</td>
						<td align="left" width="20%">
			                <form:input path="one" maxlength="50" />
						</td>
					</tr>
				</table>
			</form:form>
		</div>
            <input type="submit" value="submit" onclick="submitForgotPasswordForm('testWebDO');"/>
	</div>
    <br/><br/><br/><br/>
</div>
