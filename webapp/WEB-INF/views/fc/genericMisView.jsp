<%@ include file="/WEB-INF/includes/taglibs.jsp"%>
<%@page import="com.freecharge.recharge.businessdao.GenericMisData" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MIS Service</title>
<style>

.error {
color: #ff0000;
font-style: italic;
}

.txt1 {
	   border-color: #AABBFF;
	   border: 1px solid; 
	   font: bold 84% 'trebuchet ms',helvetica,sans-serif; 
}

input.btn { 
    color: #0000FF;
    font: 95% ;
    padding: 3px 20px;
} 	

.theader{
	bgcolor="FFCCFF";
}
li.leftNavLink{
	list-style-type: none;
	padding:2px;
}
ul.leftNavPanel{
	margin:0px;
	paddding-left:20px;
}
th{
	background-color: gray;
	color: white;
	font-weight: bold;
	padding: 5px;
	text-align: left;
}
td{
	padding-left:6px;
	padding-right:10px;
	padding-top:3px;
	padding-bottom:3px;
	
}
td.entityID{
	width:30px;
	text-align: right;
}
tr.color0{
	background-color: #F0F0F0;
}
tr.color1{
	background-color: #D0D0D0;
}
body {
	font-family: verdana;
	font-size: 10px;
	color: #444444;
	
}
div.leftMenu{
	padding-right:60px;
	float:left;
}
div.displayList{
	float:left;
	width:60%;
}
p.addLink{
	padding-left:10px;
	float:left;
}
input.txt, select.txt{
padding:0px;
font-size: 10px;
width:100px;
}

p.addHeader{
	font-size:15px;
	 
}
div.header{
       background-color: #AAAAFF;
    color: #5555FF;
    font-family: Trebuchet MS;
    font-size: 35px;
    font-weight: bold;
    padding: 20px;
    text-align: center;

 
}
.floatRight{
	float:right;
}
div.searchCriteria{
    border: thin solid #AAAAFF;
    left: 5%;
    margin: 5px;
    position: relative;
    width: 90%;
	
}
</style>
			<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css?v=${mt:keyValue("version.no")}" type="text/css" media="all" />
			<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css?v=${mt:keyValue("version.no")}" type="text/css" media="all" />
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>
			<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>
			<script src="http://jquery-ui.googlecode.com/svn/tags/latest/external/jquery.bgiframe-2.1.2.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>
<script>
	$(function() {
		$( "#startDate" ).datepicker({dateFormat: 'yy/mm/dd'});
	});
	$(function() {
		$( "#endDate" ).datepicker({dateFormat: 'yy/mm/dd'});
	});
	function validateForm()
	{
	var x=document.forms["myForm"]["txnid"].value;
	if (isNaN(x))
	  {
		document.getElementById("msg").innerHTML='<font color="red">Please enter TXN ID as number</font>';
		return false;
	  }
	};
	
</script>

<script>
   document.getElementById('startDate').value = (new java.util.Date()).format("m/dd/yy");
</script>

</head>

<body>
<div class="header">GENERIC MIS VIEW</div>
 <%
 	 int pageCount=0; 
     if(session.getAttribute("misRequest") != null){
    	 GenericMisData misRequest = (GenericMisData)session.getAttribute("misRequest");
    	 pageCount = misRequest.getPageNo();
    	 /* pageCount=pageCount+1; */
    	 misRequest.setPageNo(pageCount);
    	 session.setAttribute("misRequest", misRequest);
     }
 %>
 <div id="msg"></div>
<div>
<c:url var="addUserUrl" value="/adminPaymentRecharge/searchRecords.htm" />
<form:form name="myForm" modelAttribute="misRequest" method="POST" action="${addUserUrl}"  onsubmit="return validateForm()" >
	<div class="searchCriteria">
	<table align="center" width="90%" border="0">
	<tr>
	<td align="right" width="7%">Start Date</td>
	<td align="left"><form:input id="startDate" path="startDate" class="txt"/></td>
	<td align="right">Mobile No</td>
	<td align="left"><form:input path="mobileNo" cssClass="txt"  /></td>
	<td align="right">InRequestID</td>
	<td align="left"><form:input path="inRequestID" id="inRequestID" cssClass="txt"  /></td>
	<td align="right">PgRequestID</td>
	<td align="left"><form:input path="pgRequestID" id="pgRequestID" cssClass="txt"  /></td>
	<td align="right">IN Aggregator</td>
	 <td align="left"><form:select path="inAggregator" cssClass="txt"  >
						<form:option value="" label="ALL" selected="true"/>
						<c:forEach items="${inAggrOprCircleMaps}"  var="al">
							<form:option value="${al}" label="${al}"/>
						</c:forEach>
					</form:select></td> 
	</tr> 	
	<tr>
	<td align="right">End Date</td>
	<td align="left"><form:input id="endDate" path="endDate" class="txt"/></td>
	<td align="right">Payment Gateway</td>
 	<td align="left"><form:select path="paymentGateway" cssClass="txt"  >
						<form:option value="" label="ALL" selected="true"/>
						<c:forEach items="${paymentgateway}"  var="al">
							<form:option value="${al}" label="${al}"/>
						</c:forEach>
					</form:select></td>   
					
	
	<td align="right">Order Id</td>
	<td align="left"><form:input path="appTxnID" cssClass="txt"  /></td>
	<td align="right">User Fulfilment Status</td>
	<td align="left">
		<form:select path="userTxnStatus" cssClass="txt"  >
			<form:option selected="true" value="" label="ALL" />
			<form:option value="1" label="Success" />
			<form:option value="0" label="Failure" />
		</form:select>
	<td colspan ="4"></td>
	</tr>
	<br/>
	<tr>
	<td colspan="12" align="center"><input type="submit" value="Run Report" class="btn"/></td>
	</tr>
	</table>
	</div>
</form:form>
</div>
<div>
<table align="center" width="100%" border="0">			
			<tr>
				<th>Order ID</th>
				<th>IN_RequestID</th>
				
				<th>Mobile NO</th>
				<th>Operator_Circle</th>
				<th>Amount</th>
				<th>IN_Aggregator</th>
				<th>IN Resp Code</th>
				<th>IN Resp message</th>
				<th>PG_RequestID</th>
				<th>PG_Gateway</th>
				<th>PG_Type</th>
				<th>PG_Option</th>
				<th>PG_Status</th>
				<th>PG_Message</th>
				<th>User_Email</th>
				<th>Txn Fulfilment Status</th>
				<th>Fulfilment updated Time</th>
			</tr>
			
			<c:forEach items="${misData}"  var="misDefault" varStatus="loopStatus">
				<c:set var="colorIndex">${loopStatus.index mod 2}</c:set>
				<tr class="color${colorIndex}">
					<td align="center">${misDefault.orderID }</td>
					<td align="center">${misDefault.inRequestID }</td>
					<td align="center">${misDefault.mobileNumber }</td>
					<td align="center">${misDefault.operator} | ${misDefault.circle}</td>
					<td align="center">${misDefault.amount }</td>
					<td align="center">${misDefault.inAggregator}</td>
					<td align="center">${misDefault.inResponseCodeStatus }</td>
					<td align="center">${misDefault.inResponseMessage }</td>
					<td align="center">${misDefault.pgRequestID }</td>
					<td align="center">${misDefault.paymentGateway }</td>
					<td align="center">${misDefault.paymentType }</td>
					<td align="center">${misDefault.paymentOption}</td>
					<td align="center">${misDefault.pgStatusCode }</td>
					<td align="center">${misDefault.pgMessage }</td>
					<td align="center">${misDefault.userEmailD}</td>
					<td align="center">${misDefault.userTxnStatus }</td>
					<td align="center">${misDefault.requestDate}</td>
				</tr>	
			</c:forEach>
<tr>
<td><a href="searchRecords.htm?iPageNo=<%=pageCount-1 %>">Prev</a></td><td><a href="searchRecords.htm?iPageNo=<%=pageCount+1 %>">Next</a></td>

</tr>
<tr>
<td colspan="2"></td>
</tr>
<tr>
<td colspan="2"><a href="/adminPaymentRecharge/excelReport">Export to Excel</a></td>
</tr>
</table>

</div>
</body>
</html>