    <p class="discount-title"><span></span>Check for discount on your <strong>Axis bank Credit/Debit card?</strong></p>
    <div class="discount-content clearfix">
        <div class="discount-details">
            <div>
                <h6>Get <span class="webRupee">Rs.</span>30 off on a minimum recharge of <span class="webRupee">Rs.</span>50 + 2x value coupons</h6>
                <a href="javascript:void(0)" id="discount-terms-link">Terms &amp; conditions</a>
                <div id="discount-terms" style="display: none">
                    <ol>
                        <li>This offer can be redeemed only on your first transaction on www.freecharge.in  by Axis Bank Credit and Debit card holders.</li>
                        <li>The customer gets <span class="webRupee">Rs.</span>30 off on minimum recharge of <span class="webRupee">Rs.</span>50 and above + Get 2 x value coupons during the transaction using Axis Bank Credit and Debit cards.</li>
                        <li>The promotion/offer is not valid in conjunction with any other offer/scheme/promotions.</li>
                        <li>Strict action will be taken against any forge transactions and in case of any dispute, Freecharge reserves the right to decide  on mobile, dth, data card recharge, redemption and coupons.</li>
                        <li>For any query or feedback, please contact us at <a href="mailto:care@freecharge.com" target="_blank">care@freecharge.com</a>.</li>
                    </ol>
                </div>
            </div>
            <img src="${mt:keyValue("imgprefix1")}/images/${opts.imgUrl}/axisbank.png" alt="Axis bank" />
            <input type="hidden" id="ccdbCardNumberOfferType" value="-1" />
            <input id="ccdbCardNumber" name="couponCode" class="discount-input" value="" type="text" placeholder="Axis bank Credit/Debit card number" />
            <button class="buttonOne small-button" id="addBinOffer" onClick="applyBinOffer(this, '#removeBinOffer', '#promoCodePrice', true)" >Apply</button>
            <button class="small-button remove-link" style="display:none" id="removeBinOffer" onClick="applyBinOffer('#addBinOffer', '#removeBinOffer', '#promoCodePrice', false)">Remove</button>
        </div>

        <div id="binofferApplyMsgHolder" class="fc-message"  style="display:none;"  ></div>
    </div>

    <script type="text/javascript">

        function applyBinOffer(obj, removeId, promoCost, flag) {
            var productType = "<%=productType%>";
            var lookupID = "<%=lookupid%>";
            var couponValue = $('#ccdbCardNumber').val();
            var formData = 'couponCode='+couponValue+'&productType='+productType+'&lookupID='+lookupID;
            if (couponValue == '' && flag) {
                $("#binofferApplyMsgHolder").show().html('<p class="failureMsgSmall " >Not so fast! Please enter a valid card number.</p>');
                return false;
            }
            if (flag ) {
                var url='/binoffer/apply.htm';
                $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    dataType: "json",
                    cache: false,
                    timeout:1800000,
                    success: function (responseText) {
                        var freefundCoupon = responseText.freefundCoupon;
                        if (responseText.status == 'success') {

                            $('#ccdbCardNumberOfferType').val(freefundCoupon.freefundClassId); //Now set the offerType of this coupon code (this field can be used to differentiate different card types)
                            var ffCart =  responseText.data;
                           
                            $( removeId ).show();
                            $(obj).hide().next('.submitDisabled').show();
                            $("#promoCodePriceHolder").html(ffCart.freeCouponAmount);
                            $("#ccdbCardNumber").attr("disabled", "disabled");
                            $("#promoEntityStore").val(ffCart.promoEntity);
                            
                            $("#fcBalanceSection").hide();
                            $("#usePartialWalletPaymentCheck").attr('checked', false);
                            
                            $("#allPaymentOpt").slideDown(200);
                            
                            $("#binofferApplyMsgHolder").show().html('<p class="successMsgSmall " >'+responseText.message+'.</p>');
                        } else if (responseText.status == 'failure') {

                            if (responseText.errorCode == 'minRechargeFailure') {
                                $("#binofferApplyMsgHolder").show().html('<p class="failureMsgSmall " > '+responseText.message+' </p>');
                            } else if (responseText.errorCode == 'couponMismatchFailure') {
                                $("#binofferApplyMsgHolder").show().html('<p class="failureMsgSmall " >'+responseText.message+'</p>');
                            }
                        }
                        refreshCart(lookupID);
                   	    refreshAmountToPay(lookupID);
                   	
                    },
                    error:function (xhr,status,error){
                        alert("Error occurred " + error);
                  }
               })
            } else {
                var url='/binoffer/reset.htm';
                $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    dataType: "json",
                    cache: false,
                    timeout:1800000,
                    success: function (responseText) {
                        $('#ccdbCardNumberOfferType').val(-1);
                        if(responseText.status == 'success') {

                            var ffCart =  responseText.data;
                            refreshCart(lookupID);
                       	    
                            $( removeId ).hide();
                            $(obj).show().next('.submitDisabled').hide();
                            $("#promoCodePriceHolder").html('');
                            $("#ccdbCardNumber").removeAttr("disabled");
                            $("#ccdbCardNumber").val('');
                            $("#promoEntityStore").val('');
                            $("#binofferApplyMsgHolder").hide().html('');
                            $("#fcBalanceSection").show();
                       	    refreshAmountToPay(lookupID);
                        } else if (responseText.status == 'failure') {
                            alert("Error occurred; " + responseText.errors);
                        }
                    },
                    error:function (xhr,status,error){
                        alert("Error occurred " + error);
                    }
                })
            }

            return false;
         }
    </script>