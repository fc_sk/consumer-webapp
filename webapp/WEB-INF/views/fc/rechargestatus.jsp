This page will simply list the recharge status
<br/>
<br/>

<div>
Recharge status: <span id="recharge-status-message"></span>
</div>

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery.timer.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">
  function refresh() {
    $.get('${ajaxURL}', function(data) {
      $("#recharge-status-message").html(data["message"] + " - last checked at " + new Date());
    }, "json");
  }
  function timedRefresh() {
    var timer = $.timer(refresh);
    timer.set({ time : 5000, autostart : true });
  }
  $(document).ready(function() {
    refresh();
    timedRefresh();
  });
</script>