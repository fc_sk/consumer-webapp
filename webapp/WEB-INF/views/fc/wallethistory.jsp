<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page import="com.freecharge.common.util.FCConstants"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<c:set var="walletDisplayName"
	value="<%=FCConstants.WALLET_DISPLAY_NAME%>"></c:set>


	
		<div class="header"><spring:message code="customer.fcbalance.history" text="#customer.fcbalance.history#"/></div>
	
							<div class="wallet-account">
								<div class="row clearfix" >
									<div class="fcBalance-generalBlock lf" >
										<h6>
											<spring:message code="available.balance" text="#available.balance#"/>: <b><span class="webRupee"><spring:message
														code="rupee" text="#rupee#" /></span>.${balance}</b>
										</h6>
										<p>
											<strong><spring:message code="last.activity" text="#last.activity#"/>:</strong>
											<c:choose>
												<c:when test="${not empty lastEntry}">
													<c:choose>
														<c:when test="${lastEntry.IS_DEBIT}">
															<spring:message code="payment.using" text="#payment.using#" />
															<span class="webRupee"><spring:message code="rupee"
																	text="#rupee#" /></span>${lastEntry.txn_amount}
		                                                </c:when>
														<c:otherwise>
															<span class="webRupee"><spring:message code="rupee"
																	text="#rupee#" /></span>${lastEntry.txn_amount} <spring:message
																code="deposited.fc" text="#deposited.fc#" />  
		                                                </c:otherwise>
													</c:choose>
												</c:when>
												<c:otherwise>
		                                            <spring:message code="none" text="#none#"/>
		                                        </c:otherwise>
											</c:choose>
										</p>
										<p><b><spring:message code="order.id" text="#order.id#"/>:</b> ${lastEntry.order_id} </p>
								<p><b><spring:message code="date" text="#date#"/>:</b> ${lastEntry.created_ts}</p>
									</div>
								</div>

								<table class="table-generic">
									<thead>
										<tr>
											<th width="11%" ><spring:message code="date" text="#date#"/></th>
											<th width="43%" ><spring:message code="description" text="#description]#"/></th>
											<th width="18%" ><spring:message code="order.id" text="#order.id#"/></th>
											<th width="8%" ><spring:message code="deposit" text="#deposit#"/></th>
											<th width="11%"><spring:message code="withdrawal" text="#withdrawal#"/></th>
											<th width="9%" ><spring:message code="balance" text="#balance#"/></th>
											<th width="12%"><spring:message code="fund.source" text="#fund.source#"/></th>
											<th width="12%"><spring:message code="fund.destination" text="#fund.destination#"/></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${rows}" var="each">
											<tr>
												<td>${each.created_ts}</td>
												
												<c:choose>
												  <c:when test="${each.operator != NULL}">
													<td>
													
													${each.description}
													
													<c:if test="${each.fund_source eq 'REFUND'}">
													<span class="flag-rechargeStatus" ><spring:message code="refund" text="#refund#"/></span>
													</c:if>
													
													<c:if test="${each.fund_destination eq 'RECHARGE' and each.recharge_failed eq true}">
													<span class="flag-rechargeStatus flag-rechargeStatus-failed" ><spring:message code="failed" text="#failed#"/></span>
													</c:if>
													
													<c:choose>
													  <c:when test="${each.fund_source eq 'RECHARGE_FUND'}">
													  </c:when>
													  <c:otherwise>
													    <p class="clearfix" >${each.operator} | ${each.subscriber_number} | ${each.circle} </p>
													  </c:otherwise>
													
													</c:choose>
													</td>
												  </c:when>
												   <c:otherwise>
													<td>
													  ${each.description}
													</td>
												  </c:otherwise>
												</c:choose>
												
												<td>${each.order_id}</td>

												<c:choose>
													<c:when test="${each.IS_DEBIT eq 'true'}">
														<td></td>
														<td><span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span>${each.txn_amount}</td>
													</c:when>
													<c:otherwise>
														<td><span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span>${each.txn_amount}</td>
														<td></td>
													</c:otherwise>
												</c:choose>
												<td><span class="webRupee"><spring:message code="rupee" text="#rupee#" /></span> ${each.NEW_BALANCE}</td>
												<td>${each.fund_source}</td>
												<td>${each.fund_destination }
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>


