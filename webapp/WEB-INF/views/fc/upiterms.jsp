<!DOCTYPE html>
<html>
<head>
	<title>UPI Terms and Conditions</title>
	<meta name = "viewport" content = "width=device-width, initial-scale=1.0">
	<style type="text/css">
	body{
		margin: 0;
		padding: 0;
	}
	.upiTerms{
		color: #a7a7a7;
		font-size: 14px;
		line-height: 1.8;
		padding: 30px 25px;			 
		font-family: 'Source Sans Pro', 'SansSerif', Helvetica;
		text-shadow: 1px 1px 1px rgba(0, 0, 0, .004);
		outline: none;
		text-decoration: none;
		letter-spacing: 1px;
		word-wrap: break-word;
	}
	.upiTerms h2{
		text-decoration: underline;
		font-size: 16px;
		color: #a7a7a7;
	}
	.upiTerms p{
		font-size: 14px;
		margin-bottom: 15px;
		margin-top: 0px;
	}
	.upiTerms > ul{
		padding-left:15px;
		margin-bottom: 15px;
	}

	.upiTerms ul li{
		color: #a4a9ac;
		font-size: 14px;
		line-height: 1.8;
		list-style-type: disc;
		margin-bottom: 10px;
	}
	.upiTerms > ul > li{
		list-style-position: inside;
	}
	.upiTerms > ul >li >ul{
		padding-left: 10px;
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.upiTerms > ul >li >ul > li{
		list-style-type: square;
	}
	.upiTerms > ul >li >ul > li > ul{
		padding-left: 10px;
	}
	.upiTerms > ul >li >ul > li > ul >li{
		list-style-type: circle;
		padding-left: 10px;
	}
	p.head{
		margin: 15px 0px;
		font-size: 15px;
		padding: 0px 5px;
	}
	table.upiTable{
		margin: 20px 0px;
		border-collapse: collapse;
		margin-left: 30px;
	}
	table.upiTable tr td{
		border:1px solid #a7a7a7;
	}
</style>
</head>
<body>
	<div class="upiTerms">
		<h2>Terms of Use of Freecharge UPI</h2>
		<p>
			THIS DOCUMENT IS PUBLISHED IN COMPLIANCE OF AND SHALL BE GOVERNED BY INDIAN LAW, INCLUDING BUT NOT LIMITED TO (I) THE INDIAN CONTRACT ACT, 1872; (II) THE INFORMATION TECHNOLOGY ACT, 2000, THE RULES, REGULATIONS, GUIDELINES AND CLARIFICATIONS FRAMED THEREUNDER INCLUDING THE INFORMATION TECHNOLOGY (REASONABLE SECURITY PRACTICES AND PROCEDURES AND SENSITIVE PERSONAL INFORMATION) RULES, 2011, AND THE PROVISIONS OF RULE 3 (1) OF THE INFORMATION TECHNOLOGY (INTERMEDIARIES GUIDELINES) RULES, 2011; (III) THE PAYMENT AND SETTLEMENT SYSTEMS ACT, 2007 AND APPLICABLE RULES, REGULATIONS AND GUIDELINES MADE THEREUNDER; AND (IV) RESERVE BANK OF INDIA ACT, 1934 AND THE APPLICABLE RULES, REGULATIONS AND GUIDELINES MADE THEREUNDER.
		</p>
		<p>
			THIS DOCUMENT IS AN ELECTRONIC RECORD IN THE FORM OF AN ELECTRONIC CONTRACT FORMED UNDER INFORMATION TECHNOLOGY ACT, 2000 AND RULES MADE THEREUNDER AND THE AMENDED PROVISIONS PERTAINING TO ELECTRONIC DOCUMENTS / RECORDS IN VARIOUS STATUTES AS AMENDED BY THE INFORMATION TECHNOLOGY ACT, 2000. THIS AGREEMENT DOES NOT REQUIRE ANY PHYSICAL, ELECTRONIC OR DIGITAL SIGNATURE. 
		</p>
		<p>
			THIS AGREEMENT IS A LEGALLY BINDING DOCUMENT BETWEEN YOU AND FREECHARGE (BOTH TERMS DEFINED BELOW). THE TERMS OF THIS DOCUMENT WILL BE EFFECTIVE UPON YOUR ACCEPTANCE OF THE SAME (IN ELECTRONIC FORM OR BY MEANS OF AN ELECTRONIC RECORD OR OTHER MEANS) AND WILL GOVERN THE RELATIONSHIP BETWEEN YOU AND FREECHARGE FOR THE USE OF FREECHARGE UPI AND SERVICES (DEFINED BELOW). IF ANY TERMS OF THIS DOCUMENT CONFLICT WITH ANY OTHER DOCUMENT/ELECTRONIC RECORD IN THIS BEHALF, THE TERMS AND CONDITIONS OF THIS AGREEMENT SHALL PREVAIL, UNTIL FURTHER CHANGE / MODIFICATIONS ARE NOTIFIED BY FREECHARGE.
		</p>
		<p>
			These terms and conditions regulate the payments under the Unified Payment Interface ("UPI"), a payment service platform ("Platform") developed by National Payments Corporation of India ("NPCI"), an umbrella organisation incorporated in 2008 and acting as the settlement/clearing house/regulatory agency for UPI services. The application software ("Freecharge App") which connects the Platform is being offered by Freecharge Payment Technologies Private Limited a company incorporated under the Companies Act, 2013 with its registered office at 68, Okhla Industrial Estate, Phase-III, New Delhi – 110020 India (hereinafter referred to as "Freecharge ") in partnership/association with Axis Bank Limited ("Axis Bank"), a bank licensed under the Banking Regulation Act, 1949. 
		</p>
		<p>The UPI services ("Services") are being offered under the brand "Freecharge". </p>
		<p>Your use of the Freecharge UPI and related tools are governed by the following terms and conditions ("Terms of Use") which are incorporated herein by way of reference. </p>
		<p class="head">1. For the purpose of these Terms of Use, wherever the context so requires-</p>
		<ul>
			<li>'You/Your' or "User" shall mean any natural or legal person who has registered for Freecharge Services, linked his/her bank account for UPI payments and who has accepted these Terms of Use.</li>
			<li>"We", "Us", "Our" shall mean Freecharge Payment Technologies Private Limited. </li>
			<li>"Merchant" shall include any establishment and/or entity which  accepts the UPI payments through Freecharge App ("Freecharge UPI") as a payment method for online or offline purchase of goods and/or services. </li>
			<li>"Buyer" shall refer to the person who purchases any of the goods or services provided online or offline by the Merchants. </li>
		</ul>
		<p class="head">2. If You transact using the Freecharge App or any Merchant Website/ Merchant Platform, these conditions will be applicable. By mere use of the Freecharge App, You shall be contracting with Freecharge and these terms and conditions including the policies constitute Your binding obligations, with Freecharge.</p>		
		<p class="head">3. We reserve the right, at Our sole discretion, to change, modify, add or remove portions of these Terms of Use, at any time without any prior written notice to You. It is Your responsibility to review these Terms of Use periodically for updates / changes. Your continued use of Freecharge App following the posting of changes will mean that You accept and agree to the revisions including additional Terms or removal of portions of these Terms, modifications etc.  As long as You comply with these Terms of Use, We grant You a personal, non-exclusive, non-transferable, limited privilege to enter and avail the Services. </p>
		<p class="head">4. USING FREECHARGE APP INDICATES YOUR AGREEMENT TO ALL THE TERMS AND CONDITIONS UNDER THESE TERMS OF USE, SO PLEASE READ THE TERMS OF USE CAREFULLY BEFORE PROCEEDING. By impliedly or expressly accepting these Terms of Use, You also accept and agree to be bound by Freecharge Policies (including but not limited to Privacy Policy available on the Freecharge website www.freecharge.com as amended from time to time.</p>
		<p class="head">5. About UPI</p>
		<ul>
			<li>The Unified Payments Interface (UPI) is a payment platform built by NPCI (National Payments Corporation of India) that allows instant online payments between the bank accounts of any two parties. UPI offers an architecture and a set of standard API specifications to facilitate these online payments. It aims to simplify and provide a single interface across all NPCI systems besides creating interoperability and superior customer experience. </li>
			<li>The Unified Payments Interface (UPI) is a payment platform built by NPCI (National Payments Corporation of India) that allows instant online payments between the bank accounts of any two parties’. UPI offers an architecture and a set of standard API specifications to facilitate these online payments. It aims to simplify and provide a single interface across all NPCI systems besides creating interoperability and superior customer experience. </li>
		</ul>
		<p class="head">6. Your Account and Registration Obligations</p>
		<ul>
			<li>
				To enable UPI payments, the user first needs to have a Freecharge account. User can register and create a Freecharge account by:
				<ul>
					<li>Downloading the Freecharge App and registering himself/herself. Or,</li>
					<li>Registering with Freecharge from a third party partner platform.</li>
				</ul>
			</li>
			<li>User has to necessarily install the Freecharge App on his/her smartphone to be able to register and transact using Freecharge UPI.</li>
			<li>Thereafter, You will be able to register for UPI payments by:
				<ul>
					<li>Selecting your bank:
						<ul>
							<li>You first need to select the bank from amongst the list of UPI-member banks as shown in the App. The list comprises of all the banks connected to the UPI Platform of NPCI.</li>
							<li>If the name of Your bank does not appear in the list, You can still add Your bank by providing the bank account number and the IFSC code. However, You will not be able to make UPI payments from Your bank account. </li>
						</ul>
					</li>
				</ul>
			</li>
			<li>If You have selected a bank from the UPI-member list, Your mobile number will be OTP (One Time Password) verified by the UPI-member bank. Please note that Your mobile number, should be registered with Your bank. If Your mobile number is not registered, then Your OTP verification will fail. You will not be able to avail the UPI Service unless You first register your mobile number with the Bank.</li>
			<li>Once Your mobile number is verified by OTP, Your bank will share the masked bank account numbers against the registered mobile number. You need to select the bank account which You wish to link with Freecharge App. </li>
			<li>The selected bank account can be linked with Freecharge App in the following manner:
				<ul>
					<li>If You already have the 4-6 digit UPI PIN (UPI Personal Identification Number) for Your bank account, then You need to simply select the "Link" option and continue.
						<ul>
							<li>If you do not remember the UPI PIN, you can click on "forgot UPI PIN" and set a new one by providing the Debit card number linked with Your bank account and the Expiry date of the Card. Your mobile number will be OTP verified by Your bank. If the verification is successful, you will be taken to the NPCI page, to set a new UPI PIN. After setting the UPI PIN, you will be taken back to the Freecharge App screen, where you have to click on "Link" and then continue.</li>
							<li>You can Reset your UPI PIN by clicking on "Reset UPI PIN". You can then set a new UPI PIN by providing the old and new UPI PINs.</li>
						</ul>
					</li>
					<li>If You have never set the UPI PIN before, then You will see an option to Set UPI PIN in settings . Please click on the link and set the UPI PIN by providing the Debit card number linked with Your bank account and the Expiry date of the Card. Your mobile number will be OTP verified by Your bank. If the verification is successful, You will be taken to the NPCI page, to set a new UPI PIN. After setting the UPI PIN, you will be taken back to the Freecharge App screen, where you have to click on "Link" and then continue.</li>
					<li>You need to authorize every UPI payment transaction by providing the UPI PIN.</li>
				</ul>
			</li>
			<li>"KYC" stands for Know Your Customer and refers to the various norms, rules, laws and statutes issued by RBI from time to time under which Freecharge is required to procure personal identification details from You before any Services can be delivered. Know Your Customer (KYC) documents may be asked  by the Merchant (on behalf of Freecharge) from You at the time of activation and/ or on a later date, for availing and / or continuation of Services. Freecharge/Merchant shall not be responsible for wrong details being entered by the User.</li>
			<li>In the event the beneficiary/KYC details provided are found to be incorrect/ insufficient, Freecharge retains the right to block Your Freecharge account. Freecharge retains the right to share the details of the transaction undertaken using the Freecharge account and the end beneficiary/ KYC details with RBI, as per statutory guidelines issued from time to time.</li>
			<li>You are solely responsible for linking Your correct bank account.</li>
			<li>Since Your mobile number is treated as the primary identifier, Your mobile number needs to be updated with the bank linked to Freecharge App in case of any changes.</li>
			<li>If You change the mobile number registered with Your Freecharge account, You will have to re-register Your new mobile number with Freecharge. To re-enable the Service, You need to register Your new mobile number with Your bank as well.</li>
			<li>You agree that Your UPI ID will act as Your identity for all payment transactions and all requests to Your Freecharge account will be raised to Your UPI ID unless another UPI ID is explicitly specified.</li>
			<li>You agree that if You provide any information that is untrue, inaccurate, not current or incomplete or We have reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, or not in accordance with the this Terms of Use, We shall have the right to indefinitely suspend or terminate or block access to Your Account.</li>
			<li>We shall ensure that all Your confidential data pertaining to Your money transactions, bank account details, and all other sensitive personal information are protected and kept confidential by employing best available protection standards, which are more fully set out in our Privacy Policy. </li>
			<li>You are solely responsible to keep Your OTP, UPI PIN and bank account related details confidential. Sharing such information with others may lead to unauthorized usage, for which Freecharge shall not be responsible.
			</li>
		</ul>
		<p class="head">7. Transactions through Freecharge UPI</p>	
		<ul>
			<li>Freecharge UPI can be used for Person to Person money transfers or to purchase products and services online or offline on any Merchant platform.</li>
			<li>The UPI PIN needs to be entered on Your mobile to authorize every Freecharge UPI payment. You need to ensure that You are connected to the internet at the time of making any transaction.</li>
			<li>Freecharge UPI is one of the payment options available to the Users on various Merchant platforms and We assume no responsibility for the products or services purchased using Freecharge UPI and any liability thereof is expressly disclaimed.</li>
			<li>Freecharge UPI is one of the payment options available to the Users on various Merchant platforms and We assume no responsibility for the products or services purchased using Freecharge UPI and any liability thereof is expressly disclaimed.</li>
			<li>You shall  only use Freecharge UPI Services to process a Transaction to make payment for legitimate and bona fide purposes including purchase of goods and services. You shall  not use Freecharge UPI Service to process Payment Transactions in connection with the sale or exchange of any illegal goods or services or any other underlying illegal transaction.</li>
			<li>We may establish general practices and limits concerning use of Freecharge UPI. We reserve the right to change, suspend or discontinue any aspect of Freecharge UPI at any time, including hours of operation or availability of Freecharge UPI Services or any Freecharge UPI Service feature, without notice and without liability. We also reserve the right to impose limits on certain features or restrict access to parts or all of the service without notice and without liability. We may decline to process any Payment Transaction without prior notice to Sender or Recipient. We also reserve the right to automatically block any communication received by a User from a non- Freecharge UPI account, including any payment requests, that We deem to be spam or a fraudulent communication.</li>
			<li>We do not warrant that the functions contained in Freecharge UPI will be uninterrupted or error free, and We shall not be responsible for any Service interruptions (including, but not limited to, power outages, system failures or other interruptions that may affect the receipt, processing, acceptance, completion or settlement of payment transactions or any other reasons beyond reasonable control of Freecharge).</li>
			<li>We do not warrant that the functions contained in Freecharge UPI will be uninterrupted or error free, and We shall not be responsible for any Service interruptions (including, but not limited to, power outages, system failures or other interruptions that may affect the receipt, processing, acceptance, completion or settlement of payment transactions or any other reasons beyond reasonable control of Freecharge).</li>
			<li>We do not warrant that the functions contained in Freecharge UPI will be uninterrupted or error free, and We shall not be responsible for any Service interruptions (including, but not limited to, power outages, system failures or other interruptions that may affect the receipt, processing, acceptance, completion or settlement of payment transactions or any other reasons beyond reasonable control of Freecharge).</li>
			<li>We may reject a Transaction and/or settlement of payments  for various reasons, including but not limited to risk management, suspicion of fraudulent, illegal or doubtful Transactions, selling of prohibited items, use of compromised or blacklisted cards or UPI accounts, chargebacks/complaints or for other reasons as prescribed under Applicable Laws or rules of Participating Banks. In the event that a Transaction is rejected or is unable to be completed, We will either transfer the funds back to the Your Funding Account or will handle the funds in accordance with Applicable Laws or Participating Bank Rules.</li>
			<li class="head">UPI limits issued by NPCI</li>
		</ul>
		<p class="head">8. Requirements and Limits for Freecharge UPI</p>
		<table  class="upiTable" border="0" cellpadding="10" cellspacing="0">
			<tr>
				<td>Particular</td>
				<td>Transaction Limit</td>
			</tr>
			<tr>
				<td>Monthly</td>
				<td>None</td>
			</tr>
			<tr>
				<td>Per Transaction</td>
				<td>Rs. 1 lakh</td>
			</tr>
		</table>
		<p class="head">9. Charges</p>
		<ul>
			<li>Membership is free for Users. Freecharge does not charge its Users any fee for creating an Account or use of Services. Freecharge reserves the right to change its fee policy from time to time. In particular, Freecharge may at its sole discretion introduce new services and modify some or all of the existing services offered on the website. In such an event, We reserve the right to introduce fees for the new services offered or amend/introduce fees for existing services, as the case may be. Changes to the fee policy shall be posted on the website/app and such changes shall automatically become effective immediately after they are posted. Unless otherwise stated, all fees shall be quoted in Indian Rupees. </li>
			<li>Your bank may charge You a nominal transaction fee for UPI transfers- please check with Your bank for any such charges.</li>
			<li>The reporting and payment of any applicable taxes arising from the use of Freecharge UPI is Your responsibility. You hereby agree to comply with any and all applicable tax laws in connection with Your use of Freecharge UPI, including without limitation, the reporting and payment of any taxes arising in connection with Payments made through Freecharge UPI, or funds/income received through Freecharge UPI.</li>
		</ul>
		<p class="head">10. You herein agrees and accepts that, in case of any discrepancy in the information provided by You for availing this Service and the onus thereof shall always be upon the You only and thus You agrees to furnish accurate information at all times to the Freecharge .</p>
		<p class="head">11. If You suspects that there is an error in the information supplied by Freecharge, You shall inform Freecharge immediately. Freecharge will endeavor to correct the error promptly wherever possible on a best effort basis. </p>
		<p class="head">12. Freecharge shall not be held liable & responsible for any loss, cost or damage suffered by You due to disclosure of Your Personal or any other information to a third party including Govt./Regulatory authorities by Freecharge, for reasons inclusive but not limited to participation in any telecommunication or electronic clearing network, in compliance with a legal or regulatory directive, for statistical analysis or for credit rating or for any legal or regulatory compliance. </p>
		<p class="head">13. You are solely responsible for protecting Your Mobile phone/device, virtual address and UPI PIN set for the availing of the Services..</p>
		<p class="head">14. You shall be liable for any kind of unauthorized or unlawful use, misuse or leakage  of any of the virtual address or MPIN/Passwords/Passcodes issued by Freecharge in respect of the Services or any fraudulent or erroneous instruction given by You and any financial charges thus incurred shall be payable by You  only. </p>
		<p class="head">15. You shall be liable for all loss, cost or damage, if You have  breached the terms and conditions contained herein or other additional terms & conditions mentioned above or contributed or caused the loss by negligent actions or a failure on Your part to advise Freecharge within a reasonable time about any unauthorized access in Your Freecharge account used for this Services. </p>
		<p class="head">16. We reserve the right, in our sole and absolute discretion, to suspend or terminate Your use of one or more Freecharge UPI Services, without notice and without liability to You or any third party, for any reason, including without limitation inactivity or violation of these Freecharge UPI Terms of Use or other policies We e may establish from time to time.</p>
		<p class="head">17. <strong>Disclaimer:</strong></p>
		<p>
			THE SERVICES, INCLUDING ALL CONTENT, SOFTWARE, FUNCTIONS, MATERIALS, AND INFORMATION MADE AVAILABLE ON, PROVIDED IN CONNECTION WITH OR ACCESSIBLE THROUGH THE SERVICES, ARE PROVIDED "AS IS." TO THE FULLEST EXTENT PERMISSIBLE BY LAW, FREECHARGE, AND THEIR AGENTS, CO-BRANDERS OR OTHER PARTNERS, INCLUDING BUT NOT LIMITED TO, DEVICE MANUFACTURERS (COLLECTIVELY, " FREECHARGE PARTIES"), MAKE NO REPRESENTATION OR WARRANTY OF ANY KIND WHATSOEVER FOR THE SERVICES OR THE CONTENT, MATERIALS, INFORMATION AND FUNCTIONS MADE ACCESSIBLE BY THE SOFTWARE USED ON OR ACCESSED THROUGH THE SERVICES, OR FOR ANY BREACH OF SECURITY ASSOCIATED WITH THE TRANSMISSION OF SENSITIVE INFORMATION THROUGH THE SERVICES. EACH FREECHARGE PARTY DISCLAIMS WITHOUT LIMITATION, ANY WARRANTY OF ANY KIND WITH RESPECT TO THE SERVICES, NONINFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. FREECHARGE DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THE SERVICES WILL BE UNINTERRUPTED OR ERROR FREE. FREECHARGE  SHALL NOT BE RESPONSIBLE FOR ANY SERVICE INTERRUPTIONS, INCLUDING BUT NOT LIMITED TO SYSTEM FAILURES OR OTHER INTERRUPTIONS THAT MAY AFFECT THE RECEIPT, PROCESSING, ACCEPTANCE, COMPLETION OR SETTLEMENT OF PAYMENT TRANSACTIONS, P2P PAYMENTS OR THE SERVICES.
		</p>
		<p>THE FREECHARGE PARTIES ARE NOT RESPONSIBLE FOR THE ACCURACY OF ANY PAYMENT INSTRUMENT, INCLUDING WITHOUT LIMITATION WHETHER SUCH INFORMATION IS CURRENT AND UP-TO-DATE. WITHOUT LIMITING THE GENERALITY OF THE PRECEDING SENTENCE, YOU EXPRESSLY ACKNOWLEDGE AND AGREE THAT SUCH INFORMATION IS REPORTED BY THE BANK AS OF A PARTICULAR TIME ESTABLISHED BY THE BANK  AND MAY NOT ACCURATELY REFLECT YOUR CURRENT TRANSACTIONS, AVAILABLE BALANCE, OR OTHER ACCOUNT OR PROGRAM DETAILS AT THE TIME THEY ARE DISPLAYED TO YOU THROUGH THE SERVICES OR AT THE TIME YOU MAKE A PURCHASE OR REDEMPTION. YOU MAY INCUR FEES, SUCH AS OVERDRAFT FEES OR OTHER CHARGES AS A RESULT OF SUCH TRANSACTIONS, PER YOUR AGREEMENT WITH YOUR BANK , OR YOUR ATTEMPT TO MAKE A PURCHASE OR REDEMPTION MAY NOT BE SUCCESSFUL.
		</p>
		<p class="head"> 18.  You agree to release Freechage and its partner Bank (Axis Bank), agents, contractors, officers and employees, from all claims, demands and damages (actual and consequential) arising out of or in any way connected to any transaction made through Freechage App. You agree that you will not involve Freecharge in any litigation or other dispute arising out of or related to any transaction, agreement, or arrangement with any Recipient, Sender, Merchant, advertiser or other third party in connection with the Freecharge UPI Service. If You attempt to do so, (i) You shall pay all costs and attorneys' fees of Freecharge, and Group Companies and shall provide indemnification as set forth below, and (ii) the jurisdiction for any such litigation or dispute shall be limited as set forth below. </p>
		<p>
			You agree to indemnify, defend and hold harmless Freecharge, its partner Bank (Axis Bank), Group Companies and its Directors, officers, owners, agents or other partners, employees, contractors and other applicable third parties (collectively "Indemnified Parties") from and against any and all claims, demands, causes of action, debt or liability, including reasonable attorneys fees, including without limitation attorneys fees and costs incurred by the Indemnified Parties arising out of, related to, or which may arise from:
		</p>
		<p>(a) Your use of the Services;</p>

		<p>(b) any breach or non-compliance by You of any term of this Freecharge UPI Terms of Use or any related  policies;</p>

		<p>(c) any dispute or litigation caused by Your actions or omissions; or</p>

		<p>(d) Your negligence or violation or alleged violation of any applicable law or rights of a third party.</p>
	</ul>
	<p class="head">19 <strong>Jurisdiction :</strong> You agree that any legal action or proceedings arising out of the aforementioned documents shall  be brought exclusively in the competent courts/tribunals having jurisdiction in New Delhi, India and irrevocably submit themselves to the jurisdiction of such courts/tribunals. </p>
	<p class="head">20. These Freecharge UPI Terms and any rights and licenses granted hereunder, shall  not be transferred or assigned by You.  However, We may assign, in whole or in part, the benefits or obligations of this Agreement. We will provide an intimation of such assignment to You, which will be binding on the parties to these Terms and Conditions.</p>
	<p class="head">21. Unless otherwise expressly stated in this Agreement, the failure to exercise or delay in exercising a right or remedy under Freecharge UPI Terms of Use  will not constitute a waiver of the right or remedy or a waiver of any other rights or remedies, and no single or partial exercise of any right or remedy under the Freecharge UPI Terms of Use will prevent any further exercise of the right or remedy or the exercise of any other right or remedy.</p>
	<p class="head">22. The terms and conditions of the Freecharge UPI Terms of Use, which by their nature and content are intended to survive the performance hereof by any or all parties hereto will so survive the completion and termination of this Contract.</p>
	<p class="head">23. If any provision of the Freecharge UPI Terms of Use is or becomes, in whole or in part, invalid or unenforceable but would be valid or enforceable if some part of that provision was deleted, that provision will apply with such deletions as may be necessary to make it valid. If any court/tribunal of competent jurisdiction holds any of the provisions of the Freecharge UPI Terms of Use unlawful or otherwise ineffective, the remainder of the Freecharge UPI Terms of Use will remain in full force and the unlawful or otherwise ineffective provision will be substituted by a new provision reflecting the intent of the provision so substituted.</p>
</div>
</body>
</html>