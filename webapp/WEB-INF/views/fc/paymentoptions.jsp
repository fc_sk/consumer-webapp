<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>

<div class="mainContainer innerPageGrad">
    <div class="pageWidth clearfix">

        <div class="rechargeProgress">
            <p class="rechargeProgressTitle"><span class="done">Enter Recharge Details</span><span class="done">Select Coupons</span><span
                    class="done">Provide Details</span><span>Pay and Exit</span></p>

            <div class="rechargeProgressBar"><p class="complete lt"><span>&nbsp;</span></p>

                <p class="complete"><span>&nbsp;</span></p><p class="complete"><span>&nbsp;</span></p>

                <p class="rt"><span>&nbsp;</span></p></div>
        </div>
        <!-- /.rechargeProgress -->


        <div class="ltContainer fLeft">
            <div class="paymentModeWrapper clearfix">
                <h2 class="mainHeader ">Make Payment</h2>
                <ul class="vertCustomNav fLeft">
                    <li class="active ">
                        <a href="javascript:void(0)" onclick="sendRequest('NB')">Net Banking</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="sendRequest('DC')">Debit Card</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="sendRequest('CC')">Credit Card</a>
                    </li>
                </ul>
                <!-- /.vertCustomNav  -->
                <div class="paymentModeInner fLeft">
                    <div class="paymentMode" id="paymenttypeDiv">
                        <h4 class="sectionHeader">Enter card details</h4>

                        <div class="formField clearfix">
                            <label class="fLeft"><strong class="redClr">*</strong> Card Number:</label>
                            <input type="text" class="text" value="Card Number" onBlur="clearText(this)"
                                   onFocus="clearText(this)">
                        </div>
                        <div class="formField clearfix">
                            <label class="fLeft"><strong class="redClr">*</strong> Name on Card:</label>
                            <input type="text" class="text" value="Name on Card" onBlur="clearText(this)"
                                   onFocus="clearText(this)">
                        </div>
                        <div class="formField clearfix">
                            <label class="fLeft"><strong class="redClr">*</strong> Name on Card:</label>

                            <div class="formFieldInnerWrap fLeft">
                                <select class="fLeft smlSelect">
                                    <option>Select Month:</option>
                                </select>
                                <select class="fRight smlSelect">
                                    <option>Select Year:</option>
                                </select>
                            </div>
                        </div>
                        <div class="formField clearfix">
                            <label class="fLeft"><strong class="redClr">*</strong> CVV Number:</label>
                            <input type="text" class="text" value="" onBlur="clearText(this)" onFocus="clearText(this)">
                        </div>
                    </div>
                </div>
                <!-- /.paymentModeInner -->
                <span class="clearBoth">&nbsp;</span>
                <input type="button" class="submitButton fRight" value="Pay and Finish &gt;">

            </div>
            <!-- /.paymentModeWrapper -->
        </div>
        <!-- /.ltContainer -->

        <div class="rtSidebar fRight">
            <div class="couponSelectSummary">
                <h1 class="mainHeader">Summary</h1>

                <div class="curveBlock couponSummaryBlock ">

                    <div class="couponSummaryRow rowFirst">
                        <div class="smlPattern clearfix">
                            <h4 class="sectionHeader fLeft"><span class="collapseArrow">&nbsp;</span>Recharge Details
                            </h4>
                            <a href="#" class="fRight editLink">Edit</a>

                            <div class="couponSummaryCont cl">
                                <p class="topTxt">9819292903 | <span class="webRupee" >Rs.</span> 500</p>
                            </div>
                        </div>
                        <!-- /.smlPattern -->
                        <div class="editMode clearfix" style="display:none">

                        </div>
                        <!-- /.editMode -->
                    </div>
                    <!-- /.couponSummaryRow -->

                    <div class="couponSummaryRow ">
                        <div class="smlPattern clearfix">
                            <h4 class="sectionHeader fLeft"><span class="collapseArrow">&nbsp;</span>Coupon Selection
                            </h4>
                            <a href="#" class="fRight editLink">Edit</a>

                            <div class="couponSummaryCont cl">
                                <p class="topTxt">21 coupons | worth <span class="webRupee" >Rs.</span> 500</p>
                            </div>
                        </div>
                        <!-- /.smlPattern -->

                        <div class="editMode clearfix" style="display:none">
                            <h4 class="sectionHeader"><span class="expandArrow">&nbsp;</span>Coupon Selection</h4>

                            <div class="couponSummaryCont cl">
                                <p class="topTxt"><i>Handling charge INR 10</i></p>

                                <div class="couponSelectedAll">
                                    <h6 class="listHeader"><span>Barista (2)</span></h6>

                                    <div class="couponSmall fLeft cl">
                                        <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                     width="68" height="50"></span>

                                        <p><span class="webRupee" >Rs.</span> 50</p>
                                    </div>
                                    <div class="couponSmall fRight">
                                        <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                     width="68" height="50"></span>

                                        <p><span class="webRupee" >Rs.</span> 50</p>
                                    </div>
                                    <h6 class="listHeader"><span>Barista (2)</span></h6>

                                    <div class="couponSmall fLeft cl">
                                        <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                     width="68" height="50"></span>

                                        <p><span class="webRupee" >Rs.</span> 50</p>
                                    </div>
                                    <div class="couponSmall fRight">
                                        <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                     width="68" height="50"></span>

                                        <p><span class="webRupee" >Rs.</span> 50</p>
                                    </div>
                                    <h6 class="listHeader"><span>Barista (2)</span></h6>

                                    <div class="couponSmall fLeft cl">
                                        <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                     width="68" height="50"></span>

                                        <p><span class="webRupee" >Rs.</span> 50</p>
                                    </div>
                                    <div class="couponSmall fRight">
                                        <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                     width="68" height="50"></span>

                                        <p><span class="webRupee" >Rs.</span> 50</p>
                                    </div>
                                </div>
                                <!-- /.couponSelectedAll -->
                                <p class="leftCoouponSummary"><span class="webRupee" >Rs.</span> 150 still left!</p>
                            </div>
                            <!-- /.couponSummaryCont -->
                        </div>
                        <!-- /.editMode -->

                    </div>
                    <!-- /.couponSummaryRow -->

                    <div class="couponSummaryRow ">
                        <div class="smlPattern clearfix">
                            <h4 class="sectionHeader fLeft"><span class="collapseArrow">&nbsp;</span>Deals</h4>
                            <a href="#" class="fRight editLink">Edit</a>

                            <div class="couponSummaryCont cl">
                                <p class="topTxt">Tantra T-shirt | worth <span class="webRupee" >Rs.</span> 300</p>
                            </div>
                        </div>
                        <!-- /.smlPattern -->

                        <div class="editMode clearfix" style="display:none">
                            <h4 class="sectionHeader"><span class="expandArrow">&nbsp;</span>Deals</h4>

                            <div class="couponSummaryCont cl">
                                <span class="dealsImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                            width="80"></span>
                            </div>
                            <!-- /.couponSummaryCont -->
                        </div>
                        <!-- /.editMode -->

                    </div>
                    <!-- /.couponSummaryRow -->

                    <div class="couponSummaryRow rowLast">
                        <div class="smlPattern clearfix">
                            <h4 class="sectionHeader fLeft"><span class="collapseArrow">&nbsp;</span>Your Details</h4>
                            <a href="#" class="fRight editLink">Edit</a>

                            <div class="couponSummaryCont cl">
                                <p class="topTxt">Rajesh Singh <br> 123 Jawahar Nagar....</p>
                            </div>
                        </div>
                        <!-- /.smlPattern -->

                        <div class="editMode clearfix" style="display:none">

                        </div>
                        <!-- /.editMode -->

                    </div>
                    <!-- /.couponSummaryRow -->

                </div>
                <!-- /.couponSelected  -->

                <div class="totalPayTxt clearfix"><span class="txt">Total Payment  </span><span
                        class="val">: Rs  510</span></div>
            </div>
            <!-- /.couponSelectSummary -->
        </div>
        <!-- /.rtSidebar -->

    </div>
</div>