<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isErrorPage="true"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>



<form:form name="fgPassword"  method="post"  modelAttribute="changepassword"   action="changepassword.htm" >

    <div class="clear"><label>Old Password  :</label>
    <spring:bind path="changepassword.oldPassword">
        <form:input id="oldPassword"  path="oldPassword" />
    </spring:bind>

    </div>

    <div class="clear"><label>New Password  :</label>
    <spring:bind path="changepassword.newPassword">
        <form:input id="newPassword"  path="newPassword" />
    </spring:bind>

    </div>

    <div class="clear"><label>Confirm Password  :</label>
    <spring:bind path="changepassword.confirmPassword">
        <form:input id="confirmPassword"  path="confirmPassword"  />
    </spring:bind>

    </div>

 <input type="submit"  value="Get Password" />

</form:form>
             <b><c:out value="${changePasswordSuccessMsg}"/></b>
