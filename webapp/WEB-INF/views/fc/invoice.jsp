
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
	<head>
		<title>FreeCharge.in - Recharge Made Free</title>

<link rel="apple-touch-icon" href="${mt:keyValue("imgprefix1")}/images/favicon-new.ico"/>
<link rel="icon" href="${mt:keyValue("imgprefix1")}/images/favicon-new.ico" type="image/x-icon">
<link rel="shortcut icon" href="${mt:keyValue("imgprefix1")}/images/favicon-new.ico" type="image/x-icon">
<link href="${mt:keyValue("cssprefix1")}/base.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css" >	
<link href="${mt:keyValue("cssprefix1")}/layout.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
<link href="${mt:keyValue("cssprefix2")}/invoice.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
<link href="${mt:keyValue("cssprefix1")}/ie6.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css"/>
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
        
         <!--[if lt IE 7]>
			<link rel="stylesheet" media="all" href="css/ie6.css?v=${mt:keyValue("version.no")}"/>
		<![endif]-->
 		</head>
		<body id="invoiceWrapper"  >
            	<div class="pageWidth" >
                    <h1 class="invoicePageHeader" ><strong>Thank you!!! </strong>for using www.freecharge.in</h1>
                    <div class="invvoiceMainConatienr" >
                    
                        <div class="invoiceCont" >
                        
                                <div class="invoiceContHeader clearfix" >
                                    <h4  class="titleMain fLeft" >BILL OF SUPPLY</h4>
                                    <a href="#" class="invoiceLogo fRight" ><img src="${mt:keyValue("imgprefix1")}/images/logo/freecharge.png" alt="FreeCharge.in - Recharge Made Free" width="151" height="34" ></a>
                                </div> <!-- /.invoiceContHeader -->
                                
                                <div class="invoiceContMiddle clearfix" >
                                    <div class="detailsOuter clearfix" >
                                        <ul class="detailsList fLeft" >
                                            <li>
                                                <span class="ltTxt" >Order Id</span>
                                                
                                                <c:choose>
                                                  <c:when test="${isWalletCash}">
                                                    <span class="rtTxt" >:&nbsp;  <c:out value="${orderId}"/></span>
                                                  </c:when>
                                                  <c:otherwise>
                                                <span class="rtTxt" >:&nbsp;  ${rechargeInfo.orderId}</span>
                                                  </c:otherwise>
                                                </c:choose>
                                            </li>
                                            <li>
                                                <c:choose>
                                                  <c:when test="${isWalletCash}">
                                                    <span class="ltTxt" >Payment Date</span>
                                                    <span class="rtTxt" >:&nbsp;  <c:out value="${paymentTime}"/></span>
                                                  </c:when>
                                                  <c:when test="${isGiftCard}">
                                                    <span class="ltTxt" >Payment Date</span>
                                                    <span class="rtTxt" >:&nbsp;  <c:out value="${paymentTime}"/></span>
                                                  </c:when>
                                                    <c:when test="${isInsurance}">
                                                        <span class="ltTxt" >Payment Date</span>
                                                        <span class="rtTxt" >:&nbsp;  <c:out value="${paymentTime}"/></span>
                                                    </c:when>
                                                  <c:otherwise>
                                                  <c:if test="${isHcoupon}">
                                               		 <span class="ltTxt" >Transaction Date</span>
                                                	</c:if>
                                                	<c:if test="${!isHcoupon}">
                                                    <c:choose>
                                                    <c:when test="${isUtilityPayment}">
                                                      <c:choose>
                                                      <c:when test="${isMetroPayment}">
                                                      <span class="ltTxt" >Recharge Date</span>
                                                      </c:when>
                                                      <c:otherwise>
                                                      <span class="ltTxt" >Payment Date</span>
                                                      </c:otherwise>
                                                      </c:choose>
                                                    </c:when>
                                                    <c:otherwise>
                                                		  <span class="ltTxt" >Recharge Date</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                                	</c:if>
                                                <span class="rtTxt" >:&nbsp;  <c:out value="${rechargeInfo.rechargedDate}"/></span>
                                                  </c:otherwise>
                                                </c:choose>
                                            </li>
                                            <li>
                                                <c:if test="${! isWalletCash}">
                                                <c:if test="${!isHcoupon}">
                                                  <c:choose>
                                                    <c:when test="${isUtilityPayment}">
                                                    <c:choose>
                                                    <c:when test="${isMetroPayment}">
                                                      <span class="ltTxt" >Card Number</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                      <span class="ltTxt" >Account Number</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                                      <span class="rtTxt" >:&nbsp;<strong><c:out value="${rechargeInfo.rechargedNo}"/></strong></span>
                                                    </c:when>
                                                    <c:when test="${isGiftCard}">
                                                      <span class="ltTxt" >Beneficiary Email</span>
                                                      <span class="rtTxt" >:&nbsp; <c:out value="${beneficiaryEmail}"/></span>
                                                    </c:when>
                                                      <c:when test="${isInsurance}">
                                                          <span class="ltTxt" >Biller Name</span>
                                                          <span class="rtTxt" >:&nbsp; <c:out value="${billerName}"/></span>
                                                      </c:when>
                                                      <c:when test="${isMunicipal}">
                                                          <span class="ltTxt" >Municipality Name</span>
                                                          <span class="rtTxt" >:&nbsp; <c:out value="${municipalityName}"/></span>
                                                      </c:when>
                                                    <c:otherwise>
                                                      <span class="ltTxt" >Recharged  No.</span>
                                                      <span class="rtTxt" >:&nbsp; <c:out value="${rechargeInfo.rechargedNo}"/></span>
                                                    </c:otherwise>
                                                  </c:choose>
                                                </c:if>
                                                </c:if>
                                            </li>
                                            <c:if test="${!isWalletCash}">
                                                <c:if test="${!isHcoupon}">
                                                    <c:choose>
                                                        <c:when test="${isInsurance}">
                                                            <li>
                                                                <span class="ltTxt">Policy Number</span>
                                                                <span class="rtTxt">:&nbsp; <c:out
                                                                        value="${policyNumber}"/></span>
                                                            </li>
                                                        </c:when>
                                                        <c:when test="${isMunicipal}">
                                                            <li>
                                                                <span class="ltTxt">Consumer Number</span>
                                                                <span class="rtTxt">:&nbsp; <c:out
                                                                        value="${consumerAccountNumber}"/></span>
                                                            </li>
                                                            <li>
                                                                <span class="ltTxt">Service Type</span>
                                                                <span class="rtTxt">:&nbsp; <c:out
                                                                        value="${serviceType}"/></span>
                                                            </li>
                                                            <li>
                                                                <span class="ltTxt">Receipt Number</span>
                                                                <span class="rtTxt">:&nbsp; <c:out
                                                                        value="${receiptNumber}"/></span>
                                                            </li>
                                                        </c:when>
                                                    </c:choose>
                                                </c:if>
                                            </c:if>
                                        </ul> <!-- /.detailsList -->
                                           
                                        <div class="paymentTable fRight" >
                                           <table width="100%" border="0" cellspacing="0" cellpadding="0" class="invoiceTable" >
                                                <tr>
                                                    <th width="11%"  class="center"  >S.No</th>
                                                    <th width="55%"  >Particular</th>
                                                    <th width="34%" >Amount</th>
                                                </tr>
                                              <c:choose>
                                                <c:when test="${isWalletCash}">
                                              <tr>
                                                <td class="center" >1.</td>
                                                    <td> Credits Top-up Amount</td>
                                                    <td>Rs. <c:out value="${totalAmount}"/> </td>
                                                   </tr>
                                                
                                                   <tr>
                                                     <td colspan="2" class="alignRt" > Total Amount</td>
                                                     <td><strong>Rs. <c:out value="${totalAmount}"/></strong></td>
                                                   </tr>
                                                
                                                </c:when>
                                                <c:otherwise>
                                                  <tr>
                                                    <td class="center" >1.</td>                                                    
                                                    <c:choose>
                                                    	<c:when test="${isGiftCard}">
                                                    		<td> Amount</td>
                                                    	</c:when>
                                                    	<c:when test="${isHcoupon}">
                                                    		<td> Transaction Amount</td>
                                                    	</c:when>
                                                    	<c:when test="${!isHcoupon}">
                                                    			<c:choose>
				                                                    <c:when test="${isUtilityPayment}">
				                                                    <c:choose>
				                                                    	<c:when test="${isMetroPayment}">
				                                                    		<td> Recharge Amount</td>
				                                                    	</c:when>
				                                                    	<c:otherwise>
					                                                      <td> Bill Amount</td>
					                                                    </c:otherwise>
				                                                    </c:choose>
				                                                    </c:when>
                                                                    <c:when test="${isMunicipal}">
                                                                        <td> Bill Amount</td>
                                                                    </c:when>
				                                                    <c:otherwise>
				                                                      <td> Recharge Amount</td>
				                                                    </c:otherwise>
				                                                </c:choose>
                                                    	</c:when>
                                                    	<c:otherwise>
                                                    	</c:otherwise>
                                                   </c:choose>
                                                <td>Rs. <c:out value="${rechargeInfo.rechargeAmount}"/> </td>
                                              </tr>
											  <tr>
                                                <td class="center" >2.</td>
                                                <c:if test="${rechargeInfo.hasPaidCoupons}">
                                               		 <td>Coupon charges</td>
                                               		 <td>Rs. <c:out value="${rechargeInfo.couponCharges}"/></td>
                                                </c:if>
                                                <c:if test="${!rechargeInfo.hasPaidCoupons}">
                                                	<td>Coupon charges</td>
                                                	<td>Rs. 0.0</td>
                                                </c:if>
                                              </tr>
                                              
                                              <c:if test="${rechargeInfo.hasFeeDetails}">
											  <tr>
                                                <td class="center" >3.</td>
                                                <td>Payment Handling Charges</td>
                                                <td>Rs. <c:out value="${rechargeInfo.feeDetails.paymentHandlingCharge}"/></td>
                                              </tr>
                                              <tr>
                                                <td class="center" >4.</td>
                                                <td>Service tax</td>
                                                <td>Rs. <c:out value="${rechargeInfo.feeDetails.serviceTax}"/></td>
                                              </tr>
                                              <tr>
                                                <td class="center" >5.</td>
                                                <td>Swachh Bharat Cess</td>
                                                <td>Rs. <c:out value="${rechargeInfo.feeDetails.swachhBharatCess}"/></td>
                                              </tr>
                                              <tr>
                                                <td class="center" >6.</td>
                                                <td>Krishi Kalyan Cess</td>
                                                <td>Rs. <c:out value="${rechargeInfo.feeDetails.krishiKalyanCess}"/></td>
                                              </tr>
                                              </c:if>
                                              
                                              <c:if test="${rechargeInfo.hasFreeFund}">
											  <tr>
											  	<c:if test="${rechargeInfo.hasFeeDetails}">
                                                	<td class="center" >7.</td>
                                                </c:if>
                                                <c:if test="${rechargeInfo.hasFeeDetails}">
                                                	<td class="center" >3.</td>
                                                </c:if>
                                                <td>FreeFund Discount</td>
                                                <td>(Rs. <c:out value="${rechargeInfo.freeFundAmount}"/>)</td>
                                              </tr>
                                              </c:if>
                                                    <c:if test="${isMunicipal}">
                                                        <tr>
                                                            <td class="center">3.</td>
                                                            <td>Convenience Fee</td>
                                                            <td>0.00</td>
                                                        </tr>
                                                        <tr style="height: 46px;">
                                                            <td class="center"></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </c:if>
                                              <tr>
                                                <td colspan="2" class="alignRt" > Total Amount</td>
                                                <td><strong>Rs. <c:out value="${totalAmount}"/></strong></td>
                                              </tr>
                                                </c:otherwise>
                                              </c:choose>
                                          </table> <!-- /.invoiceTable -->
                                        </div> <!-- /paymentTable -->
                                    </div>    
                                    
                                    <p class="contactDetails cl" >
                                             Accelyst Solutions Pvt Ltd   
                                            <br>Registered Address : 01st Floor, Corporate Park II Building, Sion Trombay Road, Next to Swastik chambers, Mumbai -400071, Maharashtra
	                                        <br>Address : 16th Floor, DLF Cyber Greens, Tower-C, Phase-III, DLF Cyber City, Gurugram-122002
	                                        <br>Company Pan No. AAHCA2192H
	                                        <br>CIN: U72900MH2008PTC185202
	                                        <br>Contact us at care@freecharge.com
	                                        <br>Website: www.freecharge.com
                                    </p> <!-- /.contactDetails -->
                                    <p class="contactDetails cl" style="text-align:right"> <img src="https://d32vr05tkg9faf.cloudfront.net/content/images/invoice/SignInvoice.png"> <br> Authorised Signatory
                                    </p>
                                </div> <!-- /.invoiceContMiddle -->
                        </div><!-- /.invoiceCont -->
                         <c:if test="${fn:length(cartInfo) gt 0 }">
                        <div class="invoiceCont invoiceContlast" >
                        
                                <div class="invoiceContHeader clearfix" >
                                    <h4  class="titleMain fLeft" >COUPONS DETAIL</h4>
                                    <a href="#" class="invoiceLogo fRight" ><img src="${mt:keyValue("imgprefix1")}/images/logo/freecharge.png" alt="FreeCharge.in - Recharge Made Free" width="151" height="34" ></a>
                                </div> <!-- /.invoiceContHeader -->
                                
                                <div class="invoiceContMiddle clearfix" >
                                    <div class="detailsOuter clearfix" >
                                        <ul class="detailsList fLeft" >
                                            <li>
                                                <span class="ltTxt" >Order Id</span>
                                                <span class="rtTxt" >:&nbsp;  ${rechargeInfo.orderId}</span>
                                            </li>
                                            <li>
                                            		<c:if test="${isHcoupon}">
                                               		 <span class="ltTxt" >Transaction Date</span>
                                                	</c:if>
                                                	<c:if test="${!isHcoupon}">
                                                    <c:choose>
                                                    <c:when test="${isUtilityPayment}">
                                                      <c:choose>
                                                      <c:when test="${isMetroPayment}">
                                                      <span class="ltTxt" >Recharged Date</span>
                                                      </c:when>
                                                      <c:otherwise>
                                                      <span class="ltTxt" >Payment Date</span>
                                                      </c:otherwise>
                                                      </c:choose>
                                                    </c:when>
                                                    <c:otherwise>
                                                      <span class="ltTxt" >Recharged Date</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                                		
                                                	</c:if>
                                                <span class="rtTxt" >:&nbsp;  ${rechargeInfo.rechargedDate}</span>
                                            </li>
                                            <li>
                                            	 <c:if test="${!isHcoupon}">
                                                <c:choose>
                                                  <c:when test="${isUtilityPayment}">
                                                    <c:choose>
                                                    <c:when test="${isMetroPayment}">
                                                      <span class="ltTxt" >Card Number</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                      <span class="ltTxt" >Account Number</span>
                                                    </c:otherwise>
                                                    </c:choose>
                                                    <span class="rtTxt" >:&nbsp;<strong><c:out value="${rechargeInfo.rechargedNo}"/></strong></span>
                                                  </c:when>
                                                  <c:otherwise>
                                                    <span class="ltTxt" >Recharged  No.</span>
                                                    <span class="rtTxt" >:&nbsp;  +91 <strong><c:out value="${rechargeInfo.rechargedNo}"/></strong></span>
                                                  </c:otherwise>
                                                </c:choose>
                                                	
                                                </c:if>
                                            </li>
                                        </ul> <!-- /.detailsList -->
                                           
                                        <div class="couponInvoiceTable fRight" >
                                           <table width="100%" border="0" cellspacing="0" cellpadding="0" class="invoiceTable" >
                                                <tr>
                                                    <th width="9%"  class="center"  >S.No</th>
                                                    <th width="43%"  >Particular</th>
                                                    <th width="13%"  class="center"   >Qty.</th>
                                                    <th width="35%" >Amount</th>
                                                </tr>
                                                <c:forEach items="${cartInfo}" var="cartInfo"  varStatus="cnt" >
	                                              <tr>
	                                                <td class="center" >${cnt.count}</td>
	                                                <td>${cartInfo.campaignName}</td>
	                                                <td  class="center" >${cartInfo.quantity}</td>
	                                                <td>Rs. ${cartInfo.couponValue}</td>
	                                              </tr>
                                             </c:forEach>
                                              <tr>
                                                <td colspan="3" class="alignRt" > Total Amount</td>
                                                <td><strong>Rs. <c:out value="${totalAmount}"/></strong></td>
                                              </tr>
                                          </table> <!-- /.invoiceTable -->
                                        </div> <!-- /couponInvoiceTable -->
                                    </div>    
                                        
                                    <p class="contactDetails cl" >
                                             Accelyst Solutions Pvt Ltd   
                                            <br>Registered Address:  01st Floor, Corporate Park II Building, Sion Trombay Road, Next to Swastik chambers, Mumbai -400071, Maharashtra
                                            <br>Address: 16th Floor, DLF Cyber Greens, Tower-C, Phase-III, DLF Cyber City, Gurugram-122002
	                                        <br>Company Pan No. AAHCA2192H
	                                        <br>CIN: U72900MH2008PTC185202
	                                        <br>Contact us at care@freecharge.com
	                                        <br>Website: www.freecharge.com
                                    </p>
                                    </p> <!-- /.contactDetails --> 
                                    
                                    <p class="contactDetails cl" style="text-align:right"> <img src="https://d32vr05tkg9faf.cloudfront.net/content/images/invoice/SignInvoice.png"> <br> Authorised Signatory
                                    </p>
                                </div> <!-- /.invoiceContMiddle -->

                        
                        </div><!-- /.invoiceCont -->
                        </c:if>
                     </div><!-- /.invvoiceMainConatienr -->   
                </div> <!-- /.pageWidth -->
		</body>
</html>
