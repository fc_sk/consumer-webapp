<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="fc-card" id="recharge-status-container">
    <div class="status recharge-fail">
        <h2>Recharge Failed</h2>
        <p><img src="${mt:keyValue("imgprefix1")}/images/icons/sad_smiley.png" title="sad!" alt="sad!"></p>
        <p>Uh oh! Your recharge was <strong class="text-color-3" >unsuccessful</strong>.</p>
        <p>Your payment refund will reflect in your <span class="text-color-1">Credits</span><i class="icon-info" id="fcb-recharge-fail-tip"></i> soon. </p>
        <div id="fcb-recharge-fail-tooltip" style="display: none">
            <h6>What are Credits?</h6>
            <p>If for some reason, your recharge is unsuccessful, we make a refund to your FreeCharge Credits. Your money is absolutely safe, secure and available to you for future transactions on <a href="/app/home.htm" target="_blank">FreeCharge.in</a>. What's more, you can also deposit cash in your Credits anytime to facilitate faster and easier recharging by clicking <a href="/app/mycredits.htm" target="_blank">here</a>. </p>
            <p><a href="/app/faq.htm" target="_blank">Know more</a></p>
                   </div> 
        <br>
        <p>Your Order ID for this transaction is - <strong>${rechargeInitiateWebDo.orderId}</strong></p>
            
        </div>
    <div class="recharge-info clearfix">
        <div class="operator-logo">
            <img src="${mt:keyValue("imgprefix1")}/${operatorImgUrl}" title="${operator}" alt="${operator}">
    </div>
        <div class="service-number">
            <div><c:out value="${rechargeInitiateWebDo.mobileNo}"/></div>
            <p>${product} | ${operator} | ${circle}</p>
</div>
        <div class="amount">
            Rs. <c:out value="${rechargeInitiateWebDo.amount}"/>
        </div>
    </div>
</div>

<script type="text/javascript">
_gaq.push(['_trackEvent', 'Recharge Status', 'IN Failed']);
</script> 
