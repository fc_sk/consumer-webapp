<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
<div class="accountpage-myProfileForm" >
    <c:if test="${updstatus}">
        <p class="message-small-success" id="updatemsg" style="display:none;"><c:out value="${message}"/></p>
    </c:if>
    <c:if test="${!updstatus}">
        <p class="message-small message-small-failure" id="updatemsg" style="display:none;"><c:out value="${message}"/></p>
    </c:if>

    <form:form action="#" method="post" name="profileupdateform" id="profileupdateform" modelAttribute="userprofile"  >
        <input type="hidden" id='profileupdatestatus' value='${updstatus}'/>
        <div class="row" >
            <label><spring:message code="email" text="#email#">:</spring:message></label><c:out value="${userprofile.email}"/>
        </div>
        <spring:bind path="userprofile.email">
            <form:input id="email" path="email" type="hidden" readonly="readonly"/>
        </spring:bind>
        <div class="row" >
            <label><spring:message code="name" text="#name#"/>:</label>
            <spring:bind path="userprofile.title">
                <form:select path="title" id="title" Class="validate[required] select-title">
                    <c:forEach var='title' items='${usertitle}'>
                        <c:choose>
                            <c:when test="${title eq userprofile.title}">
                                <option value="${title}" selected="selected" >${title}.</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${title}">${title}.</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </form:select>
            </spring:bind>
            <spring:bind path="userprofile.name">
                <form:input id="name" path="name" class="validate[required,funcCall[firstName],custom[onlyLetterSp]]"/>
            </spring:bind>
        </div>
        <div class="row" >
            <label><spring:message code="mobile" text="#mobile#"/>:</label>
            <spring:bind path="userprofile.mobileNo">
                <form:input id="mobileNo" path="mobileNo" maxlength='10' class="validate[required,custom[onlyNumberSp],funcCall[mobileNoValidation]]" onkeypress="return isNumberKey(event,10,id)"/>
            </spring:bind>
        </div>
        <div class="row" >
            <label id="passwordlabel"><spring:message code="password" text="#password#"/>:</label>
            <span class="pwdStatic"  >********  &nbsp; &nbsp; &nbsp;<a href="javascript:void(0)" onClick="changePwd(1)" ><spring:message code="change.password" text="#change.password#"/></a></span>
            <span class="pwdDynamic" style="display:none" >
                <spring:bind path="userprofile.oldpassword">
                    <form:input id="oldpassword" path="oldpassword" name="oldpassword" type="password" onblur="validateOldpassword(this)" Class="validate[required,maxSize[20],funcCall[oldPasswordMin]] text-input " maxlength="20" />
                </spring:bind>
            </span>
        </div>
        <p class="message-small message-small-failure" id="pswderromsg" style="display:none;"></p>

        <div class="row pwdDynamic" style="display:none" >
            <label><spring:message code="new.password" text="#new.password#"/>:</label>
            <spring:bind path="userprofile.newpassword">
                <form:input id="newpassword" path="newpassword" type="password" cssClass="validate[required,funcCall[passwordMin],maxSize[20]] text-input " maxlength="20"/>
            </spring:bind>
        </div>
        <div class="row pwdDynamic" style="display:none" >
            <label><spring:message code="confirm.password" text="#confirm.password#"/>:</label>
            <spring:bind path="userprofile.confirmpassword">
                <form:input id="confirmpassword" path="confirmpassword" type="password"  Class="validate[required,funcCall[confirmPasswordMin],maxSize[20],equals[newpassword]] text-input " maxlength="20" />
            </spring:bind>
            <a href="javascript:void(0)" onClick="changePwd()" ><spring:message code="cancel" text="#cancel#"/></a>
        </div>
    </form:form>

    <div class="row" >
        <label><spring:message code="address" text="#address#"/>:</label>
         <div id="myShippingAdd_01"   class="addressEditWrapper" >
            <span class="address-static clearfix" >
               <span><c:out value="${userprofile.address}"/></span><a href="javascript:void(0)" class="" onClick="editShippingAdd(this, '01', 'submit');"><spring:message code="edit" text="#edit#"/></a> 
            </span>
            <div class="addressEditForm" style="display:none;" >
               <%-- Profile address form start  --%>
               <form:form action="#" method="post" name="addressupdateform" id="addressupdateform" modelAttribute="userprofile">
                   <div class="row">
                       <spring:bind path="userprofile.address">
                           <form:input id="address" path="address" maxlength="50"  Class="validate[required,funcCall[street]] text-input" />
                       </spring:bind>
                   </div>
                   <div class="row">
                       <label><spring:message code="area" text="#area#"/>:</label>
                       <spring:bind path="userprofile.area">
                           <form:input id="area"  path="area"   maxlength="50" Class="validate[required,funcCall[area]] text-input" />
                       </spring:bind>
                   </div>
                   <div class="row">
                       <label><spring:message code="landmark" text="#landmark#"/>:</label>
                       <spring:bind path="userprofile.landmar">
                           <form:input id="landmark"  path="landmar"   maxlength="50" Class="validate[requered]"  />
                       </spring:bind>
                   </div>
                   <spring:bind path="userprofile.email">
                       <form:input id="email" path="email" type="hidden" 	readonly="readonly"/>
                   </spring:bind>
                   <div class="row">
                       <label><spring:message code="state" text="#state#"/>:</label>
                       <spring:bind path="userprofile.state">
                           <form:select path="state" id="state" Class="validate[required]" onchange="getCityList()" >
                               <c:forEach var="stateMaster" items="${userprofile.statelist}">
                                   <c:choose>
                                       <c:when test="${stateMaster.stateMasterId eq userprofile.state}">
                                           <option value='${stateMaster.stateMasterId}' selected="selected">${stateMaster.stateName}</option>
                                       </c:when>
                                       <c:otherwise><option value='${stateMaster.stateMasterId}'>${stateMaster.stateName}</option></c:otherwise>
                                   </c:choose>
                               </c:forEach>
                           </form:select>
                       </spring:bind>
                   </div>

                   <div class="row">
                       <label><spring:message code="city" text="#city#"/>:</label>
                       <span id="citydiv">
                           <spring:bind path="userprofile.city">
                               <form:select path="city" id="city" Class="validate[required]" >
                                   <c:forEach var="citymaster" items="${userprofile.citylist}">
                                       <c:choose>
                                           <c:when test="${citymaster.cityMasterId eq userprofile.city}">
                                               <option value="${citymaster.cityMasterId}" selected="selected">${citymaster.cityName}</option>
                                           </c:when>
                                           <c:otherwise>
                                               <option value="${citymaster.cityMasterId}">${citymaster.cityName}</option>
                                           </c:otherwise>
                                       </c:choose>
                                   </c:forEach>
                               </form:select>
                           </spring:bind>
                       </span>
                   </div>

                   <div class="row">
                       <label><spring:message code="pin.code" text="#pin.code#"/>:</label>
                       <spring:bind path="userprofile.pincode">
                           <form:input id="pincode"  path="pincode" class="validate[required,custom[onlyNumberSp],funcCall[postalCodeValidation]]"  onkeypress="return isNumberKey(event,6,id)"   maxlength="6" />
                       </spring:bind>
                   </div>

                   <div class="row">
                       <label></label>
                       <input type="button" class="button buttonThree"  onclick="updateProfileAddress()" value="Save Address"  />
                       <input type="button"  onclick="saveShippingAdd(this,'01', 'submit')" value="Cancel" class="button" />
                   </div>
               </form:form>
                <%-- Profile address form end  --%>
           </div>
       </div>
     </div>

    <div class="row" id="profileupdateformdiv">
        <label></label>
        <input type="button" id="updateprofilebtn" onclick="updateProfile();" value="Save Changes" class="button buttonThree">
    </div>
</div>
