<%@page import="com.freecharge.common.util.FCSessionUtil"%>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeContextDirectory" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="com.freecharge.payment.util.PaymentConstants" %>
<%@ page import="com.freecharge.web.controller.ProductPaymentController" %>
<%@ page import="com.freecharge.common.framework.util.CSRFTokenManager" %>
<%@ page import="com.freecharge.cardstorage.CardStorageKey" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    FreechargeSession fs = FreechargeContextDirectory.get().getFreechargeSession();
    Object login = false;
    if (fs != null) {
        Map<String, Object> sessionData = fs.getSessionData();
        if (sessionData != null)
            login = sessionData.get(WebConstants.SESSION_USER_IS_LOGIN);
    }

    String lookId = request.getParameter("lid");
    String lid = request.getParameter("lid");
    String lookupid = request.getParameter("lookupID");

    if (lookupid == null && lid != null) {
        lookupid = lid;
    }

    String productType = FCSessionUtil.getRechargeType(lookupid, fs);
    Integer productId = FCConstants.productMasterMapReverse.get(productType);
    List<String> allowablePGList = (List<String>) request.getAttribute("allowablePGList");
    if (allowablePGList == null) {
        allowablePGList = new ArrayList<String>();
    }
%>

<c:set value="<%=login%>" var="login"/>
<c:set var="primaryProductType" value='<%=FCConstants.PRIMARY_PRODUCT_TYPE%>' scope="application"/>
<c:set var="secondaryProductType" value='<%=FCConstants.SECONDARY_PRODUCT_TYPE%>' scope="application"/>
<c:set var="chargesProductType" value='<%=FCConstants.CHARGES_PRODUCT_TYPE%>' scope="application"/>
<c:set var="crosssellEntityName" value='<%=FCConstants.ENTITY_NAME_FOR_CROSSSELL%>' scope="application"/>
<c:set var="couponEntityName" value='<%=FCConstants.COUPON_ENTITY_ID%>' scope="application"/>
<c:set var="lid" value='<%=request.getParameter(FCConstants.LOOKUPID)%>' />

<c:set var="lookupID" value="<%=request.getParameter(\"lookupID\")%>" scope="request" />
<c:set var="walletDisplayName" value="<%=FCConstants.WALLET_DISPLAY_NAME%>"></c:set>

<input type="hidden" id="productType" name="productType" value="<%=productType%>"/>
<input type="hidden" value="2" name="pagecount" id="pagecount"/>

<div class="twelve columns">

<c:set var="ptypes" value="${paymentTypes}" scope="request" />
<%-- now the product type hard coded later we have to change dynamically --%>
<input type="hidden" id="productType" name="productType" value="<%=productType%>"/>

<%
    if (allowablePGList.contains(PaymentConstants.ZEROPAY_PAYMENT_OPTION)) {
%>
 
 <div id="zeroPaymentOptionsBlock" > <%--Call refreshAmountToPay(lid) to enable this --%>
       <form name="ZeroPay" action="${mt:keyValue("application.domain")}${mt:keyValue("application.product.payment.request.url")}" method="post" id="ZeroPay">
       	 <input type="hidden" name="paymenttype" id="8-paymenttype" value="<%=PaymentConstants.PAYMENT_TYPE_ZEROPAY%>" />
           <input type="hidden" name="productId" id="8-productId" value="<%=productId%>" />
           <input type="hidden" name="affiliateid" id="8-affiliateid" value="<%=FCConstants.DEFAULT_AFFILIATE_ID%>" />
           <input type="hidden" name="lookupID" id="8-lookupID" value="<%=lookupid%>" />
           <input type="hidden" name="cartId" id="8-cartId" value="${cartId}" />
           <input type="hidden" name="<%=CSRFTokenManager.CSRF_REQUEST_IDENTIFIER%>"  value="${mt:csrfToken()}" />
           <div class="payment-block">
	           <div class="payment-button-main clearfix">
		            <p>
		            	Congratulations !! <br/> The Total payable amount is <strong><span class="webRupee">Rs.</span><span class="total-amount-to-pay">0</span></strong>
		            	<br/>
		            	<button type="button" onclick="_gaq.push(['_trackEvent', 'Button', 'Make Payment']);doPayment('8');" class="button buttonThree rf">Continue &rarr;</button>
		            </p>
		       </div>
	       </div> 
	       
       </form>
   </div> 
<%
     } else {
 %>

<div class="page-head">
    <h2>Payment Options</h2>
    <hr />
</div>

<c:if test="${payment eq 'fail'}">
    <%--payment failure message--%>
    <%-- Message for Payment fauilure due to invalid payment Starts --%>
    <div class="row">
        <p class="failureMsgBig"  id="GAcall" >Sure you punched in the right bank details? Your payment wasn&#39;t successful. <br>Retry, pretty please? </p>
        <c:if test="${isDeletedFreefund == true}">
          <p class="failureMsgBig"  id="GAcall" >Damn! Your Promo code has been reset. Don&#39;t want to miss your discount? Re-apply and retry!</p>
        </c:if>  
    </div>
    <script type="text/javascript">
        $(document.getElementById('GAcall')[0]).ready(function() {_gaq.push(['_trackEvent', 'Recharge Status', 'Payment Failed']);});
    </script>
</c:if>

<script type="text/javascript">
    function checkStoredCard() {
        <%-- When the user clicks on the debit card or credit card tab, if the user has any stored cards, check them by default --%>
        $('.payment-option').bind('click', function(event){
            if ($(event.target).attr('data-payment-type') == 'Credit Card') {
                if ($('#savedCreditCards').length > 0) {
                    $("input[name=creditCardSelection]:first").attr("checked", "checked");
                    ccCardSelection($("input[name=creditCardSelection]:first").get(0));
                }
            }

            if ($(event.target).attr('data-payment-type') == 'Debit Card') {
                checkStoredDebitCard();
            }
        });
    }

    function checkStoredDebitCard() {
        if ($('#savedDebitCards').length > 0) {
            $("input[name=debitCardSelection]:first").attr("checked", "checked");
            dbCardSelection($("input[name=debitCardSelection]:first").get(0));
        }
    }
</script>


<div class="payment-block">
    <div class="clearfix" id="nonZeroPaymentOptionsBlock">

		<%
		    if (allowablePGList.contains(PaymentConstants.ALL)) {
		%>    

            <c:if test="${(isWalletPayEnabled eq true)}">
            <%--fcb unit starts--%>
            <div id="fcBalanceSection" class="fcBalance clearfix" >
                <div class="block-head">
                    <h3>Available Credits <a class="highlighter" href="javascript:void(0)" id="fcb-amount-tip"><span class="webRupee">Rs.</span>${walletBalance}</a></h3>
                    <div id="fcb-amount-tooltip" style="display: none">
                        <p>You currently have <span class="webRupee">Rs.</span> ${walletBalance} in your Credits.</p>
                    </div>
                    <a class="more" href="javascript:void(0)" id="fcb-tip" >What are Credits?</a>
                    <div id="fcb-tooltip" style="display: none">
                        <h6>What are Credits?</h6>
                        <p>If for some reason, your recharge is unsuccessful, we make a refund to your FreeCharge Credits. Your money is totally safe with us, and you can use it anytime for future recharging. Find out more, <a href="/payment-options/freecharge-credits"  target="_blank">here.</a>You can also deposit cash to facilitate quick and secure recharging <a href="/app/mycredits.htm" target="_blank>">Do it here</a>. </p>
                    </div>
                </div>
                
                <c:if test="${eligibleForPartialPayment or eligibleForFullWalletPayment}">
                    <div class="block-content clearfix">
                        <ul class="block-summary" >
                            <li class="clearfix"><span class="lf">Amount payable </span><span class="amount" ><span class="webRupee">Rs.</span> <span id="walletTotalAmountToPay"> <c:out value="${payableTotalAmount}" /> </span> </span></li>
                            <li id="walletPayableAmountArea" class="clearfix fcb"><span class="lf" >Credits used </span><span class="amount" ><strong>-</strong> <span class="webRupee">Rs.</span> <span id="walletOnlyPayableAmount"> <c:out value="${payableWalletAmount}" /> </span> </span></li>
                            <li id="outstandingPayAmountArea" <c:if test="${eligibleForFullWalletPayment}">style="display: none;"</c:if> class="clearfix total"><span class="lf">Outstanding amount payable </span><span class="amount" ><span class="webRupee">Rs.</span> <span id="pgTotalAmountToPay"> <c:out value="${payablePGAmount}" />  </span> </span></li>
                        </ul>
                        <div id="fcPayOption" class="block-payment" >
                            <h6>Pay Using <spring:message code="fc.money" text="#fc.money#"/></h6>
                                <div id="partialPaymentArea" class="pay-partial" style="
                                <c:choose>
                                <c:when test="${eligibleForPartialPayment}"> display:block; </c:when>
                                <c:otherwise> display:none; </c:otherwise>
                                </c:choose>
                                ">
                                <p><strong>Note:</strong> You can partially pay for this transaction with Credits! If you choose to use your Credits, you will be required to pay the outstanding amount through the payment options below.</p>
                                <div>
                                    <input onclick="refreshOutstandingAmount()" type="checkbox" id="usePartialWalletPaymentCheck"  name="partialEnabled" 
                                    <c:if test="${eligibleForPartialPayment}">checked="checked"</c:if>
                                     />
                                    <label for="usePartialWalletPayment">Use Credits</label>
                                </div>
                                </div>

                                <div id="fullPaymentArea" class="pay-full" style="
                                <c:choose>
                                <c:when test="${eligibleForFullWalletPayment}"> display:block; </c:when>
                                <c:otherwise> display:none; </c:otherwise>
                                </c:choose>
                                "
                                >
                                <p><strong>Note:</strong> You have enough Credits to fully pay for this transaction! If you would like to save your Credits, please choose another payment option below.</p>
                                <div>
                                    <form name="FC Balance" action="${mt:keyValue("application.domain")}${mt:keyValue("application.product.payment.request.url")}" method="post" id="FC-Balance">
                                        <input type="hidden" name="paymenttype" id="6-paymenttype" value="<%=PaymentConstants.PAYMENT_TYPE_WALLET%>" />
                                        <input type="hidden" name="productId" id="6-productId" value="<%=productId%>" />
                                        <input type="hidden" name="affiliateid" id="6-affiliateid" value="<%=FCConstants.DEFAULT_AFFILIATE_ID%>" />
                                        <input type="hidden" name="lookupID" id="6-lookupID" value="<%=lookupid%>" />
                                        <input type="hidden" name="cartId" id="6-cartId" value="${cartId}" />
                                        <div>
                                            <button type="button" onclick="$(this).attr('disabled', 'disabled');_gaq.push(['_trackEvent', 'Button', 'Make Payment']);doWalletPayment(6, '');" class="buttonTwo small-button">Pay using Credits &rarr;</button>
                                            <em>-Or-</em>
                                            <button type="button" class="button small-button" onclick="handleOtherPayOptionShow()">Other payment options</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
            </div>
            </c:if>
            <%--fcb unit ends--%>


        <div class="payment-freefund-block clearfix">
            <c:choose>
                <c:when test="${containsFreefund}">
                    <p>
                     <c:choose>
                     	<c:when test="${(codeType eq 'generic-cashback-promocode') or (codeType eq 'cashback-promocode') }">
                     	 	<strong>You have successfully applied ${code} , your cash back will be processed within 24 - 48 hrs.</strong>
                     	</c:when>
                     	<c:otherwise>
                     		<strong>Congratulations !! Discount of <span class="webRupee">Rs.</span><span>${freefundItemPrice}</span> applied successfully.</strong>
                     	</c:otherwise>
                     </c:choose>
                    </p>
                </c:when>
                <c:otherwise>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" onchange="$('#redeemFreefund').toggle();" />
                            I have a freefund / promocode.
                        </label>
                    </div>


                    <form data-validate="parsley" id='redeemFreefund' action="javascript:void(0)" style="display: none;" role="form">

                        <%--capcha--%>
                            <div id="captcha-placement-div" style="display:none;">
                                <div id="captcha-placement-id"   >
                                    Please enter the letters you see in the image below.
                                    <div style="margin-bottom: 15px;">
                                        <div id="captcha-draw-div" style="border: 1px dotted #dddddd; width:300px; padding-top: 5px; padding-left: 5px;">

                                        </div>
                                        <div >
                                            <input name="captcha" type="text" autocomplete="off" placeholder="Enter text here"  class="englishOnlyField required" />
                                            <a href="javascript:void(0)" onclick="PROMOCODE.redrawCaptcha();" style="padding-left: 12px;">Refresh</a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                                <div class="input-group">
                                    <input name="freefundCode" id="freefundCode" type="text" data-required="true" data-required-message="Enter Freefund/Promocode code." class='custom-input input-large form-control freefund-input' data-error-container=".freefund-code-error" placeholder='Enter freefund code' style="font-size:16px;">
                                      <span class="input-group-btn">
                                          <button class="btn btn-secondary freefund-reedem" type="submit" id='redeemButton' onClick="PROMOCODE.redeemCode();">Redeem Now</button>
                                      </span>
                                </div>

                        <div class='freefund-code-success'></div>
                        <div class='freefund-code-error'></div>
                    </form>
                </c:otherwise>
            </c:choose>
        </div>



        <div id="allPaymentOpt" class="other-payments">
        <c:if test="${isWalletPayEnabled eq true}">
            <div class="sub-head">
                <h3>Other payment options</h3>
                <hr />
            </div>
        </c:if>
        <div class="tabsFour tab selectPayment clearfix" <c:if test="${eligibleForFullWalletPayment}">style="display: none" </c:if> >

        <ul id="paymenttab" class="tabTitle vertTab">
            <c:forEach items="${ptypes}" var="pts" varStatus="summaryCtr">
                <c:if test="${not (pts.id eq 7 or pts.id eq 6 or pts.id eq 8)}"> <%--if its not a HDFC Debit Card Payment Type. We are using this payment type for freefund --%>
                    <li>
                        <a href="javascript:void(0)" rel="${summaryCtr.count- 1}" class="payment-option" data-payment-type="${pts.name}">
                                ${pts.name}
                            <span class="rtArw" >&nbsp;</span>
                        </a>
                    </li>
                </c:if>
            </c:forEach>
        </ul>
        <div id="payment-options-content" class="tabsFour_content form clearfix">
        <c:forEach items="${ptypes}" var="pts" varStatus="summaryCtr">

        <c:if test="${not (pts.id eq 7 or pts.id eq 6 or pts.id eq 8)}"> <%--if its ndbExpYearot a HDFC Debit Card Payment Type. We are using this payment type for freefund --%>

        <div class="tab_contentFour unit">
        <form name="${pts.name}" action="${mt:keyValue("application.domain")}${mt:keyValue("application.product.payment.request.url")}" method="post" id="${fn:replace(pts.name,' ','-')}">
        <input type="hidden" name="paymenttype" id="${pts.id}-paymenttype" value="${pts.id}" />
        <input type="hidden" name="productId" id="${pts.id}-productId" value="<%=productId%>" />
        <input type="hidden" name="affiliateid" id="${pts.id}-affiliateid" value="<%=FCConstants.DEFAULT_AFFILIATE_ID%>" />
        <input type="hidden" name="lookupID" id="${pts.id}-lookupID" value="<%=lookupid %>" />
        <input type="hidden" name="cartId" id="${pts.id}-cartId" value="${cartId}" />



        <c:choose>
        <c:when test="${pts.name eq 'Net Banking'}">
            <ul class="bankingCont clearfix">
                <c:forEach items="${pts.options}" var="opts" varStatus="summaryCtr1">
                    <c:if test="${summaryCtr1.count le '6'}">
                        <li>
                            <label class="bankOption">
                                <c:choose>
                                    <c:when test="${summaryCtr1.count eq '1'}">
                                        <input type="radio" class="rdoIn" checked="checked" name="paymentoption" id="${pts.id}-paymentoption" value="${opts.code}" onclick="selectCheckBox();"/>
                                        <img  src="${mt:keyValue("imgprefix1")}/images/${opts.imgUrl}" alt="${opts.displayName}" title="${opts.displayName}" width="98" height="28">
                                    </c:when>
                                    <c:otherwise>
                                        <input type="radio" class="rdoIn" name="paymentoption" id="${pts.id}-paymentoption" value="${opts.code}" onclick="selectCheckBox();"/>
                                        <img  src="${mt:keyValue("imgprefix1")}/images/${opts.imgUrl}" alt="${opts.displayName}" title="${opts.displayName}" width="98" height="28" >
                                    </c:otherwise>
                                </c:choose>
                            </label>
                        </li>
                    </c:if>
                </c:forEach>
            </ul>
            <div class="otherBank">
                <div class="sub-head">
                    <h3>Other banks</h3>
                    <hr />
                </div>
                <select class="select1" name="paymentoptionNetBank" onchange="otherBankSelect();" id="paymentoptionNetBank">
                    <option value="">Online bank account</option>
                    <c:forEach items="${pts.options}" var="opts" varStatus="summaryCtr2">
                        <c:if test="${summaryCtr2.count gt '6'}">
                            <option value="${opts.code}">${opts.displayName}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>
            <p class="payment-instruction">In order to protect your account from fraudulent transactions we might redirect you to partner site,  which will verify your account information, before we proceed with the payment.</p>
        </c:when>


        <c:when test="${pts.name == 'Debit Card' && payUDebitEnabled}">

            <div class="payment-type-info">
                <p>
                    <span>Pay using your debit card.</span>
                    <span>(Cards issued in India only)</span>
                    <img src="${mt:keyValue("imgprefix1")}/images/icons/fc-debit-cards.png" />
                </p>
            </div>


            <div id="saved-cards-debit" class="saved-cards" style="display: none">
                <div class="sub-head">
                    <h3>Saved Cards</h3>
                    <hr />
                </div>
                <ul id="savedDebitCards">

                </ul>
            </div>


            <div class="new-card-entry" id="new-debit-card" style="display: none">
                <input type="radio" name="debitCardSelection" onclick="dbCardSelection(this)" value="new">
                <span><strong>Use a new debit card</strong></span>
            </div>


            <div id="seamless-debit-payment">
                <h5>Enter your debit card details <em>(Required fields are marked with <strong class="red">*</strong>)</em></h5>
                <div class="seamless-payment seamless-payment-debit clearfix form">
                    <div class="section card-type">
                        <label>Card type<strong class="red">*</strong></label>
                        <select id="seamless-debit" name="paymentoption" onchange="selectSeamlessDebit(this.value)" class="validate[required]">
                            <option value="">Select</option>
                            <option value="DMC">Mastercard</option>
                            <option value="DVI">Visa</option>
                            <option value="DCME">Maestro</option>
                            <option value="DSME">SBI Maestro</option>
                        </select>
                    </div>

                    <div class="section card-number">
                        <label>Card number<strong class="red" >*</strong></label>
                        <input autocomplete="on" type="text" onkeypress="return isNumberKey(event)" id="dbNum" maxlength="19" class="validate[required,custom[onlyNumberSp],funcCall[cardValidTest]] input"   />
                    </div>
                    <div id="dbNumOfferAlert"></div>

                    <div class="section card-name">
                        <label>Name on card<strong class="red" >*</strong></label>
                        <input type="text" id="dbHolderName" class="validate[required,custom[onlyLetterSp]] input"  />
                    </div>

                    <div class="section expiry-date">
                        <label>Expiry date<strong class="red">*</strong></label>
                        <select id="dbExpMon" class="validate[required]" >
                            <option value="" > Month </option>
                            <option> 01 </option>
                            <option> 02 </option>
                            <option> 03 </option>
                            <option> 04 </option>
                            <option> 05 </option>
                            <option> 06 </option>
                            <option> 07 </option>
                            <option> 08 </option>
                            <option> 09 </option>
                            <option> 10 </option>
                            <option> 11 </option>
                            <option> 12 </option>
                        </select>
                        <select id="dbExpYear"  class="validate[required,funcCall[cardExpiryValidation]]">
                            <option value=""> Year </option>
                            <option> 2014 </option>
                            <option> 2015 </option>
                            <option> 2016 </option>
                            <option> 2017 </option>
                            <option> 2018 </option>
                            <option> 2019 </option>
                            <option> 2020 </option>
                            <option> 2021 </option>
                            <option> 2022 </option>
                            <option> 2023 </option>
                            <option> 2024 </option>
                            <option> 2025 </option>
                            <option> 2026 </option>
                            <option> 2027 </option>
                            <option> 2028 </option>
                            <option> 2029 </option>
                            <option> 2030 </option>
                            <option> 2031 </option>
                            <option> 2032 </option>
                            <option> 2033 </option>
                            <option> 2034 </option>
                            <option> 2035 </option>
                            <option> 2036 </option>
                            <option> 2037 </option>
                            <option> 2038 </option>
                            <option> 2039 </option>
                            <option> 2040 </option>
                            <option> 2041 </option>
                            <option> 2042 </option>
                            <option> 2043 </option>
                            <option> 2044 </option>
                            <option> 2045 </option>
                            <option> 2046 </option>
                            <option> 2047 </option>
                            <option> 2048 </option>
                        </select>
                    </div>
                    <div class="section cvv-number">
                        <label>CVV2/CVC2 number<strong class="red" >*</strong></label>
                        <input autocomplete="off" type="password" id="dbCvv" onkeypress="return isNumberKey(event,3,id)" maxlength="3" class="validate[required,funcCall[cvvValid]]"  />
                        <span><em>The 3 digit number printed on the back of the card</em></span>
                    </div>

                    <div id="saveCardSection" class="section save-card">
                        <div class="">
                            <input type="checkbox"
                                   <c:if test="${cardStorageOptIn}">checked="checked"</c:if>
                                   name="saveCard"
                                   id="dbCardSaveBool" />

                            <label for="dbCardSaveBool">We'll remember your card during your recharge so you don't have to enter it again.</label>
                            <em>(note: we do not save or store your CVV2/CVC2 number)</em>
                            <p style="display: block">
                                <label>Enter a name for this card</label>
                                <input id="dbCardSaveName" type="text" placeholder="e.g My Debit Card">
                            </p>
                        </div>
                    </div>

                </div>
            </div>
            <p class="payment-instruction">In order to protect your card from fraudulent transactions we might redirect you to partner site,  which will verify your card information, before we proceed with the payment.</p>
        </c:when>

        <c:when test="${pts.name == 'Credit Card' && payUCCEnabled}">
            <%--<input type="radio" class="rdoIn" hidden="true" checked="checked" name="paymentoption" id="${pts.id}-paymentoption" value="${opts.code}" onclick="selectCheckBox();"/>--%>

            <div class="payment-type-info">
                <p>
                    <span>Pay using your credit card.</span>
                    <span>(Cards issued in India only)</span>
                    <img src="${mt:keyValue("imgprefix1")}/images/icons/fc-credit-cards.png" />
                </p>
            </div>


            <div id="saved-cards-credit" class="saved-cards" style="display: none">
                <h5>Saved credit cards</h5>
                <ul id="savedCreditCards">

                </ul>
            </div>


            <div class="new-card-entry" id="new-credit-card" style="display: none">
                <input type="radio" name="creditCardSelection" onchange="ccCardSelection(this)" value="new">
                <span><strong>Use a new credit card</strong></span>
            </div>


            <div id="seamless-credit-payment">

                <h5>Enter your credit card details <em>(Required fields are marked with <strong class="red">*</strong>)</em></h5>
                <div class="seamless-payment seamless-payment-credit clearfix form">

                    <div class="section card-number">
                        <label>Card number <strong class="red">*</strong></label>
                        <input autocomplete="on" type="text" id="ccNum" onkeypress="return isNumberKey(event)"  maxlength="19" class="validate[required,custom[onlyNumberSp],funcCall[cardValidTest]] input"  />
                    </div>
                    <div id="ccNumOfferAlert"></div>

                    <div class="section ">
                        <label>Name on card <strong class="red">*</strong></label>
                        <input type="text" id="ccHolderName" class="validate[required,custom[onlyLetterSp]] input"  />
                    </div>
                    <div class="section">
                        <label>Expiry date <strong class="red">*</strong></label>
                        <select id="ccExpMon" class="validate[required]" >
                            <option value=""> Month </option>
                            <option> 01 </option>
                            <option> 02 </option>
                            <option> 03 </option>
                            <option> 04 </option>
                            <option> 05 </option>
                            <option> 06 </option>
                            <option> 07 </option>
                            <option> 08 </option>
                            <option> 09 </option>
                            <option> 10 </option>
                            <option> 11 </option>
                            <option> 12 </option>
                        </select>
                        <select id="ccExpYear" class="validate[required,funcCall[cardExpiryValidation]]">
                            <option value=""> Year </option>
                            <option> 2014 </option>
                            <option> 2015 </option>
                            <option> 2016 </option>
                            <option> 2017 </option>
                            <option> 2018 </option>
                            <option> 2019 </option>
                            <option> 2020 </option>
                            <option> 2021 </option>
                            <option> 2022 </option>
                            <option> 2023 </option>
                            <option> 2024 </option>
                            <option> 2025 </option>
                            <option> 2026 </option>
                            <option> 2027 </option>
                            <option> 2028 </option>
                            <option> 2029 </option>
                            <option> 2030 </option>
                            <option> 2031 </option>
                            <option> 2032 </option>
                            <option> 2033 </option>
                            <option> 2034 </option>
                            <option> 2035 </option>
                            <option> 2036 </option>
                            <option> 2037 </option>
                            <option> 2038 </option>
                            <option> 2039 </option>
                            <option> 2040 </option>
                            <option> 2041 </option>
                            <option> 2042 </option>
                            <option> 2043 </option>
                            <option> 2044 </option>
                            <option> 2045 </option>
                            <option> 2046 </option>
                            <option> 2047 </option>
                            <option> 2048 </option>
                        </select>
                    </div>
                    <div class="section cvv-number">
                        <label>CVV2/CVC2 number <strong class="red">*</strong></label>
                        <input autocomplete="off" type="password" id="ccCvv"  onkeypress="return isNumberKey(event,3,id)" maxlength="3" class="validate[required,funcCall[cvvValid]]" />
                        <span><em>The 3 digit number printed on the back of the card</em></span>
                    </div>

                    <div class="section save-card">
                        <div>
                            <input
                                    type="checkbox"
                                    name="saveCard"
                                    id="ccCardSaveBool"
                                    <c:if test="${cardStorageOptIn}">checked="checked"</c:if>
                                    />
                            <label for="ccCardSaveBool">We'll remember your card during your recharge so you don't have to enter it again.</label>
                            <em>(note: we do not save or store your CVV2/CVC2 number)</em>
                            <p>
                                <label>Enter a name for this card</label>
                                <input id="ccCardSaveName" type="text" placeholder="e.g. My Credit Card">
                            </p>
                        </div>
                    </div>

                </div>
            </div>

            <p class="payment-instruction">In order to protect your card from fraudulent transactions we might redirect you to partner site,  which will verify your card information, before we proceed with the payment.</p>

        </c:when>

        <c:otherwise>
            <ul class="bankingCont clearfix">
                <c:forEach items="${pts.options}" var="opts" varStatus="summaryCtr1">
                    <li>
                        <label class="bankOption">
                            <c:choose>
                                <c:when test="${summaryCtr1.count eq '1'}">
                                    <input type="radio" class="rdoIn" checked="checked" name="paymentoption" id="${pts.id}-paymentoption" value="${opts.code}" />
                                    <img  src="${mt:keyValue("imgprefix1")}/images/${opts.imgUrl}" alt="${opts.displayName}" title="${opts.displayName}" width="98" height="28">
                                </c:when>
                                <c:otherwise>
                                    <input type="radio" class="rdoIn" name="paymentoption" id="${pts.id}-paymentoption" value="${opts.code}" />
                                    <img  src="${mt:keyValue("imgprefix1")}/images/${opts.imgUrl}" alt="${opts.displayName}" title="${opts.displayName}" width="98" height="28" >
                                </c:otherwise>
                            </c:choose>
                        </label>
                    </li>
                </c:forEach>
            </ul>
            <p class="payment-instruction">In order to protect your card from fraudulent transactions we might redirect you to partner site,  which will verify your card information, before we proceed with the payment.</p>
        </c:otherwise>
        </c:choose>
        <div class="payment-button-main clearfix">
            <p>Total payable amount: <strong><span class="webRupee">Rs.</span><span class="total-amount-to-pay">${payableTotalAmount}</span></strong></p>
            <button type="button" onclick="_gaq.push(['_trackEvent', 'Button', 'Make Payment']);doPayment('${pts.id}');" class="button buttonThree rf">Make payment &rarr;</button>
        </div>
        </form>
        </div>
        </c:if>
        </c:forEach>
        </div>
        </div>
        </div>


			<%
			    } else if (allowablePGList.contains(PaymentConstants.ICICNETBANKING_PAYMENT_OPTION)) {
			%>
                <div class="promoCodeBankOptBlock clearfix" id="promoCodeBankOptBlock-icici-netbanking" style="padding:10px;" >
                    <form name="Net Banking" action="${mt:keyValue("application.domain")}${mt:keyValue("application.product.payment.request.url")}" method="post" id="Net-Banking">
                        <span class="bankingCont" >
                        <input type="hidden" name="paymenttype" id="30-paymenttype" value="<%=PaymentConstants.PAYMENT_TYPE_NETBANKING%>" />
                        <input type="hidden" name="productId" id="30-productId" value="<%=productId%>" />
                        <input type="hidden" name="affiliateid" id="30-affiliateid" value="<%=FCConstants.DEFAULT_AFFILIATE_ID%>" />
                        <input type="hidden" name="lookupID" id="30-lookupID" value="<%=lookupid%>" />
                        <input type="hidden" name="cartId" id="30-cartId" value="${cartId}" />
                        <label class="bankOption"><input type="radio" class="rdoIn" id="30-paymentoption" name="rdo" checked value="<%=PaymentConstants.ICICNETBANKING_PAYMENT_OPTION%>"  ><img src="${mt:keyValue("imgprefix1")}/images/${opts.imgUrl}/icicibank.png" alt="Icici Bank" title="Icici Bank" width="98" height="28" > <span class="txtLbl" >Net Banking</span></label>
                        </span>
                        <p class="otherOptLink" style="position:relative; top:9px; margin-left: 10px" >
                            <a href="#otherPaymentOptMsg" class="otherPaymentOpt" >Other payment options</a>
                        </p>
                        <br>
                        <div class="payment-button-main clearfix">
                            <p>Total payable amount: <strong><span class="webRupee">Rs.</span><span class="total-amount-to-pay">${payableTotalAmount}</span></strong></p>
                            <button type="button" onclick="_gaq.push(['_trackEvent', 'Button', 'Make Payment']);doPayment('30');" class="button buttonThree rf">Make payment &rarr;</button>
                        </div>
                    </form>
                </div>
			<%
			    } else if (allowablePGList.contains(PaymentConstants.HDFCNETBANKING_PAYMENT_OPTION)) {
			%>
                <div class="promoCodeBankOptBlock clearfix" id="promoCodeBankOptBlock-hdfc-netbanking" style="padding:10px;" >
                    <form name="Net Banking" action="${mt:keyValue("application.domain")}${mt:keyValue("application.product.payment.request.url")}" method="post" id="Net-Banking">
                        <span class="bankingCont" >
                        <input type="hidden" name="paymenttype" id="31-paymenttype" value="<%=PaymentConstants.PAYMENT_TYPE_NETBANKING%>" />
                        <input type="hidden" name="productId" id="31-productId" value="<%=productId%>" />
                        <input type="hidden" name="affiliateid" id="31-affiliateid" value="<%=FCConstants.DEFAULT_AFFILIATE_ID%>" />
                        <input type="hidden" name="lookupID" id="31-lookupID" value="<%=lookupid%>" />
                        <input type="hidden" name="cartId" id="31-cartId" value="${cartId}" />
                        <label class="bankOption"><input type="radio" class="rdoIn" id="31-paymentoption"  name="rdo" checked value="<%=PaymentConstants.HDFCNETBANKING_PAYMENT_OPTION%>" ><img src="${mt:keyValue("imgprefix1")}/images/${opts.imgUrl}/hdfcbank.png" alt="HDFC Bank" title="HDFC Bank" width="98" height="28" > <span class="txtLbl" >Net Banking</span></label>
                        </span>
                        <p class="otherOptLink" style="position:relative; top:9px; margin-left: 10px" >
                            <a href="#otherPaymentOptMsg" class="otherPaymentOpt" >Other payment options</a>
                        </p>
                        <br>
                        <div class="payment-button-main clearfix">
                            <p>Total payable amount: <strong><span class="webRupee">Rs.</span><span class="total-amount-to-pay">${payableTotalAmount}</span></strong></p>
                            <button type="button" onclick="_gaq.push(['_trackEvent', 'Button', 'Make Payment']);doPayment('31');" class="button buttonThree rf">Make payment &rarr;</button>
                        </div>
                    </form>
                </div>
			<%
			    } else if (allowablePGList.contains(PaymentConstants.HDFCDEBITCARD_PAYMENT_OPTION)) {
			%>
                <div class="promoCodeBankOptBlock clearfix" id="promoCodeBankOptBlock-hdfc-debitcard" style="padding:10px;" >
                    <form name="Net Banking" action="${mt:keyValue("application.domain")}${mt:keyValue("application.product.payment.request.url")}" method="post" id="Net-Banking">
                        <span class="bankingCont" >
                        <input type="hidden" name="paymenttype" id="32-paymenttype" value="<%=PaymentConstants.PAYMENT_TYPE_HDFC_DEBIT_CARD%>" />
                        <input type="hidden" name="productId" id="32-productId" value="<%=productId%>" />
                        <input type="hidden" name="affiliateid" id="32-affiliateid" value="<%=FCConstants.DEFAULT_AFFILIATE_ID%>" />
                        <input type="hidden" name="lookupID" id="32-lookupID" value="<%=lookupid%>" />
                        <input type="hidden" name="cartId" id="32-cartId" value="${cartId}" />
                        <label class="bankOption"><input test="test" type="radio" class="rdoIn" id="32-paymentoption"  name="rdo" checked value="<%=PaymentConstants.HDFCDEBITCARD_PAYMENT_OPTION%>" ><img src="${mt:keyValue("imgprefix1")}/images/${opts.imgUrl}/hdfcbank.png" alt="HDFC Bank" title="HDFC Bank" width="98" height="28" > <span class="txtLbl" >Debit Card</span></label>
                        </span>
                        <p class="otherOptLink" style="position:relative; top:9px; margin-left: 10px" >
                            <a href="#otherPaymentOptMsg" class="otherPaymentOpt" >Other payment options</a>
                        </p>
                        <br>
                        <div class="payment-button-main clearfix">
                            <p>Total payable amount: <strong><span class="webRupee">Rs.</span><span class="total-amount-to-pay">${payableTotalAmount}</span></strong></p>
                            <button type="button" onclick="_gaq.push(['_trackEvent', 'Button', 'Make Payment']);doPayment('32');" class="button buttonThree rf">Make payment &rarr;</button>
                        </div>
                    </form>
                </div>
			<%
			    }
			%>
                <%-- Temporary fix done. Need to think more to make it bankwise --%>
                <div id="otherPaymentOptMsg" style="display:none" >
                    <div class="lightbox">
                        <h2>Other payment option?</h2>
                        <div>
                            <p>By selecting other payment option, you won't be able to avail promotional discount &amp; double coupons.</p>
                            <p><strong>Still want to continue with other payment option ?</strong></p>
                            <div class="center">
                                <input type="button" value="No"  onClick="$.fancybox.close();"  class="button">
                                <input type="button" value="Yes" onClick="loadAllPaymentOptions();"class="button buttonTwo">
                            </div>
                        </div>
                    </div>
                </div>
        </div>

</div>


<span style="display: none;" id="paymentwaitPageid"></span>
<input type="hidden" id="promoEntityStore" value="" />

<c:if test="${pgOpenNewWindow}">
    <div id='childWindowProxy' style='display: none;'></div>
</c:if>

<jsp:include page="promocode-js.jsp"></jsp:include>
<c:if test="${forceCaptcha eq true}">
<script type="text/javascript">
	$(function(){
		$("#captcha-placement-div").show();
		PROMOCODE.redrawCaptcha();
	});
</script>
</c:if>


<script type="text/javascript">
    $(document).ready(function(){      // on document ready
        $(".tabsFour").tabEffect();

        /* seamless payment form validation */
        jQuery("#Debit-Card, #Credit-Card").validationEngine({

            ajaxFormValidation: false ,
            scroll:false
        });
        
    	pageInit();
        checkStoredCard();
        refreshOutstandingAmountAlone();
    });       // document ready ends


    function otherBankSelect(){
	    $('form[id="Net-Banking"]').find("input:radio:checked").prop('checked',false);
    }

    function selectCheckBox(){
        $('#paymentoptionNetBank').val('');
    }

    /* Debit card type selection, validation */
    function selectSeamlessDebit(cardType) {
        $("form").validationEngine('hideAll');
        $("#dbCvv").removeClass().addClass("validate[required,funcCall[cvvValid]]");
        $("#dbExpMon , #dbExpYear").addClass("validate[required]");
        $("#dbNum").attr("class" , "validate[required,funcCall[cardValidTest], custom[onlyNumberSp]] input");
        if (cardType == 'DMC') {
            $("#dbNum").attr("class" , "validate[required,funcCall[masterCardValid],funcCall[cardValidTest], custom[onlyNumberSp]] input");
            $('.seamless-payment-debit .expiry-date, .seamless-payment-debit .cvv-number').show();
            $('.seamless-payment-debit .expiry-date, .seamless-payment-debit .cvv-number').find('.red').show();
        } else if (cardType == 'DVI') {
            $("#dbNum").attr("class" , "validate[required,funcCall[visaCardValid],funcCall[cardValidTest], custom[onlyNumberSp]] input");
            $('.seamless-payment-debit .expiry-date, .seamless-payment-debit .cvv-number').show();
            $('.seamless-payment-debit .expiry-date, .seamless-payment-debit .cvv-number').find('.red').show();
        } else if (cardType == 'DCME') {
            $('.seamless-payment-debit .expiry-date, .seamless-payment-debit .cvv-number').show();
            $('.seamless-payment-debit .expiry-date, .seamless-payment-debit .cvv-number').find('.red').hide();
            $("#dbCvv").removeClass().addClass("validate[funcCall[cvvValid]]");
            $("#dbExpMon , #dbExpYear").removeClass();
        } else if (cardType == 'DSME') {
            $('.seamless-payment-debit .expiry-date, .seamless-payment-debit .cvv-number').hide(); // hide the expiry date and cvv
        }
    }

    /* Wallet payment method */
    function doWalletPayment(paymentTypeId, paymentTypeCode) {
        var formData = createPaymentSubmitData(paymentTypeId, paymentTypeCode);
        submitPaymentRequest(formData);
    }


    /* Luhn test of credit card numbers */
    var luhn10 = function(a,b,c,d,e) {
          for(d = +a[b = a.length-1], e=0; b--;)
            c = +a[b], d += ++e % 2 ? 2 * c % 10 + (c > 4) : c;
          return !(d%10)
    }

    function readDbCvv() {
    	var current_debit_radio = $("input:radio[name=debitCardSelection]:checked");
    	var current_debit_cvvElement = current_debit_radio.parent('li').find('.saved-card-cvv');
    	if (current_debit_radio.val() == 'new') {
    		return $('#dbCvv').val();
    	} else {

            if (current_debit_cvvElement.length > 0) {

                var debit_cvv = current_debit_cvvElement.find('input').val()

                if (current_debit_cvvElement.hasClass("optional")) {
                    return debit_cvv;
                } else if (debit_cvv && debit_cvv.length == 3) {
                    if (!isNumeric(debit_cvv)) {
                        throw "Please enter valid CVV2/CVC2 number";
                    } else return debit_cvv;
                } else {
                    throw "Please enter the CVV2/CVC2 number.";
                }
            }
    	}
    }

    function readCcCvv() {
        var current_credit_radio = $("input:radio[name=creditCardSelection]:checked");
        var current_credit_cvvElement = current_credit_radio.parent('li').find('.saved-card-cvv');
        if (current_credit_radio.val() == 'new' || $("#new-credit-card").is(':hidden')) {
            return $('#ccCvv').val();
        } else {
            var credit_cvv = current_credit_cvvElement.find('input').val()
            if (credit_cvv && credit_cvv.length == 3) {
                if (!isNumeric(credit_cvv)) {
                    throw "Please enter valid CVV2/CVC2 number";
                } else return credit_cvv;
            } else {
                throw "Please enter the CVV2/CVC2 number.";
            }
        }
    }

    function handleChildWindowRequest(actionMethod, actionUrl, dataMap) {
        var div = $('#childWindowProxy');
        div.empty();
        div.html("<form id='childWindowProxyForm' method='" +
            actionMethod + "' action='" + actionUrl + "'></form>");
        var form = $('#childWindowProxyForm');
        form.empty();
        form.method=actionMethod;
        form.action=actionUrl;
        for (var key in dataMap) {
            var val = dataMap[key];
            form.append("<input type='hidden' name='" + key + "' value='" + val + "'>");
        }
        form.submit();
    }

    function enableDisableWallet(totalPrice) {
        if (totalPrice > ${walletBalance}) {
            $('.disabledWallet').show();
            $('.enabledWallet').hide();
        } else {
            $('.disabledWallet').hide();
            $('.enabledWallet').show();
        }
    }


    /* Card storage  */

    // handler is a function that is called with the list of cards as argument
    function withSavedCards(handler) {
        $.get('/app/cards/list.html', {}, function(data, textStatus, jqXHR) {
        	if (data['status'] == "success") {
                handler(data['payload']['cards'], textStatus, jqXHR);
    	    } else {
    	    	$('div.save-card').replaceWith('If you have saved your card with us, we are having trouble retrieving it because our card storage facility is temporarily unavailable. But do not worry, everything is safe with us!');
    	    }
        });
    }


    // Saved-Card deletion
    $(document).on("click", ".saved-cards .remove a, .saved-cards .card-expired-message a", function(){
        var cardToken = $(this).attr("data-saved-card-token");
        $.post('/app/card/' + cardToken + '/delete.html', function(data) {
            if (data['status'] == "success") {
                populateSavedCards();
            } else {
                alert("Error: Failed to delete. Try again later.");
            }
        });
    })

    function populateSavedCards() {

        withSavedCards(function (cards) {
    		if (cards != null) {
                $("#savedDebitCards").html("");
                $("#savedCreditCards").html("");
                var dbCount = 0;
                var ccCount = 0;
                for (var index in cards) {
                	var each = cards[index];
                	var cardName = each['nick_name'];
                	if (cardName == "") {
                		cardName = "Card #"+(++index);
                	}
                    var storedCardOfferID = "stored-card-offer-" + each['card_token'];
                    var storedCardNumberID = "stored-card-bin" + each['card_is_in'];
                    var cardExpiryMessage = '';
                    if (each['is_card_expire'] == 'true') {
                    	cardExpiryMessage = "<p class='card-expired-message' >Your card has expired! <a href='javascript:void(0)' data-saved-card-token='"+ each['card_token'] +"' >Click here</a> to remove it from your saved cards</p>";
                    }
                    if (each['card_type'] == "DEBIT") {
                        dbCount++;
                        var paymentCode = '';
                        if (each['card_brand'] == "VISA") {
                            paymentCode = "DVI";
                        }
                        if (each['card_brand'] == "MASTERCARD") {
                            paymentCode = "DMC";
                        }
                        if (each['card_brand'] == "MAESTRO") {
                            if (each['card_issuer'] == "SBI") {
                                paymentCode = "DSME";
                            } else {
                                paymentCode = "DCME";
                            }
                        }
                        $("#savedDebitCards").append(
                                "<li class='clearfix' id='" + each['card_token'] + "'>" +
                                "<div class='remove'><a href='javascript:void(0)' data-saved-card-token='"+ each['card_token'] +"' class='icon-trash' >Delete this card</a></div>" +        
                                "<input type='radio' class='option' name='debitCardSelection' onclick='dbCardSelection(this)' cardBrand='" + each['card_brand'] + "' cardIssuer='"+ each['card_issuer'] +"' paymentCode='" + paymentCode + "'>" +
                                        "<div class='info'>" +
                                        "<span class='"+ each['card_brand'] +"'></span>" +
                                        "<p class='name'>" + cardName + "</p>" +
                                        "<p class='type'>" + each['card_number'] + ", " + each['card_issuer'] + "</p>" +
                                        "</div>" +
                                        "<input type='hidden' id='" + storedCardNumberID + "' value='" + each['card_is_in'] + "**********'/>" +
                                        "<div class='offer-check'>" +
                                        cardExpiryMessage + 
                                        "</div>" +
                                        "<div id='" + storedCardOfferID + "'></div>" +
                                        "</li>");
                    } else if (each['card_type'] == "CREDIT") {
                        ccCount++;
                        $("#savedCreditCards").append(
                                "<li class='clearfix' id='" + each['card_token'] + "'>" +
                                "<div class='remove'><a href='javascript:void(0)' data-saved-card-token='"+ each['card_token'] +"' class='icon-trash' >Delete this card</a></div>" +        
                                "<input type='radio' class='option' name='creditCardSelection' onchange='ccCardSelection(this)' cardBrand='" + each['card_brand'] + "'>" +
                                        "<div class='info'>" +
                                        "<span class='"+ each['card_brand'] +"'></span>" +
                                        "<p class='name'>" + cardName + "</p>" +
                                        "<p class='type'>" + each['card_number'] + ", " + each['card_issuer'] + "</p>" +
                                        "</div>" +
                                        "<input type='hidden' id='" + storedCardNumberID + "' value='" + each['card_is_in'] + "**********'/>" +
                                        "<div class='offer-check'>" +      
                                        cardExpiryMessage + 
                                        "</div>" +
                                        "<div id='" + storedCardOfferID + "'></div>" +
                                        "</li>");
                    }
                }

                if (dbCount > 0) {
                    $("#saved-cards-debit").slideDown(200);
                    $("#new-debit-card").slideDown(200);
                    $("#seamless-debit-payment").slideUp(200);
                    //$("input[name=debitCardSelection]:first").trigger("click");
                } else {
                    $("#saved-cards-debit").slideUp(200);
                    $("#new-debit-card").slideUp(200);
                    $("#new-debit-card input").attr('checked', 'checked');
                    $("#seamless-debit-payment").slideDown(200);
                }

                if (ccCount > 0) {
                    $("#saved-cards-credit").slideDown(200);
                    $("#new-credit-card").slideDown(200);
                    //$("#new-credit-card input").attr('checked', 'checked');
                    $("#seamless-credit-payment").slideUp(200);
                    //$("input[name=creditCardSelection]:first").trigger("click");
                } else {
                    $("#saved-cards-credit").slideUp(200);
                    $("#new-credit-card").slideUp(200);
                    $("#seamless-credit-payment").slideDown(200);
                }

                <%-- By default debit card tab is open, hence check as to whether a user has any stored debit cards and if
                 so check the first one be default--%>
                checkStoredDebitCard();
            }
    	});
    }

    /* Page initialization */
    function pageInit() {
    	// disable caching for Internet Explorer
    	$.ajaxSetup({
    	    cache: false
    	});
    	populateSavedCards();
    }

    // show/hide CVV field based on selection of saved debit card
    function dbCardSelection(selection) {
        var cvv_box = "<p class='saved-card-cvv'><label>Enter CVV2/CVC2 number: </label><input type='password' maxlength='3'  onkeypress='return isNumberKey(event,3,id)' ></p>"
        var cvv_box_optional = "<p class='saved-card-cvv optional'><label>Enter CVV2/CVC2 number (Optional)&nbsp;&nbsp;&nbsp;</label><input type='password' maxlength='3'  onkeypress='return isNumberKey(event,3,id)' ></p>"
        $(".saved-card-cvv").remove();
        if ($(selection).is(':checked')) {

            if (selection.value == 'new') { // check for new card selection

                $("#seamless-debit-payment").slideDown(200);
            } else {

                var debit_card_brand = selection.getAttribute("cardbrand");    // since its a DOM object and not a jquery object
                $("#seamless-debit-payment").slideUp(200);
                if (debit_card_brand == "MAESTRO") {

                    var debit_card_issuer = selection.getAttribute("cardissuer");
                    if (debit_card_issuer != "SBI") {

                        $(selection).parent("li").find("div.info").append(cvv_box_optional);
                    }
                } else {

                    $(selection).parent("li").find("div.info").append(cvv_box);
                }

            }
        }
    }

    function ccCardSelection(selection) {
        var cvv_box = "<p class='saved-card-cvv'><label>Enter CVV2/CVC2 number: </label><input type='password' maxlength='3' onkeypress='return isNumberKey(event,3,id)' ></p>"
        if ($(selection).is(':checked')) {
            if (selection.value == 'new') { // check for new card selection
                $("#seamless-credit-payment").slideDown(200);
                $(".saved-card-cvv").remove();
            } else {
                $("#seamless-credit-payment").slideUp(200);
                $(".saved-card-cvv").remove();
                $(selection).parent("li").find("div.info").append(cvv_box)
            }
        }
    }


    $(".save-card").click(function(){
        if ($(this).find("input[type=checkbox]").is(':checked')) {

            $(this).find("p").slideDown(200);
        } else {

            $(this).find("p").slideUp(200);
        }
    });

    function refreshAmountToPay(lid) {
    	var url='/app/total_amount_to_pay.htm?lid='+lid;
    	$.ajax({
    	    url: url,
    	    dataType: "json",
    	    cache: false,
    	    timeout:1800000,
    	    success: function (responseText) {
                if (responseText.status == 'success') {
                    //var responseObject = responseText.data;
        	    	//$(".total-amount-to-pay").html(responseObject.payableTotalAmount);
        	    	$(".total-amount-to-pay").html(responseText.payableTotalAmount);
        	    	//if($.trim(responseObject.payableTotalAmount) == '0.0' || $.trim(responseObject.payableTotalAmount) == '0.00') 
        	    	if($.trim(responseText.payableTotalAmount) == '0.0' || $.trim(responseText.payableTotalAmount) == '0.00') {
                    	$("#zeroPaymentOptionsBlock").show();
                    	$("#nonZeroPaymentOptionsBlock").hide();
                    } else {
                    	$("#zeroPaymentOptionsBlock").hide();
                    	$("#nonZeroPaymentOptionsBlock").show();
                    	//walletEligibility = responseObject.walletEligibility;
                    	walletEligibility = responseText.walletEligibility;
            	    	
            	    	if (walletEligibility.eligibleForPartialPayment == 'true' || walletEligibility.eligibleForFullWalletPayment == 'true') {
                	    	updateWalletDisplay(responseText.paymentBreakup, walletEligibility);
            	    	}
                    }
                } else if (responseText.status == 'sessionExpired') {
                    alert('Session expired. Please start over');
                } else {
                    alert('Technical error occurred. Please try again after some time.');
                }
    	    },
    	    error:function (xhr,status,error){
    	      $(".total-amount-to-pay").html("Error occurred "); 
    	    }
    	});
    }

    function refreshOutstandingAmountAlone() {
        var lid = "<%=lookupid%>";
    	var url='/app/total_amount_to_pay.htm?lid='+lid;
    	$.ajax({
    	    url: url,
    	    dataType: "json",
    	    cache: false,
    	    timeout:1800000,
    	    success: function (responseText) {
                if (responseText.status == 'success') {
                    //var responseObject = responseText.data;
        	    	//walletEligibility = responseObject.walletEligibility;
        	    	walletEligibility = responseText.walletEligibility;
        	    	if (walletEligibility.eligibleForPartialPayment == 'true') {
        	    		//$('.total-amount-to-pay').html(responseObject.paymentBreakup.payablePGAmount.amount);
        	    		$('.total-amount-to-pay').html(responseText.payablePGAmount);
        	    	}
                } else if (responseText.status == 'sessionExpired') {
                    alert('Session expired. Please start over');
                } else {
                    alert('Technical error occurred. Please try again after some time.');
                }
    	    },
    	    error:function (xhr,status,error){
    	      $(".total-amount-to-pay").html("Error occurred "); 
    	    }
    	});
    }

    function refreshOutstandingAmount() {
            var lid = "<%=lookupid%>";
        	var url='/app/total_amount_to_pay.htm?lid='+lid;
        	$.ajax({
        	    url: url,
        	    dataType: "json",
        	    cache: false,
        	    timeout:1800000,
        	    success: function (responseText) {
                    if (responseText.status == 'success') {
                        //var responseObject = responseText.data;
            	    	//walletEligibility = responseObject.walletEligibility;
            	    	walletEligibility = responseText.walletEligibility;

            	    	if (!$('#usePartialWalletPaymentCheck').is(':checked')) {
            	    		if (walletEligibility.eligibleForPartialPayment == 'true' || walletEligibility.eligibleForFullWalletPayment == 'true') {
            	    			//$('.total-amount-to-pay').html(responseObject.paymentBreakup.payableTotalAmount.amount);
            	    			$('.total-amount-to-pay').html(responseText.payableTotalAmount);
            	    			$('#outstandingPayAmountArea').hide();
            	    			$('#walletPayableAmountArea').hide();
            	    		}
            	    	} else {
            	    		if (walletEligibility.eligibleForPartialPayment == 'true') {
            	    			//$('.total-amount-to-pay').html(responseObject.paymentBreakup.payablePGAmount.amount);
            	    			$('.total-amount-to-pay').html(responseText.payablePGAmount);
            	    			$('#outstandingPayAmountArea').show();
            	    			$('#walletPayableAmountArea').show();
            	    		} else if (walletEligibility.eligibleForFullWalletPayment == 'true') {
            	    			//$('.total-amount-to-pay').html(responseObject.paymentBreakup.payableTotalAmount.amount);
            	    			$('.total-amount-to-pay').html(responseText.payableTotalAmount);
            	    		}
            	    	}
                    } else if (responseText.status == 'sessionExpired') {
                        alert('Session expired. Please start over');
                    } else {
                        alert('Technical error occurred. Please try again after some time.');
                    }
        	    },
        	    error:function (xhr,status,error){
        	      $(".total-amount-to-pay").html("Error occurred "); 
        	    }
        	});
    }
    
    function handleOtherPayOptionShow() {
        $('#allPaymentOpt').find('.selectPayment').slideDown(300);
    	//$('#allPaymentOpt').show().slideDown(200);
    	$('#walletPayableAmountArea').hide();
    }

</script>

<input type="hidden" id="binOfferId" value="0" />
<script type="text/javascript">
function checkBinOffer(cardId, alertId) {
	if($('#usePartialWalletPaymentCheck').is(':checked'))
	{
		alert("Err, bank card offers are not applicable when you use Balance. P.S.: Sorry!");
		return false;
	}
	var card = $("#"+cardId).val();
	var url="/app/binoffer/check.htm?card="+card+"&lid=<%=lookupid%>";
    $.ajax({
        url: url,
        type: "GET",
        dataType: "JSON",
        cache: false,
        timeout:1800000,
        success: function (responseText) {
           if(responseText.STATUS == 'failure')	{
        	   $("#"+alertId).addClass("failureMsgSmall status-message").html(responseText.message);
           } else if(responseText.STATUS == 'success') {
        	   $("#"+alertId).addClass("successMsgSmall status-message").html(responseText.message);
        	   $("#binOfferId").val(responseText.binOfferId);
           } else {
        	   $("#"+alertId).addClass("failureMsgSmall status-message").html("Something is broken. Please try later");
           }
           
        },
        error:function (xhr,status,error){
        	 $("#"+alertId).addClass("failureMsgSmall status-message").html("System error. Please try later");
        }
    })
}
</script>
 <%
     }
 %>
 
<span style="display: none;" id="paymentwaitPageid"></span>
<input type="hidden" id="promoEntityStore" value="" />

<c:if test="${pgOpenNewWindow}">
    <div id='childWindowProxy' style='display: none;'></div>
</c:if>
 
 <script type="text/javascript">
/*
 * Load all payment options
 */
 function loadAllPaymentOptions() {
	document.location.href = "/app/checkout.htm?lid=<%=lookupid%>&pg=ALL";
 }
 
 /* Make payment method */
 function doPayment (paymenttypeid) {
     var isValid = false;
     var paymentoption = paymenttypeid+'-paymentoption';
     var paymentOptionValue;
     
     if(paymenttypeid == 2) { // debit card option
         var current_debit_radio = $("input:radio[name=debitCardSelection]:checked");
         isValid = $("#Debit-Card").validationEngine('validate');
         if(!isValid) return false;
         if (current_debit_radio.val() == 'new') {
             paymentOptionValue = $("#seamless-debit").val();
         } else {
             paymentOptionValue = current_debit_radio.attr("paymentCode");
         }
     } else if(paymenttypeid == 1) { // credit card option
         isValid = $("#Credit-Card").validationEngine('validate');
         if(!isValid) return false;
         var current_credit_radio = $("input:radio[name=creditCardSelection]:checked");
     } else if(paymenttypeid == 8) { // ZeroPay option
         isValid =true;
         paymentOptionValue = '<%=PaymentConstants.ZEROPAY_PAYMENT_OPTION%>';
     } else { // other payment options
         var paymentOptionRadioId = 'input:radio[id='+paymentoption+']:checked';
         var payment_type = $(paymentOptionRadioId).val();
         if (typeof payment_type == 'undefined' || payment_type =='' ) {
             paymentOptionValue = $('#paymentoptionNetBank').val();
             if (typeof paymentOptionValue == 'undefined' || paymentOptionValue == '') {
                 alert("Wait! You haven't selected your bank. Do that first and click 'OK'.");
                 return;
             }
         } else {
             paymentOptionValue = payment_type;
         }
     }
     try {
         var formData = createPaymentSubmitData(paymenttypeid, paymentOptionValue);
     } catch (error) {
         alert(error);
         return false;
     }

     var formData = createPaymentSubmitData(paymenttypeid, paymentOptionValue);
     if(formData == null) return false;
     
     //Tracking on Make Payment button click, doing only if there are no validation errors
	 fcTrack({eventName: 'nMakePayment'});
   
     <c:if test="${! pgOpenNewWindow}">
             $.blockUI({ css: {
                 border: 'none',
                 padding: '15px',
                 backgroundColor: '#000',
                 opacity: .5,
                 color: '#fff'
             } });
     </c:if>
    
     <c:choose>
         <c:when test="${pgOpenNewWindow}">
	            if(paymentOptionValue == '<%=PaymentConstants.ZEROPAY_PAYMENT_OPTION%>') {
	            	submitPaymentRequest(formData);
	            } else {
	            	var url = '<%=ProductPaymentController.NEW_PAYMENT_WINDOW%>'; 
	            	window.open(url, '', 'width=1024,height=768,scrollbars=yes'); // open in new window
		            <%-- This is done so that the child window can access this form data and submit it. We do not want to pass this data
		           		as GET parameters when we open the new window, bcos card details are being passed here.
		            --%>
	                window.paymentPostParams = formData;
	            }
         </c:when>
         <c:otherwise>
             submitPaymentRequest(formData);
         </c:otherwise>
     </c:choose>

 }
 

 function createPaymentSubmitData(paymenttypeid, paymentOptionValue) {
     var payment_type_id = paymenttypeid;
     var paymenttype = '#'+paymenttypeid+'-paymenttype';
     var productId = '#'+paymenttypeid+'-productId';
     var affiliateid = '#'+paymenttypeid+'-affiliateid';
     var lookupID = '#'+paymenttypeid+'-lookupID';
     var cartId = '#'+paymenttypeid+'-cartId';
     var couponCode = $('#couponCodeId').val();
     var offerType = 0;
     var productType = "<%=productType%>";
     var ccNum = $('#ccNum').val();
     var ccHolderName = $('#ccHolderName').val();
     var ccExpMon = $('#ccExpMon').val();
     var ccExpYear = $('#ccExpYear').val();
     var ccCvv = $('#ccCvv').val();
     var dbNum = $('#dbNum').val();
     var dbHolderName = $('#dbHolderName').val();
     var dbExpMon = $('#dbExpMon').val();
     var dbExpYear = $('#dbExpYear').val();
     var dbCardSaveBool = false;
     var ccCardSaveBool = false;
     var usePartialWalletPayment = $('#usePartialWalletPaymentCheck').is(':checked');

     if (payment_type_id == 2) {
         var dbCvv = readDbCvv();
         dbCardSaveBool = $('#dbCardSaveBool').is(':checked');
         ccCardSaveBool = false;
     } else {
         var dbCvv = '';
     }
     var dbCardToken = $("input:radio[name=debitCardSelection]:checked").parent("li").attr("id");
     if (payment_type_id == 1) {
         var ccCvv = readCcCvv();
         dbCardSaveBool = false;
         ccCardSaveBool = $('#ccCardSaveBool').is(':checked');
     } else {
         var ccCvv = '';
     }
     var ccCardToken = $("input:radio[name=creditCardSelection]:checked").parent("li").attr("id");

     var dbCardSaveName = $('#dbCardSaveName').val();
     var ccCardSaveName = $('#ccCardSaveName').val();

     if ($("#seamless-debit").val() == "DCME" && $("#dbExpYear").val() == "") {
         dbExpMon = "11";
         dbExpYear = "2049";
     }
     if ($("#seamless-debit").val() == "DSME") {
         dbExpMon = "12";
         dbExpYear = "2049";
     }

     if(couponCode == "") {
         couponCode = $('#ccdbCardNumber').val();
         offerType = $('#ccdbCardNumberOfferType').val();
     }
     if(offerType >=1 && couponCode != "") { <%-- means he is availing offer --%>
         var cardNumber = "";
         if(ccNum != "") { cardNumber = ccNum; }
         if(dbNum != "") { cardNumber = dbNum; }
         if(couponCode != cardNumber) {
             var isOk = confirm("You must use the same card entered above to receive the special discount. If you do not, you will be charged the full amount. Click cancel and use the above-entered card for payment.");
             if(!isOk) {
                 return null;
             } else {
                 applyBinOffer('#addBinOffer', '#removeBinOffer', '#promoCodePrice', false);
             }
             var dbHolderName = $('#dbHolderName').val();
             var dbExpMon = $('#dbExpMon').val();
             var dbExpYear = $('#dbExpYear').val();
         }
         couponCode = cardNumber;
     }

     if (!(payment_type_id == 1 || payment_type_id == 2)) {
         if ($("#removeBinOffer").is(':visible')) {
             applyBinOffer('#addBinOffer', '#removeBinOffer', '#promoCodePrice', false);
         }
     }

     var unCheckedStoreCard = false;

     <%-- If the user has either unchecked the credit or debit card store option, then send the unCheckedStorecard variable as true. Unless the
      user tinkers around with these, both of them should be true as by default both of them are set as true--%>
     if (!($('#dbCardSaveBool').is(":checked") && $('#ccCardSaveBool').is(":checked"))) {
         unCheckedStoreCard = true;
     }

     return {
         'paymenttype': $(paymenttype).val(),
         'productId':$(productId).val(),
         'affiliateid':$(affiliateid).val(),
         'lookupID':$(lookupID).val(),
         'cartId':$(cartId).val(),
         'paymentoption':paymentOptionValue,
         'pyop':paymentOptionValue,
         'couponCode':couponCode,
         'offerType':offerType,
         'binOfferId':$("#binOfferId").val(),
         'productType':productType,
         'ccHolderName':ccHolderName,
         'ccNum':ccNum,
         'ccExpMon':ccExpMon,
         'ccExpYear':ccExpYear,
         'ccCvv':ccCvv,
         'ccCardToken':ccCardToken,
         'ccCardSaveBool':ccCardSaveBool,
         'ccCardSaveName':ccCardSaveName,
         'dbHolderName':dbHolderName,
         'dbNum':dbNum,
         'dbExpMon':dbExpMon,
         'dbExpYear':dbExpYear,
         'dbCvv':dbCvv,
         'dbCardToken':dbCardToken,
         'dbCardSaveBool':dbCardSaveBool,
         'dbCardSaveName':dbCardSaveName,
         'unCheckedStoreCard': unCheckedStoreCard,
         'usePartialWalletPayment':usePartialWalletPayment,
         '<%=CSRFTokenManager.CSRF_REQUEST_IDENTIFIER%>': '${mt:csrfToken()}'
     };
 }

 function submitPaymentRequest(formData) {
     var url = '<%=com.freecharge.web.controller.ProductPaymentController.DO_PAYMENT_ACTION%>';
     $.ajax({
         url: url,
         type: "POST",
         data: formData,
         dataType: "html",
         cache: false,
         timeout:1800000,
         success: function (responseText) {
             var code = responseText.charAt(0);
             if(code == 1){
                 responseText = responseText.substring(1);
                 $(".mainContainer").html(responseText);
                 $.unblockUI();
             }else{
                 $("#paymentwaitPageid").html(responseText);
                 var isloggedIn = $("#isloggedin").val();
                 var hostprefix = $("#hostPrefix").val();
                 if(isloggedIn != 'true'){
                     alert("Your session is expired,Please login again.");
                     window.location.href = hostprefix;
                     return false;
                 }
                 document.paymentRequest.submit();
             }
         },
         error:function (e){
             $.unblockUI();
             alert("Technical error occurred. Please try after some time or contact our customer care.");
         }
     });
 }
 
 $(function(){
	 $(".bin-offer-tnc").fancybox({type: 'ajax'});
	 <c:if test="${containsFreefund}">
	      fcTrack({eventName: 'RedeemNowSuccess'});
	 </c:if>
 });
</script>
	
</div>
<div class="four columns">
    <%-- <%@include file="../binoffer/binoffer_list.jsp"%> --%>

    <div class="security-information">
        <div class="geotrust">
            <!-- GeoTrust QuickSSL [tm] Smart  Icon tag. Do not edit. -->
            <script language="javascript" type="text/javascript" src="//smarticon.geotrust.com/si.js"></script>
            <!-- end  GeoTrust Smart Icon tag -->
        </div>

        <div class="pci-compliant-seal">
            <a href="javascript:CCPopUp('http://seal.controlcase.com/', 4252874532);">
            <img src='${mt:keyValue("imgprefix1")}/images/PCI_logo.gif' oncontextmenu='return false;' border='0'></a></br>
        </div> 
        
        <div class="symantecSSL-logo" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications." >
        	<script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.freecharge.in&amp;size=M&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script>
        </div>

        <div class="other">
            <img src="${mt:keyValue("imgprefix1")}/images/icons/security-other.png" alt="Verified by Visa, MasterCard Secure" />
        </div>

    </div>
</div>
