<nav>
    <ul>
        <li><a href="/app/aboutus.htm" >About us</a></li>
        <li><a href="/app/contactus.htm" class="active" >Contact us</a></li>
        <!--<li><a href="/app/faq.htm">FAQ</a></li>-->
        <li><a href="/app/sitemap.htm">Sitemap</a></li>
        <li><a href="http://support.freecharge.in" target="_blank">Customer support</a></li>
        <li><a href="/app/termsandconditions.htm">Terms &amp; conditions</a></li>
        <!--<li><a href="/app/privacypolicy.htm">Privacy policy</a></li>
        <li><a href="/security/disclosure.htm">Security Policy</a></li><br>-->
    </ul>
</nav>
<div class="row">
    <div class="col-md-12">
        <section class="static-content">
         <h3>Contact Us</h3>
         <h3>Customer Grievance Redressal Framework </h3>
         <p>Last Updated on January 17,2018</p>
         <p>
          The Customer Grievance Redressal Framework defines the operating guidelines for handling customer queries and complaints at Freecharge and Axis Bank.
         </p>
          <h4>Customer Service Support:-</h4>
          <h4>Level 1</h4>
          <p>(a) Call Us <br/>We have a Contact Center set up where a customer can call 080-33037133 (standard calling charges apply) to block their account or report any suspicious activity/transaction, the support is available 365 days in a year from 7 am till 11 pm. </p>
          <p>For Customer Complaints where ticket number is generated the period of resolution is 7 Business Days.</p>

          <p>(b) Mail Us<br/>Customer can also email their queries, suggestions, feedback or complaints at <a href="mailto:care@freecharge.com"> care@freecharge.com </a> </p>

          <h4>Level 2</h4>
          <h4>Grievance Redressal</h4>
          <p>We aims to resolve all complaints at the first point of contact. In case customer doesn’t get a satisfactory resolution to their query/complaint at Level 1 they can get in touch with the  Grievance Officer with ticket number and details of grievance.</p>

          <p>(a) Mail Freecharge<br/>
            Email at <a href="mailto:grievanceofficer@freecharge.com"> grievanceofficer@freecharge.com </a><br/>
            Working Hours: 9:30 AM to 6:30 PM (except National Holiday) <br/>
            Turn-around-Time: 10 Business days from the Date of Receipt.<br/>
            Postal at Freecharge Payment Technologies Private Limited, DLF Cyber Green, 16th Floor, Tower-C,DLF Cyber City, DLF Phase-3, Gurugram-Haryana-122002, India <br/>
            Registered Office- 68, Okhla Industrial Estate, Phase-3, New Delhi- 110020, India CIN- U74140DL2015PTC275419 <br/>
          </p>
          <h4>Level 3</h4>
          <h4>Axis Bank Nodal Officer</h4>
          <p>In case customer complaint remains unresolved, within 30 days of registering the complaint then customer can approach the Nodal Office of Axis Bank along with the ticket number shared by Freecharge.</p>
          <p>(a) Mail Axis Bank <br/>Email at <a href="mailto:freecharge.nodal@axisbank.com"> freecharge.nodal@axisbank.com</a>
            <br/>
            Working Hours: 9:30 AM to 5:30 PM (Monday-Friday; except bank holidays) <br/>
            Turn-around-Time: 10 Business days from the Date of Receipt.<br/>
            Postal at Nodal Officer, Axis Bank Ltd, NPC1, 5th Floor, "Gigaplex", Plot No I.T.5, MIDC, Airoli Knowledge Park, Airoli , Navi Mumbai-400708.<br/>
          </p>
          <h4>Final Redressal and Closure of Grievance</h4>
          <p>Final Redressal and Closure of Grievance:</p>
          <p>1. Where the complainant has communicated his acceptance of the company’s decision on redressal of grievance communicated by grievance redressal; or</p>
          <p>
           2. Where the complainant has not communicated his acceptance of the company’s decision, within 7 days from the date of communication of decision by Level 1 or Level 2 or Level 3, as the case may be.
         </p>
        </section>
    </div>
</div>
