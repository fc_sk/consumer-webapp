<%@page import="com.freecharge.payment.dao.PgMISRequestDataDAO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page import="com.freecharge.web.webdo.PaymentGatewayManagementWebDO" %>
<%@ page import="com.freecharge.web.controller.PaymentMisController" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<style>

.error {
    color: #ff0000;
    font-style: italic;
}

.txt1 {
    border-color: #AABBFF;
    border: 1px solid;
    font: bold 84% 'trebuchet ms', helvetica, sans-serif;
}

.theader {
    bgcolor: "FFCCFF";
}

li.leftNavLink {
    list-style-type: none;
    padding: 2px;
}

ul.leftNavPanel {
    margin: 0px;
    paddding-left: 20px;
}

th {
    background-color: gray;
    color: white;
    font-weight: bold;
    padding: 5px;
    text-align: left;
}

td {
    padding-left: 6px;
    padding-right: 10px;
    padding-top: 3px;
    padding-bottom: 3px;

}

td.entityID {
    width: 30px;
    text-align: right;
}

tr.color0 {
    background-color: #F0F0F0;
}

tr.color1 {
    background-color: #D0D0D0;
}


div.leftMenu {
    padding-right: 60px;
    float: left;
}

div.displayList {
    float: left;
    width: 60%;
}

p.addLink {
    padding-left: 10px;
    float: left;
}

input.txt, select.txt {
    padding: 0px;
    font-size: 10px;
    width: 100px;
}

p.addHeader {
    font-size: 15px;

}


.floatRight {
    float: right;
}

</style>


<script src="http://jquery-ui.googlecode.com/svn/tags/latest/external/jquery.bgiframe-2.1.2.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>

<div class="pg-setting">
    <h1><spring:message code="payment.gateway.management" text="#payment.gateway.management#"/></h1>
    <div id="msg"></div>
    <div>
        <p><spring:message code="number.records.found" text="#number.records.found#"/>: ${fn:length(data)}</p>

        <form:form name="myForm" modelAttribute="misRequest" method="POST" action="<%=PaymentMisController.MANAGE_PG_LINK%>" >
            <div class="filter">
                <table>
                    <tr>
                        <td>
                            <label><spring:message code="product" text="#product#"/></label>
                            <form:select path="product" cssClass="txt">
                                <form:option selected="true" value="" label="ALL"/>
                                <form:option value="1" label="Mobile"/>
                                <form:option value="2" label="DTH"/>
                                <form:option value="3" label="Datacard"/>
                            </form:select>
                        </td>

                        <td>
                            <label><spring:message code="bank.groups" text="#bank.groups#"/></label>
                            <form:select path="bankGrp" cssClass="txt">
                                <form:option selected="true" value="" label="ALL"/>
                                <c:forEach items="${pgBanksGroup}" var="mapEntry">
                                    <form:option value="${mapEntry.key}" label="${mapEntry.value}"/>
                                </c:forEach>
                            </form:select>
                        </td>

                        <td>
                            <label><spring:message code="payment.gateway" text="#payment.gateway#"/></label>
                            <form:select path="paymentGateway" cssClass="txt">
                                <form:option value="" label="ALL" selected="true"/>
                                <c:forEach items="${pgGatewayMaster}" var="pgEntry">
                                    <form:option value="${pgEntry.key}" label="${pgEntry.value}"/>
                                </c:forEach>
                            </form:select>
                        </td>
                        <td style="vertical-align: bottom;">
                            <input type="submit" value="Run Report" />
                        </td>
                    </tr>
                </table>
            </div>
        </form:form>
        <div>
            <table class="zebra">
                <thead>
                    <tr>
                        <th><spring:message code="id" text="#id#"/></th>
                        <th><spring:message code="product" text="#product#"/></th>
                        <th><spring:message code="bank.group.name" text="#bank.group.name#"/></th>
                        <th><spring:message code="payment.gateway" text="#payment.gateway#"/></th>
                        <th><spring:message code="action" text="#action#"/></th>
                    </tr>
                </thead>
                <%-- <c:forEach items="${misdata.displayRecords}"  var="misDefault"  varStatus="loopStatus"> --%>
                <c:forEach items="${data}" var="row" varStatus="loopStatus">
                    <tr>
                        <c:forEach items="${row}" var="entry">
                            <td id = "${entry.key}_${relid}" align="left">${entry.value}</td>
                            <c:if test="${entry.key eq 'id'}">
                                <c:set var="relid">${entry.value}</c:set>
                            </c:if>

                            <c:if test="${entry.key eq 'prname'}">
                                <c:set var="product">${entry.value}</c:set>
                            </c:if>

                            <c:if test="${entry.key eq 'bkname'}">
                                <c:set var="bank">${entry.value}</c:set>
                            </c:if>

                            <c:if test="${entry.key eq 'pgname'}">
                                <c:set var="oldValue">${entry.value}</c:set>
                            </c:if>
                        </c:forEach>

                        <td align="right">
                            <select id="pgrel_${relid}" name="pgupdaterel" class="txt" onchange="changepg(${relid},this.value,'${product}','${bank}','${oldValue}',this);">
                                <option selected="true" value="ALL" >---</option>
                                <c:forEach items="${pgGatewayMaster}" var="pgEntry">
                                    <option value="${pgEntry.key}" label="${pgEntry.value}">${pgEntry.value}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>


<script type="text/javascript">

    function changepg(id, pg, product, bank, oldValue, newValue) {
        var option = newValue.options[newValue.selectedIndex];
        var url = "<%=PaymentMisController.UPDATE_PG_LINK%>"; // the script where you handle the form input.
        var param = "rid=" + id + "&gid=" + pg + "&prd=" + product + "&bank=" + bank + "&oldv=" + oldValue + "&newv=" + $(option).attr("label");

        $.ajax({
            type : "GET",
            url : url,
            data :param,
            dataType: "json",
            success : function(response) {
                $("#pgname_" + id).html(response.gateway);
                return true;
            },
            error : function(e) {
                return true;
            }
        });
        return false;
    }
    ;

</script>