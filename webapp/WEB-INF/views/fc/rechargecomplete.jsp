<%@ page import="com.freecharge.common.util.FCConstants"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@page pageEncoding="UTF-8"%>
<%@page contentType="text/html;charset=UTF-8"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>

<c:forEach items="${resultMap}" var="mapEntry">
	<c:set var="requestMap"
		value="${requestMap}&${mapEntry.key}=${mapEntry.value}"></c:set>
	<c:if test="${mapEntry.key == 'rcmobilenumber'}">
		<c:set var="rcmobilenumber" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'Amount'}">
		<c:set var="amount" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'suc'}">
		<c:set var="issuccess" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'shippingaddress'}">
		<c:set var="shippingaddress" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customeremailid'}">
		<c:set var="customeremailid" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'orderId'}">
		<c:set var="orderId" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'Paymentstatus'}">
		<c:set var="Paymentstatus" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customername'}">
		<c:set var="customername" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customeraddress'}">
		<c:set var="customeraddress" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customerarea'}">
		<c:set var="customerarea" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customerlandmark'}">
		<c:set var="customerlandmark" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customercity'}">
		<c:set var="customercity" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customerstate'}">
		<c:set var="customerstate" value="${mapEntry.value}"></c:set>
	</c:if>
	<c:if test="${mapEntry.key == 'customerpincode'}">
		<c:set var="customerpincode" value="${mapEntry.value}"></c:set>
	</c:if>
</c:forEach>

<div class="after-payment">
    <c:choose>
        <c:when test="${issuccess eq true and !empty issuccess}">
            <div class="fc-card">
                <div class="status recharge-process">
                    <h2>Your payment was successful!</h2>
                    <p>
                        Your recharge is <span class="color1"><strong>processing</strong></span>.
                    </p>
                    <div class="recharge-loader">
                        <img src="${mt:keyValue("imgprefix1")}/images/progress_bar.gif"  alt="" width="220"
                            height="19" />
                    </div>
                    <%--<p class="color2">The details of your recharge are given below.</p>--%>
                </div>
                <div class="recharge-info clearfix">
                    <div class="operator-logo">
                        <img src="${mt:keyValue("imgprefix1")}/${operatorImgUrl}" alt="${operator}">
                    </div>
                    <div class="service-number">
                        <div>
                            <c:out value="${rcmobilenumber}" />
                        </div>
                        <p>${product} | ${operator} | ${circle}</p>
                    </div>
                    <div class="amount">
                        Rs.
                        <c:out value="${amount}" />
                    </div>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="center recharge-status-loader">
                <img src="${mt:keyValue("imgprefix1")}/images/ajax-processing.gif" alt="Redirection Under Process!! Please Wait"
                    width="150" height="130">
            </div>
        </c:otherwise>
    </c:choose>
</div>

<script type="text/javascript">
	function redirectParent() {
		window.parent.location.href = "${redirectUrl}?orderId=${orderId}";
	}

	$(document).ready(function() {
		setTimeout(redirectParent, 10000);
	});
</script>
	

