<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="fc-card" id="recharge-status-container">
    <div class="status recharge-under-process">
        <h2>Recharge Underway</h2>
        <p>Patience! </p>
        <p>Sit back, your recharge is happening. </p>
        <p>We&#39;ll notify you on the recharge status shortly.</p>
        <br>
        <p>In the event that we are not able to process your recharge, you can rest assured that your money isn&#39;t lost. The amount will be deposited in your <span class="text-color-1"><strong><spring:message code="fc.money" text="#fc.money#"/></strong></span><i class="icon-info" id="fcb-recharge-under-process-tip"></i>. </p>
        <div id="fcb-recharge-under-process-tooltip" style="display: none">
            <h6>What are Credits?</h6>
            <br>
            <p>If for some reason, your recharge is unsuccessful, we make a refund to your FreeCharge Credits. Your money is absolutely safe, secure and available to you for future transactions on <a href="/app/home.htm" target="_blank">FreeCharge.in</a>.What's more, you can also deposit cash in your Credits anytime to facilitate faster and easier recharging by clicking <a href="/app/mycredits.htm" target="_blank">here</a>.</p>
            <p><a href="/app/faq.htm" target="_blank">Know more</a></p>
        </div>
        <br>
        <p>Your Order ID for this transaction is - <strong>${rechargeInitiateWebDo.orderId}</strong>.</p>
    </div>
    <div class="recharge-info clearfix">
        <div class="operator-logo">
            <img src="${mt:keyValue("imgprefix1")}/${operatorImgUrl}" title="${operator}" alt="${operator}">
        </div>
        <div class="service-number">
            <div><c:out value="${rechargeInitiateWebDo.mobileNo}"/></div>
            <p>${product} | ${operator} | ${circle}</p>
        </div>
        <div class="amount">
            Rs. <c:out value="${rechargeInitiateWebDo.amount}"/>
        </div>
    </div>
</div>


<script type="text/javascript" >
    _gaq.push(['_trackEvent', 'Recharge Status', 'IN UnderProcess']);
</script>
