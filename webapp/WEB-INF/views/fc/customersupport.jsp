<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<!-- .mainContainer Starts  -->
<div class="mainContainer"  >
    <div class="pageWidth clearfix" >
        <!-- .innerVerList Starts  -->
        <div class="innerVerList lf">
            <ul class="innerUsrLink clearfix">
                <li><a href="/app/aboutus.htm" >About Us</a></li>
                <li><a href="/app/contactus.htm"  >Contact Us</a></li>
                <li><a href="/app/faq.htm">FAQ</a></li>
                <li><a href="/app/sitemap.htm">Sitemap</a></li>
                <li><a href="/app/feedback.htm" >Feedback</a></li>
                <li><a href="http://support.freecharge.in" target="_blank"  class="active" >Customer Support</a></li>
                 <li><a href="/security/disclosure.htm">Security Policy</a></li><br>
            </ul>
            
            <ul class="innerCstCreLink clearfix">
                <li><a href="/app/termsandconditions.htm">Terms &amp; Conditions</a></li>
                <li><a href="/app/privacypolicy.htm">Privacy Policy</a></li>
            </ul>
        </div>
        <!-- .innerVerList Ends  -->
        <!-- .innerContSec Starts  -->
        <div class="innerContSec">
            <h1>Customer Support</h1>
            
            <h3 class="blockTitleSml" >Does your query belongs to following domain?</h3>
            <p class="smlHeader clearfix upCase"  onclick="toggleBlockParent(this,'activeHeader' )" ><span class="plus lf" >&nbsp;</span><span class="lf" >Recharge failed but refund amount not received</span></p> <!-- /.smlTitle -->
            <div class="togglelblock_desc clearfix" style="display:none;" >
                <ul>
                    <li>Refund is automatically initiated from our systems on the same day.</li>
                    <li>Refund will reflect in your account  within 5-7 working days, depending  upon the time taken by the bank to process it. </li>
                </ul>
            </div> <!-- /.togglelblock_desc -->
            <p class="smlHeader clearfix upCase"  onclick="toggleBlockParent(this,'activeHeader' )"  >
                 <span class="plus lf" >&nbsp;</span><span class="lf" >Money deducted from your bank account but mobile not recharged yet</span>
            </p>  
            <div class="togglelblock_desc clearfix" style="display:none;" >
                <ul>
                    <li>Please check your balance again</li>
                    <li>Contact your service provider  customer care.</li>
                </ul>
            </div> <!-- /.togglelblock_desc -->
            <p class="smlHeader clearfix upCase"  onclick="toggleBlockParent(this,'activeHeader' )"  >
                 <span class="plus lf" >&nbsp;</span><span class="lf" >Have not received my FREE coupons yet </span>
            </p>  
            <div class="togglelblock_desc clearfix" style="display:none;" >
                <ul>
                    <li>Delivery of FREE coupons takes upto 7 working days</li>
                </ul>
            </div> <!-- /.togglelblock_desc -->
            <br>
            
            <h3 class="blockTitleSml" >If not, please write us!</h3>
            
            <div class="tab form">
                    <ul class="tabsTwo tabTitle clearfix">
                        <li><a href="#"  rel="0">New Complaint</a></li>
                        <li><a href="#"  rel="1" >Old Complaint Status</a></li>
                    </ul>
                    <div class="tabsTwo_content clearfix tabCont">
                         <div>
                            <p class="sectionHeader"  >  Fill in the details if you have never Raised complaint before </p>
                            <div class="formField clearfix" >
                                    <label class="lf" ><strong class="redClr" >*</strong>Full Name:</label>
                                    <div class="field lf" ><input type="text" maxlength="50" value="" size="25" class="input1 softNormText"  ></div>
                            </div> <!-- /.formField -->
                         
                           <div class="formField clearfix" >
                                    <label class="lf" ><strong class="redClr" >*</strong>Email Address:</label>
                                    <div class="field lf" ><input type="text" maxlength="40" value="" size="25" class="input1 softNormText" ></div>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" >
                                    <label class="lf" ><strong class="redClr" >*</strong>Contact Number:</label>
                                    <div class="field lf" ><input type="text" maxlength="10" value="" size="25" class="input1 softNormText" ></div>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" >
                                    <label class="lf" ><strong class="redClr" >*</strong>Help Topic:</label>
                                    <div class="field lf" >
                                        <select name="topicId" id="topicId" class="select1 softNormText" >
                                            <option selected="" value="1">Select One</option>
                                            <option value="2" >Recharge Issues</option>
                                            <option value="3" >Coupon Delivery Issues</option>
                                            <option value="4" >Refund Issues</option>
                                            <option value="5" >General Issues / Feedback</option>
                                        </select>
                                    </div>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" style="display:none" id="show_orderId" >
                                    <label class="lf" ><strong class="redClr" >*</strong>Order ID:</label>
                                    <div class="field lf" ><input type="text" maxlength="10" value="" size="10" class="input1 softNormText" ></div>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" style="display:none" id="show_subject"  >
                                    <label class="lf" ><strong class="redClr" >*</strong>Subject:</label>
                                    <div class="field lf" ><input type="text" maxlength="50" value="" size="25"  class="input1 softNormText" ></div>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" >
                                    <label class="lf" >Message:</label>
                                    <div class="field lf" ><textarea   rows="1" cols="1"  class="input1 softNormText"  ></textarea></div>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" >
                                    <label class="lf" >Attachment:</label>
                                    <div class="field lf" ><input type="file" name="attachment"></div>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" >
                                    <label class="lf" >Captcha Text:</label>
                                    <div class="field lf" >
                                        <img width="88" height="31" border="0" align="left" src="${mt:keyValue("imgprefix1")}/images/capcha.png">&nbsp;
                                        <span>&nbsp;&nbsp;<input type="text" value="" size="7" name="captcha" class="cmnTxt softNormText" ></span>
                                        <i> &nbsp; Enter the text shown on the image.</i>
                                    </div>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" >
                                    <label class="lf" >&nbsp;</label>
                                    <div class="field lf" >
                                                    	 
                                                    	<input type="button" value="Submit" class="regularButton lf">
                    									<input type="button" value="Reset" class="regularButton lf btnSpace">
                                    </div>
                            </div> <!-- /.formField -->
                        </div>
                        <div>
                            <p class="sectionHeader"  >  Get status of your previous complaint(s).  </p>
                            <div class="formField clearfix" >
                                    <label class="lf" ><strong class="redClr" >*</strong>Email:</label>
                                    <div class="field lf" ><input type="text" maxlength="50" value="" size="25" class="input1 softNormText"  ></div>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" >
                                    <label class="lf" >Complaint ID:</label>
                                    <div class="field lf" ><input type="text" maxlength="50" value="" size="25" class="input1 softNormText"  ></div>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" >
                                    <label class="lf" >OR</label>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" >
                                    <label class="lf" >Order ID:</label>
                                    <div class="field lf" ><input type="text" maxlength="50" value="" size="25" class="input1 softNormText"  ></div>
                            </div> <!-- /.formField -->
                            
                            <div class="formField clearfix" >
                                    <label class="lf" >&nbsp;</label>
                                    <div class="field lf" ><input type="button" value="Check Status" class="regularButton "></div>
                            </div> <!-- /.formField -->
                         </div>
                    </div><!-- /.tabsTwo_content -->
           </div> <!-- /.customerSupportFrm -->
            
        </div>
        <!-- .innerContSec Ends  -->
    </div>  
</div>
<!-- .mainContainer Ends  -->

<script type="text/javascript" >
$(document).ready(function(){
	// Customer support HelpTopic change function call
	$("#topicId").change(function(){
		switch( Number( $(this).val() ))
		{
			case 1:
				$("#show_orderId, #show_subject").hide();
				break;
		    case 5:
		   		$("#show_subject").fadeIn();
				$("#show_orderId").hide();
		   		break;
		   default:
		   		$("#show_orderId").fadeIn();
				$("#show_subject").hide();
		   		break;		
		}
	})
	
	// Need to add tageffect plugin to staticpageplugins.js
	$(".tabsTwo").tabEffect();
	
}); // document ready event ends  *******************************
</script>

