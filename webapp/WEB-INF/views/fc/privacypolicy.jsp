        <nav>
            <ul>
                <li><a href="/app/aboutus.htm">About us</a></li>
                <li><a href="/app/contactus.htm"  >Contact us</a></li>
                <li><a href="/app/faq.htm">FAQ</a></li>
                <li><a href="/app/sitemap.htm">Sitemap</a></li>
                <%--<li><a href="/app/feedback.htm" >Feedback</a></li>--%>
                <li><a href="http://support.freecharge.in" target="_blank" >Customer support</a></li>
                <li><a href="/app/termsandconditions.htm">Terms &amp; conditions</a></li>
                <li><a href="/app/privacypolicy.htm" class="active" >Privacy policy</a></li>
                 <li><a href="/security/disclosure.htm">Security Policy</a></li><br>
            </ul>
        </nav>

<div class="row">
    <div class="col-md-12">

        <section class="static-content">
            <h1>Privacy Policy</h1>
            <p>We are committed towards protecting your privacy. Authorized employees within the company on a need to know basis only use any information collected from individual customers. We constantly review our systems and data to ensure the best possible data security to our customers. In case of any failures, we will investigate all actions with a view to prosecuting and/or taking civil proceedings to recover damages against those responsible.</p>

            <p>We will not sell, share or rent your personal information to any third party or use your e-mail address for unsolicited mail unless explicitly mentioned. Any emails sent by this Company will only be in connection with the provision of agreed services and products.</p>

            <h2>Log Files</h2>
            <p>We use IP addresses to analyse trends, administer the site, track usage patterns, and gather broad demographic information for aggregate use. IP addresses are not linked to personally identifiable information. Additionally, for systems administration, detecting usage patterns and troubleshooting purposes, our web servers automatically log standard access information including browser type, access times/open mail, URL requested, and referral URL. This information is not shared with third parties and is used only within this Company on a need-to-know basis. Any individually identifiable information related to this data will never be used in any way different to that stated above without your explicit permission.</p>

            <h2>Cookies</h2>
            <p>Like most interactive web sites this Company's website [or ISP] uses cookies to enable us to retrieve user details for each visit. Cookies are used in some elements of our site to enable the functionality of that element and ease of use for for the user. Some of our affiliate partners may also use cookies.</p>

            <h2>Links to this website</h2>
            <p>You may not create a link to any page of this website without our prior written consent. If you do create a link to a page of this website you do so at your own risk and the exclusions and limitations set out above will apply to your use of this website by linking to it.</p>

            <h2>Links from this website</h2>
            <p>We do not monitor or review the content of other party's websites which are linked to from this website. Opinions expressed or materials appearing on such websites are not necessarily shared or endorsed by us and should not be regarded as the publisher of such opinions or material. Please be aware that we are not responsible for the privacy practices, or content, of these sites. We encourage our users to be aware when they leave our site & to read the privacy statements of these sites. You should evaluate the security and trustworthiness of any other site connected to this site or accessed through this site yourself, before disclosing any personal information to them. This Company will not accept any responsibility for any loss or damage in whatever manner, howsoever caused, resulting from your disclosure to third parties of personal information.</p>

            <h2>Communication</h2>

            <p>For any kind of communication, please write to <a href="mailto:info@freecharge.in">info@freecharge.in</a></p>

            <h2>FreeCharge is headquartered at:</h2>
            <p>
                Accelyst solutions Pvt. Ltd.<br/>
                Vaibhav Chambers, Unit No. 2A, 2nd floor<br/>
                Bandra Kurla Complex, Bandra East, Mumbai-400 051<br/>
            </p>
        </section>
    </div>
</div>