<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="my-account" >
    <div class="sixteen columns">
		<h1><spring:message code="my.account" text="#my.account#"/></h1>
    </div>
    <div class="three columns">
        <div class="my-account-list">
            <ul>
			    <li><a href="mycredits.htm"><spring:message code="fc.money" text="#fc.money#"/></a></li>
           		 <li><a href="myrecharges.htm"><spring:message code="previous.recharges" text="#previous.recharges#"/></a></li>
           		 <li><a href="mycontacts.htm"><spring:message code="contacts" text="#contacts#"/></a></li>
            	<li><a href="myprofile.htm"><spring:message code="profile" text="#profile#"/></a></li>
            	<li class="active"><a href="mycoupons.htm"><spring:message code="my.coupons" text="#coupons#"/></a></li>
			</ul>
        </div>
    </div>
    <div class="thirteen columns">
        <div class="my-account-content">
        	Note: Showing coupons of orders placed after 23.07.2013. <br><br>
	        <div id="couponsDiv" data-user-email="${email}"></div>
        </div>
    </div>
</div>
<jsp:include page="./coupons-js.jsp"></jsp:include>
<c:if test="${isMyCouponsDisplayEnabled}">
	<script type="text/javascript">
	$(document).ready(function (){
		loadCouponsForUser("couponsDiv");
	});
	</script>
</c:if>
