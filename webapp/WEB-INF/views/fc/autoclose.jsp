<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery-1.8.2.min.js?v=${mt:keyValue("version.no")}"></script>
<script lang="javascript">
  function refreshParent() {
    var url = window.opener.location.href;
    // url = url.replace(/\&payment=fail/, '');
    window.opener.location.href= url;
    if (window.opener.progressWindow) {
      window.opener.progressWindow.close();
    }
  }
  $(document).ready(function () {
    try {
      if (window.opener==null) {
        window.location.href = "/";
      } else {
        refreshParent();
      }
    } catch(err) {
        // ignore errors
    }
    window.close();
  });
</script>
