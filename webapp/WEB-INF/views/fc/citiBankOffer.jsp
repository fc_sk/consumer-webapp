<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<%@ page import="com.freecharge.citibankoffer.controller.CitiBankOfferController" %>

<script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery-1.8.2.min.js?v=${mt:keyValue("version.no")}"></script>

<script language="javascript" >
    function submitCitiBankForm() {
        var params = $('#frmfreecharge').serialize();
        $.ajax({
            url: '<%=CitiBankOfferController.CREATE_LINK%>',
            type: "POST",
            dataType: 'json',
            data: params,
            cache: false,
            success: function (responseText) {
                if (responseText.accept) {
                    $("#citiBankOfferAccept").show();

                    $('#citiBankFormVisitor').attr('value', '91' + $('#Mobile').val());
                    $('#frmfreecharge').submit();

                    $("#citiBankOfferForm").hide();
                } else if (responseText.reject) {
                    $("#citiBankOfferReject").show();
                    $("#citiBankOfferForm").hide();
                }
            },
            error:function (e) {
                showErrorMessage();
            }
        });

        function showErrorMessage() {
            $("#citiBankOfferErrorMessage").html('Something went wrong at our end, please try again.');
        }
    }

    function enter_check(e)
    {
        var whichCode = (window.Event) ? e.which : e.keyCode;
        if (whichCode==13)
        {
            if(validate(1))
                return true;
            return false;
        }
    }

    function special_validation(incomingString, defaultValue)
    {
        if(trimSpace(incomingString).length == 0 || incomingString.search(/[^a-zA-Z0-9.,'&-/ ]/g) != -1 || incomingString==defaultValue)
            return false;
        else
            return true;
    }
    function limitlength(obj, length)
    {
        var maxlength=length;
        if (obj.value.length>maxlength)
            obj.value=obj.value.substring(0, maxlength);
    }
    function setlength(obj, length)
    {
        if(navigator.appName=="Netscape")
        {
            var maxlength=length;
            if (obj.value.length>maxlength);
            obj.value=obj.value.substring(0, maxlength);
        }
    }
    function trimSpace(x)
    {
        var emptySpace = / /g;
        var trimAfter = x.replace(emptySpace,"");
        return(trimAfter);
    }

    function textValidate(incomingString, defaultValue)
    {
        if(trimSpace(incomingString).length == 0 || incomingString.search(/[^a-zA-Z ]/g) != -1 || incomingString==defaultValue)
            return false;
        else
            return true;
    }
    function nameValidate(incomingString, defaultValue)
    {
        if(trimSpace(incomingString).length == 0 || incomingString.search(/[^a-zA-Z ]/g) != -1 || incomingString==defaultValue)
            return false;
        else
            return true;
    }
    function numberValidate(incomingString, defaultValue)
    {
        if(trimSpace(incomingString).length == 0 || incomingString.search(/[^0-9]/g) != -1)
        {
            return false;
        }
        else
            return true;
    }
    function alphaValidatePAN(incomingString, defaultValue)
    {
        if(trimSpace(incomingString).length == 0 || incomingString.search(/[^a-zA-Z]/g) != -1 || incomingString==defaultValue || parseInt(incomingString, 10) <= 0 )
            return false;
        else
            return true;
    }
    function switchText(object, baseText, eventName)
    {
        if (eventName == "blur")
        {
            if (object.value == "")
            {
                object.value = baseText;
            }
        }
        else
        {
            if (object.value == baseText)
            {
                object.value = "";
            }
        }
    }
    function validate()
    {
        var DobFailed = false;
        if((document.getElementById('FirstName').value=='*First Name') || trimSpace(document.getElementById('FirstName').value)=="")
        {
            alert("Please enter your First Name")
            document.getElementById('FirstName').focus()
            document.getElementById('FirstName').select()
            return false
        }
        if(!nameValidate(document.getElementById('FirstName').value,''))
        {
            alert("Please enter your First Name (Alphabets only)")
            document.getElementById('FirstName').focus()
            document.getElementById('FirstName').select()
            return false
        }

        if((document.getElementById('LastName').value=='*Last Name') || trimSpace(document.getElementById('LastName').value)=="")
        {
            alert("Please enter your Last Name")
            document.getElementById('LastName').focus()
            document.getElementById('LastName').select()
            return false
        }
        if(!nameValidate(document.getElementById('LastName').value,''))
        {
            alert("Please enter your Last Name (Alphabets only)")
            document.getElementById('LastName').focus()
            document.getElementById('LastName').select()
            return false
        }
        if(document.getElementById('city').selectedIndex==0)
        {
            alert("Please select your City")
            document.getElementById('city').focus()
            return false
        }
        if(document.getElementById('age').selectedIndex==0)
        {
            alert("Please select your Age")
            document.getElementById('age').focus()
            return false
        }
        if(trimSpace(document.getElementById('Mobile').value)=='')
        {
            alert("Please enter your Mobile Number");
            document.getElementById('Mobile').focus();
            document.getElementById('Mobile').select();
            return false;
        }
        if(!numberValidate(document.getElementById('Mobile').value,''))
        {
            alert("Please enter a valid Mobile Number");
            document.getElementById('Mobile').focus();
            document.getElementById('Mobile').select();
            return false;
        }
        if(document.getElementById('Mobile').value.length!=10 || (document.getElementById('Mobile').value.charAt(0)!=9 && document.getElementById('Mobile').value.charAt(0)!=8 && document.getElementById('Mobile').value.charAt(0)!=7))
        {
            alert("Please enter a valid Mobile Number");
            document.getElementById('Mobile').focus();
            document.getElementById('Mobile').select();
            return false;
        }
        if(document.getElementById('Profession').selectedIndex==0)
        {
            alert("Please select your Profession")
            document.getElementById('Profession').focus()
            return false
        }
        if(document.getElementById('gross_annual_income').selectedIndex==0)
        {
            alert("Please select your Annual Salary")
            document.getElementById('gross_annual_income').focus()
            return false
        }

        submitCitiBankForm();
    }

    function isNum(arg)
    {
        var args = arg;

        if (args == "" || args == null || args.length == 0)
        {
            return false;
        }
        args = args.toString();
        for (var i = 0;  i<args.length;  i++)
        {
            if ((args.substring(i,i+1) < "0" || args.substring(i, i+1) > "9") && args.substring(i, i+1) != ".")
            {
                return false;
            }
        }
        return true;
    }

</script>


<div id="citiBankOffer">
    <div id="citiBankOfferSuccessMessage">
        <div id="citiBankOfferAccept" style="display: none;">
            <table width="600" height="80px" border="0" cellspacing="0" cellpadding="0" style="border-style:solid; border-width:1px; color:#ececec">
                <tr>
                    <td width="598" height="19" align="left" valign="top" background="${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/head_bg.gif"><table width="590" border="0" align="center" cellpadding="2" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#ffffff; font-weight:bold">
                        <tr>
                            <td width="339" align="left">Thank You</td>
                        </tr>
                    </table></td>
                </tr>
                <tr>
                    <td align="left" valign="top"><table width="586" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#1E4079; padding:3px 0 0 0">Thank you for expressing your interest in the Citibank  Credit Card. A Citibank representative will contact you within 2 working days.</td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#6D6E71; padding:6px 0 0 0">CITIBANK is a registered service mark of Citibank, N.A.</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
        </div>

        <div id="citiBankOfferReject" style="display: none;">
            <table width="600" height="95px" border="0" cellspacing="0" cellpadding="0" style="border-style:solid; border-width:1px; color:#ececec">
                <tr>
                    <td width="598" height="19" align="left" valign="top" background="${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/head_bg.gif"><table width="590" border="0" align="center" cellpadding="2" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#ffffff; font-weight:bold">
                        <tr>
                            <td width="339" align="left">Thank You</td>
                        </tr>
                    </table></td>
                </tr>
                <tr>
                    <td align="left" valign="top"><table width="586" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#1E4079; padding:3px 0 0 0">We regret to inform you that we will not be able to process your application as the details provided do not meet the eligibility criteria. We apologize for the inconvenience this may have caused but would like to thank you for considering Citibank.</td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#6D6E71; padding:8px 0 0 0">CITIBANK is a registered service mark of Citibank, N.A.</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
        </div>
    </div>

    <div id="citiBankOfferErrorMessage">
    </div>

    <div id="citiBankOfferForm">
        <form id="frmfreecharge" name="frmfreecharge" method="POST" action="http://c4c.phonon.in/citi/request">
            <input value="" name="externalOfferCode" id="externalOfferCode" type="hidden"/>
            <input name="Site" id="Site" value="" type="hidden"/>
            <input name="Creative" id="Creative" value="" type="hidden"/>
            <input name="Section" id="Section" value="" type="hidden"/>
            <input name="Agency_Code" id="Agency_Code" value="" type="hidden"/>
            <input name="Campaign_Code" id="Campaign_Code" value="" type="hidden"/>
            <input name="Product_Code" id="Product_Code" value="CARDS" type="hidden"/>
            <input name="Source_Id" id="Source_Id" value="Internet" type="hidden"/>
            <input name="Country_Id" id="Country_Id" value="IN" type="hidden"/>
            <table width="600" height="100px" border="0" cellspacing="0" cellpadding="0" style="border-style:solid; border-width:1px; color:#ececec">
                <tr>
                    <td width="598" height="19" align="left" valign="top" background="${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/head_bg.gif"><table width="590" border="0" align="center" cellpadding="2" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#ffffff; font-weight:bold">
                        <tr>
                            <td width="339" align="left">Confirm your details</td>
                            <td width="243" align="right" style="font-size:10px"><%--[ Edit Details ]--%></td>
                        </tr>
                    </table></td>
                </tr>
                <tr>
                    <td align="left" valign="top"><table width="586" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#000; padding:3px 0 0 0"><span style="color:#f00">*</span>Mandatory</td>
                        </tr>
                        <tr>
                            <td align="left"><table width="500" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="44" align="left" valign="top"><select name="Title" id="Title" tabindex="1" style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: normal; color: #000000; background:url(${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/drop_s.gif) no-repeat 0px 0px; background-color:#f7f7f7; text-align: left;width:40px; border:1px solid #bfe5ff; height:17px; outline:none; border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px;" onkeypress="return enter_check(event);">
                                        <option selected="selected">Mr.</option>
                                        <option>Mrs.</option>
                                        <option>Ms.</option>
                                    </select></td>
                                    <td width="83" align="left" valign="top"><input tabindex="2" maxlength="50" size="50" value="*First Name" name="FirstName" id="FirstName" onblur="javascript:switchText(this, '*First Name', 'blur');" onfocus="javascript:switchText(this, '*First Name', 'click');" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #4a4848; text-decoration: none; border:0px; outline:none; width:80px;height:17px; background:url(${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/input_80.gif) no-repeat 0 0; padding:1px 0px 3px 5px;" onkeypress="return enter_check(event);" /></td>
                                    <td width="101" align="left" valign="top"><input tabindex="3" maxlength="50" size="50" value="*Last Name" name="LastName" id="LastName" onblur="javascript:switchText(this, '*Last Name', 'blur');" onfocus="javascript:switchText(this, '*Last Name', 'click');" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #4a4848; text-decoration: none; border:0px; outline:none; width:80px;height:17px; background:url(${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/input_80.gif) no-repeat 0 0; padding:1px 0px 3px 5px;" onkeypress="return enter_check(event);" /></td>
                                    <td width="33" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000; padding:1px 0 0 0"><span style="color:#f00">*</span>City:</td>
                                    <td width="109" align="left" valign="top"><select name="city" id="city" tabindex="4" style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: normal; color: #000000; background:url(${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/drop_b.gif) no-repeat 0px 0px; background-color:#f7f7f7; text-align: left;width:90px; border:1px solid #bfe5ff; height:18px; outline:none; border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px;" onkeypress="return enter_check(event);">
                                        <option selected="selected" value="">----- Select -----</option>
                                        <option value="Bengaluru">Bengaluru</option>
                                        <option value="Chennai">Chennai</option>
                                        <option value="Mumbai">Mumbai</option>
                                        <option value="Delhi">Delhi</option>
                                        <option value="Pune">Pune </option>
                                        <option value="Hyderabad">Hyderabad</option>
                                        <option value="Others">Others </option>
                                    </select></td>
                                    <td width="36" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000; padding:1px 0 0 0"><span style="color:#f00">*</span>Age:</td>
                                    <td width="94" align="left" valign="top"><select name="age" id="age" tabindex="5" style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: normal; color: #000000; background:url(${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/drop_b.gif) no-repeat 0px 0px; background-color:#f7f7f7; text-align: left;width:90px; border:1px solid #bfe5ff; height:18px; outline:none; border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px;" onkeypress="return enter_check(event);">
                                        <option selected="selected">-- Select --</option>
                                        <option value="18-22">18-22</option>
                                        <option value="23-30">23-30</option>
                                        <option value="31-40">31-40</option>
                                        <option value="41 and above">41 and above</option>
                                    </select></td>
                                </tr>
                            </table></td>
                        </tr>
                        <tr>
                            <td align="left" style="padding:3px 0 0 0"><table width="586" border="0" align="center" cellpadding="0" cellspacing="0" >
                                <tr>
                                    <td align="left" valign="top" style="padding:8px 0 0 0"><table width="500" border="0" align="center" cellpadding="0" cellspacing="0" >
                                        <tr>
                                            <td width="45" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000; padding:1px 0 0 0"><span style="color:#f00">*</span>Mobile:</td>
                                            <td width="110" align="left" valign="top"><input tabindex="6" maxlength="10" size="10" name="Mobile" id="Mobile" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #4a4848; text-decoration: none; border:0px; outline:none; width:90px;height:17px; background:url(${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/input_90.gif) no-repeat 0 0; padding:1px 0px 3px 5px;" onkeypress="return enter_check(event);" /></td>
                                            <td width="63" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000; padding:1px 0 0 0"><span style="color:#f00">*</span>Profession:</td>
                                            <td width="110" align="left" valign="top"><select name="Profession" id="Profession" tabindex="7" style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: normal; color: #000000; background:url(${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/drop_b.gif) no-repeat 0px 0px; background-color:#f7f7f7; text-align: left;width:90px; border:1px solid #bfe5ff; height:18px; outline:none; border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px;" onkeypress="return enter_check(event);">
                                                <option selected="selected" value="">----- Select -----</option><option value="Salaried">Salaried</option><option value="Self-employed">Self-employed</option>
                                            </select></td>
                                            <td width="78" align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000; padding:1px 0 0 0"><span style="color:#f00">*</span>Annual Salary:</td>
                                            <td width="94" align="left" valign="top"><select name="gross_annual_income" id="gross_annual_income" tabindex="8" style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: normal; color: #000000; background:url(${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/drop_b.gif) no-repeat 0px 0px; background-color:#f7f7f7; text-align: left;width:90px; border:1px solid #bfe5ff; height:18px; outline:none; border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px;" onkeypress="return enter_check(event);">
                                                <option selected="selected" value="">----- Select -----</option>
                                                <option value="Below 2.4L p.a">Below 2.4L p.a</option>
                                                <option value="2.4L - 3L p.a">2.4L - 3 p.a</option>
                                                <option value="3L - 5L p.a">3L - 5L p.a</option>
                                                <option value="5L - 10L p.a">5L - 10L p.a</option>
                                                <option value="10L and above">10L and above</option>
                                            </select></td>
                                        </tr>
                                    </table></td>
                                    <td width="89" align="right" valign="top"><img src="${mt:keyValue("imgprefix1")}/images/exclusive_offers/citibank/btn_talk.gif" width="77" height="30" border="0" style="cursor:pointer" onclick="return validate();"/></td>
                                </tr>
                            </table></td>
                        </tr>
                    </table></td>
                </tr>
            </table>
            <input type="hidden" name="visitor" id="citiBankFormVisitor">
            <input type="hidden" name="udf5" value="FCP">
            <input type="hidden" name="uniqueID" value="${orderId}">
            <input type="hidden" name="custToken" value="df23ghf4">
        </form>
    </div>
</div>