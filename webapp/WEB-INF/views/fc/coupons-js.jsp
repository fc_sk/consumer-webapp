<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	<script type="text/javascript">
	function loadCouponsForOrder(id){
		var $this = $('#'+id);
		var requestUrl = "/rest/orders/" + $this.data("order-id") + "/coupons";
		loadCoupons(id, requestUrl);
	}
	
	function loadCouponsForUser(id){
		var $this = $('#'+id);
		var requestUrl = "/rest/users/" + $this.data("user-email") + "/coupons";
		loadCoupons(id, requestUrl);
	}
	
	function loadCoupons (id, requestUrl){
		var $this = $('#'+id);
		var content = "";
		content = content + "<div style='text-align:center;'>Loading Coupons...<br/><img src='${mt:keyValue("imgprefix1")}/images/ajax-loader.gif'></div>";
		$this.html(content);
	    var couponsData = $.ajax({
	        url : requestUrl,
	        dataType : "json",
	        async : false,
	        cache : false
	    }).responseText;
	    content = "";
	    var couponsDataJson = jQuery.parseJSON(couponsData);
	    if (couponsDataJson == null || couponsDataJson.length == 0){
	    	content = content + "No coupons or special offers.";
	    }
	    else{
	        for (var i=0; i<couponsDataJson.length; ++i){
	        var couponLink = couponsDataJson[i].couponHyperlink;
	        var couponImagePath = couponsDataJson[i].couponImagePath;
	        var campaignName = couponsDataJson[i].campaignName;
	        var couponType = couponsDataJson[i].couponType;
	        var couponNature = couponsDataJson[i].couponNature;
	        var couponCode = couponsDataJson[i].couponCode;
	        var validityDate = couponsDataJson[i].validityDate;
	        var termsAndConditions = couponsDataJson[i].termsAndConditions;
	        var orderId = couponsDataJson[i].orderId;
	        var code = "Couriered to you.";
	        var couponTypeIndicatorClass = '';
	    	if (couponType != 'P'){
	    		code = couponCode;
	    		couponTypeIndicatorClass='coupon-history-indicator';
	    	}
	    	content = content + "<table width='100%' cellpadding='0' cellspacing='0' style='border:2px dashed #0d2651; background:#FEFDF5; mso-table-lspace:0pt;mso-table-rspace:0pt;' >";
	    	content = content + "<tr><td colspan='4'><div style='height: 10px;''>&nbsp;</div></td><td><div class='" + couponTypeIndicatorClass + "'></div></td></tr>";
	    	content = content + "<td colspan='5'><table><tr><td width='90px;'></td>"
	    	content = content + "<td width='95' rowspan='2' style='font-size:11px; padding:0px;' align='center'>";
	    	if (couponLink != null){
	    		content = content +  "<a href='"+ couponLink +"' style='text-decoration: none; border:0;font-size:11px; color:#000000; ' >";
	    	}
	    	content = content + "<img style='height:50px; width:50px display: block; text-decoration:none; border:0; word-break:break-all;' src='"+ couponImagePath + "' height='50' title='" + couponsDataJson[i].campaignName + "' alt='" + couponsDataJson[i].campaignName + "' width='95'/>";
	    	if (couponLink != null){
	    		content = content + "</a>";
	    	}
	    	content = content + "</td>" + 
	    	"<td width='290' valign='top' style='text-align:left; padding: 0px; padding-left: 7px' rowspan='2'>" + 
	    	"<span style='color:#999; font-size:14px; font-weight: bold; display: block;'>" + campaignName + "</span></td>";
			if (couponType != 'C'){
				content = content + 
		    	"<td width='135'  style='text-align:right; padding:0;' rowspan='2'><strong style='color:#0D2651; font-size:28px; font-weight:bold; text-align: center;'><span style='font-size:17px;'>Rs.</span>" + couponsDataJson[i].faceValue + "</strong></td>";	    		
	    	} else{
	    		content = content + 
		    	"<td width='135'  style='text-align:right; padding:0;' rowspan='2'><strong style='color:#0D2651; font-size:20px; font-weight:bold; text-align: center;'>SPECIAL OFFER</strong></td>";
	    	}
			content = content + "</tr></table></td></tr>" + 
			"<tr>" + 
			"<td style='padding:0; margin:0;' colspan='5'><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td colspan='4'><div style='height: 18px;'>&nbsp;</div></td></tr>" + 
			"<tr><td>"+ getCouponCodesTable(couponsDataJson[i]) +"</td>";
	    	if (validityDate != 'null' || validityDate != null){
	    		content = content + "<td  style='text-align:center; padding:0; font-size:16px; color:#000000;'><p style='margin:0; padding:0; font-size:11px; color:#999;'>Order Id: " + orderId + "&nbsp; </p><p style='margin:0; padding:0; font-size:11px; color:#999;'>Expire on: &nbsp; "+ validityDate +"</p></td>";	
	    	} else
    		{
	    		content = content + "<td style='text-align:center; padding:0; font-size:16px; color:#000000;'><p style='margin:0; padding:0; font-size:11px; color:#999;'>Order Id: " + orderId + "&nbsp; </p></td>";
    		}
	    	content = content + "</tr><tr><td colspan='3'><div style='height: 18px;'>&nbsp;</div></td></tr></table></td></tr></table>" + 
	    	"<table cellpadding='0' cellspacing='0' style='mso-table-lspace:0pt;mso-table-rspace:0pt;'><tr>" + 
	    	"<td style='padding:5px 10px; background:#fff;border-bottom:3px solid #f9f9f9;'>"+ 
	    	"<strong>Terms &amp; Conditions:</strong>" + 
	    		termsAndConditions + "</td></tr></table>";
	        }       
	    }  <%--end else --%>
	    $this.html(content);
	}
	
	function getCouponCodesTable(couponData){
		var code = "Couriered to you.";
		if (couponData.couponType != 'P'){
    		code = couponData.couponCode;
    	}
		var couponCodesTable = "";
    	var rows = "";
		if (couponData != null && couponData.couponNature == 'Offer List'){
			var couponCodes = couponData.couponCode.split(",");
			for (var i=0; i<couponCodes.length; ++i){
				rows = rows + "<tr><td width='90'  style='text-align: left; padding: 0px; padding-left: 7px' valign='middle'><span style='color:#999; font-size:12px;  line-height:18px;'> Offer " + (i+1) + ":</span></td>" + 
		    	"<td width='200'><div style='background: none repeat scroll 0 0 #eee; border: 1px dashed #333333; font-size: 16px; font-weight: bold; padding:1px 0px; display: block; text-align: center; width: 185px; margin: 0 5px;'>" + couponCodes[i] +"</div></td></tr>"
			}
		} else{
			rows = rows + "<tr><td width='90'  style='text-align: left; padding: 0px; padding-left: 7px' valign='middle'><span style='color:#999; font-size:12px;  line-height:18px;'> Offer " + couponData.couponNature + ":</span></td>" + 
	    	"<td width='200'><div style='background: none repeat scroll 0 0 #eee; border: 1px dashed #333333; font-size: 16px; font-weight: bold; padding:1px 0px; display: block; text-align: center; width: 185px; margin: 0 5px;'>" + code +"</div></td></tr>"
		}
		couponCodesTable = "<table>" + rows + "</table>";
		return couponCodesTable;
	}
	</script>
