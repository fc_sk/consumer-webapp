<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="my-account" >
    <div class="sixteen columns">
		<h1><spring:message code="my.account" text="#my.account#"/></h1>
    </div>
    <div class="three columns">
        <div class="my-account-list">
            <ul>
			    <li><a href="mycredits.htm"><spring:message code="fc.money" text="#fc.money#"/></a></li>
           		 <li class="active"><a href="myrecharges.htm"><spring:message code="previous.recharges" text="#previous.recharges#"/></a></li>
           		 <li><a href="mycontacts.htm"><spring:message code="contacts" text="#contacts#"/></a></li>
            	<li><a href="myprofile.htm"><spring:message code="profile" text="#profile#"/></a></li>
            	<li><a href="mycoupons.htm"><spring:message code="my.coupons" text="#coupons#"/></a></li>            	
			</ul>
        </div>
    </div>
    <div class="thirteen columns">
        <div class="my-account-content">
                	<c:choose>
                        <c:when test="${fn:length(rechargeHistoryList) gt 0 }">
                            <table class="table-generic">
                                <thead>
                                    <tr>
                            <th width="15%"><spring:message code="date" text="#date#"/></th>
                            <th width="45%"><spring:message code="recharge" text="#recharge#"/></th>
                            <th width="25%"><spring:message code="order.id" text="#order.id#"/></th>
                            <th width="15%"><spring:message code="order.status" text="#order.staus#"/></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${rechargeHistoryList}" var="rechargeHistoryList">
                                        <tr>
                                            <form name="" id="${rechargeHistoryList.orderId}-form" method="post" action="/app/rechargeagain.htm">
                                                <td>${rechargeHistoryList.createdOn}</td>
                                                <td>
                                                    <input type="hidden" name="rechargeHistoryId" id="${rechargeHistoryList.orderId}-amount" value="${rechargeHistoryList.userRechargeHistoryId}" />
                                                    <p class="no-margin">
                                            (${rechargeHistoryList.productName})
                                                    </p>
                                        <p class="no-margin"><strong>${rechargeHistoryList.serviceNumber}</strong> - ${rechargeHistoryList.operatorCode} - ${rechargeHistoryList.circleName}</p>
                                                    <p class="no-margin">Amount: <span class="webRupee">Rs.</span>${rechargeHistoryList.txnAmount}</p>
                                                </td>
                                                <td>
                                        ${rechargeHistoryList.orderId}
                                        <p>
                                            <c:if test="${rechargeHistoryList.rechargeStatus == '00' }">
                                                <a href="invoice.htm?orderId=${rechargeHistoryList.orderId}" target="_blank"> View invoice</a>
                                            </c:if>
                                        </p>
                                    </td>
                                    <td>
                                                    <c:choose>
                                            <c:when test="${rechargeHistoryList.rechargeStatus == '00'}">
                                                <span class="text-color-2"><spring:message code="success" text="#success#"/></span>
                                            </c:when>
                                            <c:when test="${rechargeHistoryList.rechargeStatus == '08' || rechargeHistoryList.rechargeStatus == '-1'}">
                                                <span class="text-color-1"><spring:message code="under.process" text="#under.process#"/></span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="text-color-3"><spring:message code="failed" text="#failed#"/></span>
                                            </c:otherwise>
                                                    </c:choose>
                                                </td>
                                            </form>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </c:when>
                        <c:otherwise>
                            <br>
                            <p class="noContentMsg"><spring:message code="recharge.historymsg" text="#recharge.historymsg#"/>.</p>
                            <br>
                        </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
