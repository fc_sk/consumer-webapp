        <nav>
            <ul>
                <li><a href="/app/aboutus.htm"  class="active">About us</a></li>
                <li><a href="/app/contactus.htm">Contact us</a></li>
                <!--<li><a href="/app/faq.htm">FAQ</a></li>-->
                <li><a href="/app/sitemap.htm">Sitemap</a></li>
                <li><a href="http://support.freecharge.in" target="_blank" >Customer support</a></li>
                <li><a href="/app/termsandconditions.htm">Terms &amp; conditions</a></li>
                <!--<li><a href="/app/privacypolicy.htm">Privacy policy</a></li>
                <li><a href="/security/disclosure.htm">Security Policy</a></li><br>-->
            </ul>
        </nav>

<div class="row">
	<div class="col-md-12">

    	<section class="static-content">
            <h1>About Us</h1>
            <p>FreeCharge, a wholly owned subsidiary of Axis Bank Limited, is India's No.1 payments app. Customers across the country use FreeCharge to make prepaid, postpaid, DTH, metro recharge and utility bill payments for numerous service providers. We launched our wallet in September 2015 and customers are already using it to pay across all major online platforms and offline stores like Shoppers Stop, McDonalds, Cinepolis, HomeStop, Crosswords, Hypercity and even for E-Rickshaws, the list is growing by the day.</p> 
            <p>
            We are on a mission to get millions of merchants both in organised and unorganised sector to be a part of the digital payments ecosystem. Our Chat-n-Pay service is all about social payments. It is an engaging and secure way for you to seamlessly Chat-n-Pay to friends, family, and merchants in less than 5 seconds. It also enables merchants, small or large to accept digital payments in less than 1 minute after registering on the FreeCharge App.
            </p>

            <h2>We promise:</h2>
            <ul>
            	<li>An easy and instant recharging process</li>
            	<li>A hassle-free online experience</li>
            	<li>A safe and secure payment process </li>
            	<li>Best discounts available in the market</li>
            	<li>Simple voucher redemption with no hidden costs or complex terms and conditions </li>
            	<li>And a totally satisfied you, who&#39;ll want to come back to us again and again </li>
            </ul>
            <p>After all, you know it&#39;s a smart thing to do! </p>
        </section>
    </div>
</div>