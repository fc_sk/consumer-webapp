<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<form name="pgDownAlertListForm" action="/admin/pgalert/list.htm" method="GET">
<h2> <a href="/admin/pgalert/create.htm">Create PaymentGateway Down Alert</a></h2>
<c:if test="${not empty noDownTime}">
  <div class="alert no-downtime">
    <c:out value="${noDownTime}" />
  </div>
</c:if>
<c:if test="${not empty param.message}">
        <div class="alert alert-success">
        <c:out value="${param.message}"/>
        </div>
    </c:if>
<br>

<c:if test="${not empty allPgAlertsList}">
    <table class="table table-bordered table-hover">
    <h2> List of All Payment Gateway Alerts </h2>
    <thead>
         <tr>
          <th> PaymentGateway ID </th>
          <th> Start Date Time</th>
          <th> End Date Time</th>
          <th> Alert Message</th>
          <th> Delete Alert </th>
         </tr>
    </thead>
    <tbody>
        <c:forEach var="item" items="${allPgAlertsList}">
            <tr>
                <td>${item.pgId}</td>
                <td>${item.startDateTime}</td>
                <td>${item.endDateTime}</td>  
                <td>${item.alertMessage}</td>
                <td><a href="/admin/pgalert/delete.htm?pgId=${item.pgId}">Delete</a></td>
            </tr>
        </c:forEach>
    </tbody>    
    </table>
</c:if>
</form>