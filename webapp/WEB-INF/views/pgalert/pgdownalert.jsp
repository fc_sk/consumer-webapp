<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<c:if test="${not empty successMsg}">
  <div class="alert alert-success">
    <c:out value="${successMsg}" />
  </div>
</c:if>

<c:if test="${not empty errorMsg}">
  <div class="alert alert-error">
    <c:out value="${errorMsg}" />
  </div>
</c:if>

<div class="row span12">
 
  <h2> <a href="/admin/pgalert/list.htm"><spring:message code="view.all.pg.alert.list" text="#view.all.pg.alert.list#"/></a></h2>
  
  <h2><spring:message code="payment.gateway.down.alert" text="#payment.gateway.down.alert#"/></h2>

    <form name="pgDownAlertForm" action="/admin/pgalert/create.htm" method="POST">
      <fieldset>
      <label><b><spring:message code="select.bank" text="#select.bank#"/></b></label>
      <select name="pgkey_list">
        <option value=""><spring:message code="select.option" text="#select.option#"/></option>
        <c:forEach var="item" items="${pgNames}">
            <option value="${item.key}"><c:out value="${item.value}"/></option>
        </c:forEach>
      </select>

      <label><spring:message code="start.date" text="#start.date#"/></label>
      <input type="text" value="" name="start_date" id="startDate"></input> (yyyy-MM-dd:hh:mm:ss)[Time in:24hr Format]

      <label><spring:message code="end.date" text="#end.date#"/></label>
      <input type="text" value="" name="end_date" id="endDate"></input> (yyyy-MM-dd:hh:mm:ss)[Time in:24hr Format]

      <label><spring:message code="alert.message" text="#alert.message#"/></label>
      <input class="span10" type="text" width="100px" value="We are facing a high failure rate for ${item.value} ICICI in the last few minutes, please try after some time" name="alertMsg" id="alertText"></input>

      <div class="form-actions">
        <button type="submit" class="btn btn-primary"><spring:message code="save.changes" text="#save.changes#"></spring:message></button>
        <button type="button" class="btn">Cancel</button>
      </div>

      </fieldset>
    </form>
</div>


