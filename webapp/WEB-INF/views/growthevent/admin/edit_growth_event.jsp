<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<style>
ul li {
	margin-top: 5px;
	list-style: none;
	padding-left: 15px;
}

ul {
	margin: 0;
	padding: 0;
	list-style: none;
}
</style>
<div class="well">
	<h3>
		<c:choose>
			<c:when test="${edit}">Edit</c:when>
			<c:otherwise>Add</c:otherwise>
		</c:choose>
		Growth Event
	</h3>
	
	<div class="alert" style='display: <c:if test="${empty errors}">none</c:if>'>${errors}<a class="close" href="#">&times;</a></div>
	
	<form:form action="/admin/growthevent/save.htm" method="POST" commandName="growthEventEntity" id="growth_event_form" cssClass="form form-horizontal">
		<form:hidden path="id" />
		<div class="control-group">
			<label for="name" class="control-label">Deal name</label>
			<div class="controls">
				<form:input path="name" id="name" />
				<form:errors path="name" cssStyle="color: red" />
			</div>
		</div>
		<div class="control-group">
			<label for="actionId" class="control-label">Event</label>
			<div class="controls">
				<form:select path="eventType" id="eventType" >
					<form:options items="${eventTypeList}" itemLabel="description"  />
				</form:select>
				<form:errors path="eventType" cssStyle="color: red" />
			</div>
		</div>
		<div class="control-group">
			<label for="rewardId" class="control-label">Reward</label>
			<div class="controls">
				<form:select path="fkRewardId" id="rewardId">
					<form:options items="${rewards}" itemLabel="name" itemValue="id" />
				</form:select>
				<form:errors path="fkRewardId" cssStyle="color: red" />
			</div>
		</div>
		<div class="control-group">
			<label for="conditionId" class="control-label">Condition</label>
			<div class="controls">
				<form:select path="fkConditionId" id="conditionId">
					<form:options items="${conditions}" itemLabel="name" itemValue="id" />
				</form:select>
				<form:errors path="fkConditionId" cssStyle="color: red" />
			</div>
		</div>
		<div class="control-group">
			<label for="channels" class="control-label">Channels</label>
			<div class="controls" id="channels">
			<input type="hidden" name="channels" id="selectedChannels"/>
					<c:forEach items="${channels}" var="channel" >
                        <label for="${channel}"><input type="checkbox" value="${channel}" id="${channel.id}"/>${channel}</label>
                    </c:forEach>
				<form:errors path="channels" cssStyle="color: red" />
			</div>
		</div>
		<div class="control-group">
			<label for="validFrom" class="control-label">Valid From</label>
			<input id="validFrom" style="display:none" name="validFrom" value="<fmt:formatDate pattern='dd/MM/yyyy HH:mm:ss' value='${growthEventEntity.validFrom}'/>" />
			<div id="validFromDTP" class="controls input-append"
				style="margin-left: 20px;">
				<input type="text" id="validFrom1" name="validFrom1" value="<fmt:formatDate pattern='dd/MM/yyyy HH:mm:ss' value='${growthEventEntity.validFrom}'/>"  data-format="dd/MM/yyyy hh:mm:ss"  />
				(dd-mm-yyyy) 24hr format 	
				<span class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i> </span>
				<form:errors path="validFrom" cssStyle="color: red" />
			</div>
		</div>
		<div class="control-group">
			 <label for="validUpto" class="control-label">Valid Upto</label>
			 <input id="validUpto" style="display:none" name="validUpto" value="<fmt:formatDate pattern='dd/MM/yyyy HH:mm:ss' value='${growthEventEntity.validUpto}'/>" />
			 <div id="validUptoDTP" class="controls input-append"
				style="margin-left: 20px;"  data-date="<fmt:formatDate pattern='dd/MM/yyyy hh:mm:ss' value='${growthEventEntity.validUpto}'/>" data-date-format="dd/MM/yyyy hh:mm:ss" >
				<input type="text" id="validUpto1" name="validUpto1" value="<fmt:formatDate pattern='dd/MM/yyyy HH:mm:ss' value='${growthEventEntity.validUpto}'/>" data-format="dd/MM/yyyy hh:mm:ss" />
				(dd-mm-yyyy) 24hr format 
				<span class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i> </span>
				<form:errors path="validUpto" cssStyle="color: red" />
			</div>
		</div>
		<div class="control-group">
			<label for="privUserCond" class="control-label"> <form:checkbox
					path="privUserCond" id="privUserCond" /> Privilege User Group
			</label>
		</div>
		<div class="form-actions">
			<input id="submitForm" class="btn btn-info" value="Save">
		</div>
	</form:form>
</div>
<script type="text/javascript">
     $(function() {
         $('#validFromDTP').datetimepicker({
             maskInput: true,
             pickDate: true,
             pickTime: true
         });
         
         $('#validUptoDTP').datetimepicker({
             maskInput: true,
             pickDate: true,
             pickTime: true
         });
     });   
     $(document).ready(function(){
        $('#submitForm').on('click', function(ev){
             $("#validFrom").val($("#validFrom1").val());
             $("#validUpto").val($("#validUpto1").val());
             var selectedChannel = '';
             $("#channels input").each(function(){
                 if($(this).attr('checked') == 'checked'){
                     selectedChannel = selectedChannel + $(this).attr('id') + ',';
                 }
             });
            selectedChannel = selectedChannel.slice(0,selectedChannel.length-1)
            $("#selectedChannels").val(selectedChannel);
            $("#growth_event_form").submit()
        });
        populateChannels();
    });
    function populateChannels(){
        <c:forEach items="${eventChannels}" var="eventChannel">
            $("#channels input[value='" + "${eventChannel}" + "']").prop('checked', true);
        </c:forEach>
    }
</script>