<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<head>
    <link href="/content/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>
<div class="container" style="padding-bottom: 10px;">
	<div class="alert"
		style='display: <c:if test="${empty param.message}">none</c:if>'>
		<c:if test="${not empty param.message}">
			<c:out value="${param.message}" />
		</c:if>
		<a class="close" href="#">&times;</a>
	</div>

	<div class="span12">
		<h2>Growth Events</h2>
		<a class="btn btn-info" href="/admin/growthevent/edit.htm"
			title="Add New Deal">Add Event</a> <br> <br>

		<c:choose>
			<c:when test="${not empty growthEvent}">
				<div class="row">
					<table class="table table-bordered table-striped">
						<tr>
							<th><strong>Actions</strong></th>
							<th><strong>Id</strong></th>
							<th><strong>Name</strong></th>
							<th><strong>Event Type</strong></th>
							<th><strong>Reward Id</strong></th>
							<th><strong>Condition Id</strong></th>
							<th><strong>Valid From</strong></th>
							<th><strong>Valid Upto</strong></th>
							<th>Channels</th>
							<th><strong>Closed User Group</strong>
						</tr>
						<c:forEach var="event" items="${growthEvent}">
							<tr>
								<td><a href="/admin/growthevent/edit?id=${event.id}">edit</a></td>
								<td>${event.id}</td>
								<td>${event.name}</td>
								<td>${event.eventType}</td>
								<td>${event.fkRewardId}</td>
								<td>${event.fkConditionId}</td>
								<td><fmt:formatDate pattern='dd/MM/yyyy HH:mm:ss' value='${event.validFrom}'/></td>
								<td><fmt:formatDate pattern='dd/MM/yyyy HH:mm:ss' value='${event.validUpto}'/></td>
								<td>${event.channels}</td>
								<td>${event.privUserCond}</td>
							</tr>
						</c:forEach>
					</table>
				</div>				
			</c:when>
			<c:otherwise>
				<h4>No Event Created!!</h4>
			</c:otherwise>
		</c:choose>

	</div>

<div class="span12">
	<div class="row">
		<div class="span8">
			 <a href="#" onClick='doPost("/admin/growthevent/flushCache")'>Flush Cache</a>
		</div>
	</div>
</div>

</div>
<script type="text/javascript">
function changeStatus(url, id, status) {
    $.post(url, {"id":id, "status":status}, function(data) {
        location.reload();
    }, 'json');
}
    function attachClose(){
        $('a.close').on('click', function(ev){
            ev.preventDefault();
            $(ev.currentTarget).parent().remove();
        });
    }
    function doPost(url) {
        $.post(url, {}, function(data) {
            location.reload();
        }, 'json');
      }
</script>