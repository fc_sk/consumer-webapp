<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test = "${mt:keyValue('system.ctx.mode') != 'prod'}">
    <style type="text/css">
        dev {
            position: absolute;
            top: 0;
            z-index: 1000;
            background-color: #fff;
            width: 100%;
            opacity: 0.8;
            bottom: auto;
        }
    </style>
    <dev>
        <wrapper>
            <strong>${mt:keyValue("system.ctx.mode")}</strong>
            <where>${mt:keyValue("build.node")}</where>        
            <span>${mt:keyValue("build.branch")}</span>
            <time>${mt:keyValue("build.time")}</time>
            <revision>${mt:keyValue("build.revision")}</revision>
            <button class="close" onclick="$(this).parents('dev').slideUp(200);">x</button>
        </wrapper>
    </dev>

    <%--<div class="system-info">
        <p>
            <span>mode</span> <span>= ${mt:keyValue("system.ctx.mode")}</span>
            <span>branch</span> <span>= ${mt:keyValue("build.branch")}</span>
            <span>revision</span> <span>= ${mt:keyValue("build.revision")}</span>
            <span>node</span> <span>= ${mt:keyValue("build.node")}</span>
            <span>build-time</span> <span>= ${mt:keyValue("build.time")}</span>
            <a class="close" onclick="$(this).parents('.system-info').slideUp(200);">x</a>
        </p>
    </div>--%>
</c:if>
