<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
	
<html>

	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

		<title>60 Crore Ka Recharge</title>
		<meta name="description" content="Buy a Pepsi bottle and Use Promo code printed on the label to get Rs.20 Off on your recharge done on FreeCharge Official site.">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				
		<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/pepsi/qbkl-grid.css?v=${mt:keyValue("version.no")}" media="screen">
		<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/pepsi/style1.css?v=${mt:keyValue("version.no")}" media="screen">

		<!-- Loading: Font Awesome -->
		<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/pepsi/font-awesome/css/font-awesome.min.css?v=${mt:keyValue("version.no")}">
		<!--[if IE 7]>
		<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/pepsi/font-awesome/css/font-awesome-ie7.min.css?v=${mt:keyValue("version.no")}">
		<![endif]-->

		<!--[if lt IE 9]>
		<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js?v=${mt:keyValue("version.no")}"></script>
		<![endif]-->
	</head>

	<body>
		
		<!-- Start: Intro -->
		<section id="intro">
			<div class="info">
				<img src="${mt:keyValue('imgprefix1')}/images/pepsi/banner2.png" >
				<div class="brands">
					<img src="${mt:keyValue('imgprefix1')}/images/pepsi/pepsi.png">
					<img src="${mt:keyValue('imgprefix1')}/images/pepsi/mountain.png">
					<img src="${mt:keyValue('imgprefix1')}/images/pepsi/mirinda.png">
					<img src="${mt:keyValue('imgprefix1')}/images/pepsi/7up.png">
					<img src="${mt:keyValue('imgprefix1')}/images/pepsi/slice.png">
				</div>
				<h4>Get Free Rs.20 recharge at FreeCharge.com</h4>
				<p class="intro-text">Pick up a bottle of Pepsi, Mountain Dew, Mirinda, 7UP (1L, 1.25L, 2L, 2.25L) or 
				Slice (1.2L). Look for a unique 11-digit code under the label of your bottle and redeem it.</p>
			</div>

			<nav id="nav">
				<!-- The content between <span> </span> tags will be hidden on mobile version -->
				<ul class="clearfix">
					<li><a href="#redeem">How to redeem</a></li>
					<li><a href="#faq">FAQ</a></li>
					<li><a href="#terms">Terms &amp; Conditions</a></li>
				</ul>
			</nav>
		</section>
		<!-- End: Intro -->

		<!-- Start: Guide -->
		<section id="redeem" class="section">
			<div class="container">
				<div class="row">
					<div class="col-full">
						<h2 class="section-title">How to redeem</h2>
						<p class="section-title-text">And get Rs.20 talktime using your unique code</p>
						<p class="section-title-text" lang="hi" >कैसे अपना युनिक कोड से रु.२० का टाक टाईम हासिल करे ?</p>
						<div class="centered line"></div>
					</div>
				</div>

				<div class="row section-content">

					<div class="col-full">
						
						<div class="steps">
							<h4>Enter your Prepaid Mobile details to recharge your phone. </h4>
							<h4 lang="hi" >अपना फोन रिचार्ज करने के लिये अपने प्रीपेड मोबाईल का विवरण एंटर करें  |</h4>
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/1.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/step1.png">
						</div>
					</div>

					<div class="col-full">
						<div class="steps">
							<h4>Pick coupons for the value of your recharge. </h4>
							<h4 lang="hi" > अपने रिचार्ज मूल्य का कूपन उठाएँ |</h4>
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/2.png">
						</div>
						<div class="steps-snapshot">
							<p class="starred">
								Please note this is optional and a handling charge of Rs.10 is applicable. You could CONTINUE 
								with or without coupon(s). </p>
							<p class="starred-hindi" lang="hi" > याद रखें कि यह ऑप्शनल कूपन है और इस  पर १० रूपए का हैंडलिंग शुल्क लागू होगा | 
							आप कूपन के साथ या उसके बिना आगे बढ़ सकते हैं | </p>
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/step2.png">
						</div>						

					</div>

					<div class="col-full">
						<div class="steps">
							<h4>Confirm if you want coupon(s). </h4>
							<h4 lang="hi" > कन्फर्म करें कि आपको कूपन चाहिए | </h4>
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/3.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/step-3_ver2.png">
						</div>
					</div>
					
					<div class="col-full">
						<div class="steps">
							<h4>If you are an existing user at FreeCharge, enter LOGIN details. </h4>
							<h4 lang="hi" > यदि आपके पास फ्रीचार्ज का अकाउंट पहले से मौजूद है तो लागिन विवरण भरें |</h4>
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/4.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/step4.png">
						</div>
					</div>

					<div class="col-full">
						<div class="steps">
							<h4>If you are new to FreeCharge, Sign Up. </h4>
							<h4 lang="hi" > यदि आप फ्रीचार्ज पर नए हैं तो साइन-अप करें | </h4>
					    	<img src="${mt:keyValue('imgprefix1')}/images/pepsi/5.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/step5.png">
						</div>
					</div>

					<div class="col-full">
						<div class="steps">
							<h4>Enter the unique 11-Digit Code mentioned under the label. </h4>
							<h4 lang="hi" > लेबल के नीचे दिया गया यूनिक ११ अंकों का कोड एंटर करें | </h4>
					    	<img src="${mt:keyValue('imgprefix1')}/images/pepsi/6.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/step6.png">
						</div>
					</div>

					<div class="col-full">
						<div class="steps">
							<h4>After you have successfully applied your Promocode, click CONTINUE. </h4>
							<h4 lang="hi" > अपना प्रोमो कोड सफलतापूर्वक अप्लाई करने के बाद <b>कंटिन्यू</b> पे क्लिक करें | </h4>
					    	<img src="${mt:keyValue('imgprefix1')}/images/pepsi/7.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/step-7.png">
						</div>
					</div>

					<div class="col-full">
						<div class="steps">
							<h4>Recharging for more than Rs.20? Pay the remaining amount. </h4>
							<h4 lang="hi" > यदि आप  रु.२० से अधिक का रिचार्ज कर रहे हैं तो शेष राशि अदा करें | </h4>
					    	<img src="${mt:keyValue('imgprefix1')}/images/pepsi/8.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/step-8.png">
						</div>
					</div>
					
					<div class="col-full">
						<div class="steps">
							<h4>That&#39;s it. Your Recharge is done! </h4>
							<h4 lang="hi" >आपका रिचार्ज सफल हुआ | </h4>
					    	<img src="${mt:keyValue('imgprefix1')}/images/pepsi/9.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/step-9.png">
							<p class="starred">*Promo code is for single use and can be redeemed by a unique mobile 
							number only once. </p>
							<p class="starred-hindi" lang="hi" >प्रोमो कोड एक बार प्रयोग के लिए ही है और यह एक मोबाइल नंबर  द्वारा केवल एक  बार ही प्रयोग 
							किया जा सकता है | </p>
						</div>
					</div>

					<div class="col-full">					
						<div class="get-started">
							<div>Okay, I understand. Take me to FreeCharge. <br>अब मुझे फ्रीचार्ज तक ले चलिए | </div>
							<a href="/"  target="_blank" 
							class="red" >Go <i class="icon-long-arrow-right"></i></a>
						</div>
					</div>

					<div class="col-full">
						<div class="separator"></div>
					</div>

				</div>
			</div>

		</section>
		<!-- End: Guide -->


		<!-- Start: FAQ -->
		<section id="faq" class="section">
			<div class="container">
				<div class="row">
					<div class="col-full">
						<h2 class="section-title">Frequently Asked Questions</h2>
						<p class="section-title-text">for redeeming pepsi voucher code</p>
						<div class="centered line"></div>
					</div>
				</div>
				<div class="row section-content">
					<div class="col-full">
						<ol class="faq-text">
							<li>
								<h2 class="question">
									What is &lsquo;Pepsi 60 Crore Tak Ka recharge&rsquo;?
								</h2>
								<p class="answer">
									The Pepsi promocode entitles you to avail Rs.20 off on your &lsquo;Total Payable Amount&rsquo; 
									when you recharge at www.freecharge.com
								</p>
							</li>
							<li>
								<h2 class="question">
									How can I get a Pepsi promocode?
								</h2>
								<p class="answer">
									It&#39;s simple. Just buy a Pepsi, Mountain Dew, Mirinda, 7UP (1 litre, 1.25 litre, 
									2 litre, 2.25 litre) or Slice (1.2 litre) bottle and look for a unique 11-digit code 
									under the label of your bottle.
								</p>
							</li>
							<li>
								<h2 class="question">
									How can I use a Pepsi promocode to recharge my number?
								</h2>
								<p class="answer">
									Again, easy! Just follow the following steps:
								</p>
								<ul class="answer">
									<li>
										Enter your mobile number, operator and recharge amount. You could also view the &lsquo;Popular Plans&rsquo;
										section to select the right denomination for your operator. Then click &lsquo;Proceed&rsquo;.
									</li>
									<li>
										Select coupons for the same value as your recharge (Note: This is optional. An additional charge of
										Rs.10 is levied if coupons are selected.) And click &lsquo;Continue&rsquo;.
									</li>
									<li>
										Enter your 11-digit Pepsi promocode and click &lsquo;Continue&rsquo;.
									</li>
									<li>
										Move on to make a payment for the remaining amount (if any).
									</li>
									<li>
										Your recharge is done! And you&#39;ve availed the discount of Rs.20.
									</li>
								</ul>
								<p class="answer">
									You could also visit www.freecharge.com/pepsi promotion for a detailed walkthrough on &lsquo;How to redeem&rsquo; the Pepsi promocode.
								</p>
							</li>
							<li>
								<h2 class="question">
									Is this offer valid for postpaid mobile numbers?
								</h2>
								<p class="answer">
									No, this offer is valid for prepaid mobile numbers only.
								</p>
							</li>
							<li>
								<h2 class="question">
									How much discount do I get on my recharge?
								</h2>
								<p class="answer">
									You get a discount of Rs.20 on the &lsquo;Total Payable Amount&rsquo;.
								</p>
							</li>
							<li>
								<h2 class="question">
									Can I recharge for Rs.20 only?
								</h2>
								<p class="answer">
									Yes, you can recharge for exactly Rs.20 (if it&#39;s a valid denomination for your operator) and pay nothing.
								</p>
							</li>
							<li>
								<h2 class="question">
									Can I also recharge for more than Rs.20?
								</h2>
								<p class="answer">
									Here&#39;s the thing – you can recharge for any amount up to Rs.1000. Just use the 11-digit code to get a
									discount of Rs.20 on the &lsquo;Total Payable Amount&rsquo;. For example: If you recharge for Rs.50 and select equal 

									value coupons too, your &lsquo;Total Payable Amount&rsquo; is Rs.60 (including the coupon handling charge of Rs.10). 

									Apply the code, avail Rs.20 off and pay Rs.40. Please make sure to check the &lsquo;Popular Plans&rsquo; section to 

									decide the right denomination for you.
								</p>
							</li>
							<li>
								<h2 class="question">
									Rs.20 is not a valid denomination for my mobile operator. How do I make the best use of the code?
								</h2>
								<p class="answer">
									You can recharge for more than Rs.20 too. You just need to pay the balance amount. Do visit our &lsquo;Popular

									Plans&rsquo; section on the homepage to select the right plan.
								</p>
							</li>
							<li>
								<h2 class="question">
									If my &lsquo;total payable amount&rsquo; is more than Rs.20, how do I pay for the remaining amount?
								</h2>
								<p class="answer">
									The remaining amount can be paid using one of these payment options – debit card, credit card, net 

									banking or cash card.
								</p>
							</li>
							<li>
								<h2 class="question">
									Does the Pepsi promocode also work for Special Recharges like Data/2G, 3G, SMS pack?
								</h2>
								<p class="answer">
									Yes.
								</p>
							</li>
							<li>
								<h2 class="question">
									Can I also use the Pepsi promocode for DTH and Data card recharge?
								</h2>
								<p class="answer">
									Sure. You can use the code for a recharge of
								</p>
								<ul class="answer">
									<li>
										Up to Rs.5000 for a DTH number
									</li>
									<li>
										Up to Rs.1500 for a data card
									</li>
								</ul>
							</li>
							<li>
								<h2 class="question">
									How many times can I use a Pepsi promocode?
								</h2>
								<p class="answer">
									The Pepsi promocode is for one-time use and can be redeemed/used only once by a user/mobile number.
								</p>
							</li>
							<li>
								<h2 class="question">
									While redeeming the Pepsi promocode, I see an error that says, &ldquo;This code has already been used.

									Please try another unique code.&rdquo; What do I do?
								</h2>
								<p class="answer">
									Well, looks like the promocode entered by you has already been used by another login Id or mobile

									number. If you have another unique 11-digit code, you can use that to avail the offer.
								</p>
							</li>
							<li>
								<h2 class="question">
									When I try using the Pepsi promocode, I get an error saying, &ldquo;A code can be used by a login Id only

									once&rdquo;. What&#39;s with that?
								</h2>
								<p class="answer">
									You see this message because the e-mail Id you&#39;ve used to log in with has already availed this offer.
								</p>
							</li>
							<li>
								<h2 class="question">
									I am trying to redeem the Pepsi promocode but I get an error saying, &ldquo;A code can be used by a Mobile

									Number only once.&rdquo; What&#39;s the scene?
								</h2>
								<p class="answer">
									This happens when the mobile number you&#39;ve entered has already availed this offer. Try entering a

									number that hasn&#39;t availed the offer yet.
								</p>
							</li>
							<li>
								<h2 class="question">
									I am trying to redeem the Pepsi promocode but I am getting an error message that says, &ldquo;This code has expired.&rdquo;
								</h2>
								<p class="answer">
									The Pepsi offer is valid till 31st January, 2014. This message is displayed only if you try to avail the codeafter the offer period.
								</p>
							</li>
							<li>
								<h2 class="question">
									Can I select equal value coupons during my recharge?
								</h2>
								<p class="answer">
									Of course, you can! After signing-in, enter your recharge details and click &lsquo;Proceed&rsquo; to unlock equal value

									coupons. You can then pick coupons you like for the value of your recharge, and continue to pay. Note: A 

									handling charge of Rs.10 will be applicable.
								</p>
							</li>
							<li>
								<h2 class="question">
									Is the Pepsi offer valid for International numbers?
								</h2>
								<p class="answer">
									Sorry, but this offer is valid only for Indian numbers.
								</p>
							</li>
							<li>
								<h2 class="question">
									Are all operators covered under this offer?
								</h2>
								<p class="answer">
									The Pepsi offer is valid for the following operators: Airtel, Aircel, Vodafone, BSNL, Tata Docomo, Idea, Tata

									Walky, Loop, MTNL Trump, Reliance CDMA, Reliance GSM, Tata Indicom, Uninor, MTS, Videocon, Virgin 

									CDMA and Virgin GSM.
								</p>
							</li>
							<li>
								<h2 class="question">
									If my payment fails, can I use the promocode again?
								</h2>
								<p class="answer">
									Yes!
								</p>
							</li>
							<li>
								<h2 class="question">
									What if my recharge fails? Can I use the promocode again?
								</h2>
								<p class="answer">
									Yes. However, if you get a message saying, &ldquo;Promocode blocked. Try in sometime&rsquo;, it is possible that

									your recharge is under process. In this scenario, we suggest you wait and try using the code again, if your 

									recharge fails.
								</p>
							</li>
						</ol>
					</div>
					
					<div class="col-full">					
						<div class="get-started">
							<div>Okay, I understand. Take me to FreeCharge. <br>अब मुझे फ्रीचार्ज तक ले चलिए | </div>
							<a href="/"  target="_blank" 
							class="red" >Go <i class="icon-long-arrow-right"></i></a>
						</div>
					</div>

					<div class="col-full">
						<div class="separator"></div>
					</div>

				</div>
			</div>

		</section>
		<!-- End: How to FAQ -->
		

		<!-- Start: T&C -->
		<section id="terms" class="section">
			<div class="container">
				<div class="row">
					<div class="col-full">
						<h2 class="section-title">Terms &amp; Conditions</h2>
						<p class="section-title-text">for participating in 60 Crore tak ka recharge</p>
						<div class="centered line"></div>
					</div>
				</div>

				<div class="row section-content">

					<div class="col-full">
						
						<ol class="sub-section-text">
							<li>
								<p>
									This Talktime Promotion (&ldquo;Offer&rdquo;) is being brought to by PepsiCo India Holdings Pvt. Limited (&ldquo;PIH&rdquo;) in association with FreeCharge.com.
								</p>
							</li>
							<li>
								<p>
									PepsiCo India Holdings Pvt. Ltd. (&ldquo;PIH&rdquo;) in association with FreeCharge.com is 
									organizing this consumer offer by the name &lsquo;Rs. 60 Crore tak ka recharge&rsquo; in respect 
									of some of its beverage products.
								</p>
							</li>
							<li>
								<p>
									This Offer is valid in India from 20th Sept, 2013 to 31st Jan, 2014 (both dates included) 
									(&lsquo;offer period&rsquo;) on purchase of Pepsi, Mountain Dew, Mirinda and 7UP (1 litre, 1.25 litre, 2 litre, 2.25 litre) 
									and Slice (1.2 litre), hereinafter referred to as &ldquo;Products&rdquo;.
								</p>
							</li>
							<li>
								<p>
									The offer is open to Indian residents except employees and the family members of Organizers, 
									their associate companies, advertising agencies and their auditors.
								</p>
							</li>
							<li>
								<p>
									Participation is optional.
								</p>
							</li>
							<li>
								<p>
									The campaign is only valid for the citizens of India, holding valid prepaid GSM / CDMA

									mobile phone connections of the following operators:
								</p>
								<ol type="a">
									<li>
										Airtel
									</li>
									<li>
										Aircel
									</li>
									<li>
										Vodafone
									</li>
									<li>
										BSNL
									</li>
									<li>
										Tata Docomo
									</li>
									<li>
										Idea
									</li>
									<li>
										Tata Walky
									</li>
									<li>
										Loop
									</li>
									<li>
										MTNL Trump
									</li>
									<li>
										Reliance CDMA
									</li>
									<li>
										Reliance GSM
									</li>
									<li>
										Tata Indicom
									</li>
									<li>
										Uninor
									</li>
									<li>
										MTS
									</li>
									<li>
										Videocon
									</li>
									<li>
										Virgin CDMA
									</li>
									<li>
										Virgin GSM
									</li>
								</ol>
							</li>
							<li>
								<p>
									The Offer is being conducted under the supervision of independent auditors.
								</p>
							</li>
							<li>
								<p>
									Look under the label and you could win a free talktime recharge of Rs. 20/-.
								</p>
							</li>
							<li>
								<p>
								To participate,
								</p>
								<ol type="a">
									<li>
										Visit and register on &lsquo;www.FreeCharge.com&rsquo; or &lsquo;www.FreeCharge.com/m&rsquo;.
									</li>
									<li>
										Enter your prepaid mobile number and insert the recharge value of Rs. 20/-
									</li>
									<li>
										Enter the unique promocode printed behind the label and get Rs. 20/- free
										talktime!
									</li>
								</ol>
							</li>
							<li>
								<p>
									A code can be used to recharge only once. A prepaid mobile no./user account is

									entitled to only one free talktime recharge of Rs. 20/- during the period of the Offer. All 

									subsequent attempts to recharge the same mobile number with a different code will be 

									rejected. 
								</p>
							</li>
							<li>
								<p>
									Participants already registered with FreeCharge can also use their existing credentials to

									avail of the Offer.
								</p>
							</li>
							<li>
								<p>
									In case the Participant enters a recharge amount higher than Rs.20/-, then the differential

									balance amount will have to be paid directly by the Participant using any of the online 

									payment options available on FreeCharge; and PIH will not be responsible to pay this differential 

									balance amount.
								</p>
							</li>
							<li>
								<p>
									PIH will print 3 Crore unique codes on the labels of the products. It is however clarified that this 
									is subject to general production losses and damage to labels.
								</p>
							</li>
							<li>
								<p>
									On direct payment of the applicable handling charges, FreeCharge may offer the Participant

									to select exciting coupons of value equal to the recharge amount. In case the Participant 

									avails such add ons from FreeCharge, PIH will not be responsible to pay charges for any 

									additional or other facility (ies) / offer(s) availed by the Participant. It is clarified that PIH &amp; Freecharge are responsible for
									giving talktime worth Rs. 20/-, in accordance with the present terms and
									conditions.
								</p>
							</li>
							<li>
								<p>
									The talktime credited into a customer&#39;s account is non-transferrable, non-refundable;
									and no cash payment will be made in lieu of this talktime. Actual
									talktime will vary as per the top-up plan of the respective operator and is subject
									to applicable taxes and levies.
								</p>
							</li>
							<li>
								<p>
									The code cannot be redeemed retrospectively i.e. after the expiry of the offer period. Any

                                    unused balance will not be refunded or credited when offer period has expired.
								</p>
							</li>
							<li>
								<p>
									The offer is not valid in conjunction with any other offer.
								</p>
							</li>
							<li>
								<p>
									Neither PIH nor FreeCharge will  be liable for any delay in crediting the talktime into the

                       				customers account while transacting on FreeCharge as the time taken for  crediting the talktime

                       				into the customer's account is dependent on the telecom operators of the prepaid mobile customer.
								</p>
							</li>
							<li>
								<p>
									PIH and FreeCharge shall not be responsible for downtime on services rendered by the telecom

									operators / provider that may occur for any reason whatsoever including without 

									limitation due to decisions and / or changes in regulations that are carried out by 

									TRAI , DoT or any other regulatory body.
								</p>
							</li>
							<li>
								<p>
									PIH and FreeCharge shall not be responsible for 

									downtime caused due to technical difficulties or downtime created at the telecom 

									operator / provider&#39;s end either due to failure of hardware equipment at the telecom 

									providers end, configuration issues at the end of the Telecom operator /provider&#39;s end, 

									network congestion or for any other reason whatsoever.PIH and FreeCharge will not be 

									responsible for any loss or damage of the code.
								</p>
							</li>
							<li>
								<p>
									All customers related queries for this promotion can be addressed to care@freecharge.com
								</p>
							</li>
							<li>
								<p>
									PIH reserves the right to update and/or amend these Terms and Conditions.
								</p>
							</li>
							<li>
								<p>
									PIH reserves the right to extend or terminate this Offer without prior notice.
								</p>
							</li>
							<li>
								<p>
									PIH and FreeCharge collectively reserve the right to refuse to credit the talktime into the

  									participants’ mobile account due to fraud or system security breaches.
								</p>
							</li>
							<li>
								<p>
									Decision of PIH will be final and binding with regard to promotion and prize and no correspondence will be entertained in this regard.
								</p>
							</li>
							<li>
								<p>
									At the request of PIH, winners may be required to participate in promotional activities 
									(such as publicity and photography), free of charge, and he/she consents to the PIH for using 
									their name, likeness, image and/or voice in the event the winner (including photograph, film 
									and/or recording of the same) in promotional material or in any media for an unlimited period 
									without remuneration for the purpose of promoting this promotion (including any outcome , and 
									promoting any products manufactured, distributed and/or supplied by PIH.
								</p>
							</li>
							<li>
								<p>
									Pepsi, Mirinda, Mountain Dew, 7UP and Slice are registered trademarks of PepsiCo Inc. USA.
								</p>
							</li>
							<li>
								<p>
									FreeCharge.com is a registered trademark of Accelyst Solutions Pvt Ltd (ASPL).
								</p>
							</li>
							<li>
								<p>
									The participant is bound by the terms and conditions imposed by FreeCharge.com on its

									website.
								</p>
							</li>
							<li>
								<p>
									No cash payment will be made in lieu of the prize.
								</p>
							</li>
							<li>
								<p>
									PIH accepts no responsibility for any tax implications that may arise from the prize 
									winning. Participants will have to bear incidental costs, if any, that may arise for 
									redemption of the prize.
								</p>
							</li>
							<li>
								<p>
									PIH shall not be responsible for any loss, injury or any other liability arising out 
									of the Promotion or due to participation by any person in the Promotion.
								</p>
							</li>
							<li>
								<p>
									PIH shall not be liable for any loss or damage due to Act of God, Governmental actions, 
									other force majeure circumstances and shall not be liable to pay any amount as compensation 
									or otherwise for any such loss.
								</p>
							</li>
							<li>
								<p>
									PIH reserves its right to change/modify/or withdraw the promotion without any prior 
									notice of the same at its sole discretion. PIH also reserve the right to modify the terms 
									and conditions without any prior notice.
								</p>
							</li>
							<li>
								<p>
									All disputes relating to this Promotion shall be subject to the exclusive jurisdiction 
									of Courts at New Delhi only.
								</p>
							</li>
							
						</ol>
					</div>


					<div class="col-full">					
						<div class="get-started">
							<div>Okay, I understand. Take me to FreeCharge. <br>अब मुझे फ्रीचार्ज तक ले चलिए | </div>
							<a href="/"  target="_blank" 
							class="red" >Go <i class="icon-long-arrow-right"></i></a>
						</div>
					</div>


					<div class="col-full">
						<div class="separator"></div>
					</div>

				</div>
			</div>
		</section>
		<!-- End: Work Experience -->

		<!-- Loading: jQuery and custom script -->
		<script src="${mt:keyValue("jsprefix2")}/pepsi/jquery-1.10.2.min.js?v=${mt:keyValue("version.no")}"></script>

		<!-- Loading: jQuery Sticky -->
		<script src="${mt:keyValue("jsprefix2")}/pepsi/jquery.sticky.js?v=${mt:keyValue("version.no")}"></script>


		<script src="${mt:keyValue("jsprefix2")}/pepsi/jquery.scrollto.min.js?v=${mt:keyValue("version.no")}"></script>
		<script src="${mt:keyValue("jsprefix2")}/pepsi/jquery.nav.js?v=${mt:keyValue("version.no")}"></script>

		<!--[if lt IE 9]>
		<script src="${mt:keyValue("jsprefix2")}/pepsi/excanvas.js?v=${mt:keyValue("version.no")}"></script>
		<![endif]-->

		<!-- Loading: Custom scripting -->
		<script src="${mt:keyValue("jsprefix2")}/pepsi/custom.curriculum.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>

		
		<script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-12939857-1']);
            _gaq.push(['_setDomainName', 'freecharge.in']);
            _gaq.push(['_trackPageview']);
            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>

		
	</body>
</html>