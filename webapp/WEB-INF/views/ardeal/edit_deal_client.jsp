<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<div class="well">
    <fieldset>
        <legend>Save Deal client</legend>
        <form:form action="/admin/clients/save.htm" method="post" commandName="client" cssClass="form form-horizontal">
            <form:hidden path="id"/>
            <div class="control-group">
                <label class="control-label">Name</label>
                <div class="controls">
                    <form:input path="name" id="name" cssClass="input-xlarge"/>
                    <form:errors path="name" cssStyle="color: red"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Deal</label>
                <div class="controls">
                    <form:select path="dealId" cssClass="input-xlarge">
                        <form:options items="${deals}" itemLabel="name" itemValue="id"/>
                    </form:select>
                </div>
            </div>
            <div class="form-actions">
                <input type="submit" value="Save" class="btn btn-success">
            </div>
        </form:form>
    </fieldset>

</div>