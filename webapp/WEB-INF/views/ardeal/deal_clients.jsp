<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<div class="span12">
    <fieldset>
        <legend>All Deal Clients <a href="/admin/clients/edit">Add Deal Client</a> </legend>
        <table class="table table-bordered table-strpped">
            <tr>
                <th>&nbsp;</th>
                <th>ID</th>
                <th>Name</th>
                <th>Deal ID</th>
            </tr>
            <c:forEach items="${clients}" var="client">
                <tr>
                    <td><a href="/admin/clients/edit?id=${client.id}">edit</a> </td>
                    <td>${client.id}</td>
                    <td>${client.name}</td>
                    <td>${client.dealId}</td>
                </tr>
            </c:forEach>
        </table>
    </fieldset>
</div>