<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<style media="screen" type="text/css">
.table
{
   display:table;
}

.table-row
{
   display:table-row;
   margin: 10px;
}

.table-cell
{
   display:table-cell;
   padding: 2px 12px;
}

ul li {margin-top: 5px; list-style: none; padding-left: 15px;}
ul {margin: 0; padding: 0; list-style: none;}

</style>

<h1><c:choose><c:when test="${edit}">Edit</c:when><c:otherwise>Add</c:otherwise></c:choose> AR Reward</h1>
<div style="margin: 10px">
    <form:form action="/admin/ardeal/reward/save.htm" method="POST" commandName="arRewardEntity" id="ardeal_form" cssClass="table">
        <form:hidden path="id"/>
        <div class="table-row">
            <div class="table-cell">
                <label for="name">Reward name</label>
                <form:input path="name" id="name"/>
                <form:errors path="name" cssStyle="color: red"/>
            </div>
        </div>
        <div class="table-row">
            <div class="table-cell">
                <label for="name">
                    <form:checkbox path="multiple" id="multiple"/>
                    Multiple Rewards Allowed?
                </label>
                <form:errors path="name" cssStyle="color: red"/>
            </div>
        </div>
        <div class="table-row">
            <div class="table-cell">
                <label for="rewardTypeId">Reward Type</label>
                <form:select path="fkARRewardTypeId" id="rewardTypeId">
                    <form:options items="${rewardTypes}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
        </div>
        <div class="table-row">
            <div class="table-cell">
                <label for="rewardTypeId">Reward Processing Type</label>
                <form:select path="rewardTypeEnum" id="rewardProcessTypeId">
                    <form:options items="${rewardEnumType}" itemValue="rewardType"/>
                </form:select>
            </div>
        </div>
        <div class="table-row">
            <div class="table-cell">
                <label for="name">Email Subject</label>
                <form:input path="emailSubject" id="emailSubject"/>
                <form:errors path="emailSubject" cssStyle="color: red"/>
            </div>
        </div>
        <div class="control-group">
            <label for="email_template" class="control-label">Email Template (Add __username__ , __amount__ , __metadata__ (if present). These will be replaced by actual username, amount and metadata.)</label>
            <div class="controls">
                <form:textarea path="emailTemplate" id="email_template" cols="100" rows="30" cssStyle="width: auto"></form:textarea>
            </div>
        </div>
        <div class="table-row">
            <div class="table-cell">
                <label for="params_list">Params</label>
                <div class="controls">
                    <div class="row-fluid" id="params_list">
                        <a href="#" id="new_param" class="btn btn-primary btn-small"><i class="icon-plus-sign"></i> </a>
                    </div>
                </div>
            </div>
        </div>
        <form:hidden path="params" id="params"/>
        <div>
            <input type="submit" value="Save">
        </div>
    </form:form>
</div>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/admin/ardeal_params.js?v=${mt:keyValue("version.no")}"></script>