<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<head>
    <link href="/content/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>
<div class="container" style="padding-bottom: 10px;">
    <div class="alert" style='display: <c:if test="${empty param.message}">none</c:if>'>
        <c:if test="${not empty param.message}">
            <c:out value="${param.message}"/>
        </c:if>
        <a class="close" href="#">&times;</a>
    </div>

    <div class="span12">
        <h2>ARDeals</h2>
        <a class="btn btn-info" href="/admin/ardeal/edit.htm" title="Add New Deal">Add Deal</a>&nbsp;&nbsp;
        <a href="/admin/ardeal/import.htm" class="btn btn-large">Import a Deal</a>
        <a class="pull-right btn btn-inverse" href="/admin/ardeal/list_templates.htm" title="Manage Templates" style="padding-right: 10px;">Manage Templates</a>
        <table class="table table-bordered table-striped">
            <tr>
                <th><strong>Actions</strong></th>
                <th><strong>Id</strong></th>
                <th><strong>Name</strong></th>
                <th><strong>ActionId</strong></th>
                <th><strong>RewardId</strong></th>
                <th><strong>ConditionId</strong></th>
                <th><strong>IsEnabled</strong></th>
                <th><strong>Hookpoint</strong></th>
                <th>Channel</th>
                <th><strong>Template ID</strong></th>
                <th><strong>Closed User Group</strong></p>
                <th><strong>Priority</strong></th>
            </tr>
            <c:forEach var="ardeal" items="${ARDEALS}">
                <tr>
                    <td><a href="/admin/ardeal/edit?id=${ardeal.id}">edit</a>&nbsp;|&nbsp;
                        <a href="/admin/ardeal/export?id=${ardeal.id}">export</a>
                    </td>
                    <td>${ardeal.id}</td>
                    <td>${ardeal.name}</td>
                    <td>${ardeal.fkActionId}</td>
                    <td>${ardeal.fkRewardId}</td>
                    <td>${ardeal.fkConditionId}</td>
                    <td>
                            ${ardeal.enabled}
                        <a href="#" onClick='changeStatus("/admin/ardeal/update/status", ${ardeal.id}, ${ardeal.enabled})'>Enable/Disable</a>
                    </td>
                    <td >${ardeal.hookpoint}</td>
                    <td>${ardeal.channelStr}</td>
                    <td >${ardeal.templateId}</td>
                    <td >${ardeal.isPrivUserCond}</td>
                    <td>
                        <c:if test="${ardeal.enabled}">
                            <input type="text" class="input-mini" value="${ardeal.priority}" data-id="${ardeal.id}">
                        </c:if>
                    </td>

                </tr>
            </c:forEach>
            <tr>
                <td colspan="10"><a href="#" id="save_priority" class="btn btn-inverse pull-right">Save Priority</a></td>
            </tr>
        </table>
    </div>
    <div class="span12">
        <h2>ARActions</h2>
        <a class="pull-right" href="/admin/ardeal/action/edit">Add AR Action</a>
        <table class="table table-bordered table-striped">
            <tr>
                <th><strong>Actions</strong></th>
                <th><strong>id</strong></th>
                <th><strong>name</strong></th>
                <th><strong>actionTypeId</strong></th>
                <th><strong>params</strong></th>
            </tr>
            <c:forEach var="action" items="${ARACTIONS}">
                <tr>
                    <td><a href="/admin/ardeal/action/edit?id=${action.id}">edit</a></td>
                    <td>${action.id}</td>
                    <td>${action.name}</td>
                    <td>${action.fkARActionTypeId}</td>
                    <td>${fn:escapeXml(action.params)}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div class="span12">
        <h2>ARRewards</h2> <a href="/admin/ardeal/reward/edit.htm" class="pull-right">Add AR Reward</a>
        <table class="table table-bordered table-striped">
            <tr>
                <th><strong>Actions</strong></th>
                <th><strong>id</strong></th>
                <th><strong>name</strong></th>
                <th><strong>rewardTypeId</strong></th>
                <th><strong>Reward Processing Type</strong></th>
                <th><strong>Multiple Rewards Allowed</strong></th>
                <th><strong>params</strong></th>
            </tr>
            <c:forEach var="reward" items="${ARREWARDS}">
                <tr>
                    <td><a href="/admin/ardeal/reward/edit?id=${reward.id}">edit</a></td>
                    <td>${reward.id}</td>
                    <td>${reward.name}</td>
                    <td>${reward.fkARRewardTypeId}</td>
                    <td>${reward.rewardTypeEnum}</td>
                    <td>${reward.multiple}</td>
                    <td>${reward.params}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div class="span12">
        <h2>ARConditions</h2>
        <a href="/admin/ardeal/condition/edit.htm" class="pull-right">Add AR Condition</a>
        <table class="table table-bordered table-striped">
            <tr>
                <th><strong>Actions</strong></th>
                <th><strong>id</strong></th>
                <th><strong>name</strong></th>
                <th><strong>conditionTypeId</strong></th>
                <th><strong>Context</strong></th>
                <th><strong>params</strong></th>
            </tr>
            <c:forEach var="condition" items="${ARCONDITIONS}">
                <tr>
                    <td><a href="/admin/ardeal/condition/edit?id=${condition.id}">edit</a> </td>
                    <td>${condition.id}</td>
                    <td>${condition.name}</td>
                    <td>${condition.fkARConditionTypeId}</td>
                    <td>${condition.context}</td>
                    <td>${condition.params}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div class="span12">
        <div class="row">
            <div class="span4">
                <a href="/admin/ardeal/action/actionTypes">Action Types</a>
            </div>
            <div class="span8">
                Number of cached enabled ARDeals: ${NUM_CACHED_ENABLEDARDEALS}
                <a href="#" onClick='doPost("/admin/ardeal/flushCache")'>Flush Cache</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  function doPost(url) {
    $.post(url, {}, function(data) {
        location.reload();
    }, 'json');
  }
  function changeStatus(url, id, status) {
	    $.post(url, {"id":id, "status":status}, function(data) {
	        location.reload();
	    }, 'json');
  }
  $(document).ready(function(){
      $('#save_priority').on('click', function(ev){
          ev.preventDefault();
          var data = {};
          $('input.input-mini').each(function(){
              data[$(this).data('id')] = $(this).val();
          });
          $.ajax('/admin/ardeal/ordering.htm', {
              data: {'order': JSON.stringify(data)},
              type: 'POST',
              dataType: 'json',
              success: function(result){
                  $('div.alert').addClass("alert-success").html(result.message+'<a class="close" href="#">&times;</a>').show();
                  attachClose();
              },
              error: function(xhr){
                  $('div.alert').addClass("alert-error").html($.parseJSON(xhr.responseText).message+'<a class="close" href="#">&times;</a>').show();
                  attachClose();
              }
          })
      });
  });
    function attachClose(){
        $('a.close').on('click', function(ev){
            ev.preventDefault();
            $(ev.currentTarget).parent().remove();
        });
    }
</script>