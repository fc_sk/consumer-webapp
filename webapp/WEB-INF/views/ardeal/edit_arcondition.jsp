<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<style>
    ul li {margin-top: 5px; list-style: none; padding-left: 15px;}
    ul {margin: 0; padding: 0; list-style: none;}

</style>
<h1><c:choose><c:when test="${edit}">Edit</c:when><c:otherwise>Add</c:otherwise></c:choose> AR Condition</h1>
<div style="margin: 10px">
    <form:form action="/admin/ardeal/condition/save.htm" method="POST" commandName="arConditionEntity" id="ardeal_form" cssClass="form-horizontal">
        <form:hidden path="id"/>
        <div class="control-group">
            <label class="control-label" for="name">Condition name</label>
            <div class="controls">
                <form:input path="name" id="name"/>
                <form:errors path="name" cssStyle="color: red"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="conditionTypeId">Condition Type</label>
            <div class="controls">
                <form:select path="fkARConditionTypeId" id="conditionTypeId">
                    <form:options items="${conditionTypes}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
        </div>
         <div class="control-group">
            <label for="context" class="control-label">Context</label>
            <div class="controls">
                <form:select path="context" >
                    <form:options items="${context}" itemLabel="missionType" />
                </form:select>
            </div>
        </div>
        <p id="params_hint"><strong></strong></p>
        <div class="control-group">
            <label for="params_list" class="control-label">Params</label>
            <div class="controls">
                <div class="row-fluid" id="params_list">
                    <a href="#" id="new_param" class="btn btn-primary btn-small"><i class="icon-plus-sign"></i> </a>
                </div>
            </div>
        </div>
        <form:hidden path="params" id="params"/>
        <div class="form-actions">
            <input type="submit" value="Save">
        </div>
    </form:form>
</div>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/admin/ardeal_params.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">
    var arCs = $.parseJSON("${mt:escapeJs(jsConditionTypes)}");
    var $hint = $('p#params_hint strong');
    var $cond = $('#conditionTypeId');
    $hint.html(arCs[$cond.val()].description);
    $(document).ready(function(){
        $cond.on('change', function(ev){
            if(arCs[$(this).val()].description !=null){
                $hint.html(arCs[$(this).val()].description);
            }else {
                $hint.html("");
            }
        });
    });
</script> 