<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<div class="container">
    <div class="row-fluid">
        <div class="span12">
            <form class="form-horizontal" enctype="multipart/form-data" action="/admin/ardeal/do_import.htm" method="post">
                <div class="control-group">
                    <label class="control-label">Json File</label>
                    <div class="controls">
                        <input type="file" name="dealFile" class="btn btn-large">
                    </div>
                    <input type="submit" class="btn btn-large btn-success">
                </div>
            </form>
        </div>
    </div>
</div>