<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<style media="screen" type="text/css">
.table
{
   display:table;
}

.table-row
{
   display:table-row;
}

.table-cell
{
   display:table-cell;
   padding: 2px 12px;
}
</style>

<h2>ARActionTypes</h2>
<div class="table">
    <div class="table-row">
        <div class="table-cell"><strong>id</strong></div>
        <div class="table-cell"><strong>name</strong></div>
        <div class="table-cell"><strong>classIdent</strong></div>
        <div class="table-cell"><strong>---</strong></div>
    </div>               
    <c:forEach var="actionType" items="${ARACTIONTYPES}">
    <div class="table-row">    
        <div class="table-cell">${actionType.id}</div>
        <div class="table-cell">${actionType.name}</div>
        <div class="table-cell">${actionType.classIdent}</div>
        <div class="table-cell"><a href="/admin/ardeal/action/${actionType.name}/configure">Configure actions</a></div>
    </div>
    </c:forEach>
</div>
