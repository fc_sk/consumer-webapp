<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<div class="hero-unit">
    <c:choose>
        <c:when test="${status == 'success'}">
            <div class="alert alert-success">
                    ${message}
            </div>
        </c:when>
        <c:otherwise>
            <div class="alert alert-danger">
                    ${message}
            </div>
        </c:otherwise>
    </c:choose>

    <h4>Process Reward for UniqueId</h4>
    <form class="form-horizontal" method="post" action="">
        <div class="control-group">
            <label class="control-label">Enter UniqueId: </label>
            <div class="controls">
                <input type="text" name="uid" value="${uid}">
                <input type="submit" class="btn btn-inverse" value="Give Reward">
            </div>
        </div>
    </form>
</div>