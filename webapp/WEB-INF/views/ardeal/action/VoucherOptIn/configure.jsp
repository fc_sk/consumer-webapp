<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<div class="upsell-setting">
    <h1>Configure VoucherOptIn Actions</h1>
    <c:if test="${not empty param.message}">
        <div style="background-color:yellow;padding:10px;"><c:out value="${param.message}"/></div>
    </c:if>

    <div class="flex-layout clearfix">
        <div class="one">
            <div class="left-content">
               
            </div>
        </div>
        
        <div class="two">
            <div class="mid-content">
                <h2>Upload Voucher codes</h2>
                <form name="uploadform" id="uploadform" action="/admin/ardeal/action/VoucherOptIn/doupload" method="POST" enctype="multipart/form-data" cssClass="form" >
                    <select name="action_id">
                    	<c:forEach var="item" items="${ATTR_ARACTION_ENTITIES}">
		                    <option value="${item.id}">${item.name}</option>
		                </c:forEach>
                    </select>
                    
                   <br>
				   <b>Upload voucher code file</b><spring:message code="csv.file" text="#csv.file#"/><br>
				   <input type="file" class="input"  id="voucher_codes" name="voucher_codes" ><br><br>
				   <input type="submit"  title="Submit" value="Submit" class="button" id="butsubmit" name="addbut">
				   
                </form>
            </div>
        </div>
    </div>
</div>
