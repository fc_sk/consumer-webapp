<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<head>
    <link href="/content/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>
<div class="container" style="padding-bottom: 10px;">
	<div class="span12">
		<h2>ARDeals-Cashback</h2>
        Upload a file in csv format(comma separated) with email and ardealId.
		<div class="container">
		    <div class="row-fluid">
		        <div class="span12">
		            <form class="form-horizontal" enctype="multipart/form-data"
						  action="/admin/ardeal/cashback/do_import.htm" method="post"
						  onsubmit="submitFile.disabled = true; return true;">
		                <div class="control-group">
		                    <label class="control-label">CSV File</label>
		                    <div class="controls">
		                        <input type="file" name="cashbackFile" class="btn btn-large">
		                    </div>
		                    <br><br>
		                    <input type="submit" class="btn btn-large btn-success" name="submitFile">
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
	</div>

<c:if test="${not empty processedRows}">
<h3> Processed Rows</h3>
	<table class="table table-striped">
	     <thead>
	       <tr>
	         <th>UniqueId</th>
	         <th>Reward Amount</th>
	         <th>Message</th>
	         <th>EmailId</th>
	         <th>Previous Balance</th>
	         <th>New Balance</th>
	       </tr>
	     </thead>
	     <tbody>
            <c:forEach var="row" items="${processedRows}">
		       <tr>
                   <c:forEach var="v" items="${row}">
                       	<td>${v}</td>
                   </c:forEach>
		        </tr>         	
            </c:forEach>		    
	     </tbody>
	</table>
</c:if>

<c:if test="${not empty UNPROCESSED_RECORD}">
<h3> Unprocessed records </h3>
	<table class="table table-striped">
	     <thead>
	       <tr>
	         <th>UniqueId</th>
	         <th>Reward Amount</th>
	         <th>Message</th>
	         <th>EmailId</th>
	         <th>Previous Balance</th>
	         <th>New Balance</th>
	       </tr>
	     </thead>
	     <tbody>
	        <c:forEach var="row" items="${UNPROCESSED_RECORD}">
				<tr>
                   <c:forEach var="v" items="${row}">
                       	<td>${v}</td>
                   </c:forEach>
			    </tr>         	
            </c:forEach>
	     </tbody>
	</table>
</c:if>
</div>