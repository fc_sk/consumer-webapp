<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<head>
    <link href="/content/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>
<div class="container" style="padding-bottom: 10px;">
    <div class="alert" style='display: none'>

    </div>
    <div class="row-fluid">
        <div class="span12" id="edit_template" style="display: none">
            <fieldset>
                <legend>Add/Edit Template</legend>
                <form class="form-horizontal">
                    <input type="hidden" name="id" id="tem_id">
                    <div class="control-group">
                        <label class="control-label">Template Name</label>
                        <div class="controls">
                            <input type="text" class="input-large" maxlength="25" name="name" id="name">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Content</label>
                        <div class="controls">
                            <textarea rows="20" cols="80" class="span12" name="content" id="content"></textarea>
                        </div>
                    </div>
                    <div class="controls">
                        <a href="#" class="btn btn-success" id="save_template">Save</a>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
    <div class="span12">
        <h2>Ardeal Templates</h2>
        <a class="pull-right btn btn-info edit-template" href="#" title="Add New Template">Add Template</a>
        <table class="table table-bordered table-striped" id="templates-tab">
            <tr>
                <th><strong>Actions</strong></th>
                <th><strong>Id</strong></th>
                <th><strong>Name</strong></th>
            </tr>
            <c:forEach var="template" items="${templates}">
                <tr>
                    <td><a href="#" data-id="${template.id}" class="edit-template">edit</a></td>
                    <td>${template.id}</td>
                    <td id="name_${template.id}">${template.name}</td>
                    <td><div id="content_${template.id}" style="display: none">${template.content}</div></td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <script type="text/javascript">
        var $editTemplate = $('#edit_template');
        $('.edit-template').on('click', function(ev){
            ev.preventDefault();
            $editTemplate.show();
            var tId = $(ev.currentTarget).data('id');
            if(tId !=null && tId!=''){
                $('#tem_id').val(tId);
                $('#name').val($('#name_'+tId).html());
                $('#content').val($('#content_'+tId).html());
            }
        });
        $('#save_template').on('click', function(ev){
            ev.preventDefault();
            var data = {};
            $('.form-horizontal').find(':input').each(function(){
                data[$(this).attr('name')] = $(this).val();
                $(this).val('');
            });
            $editTemplate.hide();
            $.ajax({
                url: '/admin/ardeal/save_template',
                data: data,
                dataType: 'json',
                type: 'post',
                success: function(result) {
                    $('.alert').addClass('alert-success').html(result.message+'<a class="close" href="#">&times;</a>').show();
                    window.location.reload();
                },
                error: function(xhr){
                    console.log($.parseJSON(xhr.responseText));
                }
            });
        });

    </script>
</div>