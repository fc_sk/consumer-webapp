<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<style>
    ul li {margin-top: 5px; list-style: none; padding-left: 15px;}
    ul {margin: 0; padding: 0; list-style: none;}
</style>
<div class="well">
    <h2>Giudelines</h2>
    <p>
        Configure your deal here.
        A deal may have different configuration options based on what action type deal has. eg. FormArActionType, PromoOptinActionType etc.
        Please check FreechargeUserSurvey for form based mission and bank bazaar, reward me for promooptin type.
        Basic user info will be automatically populated.
    </p>
    <h3><c:choose><c:when test="${edit}">Edit</c:when><c:otherwise>Add</c:otherwise></c:choose> AR Deal</h3>
    <form:form action="/admin/ardeal/save.htm" method="POST" commandName="arDealEntity" id="ardeal_form" cssClass="form form-horizontal">
        <form:hidden path="id"/>
        <div class="control-group">
            <label for="name" class="control-label">Deal name</label>
            <div class="controls">
                <form:input path="name" id="name"/>
                <form:errors path="name" cssStyle="color: red"/>
            </div>
        </div>
        <div class="control-group">
            <label for="actionId" class="control-label">Action</label>
            <div class="controls">
                <form:select path="fkActionId" id="actionId">
                    <form:options items="${actions}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
        </div>
        <div class="control-group">
            <label for="rewardId" class="control-label">Reward</label>
            <div class="controls">
                <form:select path="fkRewardId" id="rewardId">
                    <form:options items="${rewards}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
        </div>
        <div class="control-group">
            <label for="conditionId" class="control-label">Condition</label>
            <div class="controls">
                <form:select path="fkConditionId" id="conditionId">
                    <form:options items="${conditions}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
        </div>
        <div class="control-group">
            <label for="template" class="control-label">Template</label>
            <div class="controls">
                <form:select path="templateId" id="template">
                    <form:options items="${templates}" itemLabel="name" itemValue="id"/>
                </form:select>
                <form:errors path="theme" cssStyle="color: red"/>
            </div>
        </div>
        <div class="control-group">
            <label for="channel" class="control-label">Channel</label>
            <div class="controls">
                <form:select path="channel" id="channel">
                    <form:options items="${channels}" itemLabel="name" itemValue="id"/>
                </form:select>
                <form:errors path="channel" cssStyle="color: red"/>
            </div>
        </div>
        <div class="control-group">
            <label for="missionType" class="control-label">Mission Type</label>
            <div class="controls">
                <form:select path="missionType" id="missionType">
                    <form:options items="${missionTypes}" itemLabel="name" itemValue="id"/>
                </form:select>
                <form:errors path="missionType" cssStyle="color: red"/>
            </div>
        </div>
        <div class="control-group">
            <label for="enabled" class="control-label">
                <form:checkbox path="passive" id="passive"/> Is Passive
            </label>
        </div>
        <div class="control-group">
            <label for="css_theme" class="control-label">Css Theme</label>
            <div class="controls">
                <form:select path="cssTheme" id="css_theme"></form:select>
            </div>
        </div>
        <div class="control-group">
            <label for="hookPoint" class="control-label">HookPoint</label>
            <div class="controls">
                <form:input path="hookpoint" id="hookPoint"/>
                <form:errors path="hookpoint" cssStyle="color: red"/>
            </div>
        </div>
        <div class="control-group">
            <label for="enabled" class="control-label">
                <form:checkbox path="enabled" id="enabled"/> Enabled
            </label>
        </div>
        <div class="control-group">
            <label for="isPrivUserCond" class="control-label">
                <form:checkbox path="isPrivUserCond" id="isPrivUserCond"/> Privilege User Group
            </label>
        </div>
        <div class="control-group">
            <label for="params_list" class="control-label">Params</label>
            <div class="controls">
                <div class="row-fluid" id="params_list">
                    <a href="#" id="new_param" class="btn btn-primary btn-small"><i class="icon-plus-sign"></i> </a>
                </div>
            </div>
        </div>
        <form:hidden path="params" id="params"/>
        <div class="form-actions">
            <input type="submit" class="btn btn-info" value="Save">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" id="preview_deal">Preview AR Deal</a>
        </div>
    </form:form>
</div>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/admin/ardeal_params.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/admin/ard-themes.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">
    var _ar_actions = $.parseJSON("${mt:escapeJs(jsActions)}");
    var _ar_rewards = $.parseJSON("${mt:escapeJs(jsRewards)}");

    $(document).ready(function(){
        var cssTheme = '${arDealEntity.cssTheme}';
        var cssOptions = '';
        for(var t in Themes){
            if(t == cssTheme){
                cssOptions+='<option value="'+t+'" selected="selected">'+t+'</option>';
            }else {
                cssOptions+='<option value="'+t+'">'+t+'</option>';
            }
        }
        $('#css_theme').html(cssOptions);

        $('a#preview_deal').on('click', function(ev){
            ev.preventDefault();

            var dealParams = '';
            $('input[id^=k_]').each(function(){
                var key = $(this).val();
                if(key!=null && key.trim().length>0){
                    var eid = $(this).attr('id').substr(2);
                    dealParams+="&"+key+"="+$('input#v_'+eid).val();
                }
            });
            if(dealParams.length>0){
                dealParams = dealParams.substr(1);
            }
            var actId = $('#actionId').val();
            var rewId = $('#rewardId').val();

            var data = {params: dealParams, fkActionId: actId, fkConditionId: $('#conditionId').val(),
                fkRewardId:rewId , hookpoint: $('#hookPoint').val(), name: $('#name').val(), templateId: $('#template').val(),
                cssTheme:$('#css_theme').val()};

            $.ajax('/admin/ardeal/preview.htm', {
                data: data,
                type: 'POST',
                dataType: 'json',
                success: function(res){
                    var fw = 600;
                    var left = (window.screen.width-fw)/2;
                    var top = (window.screen.height-200)/2;
                    $('body').append('<div id="ard-container" style="visibility: visible; position: absolute; margin: auto;' +
                            ' z-index: 99999; background-color: #efefef; border: 2px solid #ccd9d9; width: '+fw+'px; top: '+top+'px; left: '+
                            left+'px; display: block; background-position: initial initial; background-repeat: initial ' +
                            'initial;"><a href="#" class="close">X</a><iframe id="ardeal-frame" style="width: 100%; ' +
                            'background-color: transparent;height:auto;" scrolling="no"></iframe> </div>');
                    var iframe = document.getElementById('ardeal-frame');
                    iframe.contentWindow.document.write(res.template);
                    setTimeout(function(){
                        $(iframe).css('height', $(iframe.contentWindow.document).height()+'px');
                    }, 1);

                    $('a.close').on('click', function(ev){
                        ev.preventDefault();
                        $('div#ard-container').remove();
                    });
                },
                error: function(xhr){
                    console.log(xhr.responseText);
                }
            });
        });
    });
</script>