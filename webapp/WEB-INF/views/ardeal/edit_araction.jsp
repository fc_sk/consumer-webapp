<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<style>
    ul li {margin-top: 5px; list-style: none; padding-left: 15px;}
    ul {margin: 0; padding: 0; list-style: none;}
</style>

<div class="well">
    <h1><c:choose><c:when test="${edit}">Edit</c:when><c:otherwise>Add</c:otherwise></c:choose> AR Action</h1>
    <form:form action="/admin/ardeal/action/save.htm" method="POST" commandName="arActionEntity" id="ardeal_form" cssClass="form form-horizontal">
        <form:hidden path="id"/>
        <div class="control-group">
            <label for="name" class="control-label">Action name</label>
            <div class="controls">
                <form:input path="name" id="name" cssClass="input-xxlarge"/>
                <form:errors path="name" cssStyle="color: red"/>
            </div>
        </div>
        <div class="control-group">
            <label for="actionTypeId" class="control-label">Action Type</label>
            <div class="controls">
                <form:select path="fkARActionTypeId" id="actionTypeId">
                    <form:options items="${actionTypes}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
        </div>
        <div class="control-group">
            <label for="email_template" class="control-label">Email Template (Add __user__ and __voucher_code__ (if present). These will be replaced by actual user and voucher code.)</label>
            <div class="controls">
                <form:textarea path="emailTemplate" id="email_template" cols="100" rows="30" cssStyle="width: auto"></form:textarea>
            </div>
        </div>
        <div class="control-group">
            <label for="params_list" class="control-label">Params (heading, headingsupport, text, image, emailSubject)</label>
            <div class="controls">
                <div class="row-fluid" id="params_list">
                    <a href="#" id="new_param" class="btn btn-primary btn-small"><i class="icon-plus-sign"></i> </a>
                </div>
            </div>
        </div>
        <form:hidden path="params" id="params"/>
        <div class="form-actions">
            <input type="submit" value="Save" class="btn btn-primary">
        </div>
    </form:form>
</div>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/admin/ardeal_params.js?v=${mt:keyValue("version.no")}"></script>
