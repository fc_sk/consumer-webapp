<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<p class="expiry-message" >This deal has expired!</p>
<div class="secretcrush-banner" ></div>
<div class="container moonbox" >
	<div class="secretcrush-promocodeBanner" ><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/scPromoCodeBanner.png" alt="promo code - &ldquo;secretcrush&rdquo;"  width="960" height="87" /></div>
	<div class="secretcrush-wrapper" >
    	<h1>FreeCharge presents Secret Crush contest :</h1>
        <h3>How to participate in &ldquo;Secret Crush&rdquo; Contest?</h3>
        <ol>
        	<li>Recharge on FreeCharge and get your order ID.</li>
            <li>Enter the promo code <b>&ldquo;secretcrush&rdquo;</b>.</li>
            <li>Enter the details about your secret crush and your personalized message for her/him.  On Valentine&#39;s Day, we will send the e-card with your &ldquo;Dil ki Baat&rdquo; to your Secret Crush! Please see the T&amp;C to submit a valid entry. </li>
        </ol>
 		<h3>Terms &amp; Conditions:</h3>	
        <ol>
        	<li>By submiting the form you confirm that you agree to the terms and conditions. </li>
            <li>The order ID should correspond to recharge done on www.freecharge.in after the contest began on 8/2/2013.</li>
            <li>One order ID is eligible for only one entry. Though a user can fill multiple times.</li>
            <li>All messages will be scanned by our quality control and audit team.</li>
            <li>If required, we may be tracking the location of the participant via IP addresses, mobile and WI-FI triangulation. </li>
            <li>FreeCharge shall reserve the rights to make all the decisions related to the contest.</li>
            <li>If we receive a written complaint from anybody, we will take strict appropriate action against the miscreant.</li>
        </ol>
 		<%-- <iframe src="https://docs.google.com/forms/d/1qtAK5Nr2qwkfDSQn54zwJ7vli1YQwO2M7vZ-rKIb33Y/viewform?embedded=true" width="760" height="800" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>  --%>
    </div>   
</div>