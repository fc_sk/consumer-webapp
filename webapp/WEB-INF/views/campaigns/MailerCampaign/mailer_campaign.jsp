<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<head>
    <link href="/content/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>
<div class="container" style="padding-bottom: 10px;">
    <div class="alert" style='display: <c:if test="${empty param.message}">none</c:if>'>
        <c:if test="${not empty param.message}">
            <c:out value="${param.message}"/>
        </c:if>
        <a class="close" href="#">&times;</a>
    </div>
     <div class="span12">
        <h2>Mailer Campaigns</h2>
        <a class="btn btn-info" href="/admin/mailer/campaign/edit.htm" title="Add New Campaign">Add Campaign</a>&nbsp;&nbsp;
        
        <table class="table table-bordered table-striped">
            <tr>
                <th><strong>Actions</strong></th>
                <th><strong>Id</strong></th>
                <th><strong>Name</strong></th>
                <th><strong>Condition Id</strong></th>
                <th><strong>Mailer Type</strong></th>
                <th><strong>Start Date (yyyy-mm-dd)</strong></th>
                <th><strong>End Date (yyyy-mm-dd)</strong></th>
                <th><strong>Params</strong></th>
                <th><strong>Priority</strong></th>
            </tr>
            <c:forEach var="camp" items="${mailerCampaigns}">
                <tr>
                    <td><a href="/admin/mailer/campaign/edit?id=${camp.id}">edit</a></td>
                    <td>${camp.id}</td>
                    <td>${camp.campaignName}</td>
                    <td>${camp.conditionId}</td>
                    <td>${camp.mailerType}</td>
                    <td>${camp.startDate}</td>
                    <td>${camp.endDate}</td>
                    <td>${camp.params}</td>
                    <td>
                        <c:if test="${camp.enabled}">
                            <input type="text" class="input-mini" value="${camp.priority}" data-id="${camp.id}">
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            <tr>
                <td colspan="10"><a href="#" id="save_mailer_priority" class="btn btn-inverse pull-right">Save Priority</a></td>
            </tr>
        </table>
     </div>
     
</div>
<script type="text/javascript">
  function changeStatus(url, id, status) {
	    $.post(url, {"id":id, "status":status}, function(data) {
	        location.reload();
	    }, 'json');
  }
  
  $(document).ready(function(){
      $('#save_mailer_priority').on('click', function(ev){
          ev.preventDefault();
          var data = {};
          $('input.input-mini').each(function(){
              data[$(this).data('id')] = $(this).val();
          });
          $.ajax('/admin/mailer/campaign/ordering.htm', {
              data: {'order': JSON.stringify(data)},
              type: 'POST',
              dataType: 'json',
              success: function(result){
                  $('div.alert').addClass("alert-success").html(result.message+'<a class="close" href="#">&times;</a>').show();
                  attachClose();
              },
              error: function(xhr){
                  $('div.alert').addClass("alert-error").html($.parseJSON(xhr.responseText).message+'<a class="close" href="#">&times;</a>').show();
                  attachClose();
              }
          })
      });
  });
</script>

