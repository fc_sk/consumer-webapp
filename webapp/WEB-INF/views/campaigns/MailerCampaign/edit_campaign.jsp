<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<style>
    ul li {margin-top: 5px; list-style: none; padding-left: 15px;}
    ul {margin: 0; padding: 0; list-style: none;}
</style>
<div class="well">
    <h3><c:choose><c:when test="${edit}">Edit</c:when><c:otherwise>Add</c:otherwise></c:choose> Mailer Campaign</h3>
    <form:form action="/admin/mailer/campaign/save.htm" method="POST" commandName="mailerEntity" id="ardeal_form" cssClass="form form-horizontal">
        <form:hidden path="id"/> 
        <div class="control-group">
            <label for="campaignName" class="control-label">Campaign name</label>
            <div class="controls">
                <form:input path="campaignName" id="campaignName"/>
                <form:errors path="campaignName" cssStyle="color: red"/>
            </div>
        </div>
        <div class="control-group">
            <label for="conditionId" class="control-label">Condition</label>
            <div class="controls">
                <form:select path="conditionId" >
                    <form:options items="${conditions}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
        </div>
        <div class="control-group">
        	 <label for="mailerType" class="control-label">Mailer Type</label>
        	 <div class="controls">
                 <form:select path="mailerType" >
                    <form:options items="${mailerTypeEnum}" itemValue="id"/>
                </form:select>
            </div>
        </div>
        <div class="control-group">
        	 <label for="startDateStr" class="control-label">Start Date (yyyy-mm-dd)</label>
        	 <div class="controls">
                <form:input path="startDateStr" id="startDateStr"/>
            </div>
        </div>
        <div class="control-group">
        	 <label for="endDateStr" class="control-label">End Date (yyyy-mm-dd)</label>
        	 <div class="controls">
                <form:input path="endDateStr" id="endDateStr"/>
            </div>
        </div>
              
        <div class="control-group">
            <label for="params_list" class="control-label">Params</label>
            <div class="controls">
                <div class="row-fluid" id="params_list">
                    <a href="#" id="new_param" class="btn btn-primary btn-small"><i class="icon-plus-sign"></i> </a>
                </div>
            </div>
        </div>
        <form:hidden path="params" id="params"/>
        <div class="form-actions">
            <input type="submit" class="btn btn-info" value="Save">
        </div> 
    </form:form>
</div>
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/ardeal-params.js?v=${mt:keyValue("version.no")}"></script>