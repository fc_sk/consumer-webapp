<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<p class="expiry-message" >This deal has expired!</p>
<div class="container clearfix" >
	<div class="fcHoli-offer" >
        <h1>FreeCharge - Holi Cash Back offer </h1>
        
        <div class="fcHoli-offer-row clearfix fcHoli-offer-maintext" >
        	 <h2>Holi is now a FreeCharge Day! </h2>
             <p>A FreeCharge Day is a day of Cash Backs. Put simply, it is a LOOT day for you to get cash backs from FreeCharge. <br /><strong> Recharge on FreeCharge and get a chance to win Cash Back upto Rs.1000!</strong></p>
        </div>
        <div class="fcHoli-offer-row clearfix fcHoli-offer-banner" >
        	<img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/holiCashbackBanner.jpg" alt="FreeCharge Day - Holi Cash Back offer" title="FreeCharge Day - Holi Cash Back offer"  width="920" height="197" />
        </div>  
        <div class="fcHoli-offer-row clearfix" >
           <h4>Offer details</h4>
           <p class="fcHoli-offer-midtext" >After Valentine&#39;s Day cash back offer, we are back, yet again! We know Holi is a day for you to wish your loved ones, but alas! the special rate cutter plans don&#39;t work, which means that call rates and SMS rates are charged at normal rate! Does this drain up your balance? Don&#39;t worry, we got your back.</p>
		   <p>Presenting, Holi Cash Back offer from FreeCharge. For today, depending on the last digits of your order ID, we will give <strong>Cash Back upto Rs.1000</strong>.  <br />Put simply, here is how it rolls: </p>
 			<ol>
                <li>Recharge on www.freecharge.in (27th March 2013). </li>
                <li>By default, every person recharging today will be eligible to participate in this contest.</li>
                <li>Proceed with your recharge. Once you complete your transaction, you will get an order ID (you will get the same on email also). </li>
                <li>If your order ID end with &ldquo;THE LUCKY DIGITS&rdquo;, you get to win ASSURED cash back of upto Rs.1000. </li>
            </ol>
            <p>Since this contest is related to order IDs, a person can win multiple times! So, what are you waiting for? Recharge your phone, your GF&#39;s/ Wife&#39;s phone, your relatives&#39; phone and celebrate with this unique way to wish HOLI.</p>
		</div>
        <div class="fcHoli-offer-row clearfix" >
           <h4>Offer terms &amp; conditions:</h4>
 			<ol>
                <li>Only criteria for winning the cash back is to have the order ID&#39;s last digits as exactly same as THE LUCKY DIGITS. </li>
                <li>Lucky users will be informed on their registered email ID before 29th March 2013.</li>
                <li>Cash back will be issued in the form of promo codes which will help you get the offer. </li>
                <li>FreeCharge shall reserve the rights to make all the decisions related to the campaign. </li>
                <li>Holi Cash Back offer ends 23:59:59 on 27th March 2013.</li> 
            </ol>
            <h2 class="rechargeNow-title lf" >To get the Holi Cash Back Offer</h2>
            <div class="rechargeNow-set lf" >
            	<a href="/"><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/rechargeNow-button.png" alt="Recharge now" title="Recharge now "  width="171" height="40" /></a>
              	<a href="/" target="_blank"  class="rechargeNow-set-links clearfix" >Mobile &bull; DTH &bull; Data Card</a>
            </div>
		</div>
    </div>
</div>