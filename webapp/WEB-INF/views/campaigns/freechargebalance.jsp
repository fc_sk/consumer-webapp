<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<div class="container clearfix" >
    <div class="sixteen columns">
	<div class="paymentoptions-balance" >
        <h1>Hey you!</h1>
        <p style="font-size: 16px">If FreeCharging was fun, wait till you check out something cooler - Credits.</p>
        
        <div class="paymentoptions-fcbalance-storywrapper clearfix" >
        	<h2 class="paymentoptions-fcbalanceStory_title"  ><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/balance/balanceStory_title-new.png" alt="The FreeCharge Credits Story" title="The FreeCharge Credits Story" width="909" height="71"  /></h2>
            <div class="paymentoptions-fcbalanceStory_steps clearfix" >
            	<div class="paymentoptions-fcbalanceStory_stepcolumn clearfix" >
            		<a href="/app/mycredits.htm" target="_blank" class="rf"><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/balance/balanceStory1_step1.png" alt="You started FreeCharging" title="You started FreeCharging" width="266" height="180"  /></a>
                    <p>Once upon a time, recharging was a pain in the (you know where). And then, you started FreeCharging. Ah, the joy of getting recharged in less than a minute!</p>
           	    </div>
                <%--<img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/balance/balanceStory1_divider1.png"  class="paymentoptions-fcbalanceStory-divider" alt="| | | | | | | | | | | | |"  width="7" height="249"  />--%>
                <div class="paymentoptions-fcbalanceStory_stepcolumn clearfix" >
            		<a href="/app/mycredits.htm" target="_blank" class="lf"><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/balance/balanceStory1_step2.png" alt="Bank refunds suck" title="Bank refunds suck" width="266" height="180"  /></a>
                    <p>In the odd case (very rare, actually) that your recharge doesn&#39;t happen, we owe you a refund. But refunds suck, right? Because your bank could take up to 30 days to give back your money. Why wait when you have Credits?</p>
           	    </div>
                <%--<img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/balance/balanceStory1_divider2.png" class="paymentoptions-fcbalanceStory-divider"  alt="| | | | | | | | | | | | |"  width="10" height="249"  />--%>
                <div class="paymentoptions-fcbalanceStory_stepcolumn clearfix" >
            		<a href="/app/mycredits.htm" target="_blank" class="rf"><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/balance/balanceStory1_step3.png" alt="FreeCharge Balance - Get refund money instantly" title="FreeCharge Balance - Get refund money back instantly" width="300" height="180"  /></a>
                    <p>Say hello to FreeCharge Credits! Get your refund money deposited into your Credits - a virtual wallet where we&#39;ll keep it safe [safer than your bank locker :)] for you to use it whenever you want to!</p>
           	    </div>
            </div>
        </div>
        <p class="balance-feature-message">Credits aren't just about collecting your refunds, you can do cool things with it too.<br>Want to know what? Read on</p>
        <div class="paymentoptions-balance-storywrapper clearfix" >
        	<h3 class="paymentoptions-balanceStory_title"  ><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/balance/balanceStory_title2-new.png" alt="The Credits Story" title="The Credits Story" width="907" height="50"  /></h3>
            <div class="paymentoptions-balanceStory_steps clearfix" >
            	<div class="paymentoptions-balanceStory_stepcolumn paymentoptions-balanceStory_stepcolumn1 clearfix" >
                    <img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/balance/balanceStory2_step1-new.png" class="rf" alt="Pay with your Credits" title="Pay with your Credits" width="246" height="293"  />
                    <h5>Pay with your Credits!</h5>
                    <p class="description" >Here&#39;s how you utilize your refunds towards future recharges: </p>

                    <ul class="" >
                        <li>- Ditch the long-drawn process of paying via debit/credit card or net banking.</li>
                        <li>- Pay for your recharge the smart way using Credits.</li>
                        <li>- Look out for the &ldquo;Pay with Credits&rdquo; option in the payment section and get it done in a jiffy! </li>
                    </ul>
                </div>
                <%--<img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/balance/balanceStory2_divider1.png"  class="paymentoptions-balanceStory-divider" alt=" | | | | | | | | | | | | | | | | | | | | | | |"  width="8" height="385"  />--%>
                <div class="paymentoptions-balanceStory_stepcolumn clearfix" >
                    <a href="/app/mycredits.htm" target="_blank" ><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/balance/balanceStory2_step2.png" class="lf" alt="Deposit Cash in your Balance!" title="Deposit Cash in your Balance!" width="246" height="293"  /></a>
                    <h5>Deposit Cash in your Credits!</h5>
                    <p class="description" >Here's how you make your recharge process up to 10x faster and simpler:</p>
                    <ul class="" >
                        <li>- Don&#39;t have a refund? You can still have Credits and use it to pay for your recharge.</li>
                        <li>- Look out for the &ldquo;Deposit&rdquo; feature under Credits. Load cash and use it for future recharges, whenever you want!</li>
                    </ul>
           	    </div>
            </div>
            <p  class="description" >Long story short, Credits help you avail your refund quicker, manage your funds effectively and recharge faster. But you already knew that! :)</p>
        </div>      
    </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".fcMoney-saver-offer-tabs1, .fcMoney-saver-offer-tabs2, .fcMoney-saver-offer-tabs3, .fcMoney-saver-offer-tabs4, .fcMoney-saver-offer-tabs5").customTabEffect({ activeTabElement : 'span.tab'});
	});
	
//customTabEffect Plugin .. Author : Priya
(function($){
	$.fn.extend({
			customTabEffect:function(options){
				var defaults = {
							openDefault : null,
							activeTabClass : 'activeTab',
							activeTabElement : 'li a',
							closeButton : ".closeTabButton"
					};
			 	var options = $.extend(defaults, options);
				 return this.each(function() {
						var o = options;
						var obj = $(this);
                        var objClass = $(this)[0].className.split(" ")[0];  // First class name
						var contentHandler = $("." + objClass + "_content");
						var activeTabClass = o.activeTabClass; 
						var activeTabElement = o.activeTabElement;
						var closeButton = o.closeButton;
						if( o.openDefault != null){
							obj.find(activeTabElement).removeClass(activeTabClass).eq(o.openDefault).addClass(activeTabClass);
							contentHandler.find("> div" ).hide().eq(o.openDefault).show();
						}
						obj.find(activeTabElement).click(function(){
								var rel = $(this).attr("rel");
								contentHandler.find("> div" ).hide().eq(rel).show();
								$(this).addClass(activeTabClass).siblings().removeClass(activeTabClass)
								return false;	
							})
						obj.find(closeButton).click(function(){
								$(this).parent("div").hide();	
								obj.find(activeTabElement).removeClass(activeTabClass);
								return false;	
							})	
							
          		  });	
				}
		});
})(jQuery);     

</script>