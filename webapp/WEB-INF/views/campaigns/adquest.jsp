<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<div class="container clearfix" >
    <div class="sixteen columns">
		<div class="adquest-wrapper clearfix" >
        	<div class="row clearfix" >
                <div class="ten columns">
                    <a href="https://img.freecharge.in/content/images/campaign_mailer/adquest/images/adquest-contest.pdf" target="_blank" class="adquest-video" ><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/AdQuest/video.png" width="540" height="374" alt="Ad-Quest - The hunt for India&#39;s Best Advertainers" title="Ad-Quest - The hunt for India&#39;s Best Advertainers" ></a>
                    <img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/AdQuest/img_03.png" width="19" height="20" alt="Ad-Quest" title="Ad-Quest - The hunt for India&#39;s Best Advertainers" class="adquest-lt-floatingIcon" >
                    <img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/AdQuest/img_01.png" width="540" height="247" alt="Ad-Quest - The hunt for India&#39;s Best Advertainers" >
                </div>
                <div class="five columns">
                    <img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/AdQuest/img_02.png" width="252" height="134" alt="The Winners Walk Away With" title="The Winners Walk Away With" >
                    
                    <ul class="adquest-prize-list" >
                        <li>
                            <img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/AdQuest/prize_01.png" width="120" height="68" alt="Cash Prize" title="Cash Prize upto Rs.25,000" >
                            <p>Cash Prize upto Rs.25,000 to be won</p>
                        </li>
                        <li>
                            <img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/AdQuest/prize_02.png" width="120" height="68" alt="Free Recharge*" title="Free Recharge* for a year" >
                            <p>Free Recharge* for a year for the winners</p>
                        </li>
                        <li>
                            <img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/AdQuest/prize_03.png" width="120" height="68" alt="FCR league" title="Shortcut to elite FCR league">
                            <p>A shortcut to elite FCR league (selection process)</p>
                        </li>
                        <li>
                            <img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/AdQuest/prize_04.png" width="120" height="68" alt="Surprise Gifts" title="Surprise Gifts" >
                            <p>Surprise Gifts, and ofcourse, ultimate glory!</p>
                        </li>
                    </ul>
                    
                    <a href="https://img.freecharge.in/content/images/campaign_mailer/adquest/images/adquest-contest.pdf" target="_blank" class="paricipateButton" ><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/AdQuest/participateButton.png" width="251" height="48" alt="Participate Now" title="Participate Now"></a>
                    <p class="closing-details" >[ Round 1 ends 24th July 23:59 hrs. ]</p>
                    <div class="contact-info" >
                        <h5>Contact info;</h5>
                        <p>Shankey (IIM C) : 8272928347</p>
                        <p>Akhilesh (IIM C) : 9836334991</p>
                        <p>Kushal (FreeCharge) : 9663572819</p>
                    </div>
                </div>
            </div>
            <div class="row" >
            	<div class="adquest-partnerBanner" >
                	<ul class="adquest-partnerList clearfix" >
                    	<li><span>Youth Partner : </span><a href="https://www.letsintern.com/" target="_blank" ><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/AdQuest/letsintern_logo.png" width="139" height="61" alt="Letsintern" title="Letsintern" ></a></li>
                        <li><span>Competition Partner : </span><a href="https://www.dare2compete.com/" target="_blank" ><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/AdQuest/dare2Compete_logo.png" width="188" height="61" alt="Dare2Compete" title="Dare2Compete" ></a></li>
                    </ul>
                </div>
            </div>
 
	    </div>
    </div>
</div>