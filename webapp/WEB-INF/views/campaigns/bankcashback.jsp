<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<div class="container clearfix" >
	<div class="fcMoney-saver-offer" >
        <h1>FreeCharge - Cash back Bank offers</h1>
                      
        <div class="fcMoney-saver-offer-row clearfix" >
            <div class="fcMoney-saver-offer-bankicon lf" ><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/hdfcBankLogo.png" alt="HDFC Debit Card offer" title="HDFC Debit Card offer" width="263" height="125" /></div>
            <div class="fcMoney-saver-offer-data rf" >
                <h4>HDFC Debit Card offer</h4>
                <ul>
                	<li><strong>Get Rs.30 Cash Back</strong> (on minimum recharge of <span class="webRupee">Rs.</span>50) + <strong>2x value coupons</strong> (on your favourite brands) </li>
                    <li>Available only for HDFC Corporate salary account</li>
                </ul>
                <div class="fcMoney-saver-offer-tabs4" >
                    <div class="fcMoney-saver-offer-tabtitle clearfix">
                        <span class="tab lf" rel="0" >How to avail</span>
                        <span class="tab rf" rel="1" >Terms and conditions</span>
                        <span class="mid rf" >Valid till: <strong>30th Nov 2013</strong></span>
                    </div>
                    <div class="fcMoney-saver-offer-tabs4_content">
                        <div class="fcMoney-saver-offer-tabdescription" style="display:none">
                            <p>How to avail</p>
                            <ol>
                            	<li>Log on to <a href="/" target="_blank" >www.freecharge.in</a> &amp; select recharge option for DTH, Mobile or Data card. </li>
                                <li>Continue with the transaction &amp; register by providing mandatory details. </li>
                                <li>Please enter your HDFC Bank Debit card number in the &ldquo;Card Number&rdquo; field under &ldquo;Other payment options&rdquo; and click &ldquo;Check for offer&rdquo;. Proceed with the payment by clicking on &ldquo;Make payment&rdquo; button. </li>
                                <li>Enjoy cash back in your FreeCharge Credits.</li>
                            </ol>
                            <span class="closeTabButton rf" >-</span>
                        </div>
                        <div class="fcMoney-saver-offer-tabdescription" style="display:none">
                            <p>Terms and conditions</p>
                            <ol>
                            	<li>The offer can be redeemed only on your first transaction on <a href="/" target="_blank" >www.freecharge.in</a> by customers holding corporate salary account with HDFC Bank. Offer valid on HDFC Debit Card only.</li>
                                <li>The customer gets Rs.30 Cash Back in FreeCharge Credits on minimum recharge of Rs.50 and above + Gets 2 x value coupons during the transaction using HDFC Bank Debit cards.</li>
                                <li>The promotion/offer is not valid in conjunction with any other offer/scheme/promotions. </li>
                                <li>Steps to avail offer: Visit <a href="/" target="_blank" >www.freecharge.in</a> -&gt; Enter Mobile, DTH, Data card number, Amount, Operator and Circle -&gt; Click Proceed -&gt; Select exciting discount coupons of equal value -&gt; Click Continue -&gt; Login/Register -&gt; Press Continue -&gt; Enter your HDFC debit card number in the Card Number field under other payment options -&gt; Click Check for Offer -&gt; Press Make payment -&gt; Enjoy cash back in your FreeCharge Credits. </li>
                                <li>Strict action will be taken against any forge transactions and in case of any dispute, FreeCharge reserves the right to decide on Mobile, DTH, Data card recharge, redemption and coupons.</li>
                                <li>For any query or feedback, please contact us at <a href="mailto:care@freecharge.com" >care@freecharge.com</a> For more details, kindly visit <a href="/" target="_blank" >www.freecharge.in</a>.</li>
                            </ol>
                            <span class="closeTabButton rf" >-</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          
        
                
        <div class="fcMoney-saver-offer-row clearfix" >
            <div class="fcMoney-saver-offer-bankicon lf" ><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/masterMaestroLogo.png" alt="Master &amp; Maestro card double value coupon" title="Master &amp; Maestro card double value coupon offer"  width="263" height="125" /></div>
            <div class="fcMoney-saver-offer-data rf" >
                <h4>MASTER/ MAESTRO CARD Double value coupons Offer</h4>
                <ul>
                    <li><strong>Get 2x value coupons</strong>, when you use MasterCard Debit/ Credit Card/ Maestro Debit Cards  </li>
                </ul>
                
                <div class="fcMoney-saver-offer-tabs2" >
                    <div class="fcMoney-saver-offer-tabtitle clearfix">
                        <span class="tab lf" rel="0" >How to avail</span>
                        <span class="tab rf" rel="1" >Terms and conditions</span>
                        <span class="mid rf" >Valid till: <strong>30th Nov 2013</strong></span>
                    </div>
                    <div class="fcMoney-saver-offer-tabs2_content">
                        <div class="fcMoney-saver-offer-tabdescription" style="display:none">
                            <p>How to avail</p>
                            <ol>
                            	<li>Log on to <a href="/" target="_blank" >www.freecharge.in</a></li>
                                <li>Enter your mobile/ DTH/ Data Card number and supporting details.</li>
                                <li>Select the coupons you want and proceed to payment.</li>
                                <li>In the payments page, select Debit Card/ Credit Card as your payment mode and select Master Card/ Maestro Debit Card. Your selected coupons will be doubled.</li>
                                <li>Proceed with payments as earlier.</li>
                            </ol>
                            <span class="closeTabButton rf" >-</span>
                        </div>
                        <div class="fcMoney-saver-offer-tabdescription" style="display:none">
                            <p>Terms and conditions</p>
                            <ol>
                            	<li>Offer valid for all MasterCard (Credit and Debit cards) and Maestro Debit cards only.</li>
                                <li>This offer is valid for Indian residents and citizens only.</li>
                                <li>The terms and conditions of this promotion are subject to change without any prior notice.</li>
                                <li>Offer period: 30th Nov 2013.</li>
                            </ol>
                            <span class="closeTabButton rf" >-</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".fcMoney-saver-offer-tabs2, .fcMoney-saver-offer-tabs4").customTabEffect({ activeTabElement : 'span.tab'});
	});
	
//customTabEffect Plugin .. Author : Priya
(function($){
	$.fn.extend({
			customTabEffect:function(options){
				var defaults = {
							openDefault : null,
							activeTabClass : 'activeTab',
							activeTabElement : 'li a',
							closeButton : ".closeTabButton"
					};
			 	var options = $.extend(defaults, options);
				 return this.each(function() {
						var o = options;
						var obj = $(this);
                        var objClass = $(this)[0].className.split(" ")[0];  // First class name
						var contentHandler = $("." + objClass + "_content");
						var activeTabClass = o.activeTabClass; 
						var activeTabElement = o.activeTabElement;
						var closeButton = o.closeButton;
						if( o.openDefault != null){
							obj.find(activeTabElement).removeClass(activeTabClass).eq(o.openDefault).addClass(activeTabClass);
							contentHandler.find("> div" ).hide().eq(o.openDefault).show();
						}
						obj.find(activeTabElement).click(function(){
								var rel = $(this).attr("rel");
								contentHandler.find("> div" ).hide().eq(rel).show();
								$(this).addClass(activeTabClass).siblings().removeClass(activeTabClass)
								return false;	
							})
						obj.find(closeButton).click(function(){
								$(this).parent("div").hide();	
								obj.find(activeTabElement).removeClass(activeTabClass);
								return false;	
							})	
							
          		  });	
				}
		});
})(jQuery);     

</script>
