<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<div class="container clearfix" >
    <div class="sixteen columns">
		<div class="freecharge-apps-wrapper" >
	       <img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/mobileApp/fcapp_bg2.jpg" alt="FreeCharge on the go" 
	        title="FreeCharge on the go" class="freecharge-apps-bg" >
            <img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/mobileApp/mobileInHand4.png" width="480" 
            height="583" alt="FreeCharge on the go" title="FreeCharge on the go" class="freecharge-apps-handIcon" >
            <div class="freecharge-apps-content rf" >           	 
           		<h2>FreeCharge on the go!</h2> 
           		<p>Get recharged, instantly anytime, anywhere. Unlock cool coupons for an equal value, on the go!</p>         		
                <ul class="freecharge-apps-list" >
                    <li><a href="https://play.google.com/store/apps/details?id=com.freecharge.android" target="_blank" ><img src="${mt:keyValue("imgprefix1")}/images/marketing_campaigns/mobileApp/GooglePlay-icon.png" alt="Google play" title="Get it now on Google play" height="83" width="246"  /></a></li>
                </ul>
            </div>
	    </div>
    </div>
</div>