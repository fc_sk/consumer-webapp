<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>

<div class="upsell-setting">
    <h1><spring:message code="fc.upsell.settings" text="#fc.upsell.settings#"/></h1>
    <c:if test="${not empty param.message}">
        <div style="background-color:yellow;padding:10px;"><c:out value="${param.message}"/></div>
    </c:if>

    <div class="flex-layout clearfix">
        <div class="one">
            <div class="left-content">
                <h2><spring:message code="available.upsell" text="#available.upsell#"/></h2>
                <ul>
                    <c:forEach var="item" items="${ATTR_CROSSSELL_BUSSINESS_DO_LIST}">
                        <li>
                            <p><c:out value="${item.title}"/></p>
                            <c:choose>
                                <c:when test="${item.crosssell.isActive}">
                                    <a id="disableLink${item.storeId}" href="javascript:updateStatus(${item.storeId},0)" ><spring:message code="disable" text="#disable#"/></a>
                                    <a id="enableLink${item.storeId}" href="javascript:updateStatus(${item.storeId},1)" style="display: none;"><strong><spring:message code="enable" text="#enable#"/></strong></a>
                                </c:when>
                                <c:otherwise>
                                    <a id="disableLink${item.storeId}" href="javascript:updateStatus(${item.storeId},0)" style="display: none;"><spring:message code="disable" text="#disable#"/></a>
                                    <a id="enableLink${item.storeId}" href="javascript:updateStatus(${item.storeId},1)"><strong>Enable</strong></a>
                                </c:otherwise>
                            </c:choose>
                            <a href="/admin/crosssell/home.htm?crosssell_id=${item.crosssell.id}"><spring:message code="edit" text="#edit#"/></a>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
        <div class="two">
            <div class="mid-content">
                <h2><spring:message code="create.upsell" text="#create.upsell#"/></h2>
                <form  name="crosssellform" <c:if test="${not empty ATTR_CROSSSELL_BUSSINESS_DO}"> action="/admin/crosssell/save.htm"</c:if><c:if test="${empty ATTR_CROSSSELL_BUSSINESS_DO}">action="/admin/crosssell/create.htm"</c:if> method="POST" cssClass="form" id="crosssellform" >
                    <input type="hidden" value="${ATTR_CROSSSELL_BUSSINESS_DO.crosssell.id}"  name="crosssell_id"  >
                    <table width="100%" class="create-upsell">
                        <tbody>
                            <tr>
                                <td colspan="2"><p class="small-font"><spring:message code="fields.marked" text="#fields.marked"/><span class="red"> * </span><spring:message code="are.mandatory" text="#are.mandatory#"/></p></td>
                            </tr>
                            <!-- formfields starts here -->
                            <tr>
                                <td colspan="2"><!-- error container div start -->
                                    <!-- Div error message start -->
                                    <div style="display:none;" id="err_container">
                                        <p id="inner_err_container"></p>
                                    </div>
                                    <!-- Div error message end -->

                                    <!-- Div confirmation message start -->
                                    <div style="display:none;" id="conf_container">
                                        <p id="inner_conf_container"></p>
                                    </div>
                                    <!-- Div error message end -->
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="title" text="#title#"/></span>:</td>
                                <td width="75%" >
                                    <input type="text" value="${ATTR_CROSSSELL_BUSSINESS_DO.title}"  name="title"  title="Enter Title">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" ><span id="txt_price"><spring:message code="price" text="#price#"/></span>:</td>
                                <td width="75%" >
                                    <input type="text" value="${ATTR_CROSSSELL_BUSSINESS_DO.crosssell.price}"  name="price"  title="Enter Price">
                                    <input type="hidden" value="0"  name="position"  >
                                    <input type="hidden" value="Y"  name="isRefundable"  >
                                    <input type="hidden" value="100"  name="maxuses" >
                                    <input type="hidden" value="100"  name="threshold_count">
                                    <input type="hidden" value="Y"  name="uniquecode">
                                    <input type="hidden" value="test2"  name="other_condition">
                                    <input type="hidden" value="genericCrossSell"  name="className">
                                    <input type="hidden" value="templates/mail/genericCrossSellTemplate.vm"  name="template">
                                    <input type="hidden" value="CROSS_SELL"  name="category">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" ><span class="red">*</span> <span id="txt_tnc"><spring:message code="terms.condition" text="#terms.condition#"/></span>:</td>
                                <td width="75%" >
                                    <textarea rows="7" cols="50" style="width:400px;"  name="tnc"  id="tnc" title="Enter Terms &amp; Condition">${ATTR_CROSSSELL_BUSSINESS_DO.tnc}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" ><span class="red">*</span><span id="txt_headDesc"><spring:message code="head.description" text="#head.description#"/></span>:</td>
                                <td width="75%" >
                                    <textarea rows="7" cols="50"  style="width:400px;"   name="headDesc" id="headDesc"  title="Enter Head Description">${ATTR_CROSSSELL_BUSSINESS_DO.headDescription}</textarea>
                                </td>
                            </tr>
                        <tr>
                            <td width="25%" ><span class="red">*</span><span id="txt_description"><spring:message code="description" text="#description#"/></span>:</td>
                            <td width="75%" >
                                <textarea rows="7" cols="50"  style="width:400px;"   name="description" id="description"  title="Enter Description">${ATTR_CROSSSELL_BUSSINESS_DO.description}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" ><span class="red">*</span><span id="txt_primary_product"><spring:message code="primary.product" text="#primary.product#"/></span>:</td>
                            <td width="75%" >
                                <input type="checkbox" <c:if test="${fn:contains(ATTR_CROSSSELL_BUSSINESS_DO.primaryproduct, 'V')}">checked='true'</c:if> value="V" name="primary_product"><spring:message code="mobile" text="#mobile#"/>
                                <br>
                                <input type="checkbox" <c:if test="${fn:contains(ATTR_CROSSSELL_BUSSINESS_DO.primaryproduct, 'C')}">checked='true'</c:if>  value="C" name="primary_product"><spring:message code="datacard" text="#datacard#"/>
                                <br>
                                <input type="checkbox" <c:if test="${fn:contains(ATTR_CROSSSELL_BUSSINESS_DO.primaryproduct, 'D')}">checked='true'</c:if>  value="D" name="primary_product"><spring:message code="dth" text="#dth#"/>
                                <br>
                                <input type="checkbox" <c:if test="${fn:contains(ATTR_CROSSSELL_BUSSINESS_DO.primaryproduct, 'M')}">checked='true'</c:if>  value="M" name="primary_product"><spring:message code="MobilePostpaid" text="#MobilePostpaid#"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" >Primary product amount</span>:</td>
                            <td width="75%" >
                               <input type="text" value="${ATTR_CROSSSELL_BUSSINESS_DO.primaryproductamount}"  name="primary_product_amount">
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" ><span class="red">*</span> <span id="txt_mailTnc"><spring:message code="mail.tnc" text="#mail.tnc#"/></span>:</td>
                            <td width="75%" >
                                <textarea rows="7" cols="30"  style="width:400px;"    id="mailTnc" name="mailTnc" title="Enter mailer terms and conditions">${ATTR_CROSSSELL_BUSSINESS_DO.mailTnc}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" ><span class="red">*</span> <span id="txt_mailSubject"><spring:message code="mail.subject" text="#mail.subject#"/></span>:</td>
                            <td width="75%" >
                                <input type="text" value="${ATTR_CROSSSELL_BUSSINESS_DO.mailSubject}"  name="mailSubject" >
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" ><span class="red">*</span> <span id="txt_mailImgUrl"><spring:message code="mail.imgurl" text="#mail.imgurl#"/></span>:</td>
                            <td width="75%" >
                                <input type="text" value="${ATTR_CROSSSELL_BUSSINESS_DO.mailImgUrl}"  name="mailImgUrl"  title="Enter Mailer Image Url">
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" ><span class="red">*</span> <span id="txt_lable"><spring:message code="label" text="#label#"/></span>:</td>
                            <td width="75%" >
                                <input type="text" value="${ATTR_CROSSSELL_BUSSINESS_DO.crosssell.label}"  name="lable"  title="Enter Lable">
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" ><span id="txt_valid_upto"><spring:message code="valid.upto" text="#valid.upto#"/></span>:</td>
                            <td width="75%" >
                                <input type="text"  class="calender_input" value="<fmt:formatDate pattern='yyyy-MM-dd' value='${ATTR_CROSSSELL_BUSSINESS_DO.couponStore.validityDate}'/>" id="valid_upto" name="valid_upto" >
                            </td>
                        </tr>

                        <tr>
                            <td width="25%" ><span class="red">*</span> <span id="txt_city"><spring:message code="city" text="#city#"/></span>:</td>
                            <td width="75%" >
                                <select  multiple="multiple" style="width:163px" size="10" id="city_list" name="city_list">
                                    <c:forEach var="city" items="${ATTR_CITY_LIST}">
                                        <option <c:if test="${  fn:contains( ATTR_STORED_CITY_LIST, city.cityMasterId ) }">selected='true'</c:if> value="${city.cityMasterId}"><c:out value="${city.cityName}"/></option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td width="26%" valign="top" ><span id="txt_crosssell_upload"><spring:message code="upsell.image.url" text="#upsell.image.url#"/>:</span></td>
                            <td >
                                <input type="text" value="${ATTR_CROSSSELL_BUSSINESS_DO.imgurl}"   id="crosssell_image" name="crosssell_image" >
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" align="right">
                                <input type="submit"  value="<c:if test="${not empty ATTR_CROSSSELL_BUSSINESS_DO}">Save</c:if><c:if test="${empty ATTR_CROSSSELL_BUSSINESS_DO}">Submit</c:if>" class="button"  name="addbut">
                            </td>
                        </tr>

                        </tbody></table>
                </form>
            </div>
        </div>
        <div class="three">
            <div class="right-content">
                <div class="unit">
                    <h2><spring:message code="upload.codes" text="#upload.codes#"/></h2>
                    <form  name="crossselluploadform" action="/admin/crosssell/upload.htm" method="post" enctype="multipart/form-data" cssClass="form" id="crossselluploadform" >
                        <div class="row">
                            <label><spring:message code="select.upsell" text="#select.upsell#"/></label>
                            <select  name="coupon_store_id">
                                <option value=""><spring:message code="select.option" text="#select.option#"/></option>
                                <c:forEach var="item" items="${ATTR_CROSSSELL_BUSSINESS_DO_LIST}">
                                    <option value="${item.storeId}"><c:out value="${item.title}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="row">
                            <label><spring:message code="upload.file" text="#upload.file#"/></label>
                            <input type="file"   id="crosssell_codes" name="crosssell_codes" >
                        </div>
                        <div class="row">
                            <input type="submit"  title="Submit" value="Submit" name="addbut">
                        </div>
                    </form>
                </div>

                <div class="unit">
                    <h2><spring:message code="download.unused.codes" text="#download.unused.codes#"/></h2>
                    <form  name="crossselldownloadform" action="/admin/crosssell/download.htm" method="get"  cssClass="form" id="crossselldownloadform" >
                        <div class="row">
                            <label><spring:message code="select.upsell" text="#select.upsell#"/></label>
                            <select  name="coupon_store_id">
                                <option value=""><spring:message code="select.option" text="#select.option#"/></option>
                                <c:forEach var="item" items="${ATTR_CROSSSELL_BUSSINESS_DO_LIST}">
                                    <option value="${item.storeId}"><c:out value="${item.title}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="row"><input type="submit"  title="Submit" value="Submit" name="addbut"></div>
                    </form>
                </div>

                <div class="unit">
                    <h2><spring:message code="block.unused.codes" text="#block.unused.codes#"/></h2>
                    <form  name="crosssellblockform" action="/admin/crosssell/blockcodes.htm" method="post"  cssClass="form" id="crosssellblockform" >
                        <div class="row">
                            <label><spring:message code="select.upsell" text="#select.upsell#"/></label>
                            <select  name="coupon_store_id" title="Select upsell">
                                <option value=""><spring:message code="select.option" text="#select.option#"/></option>
                                <c:forEach var="item" items="${ATTR_CROSSSELL_BUSSINESS_DO_LIST}">
                                    <option value="${item.storeId}"><c:out value="${item.title}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="row">
                            <label><spring:message code="reason.block" text="#reason.block#"/></label>
                            <input type="text" name="reason_to_block" value="" placeholder="(i.e CCDoutdoorCampaign)" />
                        </div>
                        <div class="row">
                            <input type="submit"  title="Submit" value="Block Codes" name="addbut">
                        </div>
                    </form>
                </div>

                <div class="unit">
                    <h2 class="no-margin"><spring:message code="re.trigger" text="#re-trigger#"/></h2>
                    <p><spring:message code="sends.new.upsell.code" text="#sends.new.upsell.code#"/></p>
                    <form  name="retriggerform" action="/admin/crosssell/retrigger.htm" method="get"  cssClass="form" id="retriggerform" >
                        <div class="row">
                            <label><spring:message code="enter.order.id" text="#enter.order.id#"/></label>
                            <input type="text" name="order_id" value="" />
                        </div>
                        <div class="row">
                            <input type="submit"  title="Submit" value="Submit" name="addbut">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

function updateStatus(store_id, status){
	
	$.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    } }); 
	var url = '/admin/crosssell/updateStatus.htm';
	
	var formData = 'status='+status+'&store_id='+store_id;
	$.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: "html",
        cache: false,
        timeout:1800000,
        success: function (responseText) {
        	var responseObj = jQuery.parseJSON(responseText);
        	if(responseObj.STATUS == 'error'){
        		alert("Activate failed due to some Error.");                
            } else if(responseObj.STATUS == 'success'){
            	alert("Activate/De-Activate success.");
            	$('#enableLink'+store_id).toggle();
            	$('#disableLink'+store_id).toggle();
            } else {
            	alert("Could not Activate/De-Activate.");
            }
        	$.unblockUI();
        },
        failure: function (responseText) {
        	alert("Activate failed."); 
        	$.unblockUI();
        }
  	}); 
	
}

/* $(function() {
	$( "#effective_from" ).datepicker({dateFormat: 'yy-mm-dd'});
}); */

$(function() {
	$( "#valid_upto" ).datepicker({dateFormat: 'yy-mm-dd'});
});
</script>
<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/jHtmlArea.css?v=${mt:keyValue("version.no")}" type="text/css" media="all" />
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jHtmlArea-0.7.5.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">
$(function() {
	$( "#description" ).htmlarea();
	$( "#headDesc" ).htmlarea();
	$( "#tnc" ).htmlarea();
	$( "#mailTnc" ).htmlarea();
});

</script>