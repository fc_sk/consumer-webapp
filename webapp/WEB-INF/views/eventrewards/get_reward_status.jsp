<%@include file="/WEB-INF/includes/taglibs.jsp"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <c:if test="${not empty param.message}">
        <div class="span12" style="background-color:yellow;padding:10px;"><c:out value="${param.message}"/></div>
    </c:if>
</div>
<div class="row">
<div class="span2">
    <ul class="nav nav-list">
        <li class="nav-header">
            <a href="/admin/eventrewards/getActivities.htm" title="Displays the list of activities and the details of the successfully given rewards">Reward Activities</a>
        </li>
        <li class="nav-header">
            <a href="/admin/eventrewards/getRewardStatus.htm" title="Displays the statuses(failure reasons in case of failure, reward details in case of success) of rewards">Reward Statuses</a>
        </li>
        <li class="nav-header">
            <a href="/admin/eventrewards/getCouponRewards.htm" title="Displays the details of coupon reward that can be searched by a mobile number">Coupon Rewards</a>
        </li>
    </ul>
</div>

<div class="span10">
<c:if test="${page eq 'activities' }">
    <div id="rewardActivitiesDiv"  class="sectionDiv">
    <h4>Reward Activities</h4>
        <b>Select Event <sup>*</sup></b>
        <form name="rewardactivities" action="/admin/eventrewards/getActivities">
            <select class="input" name="eventType" required>
                <option value="">Event Type</option>
                <c:forEach items="${eventsList}" var="event">
                    <option value="${event}">${event.description}</option>
                </c:forEach>
            </select><br/>

            <b>User's email address<sup>*</sup></b><br/><input type="text" placeholder="email" name="emailId" required />
            <c:if test="${not empty message}"><span style="color:red;padding:10px;">${message}</span></c:if><br/>
            <input type="submit" value="Submit" class="btn btn-primary"/>
            <input type="reset" value="Reset" class="btn btn-primary"/>
        </form>
        <div style="width:100%;overflow:auto; <c:if test="${empty activities}"> display:none</c:if>">
            <b>Activities:</b>
            <table class="table table-striped" style="table-layout:fixed;word-break:break-word;">
                <thead>
                    <tr>
                        <th style="width:4%">Event Id</th>
                        <th style="width:5%">Event Name</th>
                        <th style="width:5%">Session Id</th>
                        <th style="width:5%">Lookup Id</th>
                        <th style="width:5%">Thread Id</th>
                        <th style="width:6%">Activity</th>
                        <th style="width:6%">Timestamp</th>
                    </tr>
                </thead>
                <tbody style="word-break:break-all;">
                    <c:forEach items="${activities}" var="activity">
                        <tr>
                            <td>${activity.eventId}</td>
                            <td>${activity.eventName}</td>
                            <td>${activity.sessionId}</td>
                            <td>${activity.lookupId}</td>
                            <td>${activity.threadId}</td>
                            <td>${activity.activity}</td>
                            <td>${activity.timestamp}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
</c:if>

<div class="span10">
<c:if test="${page eq 'statuses' }">
    <div id="rewardActivitiesDiv"  class="sectionDiv">
    <h4>Reward Statuses</h4>
        <b>Select Event <sup>*</sup></b>
        <form name="rewardstatuses" action="/admin/eventrewards/getRewardStatus">
            <select class="input" name="eventType" required>
                <option value="">Event Type</option>
                <c:forEach items="${eventsList}" var="event">
                    <option value="${event}" <c:if test="${event == selectedEventType}">selected</c:if> >${event.description}</option>
                </c:forEach>
            </select><br/>

            <b>Select Your Key to Search <sup>*</sup></b><br/>
            <select class="input" name="searchKey" required>
                <option value="">Key</option>
                <c:forEach items="${searchKeys}" var="searchKey">
                    <option value="${searchKey}" <c:if test="${searchKey == selectedSearchKey}">selected</c:if> >${searchKey.description}</option>
                </c:forEach>
            </select><br/>

            <b>Value<sup>*</sup></b><br/><input type="text" placeholder="value" name="value"  value = "${value}" required/>
            <c:if test="${not empty message}"><span style="color:red;padding:10px;">${message}</span></c:if><br/>

            <input type="submit" value="Submit" class="btn btn-primary"/>
            <input type="reset" value="Reset" class="btn btn-primary"/>
        </form>
        <div style="width:100%;overflow:auto; <c:if test="${empty statuses}"> display:none</c:if>">
            <b>Results:</b>
            <table class="table table-striped" style="table-layout:fixed;word-break:break-word;">
                <thead>
                    <tr>
                        <th style="width:2%">Event Id</th>
                        <th style="width:4%">Event Name</th>
                        <th style="width:7%">User Email</th>
                        <th style="width:4%">Order Id</th>
                        <th style="width:4%">Recharge Mobile Number</th>
                        <th style="width:3%">Status</th>
                        <th style="width:10%">Details</th>
                        <th style="width:4%">Timestamp</th>
                    </tr>
                </thead>
                <tbody style="word-break:break-all;">
                    <c:forEach items="${statuses}" var="status">
                        <tr>
                            <td>${status.growthEventId}</td>
                            <td>${status.eventName}</td>
                            <td>${status.emailId}</td>
                            <td>${status.orderId}</td>
                            <td>${status.rechargeMobileNumber}</td>
                            <td>${status.status}</td>
                            <td style="font-size: 12px;">${status.details}
                            <c:if test="${status.retriggerSMSButtonEnable}"><br>
                            <input type = "button" id = "triggerButton" style="font-size: 12px;" onclick = "retriggerSMS(this, '${status.rechargeMobileNumber}', '${status.rewardValue}', '${status.growthEventId}')" value="Retrigger SMS"></c:if></td>
                            <td>${status.createdAt}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
</c:if>
<c:if test="${page eq 'couponrewards' }">
    <div id="couponRewardsDiv"  class="sectionDiv">
    <h4>Coupon Rewards</h4>
        <b>Select Event</b>
        <form name="couponRewards" action="/admin/eventrewards/getCouponRewards">
            <select class="input" name="eventType">
                <option value="">Event Type</option>
                <c:forEach items="${eventsList}" var="event">
                    <option value="${event}">${event.description}</option>
                </c:forEach>
            </select><br/>

            <b>Recharge Mobile Number<sup>*</sup></b><br/><input type="text" placeholder="mobileNumber" name="serviceNumber" required />
            <c:if test="${not empty message}"><span style="color:red;padding:10px;">${message}</span></c:if><br/>
            <input type="submit" value="Submit" class="btn btn-primary"/>
            <input type="reset" value="Reset" class="btn btn-primary"/>
        </form>

        <div style="width:100%;overflow:auto; <c:if test="${empty rewards}"> display:none</c:if>">
            <b>Rewards List</b>
            <table class="table table-striped" style="table-layout:fixed;word-break:break-word;">
                <thead>
                    <tr>
                        <th style="width:4%">Event Id</th>
                        <th style="width:8%">Event Name</th>
                        <th style="width:6%">User Id</th>
                        <th style="width:18%">User Email</th>
                        <th style="width:10%">Order Id</th>
                        <th style="width:6%">FreeFund Class Id</th>
                        <th style="width:6%">Coupon Code</th>
                        <th style="width:20%">TimeStamp</th>
                   </tr>
                </thead>
                <tbody style="word-break:break-all;">
                    <c:forEach items="${rewards}" var="reward">
                        <tr>
                            <td>${reward.eventId}</td>
                            <td>${reward.eventName}</td>
                            <td>${reward.userId}</td>
                            <td>${reward.userEmail}</td>
                            <td>${reward.orderId}</td>
                            <td>${reward.freefundClassId}</td>
                            <td>${reward.couponCode}</td>
                            <td>${reward.timestamp}</td>
                            <td><input type = "button" id = "triggerButton" onclick = "retriggerSMS(this, '${reward.serviceNumber}', '${reward.couponCode}', '${reward.eventId}')" value="Retrigger SMS"></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
</c:if>
</div>
</div>

<script>
    function retriggerSMS(button, serviceNumber, rewardValue, eventId) {
        $.ajax({
            url: '/admin/eventrewards/retriggerSMS?serviceNumber='+serviceNumber+'&rewardValue='+rewardValue+'&eventId='+eventId,
            type:'GET',
            dataType: 'JSON'
        });
        $(button).prop('disabled',true);
        setTimeout(function(){$(button).prop('disabled',false)}, 300000); // disabling button for 5 mins
    }
</script>
