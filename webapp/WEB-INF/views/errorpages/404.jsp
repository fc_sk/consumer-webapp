<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../../layouts/googleCode.jsp"%>
    <meta charset="utf-8">
    <title>Error-404</title>

    <link href="${mt:keyValue("cssprefix1")}/singlepage/b.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css" >
    <link href="${mt:keyValue("cssprefix1")}/singlepage/freecharge.css?v=${mt:keyValue("version.no")}" rel="stylesheet" type="text/css" >
    <style>
        html, body {
            height: 90%;
        }
        .full-container {
            display: table;
            text-align: center;
            height: 100%;
            width: 100%;
        }
        .center-image {
            display: table-cell;
            vertical-align: middle;
        }

    </style>
</head>
<body>

    <jsp:include page="../../singlepage/header.jsp"></jsp:include>

    <div class="full-container">
        <div class="center-image">
            <img src="${mt:keyValue("imgprefix1")}/images/404.png" alt="Freecharge.in - Page not found(404)" />
        </div>
    </div>



</body>
</html>
