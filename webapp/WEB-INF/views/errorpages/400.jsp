<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../../layouts/googleCode.jsp"%>
    <meta charset="utf-8">
    <title>Error-400</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            background-color: #396db5;
            position: relative;
        }
        #mid-section {
            font-family: Tahoma, sans-serif;
            font-size: 20px;
            text-align: center;
            position: relative;
            top: 300px;
        }
        .logo {
            margin-bottom: 30px;
        }
        .logo a {
            outline: none;
        }
        .logo a img {
            border: none;
        }
        #mid-section p {
        }
    </style>
</head>
<body>

<div class="mainContainer">
	<div class="pageWidth clearfix">

		<div class="errorMsgWrapp  ">
            <img src="${mt:keyValue("imgprefix1")}/images/oops.png" width="130" height="130" >
			<h5 class="pageHeader ">We're sorry!!!</h5>
			<p>We are unable to process your request.<p>
			<p>
				You may <a href="/app/contactus.htm">contact us</a> or go back to <a
					href="${mt:keyValue("hostprefix")}">homepage</a>.
			</p>
		</div>
		<!-- /.successMsgWrapper -->
	</div>
</div>

</body>
</html>
