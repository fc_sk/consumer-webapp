<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="../../layouts/googleCode.jsp"%>
    <meta charset="utf-8">
    <title>Under maintenance-503</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            background-color: #396db5;
            position: relative;
        }
        #mid-section {
            font-family: Tahoma, sans-serif;
            font-size: 20px;
            text-align: center;
            position: relative;
            top: 300px;
        }
        .logo {
            margin-bottom: 30px;
        }
        .logo a {
            outline: none;
        }
        .logo a img {
            border: none;
        }
        #mid-section p {
        }
    </style>
</head>
<body>


<div id="mid-section">
    <div class="logo">
        <a href="/"><img src="${mt:keyValue("imgprefix1")}/images/logo/freecharge.png?v=${mt:keyValue("version.no")}" alt="FreeCharge.in"></a>
    </div>
    <p>We are under maintenance right now and will be back up soon. Thank you for your patience.</p>
</div>

</body>
</html>

