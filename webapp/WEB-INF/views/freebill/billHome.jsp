<%@page errorPage="/WEB-INF/views/fc/error/error.jsp"%>
<%@ page import="com.freecharge.common.util.FCConstants" %>
<%@ page import="com.freecharge.common.util.SpringBeanLocator" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeSession" %>
<%@ page import="com.freecharge.common.framework.session.FreechargeContextDirectory" %>
<%@ page import="java.util.Map, java.util.List" %>
<%@ page import="com.freecharge.web.util.WebConstants" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="springF" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<div>
  <h3>Bill payment</h3>
  <form name="billpayment" action="#">
    <label>Mobile number</label>
    <input id="bill-mobile-number" type="text" name="mobile-number" onblur="fetchMobileBill()" tabindex=1>
    <select id="bill-merchant-id" tabindex=3>
      <option value="">Select one</option>
      <c:forEach items="${merchantList}" var="itr">
      <option value="${itr.merchantID}">${itr.displayName}</option>
      </c:forEach>
    </select>
    <span id="msg" style="background-color: #ccaa88; font-size:150%; display: none;">Could not get details</span>
    <label>Amount</label>
    <input id="bill-amount" type="text" name="amount" tabindex=2 />
    <input id="bill-proceed" type="button" value="Proceed" onclick="proceedMobileBill()" />
    <span id="proceedFailed" style="background-color: #ccaa88; font-size:150%; display: none;">Proceed failed</span>
    <input id="bill-continue" style="display: none;" type="button" value="Continue" onclick="continueMobileBill()" />
  </form>
</div>

    <springF:form action="/app/saveCrossSell.htm" name="couponForm" method="post" id="billContinue">
        <input type="hidden" id="hidden-inputLookupId"        name="lookupID"       value="" />
        <input type="hidden" id="hidden-txn-homepage-id"      name="homePageId"     value="" />
        <input type="hidden" id="hidden-bill-txn-homepage-id" name="billHomePageId" value="" />
        <input type="hidden" id="hidden-pro-type"             name="proType"        value=""/>
        <input type="hidden" id="hidden-coupon-type"          name="type"           value="coupon" />

        <input type="hidden" id="hidden-selectedCoupons"   name="itemIdArray"  value="" />
        <input type="hidden" id="hidden-service-no-id"     name="serviceNo"    value="" />
        <input type="hidden" id="hidden-operator-id"       name="operatorId"   value="" />
        <input type="hidden" id="hidden-operator-name"     name="operatorName" value="" />
        <input type="hidden" id="hidden-postpaid-number"   name="postpaidNumber"   value="" />
        <input type="hidden" id="hidden-postpaid-operator" name="postpaidOperator" value="" />
        <input type="hidden" id="hidden-amount-id"         name="amount"       value="" />

        <input type="hidden" id="hidden-service-charge"    name="hidden-service-charge" value="10" />
        <input type="hidden" id="hidden-handling-charges"  name="serviceCharge"         value="0" />
    </springF:form>

<script type="text/javascript">
  function fetchMobileBill() {
    var mobileNumber = $("#bill-mobile-number").val();
    var url = "/protected/bill/postpaid/mobile/" + mobileNumber + "/provider";
    $.getJSON(url, function(data) {
      if (data.result === 'success') {
        $("#bill-merchant-id").val(data.merchantID);
        if (!(typeof data.amount === "undefined")) {
          $("#bill-amount").val(data.amount);
        }
      } else {
        flashMessage($("#msg"));
      }
    });
  }
  function flashMessage(spanDiv) {
    $(spanDiv).slideDown(function() {
      setTimeout(function() {
        $(spanDiv).slideUp();
      }, 5000);
    });
  }
  function proceedMobileBill() {
    var mobileNumber = $("#bill-mobile-number").val();
    var merchantID   = $("#bill-merchant-id").val();
    var amount       = $("#bill-amount").val();
    var url = "/protected/bill/postpaid/mobile/" + mobileNumber + "/merchant/" + merchantID + "/amount/" + amount;
    $.post(url, {}, function(data) {
      if (data['string'] === 'success') {
        $("#hidden-inputLookupId").val(data['lookupId']);
        $("#hidden-bill-txn-homepage-id").val(data['billTxnHomePageID']);
        $("#hidden-pro-type").val("M");
        $("#hidden-amount-id").val(amount);
        $("#hidden-postpaid-number").val(mobileNumber);
        $("#hidden-postpaid-operator").val($("#bill-merchant-id option:selected").text());
        // make Continue button visible
        $("#bill-continue").show();
      } else {
        var msg = $("#proceedFailed");
        msg.html(JSON.stringify(data));
        flashMessage(msg);
      }
    }, "JSON");
  }
  function continueMobileBill() {
    $("#billContinue").submit();
  }
</script>
