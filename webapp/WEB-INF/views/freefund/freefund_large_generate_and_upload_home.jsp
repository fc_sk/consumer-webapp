<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %><html>

<section class="container">
    <div class="row">
        <c:if test="${not empty param.message}">
            <div class="span12" style="background-color:yellow;padding:10px;"><c:out value="${param.message}"/></div>
        </c:if>
    </div>
    <div class="row">
        <div class="span2">
            <ul class="nav nav-list">
                <li class="nav-header">
                    <a href="/admin/freefund/largegenerateandupload/home/generatecodes.htm">Generate Codes</a>
                </li>
                <li class="nav-header">
                    <a href="/admin/freefund/largegenerateandupload/home/downloadcodes.htm">Download Codes</a>
                </li>
                <li class="nav-header">
                    <a href="/admin/freefund/largegenerateandupload/home/uploadcodes.htm">Upload Codes Manually</a>
                </li>
            </ul>
        </div>
        <div class="span10">
            <c:if test="${ page eq 'generateCodes'}">
                <div  id="generateCodesDiv" class="sectionDiv" >
                    <h4>Generate Freefund Codes</h4>
                    <form:form  name="freefundgenerator" cssClass="form" id="crossselluploadform" action="/admin/freefund/largegenerateandupload/generate.htm" method="post" >
                        <b>Choose a class</b><br/>
                        <select class="input" name="class_id" title="Select Class" >
                            <c:forEach items="${freefundClasses }" var="freefundClass">
                                <option value="${freefundClass.id }">${freefundClass.name}</option>
                            </c:forEach>
                        </select><br>
                        <b><spring:message code="prefix" text="#prefix#"/> <a href="#" rel="tooltip" class="tip" title="2 or 3 letter prefix to add in front of each code generated, helps to avoid duplicates" data-original-title="Help">[?]</a></b> <br/>
                        <input type="text" name="prefix"  onkeypress="return alphanumeric_only(event);"/></br>
                        <br>
                        <b><spring:message code="no.codes" text="#no.codes#"/> <a href="#" rel="tooltip" class="tip" title="Number of codes to be generated" data-original-title="Help">[?]</a></b><br/>
                        <input type="text" id="code" name="codes" value="" onkeypress="return isNumberKey(event,2,id);"  onblur="return populateLength(event);"/> <br/>
                        <br>
                        <b><spring:message code="length" text="#length#" /> <a href="#" rel="tooltip" class="tip" title="Length of each promocode , please use the suggested code length before you generate" data-original-title="Help">[?]</a></b><br/>
                        <input type="text" id="len" name="length" onchange="checkForDesiredLength();"  onkeypress="return isNumberKey(event,2,id);"/>
                        <div style="background-color: #cc0000; border : 1px solid #cc1111; color : #ffffff;" id="desiredLengthMsg"></div>
                        <br>
                        <input type="submit"  title="Submit" value="Generate" class="btn btn-primary" id="butsubmit" name="addbut">
                    </form:form>
                </div>
            </c:if>
            <c:if test="${ page eq 'downloadCodes'}">
                <div id="codeDownloadDiv" class="sectionDiv">
                    <b>Choose a class</b><br/>

                    <select class="input" id="classes" name="class_id" title="Select Class" >
                        <c:forEach items="${freefundClasses }" var="freefundClass">
                            <option value="${freefundClass.id }">${freefundClass.name}</option>
                        </c:forEach>
                    </select><br>

                    <table id="codeStatus" width="auto" border="1">
                    </table>
                    <script>
                        $(document).ready(function() {
                            addRows();
                        });

                        $("#classes").change(function() {
                            deleteRows();
                            addRows();
                        });

                        function addRows() {
                            tableElem = document.getElementById("codeStatus");
                            var downloadData = getData($("#classes").val(), "");
                            var length = downloadData.data.length;
                            for (var i = 0; i < length; i++) {
                                rowElem = document.createElement('tr');
                                colElem = document.createElement('td');
                                var span, a, data;
                                span = document.createElement('span');
                                data = downloadData.data[i];
                                span.innerHTML = data.numberOfCodes + " codes created by " +
                                        data.createdByEmailId + " at " + data.createdAt + " ";
                                colElem.appendChild(span);
                                a = document.createElement('a');
                                a.href='/admin/freefund/largegenerateandupload/getCodes/'+ data.campaignId +
                                        '?&timestamp=' + data.fileCreationTimeStamp +
                                        '&numberOfFiles=' + data.numberOfFiles + '&prefix=' + data.prefix +
                                        '&codeLength=' + data.codeLength;
                                a.innerHTML = "Download"
                                colElem.appendChild(a);
                                rowElem.appendChild(colElem);
                                tableElem.appendChild(rowElem);
                            }
                        }

                        function deleteRows() {
                            var table = document.getElementById("codeStatus");
                            table.innerHTML = "";
                        }

                    </script>
                </div>
            </c:if>
            <c:if test="${ page eq 'uploadCodes'}">
                <div id="codeUploadDiv" class="sectionDiv">
                    <b>Choose a class</b><br/>

                    <select class="input" id="classes" name="class_id" title="Select Class" >
                        <c:forEach items="${freefundClasses}" var="freefundClass">
                            <option value="${freefundClass.id}">${freefundClass.name}</option>
                        </c:forEach>
                    </select><br>

                    <table id="upload" width="auto" border="1">
                    </table>
                    <script>
                        $(document).ready(function() {
                            addRows();
                        });

                        $("#classes").change(function() {
                            deleteRows();
                            addRows();
                        });

                        function addRows() {
                            tableElem = document.getElementById("upload");
                            var downloadData = getData($("#classes").val(), "Pending");
                            var length = downloadData.data.length;
                            for (var i = 0; i < length; i++) {
                                rowElem = document.createElement('tr');
                                colElem = document.createElement('td');
                                var span, a, data;
                                span = document.createElement('span');
                                data = downloadData.data[i];
                                span.innerHTML = data.numberOfCodes + " codes created by " +
                                        data.createdByEmailId + " at " + data.createdAt + " ";
                                colElem.appendChild(span);
                                a = document.createElement('a');
                                a.href='/admin/freefund/largegenerateandupload/runjob?classId='+ data.campaignId +
                                        '&timestamp=' + data.fileCreationTimeStamp +
                                        '&numberOfFiles=' + data.numberOfFiles + '&prefix=' + data.prefix +
                                        '&codeLength=' + data.codeLength + '&emailId=' + data.createdByEmailId +
                                        '&fileCount=' + data.fileCount;
                                a.innerHTML = "Upload"
                                colElem.appendChild(a);
                                rowElem.appendChild(colElem);
                                tableElem.appendChild(rowElem);
                            }
                        }

                        function deleteRows() {
                            var table = document.getElementById("upload");
                            table.innerHTML = "";
                        }
                    </script>
                </div>
            </c:if>
        </div>
    </div>
</section>

<script type="text/javascript">

    function checkForDesiredLength(){
        $('#desiredLengthMsg').html("");
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });

        var url = '/admin/freefund/largegenerateandupload/generate/validate.htm?codes='+$('input[name=codes]').val()+"&length="+$('input[name=length]').val();

        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            timeout:1800000,
            success: function (responseText) {
                if(responseText.status == 'fail') {
                    $('#desiredLengthMsg').html(responseText.message);
                }
                $.unblockUI();
            },
            failure: function (responseText) {
                alert("Validation failed.");
                $.unblockUI();
            }
        });
    }

    function alphanumeric_only(event)
    {
        var keycode;
        keycode=event.keyCode?event.keyCode:event.which;
        var splChar = "%";
        if ((keycode == 32 || keycode == 8 || keycode == 46) || (keycode >= 47 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 97 && keycode <= 122)) {
            return true;
        }
        else {
            // alert("Sorry You can not insert Special Character");
            return false;
        }
        return true;
    }
    //Populate length field based on num codes entered
    function populateLength(evt){
        $('#desiredLengthMsg').html("");
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });
        var url = '/admin/freefund/largegenerateandupload/getLengthForCodes?codes='+$('input[name=codes]').val();

        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            timeout:1800000,
            success: function (responseText) {
                if(responseText.status == 'fail') {
                    $('#desiredLengthMsg').html(responseText.message);
                } else{
                    $(len).val(responseText.length);
                }
                $.unblockUI();
            },
            failure: function (responseText) {
                alert("Validation failed.");
                $.unblockUI();
            }
        });
    }

    function getData(freefundClassId, status) {
        var url = '/admin/freefund/largegenerateandupload/getData?freefundClassId=' + freefundClassId +
                '&status=' + status;
        var data;
        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            timeout:1800000,
            async: false,
            success: function (responseText) {
                data = responseText;
            },
            failure: function (responseText) {
                data =  responseText;
            }
        });
        return data;
    }
</script>

