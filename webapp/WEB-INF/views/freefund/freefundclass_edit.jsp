<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="mainContainer"  >
    <div class="pageWidth clearfix form">
    	<c:if test="${not empty param.message}">
    	<div style="background-color:yellow;padding:10px;"><c:out value="${param.message}"/></div>
    	</c:if>
    	 <div class="flex-layout clearfix">
        <div class="one" style="float: left;" >
            	<div class="left-content">
    			<h1><spring:message code="list.freefund.classes" text="#list.freefund.classes#"/></h1>
					<c:forEach items="${freefundclasview}" var="freefundclass">
      				<p>${freefundclass.name}
        	 			<a href="/admin/freefund/viewAndedit.htm?class_id=${freefundclass.id}"><spring:message code="edit" text="#edit#"/></a><br>
    				 </p>
 				 </c:forEach>
 		 	</div>
 		 </div>
		 <div class="three"  style="float: left;">
            <div class="right-content">
    	     <h1><spring:message code="edit.save" text="#edit.save#"/></h1> <br>
    	       <c:set var="freefundClass" value="${freefundClass }"></c:set>
			  	 <form:form  name="freefunduploadform" action="/admin/freefund/save.htm" method="post" cssClass="form">
			   		  <input type="hidden" value="${freefundClass.id}"  name="class_id">
					    <b><spring:message code="name" text="#name#"/></b><br/>
				  	    <input type="text" name="name" value="${freefundClass.name}" /><spring:message code="icici.axis" text="#icici.axis#"/><br/>
				  	    
				  	    <c:choose>
				  	    	<c:when test="${empty freefundClass.promoEntity}">
				  	    		<b><spring:message code="select.payment.type" text="#select.payment.type#"/></b> (Once saved, can not be reverted for this class) <br/>
							    <select class="input" name="promo_entity" title="Select Payment Type" >
									<option value="all-payment-options">All Payment Options</option>
									<option value="icici-netbanking">ICICI Net Banking</option>
									<option value="hdfc-debitcard">HDFC Debit Card</option>
									<option value="redeem-to-fcbalance">Redeem to FCBalance</option>
								</select><br>
								<script type="text/javascript">$("select[name='promo_entity']").val('${freefundClass.promoEntity}');</script>
				  	    	</c:when>
				  	    	<c:otherwise>
				  	    		<input type="hidden" name="promo_entity" value="${freefundClass.promoEntity}" />
				  	    	</c:otherwise>
				  	    </c:choose>
				  	    
						<b><spring:message code="freefund.value" text="#freefund.value#"/></b><br/>
						<input type="text" name="freefund_value"  value="${freefundClass.freefundValue}" /><br/>
						<b><spring:message code="minimum.recharge.amount" text="#minimum.recharge.amount#"/></b><br/>
						<input type="text" name="min_recharge_value"  value="${freefundClass.minRechargeValue}" />
				  	    <br/>
				  	    <b>Discount Type</b><br/>
				  	    <c:set var="discountType" value="${freefundClass.discountType}" />
						<select class="input" name="discount_type" title="Select Discount Type" >							
							<option value="FLAT" <c:if test="${discountType eq 'FLAT'}" >selected="selecte"</c:if> >FLAT</option>
							<option value="PERCENT" <c:if test="${discountType eq 'PERCENT'}" >selected="selecte"</c:if> >PERCENT</option>
						</select><br/>
						<b>Max Discount</b><br/>
					    <input type="text" name="max_discount" value="<c:if test="${discountType eq 'PERCENT'}" >${freefundClass.maxDiscount}</c:if>" /> (# Applicable for <b>PERCENT</b> Discount Type only)<br/>
						<b>Apply Condition Id</b><br/>
				    	<%-- <input type="text" name="max_redemptions" value="${freefundClass.maxRedemptions}" /> (# of times user can avail a freefund/promo code of this class)<br/> --%>
				    	<input type="text" name="apply_condition_id" value="${freefundClass.applyConditionId}" /> (# apply condition id which has constraints configured for the usage of the codes of this class)<br/>
				    	<b>Payment Condition Id</b><br/>
				    	<input type="text" name="payment_condition_id" value="${freefundClass.paymentConditionId}" /> (# payment condition id which has constraints configured for the usage of the codes of this class)<br/>
						<b><spring:message code="description" text="#description#"/></b><br/>
						<textarea name="description">${freefundClass.description}</textarea> <br/>
						<b><spring:message code="valid.from" text="#valid.from#"/></b><br/>
						<input type="text" class="calender_input" name="valid_from" class="calender_input" value="<fmt:formatDate pattern='dd/MM/yyyy' value="${freefundClass.validFrom.time}"/>"/> (dd-mm-yyyy)
						<input type="text" class="calender_input" name="valid_from_time" class="calender_input" value="<fmt:formatDate pattern='HH:mm:ss' value="${freefundClass.validFrom.time}"/>"/> (HH:mm:ss)  24hr format 
						<br>
						<b><spring:message code="valid.upto" text="#valid.upto#"/></b> <br>
				   		<input type="text" class="calender_input" name="valid_upto"  value="<fmt:formatDate pattern='dd/MM/yyyy' value="${freefundClass.validUpto.time}"/>" /> (dd-mm-yyyy)
				   		<input type="text" class="calender_input" name="valid_upto_time"  value="<fmt:formatDate pattern='HH:mm:ss' value="${freefundClass.validUpto.time}"/>" /> (HH:mm:ss)  24hr format <br><br>
				  	 <input type="submit"  title="Submit" value="Save" class="button" id="butsubmit" name="addbut">
		     <br><br>
		     </form:form>
        </div>  
     </div> 
   </div>
 </div>
</div>