<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@ page import="com.freecharge.freefund.util.FreefundUtil"  %>
<%@ page import="com.freecharge.promo.util.PromocodeConstants"  %>
<section class="container">
<div class="row">
    <c:if test="${not empty param.message}">
        <div class="span12" style="background-color:yellow;padding:10px;"><c:out value="${param.message}"/></div>
    </c:if>
</div>
<div class="row">
<div class="span2">
    <ul class="nav nav-list">
        <li class="nav-header">
            <a href="/admin/freefund/home/viewclasses.htm">View Classes</a>
        </li>
        <li class="nav-header">
            <a href="/admin/freefund/home/createclass.htm">Create Class</a>
        </li>
        <li class="nav-header">
            <a href="/admin/ardeal/condition/edit.htm">Create Condition</a>
        </li>
        <li class="nav-header">
            <a href="/admin/ardeal/home.htm">View Conditions</a>
        </li>
        <li class="nav-header">
            <a href="/admin/freefund/bulkreset.htm">Bulk Reset</a>
        </li>

    </ul>
</div>
<div class="span10">
<c:if test="${page eq 'viewclasses' }">
    <div class="pageWidth clearfix form">
        Filter Classes By
        <ul class="nav nav-pills">
            <li><a class="fltr-item " href="javascript:void(0)" onclick="$('.allclasses').hide();$('.activestatus1').show();highlight(this)">Active</a></li>
            <li><a class="fltr-item " href="javascript:void(0)" onclick="$('.allclasses').hide();$('.activestatus0').show();highlight(this)">Expired</a></li>
            <li><a class="fltr-item " href="javascript:void(0)" onclick="$('.allclasses').hide();$('.all-payment-options').show();highlight(this)">All Payment Options</a></li>
            <li><a class="fltr-item " href="javascript:void(0)" onclick="$('.allclasses').hide();$('.icici-netbanking').show();highlight(this)">ICICI Net Banking</a></li>
            <li><a class="fltr-item " href="javascript:void(0)" onclick="$('.allclasses').hide();$('.hdfc-debitcard').show();highlight(this)">HDFC Debit Card</a></li>
            <li><a class="fltr-item " href="javascript:void(0)" onclick="$('.allclasses').hide();$('.generic-cashback-promocode').show();highlight(this)">Generic Cashback Promocode</a></li>
            <li><a class="fltr-item " href="javascript:void(0)" onclick="$('.allclasses').hide();$('.generic-discount-promocode').show();highlight(this)">Generic Discount Promocode</a></li>
        	<li><a class="fltr-item " href="javascript:void(0)" onclick="$('.allclasses').hide();$('.cashback-promocode').show();highlight(this)">Cashback Promocode</a></li>
        </ul>
        <script type="text/javascript">
            function highlight(li) {
                $('.fltr-item').css('background-color', '#ffffff');
                $(li).css('background-color', '#dddddd');
            }
        </script>
        <h4>List of Promocode / Freefund classes</h4>
        <hr/>
        <c:forEach items="${freefundClasses}" var="freefundclass">
            <div class="span4 activestatus${freefundclass.isActive} allclasses ${freefundclass.promoEntity}" >
                <a href="/admin/freefund/home/editclass.htm?class_id=${freefundclass.id}">${freefundclass.name}</a>
            </div>
        </c:forEach>
    </div>
</c:if>
<c:if test="${ (page eq 'createclass') or (page eq 'editclass')}">

    <div id="classCreateDiv"  class="sectionDiv">
        <h4>Create freefund class</h4>
        <form  name="freefunduploadform" <c:if test="${not empty param.class_id}">action="/admin/freefund/class/save.htm"</c:if> <c:if test="${empty param.class_id}">action="/admin/freefund/class/upload.htm"</c:if> method="post" cssClass="form">
            <c:if test="${not empty param.class_id}">
                <input type="hidden" value="${freefundClass.id}"  name="class_id">
            </c:if>
            <b><spring:message code="name" text="#name#"/> <a href="#" rel="tooltip" class="tip" title="Give a name for the class for identification, like pepsi promo code, kingfisher etc" data-original-title="Help">[?]</a></b>
            	<span style="font-size:12px;">(# For CashBack offers it will be used for CashBack Mailers)</span><br/>
            <input type="text" name="name" value="${freefundClass.name}" />
            <br/>

            <select id="NewCampaignSelector" class="input" name="isNewCampaign" title="Select Campaign framework">
                <option value="false" <c:if test="${empty freefundClass.isNewCampaign or freefundClass.isNewCampaign eq false}"> selected="true" </c:if>>Old Promocode framework</option>
                <option value="true" <c:if test="${freefundClass.isNewCampaign eq true}"> selected="true" </c:if>>New campaign framework</option>
                <input type="hidden" name="isNewCampaign" value="$(NewCampaignSelector).val()" />
            </select><br>

            <c:choose>
                <c:when test="${empty freefundClass.promoEntity}">
                    <b>Select a class type</b><a href="#" rel="tooltip" class="tip" title="Once saved, can not be reverted for this class" data-original-title="Help">[?]</a><br/>
                    <select class="input" name="promo_entity" title="Select Payment Type" style="width: 500px;" >
 						<option value="all-payment-options">Unique Promocode With Instant Disocunt</option>
						<option value="cashback-promocode">Unique Promocode With Cashback</option>
						<option value="generic-discount-promocode">Generic Promocode With Instant Disocunt</option>
						<option value="generic-cashback-promocode">Generic Promocode With Cashback</option>
						<option value="redeem-to-fcbalance">Freefund Class To Redeem To FCBalance</option>
                     </select><br>
                    <script type="text/javascript">$("select[name='promo_entity']").val('${freefundClass.promoEntity}');</script>
                </c:when>
                <c:otherwise>
                	<b>Class Type :</b> <br /> 
                	<c:choose>
                		<c:when test="${freefundClass.promoEntity eq 'all-payment-options'}" >Unique Promocode With Instant Disocunt</c:when>
                		<c:when test="${freefundClass.promoEntity eq 'cashback-promocode'}" >Unique Promocode With Cashback</c:when>
                		<c:when test="${freefundClass.promoEntity eq 'generic-discount-promocode'}" >Generic Promocode With Instant Disocunt</c:when>
                		<c:when test="${freefundClass.promoEntity eq 'generic-cashback-promocode'}" >Generic Promocode With Cashback</c:when>
                		<c:when test="${freefundClass.promoEntity eq 'redeem-to-fcbalance'}" >Freefund Class To Redeem To FCBalance</c:when>
                	</c:choose><br /><br />
                    <input type="hidden" name="promo_entity" value="${freefundClass.promoEntity}" />
                </c:otherwise>
            </c:choose>          
			<b>Campaign Sponsorship</b>
			<a href="#" rel="tooltip" class="tip" title="Select Campaign Sponsorship from drop down" data-original-title="Help">[?]</a><br />  
			<select class="input" name="campaign_sponsorship" title="Select Campaign Sponsorship">
			 	<c:forEach var="CAMPAIGN_SPONSORSHIP" items="<%=FreefundUtil.CampaignSponsorshipEnum.values()%>" >
			 		 <option value="${CAMPAIGN_SPONSORSHIP}" <c:if test="${freefundClass.campaignSponsorship eq CAMPAIGN_SPONSORSHIP}" >
			 		 selected="selected"</c:if> >${CAMPAIGN_SPONSORSHIP}</option>
			 	</c:forEach>
			</select> 				
			<br /><b>Campaign Description</b>
			<a href="#" rel="tooltip" class="tip" title="Detailed Campaign Description Required" data-original-title="Help">[?]</a><br />
			<textarea name="campaign_description">${freefundClass.campaignDescription}</textarea>
			<br /> 			
			<b>Promocode/Freefund Value</b> <a href="#" rel="tooltip" class="tip" title="How much discount to be given or how much to be credited to wallet" data-original-title="Help">[?]</a> <br/>
            <input type="text" name="freefund_value"  value="${freefundClass.freefundValue}" /><br/>
            <b>Minimum recharge value</b> <a href="#" rel="tooltip" class="tip" title="For promocode redemption wht is the minimum recharge / billpay needed" data-original-title="Help">[?]</a><br/>
            <input type="text" name="min_recharge_value"  value="${freefundClass.minRechargeValue}" />
            <br/>
            <b>Discount Type</b> <a href="#" rel="tooltip" class="tip" title="In promocode case, giving a flat disocunt or a percentage on recharging amount" data-original-title="Help">[?]</a> <br/>
            <c:set var="discountType" value="${freefundClass.discountType}" />
            <select class="input" name="discount_type" title="Select Discount Type" >
                <option value="FLAT" <c:if test="${discountType eq 'FLAT'}" >selected="selecte"</c:if> >FLAT</option>
                <option value="PERCENT" <c:if test="${discountType eq 'PERCENT'}" >selected="selecte"</c:if> >PERCENT</option>
            </select><br/>
            <b>Max Discount</b><a href="#" rel="tooltip" class="tip" title="If it is a percent discount, what is maximum disocunt amount to enforce" data-original-title="Help">[?]</a> <br/>
            <input type="text" name="max_discount" value="<c:if test="${discountType eq 'PERCENT'}" >${freefundClass.maxDiscount}</c:if>" /><br/>
            <b>Apply Condition Id</b> <a href="#" rel="tooltip" class="tip" title="Condition to be checked when user clicks Apply" data-original-title="Help">[?]</a><br/>
                <%-- <input type="text" name="max_redemptions" value="${freefundClass.maxRedemptions}" /> (# of times user can avail a freefund/promo code of this class)<br/> --%>
            <input type="text" name="apply_condition_id" value="${freefundClass.applyConditionId}" /><br/>
            <b>Payment Condition Id</b> <a href="#" rel="tooltip" class="tip" title="Condition to be checked when user clicks make payment" data-original-title="Help">[?]</a><br/>
            <input type="text" name="payment_condition_id" value="${freefundClass.paymentConditionId}" /> <br/>
            <b>Redeem Condition Id</b> <a href="#" rel="tooltip" class="tip" title="Condition to be checked when user clicks make payment" data-original-title="Help">[?]</a><br/>
            <input type="text" name="redeem_condition_id" value="${freefundClass.redeemConditionId}" /> <br/>
            <b>Reward Id</b> <a href="#" rel="tooltip" class="tip" title="Used to attach double reward (Coupled with Cashback Amount)" data-original-title="Help">[?]</a><br/>
            <b>Choose a reward</b><br/>
            <select id="rewardSelector" class="input" name="rewardId" title="Select Class">
                <option value="0" <c:if test="${empty freefundClass.rewardId}"> selected="true" </c:if>>No Reward</option>
                <c:forEach items="${additionalRewardList}" var="additionalReward">
                    <option  <c:choose><c:when test="${not empty freefundClass}"><c:if test="${freefundClass.rewardId  eq (additionalReward.id)}">selected="true"</c:if></c:when></c:choose> value="${additionalReward.id}"  >${additionalReward.rewardName}</option>
                </c:forEach>
            </select><br>
            <table id="additionalReward" width="auto" border="1">
                <tbody id="tableHeader">
                <tr>
                    <td>Reward name</td>
                    <td>Reward Value</td>
                    <td>Reward DiscountType</td>
                    <td>Reward Max Discount</td>
                    <td>Reward Condition Id</td>
                    <td>Is Enabled</td>
                </tr>
                </tbody>
                <c:forEach items="${additionalRewardList}" var="additionalReward" varStatus="loop">
                    <tbody id="${additionalReward.id}" <c:choose>
                        <c:when test="${not empty additionalReward}"><c:if test="${additionalReward.id ne freefundClass.rewardId}">style="display: none;"</c:if></c:when></c:choose> >
                    <tr>
                        <td>${additionalReward.rewardName}</td>
                        <td>${additionalReward.rewardValue}</td>
                        <td>${additionalReward.rewardDiscountType}</td>
                        <td>${additionalReward.rewardMaxDiscount}</td>
                        <td>${additionalReward.rewardConditionId}</td>
                        <td>${additionalReward.isEnabled}</td>
                    </tr>
                    </tbody>
                </c:forEach>
            </table>
            <script type="text/javascript">

                $("#rewardSelector").change(function() {
                    var id = $(this).val();
                    $('#additionalReward').find('tbody').hide();
                    $('#additionalReward').find('tbody#tableHeader, tbody#'+id).show();
                });

            </script>
            <br>
            <b>AR Reward Id</b> <a href="#" rel="tooltip" class="tip" title="Used to attach AR Reward" data-original-title="Help">[?]</a><br/>
            <input type="text" name="arRewardId" value="${freefundClass.arRewardId}" onkeypress="return alphanumeric_only(event);"/>
            <br>       
            <b><spring:message code="prefix" text="#prefix#"/> <a href="#" rel="tooltip" class="tip" title="2 or 3 letter prefix to add in front of each code generated, helps to avoid duplicates" data-original-title="Help">[?]</a></b> <br/>
            <input type="text" name="prefix" value="${freefundClass.prefix}" onkeypress="return alphanumeric_only(event);"/></br>
            <br>
            <b><spring:message code="no.codes" text="#no.codes#"/> <a href="#" rel="tooltip" class="tip" title=" Approximate Number of codes to be generated. MAX 1,00,000." data-original-title="Help">[?]</a></b><br/>
            <input type="text" id="code" name="codes" value="${freefundClass.numCodes}" onkeypress="return isNumberKey(event,2,id);" onblur="return populateLength(event);"/> <br/>
            <br>
            <b><spring:message code="length" text="#length#" /> <a href="#" rel="tooltip" class="tip" title="Length of each promocode , please use the suggested code length before you generate. Max 9." data-original-title="Help">[?]</a></b><br/>
            <input type="text" id="len" name="length" value="${freefundClass.codeLength}" onchange="checkForDesiredLength();"  onkeypress="return isNumberKey(event,2,id);"/><br/>
            <b>IVR Client <a href="#" rel="tooltip" class="tip" title="IVR Client to be used to make automated calls" data-original-title="Help">[?]</a></b><br/>
            <input type="text" name="IVRSClient" value="${freefundClass.IVRSClient}">
            <br>
            <b>IVR ID <a href="#" rel="tooltip" class="tip" title="Want associate an IVR ID to make automated calls" data-original-title="Help">[?]</a></b><br/>
            <input type="text" name="ivrId" value="${freefundClass.ivrId}">
            <br>
            <b>Mapping keys <a href="#" rel="tooltip" class="tip" title="Want associate mapping keys to the campaign" data-original-title="Help">[?]</a></b><br/>
            <input type="hidden" name="mappedKeys" id="mappedKeys" value="${freefundClass.mappingKeys}">
            <c:forEach var="mappingKey" items="<%=PromocodeConstants.PROMOCODE_UNIQUE_MAPPING_KEY_LIST%>">
                ${mappingKey}<input name="mappingKeys" type="checkbox" id="${mappingKey}" value="${mappingKey}" style="margin-top:-2px;margin-left:5px;">&nbsp;&nbsp;
            </c:forEach>
            <div style="background-color: #cc0000; border : 1px solid #cc1111; color : #ffffff;" id="desiredLengthMsg"></div>
            <br>
            <b>Apply promocode Success Message <a href="#" rel="tooltip" class="tip" title="Success message to be shown upon redeeming a code of this class" data-original-title="Help">[?]</a></b><br/>
            <input type="text" id="successMessage" name="successMessage" value="${freefundClass.successMessage}"/> <br/>
            <b>Recharge Success Message <a href="#" rel="tooltip" class="tip" title="Success message to be shown when recharge is done by using a promocde of this class" data-original-title="Help">[?]</a></b><br/>
            <input type="text" id="rechargeSuccessMessage" name="rechargeSuccessMessage" value="${freefundClass.rechargeSuccessMessage}"/> <br/>
            <b><spring:message code="description" text="#description#"/></b> 
            	<br><span style="font-size:12px;">(# For CashBack offers it will be used for Wallet Credits Title and CashBack 
            	Mailers. eg. 'CashBack for Axis Bank Credit/Debit card offer')</span><br/>
            <textarea name="description">${freefundClass.description}</textarea> <br/>

            <b>Valid From</b> <a href="#" rel="tooltip" class="tip" title="Campaign starting date and time" data-original-title="Help">[?]</a><br/>

            <div class=" row ">
                <div class="span3">
                    <div id="datetimepicker1" class="input-append date">
                        <input data-format="dd/MM/yyyy" autocomplete="off" readonly="readonly" type="text"  name="valid_from"  value="<fmt:formatDate pattern='dd/MM/yyyy' value="${freefundClass.validFrom.time}"/>"  />
							    <span class="add-on">
							      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                  </i>
							    </span>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function() {
                        $('#datetimepicker1').datetimepicker({
                            maskInput: true,
                            pickTime: false
                        });
                    });
                </script>
                <div class="span2">
                    <div id="datetimepicker2" class="input-append">
                        <input data-format="hh:mm:ss" autocomplete="off" readonly="readonly" type="text"  name="valid_from_time"  value="<fmt:formatDate pattern='HH:mm:ss' value="${freefundClass.validFrom.time}"/>" />
							    <span class="add-on">
							      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                  </i>
							    </span>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(function() {
                    $('#datetimepicker2').datetimepicker({
                        maskInput: true,
                        pickDate: false
                    });
                });
            </script>
            <br>
            <b>Valid Upto</b> <a href="#" rel="tooltip" class="tip" title="Campaign ending date and time" data-original-title="Help">[?]</a><br>

            <div class=" row ">
                <div class="span3">
                    <div id="datetimepicker3" class="input-append date">
                        <input data-format="dd/MM/yyyy" autocomplete="off" readonly="readonly" type="text"  name="valid_upto"  value="<fmt:formatDate pattern='dd/MM/yyyy' value="${freefundClass.validUpto.time}"/>"  />
							    <span class="add-on">
							      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                  </i>
							    </span>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function() {
                        $('#datetimepicker3').datetimepicker({
                            maskInput: true,
                            pickTime: false
                        });
                    });
                </script>
                <div class="span2">
                    <div id="datetimepicker4" class="input-append">
                        <input data-format="hh:mm:ss" autocomplete="off" readonly="readonly" type="text"  name="valid_upto_time"  value="<fmt:formatDate pattern='HH:mm:ss' value="${freefundClass.validUpto.time}"/>" />
							    <span class="add-on">
							      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                  </i>
							    </span>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(function() {
                    $('#datetimepicker4').datetimepicker({
                        maskInput: true,
                        pickDate: false
                    });
                });
            </script>

            <input type="submit"  title="Submit" value="Save" class="btn btn-primary" id="butsubmit" name="addbut">
        </form>
    </div>
</c:if>
<c:if test="${ page eq 'bulkreset'}">
    <c:if test="${not empty invalidCodes}">
        <b>List of invalid codes</b>
        <div  style="background-color:yellow;padding:10px;">
            <c:forEach var="code" items="${invalidCodes}">
                ${code} &nbsp;&nbsp;
            </c:forEach>
        </div>
    </c:if>
    <c:if test="${not empty unblockedCodes}">
        <b>List of codes unblocked successfully</b>
        <div  style="background-color:yellow;padding:10px;">
            <c:forEach var="code" items="${unblockedCodes}">
                ${code} &nbsp;&nbsp;
            </c:forEach>
        </div>
    </c:if>
    <c:if test="${not empty nonblockedCodes}">
        <b>List of codes failed to unblock</b>
        <div style="background-color:yellow;padding:10px;">
            <c:forEach var="code" items="${nonblockedCodes}">
                ${code} &nbsp;&nbsp;
            </c:forEach>
        </div>
    </c:if>
    <div id="bulkresetDiv" class="sectionDiv">
        <h4>Reset Promocodes / Freefund codes</h4>
        <form:form  name="bulkresetform" action="/admin/freefund/bulkreset.htm" method="post" enctype="multipart/form-data" cssClass="form" id="bulkresetform" >
            <b>Codes CSV File</b><br>
            <input type="file" class="input"  id="promocodes" name="promocodes" ><br><br>
            <input type="submit"  title="Submit" value="Submit" class="btn btn-primary" id="butsubmit" name="addbut">
        </form:form>
    </div>
</c:if>

</div>
</div>
</section>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<script type="text/javascript">

    $(document).ready(function(){
        selectMappingKeys();
        function selectMappingKeys() {
            var mappedKeys = $('#mappedKeys').attr('value');
            var mappedKeyList = mappedKeys.split(',');
            $.each(mappedKeyList, function(index, value) {
                $('#' + value.trim()).prop('checked', true);
            });
        }
    });
    function checkForDesiredLength(){
        $('#desiredLengthMsg').html("");
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });
        var url = '/admin/freefund/generateandupload/generate/validate.htm?codes='+$('input[name=codes]').val()+"&length="+$('input[name=length]').val();

        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            timeout:1800000,
            success: function (responseText) {
                if(responseText.status == 'fail') {
                    $('#desiredLengthMsg').html(responseText.message);
                }
                $.unblockUI();
            },
            failure: function (responseText) {
                alert("Validation failed.");
                $.unblockUI();
            }
        });

    }

    function alphanumeric_only(event)
    {
        var keycode;
        keycode=event.keyCode?event.keyCode:event.which;
        var splChar = "%";
        if ((keycode == 32 || keycode == 8 || keycode == 46) || (keycode >= 47 && keycode <= 57) || (keycode >= 65 && keycode <= 90) || (keycode >= 97 && keycode <= 122)) {
            return true;
        }
        else {
            // alert("Sorry You can not insert Special Character");
            return false;
        }
        return true;
    }
    //Populate length field based on num codes entered
    function populateLength(evt){
        $('#desiredLengthMsg').html("");
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });
        var url = '/admin/freefund/getLengthForCodes?codes='+$('input[name=codes]').val();

        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            timeout:1800000,
            success: function (responseText) {
                if(responseText.status == 'fail') {
                    $('#desiredLengthMsg').html(responseText.message);
                } else{
                     $(len).val(responseText.length);
                }
                $.unblockUI();
            },
            failure: function (responseText) {
                alert("Validation failed.");
                $.unblockUI();
            }
        });
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.tip').tooltip();
        $("#rewardSelector").change(function() {
            var id = $(this).val();
            $('#additionalReward').find('tbody').hide();
            $('#additionalReward').find('tbody#tableHeader, tbody#'+id).show();
        });
    });
</script>