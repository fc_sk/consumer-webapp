<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>

<section class="container">
    <div  id="generateCodesDiv" class="sectionDiv" >
        <h4>Generate Freefund Codes</h4>
        <form:form  name="freefundgenerator" action="/admin/generateCodes/generate.htm" method="post" cssClass="form" id="crossselluploadform">
            <b><spring:message code="prefix" text="#prefix#"/> <a href="#" rel="tooltip" class="tip" title="2 or 3 letter prefix to add in front of each code generated, helps to avoid duplicates" data-original-title="Help">[?]</a></b> <br/>
            <input type="text" name="prefix"  onkeypress="return alphanumeric_only(event);"/></br>
            <br>
            <b><spring:message code="no.codes" text="#no.codes#"/> <a href="#" rel="tooltip" class="tip" title="Number of codes to be generated" data-original-title="Help">[?]</a></b><br/>
            <input type="text" id="code" name="codes" value="" onkeypress="return isNumberKey(event,2,id);"/> <br/>
            <br>
            <b><spring:message code="length" text="#length#" /> <a href="#" rel="tooltip" class="tip" title="Length of each promocode , please use the suggested code length before you generate" data-original-title="Help">[?]</a></b><br/>
            <input type="text" id="len" name="length" onchange="checkForDesiredLength();"  onkeypress="return isNumberKey(event,2,id);"/>
            <div style="background-color: #cc0000; border : 1px solid #cc1111; color : #ffffff;" id="desiredLengthMsg"></div>
            <br>
            <input type="submit"  title="Submit" value="Generate" class="btn btn-primary" id="butsubmit" name="addbut">
        </form:form>
    </div>
</section>

<script type="text/javascript">

    function checkForDesiredLength(){
        $('#desiredLengthMsg').html("");
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });
        var url = '/admin/generateCodes/generate/validate.htm?codes='+$('input[name=codes]').val()+"&length="+$('input[name=length]').val();

        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            timeout:1800000,
            success: function (responseText) {
                if(responseText.status == 'fail') {
                    $('#desiredLengthMsg').html(responseText.message);
                }
                $.unblockUI();
            },
            failure: function (responseText) {
                alert("Validation failed.");
                $.unblockUI();
            }
        });

    }
</script>
