<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>AFC table</title>
<style>

#pageselect {width:100%; min-height:20px}

#pageselect {list-style-type: none; margin: 15px 15px 15px 15px; padding:0px 0px 0px; float: left;}
#pageselect li, #pageselect a {display: block; float: left; margin: 0}
#pageselect a { text-decoration: none;}
#pageselect a.active {background: yellow;}


#pageselect a{
  border: 1px solid #666;
  text-decoration: none;
  padding: 14px 20px;
  font-size: 16px;
  font-weight: bold;
  background: lightblue;
  color: #fff;
  border: 2px solid #fff;
}

#pageselect a:hover, #pageselect a.active, #pageselect a.active:hover{
  background: darkblue;
  border: 1px solid #666;
  text-decoration: none;
  font-size: 16px;
  font-weight: bold;
  color: #fff;
  border: 2px solid #fff;
}

</style>
</head>
<body>
<div class="container">
<div id="backtohome">
    <form action="/rest/metro/home" method="get" cssclass="form">
        <input type="submit" value="Back" name="submit" class="btn btn-primary">
    </form>
</div>
<div id="pageselect">
    <c:forEach var="i" begin="1" end="${noOfPages}">
        <c:choose>
            <c:when test="${i eq currentPage}">
                <a href="#"><c:out value="${i}"/></a>
            </c:when>
            <c:otherwise>
                <a href="/rest/metro/data?pageNumber=${i}&source=dataPage"><c:out value="${i}"/></a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</div>
<form name="DateForm" action="/rest/metro/download" method="get" cssclass="form">
<table  id=“AFC Table" class="display"  style="width:90%;">
<thead>
		<tr align="center">
			<th>Freecharge Tranx ID</th>
			<th>Date & Time of Payment Txn</th>
			<th>Date & Time of Recharge Txn</th>
			<th>Amount</th>
			<th>Payment status</th>
			<th>Payment Type</th>
			<th>Recharge status</th>
			<th>Name</th>
			<th>Email</th>
			<th>Phone No</th>
			<th>AFC Card no</th>
			<th>Refund Status</th>
			<th>Refund Date</th>
		</tr> 

	</thead>
<tbody align="center">
		<c:forEach var="SampleDataMap"
			items="${SampleDataMapList}">
			<tr>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.orderId}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.paymentTime}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.rechargeTime}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.amount}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.paymentStatus}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.paymentType}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.rechargeStatus}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.firstName}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.email}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.phoneNo}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.afcCardNumber}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.refundStatus}</td>
				<td style="height:30px;padding:4px;font-size:12px;">${SampleDataMap.refundDate}</td>
 			</tr>
		</c:forEach>
	</tbody> 
	</table>
</div>
</body>
<input type="hidden" data-format="yyyy/dd/MM" autocomplete="off" type="text" name="start_date"  value="${InputDataMap.startDate}">
<input type="hidden" data-format="yyyy/dd/MM" autocomplete="off" type="text" name="end_date"  value="${InputDataMap.endDate}">
<c:choose>
    <c:when test='${not empty "${InputDataMap.afcCardNumber}"}'>
        <input type="hidden"   type="text" name="afcCardNo" value="${InputDataMap.afcCardNumber}">
    </c:when>
    <c:otherwise>
        <input type="hidden"   type="text" name="afcCardNo" >
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test='${not empty "${InputDataMap.paymentStatus}"}'>
        <input type="hidden"   type="text" name="paymentStatus" value="${InputDataMap.paymentStatus}">
    </c:when>
    <c:otherwise>
        <input type="hidden"   type="text" name="paymentStatus" >
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when test='${not empty "${InputDataMap.orderId}"}'>
        <input type="hidden"   type="text" name="orderId" value="${InputDataMap.orderId}">
    </c:when>
    <c:otherwise>
        <input type="hidden"   type="text" name="orderId" >
    </c:otherwise>
</c:choose>

<input type="hidden" data-format="yyyy/dd/MM" autocomplete="off" type="text" name="start_date"  value="${InputDataMap.startDate}">
<input type="hidden" data-format="yyyy/dd/MM" autocomplete="off" type="text" name="end_date"  value="${InputDataMap.endDate}">
<c:choose>
    <c:when test='${not empty "${InputDataMap.afcCardNumber}"}'>
        <input type="hidden"   type="text" name="afcCardNo" value="${InputDataMap.afcCardNumber}">
    </c:when>
    <c:otherwise>
        <input type="hidden"   type="text" name="afcCardNo" >
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test='${not empty "${InputDataMap.paymentStatus}"}'>
        <input type="hidden"   type="text" name="paymentStatus" value="${InputDataMap.paymentStatus}">
    </c:when>
    <c:otherwise>
        <input type="hidden"   type="text" name="paymentStatus" >
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when test='${not empty "${InputDataMap.orderId}"}'>
        <input type="hidden"   type="text" name="orderId" value="${InputDataMap.orderId}">
    </c:when>
    <c:otherwise>
        <input type="hidden"   type="text" name="orderId" >
    </c:otherwise>
</c:choose>

<input type="submit" value="Download" name="submit" class="btn btn-primary"/>

</html>