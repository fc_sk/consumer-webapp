<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %><html>

<section class="container">
  <div class="row">
    <c:if test="${not empty param.message}">
      <div class="span12" style="background-color:yellow;padding:10px;"><c:out value="${param.message}"/></div>
    </c:if>
  </div>

  <div class="row">
    <div class="span2">
      <ul class="nav nav-list">
        <li class="nav-header">
          <a href="/admin/promocode/utility/home/deleteCodes.htm">Delete/Block Codes</a>
        </li>
        <li class="nav-header">
          <a href="/admin/promocode/utility/home/codesCount.htm">Codes count for a class</a>
        </li>
      </ul>
    </div>
    <div class="span10">

      <c:if test="${ page eq 'deleteCodes'}">
        <div  id="generateCodesDiv" class="sectionDiv" >
          <h4>Delete Freefund Codes</h4>
          <form:form  name="freefunddeletor" cssClass="form" id="crossselluploadform" enctype="multipart/form-data" action="/admin/promocode/utility/deleteCodes.htm" method="post" >
            <b>Choose a class</b><br/>
            <select class="input" name="class_id" title="Select Class" >
              <c:forEach items="${freefundClasses }" var="freefundClass">
                <option value="${freefundClass.id }">${freefundClass.name}</option>
              </c:forEach>
            </select><br>
            <label><b>Block only?</b><input id="blockOnly" type="checkbox" class="checkbox inline" name="autoDetect"/></label><br/>
            <b><spring:message code="delete.freefundfile" text="#delete.freefundfile#"/></b><spring:message code="csv.file" text="#csv.file#"/><br>
            <input type="file" class="input"  id="freefund_codes" name="freefund_codes" ><br><br>
            <input type="submit"  title="Submit" value="Delete" class="btn btn-primary" id="butsubmit" name="addbut">
          </form:form>
        </div>
      </c:if>

      <c:if test="${ page eq 'codeCount'}">
        <div id="codeCountDiv" class="sectionDiv">
          <form:form  name="freefundgenerator" cssClass="form" id="crossselluploadform" action="/admin/promocode/utility/codeCount.htm" method="post" >
          <b>Choose a class</b><br/>

          <select class="input" name="class_id" title="Select Class" >
            <c:forEach items="${freefundClasses }" var="freefundClass">
              <option value="${freefundClass.id }">${freefundClass.name}</option>
            </c:forEach>
          </select><br>
          <input type="submit"  title="Submit" value="codeCount" class="btn btn-primary" id="butsubmit" name="addbut">
          </form:form>
        </div>
      </c:if>

  </div>
  </div>

</section>


