<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>other table</title>
</head>
 
<body>
 <div class="container">
<form name="DateForm" action="/rest/metro/data" method="get" cssclass="form">						
<b>Start Date:</b><br/>
					<div class="input-append date" id="datePicker1" >
						<input data-format="yyyy/dd/MM" autocomplete="off" type="text" name="start_date" />
						<span class="add-on">
							<i data-date-icon="icon-calendar"></i>
						</span>
					</div><br/>

					<script type="text/javascript">
					$(function() {
						$("#datePicker1").datetimepicker({
							maskInput: true,
							pickTime: false,
							autoClose:true
						});
					});
					</script>
<b>End Date:</b><br/>
					<div class="input-append date" id="datePicker2" >
						<input data-format="yyyy/dd/MM" autocomplete="off" type="text" name="end_date"/>
						<span class="add-on">
							<i data-date-icon="icon-calendar"></i>
						</span>
					</div><br/>

					<script type="text/javascript">
					$(function() {
						$("#datePicker2").datetimepicker({
							maskInput: true,
							pickTime: false,
							autoClose:true
						});
					});
					</script>
		
<b>AFC Card No:</b><br/>					
					<div >
						<input type="text" name="afcCardNo" />
					</div><br/>
					
<b>Payment Status:</b><br/>					
					<div >
						<input type="radio" name="paymentStatus" value="1" required name="paymentStatus" required>Success<br>
						<input type="radio" name="paymentStatus" value="0" required name="paymentStatus" required>Failed<br>
						<input type="radio" name="paymentStatus" value="-1" required name="paymentStatus" required checked>Both
					</div><br/>
						
<b>Order Id:</b><br/>					
					<div >
						<input type="text" name="orderId" />
					</div><br/>					
					<input type="hidden" value="home" name="source">
					<input type="submit" value="Submit" name="submit" class="btn btn-primary"/>
					</form>
<%-- <button id="myButton" class="float-left submit-button" >Enter</button> 

<script type="text/javascript">
    document.getElementById("myButton").onclick = function () {
        location.href = "AFC.jsp";
    };
</script>--%>


</div>
</body>
</html>