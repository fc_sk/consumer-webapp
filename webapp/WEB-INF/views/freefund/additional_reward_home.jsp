<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>

<section class="container">
    <div class="row">
        <c:if test="${not empty param.message}">
            <div class="span12" style="background-color:yellow;padding:10px;"><c:out value="${param.message}"/></div>
        </c:if>
    </div>
    <div class="row">
        <div class="span2">
            <ul class="nav nav-list">
                <li class="nav-header">
                    <a href="/admin/freefund/additionalreward/home/create.htm">Create Reward</a>
                </li>
                <li class="nav-header">
                    <a href="/admin/freefund/additionalreward/home/view.htm">View Rewards</a>
                </li>
                <li class="nav-header">
                    <a href="/admin/freefund/additionalreward/home/delete">Delete reward</a>
                </li>
            </ul>
        </div>
        <div class="span10">
            <c:choose>
            <c:when test="${ (page eq 'create') or (page eq 'edit')}">
                <div  id="createRewardDiv" class="sectionDiv" >
                   <c:if test="${empty additionalReward.rewardName}">
                        <h4>Create Additional Reward</h4>
                   </c:if>
                   <c:if test="${not empty additionalReward.rewardName}">
                       <h4>Edit Additional Reward</h4>
                       <c:if test="${additionalReward.isEnabled eq true}"><h3>This campaign is Active</h3></c:if>
                       <c:if test="${additionalReward.isEnabled eq false}"><h3>This campaign is Disabled</h3></c:if>
                   </c:if>
                    <form name="createReward" <c:choose><c:when test="${not empty additionalReward.rewardName}"> action="/admin/freefund/additionalreward/edit" </c:when> <c:otherwise>action="/admin/freefund/additionalreward/create""</c:otherwise></c:choose> method="post" cssClass="form" id="createAdditionalReward">
                        <b><spring:message code="rewardName" text="Reward Name"/> <a href="#" rel="tooltip" class="tip" title="Name for the reward" data-original-title="Help">[?]</a></b> <br/>
                        <input type="text" name="rewardName" value="${additionalReward.rewardName}"/></br>
                        </br>
                        <b>Additional Reward Value</b> <a href="#" rel="tooltip" class="tip" title="how much to be credited to wallet" data-original-title="Help">[?]</a> <br/>
                        <input type="text" name="rewardValue"  value="${additionalReward.rewardValue}" /><br/>

                        <b>Discount Type</b> <a href="#" rel="tooltip" class="tip" title="Flat disocunt or a percentage on recharging amount" data-original-title="Help">[?]</a> <br/>
                        <c:set var="discountType" value="${additionalReward.rewardDiscountType}" />
                        <select class="input" name="rewardDiscountType" title="Select Discount Type" >
                            <option value="FLAT" <c:if test="${discountType eq 'FLAT'}" >selected="selecte"</c:if> >FLAT</option>
                            <option value="PERCENT" <c:if test="${discountType eq 'PERCENT'}" >selected="selecte"</c:if> >PERCENT</option>
                        </select><br/>

                        <b>Max Discount</b><a href="#" rel="tooltip" class="tip" title="If it is a percent discount, what is maximum disocunt amount to enforce" data-original-title="Help">[?]</a> <br/>
                        <input type="text" name="rewardMaxDiscount" value="<c:if test="${additionalReward.rewardDiscountType eq 'PERCENT'}" >${additionalReward.rewardMaxDiscount}</c:if>" /><br/>

                        <b>Additional Reward Condition Id</b> <a href="#" rel="tooltip" class="tip" title="Condition(s) to be checked while giving additional reward" data-original-title="Help">[?]</a><br/>
                        <input type="text" name="rewardConditionId" value="${additionalReward.rewardConditionId}" /> <br/>

                        <input type="submit"  title="Submit" <c:choose><c:when test="${empty additionalReward.rewardName}"> value="create" </c:when><c:otherwise> value="edit"</c:otherwise></c:choose> class="btn btn-primary" id="butsubmit" name="addbut">
                    </form>
                </div>
            </c:when>
            <c:when test="${page eq 'view'}">
                <h4>View Additional Rewards</h4>
                <b>Choose a reward</b><br/>
                <select id="rewardSelector" class="input" name="additionalRewardId" title="Select Class">
                    <c:forEach items="${additionalRewards}" var="additionalReward">
                        <option  value="${additionalReward.id}" >${additionalReward.rewardName}</option>
                    </c:forEach>
                </select><br>
                <table id="additionalReward" width="auto" border="1">
                    <tbody id="tableHeader">
                    <tr>
                        <td>Reward name</td>
                        <td>Reward Value</td>
                        <td>Reward DiscountType</td>
                        <td>Reward Max Discount</td>
                        <td>Reward Condition Id</td>
                        <td>Is Enabled</td>
                        <td>Edit</td>
                    </tr>
                    </tbody>
                    <c:forEach items="${additionalRewards}" var="additionalReward" varStatus="loop">
                        <tbody id="${additionalReward.id}"<c:choose>
                               <c:when test="${not empty additionalReward}"><c:if test="${additionalReward.id ne 1}">style="display: none;"</c:if></c:when></c:choose> >
                        <tr>
                            <td>${additionalReward.rewardName}</td>
                            <td>${additionalReward.rewardValue}</td>
                            <td>${additionalReward.rewardDiscountType}</td>
                            <td>${additionalReward.rewardMaxDiscount}</td>
                            <td>${additionalReward.rewardConditionId}</td>
                            <td>${additionalReward.isEnabled}</td>
                            <td><a href="/admin/freefund/additionalreward/home/edit?rewardId=${additionalReward.id}">edit</a></td>
                        </tr>
                        </tbody>
                    </c:forEach>
                </table>
                </br>
            </c:when>
            <c:when test="${page eq 'delete'}">
                <h4>Delete Additional Reward</h4>
                <b>Choose a reward</b><br/>
                <select id="rewardSelector" class="input" name="additionalRewardId" title="Select Class">
                    <c:forEach items="${additionalRewards}" var="additionalReward">
                        <option  value="${additionalReward.id}" >${additionalReward.rewardName}</option>
                    </c:forEach>
                </select><br>
                <table id="additionalReward" width="auto" border="1">
                    <tbody id="tableHeader">
                    <tr>
                        <td>Reward name</td>
                        <td>Reward Value</td>
                        <td>Reward DiscountType</td>
                        <td>Reward Max Discount</td>
                        <td>Reward Condition Id</td>
                        <td>isEnabled</td>
                        <td>delete</td>
                    </tr>
                    </tbody>
                    <c:forEach items="${additionalRewards}" var="additionalReward" varStatus="loop">
                        <tbody id="${additionalReward.id}" <c:choose>
                            <c:when test="${not empty additionalReward}"><c:if test="${additionalReward.id ne 1}">style="display: none;"</c:if></c:when></c:choose> >
                        <tr>
                            <td>${additionalReward.rewardName}</td>
                            <td>${additionalReward.rewardValue}</td>
                            <td>${additionalReward.rewardDiscountType}</td>
                            <td>${additionalReward.rewardMaxDiscount}</td>
                            <td>${additionalReward.rewardConditionId}</td>
                            <td>${additionalReward.isEnabled}</td>
                            <td><a href="/admin/freefund/additionalreward/delete?rewardId=${additionalReward.id}">delete</a></td>
                        </tr>
                        </tbody>
                    </c:forEach>
                </table>

                </br>
            </c:when>
            </c:choose>

        </div>
    </div>

</section>

<script type="text/javascript">

    $("#rewardSelector").change(function() {
        var id = $(this).val();
        $('#additionalReward').find('tbody').hide();
        $('#additionalReward').find('tbody#tableHeader, tbody#'+id).show();
    });

</script>
