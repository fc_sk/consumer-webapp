<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>

<section class="container">
    <div class="row">
        <c:if test="${not empty param.message}">
            <div class="span12" style="background-color:yellow;padding:10px;"><c:out value="${param.message}"/></div>
        </c:if>
    </div>
    <div class="row">
        <div class="span2">
            <ul class="nav nav-list">
                <li class="nav-header">
                    <a href="/admin/freefund/generateandupload/home/generatecodes.htm">Generate Codes</a>
                </li>
                <li class="nav-header">
                    <a href="/admin/freefund/generateandupload/home/uploadcodes.htm">Upload Codes</a>
                </li>
            </ul>
        </div>
    <div class="span10">
        <c:if test="${ page eq 'generatecodes'}">
            <div  id="generateCodesDiv" class="sectionDiv" >
                <h4>Generate Freefund Codes</h4>
                <form:form  name="freefundgenerator" action="/admin/freefund/generateandupload/generate.htm" method="post" cssClass="form" id="crossselluploadform">

                    <b><spring:message code="prefix" text="#prefix#"/> <a href="#" rel="tooltip" class="tip" title="2 or 3 letter prefix to add in front of each code generated, helps to avoid duplicates" data-original-title="Help">[?]</a></b> <br/>
                    <input type="text" name="prefix"  onkeypress="return alphanumeric_only(event);"/></br>
                    <br>
                    <b><spring:message code="no.codes" text="#no.codes#"/> <a href="#" rel="tooltip" class="tip" title="Number of codes to be generated" data-original-title="Help">[?]</a></b><br/>
                    <input type="text" id="code" name="codes" value="" onkeypress="return isNumberKey(event,2,id);"/> <br/>
                    <br>
                    <b><spring:message code="length" text="#length#" /> <a href="#" rel="tooltip" class="tip" title="Length of each promocode , please use the suggested code length before you generate" data-original-title="Help">[?]</a></b><br/>
                    <input type="text" id="len" name="length" onchange="checkForDesiredLength();"  onkeypress="return isNumberKey(event,2,id);"/>
                    <div style="background-color: #cc0000; border : 1px solid #cc1111; color : #ffffff;" id="desiredLengthMsg"></div>
                    <br>
                    <input type="submit"  title="Submit" value="Generate" class="btn btn-primary" id="butsubmit" name="addbut">
                </form:form>
            </div>
        </c:if>
        <c:if test="${ page eq 'uploadcodes'}">
            <div id="codeuploadDiv" class="sectionDiv">
                <h4>Upload Promocodes / Freefund codes</h4>
                <form:form  name="freefunduploadform" action="/admin/freefund/generateandupload/upload.htm" method="post" enctype="multipart/form-data" cssClass="form" id="crossselluploadform" >
                    <b>Choose a class</b><br/>
                    <select class="input" name="class_id" title="Select Class" >
                        <c:forEach items="${freefundClasses }" var="freefundClass">
                            <option value="${freefundClass.id }">${freefundClass.name}</option>
                        </c:forEach>
                    </select><br>
                    <b><spring:message code="upload.freefundfile" text="#upload.freefundfile#"/></b><spring:message code="csv.file" text="#csv.file#"/><br>
                    <input type="file" class="input"  id="freefund_codes" name="freefund_codes" ><br><br>
                    <input type="submit"  title="Submit" value="Submit" class="btn btn-primary" id="butsubmit" name="addbut">
                </form:form>
            </div>
        </c:if>
    </div>
    </div>
</section>

<script type="text/javascript">

    function checkForDesiredLength(){
        $('#desiredLengthMsg').html("");
        $.blockUI({ css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });
        var url = '/admin/freefund/generateandupload/generate/validate.htm?codes='+$('input[name=codes]').val()+"&length="+$('input[name=length]').val();

        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            timeout:1800000,
            success: function (responseText) {
                if(responseText.status == 'fail') {
                    $('#desiredLengthMsg').html(responseText.message);
                }
                $.unblockUI();
            },
            failure: function (responseText) {
                alert("Validation failed.");
                $.unblockUI();
            }
        });

    }
</script>
