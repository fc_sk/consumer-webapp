<%--
  Created by IntelliJ IDEA.
  User: ashwini
  Date: 12/6/14
  Time: 2:25 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ include file="/WEB-INF/includes/taglibs.jsp"%>
<head>
    <style type="text/css">
        .smart {
            background-color: #c6f5ca;
            cursor: hand;
        }
        .hpoints {
            display: none;
            position: absolute;
            margin-left: 130px;
            background-color: #eaeeef;
            border: 1px solid #ccc;
            padding: 5px;
            z-index: 100;
            margin-top: -35px;
        }
    </style>
</head>
<div class="span12">
    <div class="alert" style='display: <c:if test="${empty param.message}">none</c:if>'>
        <c:if test="${not empty param.message}">
            <c:out value="${param.message}"/>
        </c:if>
        <a class="close" href="#">&times;</a>
    </div>
    <fieldset>
        <legend>MISSIONS</legend>
        <ul class="nav nav-pills">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="/admin/missions/edit-promo">Create Promo Mission</a></li>
            <li><a href="/admin/missions/edit-app-mission">Create App Mission</a></li>
            <%--<li><a href="/admin/missions/edit-moolah-promo">Create Moolah Mission</a></li>--%>
        </ul>
        <div class="row">
            <c:forEach var="deal" items="${ardeals}">
                <div class="span12" style="border: 1px solid #dad6d6;">
                    <div class="span3" style="line-height: 40px; border-right: 1px solid #dad6d6;">
                        <a href="/admin/missions/edit-email?id=${deal.id}" target="_blank" class="btn-link">editEmail</a>&nbsp;|
                        <c:choose>
                            <c:when test="${deal.channel == 3}">
                                <a href="/admin/missions/edit-app-mission?id=${deal.id}" class="btn-link">edit</a>&nbsp;|
                            </c:when>
                            <c:otherwise>
                                <a href="/admin/missions/edit-promo?id=${deal.id}" class="btn-link">edit</a>&nbsp;|
                            </c:otherwise>
                        </c:choose>
                        <a href="/admin/missions/createMissionCopy.htm?id=${deal.id}" title="Copy" data-id="${deal.id}" class="copy-deal">Copy</a>&nbsp;|
                        <a href="/admin/missions/targeting?id=${deal.id}" target="_blank" class="btn-link" title="Set Up Targeting for This Mission">Targeting</a>
                    </div>
                    <div class="hpoints" id="hp_${deal.id}">
                        <p><a href="#" id="x_${deal.id}" class="close-hp" style="float: right; margin-bottom: 10px;"><strong>X</strong></a> </p>
                        <c:forEach var="hp" items="${deal.displayHookpoints}">
                            ${hp.pageLabel}<br>
                        </c:forEach>
                    </div>
                    <div class="span2 js-deal-name" style="padding-top: 5px;" data-deal_id="${deal.id}">
                        ${deal.name}
                    </div>
                    <div class="span3" style="padding-top: 5px;">
                        <c:choose>
                            <c:when test="${deal.enabled}">
                                <c:choose>
                                    <c:when test="${deal.isPrivUserCond}">
                                        <span style="color: #e84964">Closed User Group</span>&nbsp;&nbsp;
                                        <a href="#" class="btn-link close-user" title="This will make this mission LIVE in previously set conditions." data-id="${deal.id}" data-privuser="true">Make Live</a>
                                    </c:when>
                                    <c:otherwise>
                                        <span style="color: #0ca05f;font-weight: 700">LIVE</span>&nbsp;&nbsp;
                                        <a href="#" class="btn-link close-user" title="This will make this mission under Closed User Group." data-id="${deal.id}" data-privuser="false">Closed User Group</a>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <a href="/admin/missions/changeStatus.htm?id=${deal.id}&status=${deal.enabled}" title="Enable this Mission" data-id="${deal.id}" class="enable-deal">Enable the Mission</a>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="span1" style="padding-top: 5px;">
                        <c:if test="${deal.enabled}">
                            <input type="text" class="input-mini" value="${deal.priority}" data-id="${deal.id}">
                        </c:if>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div class="row">
            <div class="span10" style="margin-top: 40px;">
                <c:choose>
                    <c:when test="${enabledDeals}">
                        <a href="/admin/missions/home?disabled=true" class="btn btn-inverse">Show Paused Missions</a>
                        <a href="#" id="save_priority" class="btn btn-inverse pull-right" style="margin-right: 40px;">Save Priority</a>
                    </c:when>
                    <c:otherwise>
                        <a href="/admin/missions/home?disabled=false" class="btn btn-inverse">Show Active Missions</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </fieldset>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#save_priority').on('click', function(ev){
            ev.preventDefault();
            var data = {};
            $('input.input-mini').each(function(){
                data[$(this).data('id')] = $(this).val();
            });
            $.ajax('/admin/missions/ordering.htm', {
                data: {'order': JSON.stringify(data)},
                type: 'POST',
                dataType: 'json',
                success: function(result){
                    $('div.alert').addClass("alert-success").html(result.message+'<a class="close" href="#">&times;</a>').show();
                },
                error: function(xhr){
                    $('div.alert').addClass("alert-error").html($.parseJSON(xhr.responseText).message+'<a class="close" href="#">&times;</a>').show();
                }
            })
        });

        $('.close-user').on('click', function(ev){
            ev.preventDefault();
            var aEl = $(ev.currentTarget);
            var dealId = aEl.data('id');
            $.ajax({
                url: '/admin/missions/changeCondition.htm',
                data: {id: dealId, isPrivUserCond: aEl.data('privuser')},
                dataType: 'json',
                success: function(res){
                    if(res.isPrivCond){
                        aEl.parent().html('<span style="color: #e84964">Closed User Group</span>&nbsp;&nbsp;' +
                                '<a href="#" class="btn-link close-user" title="This will make this mission LIVE ' +
                                'in previously set conditions." data-id="'+dealId+'" data-privuser="true">Make Live</a>');
                    }else {
                        aEl.parent().html('<span style="color: #0ca05f;font-weight: 700">LIVE</span>&nbsp;&nbsp;' +
                                '<a href="#" class="btn-link close-user" title="This will make this mission under ' +
                                'Closed User Group." data-id="'+dealId+'" data-privuser="false">Closed User Group</a>');
                    }
                }
            });
        });
        var $dealName = $('.js-deal-name');
        $dealName.on('mouseenter', function(ev){
            $('.js-deal-name').removeClass('smart');
            $(ev.currentTarget).addClass('smart');
        });
        $dealName.on('click', function(ev){
            var dealId = $(ev.currentTarget).data('deal_id');
            $('.hpoints').hide();
            $('#hp_'+dealId).show();
        });
        $('a.close-hp').on('click', function(ev){
            ev.preventDefault();
            $(ev.currentTarget).parent().parent().hide();
        });
    });
</script>
