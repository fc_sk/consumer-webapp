<%--
  Created by IntelliJ IDEA.
  User: ashwini
  Date: 30/6/14
  Time: 11:20 AM
  To change this template use File | Settings | File Templates.

--%>

<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<!-- .mainContainer Starts  -->
<div class="mainContainer"  >
    <div class="pageWidth clearfix form">
        <div class="manage_plans boxDesign" style=" ">  <h1><spring:message code="missions.uploademailids" text="#missions.uploademailids#"/></h1> <br>
            <br>
        <c:if test="${not empty messages}">
            <div style="background-color:papayawhip;padding:10px;"><c:out value="${messages}"/></div>
        </c:if>
            <br>
        <form:form  name="emaillistuploadform" action="/admin/missions/doemaillistupload" method="post" enctype="multipart/form-data" cssClass="form" id="emaillistuploadform" >
            <table>
                <tr>
                    <td><b>Deal</td>
                    <td>
                        <select name="dealId" >
                            <option value="">Choose a deal</option>
                            <c:forEach var="deal" items="${ardeals}">
                            <option value="${deal.id}"><c:out value="${deal.name}"/></option>
                            </c:forEach>
                        </select>
                    </td>
                 </tr>
                <tr>
                    <td>
                    <b><spring:message code="upload.emailids" text="#upload.emailids#"/></b> <spring:message code="upload.file" text="#upload.file#"/> <br>
                    </td>
                    <td><input type="file" class="input"  id="emailids" name="emailids" ><br><br>
                    </td>
                 </tr>
                <tr>
                    <td colspan="2">
                    <input type="button"  onclick="validateAndSubmit();" class="btn btn-primary" value="Submit" />
                    </td>
                </tr>
            </table>
        </form:form>
            <br><br>

        </div>
    </div>
</div>
<script type="text/javascript">


    function validateAndSubmit() {
        if($("select[name=dealId]").val() == "") {
            alert("Please select a deal name");
            return false;
        }
        document.emaillistuploadform.submit();
        return true;
    }


</script>