<%@include file="/WEB-INF/includes/taglibs.jsp"%>


<div class="row-fluid">
    <div class="span12">
        <fieldset>
            <legend>Create Promo Mission(Affitiates, Simple redirect etc)</legend>
            <form class="form form-horizontal" method="post" id="mission-form">
                <input type="hidden" name="id" value='<c:if test="${not empty ardeal}">${ardeal.id}</c:if>' id="id">
                <fieldset>
                    <legend>Mission Attributes</legend>
                    <div class="control-group deal-name-div">
                        <label class="control-label">Mission Name</label>
                        <div class="controls">
                            <input type="text" maxlength="35" class="input-xlarge" name="dealName" id="name">
                            <span class="help-inline">Name of your mission</span>
                        </div>
                    </div>
                    <div class="control-group deal-hookpoints">
                        <label class="control-label">Channel &amp; HookPoints</label>
                        <div class="controls">
                            <select name="channel" size="1" class="input-medium" id="channel">
                                <c:forEach var="channel" items="${channels}">
                                    <option value="${channel.id}">${channel.name}</option>
                                </c:forEach>
                            </select>
                            <select id="hookpoints" class="input-xlarge" name="hookpoint">
                                <!-- Added by js -->
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Mission Type</label>
                        <div class="controls">
                            <select class="input-large" name="missionType" id="missionType">
                                <c:forEach var="mt" items="${missionTypes}">
                                    <option value="${mt.id}">${mt.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Mission Template</legend>
                    <div class="span12">
                        <div class="control-group banner-logo-div">
                            <label class="control-label" style="width: auto">Banner Image</label>
                            <div class="controls" style="margin-left: 90px;">
                                <a href="/admin/s3/choose-file" target="_blank" class="btn btn-link">Upload File To S3</a> <br>
                                <input type="text" class="span8" name="bannerImage" id="bannerImage">
                                <span class="help helper-info">Specify Banner Image Url that you get after uploading the file.</span>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="control-group banner-logo-div">
                            <label class="control-label" style="width: auto">Image in Modal</label>
                            <div class="controls" style="margin-left: 90px;">
                                <a href="/admin/s3/choose-file" target="_blank" class="btn btn-link">Upload File To S3</a> <br>
                                <input type="text" class="span8" name="modalImage" id="modalImage">
                                <span class="help helper-info">Specify Modal Image Url that you get after uploading the file.</span>
                            </div>
                        </div>
                    </div>
                    <div class="span6" style="margin-left: 0">
                        <h3>Banner Content</h3>
                        <div class="control-group">
                            <label class="control-label" style="width: auto;">Banner Header</label>
                            <div class="controls" style="margin-left: 50px;">
                                <input type="text" name="bannerHeader" id="bannerHeader" maxlength="500" class="span11">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" style="width: auto;">Brand Name</label>
                            <div class="controls"style="margin-left: 50px;">
                                <input type="text" name="brandName" id="brandName" maxlength="500" class="span11">
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="control-group">
                            <label class="control-label" style="width: auto;">Modal Header</label>
                            <div class="controls" style="margin-left: 50px;">
                                <input type="text" name="modalHeader" id="modalHeader" maxlength="500" class="span11">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" style="width: auto;">Modal Body</label>
                            <div class="controls" id="modalDescriptionContainer" style="margin-left: 50px;">
                                <textarea name="modalContent" rows="2" cols="30" class="span11 desc"></textarea>
                            </div>
                            <a href="#" class="btn" id="desc-add"><i class="icon-plus"></i></a>
                        </div>
                    </div>
                    <div class="span12" style="margin-left: 0">
                        <div class="control-group">
                            <label class="control-label">Action Button Text</label>
                            <div class="controls">
                                <input type="text" id="btnText" class="input-large">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Do you want success message to show up?
                                <input type="checkbox" class="showSuccessContent" id="showSuccessContent" checked="checked">
                            </label>
                        </div>
                        <div class="success-content-box">
                            <h3>Success Content</h3>
                            <div class="control-group">
                                <label class="control-label" style="width: auto;">Header</label>
                                <div class="controls" style="margin-left: 50px;">
                                    <input type="text" name="successHeader" id="successHeader" maxlength="500" class="span11">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="width: auto;">Body</label>
                                <div class="controls" style="margin-left: 50px;">
                                    <textarea name="successContent" id="successContent" rows="2" cols="30" class="span11"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>User action attributes</legend>
                    <div class="span12">
                        <div class="control-group">
                            <label class="control-label">Landing Page URL</label>
                            <div class="controls">
                                <input type="text" id="redirectUrl" class="span12">
                                <span class="help-inline">Add Url without uniqueId parameter.</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Name of UniqueId Parameter</label>
                            <div class="controls">
                                <input type="text" class="uniqueId input-medium" id="uniqueIdKey">
                                <span class="help-block helper-info">FC uniqueId will be specified against this parameter.</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Prefix (If no prefix then leave it blank)</label>
                            <div class="controls">
                                <input type="text" class="prefix input-medium" id="keyPrefix">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Timestamp Param Name</label>
                            <div class="controls">
                                <input type="text" class="timestamp" id="timestamp">
                                <span class="help-block help-info">Mention the timestamp param name if present or leave blank.</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Terms & Conditions</label>
                            <div class="controls" id="tncContainer">
                                <input type="text" class="span12 tnc">
                                <input type="text" class="span12 tnc">
                                <input type="text" class="span12 tnc">
                                <input type="text" class="span12 tnc">
                                <input type="text" class="span12 tnc">
                            </div>
                            <a href="#" class="btn" id="tnc-add"><i class="icon-plus"></i></a>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Do you have an Email to Send?
                                <input type="checkbox" class="sendMail" id="sendMail">
                            </label>
                        </div>
                        <div class="mail-content-container" style="display: none">
                            <div class="control-group">
                                <label class="control-label email-subject">Email Subject</label>
                                <div class="controls">
                                    <input type="text" class="input-xxlarge" id="emailSubject">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label email-content">Mailer Content</label>
                                <div class="controls">
                                    <textarea name="mailerContent" id="emailContent" class="input-xxlarge" rows="50" cols="150"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Do you want user to be redirected to client landing url?
                                <input type="checkbox" class="redirect" id="redirect">
                            </label>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Reward SetUp</legend>
                    <div class="span12">
                        <div class="control-group">
                            <label>Is this an Instant Reward?
                                <input type="checkbox" name="rewardProcessingType" id="rewardProcessingType">
                            </label>
                        </div>
                        <div class="control-group">
                            <label>Is Multiple Rewards Allowed?
                                <input type="checkbox" name="multipleRewards" id="multipleRewards">
                            </label>
                        </div>
                        <div class="control-group">
                            <label class="control-label">CashBack Amount</label>
                            <div class="controls">
                                <input type="text" name="cashback" class="input-medium" id="cashback">
                                <span class="help-inline">Leave it blank if none.</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Cashback Type</label>
                            <div class="controls">
                                <select size="1" id="amountType" class="input-large">
                                    <option value="fixed">Fixed</option>
                                    <option value="percent">Percent</option>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Description</label>
                            <div class="controls">
                                <input type="text" class="input-xxlarge" name="description" id="rewardDescription">
                                <span class="help-inline">This will appear in User's Credits History.</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Reward Mail Subject</label>
                            <div class="controls">
                                <input type="text" class="input-xxlarge" name="mailSubject" id="rewardMailSubject">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Is Special Template Required? <input type="checkbox" id="specialType" value="yes"></label>
                            <div class="controls" id="specialTypeContainer" style="display: none">
                                <input name="specialTemplateName" id="specialTemplateName">Name of the special Template
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-actions">
                    <a href="#" id="mission-form-submit" class="btn btn-success btn-large pull-left">Save Mission</a>
                    <div id="loader" style="margin-left: 166px;padding: 5px;display: none;">
                        <img src="${mt:keyValue("imgprefix1")}/images/ajax-loader.gif">
                    </div>
                </div>
                <div class="alert alert-success" id="submit-status" style="display: none">

                </div>
            </form>
        </fieldset>
    </div>
</div>
<script type="text/javascript">
    var mission = null;
</script>
<c:if test="${not empty mission}">
    <script type="text/javascript">
        mission = $.parseJSON('${mt:escapeJs(mission)}');
    </script>
</c:if>
<script type="text/javascript">
    $(document).ready(function(){
        var hookpoints = {page1: 'Home Page', page2: 'Coupons Page', page5: 'Recharge Success Page', dp: 'Preview Page'};
        for(var k in hookpoints){
            $('#hookpoints').append('<option value="'+k+'">'+hookpoints[k]+'</option>');
        }

        if(mission != null){
            $('#hookpoints').val(mission.hookpoint);
            $('#missionType').val(mission.missionType);
            $('#name').val(mission.name);
            $('#channel').val(mission.channel);
            $('#bannerImage').val(mission.imgPrimary);
            $('#bannerHeader').val(mission.title);
            $('#brandName').val(mission.brandName);
            if(mission.steps){
                $('#modalHeader').val(mission.steps[0].title);
                $('#modalImage').val(mission.steps[0].imgSrc);
                $('#btnText').val(mission.steps[0].btnText);
            }

            if(mission.success){
                $('#showSuccessContent').prop('checked', true);
                $('#successHeader').val(mission.success.header);
                $('#successContent').val(mission.success.content);
            }
            if(mission.sendMail){
                $('#sendMail').prop('checked', true);
                $('#mailSubject').val(mission.emailSubject);
                $('#mailContent').val(mission.emailContent);
            }
            if(mission.rewardProcessingType == 'INSTANT'){
                $('#rewardProcessingType').prop('checked', true);
            }
            $('#cashback').val(mission.cashback);
            $('#amountType').val(mission.amountType);
            $('#rewardMailSubject').val(mission.rewardMailSubject);
            $('#rewardDescription').val(mission.description);
            if(mission.redirect == 1){
                $('#redirect').prop('checked', true);
            }
            if(mission.multipleRewards){
                $('#multipleRewards').prop('checked', true);
            }
            if(mission.specialType && mission.specialType == 1){
                $('#specialType').prop('checked', true);
                $('#specialTypeContainer').show();
                $('#specialTemplateName').val(mission.templateName);
            }
            $('#redirectUrl').val(mission.enteredUrl);
            $('#uniqueIdKey').val(mission.uniqueIdKey);
            $('#keyPrefix').val(mission.keyPrefix);
            $('#timestamp').val(mission.timestampKey);

            var desc = mission.steps?mission.steps[0].description:[];
            var descHtml = '';
            for(var j=0;j<desc.length;j++){
                descHtml+='<textarea rows="2" cols="50" class="span11 desc">'+desc[j]+'</textarea>';
            }
            $('#modalDescriptionContainer').html(descHtml);

            var tnc = mission.steps?mission.steps[0].tnc:[];
            var tncHtml = '';
            for(var i=0; i<tnc.length; i++){
                tncHtml+='<input type="text" class="span12 tnc" value="'+tnc[i]+'">';
            }
            $('#tncContainer').html(tncHtml);
            if(mission.steps && mission.steps[0].redirect == 1){
                $('#redirect').prop('checked', true);
            }
        }

        $('#specialType').on('click', function(ev){
            if($('#specialType').is(':checked')){
                $('#specialTypeContainer').show();
            }else {
                $('#specialTypeContainer').show();
            }
        });
        $('#desc-add').on('click', function(ev){
            ev.preventDefault();
            $('#modalDescriptionContainer').append('<textarea name="modalContent" rows="2" cols="30" class="span11 desc"></textarea>')
        });
        $('#tnc-add').on('click', function(ev){
            ev.preventDefault();
            $('#tncContainer').append('<input type="text" class="span12 tnc">');
        });

        $('.sendMail').on('click', function(ev){
            if($(ev.currentTarget).is(':checked')){
                $('.mail-content-container').show();
            }else {
                $('.mail-content-container').hide();
            }
        });

        $('#showSuccessContent').on('click', function(ev){
            if($(ev.currentTarget).is(':checked')){
                $('.success-content-box').show();
            }else {
                $('.success-content-box').hide();
            }
        });

        $('#mission-form-submit').on('click', function(ev){
            ev.preventDefault();
            var $curEl = $(ev.currentTarget);
            if($curEl.hasClass("disabled")){
                return;
            }
            $curEl.addClass('disabled');
            $('#loader').show();
            //Do Validation
            //Adding plain validations.
            var validationSuccess = true;
            if($('#name').val()==''){
                $('.deal-name-div').addClass('error');
                validationSuccess = false;
            }
            if($('#hookpoints').length<1){
                $('.deal-hookpoints').addClass('error');
                validationSuccess = false;
            }
            if($('#bannerImage').val().length<1){
                $('.banner-logo-div').addClass('error');
                validationSuccess = false;
            }
            if($('.sendMail').is(':checked')){
                if($('#emailSubject').val()==''){
                    $('.mail-content-container').find('div.email-subject').addClass('error');
                    validationSuccess = false;
                }
                if($('#emailContent').val()==''){
                    $('.mail-content-container').find('div.email-content').addClass('error');
                    validationSuccess = false;
                }
            }

            if(!validationSuccess){
                $curEl.removeClass('disabled');
                $('#loader').hide();
                return;
            }
            //Validation done. Now prepare form data for submission
            var data = {id: $('#id').val(), name: $('#name').val(), channel: $('#channel').val(), hookpoint: $('#hookpoints').val(),
                rewardProcessingType: $('#rewardProcessingType').is(':checked')?'INSTANT':'MANUAL', amountType: $('#amountType').val(),
                cashback: $('#cashback').val(), rewardDescription: $('#rewardDescription').val(), multipleRewards: $('#multipleRewards').is(':checked'),
                rewardMailSubject: $('#rewardMailSubject').val(), missionType: $('#missionType').val()};

            var actionParams = {};

            //based on actionType create action params

            var url = $('#redirectUrl').val();
            actionParams['enteredUrl'] = url;
            var secure = 'false';
            var urlParts = url.split('://');
            if(urlParts[0] == 'https'){
                secure = 'true';
            }
            var uri = urlParts[1].split("?");
            var domain = "";
            var resource = "";
            if(uri[0].indexOf('/') < 0){
                domain = uri[0];
            }else {
                domain = uri[0].substring(0, uri[0].indexOf('/'));
                resource = uri[0].substr(uri[0].indexOf('/'));
            }

            var fixedParams = {};
            if(uri.length>1){
                var queryParams = uri[1].split('&');
                for(var qp in queryParams){
                    var kv = queryParams[qp].split('=');
                    fixedParams[kv[0]] = kv[1];
                }
            }
            var urlObj = {domain: domain, resource: resource, secure: secure, fixedParams: fixedParams};
            var uniqueIdParamName = $('#uniqueIdKey').val();
            if(uniqueIdParamName==''){
                uniqueIdParamName = 'uid';
            }
            actionParams['uniqueIdKey'] = uniqueIdParamName;

            var prefix = $('#keyPrefix').val();
            actionParams['keyPrefix'] = prefix;

            var timestampKey = $('#timestamp').val();
            actionParams['timestampKey'] = timestampKey;

            urlObj['mappings'] = {uniqueId: {key: uniqueIdParamName, isPartOfResource: 'false', prefix: prefix}};
            if(timestampKey!=''){
                urlObj['mappings']['timestamp'] = {key: timestampKey, isPartOfResource: 'false'}
            }
            actionParams['urlParamMap'] = urlObj;

            if($('.sendMail').is(':checked')){
                actionParams['sendMail'] = 'true';
                actionParams['emailSubject'] = $('#emailSubject').val();
                data['emailContent'] = $('#emailContent').val();
            }
            var shouldRedirect = $('#redirect').is(':checked');
            if(shouldRedirect){
                actionParams['redirect']=1;
            }

            data['actionParams'] = JSON.stringify(actionParams);

            //Now prepare deal parameters
            var dealParams = {
                type: 'promo',
                rewardAmount: $('#cashback').val(),
                imgPrimary: $('#bannerImage').val(),
                title: $('#bannerHeader').val(),
                brandName: $('#brandName').val(),
                eventName: 'b_click',
                steps:[
                    {
                        imgSrc: $('#modalImage').val(),
                        title: $('#modalHeader').val(),
                        btnText: $('#btnText').val(),
                        eventName: 'optin',
                        redirect: shouldRedirect?1:0,
                        url: '/app/ardeal/action/promooptin/optin'
                    }
                ]
            };
            if($('#missionType').val() == '1'){
                dealParams['templateName'] = 'missionsSidebar';
            }else {
                dealParams['templateName'] = 'missionsModal';
            }
            if($('#specialType').is(':checked')){
                dealParams['specialType'] = 1;
                dealParams['templateName'] = $('#specialTemplateName').val();
            }
            if($('#showSuccessContent').is(':checked')){
                dealParams['success'] = {
                    header: $('#successHeader').val(),
                    content: $('#successContent').val()
                };
            }
            var descAr = [];
            $('.desc').each(function(){
                if($(this).val() != ''){
                    descAr[descAr.length++] = $(this).val();
                }
            });
            dealParams['steps'][0]['description'] = descAr;
            var tnc = [];
            $('.tnc').each(function(){
                if($(this).val()!=''){
                    tnc[tnc.length++] = $(this).val();
                }
            });
            dealParams['steps'][0]['tnc'] = tnc;
            data['dealParams'] = JSON.stringify(dealParams);

            $.ajax({
                url: '/admin/missions/save-promo',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function(res){
                    $('#submit-status').show().html(res.message);
                    setTimeout(function(){
                        window.location.href = '/admin/missions/home';
                    }, 3000);
                }
            });
        });
    });
</script>