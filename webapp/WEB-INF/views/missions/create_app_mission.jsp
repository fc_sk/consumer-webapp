<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<div class="row-fluid">
    <div class="span12">
        <fieldset>
            <legend>Create App Mission</legend>
            <form class="form form-horizontal" id="mission-form" method="post">
                <input type="hidden" name="id" value='<c:if test="${not empty ardeal}">${ardeal.id}</c:if>' id="id">
                <fieldset>
                    <legend>Mission Attributes</legend>
                    <div class="control-group deal-name-div">
                        <label class="control-label">Mission Name</label>
                        <div class="controls">
                            <input type="text" maxlength="35" class="input-xlarge" name="dealName" id="name">
                            <span class="help-inline">Name of your mission</span>
                        </div>
                    </div>
                    <div class="control-group deal-hookpoints">
                        <label class="control-label">Channel &amp; HookPoints</label>
                        <div class="controls">
                            <select name="channel" size="1" class="input-medium" id="channel">
                                <c:forEach var="channel" items="${channels}">
                                    <option value="${channel.id}">${channel.name}</option>
                                </c:forEach>
                            </select>
                            <select id="hookpoints" class="input-xlarge" name="hookpoint">
                                <!-- Added by js -->
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">App Mission Type</label>
                        <div class="controls">
                            <select name="subType" id="subType" class="input-large">
                                <option value="ad">Ad</option>
                                <option value="mission">Mission</option>
                            </select>
                            <span class="help-inline">Banner Ad or Mission</span>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Mission Content</legend>
                    <div class="control-group">
                        <a href="/admin/s3/choose-file" target="_blank" class="btn btn-link">Upload File To S3</a> <br>
                        <label class="control-label">Mission Banner Image</label>
                        <div class="controls">
                            <input type="text" class="span12" id="bannerImage">
                            <span class="help helper-info">Specify Banner Image Url that you get after uploading the file.</span>
                        </div>
                    </div>
                    <div class="control-group">
                        <a href="/admin/s3/choose-file" target="_blank" class="btn btn-link">Upload File To S3</a> <br>
                        <label class="control-label">Mission Page Banner</label>
                        <div class="controls">
                            <input type="text" class="span12" id="pageBanner">
                            <span class="help helper-info">Specify Page Banner Image Url that you get after uploading the file.</span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" style="width: auto;">Banner Header</label>
                        <div class="controls" style="margin-left: 50px;">
                            <input type="text" name="bannerHeader" id="bannerHeader" maxlength="500" class="span11">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" style="width: auto;">Banner Body</label>
                        <div class="controls" id="bannerBody" style="margin-left: 50px;">
                            <textarea name="content" rows="2" cols="30" class="span11 desc"></textarea>
                        </div>
                        <a href="#" class="btn" id="desc-add"><i class="icon-plus"></i></a>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Button Text</label>
                        <div class="controls">
                            <input type="text" id="btnText" class="input-xlarge">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Terms & Conditions</label>
                        <div class="controls" id="tncContainer">
                            <input type="text" class="span12 tnc">
                            <input type="text" class="span12 tnc">
                            <input type="text" class="span12 tnc">
                            <input type="text" class="span12 tnc">
                            <input type="text" class="span12 tnc">
                        </div>
                        <a href="#" class="btn" id="tnc-add"><i class="icon-plus"></i></a>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>User action attributes</legend>
                    <div class="span12">
                        <div class="control-group">
                            <label class="control-label">Landing Page URL</label>
                            <div class="controls">
                                <input type="text" id="redirectUrl" class="span12">
                                <span class="help-inline">Add Url without uniqueId parameter.</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Name of UniqueId Parameter</label>
                            <div class="controls">
                                <input type="text" class="uniqueId input-medium" id="uniqueIdKey">
                                <span class="help-block helper-info">FC uniqueId will be specified against this parameter.</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Prefix (If no prefix then leave it blank)</label>
                            <div class="controls">
                                <input type="text" class="prefix input-medium" id="keyPrefix">
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Reward SetUp</legend>
                    <div class="span12">
                        <div class="control-group">
                            <label>Is this an Instant Reward?
                                <input type="checkbox" name="rewardProcessingType" id="rewardProcessingType">
                            </label>
                        </div>
                        <div class="control-group">
                            <label>Is Multiple Rewards Allowed?
                                <input type="checkbox" name="multipleRewards" id="multipleRewards">
                            </label>
                        </div>
                        <div class="control-group">
                            <label class="control-label">CashBack Amount</label>
                            <div class="controls">
                                <input type="text" name="cashback" class="input-medium" id="cashback">
                                <span class="help-inline">Leave it blank if none.</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Cashback Type</label>
                            <div class="controls">
                                <select size="1" id="amountType" class="input-large">
                                    <option value="fixed">Fixed</option>
                                    <option value="percent">Percent</option>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Description</label>
                            <div class="controls">
                                <input type="text" class="input-xxlarge" name="description" id="rewardDescription">
                                <span class="help-inline">This will appear in User's Credits History.</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Reward Mail Subject</label>
                            <div class="controls">
                                <input type="text" class="input-xxlarge" name="mailSubject" id="rewardMailSubject">
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-actions">
                    <a href="#" id="mission-form-submit" class="btn btn-success btn-large pull-left">Save Mission</a>
                    <div id="loader" style="margin-left: 166px;padding: 5px;display: none;">
                        <img src="${mt:keyValue("imgprefix1")}/images/ajax-loader.gif">
                    </div>
                </div>
                <div class="alert alert-success" id="submit-status" style="display: none">

                </div>
            </form>
        </fieldset>
    </div>
</div>
<script type="text/javascript">
    var mission = null;
</script>
<c:if test="${not empty mission}">
    <script type="text/javascript">
        mission = $.parseJSON('${mt:escapeJs(mission)}');
    </script>
</c:if>

<script type="text/javascript">
    $(document).ready(function(){
        var hookpoints = {page1: 'Home Page', page2: 'Coupons Page', page5: 'Recharge Success Page', dp: 'Preview Page', appGal: 'App Gallery'};
        for(var k in hookpoints){
            $('#hookpoints').append('<option value="'+k+'">'+hookpoints[k]+'</option>');
        }

        if(mission != null){
            $('#hookpoints').val(mission.hookpoint);
            $('#name').val(mission.name);
            $('#channel').val(mission.channel);
            $('#bannerImage').val(mission.template.imageUrl);
            $('#pageBanner').val(mission.template.bannerImageUrl);
            $('#bannerHeader').val(mission.template.title);
            $('#btnText').val(mission.btnText);
            $('#subType').val(mission.subType);
            var descHtml = '';
            for(var j=0;j<mission.description.length;j++){
                descHtml+='<textarea rows="2" cols="50" class="span11 desc">'+mission.description[j]+'</textarea>';
            }
            $('#bannerBody').html(descHtml);

            var tncHtml = '';
            for(var i=0; i<mission.tnc.length; i++){
                tncHtml+='<input type="text" class="span12 tnc" value="'+mission.tnc[i]+'">';
            }
            $('#tncContainer').html(tncHtml);

            if(mission.reward.rewardProcessingType == 'INSTANT'){
                $('#rewardProcessingType').prop('checked', true);
            }
            $('#cashback').val(mission.reward.cashback);
            $('#amountType').val(mission.reward.amountType);
            $('#rewardMailSubject').val(mission.reward.rewardMailSubject);
            $('#rewardDescription').val(mission.reward.description);
            if(mission.multipleRewards){
                $('#multipleRewards').prop('checked', true);
            }
            $('#redirectUrl').val(mission.enteredUrl);
            $('#uniqueIdKey').val(mission.uniqueIdKey);
            $('#keyPrefix').val(mission.keyPrefix);

        }
        $('#desc-add').on('click', function(ev){
            ev.preventDefault();
            $('#bannerBody').append('<textarea name="modalContent" rows="2" cols="30" class="span11 desc"></textarea>')
        });
        $('#tnc-add').on('click', function(ev){
            ev.preventDefault();
            $('#tncContainer').append('<input type="text" class="span12 tnc">');
        });

        $('#mission-form-submit').on('click', function(ev){
            ev.preventDefault();
            var $curEl = $(ev.currentTarget);
            if($curEl.hasClass("disabled")){
                return;
            }
            $curEl.addClass('disabled');
            $('#loader').show();

            var data = {id: $('#id').val(), name: $('#name').val(), channel: $('#channel').val(),
                hookpoint: $('#hookpoints').val(), missionType: 2,
                rewardProcessingType: $('#rewardProcessingType').is(':checked')?'INSTANT':'MANUAL',
                amountType: $('#amountType').val(), cashback: $('#cashback').val(),
                rewardDescription: $('#rewardDescription').val(),
                multipleRewards: $('#multipleRewards').is(':checked'),
                rewardMailSubject: $('#rewardMailSubject').val()
            };

            var actionParams = {};

            //based on actionType create action params

            var url = $('#redirectUrl').val();
            actionParams['enteredUrl'] = url;
            var secure = 'false';
            var urlParts = url.split('://');
            if(urlParts[0] == 'https'){
                secure = 'true';
            }
            var uri = urlParts[1].split("?");
            var domain = "";
            var resource = "";
            if(uri[0].indexOf('/') < 0){
                domain = uri[0];
            }else {
                domain = uri[0].substring(0, uri[0].indexOf('/'));
                resource = uri[0].substr(uri[0].indexOf('/'));
            }

            var fixedParams = {};
            if(uri.length>1){
                var queryParams = uri[1].split('&');
                for(var qp in queryParams){
                    var kv = queryParams[qp].split('=');
                    fixedParams[kv[0]] = kv[1];
                }
            }
            var urlObj = {domain: domain, resource: resource, secure: secure, fixedParams: fixedParams};
            var uniqueIdParamName = $('#uniqueIdKey').val();
            if(uniqueIdParamName==''){
                uniqueIdParamName = 'uid';
            }
            actionParams['uniqueIdKey'] = uniqueIdParamName;

            var prefix = $('#keyPrefix').val();
            actionParams['keyPrefix'] = prefix;

            urlObj['mappings'] = {uniqueId: {key: uniqueIdParamName, isPartOfResource: 'false', prefix: prefix}};
            actionParams['urlParamMap'] = urlObj;
            actionParams['redirect']=1;
            data['actionParams'] = JSON.stringify(actionParams);

            //deal params
            var dealParams = {
                type: 'advertisement',
                rewardAmount: $('#cashback').val(),
                eventName: 'b_click',
                template: {
                    imageUrl: $('#bannerImage').val(),
                    title: $('#bannerHeader').val(),
                    bannerImageUrl: $('#pageBanner').val()
                },
                btnText: $('#btnText').val(),
                subType: $('#subType').val()
            };

            var descAr = [];
            $('.desc').each(function(){
                if($(this).val() != ''){
                    descAr[descAr.length++] = $(this).val();
                }
            });
            dealParams['description'] = descAr;
            var tnc = [];
            $('.tnc').each(function(){
                if($(this).val()!=''){
                    tnc[tnc.length++] = $(this).val();
                }
            });
            dealParams['tnc'] = tnc;
            data['dealParams'] = JSON.stringify(dealParams);
            console.log(data);
            $.ajax({
                url: '/admin/missions/save-promo',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function(res){
                    $('#submit-status').show().html(res.message);
                    setTimeout(function(){
                        window.location.href = '/admin/missions/home';
                    }, 2000);
                }
            });
        });
    });
</script>