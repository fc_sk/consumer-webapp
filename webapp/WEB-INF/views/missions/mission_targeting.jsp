<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<head>
    <link href="/content/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        .condition {
            border: 1px solid #ccc;
            padding: 10px;
        }
        .selected {
            background-color: #34495e;
            color: #fff;
        }
        .disabled {
            background-color: #eaedef;
        }
        .normal {
            background-color: #919bac;
            color: #fff;
        }
    </style>
</head>
<div class="row-fluid">
    <div class="span12">
        <c:choose>
            <c:when test="${not empty mission}">
                <fieldset>
                    <legend>SetUp Targeting for ${mission.name} Mission</legend>
                    <div class="span12">
                        <fieldset>
                            <legend>Current Targeting</legend>
                            <div class="span12 current-conditions">
                                <c:choose>
                                    <c:when test="${not empty currentCondition}">
                                        ${currentCondition}
                                    </c:when>
                                    <c:otherwise>
                                        No condition is set currently
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </fieldset>
                    </div>
                    <div class="span12">
                        <fieldset>
                            <legend>Set Up New Targeting</legend>
                            <div class="span12 form-actions">
                                <div class="span4">
                                    <label>Select Targeting type and Create</label>
                                    <select size="1" class="input-xlarge" id="cond-select">
                                        <c:forEach items="${conditionTypes}" var="condType">
                                            <option value="${condType.id}">${condType.name}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="span8" id="condMain" style="display: none">
                                    <h4 class="jsDesc"></h4>
                                    <div class="span12" id="cond-set-up"></div>
                                    <a href="#" class="btn btn-info btn-small" id="create-target">Create</a>
                                </div>

                            </div>
                            <div class="span12 hero-unit" id="new-conditions">

                            </div>
                            <div class="span12" id="combinations" style="display: none">
                                <p>
                                    <a href="#" id="c-or" data-type="OR" data-type_id="${orTypeId}" class="btn btn-inverse combine">OR</a>
                                    <a href="#" id="c-and" data-type="AND" data-type_id="${andTypeId}" class="btn btn-inverse combine">AND</a>
                                </p>
                            </div>
                            <div id="target-submit" style="display: none;">
                                <a href="#" id="final-target" class="btn btn-large btn-success pull-right">Create Targeting</a>
                                <div id="loader" style="margin-left: 166px;padding: 5px;display: none;">
                                    <img src="${mt:keyValue("imgprefix1")}/images/ajax-loader.gif">
                                </div>
                                <div id="submit-status" class="alert alert-success" style="display: none"></div>
                            </div>
                        </fieldset>
                    </div>
                </fieldset>
            </c:when>
            <c:otherwise>
                <div class="alert alert-danger">
                    ${status}: ${message}
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<script type="text/javascript">
    var conditionTypes = $.parseJSON("${mt:escapeJs(jsConditionTypes)}");
    var kvHtml = '<label class="param-label">_KEY_</label> <input data-key="_DATA_" type="text" class="input-large">';
    var kListHtml = '<label class="param-label">_KEY_</label> <div class="inp-list"> <input type="text" class="input-large"> ' +
            '<a href="#" class="btn js-add-val"><i class="icon-plus-sign"></i></a> </div>';

    function showConditionForm(id){
        var condMap = conditionTypes[id];
        var params = $.parseJSON(condMap['params']);
        $('#condMain').show();
        $('.jsDesc').html(condMap.name+"   " +condMap.description);

        var condKeys = params.condKeys;
        for(var k in condKeys){
            var key = condKeys[k];
            if(key.type == 'list'){
                var kList = kListHtml.replace('_KEY_', k);
                $('#cond-set-up').html(kList);
                $('.inp-list').data('key', k);
                $('.inp-list').on('click', 'a.js-add-val', function(ev){
                    ev.preventDefault();
                    $(ev.currentTarget).before('<input type="text" class="input-large">');
                });
            }else {
                var kHtml = kvHtml.replace('_KEY_', k).replace('_DATA_', k);
                $('#cond-set-up').html(kHtml);
            }
            $('#create-target').data('ptype', key.type);
            $('#create-target').data('cid', id);
        }
    }

    $(document).ready(function(){
        var i = 0;
        var newCondData = [];
        var formData = {id: ${mission.id}};
        var head = 0;
        var newCondHtml = '<div id="con__ID_" data-id="_ID_" class="span3 condition normal jsNewCond">' +
                '<a href="#" class="close-cond" style="float: right;color: #fff" title="Remove"><strong>X</strong></a> ' +
                '<h5>_NAME_</h5><p>_PARAMS_</p>' +
                '<input type="hidden" name="typeId" value="_TYPE_ID_">' +
                '<input type="hidden" name="params" value="_PARAMS_"><input type="hidden" name="name" value="_NAME_"></div>';
        $('#cond-select').on('change', function(ev){
            var typeId = $(ev.currentTarget).val();
            showConditionForm(typeId);
        });
        $('#create-target').on('conditionadded', function(ev){
            $('#combinations').show();
            $('#target-submit').show();
        });
        $('#create-target').on('click', function(ev){
            ev.preventDefault();
            var $me = $(ev.currentTarget);
            var paramsObj = {};
            if($me.data('ptype') == 'list'){
                var valList = [];
                $('.inp-list').find(':input').each(function(){
                    var vl = $(this).val();
                    if(vl!=''){
                        if(!isNaN(vl)){
                            valList[valList.length++] = vl*1;
                        }else {
                            valList[valList.length++] = vl;
                        }
                    }else {
                        alert("Please add proper values!");
                        return;
                    }
                });
                paramsObj[$('.inp-list').data('key')] = valList;
            }else {
                var $inpEl = $('#cond-set-up').find(':input');
                var vl = $inpEl.val();
                if(vl!=''){
                    if(!isNaN(vl)){
                        paramsObj[$inpEl.data('key')] = vl*1;
                    }else {
                        paramsObj[$inpEl.data('key')] = vl;
                    }
                }else {
                    alert("Please add proper values!");
                    return;
                }
            }
            var condObj = {
                typeId: $me.data('cid'),
                name: 'Deal_${mission.id}_'+i,
                params: JSON.stringify(paramsObj)
            };
            newCondData[i] = condObj;
            head = i;
            var conHtml = newCondHtml.replace(/_ID_/g, i+"").replace(/_PARAMS_/g, condObj.params).
                    replace(/_TYPE_ID_/g, condObj.typeId).replace(/_NAME_/g, condObj.name);
            $('#new-conditions').append(conHtml);
            $('#condMain').hide();
            i++;
            $me.trigger('conditionadded');
        });

        var combArr = [];
        $('#new-conditions').on('click', 'div.jsNewCond', function(ev){
            if($(ev.currentTarget).hasClass('disabled')){
                alert("Can not select.");
                return;
            }
            $(ev.currentTarget).addClass('selected').removeClass('normal');
            combArr[combArr.length++] = $(ev.currentTarget).data('id');
        });

        $('#new-conditions').on('click', 'a.close-cond', function(ev){
            ev.preventDefault();
            var $me = $(ev.currentTarget);
            var index = $me.parent().data('id');
            newCondData[index] = null;
            $me.parent().remove();
        });

        $('a.combine').on('click', function(ev){
            ev.preventDefault();
            var $me = $(ev.currentTarget);
            var opType = $me.data('type');
            var typeId = $me.data('type_id');
            var dealName = '';
            var dependencies = [];
            if(combArr.length<2){
                alert("Select some conditions!");
                return;
            }
            for(var k in combArr){
                if(newCondData[combArr[k]].dependencies){
                    dealName+=' ('+newCondData[combArr[k]].name+') '+opType;
                }else {
                    dealName+=' '+newCondData[combArr[k]].name+' '+opType;
                }
                dependencies[dependencies.length++] = newCondData[combArr[k]];
            }
            if(opType=='or'){
                dealName = dealName.substr(0, dealName.length-2);
            }else {
                dealName = dealName.substr(0, dealName.length-3);
            }
            var condObj = {
                displayName: dealName,
                name: 'Deal_${mission.id}_'+i,
                operator: opType,
                typeId: typeId,
                dependencies: dependencies
            };
            newCondData[i] = condObj;
            head = i;
            var conHtml = newCondHtml.replace(/_ID_/g, i+"").replace(/_PARAMS_/g, '').
                    replace(/_TYPE_ID_/g, condObj.typeId).replace(/_NAME_/g, condObj.displayName);
            $('#new-conditions').append(conHtml);
            i++;

            for(var k in combArr){
                $('#con_'+combArr[k]).find('a.close-cond').remove();
                $('#con_'+combArr[k]).removeClass('normal').addClass('disabled');
            }
            combArr = [];
        });

        $('#final-target').on('click', function(ev){
            ev.preventDefault();
            var $me = $(ev.currentTarget);
            if($me.hasClass('disabled')){
                return;
            }
            $me.addClass('disabled');
            $('#loader').show();
            prepareRequestData(newCondData[head], '');
            console.log(formData);
            //Submit the form
            $.ajax({
                url: '/admin/missions/targeting/save',
                type: 'POST',
                dataType: 'json',
                data: formData,
                success: function(res){
                    $('#submit-status').show().removeClass('alert-danger').addClass('alert-success').html(res.message);
                    setTimeout(function(){
                        window.location.reload();
                    }, 3000);
                },
                error: function(xhr){
                    $('#loader').hide();
                    $me.removeClass('disabled');
                    $('#submit-status').removeClass('alert-success').addClass('alert-danger').html($.parseJSON(xhr.responseText).message);
                }
            });
        });

        function prepareRequestData(cObj, prefix){
            var name = prefix+'name';
            var typeId = prefix+'typeId';
            var reqObj = {};
            formData[name] = cObj.name;
            formData[typeId] = cObj.typeId;
            if(cObj.operator){
                var operator = prefix+'operator';
                formData[operator] = cObj.operator;
            }
            if(cObj.dependencies){
                var deps = [];
                for(var x=0;x<cObj.dependencies.length;x++){
                    deps[deps.length++] = prepareRequestData(cObj.dependencies[x], prefix+'dependencies['+x+'].');
                }
//                var dependencies = prefix+'.dependencies';
//                reqObj[dependencies] = deps;
            }else {
                var params = prefix+'params';
                formData[params] = cObj.params;
            }
        }
    });
</script>
