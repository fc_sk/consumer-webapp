<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="row-fluid">
		<div class="span12">
			<fieldset>
	            <form class="form form-horizontal" method="post" id="edit-form" action="/admin/missions/save-email">
	                <fieldset>
		                <input type="hidden" name="id" value='${id}' id="id" name="id">
	                    <div class="control-group deal-name-div">
	                        <label class="control-label">Edit EmailTemplate</label>
	                        <div class="controls">
	                            <textarea rows="30" cols="10" class="span10" name="newEmailTemplate" id="emailTemplate">${emailTemplate}</textarea>
	                            <span class="help-inline">new email template</span>
	                        </div>
	                    </div>

						<input type="submit" value="Submit" class="btn btn-info"/>
					</fieldset>
				</form>
			</fieldset>
		</div>
	</div>
</body>
</html>