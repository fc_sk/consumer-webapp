		<nav>
			<ul>
				<li><a href="/app/aboutus.htm">About us</a></li>
				<li><a href="/app/contactus.htm">Contact us</a></li>
				<li><a href="/app/faq.htm">FAQ</a></li>
				<li><a href="/app/sitemap.htm">Sitemap</a></li>
				<%--<li><a href="/app/feedback.htm" >Feedback</a></li>--%>
				<li><a href="http://support.freecharge.in">Customer
						support</a></li>
				<li><a href="/app/termsandconditions.htm">Terms &amp;
						conditions</a></li>
				<li><a href="/app/privacypolicy.htm">Privacy
						policy</a></li>
				<li><a href="/security/disclosure.htm" class="active">Security Policy</a></li>
				<br>
			</ul>
		</nav>
<div class="row">
	<div class="col-md-12">

		<section class="static-content">
			<h1>Responsible Disclosure</h1>
			We at FreeCharge are committed to protecting our customer's privacy and ensuring that our customers have a 
			safe and secure experience with us. If you discover a security vulnerability in our platform we appreciate 
			your support in disclosing it to us in a responsible manner.Before reporting the vulnerability, please be 
			sure to review our Responsible disclosure policy and scoping guidelines. By participating in this program, 
			you agree to be bound by these rules.
			<h2>Disclosure Policy</h2>
			<h3>If you comply with the following policies while reporting a security vulnerability, we will not pursue 
			any legal action or law enforcement activity against you.
			</h3>
			<ul>
				<li>Report your finding by writing to us directly at security@freecharge.com without making any information public. 
				We will confirm a reply within 72 working hours of submission.
				</li>
				<li>Information about any vulnerability you&apos;ve discovered must be kept confidential between us until we 
				have resolved the problem.
				</li>
				<li>Make every effort to avoid privacy violations, disruption to production systems, degradation of 
					user experience and destruction of data during security testing.</li>
				<li>You do not exploit a security issue you discover for any reason. (This may include demonstrating 
					any additional risk, such as compromise of sensitive company data or probing for additional issues.)
				</li>
				<li>Perform research only within the scope, as described below </li>
				<li>Don&apos;t attempt to gain access to another user&apos;s account or data.</li>
				<li>Do not perform any attack that may harm the reliability/integrity of our services or data. DDoS/spam attacks are not allowed.</li>
				<li>Never attempt any non-technical attacks such as social engineering, phishing, or physical attacks against our infrastructure, users or employees</li>
				</ul>
				<h3> Scope </h3>
				<ul>
				    <li>https://www.freecharge.in</li>
                    <li>Freecharge mobile app -- Android, iOS and Windows</li>
			   </ul>
			   <h3> Reporting guidelines </h3>
			     <p>Please include the following information when sending us the details:</p>
			     <ul>
			         <li>Operating System name and version.</li>
			         <li>Browser name and version.</li>
			         <li>Plugin names and version installed in the browser.</li>
			         <li>Steps necessary to reproduce the vulnerability including any specific settings required to be reproduced 
			         (If this contains more than a few steps, please create a video so we can attempt to perform the same steps).</li>
			         <li>A copy of the HTML source code following your successful test.</li>
			         <li>What is the impact of the issue.</li>
			         <li>What are some scenarios where an attacker would be able to leverage this vulnerability?</li>
			         <li>What would be your suggested fix?</li>
			    </ul>
			    <h3>Legal terms:</h3>
			         <p>By participating in FreeCharge&apos;s Responsible Disclosure program (the &ldquo;Program&rdquo;), 
			     you acknowledge that you have read and agree to FreeCharge&apos;s Terms of Service as well as the following:</p>
			     <ul>
			         <li>your participation in the Program will not violate any law applicable to you, or disrupt or compromise any data that is not your own.</li>
			         <li>you are solely responsible for any applicable taxes, withholding or otherwise, arising from or 
			             relating to your participation in the Program, including from any bounty payments.</li>
			         <li>you will not under any circumstances disclose this vulnerability in social media, blogs etc. 
			             except with a written approval from the FreeCharge legal team.</li>
			         <li>FreeCharge reserves the right to terminate or discontinue the Program at its discretion.</li> 
			     </ul>
	   </section>
	</div>
</div>