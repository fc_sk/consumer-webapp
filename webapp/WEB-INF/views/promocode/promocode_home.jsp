<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<!-- .mainContainer Starts  -->
<div class="mainContainer"  >
    <div class="pageWidth clearfix form">
    	<c:if test="${not empty param.message}">
    		<div style="background-color:yellow;padding:10px;">${param.message}</div>
    	</c:if>
    	   <div class="manage_plans boxDesign" style="width:980px;">
       		 <h1><spring:message code="create.promocode" text="#create.promocode#"/></h1>
             <form  name="promocodeform" action="/admin/promocode/create_save.htm" method="POST" cssClass="form" id="promocodeform" >
             		<c:if test="${not empty PROMOCODE}">
             		<input type="hidden" value="${PROMOCODE.promocodeId}" class="input" name="promocode_id"  > 	
             		</c:if>
             		<h3><spring:message code="promo.title" text="#promo.title#"/></h3>
                	<input type="text" value="${PROMOCODE.promoTitle}" class="input"  id="promo_title" name="promo_title" >
                	<br/>
                	<h3><spring:message code="promocode" text="#promocode#"/></h3>
                	<input type="text" value="${PROMOCODE.promoCode}" class="input"  id="promo_code" name="promo_code" >
                	<br/>
                	<h3><spring:message code="on.successmsg" text="#on.successmsg#"/></h3>
                	<input type="text" value="${PROMOCODE.successMsg}" class="input"  id="success_msg" name="success_msg" >
                	<br/>
                	<h3><spring:message code="on.failuremsg" text="#on.failuremsg#"/></h3>
                	 <input type="text" value="${PROMOCODE.failureMsg}" class="input"  id="failure_msg" name="failure_msg" >
                	<br/>
                	<h3><spring:message code="valid.from" text="#valid.from#"/></h3>
                	<input type="text"  value="<fmt:formatDate pattern='yyyy-MM-dd' value='${PROMOCODE.validFrom}'/>" id="valid_from" name="valid_from" >
                	<b><spring:message code="time" text="#time#"/></b> <input type="text" value="<fmt:formatDate pattern="HH:mm"  value='${PROMOCODE.validFrom}' />" class="input"  id="valid_from_time" name="valid_from_time" >
                	<h3><spring:message code="valid.upto" text="#valid.upto#"/></h3>
                	<input type="text"  value="<fmt:formatDate pattern='yyyy-MM-dd' value='${PROMOCODE.validUpto}'/>" id="valid_upto" name="valid_upto" >
                	<b><spring:message code="time" text="#time#"/></b> <input type="text" value="<fmt:formatDate pattern="HH:mm"  value='${PROMOCODE.validUpto}' />" class="input"  id="valid_upto_time" name="valid_upto_time" >
                	<br/><br/><input type="submit"  value="<c:if test="${not empty PROMOCODE}">Save</c:if><c:if test="${empty PROMOCODE}">Submit</c:if>" class="button" id="butsubmit" name="addbut">
             </form>  
           </div>  
           <div  class="manage_plans boxDesign">
           	<table style="width: 100%">
           		<tr><th align="left"><spring:message code="promocode.id" text="#promocode.id#"/></th><th align="left"><spring:message code="promo1.title" text="#promo1.title#"/></th><th align="left"><spring:message code="promo.code" text="#promo.code#"/></th><th align="left"><spring:message code="success.msg" text="#success.msg#"/></th><th align="left"><spring:message code="failure.msg" text="#failure.msg#"/></th><th align="left"><spring:message code="valid.from" text="#valid.from#"/></th><th align="left"><spring:message code="valid.upto" text="#valid.upto#"/></th><th align="left"><spring:message code="edit" text="#edit#"/></th></tr>
           	 	<c:forEach var="promocode" items="${ALL_PROMOCODES}">
           	 	<tr><td>${promocode.promocodeId }</td><td>${promocode.promoTitle }</td><td>${promocode.promoCode }</td><td>${promocode.successMsg }</td><td>${promocode.failureMsg }</td>
           	 	<td><fmt:formatDate pattern='yyyy-MM-dd HH:mm' value='${promocode.validFrom}'/></td>
           	 	<td><fmt:formatDate pattern='yyyy-MM-dd HH:mm' value='${promocode.validUpto}'/></td>
           	 	<td><a href="/admin/promocode/home.htm?promocode_id=${promocode.promocodeId}">Edit</a></td>
           	 	</tr>
           	 	</c:forEach>
           	 </table>		
           </div>
    </div>  
</div>
<script type="text/javascript">

$(function() {
	$( "#valid_upto" ).datepicker({dateFormat: 'yy-mm-dd'});
	$( "#valid_from" ).datepicker({dateFormat: 'yy-mm-dd'});
});
</script>
