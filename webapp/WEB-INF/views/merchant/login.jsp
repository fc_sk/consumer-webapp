<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>

<div class="container">
  <div class="login clearfix">
    <div class="box">
      <h4>Merchant Login</h4>
      <div class="form">
        <form:form action="${merchantLoginAction}" modelAttribute="merchantLoginDO" method="POST" enctype="multipart/form-data">
          <div>
            <label for="">User Name</label>
            <form:input id="userName"  path="userName" />
          </div>
          <div>
            <label for="">Password</label>
            <form:password id="password"  path="password" />
          </div>
          <div>
            <input class="btn btn-primary" type="submit" value="Login">
          </div>
        </form:form>
      </div>
    </div>
  </div>		
</div>