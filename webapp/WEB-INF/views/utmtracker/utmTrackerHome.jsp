		<%@include file="/WEB-INF/includes/taglibs.jsp"%>
		<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<script type="text/javascript">

					function getShortCodeInfo(){
						var shortCodeList={
 							<c:forEach items="${utmShortCodeList}" var="shortCodes">
							"${shortCodes.shortCode}":{"shortCode":"${shortCodes.shortCode}","startDate":"${shortCodes.startDate}",
														"endDate":"${shortCodes.endDate}","defaultUrl":"${shortCodes.shortCode}",
													    "web":"${shortCodes.webUrl}","mobileWeb":"${shortCodes.mobileWebUrl}",
														"android":"${shortCodes.androidAppUrl}","iOS":"${shortCodes.iosAppUrl}",
														"windows":"${shortCodes.windowsAppUrl}"},
							</c:forEach>
						}
						var selectedShortCode=$("#shortCodeList option:selected").text();
						var shortCodeInfo=shortCodeList[selectedShortCode];
						$("table#shortCodeDetails TBODY").html("<tr><td>"+shortCodeInfo.shortCode+"</td><td>"+shortCodeInfo.startDate+"</td><td>"+shortCodeInfo.endDate+"</td><td><a target='_blank' href='"+shortCodeInfo.web+"'>"+shortCodeInfo.web+"</a></td><td><a target='_blank' href='"+shortCodeInfo.mobileWeb+"'>"+shortCodeInfo.mobileWeb+"</a></td><td><a target='_blank' href='"+shortCodeInfo.android+"'>"+shortCodeInfo.android+"</a></td><td><a target='_blank' href='"+shortCodeInfo.iOS+"'>"+shortCodeInfo.iOS+"</a></td><td><a target='_blank' href='"+shortCodeInfo.windows+"'>"+shortCodeInfo.windows+"</a></td><td style='text-align:center'><a href='/admin/utmtracker/home/editShortCode?utm_short_code="+shortCodeInfo.shortCode+"'>EDIT</a></td></tr>");
					}
		</script>

		<section class="container">
            <div class="row">
            <c:if test="${not empty param.message}">
                <div class="span12" style="background-color:yellow;padding:10px;">${param.message}</div>
            </c:if>
            <div class="span2">
				<ul class="nav nav-list">
                    <li class="nav-header">
                        <a href="#">Campaign Configurator</a>
                        <ul class="">
                            <li>
                                <a href="/admin/utmtracker/home/create.htm">Create campaign</a>
                            </li>
                            <li>
                                <a href="/admin/utmtracker/home/search.htm">Search campaign</a>
                            </li>
                        </ul>
                    </li>
					<li class="nav-header">
						<a href="#">Shortcode Configurator</a>
						<ul class="">
							<li>
								<a href="/admin/utmtracker/home/createShortcode.htm">Create Short Code</a>
							</li>
                            <li>
                                <a href="/admin/utmtracker/home/downloadData.htm">Download Short Code</a>
                            </li>
                            <li>
                                <a href="/admin/utmtracker/home/searchShortCode.htm">Search Short Code</a>
                            </li>
						</ul>
					</li>

				</ul>
			</div>

			<div class="span10">
				<c:choose>
				<c:when test="${ (page eq 'create') or (page eq 'edit')}">
					<div class="sectionDiv">
						<c:choose>
						<c:when test="${not empty param.campaignId}"> <h4>Edit campaign</h4> </c:when>
						<c:otherwise><h4>Create campaign</h4></c:otherwise>
					    </c:choose>
						
						<form name="createUtmConfig" <c:choose><c:when test="${not empty param.campaignId}"> action="/admin/utmtracker/edit.htm" </c:when> <c:otherwise>action="/admin/utmtracker/create.htm"</c:otherwise></c:choose> method="post" cssclass="form">
						<c:if test="${not empty param.campaignId}">
							<span class="span12" style="background-color:yellow;width:70%;display:block;word-wrap:break-word;"><a href="http://www.freecharge.com/trackio?fc_master=${utmTracker.fcMaster}&fc_camp=${utmTracker.campaignName}<c:if test='${not empty utmTracker.medium}'>&fc_med=${utmTracker.medium}</c:if><c:if test='${not empty utmTracker.source}'>&fc_src=${utmTracker.source}</c:if><c:if test='${not empty utmTracker.content}'>&fc_cont=${utmTracker.content}</c:if><c:if test='${not empty utmTracker.keyword}'>&fc_key=${utmTracker.keyword}</c:if><c:if test='${not empty utmTracker.url}'>&fc_url=${utmTracker.url}</c:if>" target="_blank">http://www.freecharge.com/trackio?fc_master=${utmTracker.fcMaster}&amp;fc_camp=${utmTracker.campaignName}<c:if test="${not empty utmTracker.medium}">&amp;fc_med=${utmTracker.medium}</c:if><c:if test="${not empty utmTracker.source}">&amp;fc_src=${utmTracker.source}</c:if><c:if test="${not empty utmTracker.content}">&amp;fc_cont=${utmTracker.content}</c:if><c:if test="${not empty utmTracker.keyword}">&amp;fc_key=${utmTracker.keyword}</c:if><c:if test="${not empty utmTracker.url}">&amp;fc_url=${utmTracker.url}</c:if></a></span><br/>
							
							<input type="hidden" value="${utmTracker.campaignId}" name="fc_camp_id"/><br/>
						</c:if> 
							<br/><b>FC Master <sup>*</sup></b><br/><input type="text" name="fc_master" value="${utmTracker.fcMaster}" required onblur="validateFields(this)"/><br/>
							<b>Campaign Name <sup>*</sup></b><br/><input type="text" name="fc_camp" value="${utmTracker.campaignName}" required onblur="validateFields(this)"/><br/>
							<b>Medium </b><br/><input type="text" name="fc_med" value="${utmTracker.medium}" onblur="validateFields(this)"/><br/>
							<b>Source</b><br/><input type="text" name="fc_src" value="${utmTracker.source}" onblur="validateFields(this)"/><br/>
							<b>keyword</b><br/><input type="text" name="fc_key" value="${utmTracker.keyword}" onblur="validateFields(this)"/><br/>
							<b>URL</b><br/><input type="text" name="fc_url" value="${utmTracker.url}" id="utmUrl" onblur="showUrl(this)"/><br/>
							<b>Content</b><br/><input type="text" name="fc_cont" value="${utmTracker.content}" onblur="validateFields(this)"/><br/>
							<b>Forward URL</b><br/><input type="text" name="forward_url" id="fwdUrl" value="${utmTracker.forwardUrl}" onblur="showUrl(this)"/><br/>
							<b><sup>*</sup></b> Mandatory fields <br/>
							<script type="text/javascript">
								function showUrl(holder){
									if($(holder).val().replace(/\s+/g, '').length==0){
										$(holder).val('');
										return;
									}
									var url="https://"+$(holder).val();
									var inputId=$(holder).attr('id');
									if($(holder).has('.'+inputId).length == 0){
										$('.'+inputId).remove();
									}
									$("<a class="+inputId+" target ='_blank' style='padding-left:5px;' href="+url+">"+$(holder).val()+"</a>").insertAfter(holder);
								}

								function validateFields(element){
									if($(element).val().replace(/\s+/g, '').length==0){
										$(element).val('');
									}
								}
							</script>
						<c:choose>
						<c:when test="${page eq 'create'}"> 
							<input type="submit" value="Submit" class="btn btn-primary" name="addbut"/>
							<input type="reset" value="Reset" class="btn btn-primary" name="reset"/>
						</c:when> 
						<c:when test="${page eq 'edit'}">
							<input type="submit" value="Update" class="btn btn-primary" name="update"/>
							<input type="button" value="Cancel" class="btn btn-primary" name="cancel" onclick="<c:if test='${not empty param.campaignId}'> window.location.href='/admin/utmtracker/home/edit?campaignId=${param.campaignId}' </c:if>"/>
						</c:when>
					    </c:choose>
						</form>
					</div>
		        </c:when>


			    <c:when test="${ page eq 'search'}"> 
					<div class="sectionDiv">
						<form name="searchUtm" action="/admin/utmtracker/search">
							<b>Choose search key option:</b> <br/>
							<select class="input" name="searchKey">
								<option value="utm_campaign_name">Campaign Name</option>
								<option value="fc_med">Medium</option>
								<option value="fc_src">Source</option>
								<option value="fc_key">Keyword</option>
							</select><br/>
							<b>Search word:</b><br/><input type="text" name="searchParam" placeholder="Enter Search word" /><br/>
							<input type="submit" value="Submit" class="btn btn-primary"/>
							<input type="reset" value="Reset" class="btn btn-primary"/>
						</form> 

			<%-- to show the search result part of search tab --%>
					<div style="width:100%;overflow:auto;">	
						<table class="table table-striped" style="table-layout:fixed;word-break:break-word;">
							<thead>
								<tr>
									<th style="width:5%">FC Master</th>
									<th style="width:9%">Campaign Name</th>
									<th style="width:6%">Medium</th>
									<th style="width:6%">Source</th>
									<th style="width:6%">Keyword</th>
									<th style="width:11%">URL</th>
									<th style="width:11%">Content</th>
									<th style="width:11%">Forward URL</th>
									<th style="width:5%;"></th>
								</tr>
							</thead>
							<tbody style="word-break:break-all;">
								<c:forEach items="${utmTrackerList}" var="campaign">
									<tr>
										<td>${campaign.fcMaster}</td>
										<td>${campaign.campaignName}</td>
										<td>${campaign.medium}</td>
										<td>${campaign.source}</td>
										<td>${campaign.keyword}</td>
										<td><a href="https://${campaign.url}" target="_blank">${campaign.url}</a></td>
										<td>${campaign.content}</td>
										<td><a href="https://${campaign.forwardUrl}" target="_blank">${campaign.forwardUrl}</a></td>
										<td style="text-align:center"><a href="/admin/utmtracker/home/edit?campaignId=${campaign.campaignId}">EDIT</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>		
					</div>
				</c:when> 
			
				<c:when  test="${(page eq 'createShortcode') or (page eq 'editShortCode')}">
				<div>
					<c:choose>
					<c:when test="${not empty shortCodeId}"> 
						<h4>Edit Short Code</h4> 
						<span class="span12" style="background-color:yellow;width:70%;display:block;word-wrap:break-word;">
							<a href="http://www.freecharge.in/fc/${utmShortCode.shortCode}" target="_blank">http://www.freecharge.in/fc/${utmShortCode.shortCode}</a>
						</span>
					</c:when>
					<c:otherwise><h4>Create Short Code</h4></c:otherwise>
				    </c:choose>
					<form name="keywordForm" <c:choose><c:when test="${not empty shortCodeId}"> action="/admin/utmtracker/editShortCode" </c:when> <c:otherwise>action="/admin/utmtracker/createShortcode"</c:otherwise></c:choose> method="post" cssclass="form">
						<c:if test="${not empty shortCodeId}">	
							<input type="hidden" value="${utmShortCode.shortCodeId}" name="utm_shortcode_id"/><br/>
						</c:if>
					<c:choose>
					<c:when test="${not empty shortCodeId}">
					<b>Short Code:<sup>*</sup></b><br/><input type="text" value="${utmShortCode.shortCode}" name="utm_short_code" required readonly/> <br/>
					</c:when>
					<c:otherwise>
					<b>Short Code:<sup>*</sup></b><br/><input type="text" value="${utmShortCode.shortCode}" name="utm_short_code" required onblur="validateFields(this)"/> <br/>
					</c:otherwise>
					</c:choose>
					<b>Start Date:<sup>*</sup></b><br/>
					<div class="input-append date" id="datePicker1" >
						<input data-format="yyyy/dd/MM" autocomplete="off" type="text" name="start_date" value="<fmt:formatDate pattern='yyyy/dd/MM' value='${utmShortCode.startDate}' />" required/>
						<span class="add-on">
							<i data-date-icon="icon-calendar"></i>
						</span>
					</div><br/>

					<script type="text/javascript">
					$(function() {
						$("#datePicker1").datetimepicker({
							maskInput: true,
							pickTime: false,
							autoClose:true
						});
					});
					</script>

					<b>End Date:<sup>*</sup></b><br/>
					<div class="input-append date" id="datePicker2" >
						<input data-format="yyyy/dd/MM" autocomplete="off" type="text" required name="end_date" value="<fmt:formatDate pattern='yyyy/dd/MM' value='${utmShortCode.endDate}' />" class="required"/>
						<span class="add-on">
							<i data-date-icon="icon-calendar"></i>
						</span>
					</div><br/>

					<script type="text/javascript">
					$(function() {
						$("#datePicker2").datetimepicker({
							maskInput: true,
							pickTime: false,
							autoClose:true
						});
					});
					</script>
					<label><b>Auto detect User platform?</b><input id="detectUP" type="checkbox" class="checkbox inline" name="autoDetect" onchange="showRedirectionOptions(this.checked)"/></label><br/>
					<div id="redirectionOptns" style="display:none">
						<b>Default URL:<sup>*</sup></b><br/>
						<input type="text" value="www.freecharge.in" name="defaultUrl" required onblur="validateFields(this)"/> <br/>
						<b>Web:</b><br/>
						<input type="text" name="webUrl" value="${utmShortCode.webUrl}" onblur="validateFields(this)"/> <br/>
						<b>Mobile Web:</b><br/>
						<input type="text" name="mobileWebUrl" value="${utmShortCode.mobileWebUrl}" onblur="validateFields(this)"/> <br/>
						<b>Android:</b><br/>
						<input type="text" name="androidAppUrl" value="${utmShortCode.androidAppUrl}" onblur="validateFields(this)"/> <br/>
						<b>iOS:</b><br/>
						<input type="text" name="iosAppUrl" value="${utmShortCode.iosAppUrl}" onblur="validateFields(this)"/> <br/>
						<b>Windows:</b><br/>
						<input type="text" name="windowsAppUrl" value="${utmShortCode.windowsAppUrl}" onblur="validateFields(this)"/> <br/>
					</div>
					<b><sup>*</sup></b> Mandatory fields <br/>
						<input type="submit" value="Submit" name="submit" class="btn btn-primary"/>
						<input type="reset" value="Reset" name="reset" class="btn btn-danger" onclick="formReset()"/>

						<script type="text/javascript">
							function showRedirectionOptions(checked){
								if(checked){
									$("#redirectionOptns").show();
								}else{
									$("#redirectionOptns").hide();
								}
							}
							function validateFields(element){
								if($(element).val().replace(/\s+/g, '').length==0){
									$(element).val('');
								}
							}
							function formReset(){
								<c:choose>
								<c:when test="${(not empty utmShortCode.webUrl) or (not empty utmShortCode.mobileWebUrl) or (not empty utmShortCode.androidAppUrl) or (not empty utmShortCode.iosAppUrl) or (not empty utmShortCode.windowsAppUrl)}">
								$("#redirectionOptns").show();
								</c:when>
								<c:otherwise>
								$("#redirectionOptns").hide();
								</c:otherwise>
								</c:choose>
							}
							<c:if test="${(not empty utmShortCode.webUrl) or (not empty utmShortCode.mobileWebUrl) or (not empty utmShortCode.androidAppUrl) or (not empty utmShortCode.iosAppUrl) or (not empty utmShortCode.windowsAppUrl)}">
								$("#detectUP").attr("checked","checked");
								$("#redirectionOptns").show();
							</c:if>
						</script>													
						</form>
					</div>
		        </c:when>
                    <c:when  test="${(page eq 'downloadData')}">
                    <h4>Download Short Code</h4>
                       <form name="downloadDataForm" action="/admin/utmtracker/downloadData.htm" method="post" cssclass="form">
                        <select class="input" name="utm_short_code" required>
                            <c:forEach items="${utmShortCodeList}" var="shortCode">
                                <option value="${shortCode.shortCode}">${shortCode.shortCode}</option>
                            </c:forEach>
                        </select><br/>
                        <input type="submit" value="Submit" name="submit" class="btn btn-primary"/>
                       </form>
                    </c:when>
                    <c:when  test="${page eq 'searchShortCode'}">
                    <h4>Search Short Code</h4>
                    <select id="shortCodeList" class="input" onchange="getShortCodeInfo()">
                    	<c:forEach items="${utmShortCodeList}" var="shortCodes">
                    		<option value="${shortCodes.shortCode}">${shortCodes.shortCode}</option>
                        </c:forEach>
                    </select>
                    <div style="width:100%;overflow:auto;">	
						<table id="shortCodeDetails" class="table table-striped" style="table-layout:fixed;word-break:break-word;">
							<thead>
								<tr>
									<th style="width:5%">Short Code</th>
									<th style="width:9%">Start Date</th>
									<th style="width:6%">End Date</th>
									<th style="width:6%">Web</th>
									<th style="width:6%">Mobile Web</th>
									<th style="width:6%">Android</th>
									<th style="width:6%">iOS</th>
									<th style="width:6%">Windows</th>
									<th style="width:5%;"></th>
								</tr>
							</thead>
							<tbody style="word-break:break-all;">
							</tbody>
						</table>
					</div>	
                    <script type="text/javascript">
                    	$("#shortCodeList").trigger('change');
                    </script>
                	</c:when>
                </c:choose>
			</div>
			</div>
		</section>