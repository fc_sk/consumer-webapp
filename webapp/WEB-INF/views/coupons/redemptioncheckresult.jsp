<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<div class="container">
    <div class="main">
	  <a href="/merchant/home.htm">Home</a>
      <div class="details" style="margin-top: 0px;">
      <h4>Coupon Details</h4>
        
      <p><label for="">Coupon Code</label><span><c:out value="${couponCode}"></c:out></span></p>
      <p><label for="">Redemption Status</label><span><c:out value="${redemptionStatus}"></c:out></span></p>

      <c:choose>
       <c:when test="${(redeemStateCode eq 'Redeemable') or (redeemStateCode eq 'Success') or (errorCode eq 'ExpiredCouponCode') or (errorCode eq 'RedeemedCode')}">
          
           <c:if test="${not (redeemStateCode eq 'Redeemable')}">
              <p><label for="">Freecharge Ref</label><span><c:out value="${fcRequestId}"></c:out></span></p>
              <p><label for="">Bill Id / Email / Mobile </label><span><c:out value="${requestId}"></c:out></span></p>
           </c:if>
          <c:if test="${not (redeemStateCode eq 'Redeemable')}">
          	   <c:if test="${not empty redeemCity}">
          	   	<p><label for="">Redemption City</label><span><c:out value="${redeemCity}"></c:out></span></p>
          	   </c:if>	
          	   <c:if test="${not empty redeemState}">
	            <p><label for="">Redemption State</label><span><c:out value="${redeemState}"></c:out></span></p>
	           </c:if> 
	           <c:if test="${not empty redemptionTime}">
	            <p><label for="">Redemption Time</label><span><c:out value="${redemptionTime}"></c:out></span></p>
	           </c:if> 
	           <c:if test="${not empty redeemLocation}">
	            <p><label for="">Redemption Location</label><span><c:out value="${redeemLocation}"></c:out></span></p>
	           </c:if> 
	           
	            <p><label for="">Error Description</label><span><c:out value="${errorMessage}"></c:out></span></p>
          		<p><label for="">Error Reason</label><span><c:out value="${errorReason}"></c:out></span></p>
          </c:if>
       	  <p><label for="">Merchant Name</label><span><c:out value="${merchantName}"></c:out></span></p>
          <p><label for="">Coupon Value</label><span><c:out value="${couponValue}"></c:out></span></p>
          <p><label for="">Coupon Validity</label><span><c:out value="${validity}"></c:out></span></p>
          <p><label for="">Campaign ID</label><span><c:out value="${campaignId}"></c:out></span></p>
          <c:if test="${not empty campaignDetails}">
            <p><label for="">Campaign Details</label><span><c:out value="${campaignDetails}"></c:out></span></p>
          </c:if>
          <c:if test="${not empty termsAndConditions}">
            <p><label for="">Terms &amp; Conditions</label><span><c:out value="${termsAndConditions}"></c:out></span></p>
          </c:if>
        </c:when>
        <c:otherwise>
          <p><label for="">Error Description</label><span><c:out value="${errorMessage}"></c:out></span></p>
          <p><label for="">Error Reason</label><span><c:out value="${errorReason}"></c:out></span></p>
        </c:otherwise>
      </c:choose>
      <c:if test="${redeemStateCode eq 'Redeemable'}">
     	 <div class="validate" >
          <form:form name="redemptionForm" action="${redemptionHandler}" modelAttribute="webRedemptionRequest" method="POST" enctype="multipart/form-data">
           <div>
            <label>Coupon Code </label> <form:input id="couponCode"  path="code" />
            <br>
            <label>Bill Id / Email / Mobile</label> <form:input id="requestId"  path="requestId" />
            <br>
            <label>Store Location*</label> <form:input id="location"  path="location" />
            <br>
            <form:hidden id="merchantId" path="merchantId"/>
            <form:hidden id="city" path="city"/>
            <form:hidden id="state" path="state"/>
            <form:hidden id="fcRequestId" path="fcRequestId"/>
            <br/>
            <input type="button" onclick="submitForm()"  value="Proceed to Redeem" /> 
           </div>
          </form:form>
      </div>
      </c:if> 
      <a href="/merchant/home.htm"><button>Cancel &amp; Redeem another coupon</button></a>
    </div>
</div>


<script type="text/javascript">
function submitForm() {
	if($.trim($("#location").val()) == "") {
		alert("Please enter store location");
		return;
	}
	document.redemptionForm.submit();
}
</script>