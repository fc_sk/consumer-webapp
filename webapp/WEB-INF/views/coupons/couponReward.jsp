<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
	<title>
		Rewards
	</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
	<style type="text/css">
		body{
			background: #ededed;
			font-family: Helvetica, Verdana, Arial;
		}
		.clearfix:after{
			content: "";
			display: table;
			clear: both;
		}
		.btn{
			float: right;
			padding: 18px 42px;
			background: #D55E36;
			border: 0;
			text-transform: uppercase;
			color: #fff;
			font-size: 12px;
			font-weight: 300;
			cursor: pointer;
		}
		#container{
			max-width: 720px;
			margin: 24px auto;
			background: #fff;
		}
		ul{
			list-style-type: none;
			padding: 0;
			margin: 0;
		}
		header{
			padding: 24px;
			border-bottom: 1px solid #eee;
		}
		header img{
			float: left;
			margin-right: 24px;
		}
		header h2{
			margin: 10px 0;
		}
		.coupons li{
			padding: 16px;
			padding-left: 26px;
		}
		.coupons li img{
			width: 95px;
			margin-right: 24px;
			background: #ededed;
			border: 0;
			float: left;
		}
		.coupons li h3{
			font-weight: 300;
			margin: 10px 0;
		}
		.coupons li p{
			color: #aaa;
		    text-transform: uppercase;
    		font-size: 12px;
		}
		.form{
			padding: 28px;
		}
		.form label{
		    float: left;
		    max-width: 60%;
		    line-height: 1.5;
		    color: #757575;
		    margin-bottom: 18px;
		}
		.fright{
			float: right;
		}
		#result{
			font-size: 12px;
		}
	</style>
	<script type="text/javascript" src="/content/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" >
	$( document ).ready(function() {
		$('#claimCoupon').off('click').on('click', function(){
			$.post( "/coupon/reward/claim", { l: "${l}" },function(data){
				if(data==true){
					$('#claimCoupon').prop('disabled', true);
					$('#claimCoupon').css("background-color","#aaa");
					$('#result').html('Email Sent...');
				} else {
					$('#claimCoupon').prop('disabled', true);
					$('#claimCoupon').css("background-color","#aaa");
					$('#result').html('Already Claimed');
				}
			});
		});
	});
	</script>
</head>
<body>
	<div id="container">
		<header class="clearfix">
			<img src="https://d32vr05tkg9faf.cloudfront.net/content/images/emailer/logo.png" alt="FreeCharge">
			<h2>Congratulations!</h2>
			<p>
				You have earned coupons worth Rs. ${totalCouponWorth}
			</p>
		</header>
		<section>
			<ul class="coupons">
			<c:forEach var="coupon"
			items="${coupons}">
				<li class="clearfix">
					<img src="${coupon.couponImagePath}" alt="${coupon.couponName}">
					<h3>${coupon.couponShortDesc}</h3>
					<p>
						${coupon.merchantTitle}
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						|
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Expires on ${coupon.validityDate}
					</p>
				</li>
				</c:forEach>
			</ul>
			<div class="form clearfix">
				<label>You will get an email with all the coupons and redemption details once you claim the rewards.</label>
				<div class="fright">
					<c:if test="${eligible}"> 
						<button class="btn" id="claimCoupon">Claim Coupons</button>
						<p id="result"></p>
					</c:if>
					<c:if test="${not eligible}"> 
						<button class="btn" id="claimCoupon" style="background-color:#aaa;" disabled>Claim Coupons</button>
						<p id="result">Already Claimed</p>
					</c:if>
				</div>
			</div>
		</section>
	</div>
</body>
</html>