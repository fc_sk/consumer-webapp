<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<div class="container">
    <div class="main">
      <div class="validate">
          <form:form action="${redemptionHandler}" modelAttribute="webRedemptionRequest" method="POST" enctype="multipart/form-data">
           <div>
            <label>Coupon Code </label> <form:input id="couponCode"  path="code" />
            <br>
            <form:hidden id="location"  path="location" />
            <form:hidden id="merchantId" path="merchantId"/>
            <form:hidden id="city" path="city"/>
            <form:hidden id="state" path="state"/>
            <form:hidden id="requestId"  path="requestId" />
            <form:hidden id="fcRequestId"  path="fcRequestId" />
            <br/>
            <input type="submit"  value="Check Coupon Status" />
           </div>
          </form:form>
      </div>
    </div>
</div>