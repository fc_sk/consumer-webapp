<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<div class="container">
    <div class="main">
      <a href="/merchant/home.htm">Home</a>
      <div class="details">
      <h4>Coupon Details</h4>
        
      <p><label for="">Coupon Code</label><span><c:out value="${couponCode}"></c:out></span></p>
      <p><label for="">Redemption Status</label><span><c:out value="${redemptionStatus}"></c:out></span></p>
      <p><label for="">Freecharge Ref</label><span><c:out value="${webRedemptionRequest.fcRequestId}"></c:out></span></p>
      <p><label for="">Bill Id / Email / Mobile </label><span><c:out value="${webRedemptionRequest.requestId}"></c:out></span></p>
      <c:choose>
        <c:when test="${(redeemStateCode eq 'Success') or (errorCode eq 'ExpiredCouponCode') or (errorCode eq 'RedeemedCode')}">
          <c:if test="${redeemStateCode eq 'Failure'}">
            <p><label for="">Error Description</label><span><c:out value="${errorMessage}"></c:out></span></p>
            <p><label for="">Error Reason</label><span><c:out value="${errorReason}"></c:out></span></p>
          </c:if>
          <c:if test="${not empty redeemCity}">
         	<p><label for="">Redemption City</label><span><c:out value="${redeemCity}"></c:out></span></p>
          </c:if>	
          <c:if test="${not empty redeemState}">
            <p><label for="">Redemption State</label><span><c:out value="${redeemState}"></c:out></span></p>
           </c:if> 
           <c:if test="${not empty redemptionTime}">
            <p><label for="">Redemption Time</label><span><c:out value="${redemptionTime}"></c:out></span></p>
           </c:if> 
           <c:if test="${not empty redeemLocation}">
            <p><label for="">Redemption Location</label><span><c:out value="${redeemLocation}"></c:out></span></p>
          </c:if> 
          <p><label for="">Merchant Name</label><span><c:out value="${merchantName}"></c:out></span></p>
          <p><label for="">Coupon Value</label><span><c:out value="${couponValue}"></c:out></span></p>
          <p><label for="">Coupon Validity</label><span><c:out value="${validity}"></c:out></span></p>
          <p><label for="">Campaign ID</label><span><c:out value="${campaignId}"></c:out></span></p>
           <c:if test="${not empty campaignDetails}">
            <p><label for="">Campaign Details</label><span><c:out value="${campaignDetails}"></c:out></span></p>
          </c:if>
          <c:if test="${not empty termsAndConditions}">
            <p><label for="">Terms &amp; Conditions</label><span><c:out value="${termsAndConditions}"></c:out></span></p>
          </c:if>
        </c:when>
        <c:otherwise>
          <p><label for="">Error Description</label><span><c:out value="${errorMessage}"></c:out></span></p>
          <p><label for="">Error Reason</label><span><c:out value="${errorReason}"></c:out></span></p>
        </c:otherwise>
      </c:choose>
       <a href="/merchant/home.htm"><button>Cancel &amp; Redeem another coupon</button></a>
    </div>
</div>