<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<head>
    <link href="/content/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>
<div class="container" style="padding-bottom: 10px;">
	<div class="alert"
		style='display: <c:if test="${empty errors and empty message}">none</c:if>'>
		<c:if test="${not empty errors}">
			<c:out value="${errors}" />
		</c:if>
		<c:if test="${not empty message}">
			<c:out value="${message}" />
		</c:if>
		<a class="close" href="#">&times;</a>
	</div>

	<div class="span12">
		<h2>Blacklist Domain</h2>
		
		<form action="/admin/blacklistdomain/add.htm" method="POST"  id="blacklist_domain_form" >		
		<div class="control-group">
			<label for="domain" class="control-label">Domain</label>
			<div class="controls">
				<input name="domain" type="text" id="domain" />		 
			</div>
		</div>
		<div class="control-group">
			<label for="type" class="control-label">Type</label>
			<div class="controls">
				<select name="type" id="type" >
				<c:forEach var="item" items="${blacklistType}" >
					<option value="${item}" >${item}</option>
				</c:forEach>
				</select>			 
			</div>
		</div>
		<div class="form-actions">
			<input id="submitForm" class="btn btn-info" type="submit" value="Add">
		</div>
	</form>
		
		 <br> <br>

		<c:choose>
			<c:when test="${not empty blacklistedDomain}">
				<div class="row">
					<table class="table table-bordered table-striped">
						<tr>
							<th ><strong>Sr.</strong></th>
							<th><strong>Type</strong></th>
							<th><strong>Domain</strong></th>
							<th>Action</th>
						</tr>

						<c:forEach var="entry" items="${blacklistedDomain}" varStatus="status"   >
							<tr>	
								<td>${status.count}</td>
								<td>${entry.type}</td>
								<td>${entry.domain}</td>
								<th><a href="#" onClick='deleteDomain("/admin/blacklistdomain/delete", "${entry.domain}", "${entry.type}")'>Delete</a></th>
							</tr>
						</c:forEach>
					</table>
				</div>				
			</c:when>
			<c:otherwise>
				<h4>No Blacklisted Domain!</h4>
			</c:otherwise>
		</c:choose>

	</div>

<div class="span12">
	<div class="row">
		<div class="span8">
			 <a href="#" onClick='doPost("/admin/blacklistdomain/flushCache")'>Flush Cache</a>
		</div>
	</div>
</div>

</div>
<script type="text/javascript">
 
    function attachClose(){
        $('a.close').on('click', function(ev){
            ev.preventDefault();
            $(ev.currentTarget).parent().remove();
        });
    }
    function doPost(url) {
        $.post(url, {}, function(data) {
        	window.open("/admin/blacklistdomain/home","_self")
        });
      }
    $(document).ready(function(){
    	attachClose();    	
    })
    
    function deleteDomain(url, domain, type) {
	    $.post(url, {'domain' : domain, 'type' : type}, function(data) {
	    	window.open("/admin/blacklistdomain/home","_self")
	    });
   }
</script>