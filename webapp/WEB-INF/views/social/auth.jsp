<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<div>
    <c:if test="${not empty message}">
        <h1>${message}</h1>
    </c:if>
</div>
<script type="text/javascript">
    var status = "${status}";
    var act = "${act}";
    var callback = "${callback}";
    var message = "${message}";
    if(status == 'success'){
        window.opener.FB.setVerified(true);
        window.opener.appController.view.googleChildWIndow = true;
        if(act == 'success'){
            window.opener.FB.setActionState(true);
        }
    } else {
    	window.opener.appController.view.googleChildWIndow = false;
    }
    if(callback && callback!=null && callback.length>0){
        window.opener.appController.handleRedirect({login:true});
        // the line below used to be called earlier instead of handleRedirect(), kept here to avoid regression
        /* window.opener.FB.setInitFBCallback(callback).setMessage(message); */
    }
    window.close();
</script>
