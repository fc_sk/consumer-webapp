<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
	
<html>

	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

		<title>McDonald&apos;s Gift Festival</title>
		<meta name="description" content="Purchase food worth Rs.25 and above and get a Gift Coupon with assured Gifts from McDonalds and Freecharge.com">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				
		<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/pepsi/qbkl-grid.css?v=${mt:keyValue("version.no")}" media="screen">
		<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/pepsi/style1.css?v=${mt:keyValue("version.no")}" media="screen">

		<!-- Loading: Font Awesome -->
		<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/pepsi/font-awesome/css/font-awesome.min.css?v=${mt:keyValue("version.no")}">
		<!--[if IE 7]>
		<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/pepsi/font-awesome/css/font-awesome-ie7.min.css?v=${mt:keyValue("version.no")}">
		<![endif]-->

		<!--[if lt IE 9]>
		<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js?v=${mt:keyValue("version.no")}"></script>
		<![endif]-->
	</head>

	<body>
		
		<!-- Start: Intro -->
		<section id="intro">
			<div class="info">
				<img src="${mt:keyValue('imgprefix1')}/images/banner/mcdGiftFestival-1.png" >
				<h4>Get Free Rs.15 recharge and Rs.5 Cashback at Freecharge.com</h4>
				<p class="intro-text">Purchase food worth Rs.25 and above and get a Gift Coupon with assured Gifts from McDonalds and Freecharge.com</p>
			</div>

			<nav id="nav">
				<!-- The content between <span> </span> tags will be hidden on mobile version -->
				<ul class="clearfix">
					<li><a href="#redeem">How to redeem</a></li>
					<li><a href="#faq">FAQ</a></li>
					<li><a href="#terms">Terms &amp; Conditions</a></li>
				</ul>
			</nav>
		</section>
		<!-- End: Intro -->

		<!-- Start: Guide -->
		<section id="redeem" class="section">
			<div class="container">
				<div class="row">
					<div class="col-full">
						<h2 class="section-title">How to redeem</h2>
						<p class="section-title-text">AND GET RS.15 DISCOUNT ON TALKTIME AND RS.5 CASHBACK USING YOUR UNIQUE CODE</p>
						<div class="centered line"></div>
					</div>
				</div>

				<div class="row section-content">

					<div class="col-full">
						
						<div class="steps">
							<h4>Enter your Mobile details to recharge your phone.</h4>
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/1.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/mcd/step-1.png">
						</div>
					</div>

					<div class="col-full">
						<div class="steps">
							<h4>Pick coupons for the value of your recharge.</h4>
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/2.png">
						</div>
						<div class="steps-snapshot">
							<p class="starred">
								Please note this is optional and a handling charge of Rs.10 is applicable. You could 
								CONTINUE with or without coupon(s).
							</p>
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi-new/step-2.png">
						</div>						

					</div>

					<div class="col-full">
						<div class="steps">
							<h4>If you are an existing user at FreeCharge, enter LOGIN details.</h4>
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/3.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi-new/step-3.png">
						</div>
					</div>
					
					<div class="col-full">
						<div class="steps">
							<h4>If you are new to FreeCharge, Sign Up. </h4>
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi/4.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi-new/step-4.png">
						</div>
					</div>

					<div class="col-full">
						<div class="steps">
							<h4>Enter the unique 12-Digit Code which you received on SMS.</h4>
					    	<img src="${mt:keyValue('imgprefix1')}/images/pepsi/5.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi-new/step-5.png">
						</div>
					</div>

					<div class="col-full">
						<div class="steps">
							<h4>After you have successfully applied your Promocode, click CONTINUE.</h4>
					    	<img src="${mt:keyValue('imgprefix1')}/images/pepsi/6.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi-new/step-6.png">
						</div>
					</div>

					<div class="col-full">
						<div class="steps">
							<h4>Pay the remaining amount.</h4>
					    	<img src="${mt:keyValue('imgprefix1')}/images/pepsi/7.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi-new/step-7.png">
						</div>
					</div>

					<div class="col-full">
						<div class="steps">
							<h4>That&#39;s it. Your Recharge is done!</h4>
					    	<img src="${mt:keyValue('imgprefix1')}/images/pepsi/8.png">
						</div>
						<div class="steps-snapshot">
							<img src="${mt:keyValue('imgprefix1')}/images/pepsi-new/step-8v2.png">
							<p class="starred">*Promo code is for single use and can be redeemed by a unique mobile 
							number only once.</p>
						</div>
					</div>

					<div class="col-full">					
						<div class="get-started">
							<div>Okay, I understand. Take me to FreeCharge. </div>
							<a href="/"  target="_blank" 
							class="red" >Go <i class="icon-long-arrow-right"></i></a>
						</div>
					</div>

					<div class="col-full">
						<div class="separator"></div>
					</div>

				</div>
			</div>

		</section>
		<!-- End: Guide -->


		<!-- Start: FAQ -->
		<section id="faq" class="section">
			<div class="container">
				<div class="row">
					<div class="col-full">
						<h2 class="section-title">Frequently Asked Questions</h2>
						<p class="section-title-text">for redeeming McDonalds promocode</p>
						<div class="centered line"></div>
					</div>
				</div>
				<div class="row section-content">
					<div class="col-full">
						<ol class="faq-text">
							<li>
								<h2 class="question">
									What is &lsquo;McDonalds Gift Festival&rsquo; (MCD)?
								</h2>
								<p class="answer">
									McDonalds Gift Festival is a McDonalds Festive season gifting festival which starts on Dec 27 2013 and runs till Jan 27 2014.
								</p>
							</li>
							<li>
								<h2 class="question">
									How can I get a MCD promocode?
								</h2>
								<p class="answer">
									It&#39;s simple. Just walk into any McDonalds store and purchase food above Rs.25 and get a gift card. Send your unique coupon code to 07302066666 and get an SMS from FreeCharge with your FreeCharge free gift coupon.
								</p>
							</li>
							<li>
								<h2 class="question">
									How can I get more MCD promocodes?
								</h2>
								<p class="answer">
									Visit your McDonald store regularly and order food above Rs.25
								</p>
							</li>
							<li>
								<h2 class="question">
								How can I use a promocode to recharge my number?
								</h2>
								<p class="answer">
									Visit www.freecharge.com/mcdGiftFestival-details#redeem for a detailed walkthrough on "How to Redeem".
								</p>
							</li>
							<li>
								<h2 class="question">
								Is this offer valid for postpaid mobile numbers?
								</h2>
								<p class="answer">
									Yes, this offer is valid for postpaid mobile numbers also.
								</p>
							</li>
							<li>
								<h2 class="question">
									How much discount do I get on my recharge?
								</h2>
								<p class="answer">
									You get a discount of Rs.15 on the "Total Payable Amount". In addition you get a Rs.5 cash back credited to your account.
								</p>
							</li>
							<li>
								<h2 class="question">
									Can I recharge for Rs.15 only?
								</h2>
								<p class="answer">
									No, you have to recharge for minimum Rs.20 (if its a valid denomination for your operator) to get a Rs.15 instant discount.
								</p>
							</li>
							<li>
								<h2 class="question">
									Can I also recharge for more than Rs.20?
								</h2>
								<p class="answer">
									Here&#39;s the thing – you can recharge for any amount up to Rs.1000. Just use the 12-digit code to get a
									discount of Rs.20 on the &lsquo;Total Payable Amount&rsquo;. For example: If you recharge for Rs.50 and select equal 

									value coupons too, your &lsquo;Total Payable Amount&rsquo; is Rs.60 (including the coupon handling charge of Rs.10). 

									Apply the code, avail Rs.20 off and pay Rs.40. Please make sure to check the &lsquo;Popular Plans&rsquo; section to 

									decide the right denomination for you.
								</p>
							</li>
							<li>
								<h2 class="question">
									Rs.20 is not a valid denomination for my mobile operator. How do I make the best use of the code?
								</h2>
								<p class="answer">
									You can recharge for more than Rs.20 too. You just need to pay the balance amount. Do visit our &lsquo;Popular Plans&rsquo; section on the homepage to select the right plan.
								</p>
							</li>
							<li>
								<h2 class="question">
									If my &lsquo;total payable amount&rsquo; is more than Rs.20, how do I pay for the remaining amount?
								</h2>
								<p class="answer">
									The remaining amount can be paid using one of these payment options – debit card and  credit card.
								</p>
							</li>
							<li>
								<h2 class="question">
									Does the MCD promocode also work for Special Recharges like Data/2G, 3G, SMS pack?
								</h2>
								<p class="answer">
									Yes.
								</p>
							</li>
							<li>
								<h2 class="question">
									Can I also use the MCD promocode for DTH and Data card recharge?
								</h2>
								<p class="answer">
									Sure. You can use the code for a recharge of
								</p>
								<ul class="answer">
									<li>
										Up to Rs.5000 for a DTH number
									</li>
									<li>
										Up to Rs.1500 for a data card
									</li>
								</ul>
							</li>
							<li>
								<h2 class="question">
									How many times can I use a MCD promocode?
								</h2>
								<p class="answer">
									The MCD promocode is for one-time use and can be redeemed/used only once by a user/mobile number.
								</p>
							</li>
							<li>
								<h2 class="question">
									While redeeming the MCD promocode, I see an error that says, &ldquo;This code has already been used.

									Please try another unique code.&rdquo; What do I do?
								</h2>
								<p class="answer">
									Well, looks like the promocode entered by you has already been used by another login Id or mobile

									number. If you have another unique 12-digit code, you can use that to avail the offer.
								</p>
							</li>
							<li>
								<h2 class="question">
									I am trying to redeem the MCD promocode but I am getting an error message that says, &ldquo;This code has expired.&rdquo;
								</h2>
								<p class="answer">
									The MCD offer is valid till 30 June, 2014. This message is displayed only if you try to avail the code after the offer period.
								</p>
							</li>
							<li>
								<h2 class="question">
									Can I select equal value coupons during my recharge?
								</h2>
								<p class="answer">
									Of course, you can! After signing-in, enter your recharge details and click &lsquo;Proceed&rsquo; to unlock equal value

									coupons. You can then pick coupons you like for the value of your recharge, and continue to pay. Note: A 

									handling charge of Rs.10 will be applicable.
								</p>
							</li>
							<li>
								<h2 class="question">
									Is the MCD offer valid for International numbers?
								</h2>
								<p class="answer">
									Sorry, but this offer is valid only for Indian numbers.
								</p>
							</li>
							<li>
								<h2 class="question">
									Are all operators covered under this offer?
								</h2>
								<p class="answer">
									The MCD offer is valid for the following operators: Airtel, Aircel, Vodafone, BSNL, Tata Docomo, Idea, Tata

									Walky, Loop, MTNL Trump, Reliance CDMA, Reliance GSM, Tata Indicom, Uninor, MTS, Videocon, Virgin 

									CDMA and Virgin GSM.
								</p>
							</li>
							<li>
								<h2 class="question">
									If my payment fails, can I use the promocode again?
								</h2>
								<p class="answer">
									Yes!
								</p>
							</li>
							<li>
								<h2 class="question">
									What if my recharge fails? Can I use the promocode again?
								</h2>
								<p class="answer">
								Yes. However, if you get a message saying, &ldquo;Promocode blocked. Try in sometime&rsquo;, it is possible that

									your recharge is under process. In this scenario, we suggest you wait and try using the code again, if your 

									recharge fails.
								</p>
							</li>
						</ol>
					</div>
					
					<div class="col-full">					
						<div class="get-started">
							<div>Okay, I understand. Take me to FreeCharge. </div>
							<a href="/"  target="_blank" 
							class="red" >Go <i class="icon-long-arrow-right"></i></a>
						</div>
					</div>

					<div class="col-full">
						<div class="separator"></div>
					</div>

				</div>
			</div>

		</section>
		<!-- End: How to FAQ -->
		

		<!-- Start: T&C -->
		<section id="terms" class="section">
			<div class="container">
				<div class="row">
					<div class="col-full">
						<h2 class="section-title">Terms &amp; Conditions</h2>
						<div class="centered line"></div>
					</div>
				</div>

				<div class="row section-content">

					<div class="col-full">
						
						<ol class="sub-section-text">
                <li>This Promotion ("Offer") is being brought to by FreeCharge.com.</li>
                <li>This Offer is valid in India from 27th Dec'13 to 30th June'14</li>
                <li>The offer is open to Indian residents except employees and the family members of Organizers, their associate companies, advertising agencies and their auditors.</li>
                <li>Participation is optional</li>
                <li>The campaign is only valid for the residents of India, holding valid prepaid &amp; Postpaid, GSM / CDMA mobile phone connections, DTH Connections & Datacard connections available on www.FreeCharge.com</li>
                <li>Purchase food above Rs.25 at any McDonalds store in India and get a discount promo code worth Rs.15 on a minimum recharge of Rs.20 valid only on FreeCharge.com. In addition get a Rs.5 cash back valid only on FreeCharge.com.</li>
                <li>To participate,
                  <ul class="circle">
                    <li>Visit and register on "<a href="www.FreeCharge.com/did4">www.FreeCharge.com/mcd</a>" or "<a href="www.FreeCharge.com/m">www.FreeCharge.com/m"</a> ("website").</li>
                    <li>Enter your prepaid/postpaid mobile number or DTH number or Data card number and insert the recharge value of your choice equal to or greater than Rs.20</li>
                    <li>Enter the unique promocode received via SMS after receiving your McDonalds gift card and get Rs. 15/- OFF on a minimum recharge value of Rs.20/- and an additional Rs.5 cash back.</li>
                  </ul></li>
                <li>A promo code can be used to recharge only once. A user account (1 mobile number &amp; registered ID) is entitled to only two promo code per week. All subsequent attempts to recharge the same mobile number / registered user with a different code will be rejected in that particular week. To avail the offer again, vote for your favourite contestant next week. For Eg: A customer voting for multiple contestants in 1 week will get only 1 promo code via SMS through FreeCharge.com</li>
                <li>Customers holding a valid Credit Card or Debit Card will be able to avail the offer. This offer is not valid for net-banking transactions</li>
                <li>Participants already registered with FreeCharge can also use their existing credentials to avail of the Offer.</li>
                <li>On direct payment of the applicable handling charges, FreeCharge may offer the Participant to select exciting coupons of value equal to the recharge amount. In case the Participant avails such add ons from FreeCharge, McDonalds will not be responsible to pay charges for any additional or other facility (ies) / offer(s) availed by the Participant. It is clarified that McDonalds &amp; Freecharge are responsible for giving discount worth Rs. 15/- on a minimum recharge of Rs.20, in accordance with the present terms and conditions.</li>
                <li>The talk-time credited into a customer's account is non-transferrable, non-refundable; and no cash payment will be made in lieu of this discount amount. Actual talk-time will vary as per the top-up plan of the respective operator and is subject to applicable taxes and levies.</li>
                <li>The code cannot be redeemed retrospectively i.e. after the expiry of the offer period. Any unused balance will not be refunded or credited when offer period has expired.</li>
                <li>The offer is not valid in conjunction with any other offer.</li>
                <li>Neither McDonalds nor FreeCharge will be liable for any delay in crediting the talk-time into the customers' account while transacting on FreeCharge as the time taken for crediting the talk-time into the customer's account is dependent on the telecom operators of the pre-paid mobile/DTH/Data card customer.</li>
                <li>McDonalds and FreeCharge shall not be responsible for Downtime on services rendered by the telecom operators / provider that may occur for any reason whatsoever including without limitation due to decisions and / or changes in regulations that are carried out by TRAI, DoT or any other regulatory body.</li>
                <li>McDonalds and FreeCharge shall not be responsible for downtime caused due to technical difficulties or downtime created at the telecom operator / provider's end either due to failure of hardware equipment at the telecom providers end, configuration issues at the end of the Telecom operator / provider's end, network congestion or for any other reason whatsoever. McDonalds and FreeCharge will not be responsible for any loss or damage of the code.</li>
                <li>All customers related queries for this promotion can be addressed to care@freecharge.com</li>
                <li>FreeCharge reserves the right to refuse to give discount into the participants' account due to fraud or system security breaches.</li>
                <li>Decision of FreeCharge.com will be final and binding with regard to promotion and prize and no correspondence will be entertained in this regard.</li>
                <li>FreeCharge.com is a registered trademark of Accelyst Solutions Pvt Ltd (ASPL).</li>
                <li>The participant is bound by the terms and conditions imposed by FreeCharge.com on its website.</li>
                <li>No cash payment will be made in lieu of the prize.</li>
                <li>FreeCharge.com accepts no responsibility for any tax implications that may arise from the offer promotion. Participants will have to bear incidental costs, if any, that may arise for redemption of the same.</li>
                <li>FreeCharge.com shall not be responsible for any loss, injury or any other liability arising out of the Promotion or due to participation by any person in the Promotion.</li>
                <li>FreeCharge.com shall not be liable for any loss or damage due to Act of God, Governmental actions, other force majeure circumstances and shall not be liable to pay any amount as compensation or otherwise for any such loss.</li>
                <li>FreeCharge.com reserves its right to extend/change/modify/withdraw/terminate the promotion without any prior notice of the same at its sole discretion. FreeCharge.com also reserves the right to modify the terms and conditions without any prior notice.</li>
                <li>All disputes relating to this Promotion shall be subject to the exclusive jurisdiction of Courts at Mumbai only.</li>

						</ol>
					</div>


					<div class="col-full">					
						<div class="get-started">
							<div>Okay, I understand. Take me to FreeCharge. </div>
							<a href="/"  target="_blank" 
							class="red" >Go <i class="icon-long-arrow-right"></i></a>
						</div>
					</div>


					<div class="col-full">
						<div class="separator"></div>
					</div>

				</div>
			</div>
		</section>
		<!-- End: Work Experience -->

		<!-- Loading: jQuery and custom script -->
		<script src="${mt:keyValue("jsprefix2")}/pepsi/jquery-1.10.2.min.js?v=${mt:keyValue("version.no")}"></script>

		<!-- Loading: jQuery Sticky -->
		<script src="${mt:keyValue("jsprefix2")}/pepsi/jquery.sticky.js?v=${mt:keyValue("version.no")}"></script>


		<script src="${mt:keyValue("jsprefix2")}/pepsi/jquery.scrollto.min.js?v=${mt:keyValue("version.no")}"></script>
		<script src="${mt:keyValue("jsprefix2")}/pepsi/jquery.nav.js?v=${mt:keyValue("version.no")}"></script>

		<!--[if lt IE 9]>
		<script src="${mt:keyValue("jsprefix2")}/pepsi/excanvas.js?v=${mt:keyValue("version.no")}"></script>
		<![endif]-->

		<!-- Loading: Custom scripting -->
		<script src="${mt:keyValue("jsprefix2")}/pepsi/custom.curriculum.js?v=${mt:keyValue("version.no")}" type="text/javascript"></script>

		
		<script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-12939857-1']);
            _gaq.push(['_setDomainName', 'freecharge.in']);
            _gaq.push(['_trackPageview']);
            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>

		
	</body>
</html>