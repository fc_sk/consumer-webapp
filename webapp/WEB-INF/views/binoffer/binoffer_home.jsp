<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>

<div class="upsell-setting">
    <h1><spring:message code="fc.binoffer.settings" text="#fc.bioffer.settings#"/></h1>
    <c:if test="${not empty param.message}">
        <div style="background-color:yellow;padding:10px;"><c:out value="${param.message}"/></div>
    </c:if>

    <div class="flex-layout clearfix">
        <div class="one">
            <div class="left-content">
                <h2><spring:message code="available.binoffers" text="#available.binoffers#"/></h2>
                <ul>
                    <c:forEach var="item" items="${ATTR_BINOFFER_BUSSINESS_DO_MAP}">
                        <li>
                            <p><c:out value="${item.key}"/> <br/> [<c:out value="${item.value.bank}"/>] <c:out value="${item.value.name}"/></p>
                            <c:choose>
                                <c:when test="${item.value.isActive eq 1}">
                                    <a id="disableLink${item.value.id}" href="javascript:updateStatus(${item.value.id},0)" ><spring:message code="disable" text="#disable#"/></a>
                                    <a id="enableLink${item.value.id}" href="javascript:updateStatus(${item.value.id},1)" style="display: none;"><strong><spring:message code="enable" text="#enable#"/></strong></a>
                                </c:when>
                                <c:otherwise>
                                    <a id="disableLink${item.value.id}" href="javascript:updateStatus(${item.value.id},0)" style="display: none;"><spring:message code="disable" text="#disable#"/></a>
                                    <a id="enableLink${item.value.id}" href="javascript:updateStatus(${item.value.id},1)"><strong>Enable</strong></a>
                                </c:otherwise>
                            </c:choose>
                            <a href="/admin/binoffer/home.htm?id=${item.value.id}"><spring:message code="edit" text="#edit#"/></a>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
        <div class="two">
            <div class="mid-content">
                <h2><spring:message code="create.binoffer" text="#create.binoffer#"/></h2>
                <form  name="biofferform" <c:if test="${not empty ATTR_BINOFFER_BUSSINESS_DO}"> action="/admin/binoffer/update.htm"</c:if><c:if test="${empty ATTR_BINOFFER_BUSSINESS_DO}">action="/admin/binoffer/create.htm"</c:if> method="POST" cssClass="form" id="binofferform" >
                    <input type="hidden" value="${ATTR_BINOFFER_BUSSINESS_DO.id}"  name="id"  >
                    <table width="100%" class="create-upsell">
                        <tbody>
                            <tr>
                                <td colspan="2"><p class="small-font"><spring:message code="fields.marked" text="#fields.marked"/><span class="red"> * </span><spring:message code="are.mandatory" text="#are.mandatory#"/></p></td>
                            </tr>
                            <!-- formfields starts here -->
                            <tr>
                                <td colspan="2"><!-- error container div start -->
                                    <!-- Div error message start -->
                                    <div style="display:none;" id="err_container">
                                        <p id="inner_err_container"></p>
                                    </div>
                                    <!-- Div error message end -->

                                    <!-- Div confirmation message start -->
                                    <div style="display:none;" id="conf_container">
                                        <p id="inner_conf_container"></p>
                                    </div>
                                    <!-- Div error message end -->
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="name" text="#name#"/></span>:</td>
                                <td width="75%" >
                                    <input type="text" value="${ATTR_BINOFFER_BUSSINESS_DO.name}"  name="name"  title="Enter Name">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="bank" text="#bank#"/></span>:</td>
                                <td width="75%" >
                                	<select name="bank">
                                		<c:forEach  var="bank" items="${BANKS}">
                                			<option value="${bank.key}">${bank.value}</option>
                                		</c:forEach>
                                	</select>
                                	<script type="text/javascript">
                                		$("select[name='bank']").val('${ATTR_BINOFFER_BUSSINESS_DO.bank}');
                                	</script>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="bin" text="#bin#"/></span>:</td>
                                <td width="75%" >
                                	<textarea rows="5" cols="15" style="width: 400px;"  name="bin" <c:if test="${not empty ATTR_BINOFFER_BUSSINESS_DO}"> readonly</c:if> >${binText}</textarea><br/>
                                 <%--    <input type="text" value="${ATTR_BINOFFER_BUSSINESS_DO.bin}"  name="bin"  title="Enter Bin"> --%>
                                </td>
                            </tr>

                            <c:if test="${not empty ATTR_BINOFFER_BUSSINESS_DO}">
                                <tr>
                                    <td width="25%" ><span id="txt_title"><spring:message text="Add bins"/></span>:</td>
                                    <td width="75%" >
                                        <textarea rows="5" cols="15" style="width: 400px;"  name="add_bins"></textarea><br/>
                                    </td>
                                </tr>

                            </c:if>
                             <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="offer.key" text="#offer.key#"/></span>:</td>
                                <td width="75%" >
                                    <input type="text" value="${ATTR_BINOFFER_BUSSINESS_DO.offerKey}"  name="offer_key"  title="Enter Offer Key">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="offer.type" text="#offer.type#"/></span>:</td>
                                <td width="75%" >
                                	<select name="offer_type">
                                		<c:forEach  var="offerType" items="${offerTypes}">
                                			<option value="${offerType.key}">${offerType.value}</option>
                                		</c:forEach>
                                	</select>
                                	<script type="text/javascript">
                                		$("select[name='offer_type']").val('${ATTR_BINOFFER_BUSSINESS_DO.offerType}');
                                	</script>
                                </td>
                            </tr>
                             <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="image.name" text="#image.name#"/></span>:</td>
                                <td width="75%" >
                                    <input type="text" value="${ATTR_BINOFFER_BUSSINESS_DO.imageUrl}"  name="image_url"  title="Enter Image Url">
                                </td>
                            </tr>
                             <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="discount.type" text="#discount.type#"/></span>:</td>
                                <td width="75%" >
                                	<select name="discount_type">
                                		<c:forEach  var="discountType" items="${discountTypes}">
                                			<option value="${discountType.key}">${discountType.value}</option>
                                		</c:forEach>
                                	</select>
                                	<script type="text/javascript">
                                		$("select[name='discount_type']").val('${ATTR_BINOFFER_BUSSINESS_DO.discountType}');
                                	</script>
                                </td>
                            </tr>
                             <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="discount" text="#discount#"/></span>:</td>
                                <td width="75%" >
                                    <input type="text" value="${ATTR_BINOFFER_BUSSINESS_DO.discount}"  name="discount"  title="Enter Discount"> (Enter only a number)
                                </td>
                            </tr>
                              <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="min.recharge.amount" text="#min.recharge.amount#"/></span>:</td>
                                <td width="75%" >
                                    <input type="text" value="${ATTR_BINOFFER_BUSSINESS_DO.minRechargeAmount}"  name="min_recharge_amount"  title="Enter Minimum Recharge Amount">
                                </td>
                            </tr>
                             <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="isactive" text="#isactive#"/></span>:</td>
                                <td width="75%" >
                                    <input type="text" value="${ATTR_BINOFFER_BUSSINESS_DO.isActive}"  name="is_active"  title="Enter Is Active ? (1,0)">
                                </td>
                            </tr>
							<tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="coupon.factor" text="#coupon.factor#"/></span>:</td>
                                <td width="75%" >
                                    <input type="text" value="${ATTR_BINOFFER_BUSSINESS_DO.couponFactor}"  name="coupon_factor"  title="Enter Coupon Factor">
                                </td>
                            </tr>
                        <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="valid.from" text="#valid.from#"/></span>:</td>
                                <td width="75%" >
                                    <input type="text" value="${validFrom}"  name="valid_from" readonly title="Enter Valid From">
                                </td>
                            </tr>    
                        <tr>
                        <tr>
                                <td width="25%" ><span id="txt_title"><spring:message code="valid.upto" text="#valid.upto#"/></span>:</td>
                                <td width="75%" >
                                    <input type="text" value="${validUpto}"  name="valid_upto" readonly  title="Enter Valid Upto">
                                </td>
                            </tr> 
                        <tr>
                            <td width="25%" ><span class="red">*</span>Short Description:</td>
                            <td width="75%" >
                                <textarea rows="7" cols="50"  style="width:400px;"   name="short_description" id="short_description"  title="Enter Short Description">${ATTR_BINOFFER_BUSSINESS_DO.shortDescription}</textarea>
                            </td>
                        </tr>       
                        <tr>
                            <td width="25%" ><span class="red">*</span><span id="txt_description"><spring:message code="description" text="#description#"/></span>:</td>
                            <td width="75%" >
                                <textarea rows="7" cols="50"  style="width:400px;"   name="description" id="description"  title="Enter Description">${ATTR_BINOFFER_BUSSINESS_DO.description}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" >Parameters:</td>
                            <td width="75%" >
                                <textarea rows="7" cols="50"  style="width:400px;"   name="datamap" id="datamap"  title="Enter Parameters">${ATTR_BINOFFER_BUSSINESS_DO.datamap}</textarea>
                            	<br/>( For Offer Type - 'REWARD' pass 'Parameters' as <b style="font-size:12px;" >
                            	couponStoreId=</b>66<b style="font-size:12px;" >&amp;couponQuantity=</b>1<b style="font-size:12px;" >&amp;mailerTemplate=</b>icicipvrcoupon<b style="font-size:12px;" >&amp;mailerSubject=</b>Here's your PVR Movie Ticket!&amp;bannerImage=https://d32vr05tkg9faf.cloudfront.net/content/campaigns/ICICI/images/RS_ICICI-PVR-Banner.png  )  
                            	<br/><br/> <br/> 
                            </td>
                        </tr>                       
                       <tr>
                                <td width="25%" >Conditions:</td>
                                <td width="75%" >
                                    <input type="text" value="${ATTR_BINOFFER_BUSSINESS_DO.conditions}"  name="conditions"  title="Enter Conditions" />
                                    <br/> (i.e new_user=true&amp;inactive_user=15) (inactive since 15 days)
                                </td>
                            </tr>

                        <tr>
                            <td colspan="2" align="right">
                                <input type="submit"  value="<c:if test="${not empty ATTR_BINOFFER_BUSSINESS_DO}">Save</c:if><c:if test="${empty ATTR_BINOFFER_BUSSINESS_DO}">Submit</c:if>" class="button"  name="addbut">
                            </td>
                        </tr>

                        </tbody></table>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function() {
	$( "input[name='valid_from']" ).datepicker();
	$( "input[name='valid_upto']" ).datepicker();
});
	
function updateStatus(id, status){
	
	$.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    } }); 
	var url = '/admin/binoffer/updatestatus.htm';
	
	var formData = 'status='+status+'&id='+id;
	$.ajax({
        url: url,
        type: "POST",
        data: formData,
        dataType: "json",
        cache: false,
        timeout:1800000,
        success: function (responseText) {
        	if(responseText.status == 'error'){
        		alert("Activate failed due to some Error.");                
            } else if(responseText.status == 'success'){
            	alert("Activate/De-Activate success.");
            	$('#enableLink'+id).toggle();
            	$('#disableLink'+id).toggle();
            } else {
            	alert("Could not Activate/De-Activate.");
            }
        	$.unblockUI();
        },
        failure: function (responseText) {
        	alert("Activate failed."); 
        	$.unblockUI();
        }
  	}); 
	
}

/* $(function() {
	$( "#effective_from" ).datepicker({dateFormat: 'yy-mm-dd'});
}); */

$(function() {
	$( "#valid_upto" ).datepicker({dateFormat: 'yy-mm-dd'});	
});
</script>
<link rel="stylesheet" href="${mt:keyValue("cssprefix1")}/jHtmlArea.css?v=${mt:keyValue("version.no")}" type="text/css" media="all" />
<script type="text/javascript" src="${mt:keyValue("jsprefix1")}/jHtmlArea-0.7.5.js?v=${mt:keyValue("version.no")}"></script>
<script type="text/javascript">
$(function() {
	$( "#description" ).htmlarea();
	$( "#headDesc" ).htmlarea();
	$( "#tnc" ).htmlarea();
	$( "#mailTnc" ).htmlarea();
});

</script>