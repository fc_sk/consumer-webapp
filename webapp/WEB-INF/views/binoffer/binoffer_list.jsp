<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>

<c:if test="${not empty ATTR_BINOFFER_BUSSINESS_DO_MAP}">
    <div class="bank-offers box">
        <h2>Bank Offers</h2>
        <div class="layer">
            <c:forEach var="binOfferEntry" items="${ATTR_BINOFFER_BUSSINESS_DO_MAP}">
                <div class="bank-offer">
                    <a class="bin-offer-tnc" href="/app/binoffer/gettnc/${binOfferEntry.value.id}">
                        <img src="${mt:keyValue("imgprefix1")}/images/banner/bank-offer/${binOfferEntry.value.imageUrl}" alt="${binOfferEntry.value.bank} - ${binOfferEntry.value.name}" />
                    </a>
                </div>
            </c:forEach>
        </div>
        <div class="info-bar">
            <p>Bank card offers can be availed by either entering your card number and then clicking the "<strong>Check for offer</strong>" link; or by clicking the "<strong>Check for offer</strong>" link near your saved credit or debit card.</p>
        </div>
    </div>
</c:if>
