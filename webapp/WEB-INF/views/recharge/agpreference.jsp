<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@ page import="com.freecharge.freefund.util.FreefundUtil"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body> 
  <div align="center">
  <table>
    <tr>  
  <c:forEach  items="${view}" var="view" varStatus="loopCount">
        <c:forEach  items="${view}" var="map">
        <c:if test="${loopCount.count eq 1}">
    	     	<TH>${map.key}</th>
    	</c:if>     	
        </c:forEach>
  </c:forEach>
  </tr> 
      <c:forEach  items="${view}" var="view">
      <tr>
      <form name="new_post" action = "/admin/rechargeRequestStatus/updateAggregatorPreference" method=POST>
          <c:forEach  items="${view}" var="map">
         	 <td>
         		
         		<c:if test = "${fn:contains(map.key, 'ag_')}" >
         		<div style="border:1px solid gray;">
         			<input type="text" value="${map.value}" name = "${map.key}" >
         		</div>
         		</c:if>
         		
         		<c:if test = "${fn:contains(map.key, 'is_')}" >
         		<div style="border:1px solid gray;">
         			<%-- <input type="text" value="${map.value}" name = "${map.key}" > --%>
         			<select name="${map.key}">
         					<c:if test="${map.value =='1'}">
  							<option  value=1 selected="selected">Enabled</option>
  							<option  value=0>Disabled</option>
  							</c:if>
  							<c:if test="${map.value=='0'}">
  							<option  value=1>Enabled</option>
  							<option  value=0 selected="selected">Disabled</option>
  							</c:if>
					</select>
         		</div>
         		</c:if>
         		
         		<c:if test = "${fn:contains(map.key, 'ag_') == false and fn:contains(map.key, 'is_') == false }" >
         		<div style="border:1px solid gray;">
         			<input type="text" value="${map.value}" name = "${map.key}" readonly="readonly">
         		</div>
         		</c:if>
         		
         	 </td>
          </c:forEach> 
          <!-- <td><input type = "submit" name="submit" onclick='agpreference(event)' /><td> -->
          <td><input type = "submit" name="submit" /><td>
      </form>     
      </tr>
     </c:forEach>
  </table>
  </div>
</body>
</html>

<script language="JavaScript" type="text/javascript">

$(function(){
	  $('form[name=new_post]').submit(function(){
		  var posting = $.post($(this).attr('action'), $(this).serialize(), function(json) {
			  if(json.status=='success') {
				  location.reload(true);
				  alert("Ag Preference updated successfully");
			  } else {
				  alert(json.status);
			  }
	    }, 'json');
	    return false;
	  });
	});
</script>