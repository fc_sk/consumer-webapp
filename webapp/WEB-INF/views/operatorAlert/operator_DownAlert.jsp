<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<c:if test="${not empty successMsg}">
  <div class="alert alert-success">
    <c:out value="${successMsg}" />
  </div>
</c:if>

<c:if test="${not empty errorMsg}">
  <div class="alert alert-error">
    <c:out value="${errorMsg}" />
  </div>
</c:if>

<div class="row span12">
 
  <h2> <a href="/admin/operatoralert/list.htm">View All Existing Operator Alert List</a></h2>
  
  <h2><spring:message code="operator.alert" text="#operator.alert#"/></h2>

    <form name="operatorDownAlertForm" action="/admin/operatoralert/create.htm" method="POST">
      <fieldset>
      <label><spring:message code="select.operator" text="#select.operator#"/></label>
      <select name="operator_list" onchange="updateDesc()" id="operatorList">
        <option value=""><spring:message code="select.option" text="#select.option#"/></option>
        <c:forEach var="item" items="${operatorList}">
          <option value="${item.operatorMasterId}"><c:out value="${item.operatorCode}"/></option>
        </c:forEach>
      </select>
      
      <label><spring:message code="select.circle" text="#select.circle#"/></label>
      <select name="circle_list" onchange="updateDesc()" id="circleList">
        <option value=""><spring:message code="select.option" text="#select.option#"/></option>
        <c:forEach var="item" items="${circles}">
          <option value="${item.circleMasterId}"><c:out value="${item.name}"/></option>
        </c:forEach>
      </select>

      <label><spring:message code="start.date" text="#start.date#"/></label>
      <div id="startDateTime" class="input-append date">
	      <input data-format="yyyy-MM-dd:hh:mm:ss" type="text" value="" name="start_date" id="startDate"/></input>
	      <span class="add-on">
	        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
	      </span>
      </div>
      (yyyy-MM-dd:hh:mm:ss)[Time in:24hr Format]
      
      <label><spring:message code="end.date" text="#end.date#"/></label>
      <div id="endDateTime" class="input-append date">
	      <input data-format="yyyy-MM-dd:hh:mm:ss" type="text" value="" name="end_date" id="endDate"></input>
	      <span class="add-on">
	            <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
	      </span>
      </div>
	  (yyyy-MM-dd:hh:mm:ss)[Time in:24hr Format]
	  
	  <label><spring:message code="alert.message" text="#alert.message#"/></label>
      <input class="span10" type="text" width="100px" value="" name="alertMsg" id="alertText"></input>
      <div class="form-actions">
        <button type="submit" class="btn btn-primary"><spring:message code="save.changes" text="#save.changes#"></spring:message></button>
        <button type="button" class="btn">Cancel</button>
      </div>

      </fieldset>
    </form>
</div>
<script type="text/javascript">
    function updateDesc()
	{
        $('#alertText').val("We are facing a high failure rate for "+$('#operatorList option:selected').text()+
        		" in the last few minutes, please try after some time");
	}
    

    $(function() {
      $('#startDateTime').datetimepicker({
          maskInput: true,
      });
      $('#endDateTime').datetimepicker({
          maskInput: true,
      });
    });
</script>
