<%@include file="/WEB-INF/includes/taglibs.jsp"%>

<form name="operatorDownAlertListForm" action="admin/operatoralert/list.htm" method="GET">
<h2> <a href="/admin/operatoralert/create.htm">Create Operator Down Alert</a></h2>
<c:if test="${not empty noDowntime}">
  <div class="alert no-downtime">
    <c:out value="${noDowntime}" />
  </div>
</c:if>
<c:if test="${not empty param.message}">
        <div class="alert alert-success">
        <c:out value="${param.message}"/>
        </div>
    </c:if>
<br>
 
<c:if test="${not empty operatorAlertList}">
    <table class="table table-bordered table-hover">
    
    <h2> List of All Operator Alerts </h2>
    <thead>
         <tr>
          <th> Operator Name </th>
          <th> Circle Name </th>
          <th> Start Date Time</th>
          <th> End Date Time</th>
          <th> Alert Message</th>
          <th > Delete Alert </th>
         </tr>
    </thead>
    <tbody>
        <c:forEach var="item" items="${operatorAlertList}">
            <tr>
                <td>${item.operatorCode}</td>
                <td>${item.circleName}</td>
                <td>${item.startDateTime}</td>
                <td>${item.endDateTime}</td>  
                <td>${item.alertMessage}</td>
                <td><a href="/admin/operatoralert/delete.htm?operatorId=${item.operatorId}&circleId=${item.circleId}&startDateTime=${item.startDateTime}">Delete</a></td>
            </tr>
        </c:forEach>
    </tbody>	
    </table>
</c:if>
</form>