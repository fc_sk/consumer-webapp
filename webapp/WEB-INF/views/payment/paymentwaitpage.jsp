<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>FreeCharge.in - Recharge Made Free</title>
        <meta name="description" content="FreeCharge.in - Recharge Made Free"  http-equiv="description" >
        <meta name="keywords" content="FreeCharge.in, Recharge Made Free, info@freecharge.in, Accelyst Solutions Pvt. Ltd"  http-equiv="keywords" >
        <meta name="pics-label" content="FreeCharge.in - Recharge Made Free" http-equiv="pics-label"  >
        <meta name="copyright" content="Accelyst Solutions Pvt. Ltd" >


		
        <link rel="stylesheet" media="all" href="${mt:keyValue("cssprefix1")}/layout.css?v=${mt:keyValue("version.no")}" type="text/css" >
        <link rel="stylesheet" media="all" href="${mt:keyValue("cssprefix1")}/main.css?v=${mt:keyValue("version.no")}" type="text/css">
		<meta name="viewport" content="width=device-width, initial-scale=1">


         <!--[if lt IE 7]>
			<link rel="stylesheet" media="all" href="css/ie6.css?v=${mt:keyValue("version.no")}"/>
		<![endif]-->
        <!--[if gte IE 9]>
          <style type="text/css">
            button, a, input, div {
               filter: none!important;
            }
          </style>
        <![endif]-->
 		</head>
		<body class="paymentWaitWrapper" onload="document.paymentRequest.submit()">
             <div class="paymentPageWidth clearfix">
                    <img src="${mt:keyValue("imgprefix1")}/images/ajax-processing.gif" alt="Redirection Under Process!! Please Wait" title="Redirection Under Process!! Please Wait" width="130" height="130" >
                    <p>Please wait while we redirect to the payment gateway for processing... <br>
                    Please do not REFRESH or CLOSE this window.</p>
            </div>
 		</body>
</html>
