<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>

<div style='display: none;'>
    <spring:form name="paymentRequest" id="loginheader" action="${actionUrl}" method="post">
        <table>
            <c:forEach items="${resultMap}" var="mapEntry">
                <tr>
                    <td><input type="hidden" name="${mapEntry.key}" value="${mapEntry.value}"></td>
                </tr>
            </c:forEach>
        </table>
        <input id="isloggedin" type="hidden" value="${isloggedin}">
        <input id="hostPrefix" type="hidden" value="${hostPrefix}">
        <input id="pggatewaybutton" type="submit" value="Open payment gateway in new window">
    </spring:form>
</div>

<c:if test="${pgOpenNewWindow}">
    <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery-1.8.2.min.js?v=${mt:keyValue("version.no")}"></script>

    <script lang="javascript">
    $(document).ready(function () {
	    $("#pggatewaybutton").trigger('click');
    });
  </script>
</c:if>