${code}
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
 <div class="mainContainer"  >
                        <div class="pageWidth clearfix" >
                            
                            <div class="errorMsgWrapp  ">
                            	<img src="${mt:keyValue("imgprefix1")}/images/oops.png" alt="Oops! Page not available" title="Oops! Page not available" width="130" height="130" >
                            	<h5 class="pageHeader " >We're sorry!!!</h5>
                             	<p>The page you are trying to access is not available or has been moved.</p>
                                <p> You may <a href="/app/contactus.htm">contact us</a> or go back to  <a href="${mt:keyValue("hostprefix")}">homepage</a>.</p>
                             </div><!-- /.successMsgWrapper -->
                        </div>  
 </div>

<c:forEach items="${msgs}" var="msgs">
     <fmt:message key="${msgs}" /><br/>
</c:forEach>
