<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<c:if test="${not empty messages }">
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		${messages}
	</div>
</c:if>
<div>
<form:form method="post"
	action="/admin/refund/manual/doCashbackRefund.htm"
	enctype="multipart/form-data">

    <h1>CASH BACK REFUND TOOL</h1>	
    <table>
        <tr>
            <th align="left"><font face="bold" size="3"><spring:message code="order.idss" text="#order.idss#"/></font><font color="grey"> [<spring:message code="cashback.help.text" text="#cashback.help.text#"/>]</font></th>     
        </tr>
        <tr>
            <td><textarea id="orderIds" name="orderIds">${orderIds}</textarea></td>
        </tr>
    </table>
    <br /> <br /><br />
    <br>
    <span class="label label-info"><spring:message code="cashback.note" text="#cashback.note#"/></span>
    <br>
	<table>
		<tr>
			<br>
			<th align="left"><font face="bold" size="3"><spring:message
						code="upload.file" text="#upload.file#" /></font> <font color="grey">[Upload a CSV file containing [orderid,amount][E.g:FCVW13091614173618,30]]</font></th>
		</tr>
		<tr>
			<td><input type="file" class="input" id="refundMtxnids"
				name="refundMtxnids"><br> <br></td>
		</tr>
	</table>
	<br>
	<br>
	<input type="submit" value="Do Refund" />
</form:form>

<div  style="display: none;">
     <br><h3>Cash Back Refund Status:</h3><br/>
         <c:forEach var="cashbackOrderIds" items="${cashBackorderIdList}"> 
          <c:forEach var="cashbackStatus" items="${cashbackOrderIds}">
            <c:out value="${cashbackStatus},"/>
          </c:forEach><br>
        </c:forEach> 
	</div>
	<a href="javascript:void(0);" class="more-info">Show Status</a>	
  </div>



<script type="text/javascript">
	$(document).ready(function() {

		$(".more-info").on("click", function() {

			if ($(this).prev("div").is(':visible')) {
				$(this).prev("div").slideUp(200);
				$(this).text("Show Status");
			} else {
				$(this).prev("div").slideDown(200);
				$(this).text("Show less");
			}
		});
	});
</script>	