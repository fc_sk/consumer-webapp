<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<form method="post" action="/admin/refund/manual/doNewRefund.htm"
	enctype="multipart/form-data">
   <h1><spring:message code="select.file.type" text="#select.file.type#"/></h1>
	<input type="radio" name="radio_names" value="order_id" checked="checked" ><font face="bold" size="2"><spring:message code="orderid.file" text="#orderid.file#"/></font></input><br>
    <input type="radio" name="radio_names" value="request_id"><font face="bold" size="2"><spring:message code="requestId.file" text="#requestId.file#"/></font></input><br/>
    <br>
	<table>
		<tr>
			<th align="left"><font face="bold" size="3"><spring:message code="order.idss" text="#order.idss#"/></font><font color="grey"> [<spring:message code="help.text.orderids" text="#help.text.orderids#"/>]</font></th>		
		</tr>
		<tr>
			<td><textarea id="orderIds" name="orderIds">${orderIds}</textarea></td>
		</tr>
	</table>
	<br /> <br /> <br /> <br />
	<table>
		<tr>
			<th align="left"><font face="bold" size="3"><spring:message code="upload.file" text="#upload.file#"/></font><font color="grey"> [<spring:message code="upload.file.helptext" text="#upload.file.helptext#"/>]</font></th>
		</tr>
		<tr>
			<td><input type="file" class="input" id="refundMtxnids" name="refundMtxnids"><br> <br></td>
		   <%-- <td align="center"><font color="grey"><spring:message code="upload.file.helptext" text="#upload.file.helptext#"/></font></td> --%>
		</tr>
		<tr>
			<th align="left"><font face="bold" size="3"><spring:message code="emailid.reportsent" text="#emailid.reportsent#"/></font><font color="grey"> [<spring:message code="email.id.help.text" text="#email.id.help.text#"/>]</font></th>
		</tr>
		<tr>
			<td><input id="email" name="email"></td>		
		</tr>
	</table>
	<br> <br> <input type="submit" value="Do Refunds" />
</form>

