<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<c:if test="${not empty messages }">
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		${messages}
	</div>
</c:if>
<div>
	<form:form method="post" action="/admin/refund/manual/doCashback.htm"
		enctype="multipart/form-data">

		<center><h2>CASH BACK<h9><span class="label label-success"> Fund source</span></h9></h2></center>
		<h5>Instructions : </h5>
		<ul>
            <li>Use either TextArea or File Upload at a time</li>
            <li>Format of input : EmailId,amount,CallerReference,Metadata(optional) in each row</li>
            <li>Example : uma@gmail.com,30,ARDEAL_26_FAA_31,Frecharge Rating Survey Reward</li>
            <li>Caller Reference should always be unique for every cashback process of email Id(ex:orderid value)</li>
            <li>Must select a Fund source for cash back</li>
        </ul>
		<h4>
			<span class="badge">Step 1</span> <font color="green">Enter
				Cashback Input Data </font>
		</h4>
		<table class="table table-striped table-bordered">
			<tr>
				<th align="left"><font face="bold"> Cashback Input String</font><font
					color="grey"> [In each row ,data are separated by comma ex :
						EmailId,amount,CallerReference,Metadata(optional)] </font></th>
			</tr>
			<tr>
				<td><textarea id="emailIds" name="emailIds">${emailIds}</textarea></td>
			</tr>
		</table>
		<span class="label label-warning">OR</span>
		<table class="table table-striped table-bordered">
			<tr>
				<th align="left"><font face="bold"><spring:message
							code="upload.file" text="#upload.file#" /></font> <font color="grey">[Upload
						a CSV file containing- In each row
						[EmailId,amount,CallerReference,Metadata(optional)]</font></th>
			</tr>
			<tr>
				<td><input type="file" class="input" id="cashbackemailids"
					name="cashbackemailids"><br> <br></td>
			</tr>
		</table>
		<hr>
		<h4>
			<span class="badge">Step 2</span><font color="green"> Select
				a Fund Source</font></h4>
		    <h6>Note : For Recharge cashback refund use <a href="/admin/customertrail/home.htm">CUSTOMER TRAIL</a> page or existing <a href="/admin/refund/manual/doCashbackRefund.htm">CASH BACK</a> tool.</h6>
		
		<table class="table table-striped table-bordered">
			<tr>
				<th align="left"><font face="bold">Fund source</font></th>
			</tr>
			<tr>
				<td><select name="fundSource">
						<c:forEach items="${cashBackNameList}" var="fundSource">
							<c:choose>
								<c:when test="${fundSource eq 'Select FundSource'}">
									<option value="" selected>${fundSource}</option>
								</c:when>
								<c:otherwise>
									<option value="${fundSource}">${fundSource}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
				</select></td>
			</tr>
		</table>
		<br>
		<input type="submit" value="Do Refund" />
	</form:form>

	<div style="display: none;">
		<br>
		<h1>
			<span class="label label-success">Cash Back Refund Success
				Status:</span>
		</h1>
		<c:if test="${not empty successCount }">
			<span class="label label-success">Success count : <c:out
					value="${successCount}" /></span>
		</c:if>
		<br />
		<c:forEach var="cashBackSuccessEmailIds"
			items="${cashBackSuccessEmailIdList}">
			<c:forEach var="cashbackStatus" items="${cashBackSuccessEmailIds}">
				<c:out value="${cashbackStatus}," />
			</c:forEach>
			<br>
		</c:forEach>
		<br>
		<h1>
			<span class="label label-warning">Cash Back Refund Failure
				status :</span>
		</h1>
		<c:if test="${not empty failureCount }">
			<span class="label label-warning">Failure count : <c:out
					value="${failureCount}" /></span>
		</c:if>
		</br>
		<c:forEach var="cashBackFailureEmailIds"
			items="${cashBackFailureEmailIdList}">
			<c:forEach var="cashbackFailStatus"
				items="${cashBackFailureEmailIds}">
				<c:out value="${cashbackFailStatus}," />
			</c:forEach>
			<br>
		</c:forEach>
		<br>
	</div>
	<a href="javascript:void(0);" class="more-info">Show Status</a>
</div>



<script type="text/javascript">
	$(document).ready(function() {

		$(".more-info").on("click", function() {

			if ($(this).prev("div").is(':visible')) {
				$(this).prev("div").slideUp(200);
				$(this).text("Show Status");
			} else {
				$(this).prev("div").slideDown(200);
				$(this).text("Show less");
			}
		});
	});
</script>
