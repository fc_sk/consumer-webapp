<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<table>
    <tr>
        <th colspan="2">Duplicates, Contact the dev team immediately.</th>
    </tr>
    <c:forEach var="entry" items="${result.duplicateMerchantTransactionIds}">
        <tr>
            <td>
                <c:out value="${entry.key}"/>
            </td>
            <td>
                <c:out value="${entry.value}"/>
            </td>
        </tr>
    </c:forEach>
</table>

<table>
    <tr>
        <th>Successfully updated.</th>
    </tr>
    <c:forEach var="success" items="${result.successOrderIds}">
        <tr>
            <td>
                <c:out value="${success}"/>
            </td>
        </tr>
    </c:forEach>
</table>

<table>
    <tr>
        <th>Failed updates</th>
    </tr>
    <c:forEach var="success" items="${result.failedOrderIds}">
        <tr>
            <td>
                <c:out value="${success}"/>
            </td>
        </tr>
    </c:forEach>
</table>

