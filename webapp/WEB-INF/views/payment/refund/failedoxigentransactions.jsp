<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<table>
    <tr>
        <th>Order Id</th>
        <th>Merchant Transaction Id</th>
        <th>Amount</th>
        <th>Created Time</th>
    </tr>
    <c:forEach var="failedOxigenTransaction" items="${failedOxigenTransactions}">
        <tr>
            <td>${failedOxigenTransaction.inRequest.affiliateTransId}</td>
            <td>0_${failedOxigenTransaction.inRequest.requestId}</td>
            <td>${failedOxigenTransaction.inRequest.amount}</td>
            <td>${failedOxigenTransaction.inRequest.createdTime}</td>
        </tr>
    </c:forEach>
</table>
