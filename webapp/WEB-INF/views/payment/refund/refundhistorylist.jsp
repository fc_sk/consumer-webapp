<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<table>
    <tr>
        <th>Order Id</th>
        <th>Refunded Amount</th>
        <th>Refund Status</th>
        <th>PG</th>
        <th>PG Response</th>
    </tr>
    <c:forEach var="refund" items="${refunds}">
        <tr>
            <td>${refund.paymentTransaction.orderId}</td>
            <td>${refund.refundedAmount.amount}</td>
            <td>${refund.status}</td>
            <td>${refund.paymentTransaction.paymentGateway}</td>
            <td>${refund.pgResponseDescription}</td>
        </tr>
    </c:forEach>
</table>

