<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<form:form method="post" action="/admin/refund/manual/doRefund" enctype="multipart/form-data">
    <table>
        <tr>
            <th><spring:message code="order.ids" text="#order.ids#"/></th>
        </tr>
        <tr>
            <td>
                <form:textarea path="orderIds"></form:textarea>
                <form:errors path="orderIds" />
            </td>
        </tr>
    </table>
    <br/><br/>
    <br/><br/>
    <table>
        <tr>
            <th><spring:message code="upload.file" text="#upload.file#"/></th>
        </tr>
        <tr>
            <td>
                <input type="file" class="input"  id="refundMtxnids" name="refundMtxnids" ><br><br>
            </td>
        </tr>
    </table>
    <input type="submit" value="Do Refunds"/>
</form:form>  