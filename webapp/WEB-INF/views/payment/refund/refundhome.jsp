<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<form:form method="post" action="${postAction}">
    <table>
        <tr>
            <th>OrderIds</th>
        </tr>
        <tr>
            <td>
                From(yyyy-MM-dd HH:mm:ss):
            </td>
            <td>
                <form:input path="from"></form:input>
                <form:errors path="from" />
            </td>
        </tr>
        <tr>
            <td>
                To(yyyy-MM-dd HH:mm:ss):
            </td>
            <td>
                <form:input path="to"></form:input>
                <form:errors path="to" />
            </td>
        </tr>
    </table>
    <input type="submit" value="${submitButtonName}"/>
</form:form>
