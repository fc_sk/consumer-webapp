<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@page pageEncoding="UTF-8"%>
<%@page contentType="text/html;charset=UTF-8"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<title>FreeCharge.in - Recharge Made Free</title>
	<script src="${mt:keyValue("jsprefix2")}/jquery-1.8.2.min.js?v=${mt:keyValue("version.no")}"></script>
	<script>
		$(document).ready(function() {
			try {
				var isNewPaymentFlow = "${newPaymentsFlow}";
				var isNewPaymentPage = "${isNewPaymentPage}";
				if((!isNewPaymentFlow || isNewPaymentFlow == null || isNewPaymentFlow === undefined || isNewPaymentFlow === "false")
						&& (!isNewPaymentPage || isNewPaymentPage == null || isNewPaymentPage === undefined || isNewPaymentPage === "false")){
					var url = "${redirectUrl}";
					var finalStatusUrl = "${redirectUrl}?orderId=${orderNo}";
					if (url.indexOf("payment=fail") !== -1) { // payment failure page
						refreshParent(url);
					} else { // payment success page, or something else
						 /* redirectParent(); */
						refreshParent(finalStatusUrl);
					}
				}
				window.close();
			} catch (err) {
				redirect();	
			}
			
			function redirectParent() {
				 var childHandler = window.opener.handleChildWindowRequest; 
				 childHandler("GET", "${redirectUrl}?orderId=", ${orderNo}) 
			}
			
			function refreshParent(url) {
				window.opener.location.href = url;
				if (window.opener.progressWindow) {
					window.opener.progressWindow.close();
				}
			}
			
			function redirect() {
				window.location = "${redirectUrl}?orderId=${orderNo}";
			}
			
			<c:if test="${not pgOpenNewWindow}">
				redirect();
			</c:if>
		});
	</script>
	
	<link rel="stylesheet" media="all" href="${mt:keyValue("cssprefix1")}/layout.css?v=${mt:keyValue("version.no")}" type="text/css">
	<link rel="stylesheet" media="all" href="${mt:keyValue("cssprefix1")}/main.css?v=${mt:keyValue("version.no")} " type="text/css">
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
	
<body class="paymentWaitWrapper">
	<div class="paymentPageWidth clearfix">
		<img src="${mt:keyValue("imgprefix1")}/images/ajax-processing.gif" alt="Redirection Under Process!! Please Wait" title="Redirection Under Process!! Please Wait" width="130" height="130">
		<p>
			Please Wait While We Are Processing Your Payment... <br> Please
			Do Not Press STOP, BACK or REFRESH buttons or CLOSE this window.
		</p>
	</div>
</body>
</html>
