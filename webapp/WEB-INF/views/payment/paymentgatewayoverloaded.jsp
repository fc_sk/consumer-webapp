<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<html>
<head>
<title>FreeCharge Error</title>
    <%@ include file="../../layouts/css.jsp"%>
    <%@ include file="../../layouts/errorheader.jsp"%>
</head>
<body>
 <div class="mainContainer"  >
                        <div class="pageWidth clearfix" >
                            
                            <div class="errorMsgWrapp  ">
                                <img src="${mt:keyValue("imgprefix1")}/images/oops.png" alt="Oops! Payment Gateway is overloaded" title="Oops! Payment Gateway is overloaded" width="130" height="130" >
                                <h5 class="pageHeader " >We're sorry!!!</h5>
                                <p>Payment Gateway is overloaded, please try after some time.</p>
                                <p> You may <a href="/app/contactus.htm">contact us</a> or visit our <a href="${mt:keyValue("hostprefix")}">homepage</a>.</p>
                             </div><!-- /.successMsgWrapper -->
                        </div>
 </div>
<%@ include file="../../layouts/global_footer.jsp"%>
</body>
</html>