<%@include file="/WEB-INF/includes/taglibs.jsp"%>
<%@ page import="com.freecharge.web.controller.ProductPaymentController" %>
<html>
    <script type="text/javascript" src="${mt:keyValue("jsprefix2")}/jquery-1.8.2.min.js?v=${mt:keyValue("version.no")}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var url = '<%=ProductPaymentController.DO_PAYMENT_ACTION%>';
            <%-- Acessing the post params from the parent window  --%>
            var div = $('#paymentFormCont');
            div.empty();
            div.html("<form id='paymentForm' method='POST'  action='" + url + "'></form>");
            var form = $('#paymentForm');
            form.empty();
            var paymentPostParams = window.opener.paymentPostParams;
            for (var key in paymentPostParams) {
                var val = paymentPostParams[key];
                form.append("<input type='hidden' name='" + key + "' value='" + val + "'>");
            }
            form.submit();
        });
    </script>
    <body>
        <div id="paymentFormCont"></div>
    </body>
</html>