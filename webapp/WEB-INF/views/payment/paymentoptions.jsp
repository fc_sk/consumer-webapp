<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="mt" uri="http://www.freecharge.com/myTlds" %>
<div class="mainContainer innerPageGrad">
    <div class="pageWidth clearfix">
        <div class="rechargeProgress"><p class="rechargeProgressTitle"><span
                class="done">Enter Recharge Details</span><span class="done">Select Coupons</span><span class="done">Provide Details</span><span>Pay and Exit</span>
        </p>

            <div class="rechargeProgressBar"><p class="complete lt"><span>&nbsp;</span></p>

                <p class="complete"><span>&nbsp;</span></p><p class="complete"><span>&nbsp;</span></p>

                <p class="rt complete"><span>&nbsp;</span></p></div>
        </div>
        <!-- /.rechargeProgress -->
        <div class="ltContainer fLeft">
            <div class="paymentModeWrapper clearfix"><h2 class="mainHeader ">Make Payment</h2>
                <ul class="paymentModes vertCustomNav fLeft">
                    <li><a href="javascript:void(0);" rel="0" onclick="sendRequest('CC')">Credit Card</a></li>
                    <li><a href="javascript:void(0);" rel="1" onclick="sendRequest('NB')">Net Banking</a></li>
                    <li><a href="javascript:void(0);" rel="2" onclick="sendRequest('DC')">Debit Card</a></li>
                </ul>
                <!-- /.vertCustomNav  -->
                <div class="paymentModes_content fLeft">
                    ${txnfulfilmentstatus}
                    <div class="paymentMode">
                        <div id="CC-paymenttypeDiv"></div>
                        <input type="hidden" id="CC-paytypestatus"
                               value="success"/>
                        <form name="paymentform" action="paymentdetailslist.htm" method="POST">

                        <b>Payment Options are </b> <br/>
                        <c:forEach items="${paymentOptionsList}" var="paymentOptionsList">

                            <input type="radio" name="paymenttype"   value="${paymentOptionsList.displayName}">${paymentOptionsList.displayName}   </input>               <br/>
                        </c:forEach>

                        </form>
                        <!-- /.transactionCredentialsForm -->
                        <div class="transactionOffer fRight"><h6 class="sectionHeaderBrwn">Special Offer!</h6>

                            <p>Use your ICICI Credit Card to get 5% cash back on the payment</p></div>
                        <!-- /.transactionOffer --> </div>
                    <div class="paymentMode">
                        <div id="NB-paymenttypeDiv"></div>
                        <input type="hidden" id="NB-paytypestatus"
                               value=""/>
                        <!-- /.transactionCredentialsForm -->
                        <div class="transactionOffer fRight"><h6 class="sectionHeaderBrwn">Special Offer!</h6>

                            <p>Use your ICICI Credit Card to get 5% cash back on the payment</p></div>
                        <!-- /.transactionOffer --> </div>
                    <div class="paymentMode">
                        <div id="DC-paymenttypeDiv"></div>
                        <input type="hidden" id="DC-paytypestatus"
                               value=""/> 
                        <!-- /.transactionCredentialsForm -->
                        <div class="transactionOffer fRight"><h6 class="sectionHeaderBrwn">Special Offer!</h6>

                            <p>Use your ICICI Credit Card to get 5% cash back on the payment</p></div>
                        <!-- /.transactionOffer --> </div>
                </div>
                <!-- /.paymentModes_content --> <span class="clear">&nbsp;</span>

           <form action="/payment/handlerequest.htm">
                    <input id="at" name="at" value="${paymentAmount}" type="hidden"/>
                        <input id="pycur" name="pycur" value="INR" type="hidden"/>
                        <input id="orderid" name="orderid" value="${orderId}" type="hidden"/>
                        <input id="st" name="st" value="false" type="hidden"/>
                        <input id="pgover" name="pgover" value="" type="hidden"/>
                        <input id="pyop" name="pyop" value="vi" type="hidden"/>
                        <input id="pytype" name="pytype" value="1" type="hidden"/>
                        <input id="frnm" name="frnm" value="${frName}" type="hidden"/>
                        <input id="mbph" name="mbph" value="${personalMobile}" type="hidden"/>
                        <input id="email" name="email" value="${email}" type="hidden"/>
                        <input id="blad1" name="blad1" value="${address}" type="hidden"/>
                        <input id="blctr" name="blctr" value="${cntry}" type="hidden"/>
                        <input id="blst" name="blst" value="${state}" type="hidden"/>
                        <input id="blpin" name="blpin" value="${pin}" type="hidden"/>
                        <input id="blct" name="blct" value="${ct}" type="hidden"/>
                        <input id="scurl" name="scurl" value="/testpayment/handleresponse.htm" type="hidden"/>
                        <input id="errurl" name="errurl" value="/testpayment/handleresponse.htm" type="hidden"/>
                        <input id="pdid" name="pdid" value="1" type="hidden"/>
                        <input id="affid" name="affid" value="1" type="hidden"/>
                        <input id="opid" name="opid" value="1" type="hidden"/>
                        <input id="breaker" name="breaker" value="" type="hidden"/>
                    <input type="submit" class="submitButtonLrg fRight" value="Make payment &gt;">
                                                   </form>
            </div>
            <!-- /.paymentModeWrapper --> </div>
        <!-- /.ltContainer -->
        <div class="rtSidebar fRight">
            <div class="rtSidebarFixed clearfix">
                <div class="couponSelectSummary"><h1 class="mainHeader">Summary</h1>

                    <div class="curveBlock couponSummaryBlock ">
                        <div class="couponSummaryRow rowFirst">
                            <div class="smlPattern clearfix"><h4 class="sectionHeader clearfix"><span
                                    class="title fLeft"><span
                                    class="collapseArrow">&nbsp;</span>Recharge Details  </span> <a href="#"
                                                                                                    class="fRight editLink">Edit</a>
                            </h4>

                                <div class="couponSummaryCont"><p class="topTxt">9819292903 | Rs. 500</p></div>
                            </div>
                            <!-- /.smlPattern -->
                            <div class="editMode clearfix" style="display:none"></div>
                            <!-- /.editMode --> </div>
                        <!-- /.couponSummaryRow -->
                        <div class="couponSummaryRow ">
                            <div class="smlPattern clearfix"><h4 class="sectionHeader clearfix"><span
                                    class="title fLeft"><span class="collapseArrow">&nbsp;</span>Coupon Selection</span>
                                <a href="#" class="fRight editLink">Edit</a></h4>

                                <div class="couponSummaryCont"><p class="topTxt">21 coupons | worth Rs. 500</p></div>
                            </div>
                            <!-- /.smlPattern -->

                            <div class="editMode clearfix" style="display:none">
                                <h4 class="sectionHeader clearfix"><span class="title "><span
                                        class="expandArrow">&nbsp;</span>Coupon Selection</span></h4>

                                <div class="couponSummaryCont">
                                    <p class="topTxt"><i>Handling charge INR 10</i></p>

                                    <div class="couponSelectedAll">
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft ">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft ">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <h6 class="listHeader"><span>Barista (2)</span></h6>

                                        <div class="couponSmall fLeft">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                        <div class="couponSmall fRight">
                                            <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                         width="68" height="50"></span>

                                            <p>Rs. 50</p>
                                        </div>
                                    </div>
                                    <!-- /.couponSelectedAll -->
                                    <p class="leftCoouponSummary">Rs. 150 still left!</p>
                                </div>
                                <!-- /.couponSummaryCont -->
                            </div>
                            <!-- /.editMode -->

                        </div>
                        <!-- /.couponSummaryRow -->

                        <div class="couponSummaryRow ">
                            <div class="smlPattern clearfix">
                                <h4 class="sectionHeader clearfix"><span class="title fLeft"><span
                                        class="collapseArrow">&nbsp;</span>Deals</span> <a href="#"
                                                                                           class="fRight editLink">Edit</a>
                                </h4>

                                <div class="couponSummaryCont">
                                    <p class="topTxt">Tantra T-shirt | worth Rs. 300</p>
                                </div>
                            </div>
                            <!-- /.smlPattern -->

                            <div class="editMode clearfix" style="display:none">
                                <h4 class="sectionHeader clearfix"><span class="title "><span
                                        class="expandArrow">&nbsp;</span>Deals</span></h4>

                                <div class="couponSummaryCont">
                                    <div class="couponSmall padSml fLeft ">
                                        <span class="couponImg"><img src="images/merchant_logos/barista.gif" alt=""
                                                                     width="68" height="50"></span>

                                        <p>Rs. 50</p>
                                    </div>
                                </div>
                                <!-- /.couponSummaryCont -->
                            </div>
                            <!-- /.editMode -->

                        </div>
                        <!-- /.couponSummaryRow -->

                        <div class="couponSummaryRow rowLast">
                            <div class="smlPattern clearfix">
                                <h4 class="sectionHeader clearfix"><span class="title fLeft"><span class="expandArrow">&nbsp;</span>Your Details  </span>
                                    <a href="#" class="fRight editLink">Edit</a></h4>

                                <div class="couponSummaryCont">
                                    <p class="topTxt">Rajesh Singh <br> 123 Jawahar Nagar....</p>
                                </div>
                            </div>
                            <!-- /.smlPattern -->

                            <div class="editMode clearfix" style="display:none">

                            </div>
                            <!-- /.editMode -->

                        </div>
                        <!-- /.couponSummaryRow -->

                    </div>
                    <!-- /.couponSelected  -->

                    <div class="totalPayTxt clearfix"><span class="txt">Total Payment  </span><span class="val">: Rs  510</span>
                    </div>
                </div>
                <!-- /.couponSelectSummary -->
            </div>
        </div>
        <!-- /.rtSidebar -->

    </div>
</div>
<!-- .mainContainer Ends -->
                    
